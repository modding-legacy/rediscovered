package com.legacy.rediscovered.api;

import java.util.Optional;

import com.legacy.rediscovered.block_entities.NetherReactorBlockEntity;
import com.legacy.rediscovered.registry.RediscoveredParticles;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.BonemealableBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.IPlantable;
import net.neoforged.neoforge.common.PlantType;

/**
 * Implement this interface on your Block class in order for it to respond to an
 * active Nether Reactor. {@link INetherReactorGrowable#DEFAULT_IMPL} will be
 * used if the block does not implement this interface.
 */
public interface INetherReactorGrowable
{
	public static INetherReactorGrowable DEFAULT_IMPL = new INetherReactorGrowable()
	{
	};

	public static INetherReactorGrowable get(BlockState state)
	{
		return state.getBlock() instanceof INetherReactorGrowable nrg ? nrg : DEFAULT_IMPL;
	}

	/**
	 * @return The position to display growth particles, if this succeeded
	 */
	default Optional<BlockPos> onNetherReactorGrow(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand, NetherReactorBlockEntity netherReactor)
	{
		if (!state.isAir())
		{
			Block block = state.getBlock();
			if (block instanceof IPlantable plantable && plantable.getPlantType(level, pos) == PlantType.NETHER)
			{
				state.randomTick(level, pos, rand);
				return Optional.of(pos);
			}

			if (state.is(BlockTags.BASE_STONE_NETHER) && block instanceof BonemealableBlock bonemealable)
			{
				if (bonemealable.isValidBonemealTarget(level, pos, state) && bonemealable.isBonemealSuccess(level, rand, pos, state))
				{
					bonemealable.performBonemeal(level, rand, pos, state);
					return Optional.of(pos.above());
				}
			}

			if (state.is(BlockTags.NYLIUM) && level.getBlockState(pos.above()).isAir())
			{
				Block common;
				Block rare;
				if (state.is(Blocks.WARPED_NYLIUM))
				{
					common = Blocks.WARPED_ROOTS;
					rare = Blocks.WARPED_FUNGUS;
				}
				else // Handles crimson by default
				{
					common = Blocks.CRIMSON_ROOTS;
					rare = Blocks.CRIMSON_FUNGUS;
				}
				level.setBlock(pos.above(), (rand.nextFloat() < 0.10F ? rare : common).defaultBlockState(), Block.UPDATE_ALL);
				return Optional.of(pos.above());
			}
		}
		return Optional.empty();
	}

	default void displayNetherReactorGrowth(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand, NetherReactorBlockEntity netherReactor)
	{
		Vec3 p = pos.getCenter();
		double d = 0.3;
		level.sendParticles(RediscoveredParticles.NETHER_REACTOR_GROWTH, p.x, p.y - d, p.z, 10, d, d, d, 0.001);
	}
}
