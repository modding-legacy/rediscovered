package com.legacy.rediscovered;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.rediscovered.block.RediscoveredDispenserBehavior;
import com.legacy.rediscovered.event.RediscoveredEvents;
import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredStructures;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;

@Mod(RediscoveredMod.MODID)
public class RediscoveredMod
{
	public static final String MODID = "rediscovered";
	public static final Logger LOGGER = LogManager.getLogger("ModdingLegacy/" + MODID);
	public static IEventBus MOD_BUS;

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static String find(String key)
	{
		return new String(MODID + ":" + key);
	}

	public RediscoveredMod(IEventBus modBus)
	{
		MOD_BUS = modBus;
		/*ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, RediscoveredConfig.COMMON_SPEC);*/
		ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, RediscoveredConfig.WORLD_SPEC);

		IEventBus forgeBus = NeoForge.EVENT_BUS;

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, RediscoveredConfig.CLIENT_SPEC);

			modBus.register(com.legacy.rediscovered.client.RediscoveredClientEvents.ModEvents.class);
			modBus.addListener(com.legacy.rediscovered.client.RediscoveredResourcePackHandler::packRegistry);
			com.legacy.rediscovered.client.render.RediscoveredRenderType.init(modBus);

			forgeBus.register(com.legacy.rediscovered.client.RediscoveredClientEvents.ForgeEvents.class);
		}

		RediscoveredStructures.init();
		RediscoveredDimensions.init();

		modBus.addListener(RediscoveredEntityTypes::onAttributesRegistered);
		modBus.addListener(EventPriority.LOWEST, RediscoveredEntityTypes::registerPlacements);
		modBus.addListener(EventPriority.NORMAL, RediscoveredEntityTypes::registerPlacementOverrides);

		modBus.addListener(RediscoveredMod::commonInit);

		forgeBus.register(RediscoveredEvents.class);

	}

	private static void commonInit(FMLCommonSetupEvent event)
	{
		event.enqueueWork(() ->
		{
			RediscoveredDispenserBehavior.init();
			RediscoveredEffects.registerPotions();
		});
	}
}
