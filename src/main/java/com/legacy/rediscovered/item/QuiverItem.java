package com.legacy.rediscovered.item;

import java.util.Optional;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.legacy.rediscovered.client.gui.QuiverMenu;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.item.util.QuiverTooltip;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.SlotAccess;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickAction;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.inventory.tooltip.TooltipComponent;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ItemUtils;
import net.minecraft.world.level.Level;

public class QuiverItem extends ArmorItem implements DyeableLeatherItem
{
	public static final int DEFAULT_COLOR = 14444608;

	public QuiverItem(ArmorMaterial material, Type type, Properties properties)
	{
		super(material, type, properties);
	}

	@Override
	public Optional<TooltipComponent> getTooltipImage(ItemStack stack)
	{
		// Chestplates with this attached are handled in an event
		QuiverData quiverData = QuiverData.getOrCreate(stack);
		return Optional.of(new QuiverTooltip(quiverData, this.getColor(stack)));
	}

	@Override
	public boolean overrideOtherStackedOnMe(ItemStack stack, ItemStack other, Slot slot, ClickAction click, Player player, SlotAccess access)
	{
		if (click == ClickAction.SECONDARY && slot.allowModification(player) && !other.isEmpty() && other.is(RediscoveredTags.Items.QUIVER_AMMO) && !(player.containerMenu instanceof QuiverMenu))
		{
			QuiverData quiver = QuiverData.getOrCreate(stack);
			if (quiver.add(other))
			{
				this.playInsertSound(player);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean overrideStackedOnOther(ItemStack stack, Slot slot, ClickAction click, Player player)
	{
		if (click == ClickAction.SECONDARY && slot.allowModification(player) && !(player.containerMenu instanceof QuiverMenu))
		{

			QuiverData quiver = QuiverData.getOrCreate(stack);
			ItemStack other = slot.getItem();
			if (other.isEmpty())
			{
				for (int i = 0; i < QuiverData.QUIVER_SIZE; i++)
				{
					ItemStack ammo = quiver.getAmmo().get(i);
					if (!ammo.isEmpty())
					{
						this.playRemoveOneSound(player);
						slot.set(ammo.copy());
						quiver.setAmmo(i, ItemStack.EMPTY);
						return true;
					}
				}
			}
			else if (other.is(RediscoveredTags.Items.QUIVER_AMMO))
			{
				if (quiver.add(other))
				{
					this.playInsertSound(player);
					return true;
				}
			}
		}
		return false;
	}

	private void playRemoveOneSound(Entity entity)
	{
		entity.playSound(SoundEvents.BUNDLE_REMOVE_ONE, 0.8F, 0.8F + entity.level().getRandom().nextFloat() * 0.4F);
	}

	private void playInsertSound(Entity entity)
	{
		entity.playSound(SoundEvents.BUNDLE_INSERT, 0.8F, 0.8F + entity.level().getRandom().nextFloat() * 0.4F);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		// Chestplates with this attached are handled in an event
		if (player.isSecondaryUseActive())
		{
			ItemStack stack = player.getItemInHand(hand);
			if (player instanceof ServerPlayer serverPlayer)
			{
				serverPlayer.openMenu(new QuiverMenu.Provider(stack, QuiverData.getOrCreate(stack)));
			}
			return InteractionResultHolder.success(stack);
		}
		return super.use(level, player, hand);
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot pSlot)
	{
		return ImmutableMultimap.of();
	}

	@Override
	public int getColor(ItemStack stack)
	{
		CompoundTag displayTag = stack.getTagElement("display");
		return displayTag != null && displayTag.contains("color", 99) ? displayTag.getInt("color") : DEFAULT_COLOR; // Modified default color
	}

	@Override
	public void onDestroyed(ItemEntity itemEntity)
	{
		QuiverData.get(itemEntity.getItem()).ifPresent(quiverData ->
		{
			ItemUtils.onContainerDestroyed(itemEntity, quiverData.getAmmo().stream());
		});
	}
}