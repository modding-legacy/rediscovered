package com.legacy.rediscovered.item;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.rediscovered.item.util.DragonArmorTrim;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class DragonArmorItem extends Item
{
	public DragonArmorItem(Item.Properties properties)
	{
		super(properties);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> tooltips, TooltipFlag isAdvanced)
	{
		if (level != null)
		{
			DragonArmorTrim.appendHoverText(stack, level.registryAccess(), tooltips);
		}
	}
}
