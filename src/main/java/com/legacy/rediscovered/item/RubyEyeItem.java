package com.legacy.rediscovered.item;

import java.util.Optional;

import javax.annotation.Nullable;

import com.legacy.rediscovered.registry.RediscoveredPoiTypes;
import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.village.poi.PoiManager.Occupancy;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.BuiltinStructures;
import net.minecraft.world.level.levelgen.structure.Structure;

public class RubyEyeItem extends BlockItem
{
	public static final int TEXTURE_COUNT = 4;

	public RubyEyeItem(Block block, Properties properties)
	{
		super(block, properties);
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		InteractionResult result = this.place(new BlockPlaceContext(context));
		if (!result.consumesAction())
		{
			InteractionResult useResult = this.use(context.getLevel(), context.getPlayer(), context.getHand()).getResult();
			return useResult == InteractionResult.CONSUME ? InteractionResult.CONSUME_PARTIAL : useResult;
		}
		else
		{
			return result;
		}
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack stack = player.getItemInHand(hand);
		player.startUsingItem(hand);
		if (level instanceof ServerLevel serverLevel && player instanceof ServerPlayer serverPlayer)
		{
			if (stack.is(this))
			{
				if (getStructure(stack) == null)
					setStructure(stack, BuiltinStructures.TRAIL_RUINS);

				BlockPos pos = findAndStoreStructure(stack, serverLevel, player.blockPosition(), serverPlayer);
				if (pos != null)
				{
					return InteractionResultHolder.success(stack);
				}
			}
		}
		return InteractionResultHolder.consume(stack);
	}

	@Override
	public void inventoryTick(ItemStack stack, Level level, Entity entity, int slotID, boolean isSelected)
	{
		// If held in either hand, check for a poi every 3 seconds
		if (entity instanceof ServerPlayer serverPlayer && (isSelected || serverPlayer.getOffhandItem() == stack) && level instanceof ServerLevel serverLevel && level.getGameTime() % 60 == 0)
		{
			findAndStorePoi(stack, serverLevel, entity.blockPosition(), serverPlayer);
		}
	}

	private static final String DIMENSION_KEY = "dimension", STRUCTURE_KEY = "structure", POS_KEY = "pos",
			TEXTURE_INDEX_KEY = "texture_index";

	@Nullable
	public static ResourceKey<Level> getStructureDimension(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.contains(DIMENSION_KEY, Tag.TAG_STRING))
		{
			String dimensionS = tag.getString(DIMENSION_KEY);
			if (ResourceLocation.isValidResourceLocation(dimensionS))
			{
				return ResourceKey.create(Registries.DIMENSION, new ResourceLocation(dimensionS));
			}
		}
		return null;
	}

	public static void setStructureDimension(ItemStack stack, @Nullable ResourceKey<Level> dimension)
	{
		CompoundTag tag = stack.getOrCreateTag();
		if (dimension == null)
			tag.remove(DIMENSION_KEY);
		else
			tag.putString(DIMENSION_KEY, dimension.location().toString());
	}

	@Nullable
	public static BlockPos findAndStoreStructure(ItemStack stack, ServerLevel level, BlockPos searchPos, ServerPlayer player)
	{
		// Find the poi first
		BlockPos poiPos = findAndStorePoi(stack, level, searchPos, player);
		if (poiPos != null)
			return poiPos;

		ResourceKey<Structure> structure = getStructure(stack);
		if (structure != null)
		{
			Optional<Holder.Reference<Structure>> holder = level.registryAccess().registryOrThrow(Registries.STRUCTURE).getHolder(structure);
			if (holder.isPresent())
			{
				@Nullable
				Pair<BlockPos, Holder<Structure>> nearest = level.getChunkSource().getGenerator().findNearestMapStructure(level, HolderSet.direct(holder.get()), searchPos, 100, false);
				if (nearest != null)
				{
					BlockPos pos = nearest.getFirst();
					setStructurePos(stack, pos);
					setStructureDimension(stack, level.dimension());
					return pos;
				}
			}
		}
		return null;
	}

	@Nullable
	protected static BlockPos findAndStorePoi(ItemStack stack, ServerLevel level, BlockPos searchPos, ServerPlayer player)
	{
		var poiData = level.getPoiManager().findClosestWithType(h -> RediscoveredPoiTypes.GLOWING_OBSIDIAN.get().equals(h.value()), searchPos, 48, Occupancy.ANY);
		if (poiData.isPresent())
		{
			BlockPos pos = poiData.get().getSecond();
			setStructurePos(stack, pos);
			setStructure(stack, BuiltinStructures.TRAIL_RUINS);
			setStructureDimension(stack, level.dimension());
			// On find portal from a poi, trigger advancement
			RediscoveredTriggers.LOCATE_PORTAL.get().trigger(player);
			return pos;
		}
		return null;
	}

	@Nullable
	public static ResourceKey<Structure> getStructure(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.contains(STRUCTURE_KEY, Tag.TAG_STRING))
		{
			String structureS = tag.getString(STRUCTURE_KEY);
			if (ResourceLocation.isValidResourceLocation(structureS))
			{
				return ResourceKey.create(Registries.STRUCTURE, new ResourceLocation(structureS));
			}
		}
		return null;
	}

	public static void setStructure(ItemStack stack, @Nullable ResourceKey<Structure> structure)
	{
		CompoundTag tag = stack.getOrCreateTag();
		if (structure == null)
			tag.remove(STRUCTURE_KEY);
		else
			tag.putString(STRUCTURE_KEY, structure.location().toString());
	}

	@Nullable
	public static BlockPos getStructurePos(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.contains(POS_KEY, Tag.TAG_LONG))
		{
			return BlockPos.of(tag.getLong(POS_KEY));
		}
		return null;
	}

	public static void setStructurePos(ItemStack stack, @Nullable BlockPos structurePos)
	{
		CompoundTag tag = stack.getOrCreateTag();
		if (structurePos == null)
			tag.remove(POS_KEY);
		else
			tag.putLong(POS_KEY, structurePos.asLong());
	}

	public static ItemStack setTextureIndex(ItemStack stack, int textureIndex)
	{
		CompoundTag tag = stack.getOrCreateTag();
		tag.putByte(TEXTURE_INDEX_KEY, (byte) textureIndex);
		return stack;
	}

	public static int getTextureIndex(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.contains(TEXTURE_INDEX_KEY, Tag.TAG_BYTE))
		{
			return tag.getByte(TEXTURE_INDEX_KEY);
		}
		return -1;
	}
}
