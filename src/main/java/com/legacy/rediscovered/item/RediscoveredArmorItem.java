package com.legacy.rediscovered.item;

import java.util.EnumMap;
import java.util.UUID;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.legacy.rediscovered.item.util.RediscoveredArmorMaterial;
import com.legacy.rediscovered.registry.RediscoveredAttributes;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.Util;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.ItemStack;

public class RediscoveredArmorItem extends ArmorItem
{
	private static final EnumMap<ArmorItem.Type, UUID> ARMOR_MODIFIER_UUID_PER_TYPE = Util.make(new EnumMap<>(ArmorItem.Type.class), (p_266744_) ->
	{
		p_266744_.put(ArmorItem.Type.BOOTS, UUID.fromString("845DB27C-C624-495F-8C9F-6020A9A58B6B"));
		p_266744_.put(ArmorItem.Type.LEGGINGS, UUID.fromString("D8499B04-0E66-4726-AB29-64469D734E0D"));
		p_266744_.put(ArmorItem.Type.CHESTPLATE, UUID.fromString("9F3D476D-C118-4544-8365-64846904B48E"));
		p_266744_.put(ArmorItem.Type.HELMET, UUID.fromString("2AD3F246-FEE1-4E67-B886-69FD380BB150"));
	});
	private final Multimap<Attribute, AttributeModifier> customModifiers;

	public RediscoveredArmorItem(ArmorMaterial material, Type type, Properties properties)
	{
		super(material, type, properties);

		// Combines the original attribute modifiers with the new custom ones. customModifiers needs to be referenced in places where the original would be called.
		ImmutableMultimap.Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		builder.putAll(super.getDefaultAttributeModifiers(type.getSlot()));
		UUID uuid = ARMOR_MODIFIER_UUID_PER_TYPE.get(this.getType());
		if (material instanceof RediscoveredArmorMaterial mat)
		{
			builder.put(RediscoveredAttributes.EXPLOSION_RESISTANCE.get(), new AttributeModifier(uuid, "Armor explosion resistance", mat.getExplosionResistance(), AttributeModifier.Operation.MULTIPLY_BASE));
			builder.put(RediscoveredAttributes.FIRE_RESISTANCE.get(), new AttributeModifier(uuid, "Armor fire resistance", mat.getFireResistance(), AttributeModifier.Operation.MULTIPLY_BASE));
		}
		this.customModifiers = builder.build();
	}

	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot)
	{
		return this.getType().getSlot() == slot ? this.customModifiers : ImmutableMultimap.of();
	}

	@Override
	public boolean canWalkOnPowderedSnow(ItemStack stack, LivingEntity wearer)
	{
		return stack.is(RediscoveredItems.studded_boots) || super.canWalkOnPowderedSnow(stack, wearer);
	}

	public static class Dyeable extends RediscoveredArmorItem implements DyeableLeatherItem
	{
		public Dyeable(ArmorMaterial material, Type type, Properties properties)
		{
			super(material, type, properties);
		}
	}
}
