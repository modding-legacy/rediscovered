package com.legacy.rediscovered.item;

import java.util.List;
import java.util.stream.Collectors;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.level.ItemLike;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.common.util.MutableHashedLinkedMap;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;

@Mod.EventBusSubscriber(modid = RediscoveredMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class RediscoveredCreativeTabs
{
	public static final Lazy<List<Item>> SPAWN_EGGS = Lazy.of(() -> BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(RediscoveredMod.MODID) && item instanceof SpawnEggItem).sorted((o, n) -> BuiltInRegistries.ITEM.getKey(o).compareTo(BuiltInRegistries.ITEM.getKey(n))).collect(Collectors.toList()));

	@SubscribeEvent
	public static void modifyExisting(BuildCreativeModeTabContentsEvent event)
	{
		MutableHashedLinkedMap<ItemStack, TabVisibility> entries = event.getEntries();
		ResourceKey<CreativeModeTab> tab = event.getTabKey();
		
		if (tab.equals(CreativeModeTabs.SPAWN_EGGS))
		{
			for (Item item : SPAWN_EGGS.get())
				event.accept(item);
		}

		if (tab.equals(CreativeModeTabs.COMBAT))
		{
			insertAfter(entries, List.of(RediscoveredItems.studded_helmet, RediscoveredItems.studded_chestplate, RediscoveredItems.studded_leggings, RediscoveredItems.studded_boots, RediscoveredItems.plate_helmet, RediscoveredItems.plate_chestplate, RediscoveredItems.plate_leggings, RediscoveredItems.plate_boots), Items.CHAINMAIL_BOOTS);

			insertAfter(entries, List.of(RediscoveredItems.quiver), Items.TURTLE_HELMET);

			insertAfter(entries, List.of(RediscoveredItems.purple_arrow), Items.SPECTRAL_ARROW);
			
			insertAfter(entries, List.of(RediscoveredItems.dragon_armor), Items.DIAMOND_HORSE_ARMOR);
		}

		if (tab.equals(CreativeModeTabs.TOOLS_AND_UTILITIES))
		{
			insertAfter(entries, List.of(RediscoveredBlocks.ruby_eye), Items.RECOVERY_COMPASS);
			insertAfter(entries, List.of(RediscoveredItems.music_disc_calm4), Items.MUSIC_DISC_WAIT);
			insertAfter(entries, List.of(RediscoveredItems.fish_bucket), Items.COD_BUCKET);

			insertAfter(entries, List.of(RediscoveredItems.ruby_flute, RediscoveredItems.scarecrow), Items.WARPED_FUNGUS_ON_A_STICK);
		}

		if (tab.equals(CreativeModeTabs.REDSTONE_BLOCKS))
		{
			event.accept(RediscoveredBlocks.rotational_converter);
			event.accept(RediscoveredBlocks.gear);
			insertAfter(entries, List.of(RediscoveredBlocks.obsidian_bulb), Items.REDSTONE_LAMP);
		}

		if (tab.equals(CreativeModeTabs.FUNCTIONAL_BLOCKS))
		{
			insertAfter(entries, List.of(RediscoveredBlocks.spikes), Items.MAGMA_BLOCK);
			insertAfter(entries, List.of(RediscoveredBlocks.red_dragon_egg), Items.DRAGON_EGG);
			insertAfter(entries, List.of(RediscoveredItems.scarecrow), Items.ARMOR_STAND);
			insertAfter(entries, List.of(RediscoveredBlocks.nether_reactor_core), Items.BEACON);
			insertAfter(entries, List.of(RediscoveredBlocks.mini_dragon_pylon), Items.END_CRYSTAL);
			event.accept(RediscoveredBlocks.dragon_altar);
		}

		if (tab.equals(CreativeModeTabs.INGREDIENTS))
		{
			insertAfter(entries, List.of(RediscoveredItems.ruby), Items.EMERALD);
			insertAfter(entries, List.of(RediscoveredItems.draconic_trim, RediscoveredItems.dragon_armor_chain_smithing_template, RediscoveredItems.dragon_armor_plating_smithing_template, RediscoveredItems.dragon_armor_inlay_smithing_template), Items.SPIRE_ARMOR_TRIM_SMITHING_TEMPLATE);
		}

		if (tab.equals(CreativeModeTabs.COLORED_BLOCKS))
		{
			insertAfter(entries, List.of(RediscoveredBlocks.bright_green_wool, RediscoveredBlocks.spring_green_wool, RediscoveredBlocks.sky_blue_wool, RediscoveredBlocks.slate_blue_wool, RediscoveredBlocks.lavender_wool, RediscoveredBlocks.rose_wool), Items.PINK_WOOL);
			insertAfter(entries, List.of(RediscoveredBlocks.bright_green_carpet, RediscoveredBlocks.spring_green_carpet, RediscoveredBlocks.sky_blue_carpet, RediscoveredBlocks.slate_blue_carpet, RediscoveredBlocks.lavender_carpet, RediscoveredBlocks.rose_carpet), Items.PINK_CARPET);
		}

		if (tab.equals(CreativeModeTabs.FOOD_AND_DRINKS))
		{
			insertAfter(entries, List.of(RediscoveredItems.raw_fish, RediscoveredItems.cooked_fish), Items.COOKED_COD);
		}

		if (tab.equals(CreativeModeTabs.NATURAL_BLOCKS))
		{
			insertAfter(entries, List.of(RediscoveredBlocks.ruby_ore, RediscoveredBlocks.deepslate_ruby_ore), Items.DEEPSLATE_EMERALD_ORE);
			insertAfter(entries, List.of(RediscoveredBlocks.ancient_crying_obsidian, RediscoveredBlocks.glowing_obsidian), Items.CRYING_OBSIDIAN);

			insertAfter(entries, List.of(RediscoveredBlocks.rose, RediscoveredBlocks.paeonia, RediscoveredBlocks.cyan_rose), Items.POPPY);

			insertAfter(entries, List.of(RediscoveredBlocks.ancient_cherry_leaves), Items.CHERRY_LEAVES);
			insertAfter(entries, List.of(RediscoveredBlocks.ancient_cherry_sapling), Items.CHERRY_SAPLING);
		}

		if (tab.equals(CreativeModeTabs.BUILDING_BLOCKS))
		{
			event.accept(RediscoveredBlocks.grass_slab);
			event.accept(RediscoveredBlocks.podzol_slab);
			event.accept(RediscoveredBlocks.mycelium_slab);
			event.accept(RediscoveredBlocks.dirt_path_slab);
			event.accept(RediscoveredBlocks.dirt_slab);
			event.accept(RediscoveredBlocks.coarse_dirt_slab);
			event.accept(RediscoveredBlocks.rooted_dirt_slab);

			insertAfter(entries, List.of(RediscoveredBlocks.ruby_block), Items.EMERALD_BLOCK);

			insertAfter(entries, List.of(RediscoveredBlocks.oak_chair, RediscoveredBlocks.oak_table), Items.OAK_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.spruce_chair, RediscoveredBlocks.spruce_table), Items.SPRUCE_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.birch_chair, RediscoveredBlocks.birch_table), Items.BIRCH_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.jungle_chair, RediscoveredBlocks.jungle_table), Items.JUNGLE_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.acacia_chair, RediscoveredBlocks.acacia_table), Items.ACACIA_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.dark_oak_chair, RediscoveredBlocks.dark_oak_table), Items.DARK_OAK_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.mangrove_chair, RediscoveredBlocks.mangrove_table), Items.MANGROVE_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.cherry_chair, RediscoveredBlocks.cherry_table), Items.CHERRY_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.bamboo_chair, RediscoveredBlocks.bamboo_table), Items.BAMBOO_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.crimson_chair, RediscoveredBlocks.crimson_table), Items.CRIMSON_TRAPDOOR);
			insertAfter(entries, List.of(RediscoveredBlocks.warped_chair, RediscoveredBlocks.warped_table), Items.WARPED_TRAPDOOR);

			insertAfter(entries, List.of(RediscoveredBlocks.large_bricks, RediscoveredBlocks.large_brick_stairs, RediscoveredBlocks.large_brick_slab, RediscoveredBlocks.large_brick_wall), Items.BRICK_WALL);

			insertAfter(entries, List.of(RediscoveredBlocks.brittle_packed_mud, RediscoveredBlocks.brittle_mud_bricks), Items.MUD_BRICK_WALL);
		}
	}

	protected static void insertAfter(MutableHashedLinkedMap<ItemStack, TabVisibility> entries, List<ItemLike> items, ItemLike target)
	{
		ItemStack currentStack = null;

		for (var e : entries)
		{
			if (e.getKey().getItem() == target)
			{
				currentStack = e.getKey();
				break;
			}
		}

		for (var item : items)
			entries.putAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
	}
}
