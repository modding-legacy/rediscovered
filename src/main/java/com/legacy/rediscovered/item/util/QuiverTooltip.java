package com.legacy.rediscovered.item.util;

import net.minecraft.world.inventory.tooltip.TooltipComponent;

public record QuiverTooltip(QuiverData data, int color) implements TooltipComponent
{
}
