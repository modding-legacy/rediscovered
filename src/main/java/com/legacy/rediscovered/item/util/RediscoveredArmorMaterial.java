package com.legacy.rediscovered.item.util;

import java.util.EnumMap;
import java.util.function.Supplier;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.data.RediscoveredTags;

import net.minecraft.Util;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.crafting.Ingredient;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.util.Lazy;

public enum RediscoveredArmorMaterial implements ArmorMaterial
{
	// @formatter:off
	QUIVER("quiver", -1, armorValues(0, 0, 0, 0), 0, SoundEvents.ARMOR_EQUIP_LEATHER, 0.0F, 0.0F, () -> Ingredient.of(RediscoveredTags.Items.QUIVER_REPAIR_MATERIALS), 0.0F, 0.0F),
	STUDDED("studded", 13, armorValues(2, 4, 5, 2), 13, RediscoveredSounds.ITEM_ARMOR_EQUIP_STUDDED, 0.0F, 0.0F, () -> Ingredient.of(RediscoveredTags.Items.STUDDED_ARMOR_REPAIR_MATERIALS), 0.0F, 0.08F),
	PLATE("plate", 25, armorValues(2, 5, 6, 2), 12,  RediscoveredSounds.ITEM_ARMOR_EQUIP_PLATE, 3.0F, 0.1F, () -> Ingredient.of(RediscoveredTags.Items.PLATE_ARMOR_REPAIR_MATERIALS), 0.15F, 0.0F);
	// @formatter:on

	private static final EnumMap<ArmorItem.Type, Integer> HEALTH_FUNCTION_FOR_TYPE = armorValues(13, 15, 16, 11);

	private final String name;
	private final int durabilityMultiplier;
	private final EnumMap<ArmorItem.Type, Integer> damageReductionAmountArray;
	private final int enchantability;
	private final SoundEvent soundEvent;
	private final float toughness;
	private final float knockbackResist;
	private final Lazy<Ingredient> repairMaterial;
	
	private final float explosionResist;
	private final float fireResist;
	
	private RediscoveredArmorMaterial(String name, int durabilityMultiplier, EnumMap<ArmorItem.Type, Integer> damageReductionArray, int enchantability, SoundEvent equipSound, float toughness, float knockbackResist, Supplier<Ingredient> repairMaterial, float explosionResist, float fireResist)
	{
		this.name = RediscoveredMod.find(name);
		this.durabilityMultiplier = durabilityMultiplier;
		this.damageReductionAmountArray = damageReductionArray;
		this.enchantability = enchantability;
		this.soundEvent = equipSound;
		this.toughness = toughness;
		this.knockbackResist = knockbackResist;
		this.repairMaterial = Lazy.of(repairMaterial);
		
		this.explosionResist = explosionResist;
		this.fireResist = fireResist;
	}
	
	@Override
	public int getDurabilityForType(ArmorItem.Type type)
	{
		return HEALTH_FUNCTION_FOR_TYPE.get(type) * this.durabilityMultiplier;
	}

	@Override
	public int getDefenseForType(ArmorItem.Type type)
	{
		return this.damageReductionAmountArray.get(type);
	}

	@Override
	public int getEnchantmentValue()
	{
		return this.enchantability;
	}

	@Override
	public SoundEvent getEquipSound()
	{
		return this.soundEvent;
	}

	@Override
	public Ingredient getRepairIngredient()
	{
		return this.repairMaterial.get();
	}

	@OnlyIn(Dist.CLIENT)
	public String getName()
	{
		return this.name;
	}

	@Override
	public float getToughness()
	{
		return this.toughness;
	}

	@Override
	public float getKnockbackResistance()
	{
		return this.knockbackResist;
	}
	
	public float getExplosionResistance()
	{
		return this.explosionResist;
	}
	
	public float getFireResistance()
	{
		return this.fireResist;
	}

	public static EnumMap<ArmorItem.Type, Integer> armorValues(int boots, int legs, int chest, int helmet)
	{
		return Util.make(new EnumMap<>(ArmorItem.Type.class), (material) ->
		{
			material.put(ArmorItem.Type.BOOTS, boots);
			material.put(ArmorItem.Type.LEGGINGS, legs);
			material.put(ArmorItem.Type.CHESTPLATE, chest);
			material.put(ArmorItem.Type.HELMET, helmet);
		});
	}
}