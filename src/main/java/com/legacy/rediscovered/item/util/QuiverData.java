package com.legacy.rediscovered.item.util;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ProjectileWeaponItem;

public class QuiverData implements AttachedItem.AttachedTracker
{
	public static final byte QUIVER_SIZE = 5;
	public static final Codec<QuiverData> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ItemStack.CODEC.listOf().fieldOf("ammo").forGetter(i -> i.getAmmo()), Codec.BYTE.fieldOf("selected_slot").forGetter(i -> i.getSelectedSlot())).apply(instance, QuiverData::new);
	});

	private ItemStack stack = null;
	private AttachedItem attachedItem = null;

	private final NonNullList<ItemStack> ammo = NonNullList.withSize(QUIVER_SIZE, ItemStack.EMPTY);
	private byte selectedSlot = 0;

	public QuiverData(List<ItemStack> ammo, byte selectedSlot)
	{
		int size = Math.min(QUIVER_SIZE, ammo.size());
		this.ammo.clear();
		for (byte i = 0; i < size; i++)
			this.ammo.set(i, ammo.get(i));

		this.selectedSlot = selectedSlot;
	}

	public QuiverData()
	{
	}

	public QuiverData setQuiver(ItemStack stack)
	{
		this.stack = stack;
		return this;
	}

	private static final String QUIVER_DATA_TAG = "rediscovered_quiver_data", SELECTED_SLOT_KEY = "selected_slot";

	public static QuiverData getOrCreate(ItemStack stack)
	{
		Optional<QuiverData> data = get(stack);
		if (data.isPresent())
			return data.get();
		return new QuiverData().setQuiver(stack);
	}

	public static Optional<QuiverData> get(ItemStack stack)
	{
		if (!stack.isEmpty())
		{
			CompoundTag tag = stack.getTag();
			if (tag != null && tag.contains(QUIVER_DATA_TAG, Tag.TAG_COMPOUND))
			{
				QuiverData data = new QuiverData().setQuiver(stack);

				CompoundTag dataTag = tag.getCompound(QUIVER_DATA_TAG);
				ContainerHelper.loadAllItems(dataTag, data.ammo);
				data.selectedSlot = dataTag.getByte(SELECTED_SLOT_KEY);

				return Optional.of(data);
			}
		}
		return Optional.empty();
	}

	public void save()
	{
		if (this.stack != null)
		{
			CompoundTag tag = this.stack.getOrCreateTag();

			CompoundTag dataTag = new CompoundTag();
			ContainerHelper.saveAllItems(dataTag, this.ammo);
			dataTag.putByte(SELECTED_SLOT_KEY, this.selectedSlot);

			tag.put(QUIVER_DATA_TAG, dataTag);
		}

		if (this.attachedItem != null)
			this.attachedItem.save();
	}

	public static boolean isPresent(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		return tag != null && tag.contains(QUIVER_DATA_TAG, Tag.TAG_COMPOUND);
	}

	public void cycleNextSlot(ItemStack weapon)
	{
		Set<Byte> slotsWithAmmo = this.getSlotsWithAmmo(weapon);
		if (slotsWithAmmo.size() < 1)
		{
			this.selectedSlot = 0;
		}
		else
		{
			for (int i = 0; i < QUIVER_SIZE; i++)
			{
				this.selectedSlot++;
				if (this.selectedSlot >= QUIVER_SIZE)
					this.selectedSlot = 0;
				if (slotsWithAmmo.contains(this.selectedSlot))
					break;
			}
		}
		this.save();
	}

	private Set<Byte> getSlotsWithAmmo(ItemStack weapon)
	{
		Set<Byte> slots = new HashSet<>();
		for (byte i = 0; i < QUIVER_SIZE; i++)
			if (isAmmo(this.ammo.get(i), weapon))
				slots.add(i);
		return slots;
	}

	public byte getSelectedSlot()
	{
		return this.selectedSlot;
	}

	public ItemStack getSelectedAmmo(ItemStack weapon)
	{
		ItemStack ammo = this.ammo.get(this.selectedSlot);
		if (isAmmo(ammo, weapon))
			return ammo;
		this.cycleNextSlot(weapon);
		ammo = this.ammo.get(this.selectedSlot);
		return isAmmo(ammo, weapon) ? ammo : ItemStack.EMPTY;
	}

	public NonNullList<ItemStack> getAmmo()
	{
		return this.ammo;
	}

	public void setAmmo(int slot, ItemStack stack)
	{
		this.ammo.set(slot, stack);
		this.save();
	}

	public boolean add(ItemStack stack)
	{
		boolean addedAmmo = false;
		for (int i = 0; i < QUIVER_SIZE; i++)
		{
			// If a slot is empty, put the ammo in it and be done
			ItemStack ammo = this.ammo.get(i);
			if (ammo.isEmpty())
			{
				this.ammo.set(i, stack.copyAndClear());
				addedAmmo = true;
				break;
			}
			// If the arrow matches the slot checked, add as many as possible
			else if (!stack.isEmpty() && ammo.getCount() < ammo.getMaxStackSize() && ItemStack.isSameItemSameTags(stack, ammo))
			{
				int canAdd = ammo.getMaxStackSize() - ammo.getCount();
				int toAdd = Math.min(canAdd, stack.getCount());
				ammo.grow(toAdd);
				stack.shrink(toAdd);
				addedAmmo = true;
			}
		}
		if (addedAmmo)
			this.save();
		return addedAmmo;
	}

	public void injectData(NonNullList<ItemStack> ammo)
	{
		int size = Math.min(QUIVER_SIZE, ammo.size());
		// Fill contents
		this.ammo.clear();
		for (byte i = 0; i < size; i++)
			this.ammo.set(i, ammo.get(i));

		// If the current slot is empty, cycle to the next
		if (this.ammo.get(this.selectedSlot).isEmpty())
			this.cycleNextSlot(Items.BOW.getDefaultInstance());
		this.save();
	}

	public void onConsumeAmmo(ItemStack weapon)
	{
		ItemStack stack = this.ammo.get(this.selectedSlot);
		if (stack.isEmpty())
			this.cycleNextSlot(weapon);
		this.save();
	}

	public ItemStack getQuiver()
	{
		return this.stack;
	}

	public static Optional<QuiverData> getFromChestplate(ItemStack chestplate)
	{
		return Optional.ofNullable(AttachedItem.getFromSelfOrAttached(chestplate, (s, q) -> q != null, s -> QuiverData.get(s).orElseGet(() -> null), () -> null));
	}

	public static Pair<InteractionHand, ItemStack> getHeldBow(LivingEntity entity)
	{
		ItemStack stack = entity.getItemInHand(InteractionHand.MAIN_HAND);
		if (stack.is(RediscoveredTags.Items.QUIVER_USER))
			return Pair.of(InteractionHand.MAIN_HAND, stack);
		stack = entity.getItemInHand(InteractionHand.OFF_HAND);
		return stack.is(RediscoveredTags.Items.QUIVER_USER) ? Pair.of(InteractionHand.OFF_HAND, stack) : Pair.of(InteractionHand.MAIN_HAND, ItemStack.EMPTY);
	}

	public static boolean isHoldingBow(LivingEntity entity)
	{
		return !getHeldBow(entity).getSecond().isEmpty();
	}

	public static boolean isHoldingAmmo(LivingEntity entity, ItemStack weaponStack)
	{
		Predicate<ItemStack> predicate = weaponStack.getItem() instanceof ProjectileWeaponItem projWeapon ? projWeapon.getSupportedHeldProjectiles() : s -> true;
		return !ProjectileWeaponItem.getHeldProjectile(entity, predicate.and(s -> s.is(RediscoveredTags.Items.QUIVER_AMMO))).isEmpty();
	}

	public static boolean isAmmo(ItemStack ammoStack, ItemStack weaponStack)
	{
		return isAmmo(ammoStack, weaponStack.getItem() instanceof ProjectileWeaponItem projWeapon ? projWeapon.getSupportedHeldProjectiles() : s -> true);
	}

	public static boolean isAmmo(ItemStack ammoStack, Predicate<ItemStack> predicate)
	{
		return predicate.and(s -> s.is(RediscoveredTags.Items.QUIVER_AMMO)).test(ammoStack);
	}

	public static boolean canAttachQuiver(ItemStack stack)
	{
		return stack.is(RediscoveredTags.Items.QUIVER_APPLICABLE) && !stack.is(RediscoveredTags.Items.QUIVER_INAPPLICABLE);
	}

	@Override
	@Nullable
	public AttachedItem getAttachedItem()
	{
		return this.attachedItem;
	}

	@Override
	public void setAttachedItem(@Nullable AttachedItem attached)
	{
		this.attachedItem = attached;
	}
}
