package com.legacy.rediscovered.item.util;

import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;

public class AttachedItem
{
	public static final Codec<AttachedItem> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ItemStack.CODEC.fieldOf("attached").forGetter(i -> i.getAttached())).apply(instance, AttachedItem::new);
	});

	private ItemStack stack = null;
	private ItemStack attachedStack;

	private AttachedItem(ItemStack attachedStack)
	{
		this.attachedStack = attachedStack;
	}

	public AttachedItem setOwner(ItemStack stack)
	{
		this.stack = stack;
		return this;
	}

	private static final String ATTACHED_ITEM_TAG = "rediscovered_attached_item", ITEM_KEY = "item";

	public static Optional<AttachedItem> getOrCreate(ItemStack stack)
	{
		Optional<AttachedItem> attached = get(stack);
		if (attached.isPresent())
			return attached;
		return Optional.of(new AttachedItem(ItemStack.EMPTY).setOwner(stack));
	}

	public static Optional<AttachedItem> get(ItemStack stack)
	{
		if (!stack.isEmpty())
		{
			CompoundTag tag = stack.getTag();
			if (tag != null && tag.contains(ATTACHED_ITEM_TAG, Tag.TAG_COMPOUND))
			{
				CompoundTag attachedTag = tag.getCompound(ATTACHED_ITEM_TAG);
				ItemStack attachedItem = ItemStack.of(attachedTag.getCompound(ITEM_KEY));
				return Optional.of(new AttachedItem(attachedItem).setOwner(stack));
			}
		}
		return Optional.empty();
	}

	public void save()
	{
		if (this.stack == null)
			return;

		if (this.attachedStack.isEmpty())
		{
			CompoundTag tag = this.stack.getTag();
			if (tag != null && tag.contains(ATTACHED_ITEM_TAG, Tag.TAG_COMPOUND))
			{
				tag.remove(ATTACHED_ITEM_TAG);
			}
		}
		else
		{
			CompoundTag tag = this.stack.getOrCreateTag();

			CompoundTag attachedTag = new CompoundTag();
			attachedTag.put(ITEM_KEY, this.attachedStack.save(new CompoundTag()));

			tag.put(ATTACHED_ITEM_TAG, attachedTag);
		}
	}

	public ItemStack getAttached()
	{
		return this.attachedStack;
	}

	public void setAttached(ItemStack attachedStack)
	{
		this.attachedStack = attachedStack.copy();
		this.save();
	}

	public static boolean hasAttached(ItemStack stack)
	{
		var attached = get(stack);
		return attached.isPresent() && !attached.get().getAttached().isEmpty();
	}

	public static ItemStack attachItem(ItemStack stack, ItemStack attachment)
	{
		Optional<AttachedItem> data = getOrCreate(stack);
		if (data.isPresent())
			data.get().setAttached(attachment);
		return stack;
	}

	public static ItemStack removeAttachment(ItemStack stack)
	{
		Optional<AttachedItem> data = getOrCreate(stack);
		if (data.isPresent())
			data.get().setAttached(ItemStack.EMPTY);
		return stack;
	}

	public static <T extends AttachedTracker> T getFromSelfOrAttached(ItemStack stack, BiPredicate<ItemStack, T> validator, Function<ItemStack, T> function, Supplier<T> defaultVal)
	{
		T ret = function.apply(stack);
		if (validator.test(stack, ret))
			return ret;

		Optional<AttachedItem> attachedItem = getOrCreate(stack);
		if (attachedItem.isPresent())
		{
			ItemStack attached = attachedItem.get().getAttached();
			ret = function.apply(attached);
			if (validator.test(attached, ret))
			{
				ret.setAttachedItem(attachedItem.get());
				return ret;
			}
		}
		return defaultVal.get();
	}

	public static interface AttachedTracker
	{
		void setAttachedItem(@Nullable AttachedItem attached);

		@Nullable
		AttachedItem getAttachedItem();
	}
}
