package com.legacy.rediscovered.item.util;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.armortrim.TrimMaterial;

public record DragonArmorTrim(Optional<Holder<TrimMaterial>> chain, Optional<Holder<TrimMaterial>> plating, Optional<Holder<TrimMaterial>> inlay)
{

	public static final Codec<DragonArmorTrim> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(TrimMaterial.CODEC.optionalFieldOf(Decoration.CHAIN.getSerializedName()).forGetter(DragonArmorTrim::chain), TrimMaterial.CODEC.optionalFieldOf(Decoration.PLATING.getSerializedName()).forGetter(DragonArmorTrim::plating), TrimMaterial.CODEC.optionalFieldOf(Decoration.INLAY.getSerializedName()).forGetter(DragonArmorTrim::inlay)).apply(instance, DragonArmorTrim::new);
	});

	public static final DragonArmorTrim EMPTY = new DragonArmorTrim(Optional.empty(), Optional.empty(), Optional.empty());

	public static DragonArmorTrim create(Registry<TrimMaterial> materialRegistry, @Nullable ResourceKey<TrimMaterial> base, @Nullable ResourceKey<TrimMaterial> plate, @Nullable ResourceKey<TrimMaterial> accent)
	{
		var baseH = Optional.ofNullable(base).map(k -> (Holder<TrimMaterial>) materialRegistry.getHolder(k).orElse(null));
		var plateH = Optional.ofNullable(plate).map(k -> (Holder<TrimMaterial>) materialRegistry.getHolder(k).orElse(null));
		var accentH = Optional.ofNullable(accent).map(k -> (Holder<TrimMaterial>) materialRegistry.getHolder(k).orElse(null));
		return new DragonArmorTrim(baseH, plateH, accentH);
	}

	public static DragonArmorTrim combine(Optional<DragonArmorTrim> original, DragonArmorTrim toAdd)
	{
		if (original.isPresent())
		{
			DragonArmorTrim o = original.get();
			return new DragonArmorTrim(toAdd.chain.isPresent() ? toAdd.chain : o.chain, toAdd.plating.isPresent() ? toAdd.plating : o.plating, toAdd.inlay.isPresent() ? toAdd.inlay : o.inlay);
		}
		return toAdd;
	}

	public static DragonArmorTrim create(Decoration decoration, Holder<TrimMaterial> material)
	{
		Optional<Holder<TrimMaterial>> m = Optional.ofNullable(material);
		return switch (decoration)
		{
		case CHAIN -> new DragonArmorTrim(m, Optional.empty(), Optional.empty());
		case PLATING -> new DragonArmorTrim(Optional.empty(), m, Optional.empty());
		case INLAY -> new DragonArmorTrim(Optional.empty(), Optional.empty(), m);
		};
	}

	public static DragonArmorTrim createRandom(Registry<TrimMaterial> materialRegistry, RandomSource rand)
	{
		List<ResourceKey<TrimMaterial>> keys = Stream.concat(materialRegistry.registryKeySet().stream(), Stream.of((ResourceKey<TrimMaterial>) null)).toList();
		return create(materialRegistry, Util.getRandom(keys, rand), Util.getRandom(keys, rand), Util.getRandom(keys, rand));
	}

	public DragonArmorTrim remove(Decoration decoration)
	{
		return switch (decoration)
		{
		case CHAIN -> new DragonArmorTrim(Optional.empty(), this.plating, this.inlay);
		case PLATING -> new DragonArmorTrim(this.chain, Optional.empty(), this.inlay);
		case INLAY -> new DragonArmorTrim(this.chain, this.plating, Optional.empty());
		};
	}

	public int getColor(int modelLayer)
	{
		return switch (modelLayer)
		{
		case 0 -> getColor(this.chain, 0x7A86A8);
		case 1 -> getColor(this.plating, 0xA2B0B3);
		default -> getColor(this.inlay, getColor(1));
		};
	}

	private int getColor(Optional<Holder<TrimMaterial>> material, int defaultColor)
	{
		return material.map(m -> m.value().description().getStyle().getColor().getValue()).orElse(defaultColor);
	}

	public Optional<Holder<TrimMaterial>> getMaterial(Decoration decoration)
	{
		return switch (decoration)
		{
		case CHAIN -> chain;
		case PLATING -> plating;
		case INLAY -> inlay;
		};
	}

	public boolean materialMatches(Decoration decoration, Holder<TrimMaterial> material)
	{
		var mat = getMaterial(decoration);
		return mat.isPresent() && mat.get() == material;
	}

	public static boolean setTrim(RegistryAccess registryAccess, ItemStack armor, DragonArmorTrim trim)
	{
		if (armor.is(RediscoveredItems.dragon_armor))
		{
			armor.getOrCreateTag().put("trim", CODEC.encodeStart(RegistryOps.create(NbtOps.INSTANCE, registryAccess), trim).result().orElseThrow());
			return true;
		}
		else
		{
			return false;
		}
	}

	public static Optional<DragonArmorTrim> getTrim(RegistryAccess registryAccess, ItemStack armor)
	{
		if (armor.is(RediscoveredItems.dragon_armor) && armor.getTag() != null && armor.getTag().contains("trim"))
		{
			CompoundTag compoundtag = armor.getTagElement("trim");
			DragonArmorTrim armortrim = CODEC.parse(RegistryOps.create(NbtOps.INSTANCE, registryAccess), compoundtag).resultOrPartial(RediscoveredMod.LOGGER::error).orElse(null);
			return Optional.ofNullable(armortrim);
		}
		else
		{
			return Optional.empty();
		}
	}

	public static void appendHoverText(ItemStack armor, RegistryAccess registryAccess, List<Component> tooltips)
	{
		Optional<DragonArmorTrim> optional = getTrim(registryAccess, armor);
		if (optional.isPresent())
		{
			DragonArmorTrim trim = optional.get();
			addTooltip(Decoration.CHAIN.localeKey(), trim.chain, tooltips);
			addTooltip(Decoration.PLATING.localeKey(), trim.plating, tooltips);
			addTooltip(Decoration.INLAY.localeKey(), trim.inlay, tooltips);
		}
	}

	private static void addTooltip(String key, Optional<Holder<TrimMaterial>> material, List<Component> tooltips)
	{
		if (material.isPresent())
		{
			Component tooltip = Component.translatable("%s: %s", Component.translatable(key), material.get().value().description()).withStyle(ChatFormatting.GRAY);
			tooltips.add(tooltip);
		}
	}

	public static enum Decoration implements StringRepresentable
	{
		// Avoid changing these names. Many things reference them.
		CHAIN("chain"),
		PLATING("plating"),
		INLAY("inlay");

		public static final Codec<Decoration> CODEC = StringRepresentable.fromEnum(Decoration::values);

		final String name;
		final String localeKey;

		Decoration(String name)
		{
			this.name = name;
			this.localeKey = "dragon_armor_trim." + RediscoveredMod.MODID + "." + name;
		}

		@Override
		public String getSerializedName()
		{
			return this.name;
		}

		@Override
		public final String toString()
		{
			return this.name;
		}

		public static Decoration byName(String name)
		{
			for (var d : values())
				if (d.name.equals(name))
					return d;
			return PLATING;
		}

		public String localeKey()
		{
			return localeKey;
		}
	}
}
