package com.legacy.rediscovered.item;

import javax.annotation.Nullable;

import com.legacy.rediscovered.registry.RediscoveredBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class MiniDragonPylonItem extends BlockItem
{

	public MiniDragonPylonItem(Block pBlock, Properties pProperties)
	{
		super(pBlock, pProperties);
	}

	// Places one block above where you click
	@Nullable
	@Override
	public BlockPlaceContext updatePlacementContext(BlockPlaceContext context)
	{
		Direction face = context.getClickedFace();
		BlockPos pos = context.getClickedPos();
		Level level = context.getLevel();
		BlockState above = level.getBlockState(pos.above());
		BlockState below = level.getBlockState(pos.below());
		// Don't place in between an existing pylon and the dragon altar. Basically to prevent players from goofing the respawn on accident.
		if (below.is(RediscoveredBlocks.dragon_altar) && !above.canBeReplaced())
		{
			return null;
		}
		// If the block above is clear, and we're placing on the top of a block, shift one block above
		if (face == Direction.UP && above.canBeReplaced())
		{
			return BlockPlaceContext.at(context, pos.above(), face);
		}
		return context;
	}
}
