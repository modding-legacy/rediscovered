package com.legacy.rediscovered.item.crafting;

import java.util.LinkedHashMap;
import java.util.Map;

import com.legacy.rediscovered.item.util.DragonArmorTrim;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.Ingredient;

public class SmithingDragonArmorRecipeBuilder
{
	private final RecipeCategory category;
	private final Ingredient template;
	private final DragonArmorTrim.Decoration decoration;
	private final Ingredient base;
	private final Ingredient addition;
	private final Map<String, Criterion<?>> criteria = new LinkedHashMap<>();

	public SmithingDragonArmorRecipeBuilder(RecipeCategory pCategory, Ingredient pTemplate, DragonArmorTrim.Decoration decoration, Ingredient pBase, Ingredient pAddition)
	{
		this.category = pCategory;
		this.template = pTemplate;
		this.decoration = decoration;
		this.base = pBase;
		this.addition = pAddition;
	}

	public static SmithingDragonArmorRecipeBuilder smithingTrim(Ingredient pTemplate, DragonArmorTrim.Decoration decoration, Ingredient pBase, Ingredient pAddition, RecipeCategory pCategory)
	{
		return new SmithingDragonArmorRecipeBuilder(pCategory, pTemplate, decoration, pBase, pAddition);
	}

	public SmithingDragonArmorRecipeBuilder unlocks(String key, Criterion<?> criterion)
	{
		this.criteria.put(key, criterion);
		return this;
	}

	public void save(RecipeOutput output, ResourceLocation id)
	{
		this.ensureValid(id);

		//@formatter:off
		Advancement.Builder advancement$builder = output.advancement()
	            .addCriterion("has_the_recipe", RecipeUnlockedTrigger.unlocked(id))
	            .rewards(AdvancementRewards.Builder.recipe(id))
	            .requirements(AdvancementRequirements.Strategy.OR);
		 //@formatter:on
		this.criteria.forEach(advancement$builder::addCriterion);
		SmithingDragonArmorRecipe recipe = new SmithingDragonArmorRecipe(template, decoration, base, addition);
		output.accept(id, recipe, advancement$builder.build(id.withPrefix("recipes/" + this.category.getFolderName() + "/")));
	}

	private void ensureValid(ResourceLocation pLocation)
	{
		if (this.criteria.isEmpty())
		{
			throw new IllegalStateException("No way of obtaining recipe " + pLocation);
		}
	}
}