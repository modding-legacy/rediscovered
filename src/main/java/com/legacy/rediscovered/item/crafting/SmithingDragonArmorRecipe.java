package com.legacy.rediscovered.item.crafting;

import java.util.Optional;
import java.util.stream.Stream;

import com.legacy.rediscovered.item.util.DragonArmorTrim;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredRecipeSerializers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.Holder;
import net.minecraft.core.RegistryAccess;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.item.armortrim.TrimMaterials;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SmithingRecipe;
import net.minecraft.world.level.Level;

public class SmithingDragonArmorRecipe implements SmithingRecipe
{
	final Ingredient template;
	final DragonArmorTrim.Decoration decoration;
	final Ingredient base;
	final Ingredient addition;

	public SmithingDragonArmorRecipe(Ingredient template, DragonArmorTrim.Decoration decoration, Ingredient base, Ingredient addition)
	{
		this.template = template;
		this.decoration = decoration;
		this.base = base;
		this.addition = addition;
	}

	@Override
	public boolean matches(Container container, Level level)
	{
		return this.template.test(container.getItem(0)) && this.base.test(container.getItem(1)) && this.addition.test(container.getItem(2));
	}

	@Override
	public ItemStack assemble(Container container, RegistryAccess registryAccess)
	{
		ItemStack baseItem = container.getItem(1);
		if (this.base.test(baseItem))
		{
			Optional<Holder.Reference<TrimMaterial>> material = TrimMaterials.getFromIngredient(registryAccess, container.getItem(2));
			if (material.isPresent())
			{
				// If the current trim is already applied, don't do anything
				Optional<DragonArmorTrim> currentTrim = DragonArmorTrim.getTrim(registryAccess, baseItem);
				if (currentTrim.isPresent() && currentTrim.get().materialMatches(this.decoration, material.get()))
				{
					return ItemStack.EMPTY;
				}

				ItemStack resultItem = baseItem.copy();
				resultItem.setCount(1);
				DragonArmorTrim trim = DragonArmorTrim.combine(currentTrim, DragonArmorTrim.create(this.decoration, material.get()));
				if (DragonArmorTrim.setTrim(registryAccess, resultItem, trim))
				{
					return resultItem;
				}
			}
		}

		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack getResultItem(RegistryAccess registryAccess)
	{
		return RediscoveredItems.dragon_armor.getDefaultInstance();
	}

	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return RediscoveredRecipeSerializers.DRAGON_ARMOR.get();
	}

	@Override
	public boolean isTemplateIngredient(ItemStack stack)
	{
		return this.template.test(stack);
	}

	@Override
	public boolean isBaseIngredient(ItemStack stack)
	{
		return this.base.test(stack);
	}

	@Override
	public boolean isAdditionIngredient(ItemStack stack)
	{
		return this.addition.test(stack);
	}

	@Override
	public boolean isIncomplete()
	{
		return Stream.of(this.template, this.base, this.addition).anyMatch(net.neoforged.neoforge.common.CommonHooks::hasNoElements);
	}

	public static class Serializer implements RecipeSerializer<SmithingDragonArmorRecipe>
	{
		public static final Codec<SmithingDragonArmorRecipe> CODEC = RecordCodecBuilder.create(instance ->
		{
			//@formatter:off
			return instance.group(
					Ingredient.CODEC.fieldOf("template").forGetter(o -> o.template),
					DragonArmorTrim.Decoration.CODEC.fieldOf("decoration").forGetter(o -> o.decoration),
					Ingredient.CODEC.fieldOf("base").forGetter(o -> o.base),
					Ingredient.CODEC.fieldOf("addition").forGetter(o -> o.addition)).apply(instance, SmithingDragonArmorRecipe::new);
			//@formatter:on
		});
		
		@Override
		public Codec<SmithingDragonArmorRecipe> codec()
		{
			return CODEC;
		}

		@Override
		public SmithingDragonArmorRecipe fromNetwork(FriendlyByteBuf byteBuff)
		{
			Ingredient ingredient = Ingredient.fromNetwork(byteBuff);
			DragonArmorTrim.Decoration decoration = DragonArmorTrim.Decoration.byName(byteBuff.readUtf());
			Ingredient ingredient1 = Ingredient.fromNetwork(byteBuff);
			Ingredient ingredient2 = Ingredient.fromNetwork(byteBuff);
			return new SmithingDragonArmorRecipe(ingredient, decoration, ingredient1, ingredient2);
		}

		@Override
		public void toNetwork(FriendlyByteBuf byteBuff, SmithingDragonArmorRecipe recipe)
		{
			recipe.template.toNetwork(byteBuff);
			byteBuff.writeUtf(recipe.decoration.getSerializedName());
			recipe.base.toNetwork(byteBuff);
			recipe.addition.toNetwork(byteBuff);
		}
	}
}
