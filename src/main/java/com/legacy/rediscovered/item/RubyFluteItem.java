package com.legacy.rediscovered.item;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;

import net.minecraft.ChatFormatting;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ItemUtils;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class RubyFluteItem extends Item
{
	public static final String DRAGON_NOT_LOADED_KEY = RediscoveredMod.MODID + ".message.dragon_not_loaded";
	public static final String DRAGON_STUCK_KEY = RediscoveredMod.MODID + ".message.dragon_stuck";
	public static final String DRAGON_TIRED_KEY = RediscoveredMod.MODID + ".message.dragon_too_tired";

	public RubyFluteItem(Properties properties)
	{
		super(properties);
	}

	@Override
	public int getUseDuration(ItemStack stack)
	{
		return 1200;
	}

	@Override
	public UseAnim getUseAnimation(ItemStack stack)
	{
		return UseAnim.CUSTOM;
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack stack = player.getItemInHand(hand);
		player.playSound(SoundEvents.NOTE_BLOCK_FLUTE.value(), 10.0F, 1.0F);
		player.awardStat(Stats.ITEM_USED.get(this));
		player.getCooldowns().addCooldown(this, 20);

		UUID target = getTargetUUID(stack);
		if (target != null && level instanceof ServerLevel sl)
		{
			Entity summon = sl.getEntity(target);

			if (summon != null)
				trySummon(player, sl.getEntity(target));
			else
				player.displayClientMessage(Component.translatable(DRAGON_NOT_LOADED_KEY, getTargetName(stack)), true);
		}

		return ItemUtils.startUsingInstantly(level, player, hand);
	}

	public static boolean trySummon(Player player, Entity entity)
	{
		if (entity instanceof RedDragonOffspringEntity dragon && dragon.getOwner() == player)
		{
			dragon.summonFromFlute(player);
			return true;
		}
		return false;
	}

	@Override
	public InteractionResult interactLivingEntity(ItemStack stack, Player player, LivingEntity target, InteractionHand usedHand)
	{
		if (target instanceof RedDragonOffspringEntity dragon && dragon.getOwner() == player)
		{
			setTarget(stack, target);
			player.setItemInHand(usedHand, stack);
			return InteractionResult.SUCCESS;
		}

		return InteractionResult.PASS;
	}

	@Override
	public boolean isFoil(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		return tag != null && tag.contains(TARGET_KEY);
	}

	@Override
	public boolean canGrindstoneRepair(ItemStack stack)
	{
		// Allows removing the Target Entity tag; handled in an event
		return true;
	}

	@Override
	public void appendHoverText(ItemStack stack, Level level, List<Component> tooltips, TooltipFlag isAdvanced)
	{
		Component targetName = getTargetName(stack);
		if (targetName != null)
		{
			tooltips.add(targetName.copy().withStyle(ChatFormatting.GRAY));
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void initializeClient(Consumer<net.neoforged.neoforge.client.extensions.common.IClientItemExtensions> consumer)
	{
		consumer.accept(new com.legacy.rediscovered.client.item.RubyFluteClient());
	}

	private static final String TARGET_KEY = "target", TARGET_NAME_KEY = "target_name";

	public static ItemStack setTarget(ItemStack stack, @Nullable LivingEntity target)
	{
		if (target != null)
		{
			setTargetName(stack, target.getDisplayName());
			setTargetUUID(stack, target.getUUID());
		}
		else
		{
			setTargetName(stack, null);
			setTargetUUID(stack, null);
		}
		return stack;
	}

	@Nullable
	public static UUID getTargetUUID(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.hasUUID(TARGET_KEY))
			return tag.getUUID(TARGET_KEY);
		return null;
	}

	public static ItemStack setTargetUUID(ItemStack stack, @Nullable UUID uuid)
	{
		if (uuid == null)
		{
			CompoundTag tag = stack.getTag();
			if (tag != null)
				tag.remove(TARGET_KEY);
		}
		else
		{
			CompoundTag tag = stack.getOrCreateTag();
			tag.putUUID(TARGET_KEY, uuid);
		}
		return stack;
	}

	@Nullable
	public static Component getTargetName(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.contains(TARGET_NAME_KEY, Tag.TAG_STRING))
		{
			String s = tag.getString(TARGET_NAME_KEY);
			Component name = Component.Serializer.fromJson(s);
			return name == null ? Component.literal(s) : name;
		}
		return null;
	}

	public static ItemStack setTargetName(ItemStack stack, @Nullable Component name)
	{
		if (name == null)
		{
			CompoundTag tag = stack.getTag();
			if (tag != null)
				tag.remove(TARGET_NAME_KEY);
		}
		else
		{
			CompoundTag tag = stack.getOrCreateTag();
			// Puts the name component in the tag, stripping hover events, click events, etc
			tag.putString(TARGET_NAME_KEY, Component.Serializer.toJson(name.copy().setStyle(name.getStyle().withHoverEvent(null).withClickEvent(null).withInsertion(null))));
		}
		return stack;
	}
}
