package com.legacy.rediscovered.item;

import com.legacy.rediscovered.entity.PurpleArrowEntity;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ArrowItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class PurpleArrowItem extends ArrowItem
{
	public PurpleArrowItem(Item.Properties builder)
	{
		super(builder);
	}

	@Override
	public AbstractArrow createArrow(Level level, ItemStack stack, LivingEntity shooter)
	{
		return new PurpleArrowEntity(level, shooter, stack);
	}

	@Override
	public boolean isInfinite(ItemStack stack, ItemStack bow, Player player)
	{
		return false;
	}
}