package com.legacy.rediscovered.block_entities;

import java.util.List;

import com.legacy.rediscovered.api.INetherReactorGrowable;
import com.legacy.rediscovered.block.NetherReactorBlock;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.particles.NetherReactorPulseData;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.legacy.rediscovered.registry.RediscoveredTriggers;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class NetherReactorBlockEntity extends ModBlockEntity
{
	//@formatter:off
	private static final MultiBlockPattern PATTERN = MultiBlockPattern.builder()
			.pattern()
				.validator('P', RediscoveredTags.Blocks.NETHER_REACTOR_POWER_BLOCK)
				.validator('B', RediscoveredTags.Blocks.NETHER_REACTOR_BASE_BLOCK)
				// Gold blocks. Check these first.
				.state(1, -1, 1, 'P')
				.state(1, -1, -1, 'P')
				.state(-1, -1, 1, 'P')
				.state(-1, -1, -1, 'P')
				// Bottom layer stone cross
				.state(0, -1, 0, 'B')
				.state(1, -1, 0, 'B')
				.state(-1, -1, 0, 'B')
				.state(0, -1, 1, 'B')
				.state(0, -1, -1, 'B')
				// Middle layer stone
				.state(1, 0, 1, 'B')
				.state(1, 0, -1, 'B')
				.state(-1, 0, 1, 'B')
				.state(-1, 0, -1, 'B')
				// Top layer stone cross
				.state(0, 1, 0, 'B')
				.state(1, 1, 0, 'B')
				.state(-1, 1, 0, 'B')
				.state(0, 1, 1, 'B')
				.state(0, 1, -1, 'B')
				.end()
			.build();
	private static final MultiBlockPattern STRONG_POWER_PATTERN = MultiBlockPattern.builder()
			.pattern()
				.validator('P', RediscoveredTags.Blocks.NETHER_REACTOR_STRONG_POWER_BLOCK)
				// Netherite blocks.
				.state(1, -1, 1, 'P')
				.state(1, -1, -1, 'P')
				.state(-1, -1, 1, 'P')
				.state(-1, -1, -1, 'P')
			.build();
	//@formatter:on
	public static final byte TICK_RATE = 40;
	private byte tick = 0;
	private int audioTick = 0;
	private byte obsidianConversion = 0;
	private byte growthSpeed = 2;

	public NetherReactorBlockEntity(BlockPos pos, BlockState state)
	{
		super(RediscoveredBlockEntityTypes.NETHER_REACTOR, pos, state);
	}

	private static final String TICK_KEY = "tick", OBSIDIAN_CONVERSION_KEY = "obsidian_conversion",
			GROWTH_SPEED_KEY = "growth_speed";

	@Override
	public void load(CompoundTag tag)
	{
		super.load(tag);
		this.tick = tag.getByte(TICK_KEY);
		this.obsidianConversion = tag.getByte(OBSIDIAN_CONVERSION_KEY);
		if (tag.contains(GROWTH_SPEED_KEY)) // defaults to 1
			this.growthSpeed = tag.getByte(GROWTH_SPEED_KEY);
	}

	@Override
	protected void saveAdditional(CompoundTag tag)
	{
		super.saveAdditional(tag);
		tag.putByte(TICK_KEY, this.tick);
		tag.putByte(OBSIDIAN_CONVERSION_KEY, this.obsidianConversion);
		tag.putByte(GROWTH_SPEED_KEY, this.growthSpeed);
	}

	public void onPlace()
	{
		this.tick = TICK_RATE - 10;
		this.setChanged();
	}

	@Override
	public void setRemoved()
	{
		this.level.playSound((Player) null, this.worldPosition, RediscoveredSounds.BLOCK_NETHER_REACTOR_DEACTIVATE, SoundSource.BLOCKS, 1.0F, 1.0F);
		super.setRemoved();
	}

	public static void serverTick(Level level, BlockPos pos, BlockState state, NetherReactorBlockEntity netherReactor)
	{
		if (!(level instanceof ServerLevel serverLevel))
			return;
		boolean active = state.getValue(NetherReactorBlock.ACTIVE);
		if (netherReactor.tick % TICK_RATE == 0)
		{
			boolean patternValid = PATTERN.isValid(level, pos, state);
			boolean isFullPower = patternValid && STRONG_POWER_PATTERN.isValid(serverLevel, pos, state);

			if (patternValid != state.getValue(NetherReactorBlock.ACTIVE))
			{
				active = patternValid;
				level.playSound(null, pos, patternValid ? RediscoveredSounds.BLOCK_NETHER_REACTOR_ACTIVATE : RediscoveredSounds.BLOCK_NETHER_REACTOR_DEACTIVATE, SoundSource.BLOCKS, 3.5F, 1.0F);
				level.setBlock(pos, state.setValue(NetherReactorBlock.ACTIVE, patternValid), Block.UPDATE_ALL);
				// On activate
				if (patternValid)
				{
					Vec3 center = pos.getCenter();
					for (ServerPlayer player : serverLevel.players())
						serverLevel.sendParticles(player, new NetherReactorPulseData(0, 50, 35.0F), true, center.x(), center.y(), center.z(), 1, 0, 0, 0, 0);

					for (ServerPlayer player : serverLevel.getEntitiesOfClass(ServerPlayer.class, AABB.unitCubeFromLowerCorner(center).inflate(10.0D, 5.0D, 10.0D)))
						RediscoveredTriggers.ACTIVATE_NETHER_REACTOR.get().trigger(player, isFullPower);
				}
				else
				{
					netherReactor.tick = 0;
				}
			}

			if (patternValid)
			{
				// Entity effects
				final int radius = 45;
				int x = pos.getX();
				int y = pos.getY();
				int z = pos.getZ();
				List<LivingEntity> entities = level.getEntitiesOfClass(LivingEntity.class, new AABB(x, y, z, x + 1, y + 1, z + 1).inflate(radius));
				if (!entities.isEmpty())
				{
					for (LivingEntity entity : entities)
					{
						if (pos.closerThan(entity.blockPosition(), radius))
						{
							entity.addEffect(new MobEffectInstance(RediscoveredEffects.CRIMSON_VEIL.get(), 100, isFullPower ? 1 : 0, true, true));
						}
					}
				}

				// Obsidian conversion
				for (int dx = -1; dx <= 1; dx++)
				{
					for (int dz = -1; dz <= 1; dz++)
					{
						BlockPos offsetPos = pos.offset(dx, netherReactor.obsidianConversion - 1, dz);
						BlockState offsetState = level.getBlockState(offsetPos);
						if (!offsetState.is(RediscoveredBlocks.glowing_obsidian) && offsetState.is(RediscoveredTags.Blocks.NETHER_REACTOR_BASE_BLOCK))
						{
							level.destroyBlock(offsetPos, false);
							level.setBlock(offsetPos, RediscoveredBlocks.glowing_obsidian.defaultBlockState(), Block.UPDATE_ALL);
							Vec3 p = offsetPos.getCenter();
							double d = 0.4;
							serverLevel.sendParticles(RediscoveredParticles.NETHER_REACTOR_GROWTH, p.x, p.y, p.z, 20, d, d, d, 0.001);
						}
					}
				}
				netherReactor.obsidianConversion++;
				if (netherReactor.obsidianConversion > 2)
					netherReactor.obsidianConversion = 0;
			}
			else
			{
				if (netherReactor.obsidianConversion != 0)
				{
					netherReactor.obsidianConversion = 0;
				}
			}
		}

		// Random local growth
		RandomSource rand = serverLevel.getRandom();
		final int growthRadius = 16;
		final int horizontalRadius = growthRadius / 2;
		int growthSpeed = netherReactor.growthSpeed;
		//growthSpeed = 100; // debug number
		for (int i = 0; i < growthSpeed; i++)
		{
			BlockPos growthPos = pos.offset(rand.nextIntBetweenInclusive(-growthRadius, growthRadius), rand.nextIntBetweenInclusive(-horizontalRadius, horizontalRadius), rand.nextIntBetweenInclusive(-growthRadius, growthRadius));
			BlockState growthTarget = serverLevel.getBlockState(growthPos);
			INetherReactorGrowable nrg = INetherReactorGrowable.get(growthTarget);
			nrg.onNetherReactorGrow(growthTarget, serverLevel, growthPos, rand, netherReactor).ifPresent(p -> nrg.displayNetherReactorGrowth(growthTarget, serverLevel, p, rand, netherReactor));
		}

		// Visual/Audio flare!
		if (netherReactor.audioTick % (5.1 * 20) == 0 && active)
		{
			level.playSound(null, pos, RediscoveredSounds.BLOCK_NETHER_REACTOR_IDLE, SoundSource.BLOCKS, 6.0F, 1.0F);
			netherReactor.audioTick = 0;
		}
		if ((netherReactor.audioTick - 5) % (5.1 * 10) == 0 && active)
		{
			Vec3 center = pos.getCenter();
			for (ServerPlayer player : serverLevel.players())
				serverLevel.sendParticles(player, new NetherReactorPulseData(2, 20, 5.0F), true, center.x(), center.y(), center.z(), 1, 0, 0, 0, 0);
		}

		if (active)
			netherReactor.audioTick++;
		netherReactor.tick++;
		if (netherReactor.tick >= TICK_RATE)
			netherReactor.tick = 0;
		netherReactor.setChanged();
	}

	public static void clientTick(Level level, BlockPos pos, BlockState state, NetherReactorBlockEntity netherReactor)
	{
		if (state.getValue(NetherReactorBlock.ACTIVE))
		{
			RandomSource rand = level.getRandom();
			Vec3 center = pos.getCenter();
			double x = center.x;
			double y = center.y;
			double z = center.z;
			int range = 10;
			for (int i = rand.nextInt(3); i > 0; i--)
			{
				level.addParticle(ParticleTypes.CRIMSON_SPORE, x + rand.nextInt(range) - rand.nextInt(range), y + rand.nextInt(range) - rand.nextInt(range), z + rand.nextInt(range) - rand.nextInt(range), 0, 0, 0);
			}

			double d = 1.4;
			double dy = 0.5;
			for (int i = 5; i > 0; i--)
				level.addParticle(RediscoveredParticles.NETHER_REACTOR_GROWTH, x + ((rand.nextFloat() - 0.5) * d), y + ((rand.nextFloat() - 0.5) * dy), z + ((rand.nextFloat() - 0.5) * d), 0.0001, 0.0001, 0.0001);
		}
	}
}
