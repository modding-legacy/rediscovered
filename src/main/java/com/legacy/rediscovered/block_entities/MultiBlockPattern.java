package com.legacy.rediscovered.block_entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import it.unimi.dsi.fastutil.chars.Char2ObjectOpenHashMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class MultiBlockPattern
{
	private final int size;
	private final Char2ObjectOpenHashMap<BiPredicate<BlockState, BlockState>>[] validators;
	private final StateTest[][] stateTests;

	@SuppressWarnings("unchecked")
	private MultiBlockPattern(MultiBlockPatternBuilder builder)
	{
		this.size = builder.patterns.size();
		this.validators = new Char2ObjectOpenHashMap[this.size];
		this.stateTests = new StateTest[this.size][];
		for (int i = 0; i < this.size; i++)
		{
			var pattern = builder.patterns.get(i);
			this.stateTests[i] = pattern.stateTests.toArray(StateTest[]::new);
			this.validators[i] = new Char2ObjectOpenHashMap<>(pattern.validators);
			this.validators[i].defaultReturnValue((self, test) -> false);
		}
	}

	public static MultiBlockPatternBuilder builder()
	{
		return new MultiBlockPatternBuilder();
	}

	public boolean isValid(LevelReader level, BlockPos origin, BlockState self)
	{
		boolean anyPassed = false;
		options : for (int i = 0; i < this.size && !anyPassed; i++)
		{
			var vals = this.validators[i];
			for (StateTest s : this.stateTests[i])
				if (!vals.get(s.validatorKey).test(self, level.getBlockState(origin.offset(s.x, s.y, s.z))))
					continue options; // As soon as this fails, skip to the next shape option
			anyPassed = true; // This should only run if all tests passed
		}
		return anyPassed;
	}

	private static record StateTest(byte x, byte y, byte z, char validatorKey)
	{
	}

	public static class MultiBlockPatternBuilder
	{
		private final List<PatternBuilder> patterns = new ArrayList<>(1);

		private MultiBlockPatternBuilder()
		{
		}

		public PatternBuilder pattern()
		{
			var pattern = new PatternBuilder(this);
			this.patterns.add(pattern);
			return pattern;
		}

		public MultiBlockPattern build()
		{
			if (this.patterns.isEmpty())
				throw new IllegalStateException("Cannot create MultiBlockPattern because it has no patterns");
			for (PatternBuilder pattern : this.patterns)
				for (StateTest s : pattern.stateTests)
					if (!pattern.validators.containsKey(s.validatorKey))
						throw new IllegalStateException("Cannot create MultiBlockPattern because validator '" + s.validatorKey + "' has not been defined.");
			return new MultiBlockPattern(this);
		}
	}

	public static class PatternBuilder
	{
		private final MultiBlockPatternBuilder parent;
		private Map<Character, BiPredicate<BlockState, BlockState>> validators = new HashMap<>();
		private List<StateTest> stateTests = new ArrayList<>();

		private PatternBuilder(MultiBlockPatternBuilder parent)
		{
			this.parent = parent;
		}

		public PatternBuilder validator(char key, TagKey<Block> tag)
		{
			return validator(key, (self, test) -> test.is(tag));
		}

		public PatternBuilder validator(char key, Predicate<BlockState> validator)
		{
			return validator(key, (self, test) -> validator.test(test));
		}

		/**
		 * @param key
		 * @param validator
		 *            The first state is the block at the origin, who is doing the
		 *            checking. The second is the block to check.
		 * @return
		 */
		public PatternBuilder validator(char key, BiPredicate<BlockState, BlockState> validator)
		{
			this.validators.put(key, validator);
			return this;
		}

		public PatternBuilder state(Vec3i offset, char validatorKey)
		{
			return state(offset.getX(), offset.getY(), offset.getZ(), validatorKey);
		}

		public PatternBuilder state(int x, int y, int z, char validatorKey)
		{
			this.stateTests.add(new StateTest((byte) x, (byte) y, (byte) z, validatorKey));
			return this;
		}

		public PatternBuilder or()
		{
			return this.parent.pattern();
		}

		public MultiBlockPatternBuilder end()
		{
			return this.parent;
		}
		
		public MultiBlockPattern build()
		{
			return this.end().build();
		}
	}
}
