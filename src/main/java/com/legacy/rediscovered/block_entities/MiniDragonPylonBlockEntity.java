package com.legacy.rediscovered.block_entities;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.block.DragonAltarBlock;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.particles.PylonShieldBlastData;
import com.legacy.rediscovered.entity.dragon.DragonPylonEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonBossEntity;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredPoiTypes;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.ai.village.poi.PoiManager.Occupancy;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class MiniDragonPylonBlockEntity extends ModBlockEntity
{
	public UUID mainPylonUUID = null;
	private Optional<BlockPos> beamPos = Optional.empty();
	private int respawnTimer = 0;
	private Optional<BlockPos> respawnMasterPos = Optional.empty();

	// Client data
	public int ticks;

	public MiniDragonPylonBlockEntity(BlockPos pos, BlockState state)
	{
		super(RediscoveredBlockEntityTypes.MIN_DRAGON_PYLON, pos, state);
	}

	private static final String PYLON_UUID = "pylon_uuid", BEAM_POS = "beam_pos", RESPAWN_TIMER = "respawn_timer",
			RESPAWN_MASTER = "respawn_master";

	@Override
	protected void saveAdditional(CompoundTag tag)
	{
		super.saveAdditional(tag);
		if (this.mainPylonUUID != null)
			tag.putUUID(PYLON_UUID, this.mainPylonUUID);
		if (this.beamPos.isPresent())
			tag.putLong(BEAM_POS, this.beamPos.get().asLong());
		if (this.respawnMasterPos.isPresent())
			tag.putLong(RESPAWN_MASTER, this.respawnMasterPos.get().asLong());
		tag.putInt(RESPAWN_TIMER, this.respawnTimer);
	}

	@Override
	public void load(CompoundTag tag)
	{
		super.load(tag);
		this.mainPylonUUID = tag.hasUUID(PYLON_UUID) ? tag.getUUID(PYLON_UUID) : null;
		this.beamPos = tag.contains(BEAM_POS, Tag.TAG_LONG) ? Optional.of(BlockPos.of(tag.getLong(BEAM_POS))) : Optional.empty();
		this.respawnMasterPos = tag.contains(RESPAWN_MASTER, Tag.TAG_LONG) ? Optional.of(BlockPos.of(tag.getLong(RESPAWN_MASTER))) : Optional.empty();
		this.respawnTimer = tag.getInt(RESPAWN_TIMER);
	}

	public static void serverTick(Level level, BlockPos pos, BlockState state, MiniDragonPylonBlockEntity pylon)
	{
		if (level instanceof ServerLevel)
		{
			ServerLevel sl = (ServerLevel) level;

			// Once every second, or every tick if respawning is in progress, check for pylons around to start the respawn sequence
			if (level.getGameTime() % 20 == 0 || pylon.respawnTimer > 0)
			{
				int groundOffset = 2;
				BlockPos ground = pos.below(groundOffset);
				// Only handle respawn logic if the block below is anchored obsidian and there are no original pylons nearby
				if (isRespawnBlock(level.getBlockState(ground)) && level.getEntitiesOfClass(DragonPylonEntity.class, new AABB(pos).inflate(4), e -> true).isEmpty())
				{
					if (pylon.respawnTimer >= pylon.timeUntilRespawn())
					{
						level.destroyBlock(pos, false);
						if (pylon.isRespawnMaster())
						{
							BlockPos actionPos = pylon.beamPos.orElse(pylon.worldPosition);
							BlockPos spawnPos = actionPos.above(pylon.getRespawnHeight());
							RedDragonBossEntity dragon = RediscoveredEntityTypes.RED_DRAGON_BOSS.create(level);
							dragon.setPos(spawnPos.getCenter());
							dragon.homePos = pylon.beamPos.isPresent() ? pylon.beamPos.get().below(4) : pylon.worldPosition;
							level.addFreshEntity(dragon);

							pylon.spawnBlastParticle(sl, spawnPos);

							level.playSound(null, spawnPos, RediscoveredSounds.ENTITY_RED_DRAGON_SHIELD_DOWN, SoundSource.HOSTILE, 7.0F, 1.0F);

							int advancementRadius = 32;
							List<ServerPlayer> players = sl.getPlayers(player -> player.position().distanceToSqr(actionPos.getCenter()) < advancementRadius * advancementRadius);
							for (var player : players)
								CriteriaTriggers.SUMMONED_ENTITY.trigger(player, dragon);
						}

						// The rest of this logic can be skipped since we're deleting the block anyway
						return;
					}

					// Get all nearby pylons with anchored glowing obsidian below them
					List<BlockPos> pylons = findNearbyPylons(sl, pos);

					int requiredForRespawn = 4;
					if (pylons.size() >= requiredForRespawn)
					{
						pylon.respawnTimer++;
						pylon.markUpdated();

						// No master pylon has been set, or the master has been destroyed. 
						// One from the list will become the master, and the rest will be children.
						if (pylon.respawnMasterPos.isEmpty() || pylon.getRespawnMaster() == null)
						{
							// Figure out if a respawn is currently in progress nearby. Don't continue the logic for respawning if there is
							boolean respawnInProgress = pylons.stream().map(pylonPos -> level.getBlockEntity(pylonPos, RediscoveredBlockEntityTypes.MIN_DRAGON_PYLON)).filter(Optional::isPresent).map(Optional::get).anyMatch(p -> p.getRespawnMaster() != null);
							if (!respawnInProgress)
							{
								BlockPos master = pylons.get(0);
								for (int i = 0; i < requiredForRespawn; i++)
								{
									BlockPos child = pylons.get(i);
									level.getBlockEntity(child, RediscoveredBlockEntityTypes.MIN_DRAGON_PYLON).ifPresent(p ->
									{
										p.setRespawnMasterPos(master);
										if (p.beamPos.isEmpty())
										{
											// Locates the center position based on the positions of the anchored obsidian. If you break some of the obsidian, this could be offset incorrectly
											p.computeBeamPos(groundOffset);
										}
									});
								}
							}
						}
					}

					// Not enough pylons exist, or they don't have a master. Clear their respawn data.
					if (pylons.size() < requiredForRespawn || pylon.getRespawnMaster() == null)
					{
						pylon.stopRespawnSequence();
					}
				}
			}
		}
	}

	public static List<BlockPos> findNearbyPylons(ServerLevel level, BlockPos pos)
	{
		return level.getPoiManager().getInRange(h ->
		{
			return h.is(RediscoveredPoiTypes.MINI_DRAGON_PYLON.getKey());
		}, pos, 5, Occupancy.ANY).filter(poi ->
		{
			return isRespawnBlock(level.getBlockState(poi.getPos().below(2)));
		}).map(poi ->
		{
			return poi.getPos();
		}).toList();
	}

	private static boolean isRespawnBlock(BlockState state)
	{
		return state.is(RediscoveredBlocks.dragon_altar);
	}

	public static void clientTick(Level level, BlockPos pos, BlockState state, MiniDragonPylonBlockEntity pylon)
	{
		++pylon.ticks;
	}

	@Nullable
	public BlockPos getBeamPos()
	{
		return this.beamPos.orElse(null);
	}

	public void setBeamPos(@Nullable BlockPos pos)
	{
		this.beamPos = Optional.ofNullable(pos);
		this.markUpdated();
	}

	private void computeBeamPos(int groundOffset)
	{
		// Find the center position of anchored glowing obsidian structure. If some are broken, there really isn't a way to _know_ the center, but it'll be close.
		BlockPos pos = this.getBlockPos();
		int minX = pos.getX();
		int minZ = pos.getZ();
		int maxX = pos.getX();
		int maxZ = pos.getZ();
		int verticalOffset = 3;
		int r = 2;
		for (int dx = -r; dx <= r; dx++)
		{
			for (int dz = -r; dz <= r; dz++)
			{
				BlockPos checkPos = pos.offset(dx, -groundOffset, dz);
				BlockState state = this.level.getBlockState(checkPos);
				if (isRespawnBlock(state))
				{
					// If we find the center, skip the rest and just use it.
					if (state.getValue(DragonAltarBlock.SHAPE) == DragonAltarBlock.Shape.CENTER)
					{
						this.setBeamPos(new BlockPos(checkPos.getX(), pos.getY() + verticalOffset, checkPos.getZ()));
						return;
					}
					minX = Math.min(minX, checkPos.getX());
					minZ = Math.min(minZ, checkPos.getZ());
					maxX = Math.max(maxX, checkPos.getX());
					maxZ = Math.max(maxZ, checkPos.getZ());
				}
			}
		}
		this.setBeamPos(new BlockPos((minX + maxX) / 2, pos.getY() + verticalOffset, (minZ + maxZ) / 2));
	}

	public int getRespawnHeight()
	{
		return 45;
	}

	public int timeUntilRespawn()
	{
		return 100;
	}

	public float getRespawnProgress(float partialTick)
	{
		return Mth.clamp((this.respawnTimer + partialTick) / (float) this.timeUntilRespawn(), 0, 1);
	}

	public void stopRespawnSequence()
	{
		this.respawnTimer = 0;
		this.markUpdated();
		if (this.beamPos.isPresent())
			this.setBeamPos(null);
		if (this.respawnMasterPos.isPresent())
			this.setRespawnMasterPos(null);
	}

	@Nullable
	public BlockPos getRespawnMasterPos()
	{
		return this.respawnMasterPos.orElse(null);
	}

	public void setRespawnMasterPos(@Nullable BlockPos pos)
	{
		this.respawnMasterPos = Optional.ofNullable(pos);
		this.markUpdated();
	}

	public boolean isRespawnMaster()
	{
		return this.respawnMasterPos.isPresent() && this.respawnMasterPos.get().equals(this.getBlockPos());
	}

	@Nullable
	public BlockState getRespawnMaster()
	{
		return this.respawnMasterPos.map(pos -> this.level.getBlockState(pos)).orElse(null);
	}

	public void onPylonDestroyed(Level level, BlockPos pos)
	{
		if (level instanceof ServerLevel sl)
		{
			this.spawnBlastParticle(sl, pos);

			if (this.respawnTimer > 0)
			{
				for (BlockPos pylonPos : findNearbyPylons(sl, pos))
				{
					if (!pylonPos.equals(pos))
					{
						level.getBlockEntity(pylonPos, RediscoveredBlockEntityTypes.MIN_DRAGON_PYLON).ifPresent(pylon ->
						{
							if (pylon.respawnTimer < pylon.timeUntilRespawn())
								pylon.stopRespawnSequence();
						});
					}
				}
			}
		}
	}

	private void spawnBlastParticle(ServerLevel level, BlockPos pos)
	{
		level.getEntitiesOfClass(DragonPylonEntity.class, new AABB(pos).inflate(80)).forEach(pylon -> pylon.forceLayerUpdate = true);
		Vec3 position = pos.getCenter();
		for (ServerPlayer player : level.players())
			level.sendParticles(player, new PylonShieldBlastData(), true, position.x, position.y, position.z, 1, 0.0, 0.0, 0.0, 0.0);
	}
}
