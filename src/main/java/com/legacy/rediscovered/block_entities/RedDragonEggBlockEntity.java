package com.legacy.rediscovered.block_entities;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.RedDragonEggBlock;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.Nameable;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LevelEvent;
import net.minecraft.world.level.block.state.BlockState;

public class RedDragonEggBlockEntity extends ModBlockEntity implements Nameable
{
	private Optional<UUID> owner = Optional.empty();
	// Custom name to put on the hatched dragon
	private Optional<Component> name = Optional.empty();
	// Sets if the egg is hatching
	private boolean canHatch = false;
	// Used to track hatch progress
	private int age = 0;
	// Determines how long it should shake
	private boolean startShaking = false;

	// -------- CLIENT ONLY -------
	// Purely used for animation since age doesn't get synced to client super
	// smoothly. This number just always goes up once a tick.
	private int clientShakeTime = 0;
	public int clientXShakeDir = 1, clientZShakeDir = 1, clientYShakeDir = 1;

	public RedDragonEggBlockEntity(BlockPos pPos, BlockState pBlockState)
	{
		super(RediscoveredBlockEntityTypes.RED_DRAGON_EGG, pPos, pBlockState);
	}

	public static final String OWNER_KEY = "owner", NAME_KEY = "name", AGE_KEY = "age", CAN_HATCH_KEY = "can_hatch",
			START_SHAKE_KEY = "start_shake";

	@Override
	protected void saveAdditional(CompoundTag tag)
	{
		super.saveAdditional(tag);
		if (this.owner.isPresent())
			tag.putUUID(OWNER_KEY, this.owner.get());
		if (this.name.isPresent())
			tag.putString(NAME_KEY, Component.Serializer.toJson(this.name.get()));
		if (this.age != 0)
			tag.putInt(AGE_KEY, this.age);
		if (this.canHatch)
			tag.putBoolean(CAN_HATCH_KEY, this.canHatch);
		tag.putBoolean(START_SHAKE_KEY, this.startShaking);
	}

	@Override
	public void load(CompoundTag tag)
	{
		super.load(tag);
		this.owner = tag.hasUUID(OWNER_KEY) ? Optional.of(tag.getUUID(OWNER_KEY)) : Optional.empty();
		this.name = tag.contains(NAME_KEY, Tag.TAG_STRING) ? Optional.of(Component.Serializer.fromJson(tag.getString(NAME_KEY))) : Optional.empty();
		this.age = tag.getInt(AGE_KEY);
		this.canHatch = tag.getBoolean(CAN_HATCH_KEY); // false by default
		this.startShaking = tag.getBoolean(START_SHAKE_KEY);
	}

	public static void serverTick(Level level, BlockPos pos, BlockState state, RedDragonEggBlockEntity egg)
	{
		if (egg.canHatch())
		{
			egg.age++;
			if (egg.age >= egg.getMaxAge())
			{
				if (egg.hatchDragon(level, state, pos, false))
					return;
			}

			float hatchProgress = egg.getHatchProgress();

			if (egg.startShaking)
			{
				egg.startShaking = false;
			}
			else if (level.random.nextFloat() < 0.1F * (hatchProgress * hatchProgress * hatchProgress) || egg.age >= egg.getMaxAge() - egg.maxShakeTime())
			{
				egg.startShaking = true;
			}

			// Since age is always going up, always mark updated
			egg.markUpdated();
		}
	}

	public static void clientTick(Level level, BlockPos pos, BlockState state, RedDragonEggBlockEntity egg)
	{
		if (egg.clientShakeTime < egg.maxShakeTime())
		{
			if (egg.clientShakeTime == egg.maxShakeTime() - 4)
			{
				float hatchProgress = egg.getHatchProgress();
				for (Player player : level.players())
					level.playSound(player, pos, RediscoveredSounds.BLOCK_RED_DRAGON_EGG_SHAKE, SoundSource.BLOCKS, 0.08F * hatchProgress, level.random.nextFloat() * 0.2F + 0.9F);
			}
			egg.clientShakeTime++;
		}

		// If the egg should start shaking, and has finished shaking already, set the
		// timer back to 0
		if (egg.startShaking && egg.clientShakeTime >= egg.maxShakeTime())
		{
			egg.clientShakeTime = 0;
			egg.clientXShakeDir = level.random.nextBoolean() ? 1 : -1;
			egg.clientZShakeDir = level.random.nextBoolean() ? 1 : -1;
			egg.clientYShakeDir = level.random.nextBoolean() ? 1 : -1;

		}
	}

	/**
	 * @param owner The player who should own this egg. If already set, this does
	 *            nothing.
	 */
	public boolean startHatching(Player owner)
	{
		// Egg is already started, don't do this again
		if (this.canHatch)
		{
			// Set owner if not already set. This is to fix a possible situation where the
			// owner was stripped from the egg, allowing a new owner to be set.
			// This normally wouldn't happen.
			if (this.owner.isEmpty())
			{
				this.owner = Optional.of(owner.getUUID());
				this.markUpdated();
				return true;
			}
			return false;
		}

		this.canHatch = true;
		if (this.owner.isEmpty())
			this.owner = Optional.of(owner.getUUID());
		this.markUpdated();
		return true;
	}

	/**
	 * Hatches the dragon egg. Does not hatch if the owner is offline due to
	 * dragon.tame(owner) requiring the player for taming to occur.
	 * 
	 * If forceHatch is true, the egg will hatch anyway. This is done in creative
	 * mode and taming fields are set manually, so no advancement triggers happen.
	 */
	public boolean hatchDragon(Level level, BlockState state, BlockPos pos, boolean forceHatch)
	{
		@Nullable
		UUID ownerUUID = this.getOwner();
		@Nullable
		Player owner = ownerUUID == null ? null : this.level.getPlayerByUUID(ownerUUID);

		if (!forceHatch) // This is default behavior, but creative mode players force a hatch
		{
			// Don't hatch the egg if the owner is not online (null) or if the egg isn't
			// able to hatch. If hatching is forced, ignore this
			if (!this.canHatch() || owner == null)
				return false;
		}

		RedDragonOffspringEntity dragon = RediscoveredEntityTypes.RED_DRAGON_OFFSPRING.create(level);
		dragon.setPos(pos.getX() + 0.5F, pos.getY(), pos.getZ() + 0.5F);
		dragon.setAge(-24000);
		if (this.name.isPresent())
			dragon.setCustomName(this.name.get());
		if (!forceHatch && owner != null)
		{
			// System.out.println("natural hatch");
			dragon.tame(owner);
			if (owner instanceof ServerPlayer p)
				CriteriaTriggers.SUMMONED_ENTITY.trigger(p, dragon);
		}
		else if (forceHatch && ownerUUID != null) // Egg was forced to hatch. Set fields manually
		{
			// System.out.println("forced hatch");
			dragon.setTame(true);
			dragon.setOwnerUUID(ownerUUID);
		}
		else
		{
			RediscoveredMod.LOGGER.info("Did not hatch Red Dragon at " + pos + " because the owner was not set.");
			return false;
		}

		level.setBlock(pos, Blocks.AIR.defaultBlockState(), Block.UPDATE_ALL);
		this.level.levelEvent(LevelEvent.PARTICLES_DESTROY_BLOCK, pos, Block.getId(state));
		level.playSound(null, pos, RediscoveredSounds.BLOCK_RED_DRAGON_EGG_HATCH, SoundSource.BLOCKS, 1.0F, 0.8F);
		
		// --- Uncomment for a Ready-To-Ride (trademark pending) Red Dragon
		//dragon.setAge(0);
		//dragon.equipSaddle(SoundSource.PLAYERS);
		
		level.addFreshEntity(dragon);
		return true;
	}

	public void setPlacedBy(ItemStack stack, LivingEntity placer)
	{
		if (stack.hasCustomHoverName())
			this.setCustomName(stack.getHoverName());

		if (!RediscoveredConfig.WORLD.hatchableRedDragon() && placer instanceof Player p)
			p.displayClientMessage(Component.translatable(RedDragonEggBlock.HATCH_DISABLED_KEY), true);
	}

	@Nullable
	public UUID getOwner()
	{
		return this.owner.isPresent() ? this.owner.get() : null;
	}

	public void setOwner(@Nullable UUID owner)
	{
		this.owner = Optional.ofNullable(owner);
	}

	// From Nameable interface, used for loot table
	@Override
	public Component getName()
	{
		return this.name.orElseGet(() -> this.getBlockState().getBlock().getName());
	}

	// From Nameable interface, used for loot table
	@Override
	public boolean hasCustomName()
	{
		return this.name.isPresent();
	}

	public void setCustomName(@Nullable Component name)
	{
		this.name = Optional.ofNullable(name);
	}

	@Nullable
	@Override
	public Component getCustomName()
	{
		return this.name.orElse(null);
	}

	public int age()
	{
		return this.age;
	}

	public int getMaxAge()
	{
		// Days * Minutes * Seconds * Ticks
		return 3 * 20 * 60 * 20;
		// return 100;
		// return Integer.MAX_VALUE;
	}

	public float getHatchProgress()
	{
		return Mth.clamp(this.age / (float) this.getMaxAge(), 0.0F, 1.0F);
	}

	public boolean canHatch()
	{
		return this.canHatch && RediscoveredConfig.WORLD.hatchableRedDragon();
	}

	public void setHatchable(boolean canHatch)
	{
		this.canHatch = canHatch;
		this.markUpdated();
	}

	public boolean isShaking()
	{
		return this.clientShakeTime < this.maxShakeTime();
	}

	public float clientShakeTime(float partialTicks)
	{
		return this.clientShakeTime + partialTicks;
	}

	public int maxShakeTime()
	{
		return 10;
	}
}
