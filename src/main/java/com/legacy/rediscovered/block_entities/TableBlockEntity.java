package com.legacy.rediscovered.block_entities;

import java.util.List;
import java.util.function.BiPredicate;

import javax.annotation.Nullable;

import com.legacy.rediscovered.block.TableBlock;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredStats;
import com.legacy.structure_gel.api.block_entity.IRotatable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.Container;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.decoration.ItemFrame;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.MapItem;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.saveddata.maps.MapItemSavedData;
import net.minecraft.world.phys.AABB;

public class TableBlockEntity extends ModBlockEntity implements Container, IRotatable
{
	private ItemStack item = ItemStack.EMPTY;
	private Direction facing = Direction.NORTH;
	private TableShape shape = TableShape.ONE_BY_ONE;

	// Not saved. Doesn't need to be. Used to keep track of things like hoppers adding items. When the item stack size changes, we don't know about it :/
	private int oldCount = this.item.getCount();

	public TableBlockEntity(BlockPos pos, BlockState state)
	{
		super(RediscoveredBlockEntityTypes.TABLE, pos, state);
	}

	private static final String ITEM_KEY = "item", FACING_KEY = "facing", TABLE_SHAPE_KEY = "table_shape";

	@Override
	protected void saveAdditional(CompoundTag tag)
	{
		super.saveAdditional(tag);
		tag.put(ITEM_KEY, this.item.save(new CompoundTag()));
		tag.putString(FACING_KEY, this.facing.getSerializedName());
		tag.putString(TABLE_SHAPE_KEY, this.shape.getSerializedName());
	}

	@Override
	public void load(CompoundTag tag)
	{
		super.load(tag);
		this.item = ItemStack.of(tag.getCompound(ITEM_KEY));
		this.facing = Direction.byName(tag.getString(FACING_KEY));
		this.shape = TableShape.byName(tag.getString(TABLE_SHAPE_KEY));
	}

	@Override
	public void mirror(Mirror mirror)
	{
		this.facing = mirror.mirror(this.facing);
	}

	@Override
	public void rotate(Rotation rotation)
	{
		this.facing = rotation.rotate(this.facing);
	}

	public static void serverTick(Level level, BlockPos pos, BlockState state, TableBlockEntity table)
	{
		if (level.getGameTime() % 20 == 0)
		{
			ItemStack mapStack = table.getItem();
			if (mapStack.is(Items.FILLED_MAP))
			{
				Integer id = MapItem.getMapId(mapStack);
				MapItemSavedData mapData = MapItem.getSavedData(mapStack, table.level);
				if (mapData != null && level instanceof ServerLevel serverLevel)
				{
					for (ServerPlayer serverPlayer : serverLevel.players())
					{
						ItemFrame itemFrame = new ItemFrame(serverLevel, pos, Direction.NORTH);
						mapStack.setEntityRepresentation(itemFrame);
						mapData.tickCarriedBy(serverPlayer, mapStack);
						mapData.removedFromFrame(pos, itemFrame.getId());
						Packet<?> packet = mapData.getUpdatePacket(id, serverPlayer);

						if (packet != null)
						{
							serverPlayer.connection.send(packet);
						}
						mapStack.setEntityRepresentation(null);
					}
				}
			}
		}

		if (table.oldCount != table.item.getCount())
		{
			table.onChangeItem(null, null);
			table.oldCount = table.item.getCount();
		}
	}

	public ItemStack getItem()
	{
		return this.item;
	}

	public Direction getFacing()
	{
		return this.facing;
	}

	public TableShape getShape()
	{
		return this.shape;
	}

	public boolean setShape(TableShape shape)
	{
		if (this.shape == shape)
			return false;
		this.shape = shape;
		this.markUpdated();
		return true;
	}

	public boolean interact(Player player, InteractionHand hand, ItemStack heldItem, Direction facing)
	{
		if (heldItem.isEmpty())
		{
			// If no held item and no item in the table, do nothing
			if (this.item.isEmpty())
			{
				return false;
			}
			// If an item is in the table but the hand is empty, attempt to take an item (or all if holding shift)
			else
			{
				int amount = player.isShiftKeyDown() ? this.item.getCount() : 1;
				ItemStack grabbed = this.item.split(amount);
				this.onChangeItem(player, RediscoveredSounds.BLOCK_TABLE_REMOVE_ITEM);
				player.setItemInHand(hand, grabbed);
				return true;
			}
		}
		// If the held item can be placed on the table, place it
		else if (this.item.isEmpty() || (this.item.isStackable() && this.item.getCount() < this.item.getMaxStackSize() && ItemStack.isSameItemSameTags(heldItem, this.item)))
		{
			int amount = 1;
			ItemStack placed = heldItem.split(amount);
			if (this.item.isEmpty())
			{
				this.item = placed;
				this.facing = facing;
			}
			else
			{
				this.item.grow(amount);
			}
			this.onChangeItem(player, RediscoveredSounds.BLOCK_TABLE_ADD_ITEM);
			return true;
		}
		// If the item clicked with is different from what's on the table, swap the items
		else if (!ItemStack.isSameItemSameTags(heldItem, this.item))
		{
			ItemStack oldCopy = this.item.copy();
			this.item = heldItem.copy();
			player.setItemInHand(hand, oldCopy);
			this.onChangeItem(player, RediscoveredSounds.BLOCK_TABLE_ADD_ITEM);
			return true;
		}
		return false;
	}

	public void onChangeItem(@Nullable Player player, @Nullable SoundEvent sound)
	{
		if (!this.level.isClientSide)
		{
			this.level.gameEvent(GameEvent.BLOCK_CHANGE, this.getBlockPos(), GameEvent.Context.of(player, this.getBlockState()));
			if (sound != null)
			{
				this.level.playSound(null, this.getBlockPos(), sound, SoundSource.BLOCKS, 1.0F, 1.0F);
			}
			if (player != null)
			{
				player.awardStat(RediscoveredStats.INTERACT_WITH_TABLE);
			}

			this.oldCount = this.item.getCount(); // Done to make sure we don't fire this again in serverTick
			this.markUpdated();
		}
	}

	@Override
	public void clearContent()
	{
		this.item = ItemStack.EMPTY;
		this.onChangeItem(null, null);
	}

	@Override
	public int getContainerSize()
	{
		return 1;
	}

	@Override
	public boolean isEmpty()
	{
		return this.item.isEmpty();
	}

	@Override
	public ItemStack getItem(int slot)
	{
		return this.item;
	}

	@Override
	public ItemStack removeItem(int index, int amount)
	{
		ItemStack stack = !this.item.isEmpty() && amount > 0 ? this.item.split(amount) : ItemStack.EMPTY;
		this.onChangeItem(null, null);
		return stack;
	}

	@Override
	public ItemStack removeItemNoUpdate(int slot)
	{
		ItemStack stack = this.item.copy();
		this.item = ItemStack.EMPTY;
		this.onChangeItem(null, null);
		return stack;
	}

	@Override
	public void setItem(int slot, ItemStack stack)
	{
		this.item = stack;
		this.onChangeItem(null, null);
	}

	@Override
	public boolean stillValid(Player player)
	{
		return Container.stillValidBlockEntity(this, player);
	}

	public static enum TableShape implements StringRepresentable
	{
		//@formatter:off
		ONE_BY_ONE(1, 1, MultiBlockPattern.builder().pattern()
				.validator('a', tableTest(List.of(), List.of(TableBlock.NORTH, TableBlock.SOUTH, TableBlock.EAST, TableBlock.WEST)))
				.state(0, 0, 0, 'a')
				.build()),
		ONE_BY_TWO(1, 2, MultiBlockPattern.builder().pattern()
				.validator('a', tableTest(List.of(TableBlock.SOUTH), List.of(TableBlock.NORTH, TableBlock.EAST, TableBlock.WEST)))
				.validator('b', tableTest(List.of(TableBlock.NORTH), List.of(TableBlock.SOUTH, TableBlock.EAST, TableBlock.WEST)))
				.state(0, 0, 0, 'a')
				.state(0, 0, 1, 'b')
				.build()),
		TWO_BY_ONE(2, 1, MultiBlockPattern.builder().pattern()
				.validator('a', tableTest(List.of(TableBlock.EAST), List.of(TableBlock.NORTH, TableBlock.SOUTH, TableBlock.WEST)))
				.validator('b', tableTest(List.of(TableBlock.WEST), List.of(TableBlock.NORTH, TableBlock.SOUTH, TableBlock.EAST)))
				.state(0, 0, 0, 'a')
				.state(1, 0, 0, 'b')
				.build()),
		TWO_BY_TWO(2, 2, MultiBlockPattern.builder().pattern()
				.validator('a', tableTest(List.of(TableBlock.SOUTH, TableBlock.EAST), List.of(TableBlock.NORTH, TableBlock.WEST)))
				.validator('b', tableTest(List.of(TableBlock.SOUTH, TableBlock.WEST), List.of(TableBlock.NORTH, TableBlock.EAST)))
				.validator('c', tableTest(List.of(TableBlock.NORTH, TableBlock.EAST), List.of(TableBlock.SOUTH, TableBlock.WEST)))
				.validator('d', tableTest(List.of(TableBlock.NORTH, TableBlock.WEST), List.of(TableBlock.SOUTH, TableBlock.EAST)))
				.state(0, 0, 0, 'a')
				.state(1, 0, 0, 'b')
				.state(0, 0, 1, 'c')
				.state(1, 0, 1, 'd')
				.build()),
		THREE_BY_THREE(3, 3, MultiBlockPattern.builder().pattern()
				.validator('a', tableTest(List.of(TableBlock.NORTH, TableBlock.SOUTH, TableBlock.EAST, TableBlock.WEST), List.of()))
				// Edges
				.validator('b', tableTest(List.of(TableBlock.NORTH, TableBlock.SOUTH, TableBlock.WEST), List.of(TableBlock.EAST)))
				.validator('c', tableTest(List.of(TableBlock.NORTH, TableBlock.SOUTH, TableBlock.EAST), List.of(TableBlock.WEST)))
				.validator('d', tableTest(List.of(TableBlock.NORTH, TableBlock.WEST, TableBlock.EAST), List.of(TableBlock.SOUTH)))
				.validator('e', tableTest(List.of(TableBlock.SOUTH, TableBlock.WEST, TableBlock.EAST), List.of(TableBlock.NORTH)))
				// Corners
				.validator('f', tableTest(List.of(TableBlock.SOUTH, TableBlock.EAST), List.of(TableBlock.NORTH, TableBlock.WEST)))
				.validator('g', tableTest(List.of(TableBlock.SOUTH, TableBlock.WEST), List.of(TableBlock.NORTH, TableBlock.EAST)))
				.validator('h', tableTest(List.of(TableBlock.NORTH, TableBlock.EAST), List.of(TableBlock.SOUTH, TableBlock.WEST)))
				.validator('i', tableTest(List.of(TableBlock.NORTH, TableBlock.WEST), List.of(TableBlock.SOUTH, TableBlock.EAST)))
				
				.state(0, 0, 0, 'a')
				// Edges
				.state(1, 0, 0, 'b')
				.state(-1, 0, 0, 'c')
				.state(0, 0, 1, 'd')
				.state(0, 0, -1, 'e')
				// Corners
				.state(-1, 0, -1, 'f')
				.state(1, 0, -1, 'g')
				.state(-1, 0, 1, 'h')
				.state(1, 0, 1, 'i')
				.build());
		//@formatter:on

		final int width, length;
		final String name;
		final MultiBlockPattern pattern;

		TableShape(int width, int length, MultiBlockPattern pattern)
		{
			this.width = width;
			this.length = length;
			this.name = width + "x" + length;
			this.pattern = pattern;
		}

		public static BiPredicate<BlockState, BlockState> tableTest(List<BooleanProperty> trueValues, List<BooleanProperty> falseValues)
		{
			return (self, state) ->
			{
				if (state.is(self.getBlock()))
				{
					for (var prop : trueValues)
					{
						if (!state.getValue(prop))
							return false;
					}
					for (var prop : falseValues)
					{
						if (state.getValue(prop))
							return false;
					}
					return true;
				}
				return false;
			};
		}

		public boolean growItem()
		{
			return this == THREE_BY_THREE;
		}

		public boolean isSingle()
		{
			return this == ONE_BY_ONE;
		}

		public static TableShape compute(Level level, BlockPos pos, BlockState state)
		{
			if (state.getValue(TableBlock.SINGLE))
				return ONE_BY_ONE;

			for (TableShape shape : values())
			{
				int xDist = shape.width / 2;
				int zDist = shape.length / 2;
				for (int x = -xDist; x <= xDist; x++)
				{
					for (int z = -zDist; z <= zDist; z++)
					{
						BlockPos offsetPos = pos.offset(x, 0, z);
						if (shape.pattern.isValid(level, offsetPos, state))
							return shape;
					}
				}
			}

			return ONE_BY_ONE;
		}

		public static TableShape byName(String name)
		{
			for (TableShape shape : values())
				if (shape.name.equals(name))
					return shape;
			return ONE_BY_ONE;
		}

		@Override
		public String getSerializedName()
		{
			return this.name;
		}
	}
}
