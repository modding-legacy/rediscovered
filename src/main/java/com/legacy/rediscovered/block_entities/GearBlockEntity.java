package com.legacy.rediscovered.block_entities;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.block.GearBlock;
import com.legacy.rediscovered.block.GearBlock.GearFace;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.block_entity.IRotatable;
import com.legacy.structure_gel.api.util.VoxelShapeUtil;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;

public class GearBlockEntity extends ModBlockEntity implements IRotatable
{
	// Powers stores redstone signal strength. Positive or negative relates to the
	// direction the gear should spin, while the number itself is equal to signal
	// strength. -15 - 15
	private EnumMap<GearFace, GearPower> gearPowers = Util.make(() ->
	{
		EnumMap<GearFace, GearPower> map = new EnumMap<>(GearFace.class);
		for (GearFace face : GearFace.values())
			map.put(face, new GearPower());
		return map;
	});

	// ---- USED CLIENT SIDE ONLY ----
	// Stores the angles of each gear for their rotation animation. The angle is
	// increased/decreased based on the powers array.
	// Array order matches GearFace enum
	private int[] angles = new int[] { 0, 0, 0, 0, 0, 0 };
	private int[] oldAngles = new int[] { 0, 0, 0, 0, 0, 0 };

	public GearBlockEntity(BlockPos pos, BlockState state)
	{
		super(RediscoveredBlockEntityTypes.GEAR, pos, state);
	}

	@Override
	public void mirror(Mirror mirror)
	{
		if (mirror == Mirror.FRONT_BACK)
		{
			GearPower e = this.getPower(GearFace.EAST);
			GearPower w = this.getPower(GearFace.WEST);
			this.gearPowers.put(GearFace.EAST, w);
			this.gearPowers.put(GearFace.WEST, e);
		}
		else if (mirror == Mirror.LEFT_RIGHT)
		{
			GearPower n = this.getPower(GearFace.NORTH);
			GearPower s = this.getPower(GearFace.SOUTH);
			this.gearPowers.put(GearFace.NORTH, s);
			this.gearPowers.put(GearFace.SOUTH, n);
		}
	}

	@Override
	public void rotate(Rotation rotation)
	{
		if (rotation != Rotation.NONE)
		{
			Map<Direction, GearPower> gears = new HashMap<>();
			for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
				gears.put(dir, this.getPower(GearFace.get(dir)));

			for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
			{
				Direction rotatedDir = rotation.rotate(dir);
				this.gearPowers.put(GearFace.get(rotatedDir), gears.get(dir));
			}
		}
	}

	public void startSpinning(ServerLevel level, BlockPos source, BlockPos gearPos, GearFace face, int power, int distFromSource)
	{
		// System.out.println("start " + gearPos + " " + face);
		if (power != 0 && this.getBlockState().getValue(face.stateProperty).exists() && (distFromSource < RediscoveredConfig.WORLD.maxGearDistance() || RediscoveredConfig.WORLD.maxGearDistance() == -1))
		{
			if (this.addSource(face, new Source(source, distFromSource, power)))
			{
				this.markDirty();
				this.spinNeighbors(level, source, gearPos, face, power, distFromSource + 1);
			}
		}
	}

	public void spinNeighbors(ServerLevel level, BlockPos source, BlockPos gearPos, GearFace face, int power, int distFromSource)
	{
		if (power == 0)
			return;
		List<Pair<BlockPos, GearFace>> possibleConnections = face.getPossibleConnections(gearPos);
		for (var possibleCon : possibleConnections)
		{
			BlockPos connectionPos = possibleCon.getKey();
			BlockState connectionState = level.getBlockState(connectionPos);
			GearFace connectionFace = possibleCon.getValue();
			if (connectionState.hasProperty(connectionFace.stateProperty) && connectionState.getValue(connectionFace.stateProperty).exists() && level.getBlockEntity(connectionPos)instanceof GearBlockEntity connectedGear)
			{
				GearPower connectedGearPower = connectedGear.getPower(connectionFace);
				Source connectedSource = connectedGearPower.getSource(source);

				if (connectedSource == null || (connectedSource.getPower() != -power && connectedSource.distFromSource > distFromSource))
				{
					connectedGear.startSpinning(level, source, connectionPos, connectionFace, -power, distFromSource);
				}
			}
		}
	}

	public void stopSpinning(ServerLevel level, BlockPos source, BlockPos gearPos, GearFace face)
	{
		if (this.getBlockState().getValue(face.stateProperty).exists())
		{
			if (this.removeSource(face, source))
			{
				this.stopNeighbors(level, source, gearPos, face);
				this.markDirty();
			}
		}

		for (GearFace f : GearFace.values())
		{
			GearPower power = this.getPower(f);
			if (power.getSources().isEmpty())
				continue;
			for (Source s : List.copyOf(power.getSources())) // Prevent concurrent modification
			{
				if (s.distFromSource == 0)
				{
					BlockPos sPos = s.pos;
					if (sPos.distManhattan(gearPos) > 1)
					{
						System.out.println("too far! " + gearPos);
						this.stopSpinning(level, sPos, gearPos, f);
					}
				}
			}
		}
	}

	public void stopNeighbors(ServerLevel level, BlockPos source, BlockPos gearPos, GearFace face)
	{
		List<Pair<BlockPos, GearFace>> possibleConnections = face.getPossibleConnections(gearPos);
		for (var possibleCon : possibleConnections)
		{
			BlockPos connectionPos = possibleCon.getKey();
			BlockState connectionState = level.getBlockState(connectionPos);
			GearFace connectionFace = possibleCon.getValue();
			if (connectionState.hasProperty(connectionFace.stateProperty) && connectionState.getValue(connectionFace.stateProperty).exists() && level.getBlockEntity(connectionPos)instanceof GearBlockEntity connectedGear)
			{
				GearPower connectedGearPower = connectedGear.getPower(connectionFace);
				if (connectedGearPower.hasSource(source))
				{
					connectedGear.stopSpinning(level, source, connectionPos, connectionFace);
				}
			}
		}
	}

	public void onNeighborUpdated(ServerLevel level, BlockState gearState, BlockPos gearPos)
	{
		for (GearFace face : GearFace.values())
		{
			if (gearState.getValue(face.stateProperty).exists())
			{
				List<Source> sources = List.copyOf(this.getPower(face).getSources());
				for (Source source : sources)
				{
					if (this.locateSource(level, source, gearPos, face))
					{
						this.startSpinning(level, source.pos, gearPos, face, source.getPower(), source.distFromSource);
					}
					else
					{
						this.stopSpinning(level, source.pos, gearPos, face);
					}
				}
			}
		}
	}

	// This method calls in a recursive loop until it finds the source or fails
	// trying
	public boolean locateSource(ServerLevel level, Source source, BlockPos gearPos, GearFace face)
	{
		// We found the first gear in the line
		if (source.distFromSource == 0)
			return true;

		List<Pair<BlockPos, GearFace>> possibleConnections = face.getPossibleConnections(gearPos);
		for (var possibleCon : possibleConnections)
		{
			BlockPos connectionPos = possibleCon.getKey();
			BlockState connectionState = level.getBlockState(connectionPos);
			GearFace connectionFace = possibleCon.getValue();
			if (connectionState.hasProperty(connectionFace.stateProperty) && connectionState.getValue(connectionFace.stateProperty).exists() && level.getBlockEntity(connectionPos)instanceof GearBlockEntity connectedGear)
			{
				Source connectionSource = connectedGear.getPower(connectionFace).getSource(source.pos);
				if (connectionSource != null)
				{
					if (connectionSource.distFromSource < source.distFromSource)
					{
						return this.locateSource(level, connectionSource, connectionPos, connectionFace);
					}
				}
			}
		}

		return false;
	}

	public static void clientTick(Level level, BlockPos pos, BlockState state, GearBlockEntity gear)
	{
		for (GearFace face : GearFace.values())
		{
			int i = face.ordinal();
			gear.oldAngles[i] = gear.angles[i];
			int power = gear.getPower(face).getPower();
			if (power != 0)
				gear.addAngle(i, (power / 2) + (power > 0 ? 1 : -1));
		}
	}

	public void updateConverter(GearFace face)
	{
		if (!this.level.isClientSide)
		{
			BlockPos pos = this.worldPosition.relative(face.direction);
			BlockState state = this.level.getBlockState(pos);
			if (state.is(RediscoveredBlocks.rotational_converter))
			{
				this.level.scheduleTick(pos, state.getBlock(), 2);
			}
		}
	}

	private static final String GEAR_POWERS = "powers";

	@Override
	public void load(CompoundTag tag)
	{
		super.load(tag);

		CompoundTag powersTag = tag.getCompound(GEAR_POWERS);
		for (GearFace face : GearFace.values())
		{
			GearPower power = GearPower.read(powersTag.getCompound(face.direction.getSerializedName()));
			this.gearPowers.put(face, power);
		}
	}

	@Override
	public void saveAdditional(CompoundTag tag)
	{
		super.saveAdditional(tag);

		CompoundTag powersTag = new CompoundTag();
		for (var entry : this.gearPowers.entrySet())
		{
			String key = entry.getKey().direction.getSerializedName();
			CompoundTag value = entry.getValue().write();
			powersTag.put(key, value);
		}
		tag.put(GEAR_POWERS, powersTag);
	}

	// Gets the power of the face specified.
	public GearPower getPower(GearFace face)
	{
		return this.gearPowers.get(face);
	}

	public boolean isPowered(GearFace face)
	{
		return this.getPower(face).getPower() != 0;
	}

	public boolean addSource(GearFace face, Source source)
	{
		GearPower gearPower = this.getPower(face);
		int oldPower = gearPower.getPower();

		// If trying to rotate a gear the other way while it's already moving, destroy
		// it
		if (!gearPower.addSource(source))
		{
			this.level.setBlock(this.worldPosition, this.getBlockState().setValue(face.stateProperty, GearBlock.GearState.NONE), 3);
			BlockState fakeState = this.getBlockState().getBlock().defaultBlockState().setValue(face.stateProperty, GearBlock.GearState.GEAR);
			this.level.levelEvent(2001, this.worldPosition, Block.getId(fakeState));
			Block.dropResources(fakeState, this.level, this.worldPosition, this, null, ItemStack.EMPTY);
			this.getPower(face).sources.clear();
			this.markDirty();
			return false;
		}

		if (Math.abs(oldPower) != Math.abs(gearPower.getPower()))
			this.updateConverter(face);

		return true;
	}

	public boolean removeSource(GearFace face, BlockPos source)
	{
		GearPower gearPower = this.getPower(face);
		int oldPower = gearPower.getPower();

		if (gearPower.removeSource(source))
		{
			if (Math.abs(oldPower) != Math.abs(gearPower.getPower()))
				this.updateConverter(face);
			return true;
		}
		return false;
	}

	public float getAngle(Direction direction, float partialTick)
	{
		int i = direction.get3DDataValue();
		return Mth.rotLerp(partialTick, this.oldAngles[i], this.angles[i]);
	}

	public void addAngle(int direction, int amount)
	{
		int angle = this.angles[direction];
		this.angles[direction] = Mth.wrapDegrees(angle + amount);
	}

	private void markDirty()
	{
		this.setChanged();
		this.getLevel().sendBlockUpdated(this.getBlockPos(), this.getBlockState(), this.getBlockState(), 3);
	}

	public static class Source
	{
		private final BlockPos pos;
		private final int distFromSource;
		private int power = 0;

		public Source(BlockPos pos, int distFromSource, int power)
		{
			this.pos = pos.immutable();
			this.distFromSource = distFromSource;
			this.power = power;
		}

		private static final String POS_KEY = "pos", DIST_KEY = "distance", POWER_KEY = "power";

		public static Source read(CompoundTag tag)
		{
			return new Source(NbtUtils.readBlockPos(tag.getCompound(POS_KEY)), tag.getInt(DIST_KEY), tag.getInt(POWER_KEY));
		}

		public CompoundTag write()
		{
			CompoundTag tag = new CompoundTag();
			tag.put(POS_KEY, NbtUtils.writeBlockPos(this.pos));
			tag.putInt(DIST_KEY, this.distFromSource);
			tag.putInt(POWER_KEY, this.power);
			return tag;
		}

		public BlockPos getPos()
		{
			return this.pos;
		}

		public int distFromSource()
		{
			return this.distFromSource;
		}

		public int getPower()
		{
			return this.power;
		}

		public int getRotation()
		{
			return power > 0 ? 1 : power < 0 ? -1 : 0;
		}
	}

	public static class GearPower
	{
		@Deprecated // Use getter/setter
		private final Map<BlockPos, Source> sources = new HashMap<>();
		@Deprecated // This shouldn't be referenced
		int rotation = 0;

		private GearPower()
		{
		}

		public boolean hasSource(BlockPos pos)
		{
			return this.sources.containsKey(pos);
		}

		public boolean addSource(Source source)
		{
			this.sources.put(source.pos, source);
			int oldRotation = this.rotation;
			this.rotation = source.getRotation();
			return oldRotation == 0 || oldRotation == this.rotation;
		}

		public boolean removeSource(BlockPos pos)
		{
			var prev = this.sources.remove(pos);
			if (this.sources.isEmpty())
				this.rotation = 0;
			return prev != null;
		}

		@Nullable
		public Source getSource(BlockPos pos)
		{
			return this.sources.get(pos);
		}

		public Collection<Source> getSources()
		{
			return this.sources.values();
		}

		public int getPower()
		{
			if (this.sources.isEmpty())
				this.rotation = 0;
			int power = 0;
			for (Source source : this.sources.values())
			{
				if (this.rotation > 0)
					power = Math.max(power, source.getPower());
				if (this.rotation < 0)
					power = Math.min(power, source.getPower());
			}

			return power;
		}

		private static final String ROTATION_KEY = "rotation", SOURCES_KEY = "sources";

		public static GearPower read(CompoundTag tag)
		{
			GearPower gearPower = new GearPower();
			gearPower.sources.clear();
			ListTag sourcesTag = tag.getList(SOURCES_KEY, Tag.TAG_COMPOUND);
			for (Tag t : sourcesTag)
			{
				Source source = Source.read((CompoundTag) t);
				gearPower.sources.put(source.pos, source);
			}
			gearPower.rotation = tag.getInt(ROTATION_KEY);
			return gearPower;
		}

		public CompoundTag write()
		{
			CompoundTag tag = new CompoundTag();

			ListTag sourcesTag = new ListTag();
			for (var source : this.sources.values())
				sourcesTag.add(source.write());
			tag.put(SOURCES_KEY, sourcesTag);

			tag.putInt(ROTATION_KEY, this.rotation);
			return tag;
		}
	}
}
