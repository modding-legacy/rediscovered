package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.rediscovered.registry.RediscoveredBlocks;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.TerrainParticle;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

@Mixin(TerrainParticle.class)
public class TerrainParticleMixin
{
	@Inject(at = @At("TAIL"), method = "<init>(Lnet/minecraft/client/multiplayer/ClientLevel;DDDDDDLnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;)V")
	private void initTail(ClientLevel pLevel, double pX, double pY, double pZ, double pXSpeed, double pYSpeed, double pZSpeed, BlockState pState, BlockPos pPos, CallbackInfo callback)
	{
		if (pState.is(RediscoveredBlocks.grass_slab))
			((TerrainParticle) (Object) this).setColor(0.6F, 0.6F, 0.6F);
	}
}
