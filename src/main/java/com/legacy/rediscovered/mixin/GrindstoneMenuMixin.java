package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.common.CommonHooks;

@Mixin(targets = "net/minecraft/world/inventory/GrindstoneMenu$4") // The result slot of the grindstone. Used to store the player since the forge event doesn't :/
public class GrindstoneMenuMixin
{
	@Inject(at = @At("HEAD"), method = "onTake")
	private void onTakeHead(Player player, ItemStack stack, CallbackInfo callback)
	{
		CommonHooks.setCraftingPlayer(player);
	}

	@Inject(at = @At("RETURN"), method = "onTake")
	private void onTakeReturn(Player player, ItemStack stack, CallbackInfo callback)
	{
		CommonHooks.setCraftingPlayer(null);
	}
}
