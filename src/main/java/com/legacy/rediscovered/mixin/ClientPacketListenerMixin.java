package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;

import net.minecraft.client.multiplayer.ClientPacketListener;
import net.minecraft.world.entity.Entity;

@Mixin(ClientPacketListener.class)
public class ClientPacketListenerMixin
{
	private static Entity rediscovered$lastRidden;

	@ModifyVariable(at = @At("STORE"), method = "handleSetEntityPassengersPacket(Lnet/minecraft/network/protocol/game/ClientboundSetPassengersPacket;)V", index = 2)
	private Entity modify$entity(Entity entity)
	{
		rediscovered$lastRidden = entity;
		return entity;
	}

	@ModifyArg(at = @At(value = "INVOKE", target = "net/minecraft/network/chat/Component.translatable(Ljava/lang/String;[Ljava/lang/Object;)Lnet/minecraft/network/chat/MutableComponent;"), method = "handleSetEntityPassengersPacket")
	private String modify$pKey(String pKey)
	{
		if (rediscovered$lastRidden != null && rediscovered$lastRidden instanceof RedDragonOffspringEntity dragon)
			return RedDragonOffspringEntity.MOUNT_DRAGON_KEY;

		return pKey;
	}
}
