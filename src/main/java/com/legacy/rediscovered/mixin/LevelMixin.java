package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.client.RediscoveredClientEvents;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.level.Level;

@Mixin(Level.class)
public class LevelMixin
{
	@Inject(at = @At("RETURN"), method = "getRainLevel", cancellable = true)
	private void rediscovered$overrideRainLevel(float pDelta, CallbackInfoReturnable<Float> callback)
	{
		if (((Level) (Object) this) instanceof ClientLevel && RediscoveredClientEvents.dragonThunderTime > 0)
			callback.setReturnValue(Math.max(callback.getReturnValue(), RediscoveredClientEvents.dragonThunderTime));
	}

	@Inject(at = @At("RETURN"), method = "getThunderLevel", cancellable = true)
	private void rediscovered$overrideThunderLevel(float pDelta, CallbackInfoReturnable<Float> callback)
	{
		if (((Level) (Object) this) instanceof ClientLevel && RediscoveredClientEvents.dragonThunderTime > 0)
			callback.setReturnValue(Math.max(callback.getReturnValue(), RediscoveredClientEvents.dragonThunderTime));
	}
}
