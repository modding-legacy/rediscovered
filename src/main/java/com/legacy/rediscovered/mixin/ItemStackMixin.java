package com.legacy.rediscovered.mixin;

import java.util.Optional;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.capability.util.QuiverDataHolder;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.item.util.QuiverData;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.SlotAccess;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickAction;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

@Mixin(ItemStack.class)
public class ItemStackMixin implements QuiverDataHolder
{
	// -------------------------------------------------------
	// Keeps track of what item was used to shoot an arrow from the quiver, and consumes accordingly
	// -------------------------------------------------------
	
	@Nullable
	private QuiverData rediscovered$quiverData = null;
	@Nullable
	private ItemStack rediscovered$quiverWeapon = null;

	@Override
	@Nullable
	public QuiverData rediscovered$getQuiverData()
	{
		return rediscovered$quiverData;
	}

	@Override
	public void rediscovered$setQuiverData(@Nullable QuiverData data, @Nullable ItemStack weapon)
	{
		rediscovered$quiverData = data;
		rediscovered$quiverWeapon = weapon;
	}

	@Inject(at = @At("TAIL"), method = "setCount")
	private void setCountHook(CallbackInfo callback)
	{
		if (this.rediscovered$quiverData != null)
		{
			this.rediscovered$quiverData.onConsumeAmmo(this.rediscovered$quiverWeapon == null ? ItemStack.EMPTY : this.rediscovered$quiverWeapon);
			this.rediscovered$setQuiverData(null, null);
		}
	}

	// -------------------------------------------------------
	// Handle "bundle" interactions
	// -------------------------------------------------------
	
	@Inject(at = @At("HEAD"), method = "overrideStackedOnOther", cancellable = true)
	private void overrideStackedOnOtherHook(Slot pSlot, ClickAction pAction, Player pPlayer, CallbackInfoReturnable<Boolean> callback)
	{
		Optional<AttachedItem> attached = AttachedItem.get((ItemStack) (Object) this);
		if (attached.isPresent() && attached.get().getAttached().overrideStackedOnOther(pSlot, pAction, pPlayer))
		{
			attached.get().save();
			callback.setReturnValue(true);
		}
	}

	@Inject(at = @At("HEAD"), method = "overrideOtherStackedOnMe", cancellable = true)
	private void overrideOtherStackedOnMeHook(ItemStack pStack, Slot pSlot, ClickAction pAction, Player pPlayer, SlotAccess pAccess, CallbackInfoReturnable<Boolean> callback)
	{
		Optional<AttachedItem> attached = AttachedItem.get((ItemStack) (Object) this);
		if (attached.isPresent() && attached.get().getAttached().overrideOtherStackedOnMe(pStack, pSlot, pAction, pPlayer, pAccess))
		{
			attached.get().save();
			callback.setReturnValue(true);
		}
	}
	
	// -------------------------------------------------------
	// Handles item breaking interactions
	// -------------------------------------------------------
	
	@Nullable
	private ItemStack rediscovered$attachedItemCache;
	
	@Inject(at = @At("RETURN"), method = "hurt(ILnet/minecraft/util/RandomSource;Lnet/minecraft/server/level/ServerPlayer;)Z")
	private void onDamaged(CallbackInfoReturnable<Boolean> callback)
	{
		// True is returned when the item is supposed to break
		if (callback.getReturnValue())
		{
			Optional<AttachedItem> attached = AttachedItem.get((ItemStack) (Object) this);
			if (attached.isPresent())
			{
				//System.out.println(this + " will break");
				this.rediscovered$attachedItemCache = attached.get().getAttached();
			}
		}
	}
	
	// Happens when the itemstack gets shrunk, just before it happens
	@Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/world/item/ItemStack;shrink(I)V"), method = "hurtAndBreak(ILnet/minecraft/world/entity/LivingEntity;Ljava/util/function/Consumer;)V")
	private void onBroken(int amount, LivingEntity entity, Consumer<LivingEntity> onBroken, CallbackInfo callback)
	{
		//System.out.println("Attempted to break " + this);
		if (this.rediscovered$attachedItemCache != null)
		{
			ItemStack me = (ItemStack) (Object) this;
			for (EquipmentSlot slot : EquipmentSlot.values())
			{
				if (entity.getItemBySlot(slot) == me)
				{
					//System.out.println("Found armor slot " + slot);
					//System.out.println("Replaced with " + this.rediscovered$attachedItemCache.copy());
					entity.setItemSlot(slot, this.rediscovered$attachedItemCache.copy());
					this.rediscovered$attachedItemCache = null;
					break;
				}
			}
		}
	}
}
