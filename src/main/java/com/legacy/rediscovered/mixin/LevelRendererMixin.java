package com.legacy.rediscovered.mixin;

import org.joml.Matrix4f;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.rediscovered.client.render.world.SkylandsCloudRenderer;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.LevelRenderer;

@Mixin(LevelRenderer.class)
public class LevelRendererMixin
{
	// This has to happen after the dimension special effects render to prevent overriding modded clouds
	// Prevent crashing when Sodium (or similar) guts cloud rendering code
	@Inject(require = 0, at = @At(value = "INVOKE", target = "Lnet/minecraft/client/renderer/DimensionSpecialEffects;getCloudHeight()F"), method = "renderClouds(Lcom/mojang/blaze3d/vertex/PoseStack;Lorg/joml/Matrix4f;FDDD)V", cancellable = true)
	private void renderFluffyClouds(PoseStack pPoseStack, Matrix4f pProjectionMatrix, float pPartialTick, double pCamX, double pCamY, double pCamZ, CallbackInfo callback)
	{
		if (SkylandsCloudRenderer.renderCloudOverride(pPoseStack, pProjectionMatrix, pPartialTick, pCamX, pCamY, pCamZ))
			callback.cancel();
	}
}
