package com.legacy.rediscovered.mixin;

import java.util.ArrayList;
import java.util.List;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;

import com.legacy.rediscovered.client.RediscoveredResourcePackHandler;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackRepository;

@Mixin(PackRepository.class)
public abstract class PackRepositoryMixin
{
	@ModifyReturnValue(method = "rebuildSelected", at = @At("RETURN"))
	private List<Pack> gns$rebuildSelected(List<Pack> original)
	{
		List<Pack> selected = new ArrayList<>(original);
		Pack legacy = RediscoveredResourcePackHandler.LEGACY_PACK;
		Pack pa = this.getPack("programmer_art");

		if (selected.contains(pa))
		{
			if (!selected.contains(legacy))
			{
				for (int i = 0; i < selected.size(); ++i)
				{
					if (selected.get(i) == pa)
					{
						selected.add(i + 1, legacy);
						break;
					}
				}
			}

			return selected;
		}
		else if (selected.contains(legacy))
		{
			selected.remove(legacy);
			return selected;
		}

		return original;
	}

	@Shadow
	public Pack getPack(String id)
	{
		throw new IllegalStateException();
	}
}
