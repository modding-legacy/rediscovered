package com.legacy.rediscovered.mixin.store;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.client.gui.StoreScreen;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.HorseModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.ChestedHorseRenderer;
import net.minecraft.client.renderer.entity.HorseRenderer;
import net.minecraft.client.renderer.entity.UndeadHorseRenderer;
import net.minecraft.client.renderer.entity.layers.HorseMarkingLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.animal.horse.AbstractChestedHorse;
import net.minecraft.world.entity.animal.horse.AbstractHorse;
import net.minecraft.world.entity.animal.horse.Horse;

public abstract class HorseSaddleMixins
{
	@Mixin(HorseRenderer.class)
	public static class HorseRendererMixin
	{
		@Inject(at = @At("HEAD"), method = "getTextureLocation", cancellable = true)
		private void getTextureLocation(Horse pEntity, CallbackInfoReturnable<ResourceLocation> callback)
		{
			if (StoreScreen.horseSaddle)
				callback.setReturnValue(StoreScreen.HORSE_SADDLE);
		}
	}
	
	@Mixin(UndeadHorseRenderer.class)
	public static class UndeadHorseRendererMixin
	{
		@Inject(at = @At("HEAD"), method = "getTextureLocation", cancellable = true)
		private void getTextureLocation(AbstractHorse pEntity, CallbackInfoReturnable<ResourceLocation> callback)
		{
			if (StoreScreen.horseSaddle)
				callback.setReturnValue(StoreScreen.HORSE_SADDLE);
		}
	}
	
	@Mixin(ChestedHorseRenderer.class)
	public static class ChestedHorseRendererMixin
	{
		@Inject(at = @At("HEAD"), method = "getTextureLocation", cancellable = true)
		private void getTextureLocation(AbstractChestedHorse pEntity, CallbackInfoReturnable<ResourceLocation> callback)
		{
			if (StoreScreen.horseSaddle)
				callback.setReturnValue(StoreScreen.HORSE_SADDLE);
		}
	}

	@Mixin(HorseMarkingLayer.class)
	public static class HorseMarkingLayerMixin
	{
		@Inject(at = @At("HEAD"), method = "render", cancellable = true)
		private void render(PoseStack pPoseStack, MultiBufferSource pBuffer, int pPackedLight, Horse pLivingEntity, float pLimbSwing, float pLimbSwingAmount, float pPartialTicks, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch, CallbackInfo callback)
		{
			if (StoreScreen.horseSaddle)
				callback.cancel();
		}
	}

	@Mixin(HorseModel.class)
	public static class HorseModelMixin
	{
		@ModifyVariable(at = @At(value = "STORE", ordinal = 0), method = "setupAnim")
		private boolean isSaddled(boolean original)
		{
			return original || StoreScreen.horseSaddle;
		}
	}
}