package com.legacy.rediscovered.mixin.store;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.client.gui.StoreScreen;

import net.minecraft.client.renderer.entity.VillagerRenderer;
import net.minecraft.client.renderer.entity.layers.VillagerProfessionLayer;
import net.minecraft.resources.ResourceLocation;

@Mixin(VillagerRenderer.class)
public class VillagerRendererMixin
{
	@Inject(at = @At("HEAD"), method = "getTextureLocation", cancellable = true)
	private void getTexture(CallbackInfoReturnable<ResourceLocation> callback)
	{
		if (StoreScreen.steveVillager)
			callback.setReturnValue(StoreScreen.STEVE_VILLAGER);
	}
	
	@Mixin(VillagerProfessionLayer.class)
	public static class VillagerProfessionLayerMixin
	{
		@Inject(at = @At("HEAD"), method = "getResourceLocation", cancellable = true)
		private void getTexture(String folder, ResourceLocation tex, CallbackInfoReturnable<ResourceLocation> callback)
		{
			if (StoreScreen.steveVillager && folder.equals("type"))
				callback.setReturnValue(StoreScreen.STEVE_VILLAGER);
		}
	}
}
