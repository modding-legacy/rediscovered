package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.item.ItemEntity;

@Mixin(Mob.class)
public interface MobAccessor
{
	/**
	 * Protected method normally, can't AT it.
	 */
	@Invoker("pickUpItem")
	public void rediscovered$pickUpItem(ItemEntity itemEntity);
}