package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.client.render.world.SkylandsSkyRenderer;

import net.minecraft.client.multiplayer.ClientLevel;

@Mixin(ClientLevel.ClientLevelData.class)
public class ClientLevelDataMixin
{
	@Inject(at = @At("HEAD"), method = "getClearColorScale", cancellable = true)
	private void priorityFogFunctionHook(CallbackInfoReturnable<Float> callback)
	{
		Float ret = SkylandsSkyRenderer.INSTANCE.modifyVoidDarkness();
		if (ret != null)
			callback.setReturnValue(ret);
	}
}
