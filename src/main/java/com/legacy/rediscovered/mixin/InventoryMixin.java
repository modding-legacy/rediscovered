package com.legacy.rediscovered.mixin;

import java.util.Optional;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.QuiverData;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

@Mixin(Inventory.class)
public class InventoryMixin
{
	@Shadow
	private Player player;

	@Inject(at = @At("HEAD"), method = "add(ILnet/minecraft/world/item/ItemStack;)Z", cancellable = true)
	private void addHook(int slot, ItemStack stack, CallbackInfoReturnable<Boolean> callback)
	{
		if (slot == -1 && !stack.isEmpty() && stack.is(RediscoveredTags.Items.QUIVER_AMMO) && stack.is(RediscoveredTags.Items.QUIVER_AUTO_ADD))
		{
			Optional<QuiverData> opQuiver = QuiverData.getFromChestplate(this.player.getItemBySlot(EquipmentSlot.CHEST));
			if (opQuiver.isPresent() && opQuiver.get().add(stack))
			{
				callback.setReturnValue(true);
			}
		}
	}
}
