package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.registry.RediscoveredAttachmentTypes;

import net.minecraft.world.entity.Entity;

@Mixin(Entity.class)
public class EntityMixin
{
	@Inject(at = @At("HEAD"), method = "isInRain", cancellable = true)
	private void isInRainHook(CallbackInfoReturnable<Boolean> callback)
	{
		Entity me = (Entity) (Object) this;
		long rainTime = me.getData(RediscoveredAttachmentTypes.RAIN_TIME);
		if (rainTime != 0 && me.level().getGameTime() - rainTime < 20)
			callback.setReturnValue(true);
	}
}
