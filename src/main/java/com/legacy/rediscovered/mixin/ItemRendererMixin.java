package com.legacy.rediscovered.mixin;

import java.util.Optional;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.rediscovered.client.RediscoveredClientEvents;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;

@Mixin(ItemRenderer.class)
public class ItemRendererMixin
{
	@Inject(at = @At("tail"), method = "render")
	private void renderHook(ItemStack pItemStack, ItemDisplayContext pDisplayContext, boolean pLeftHand, PoseStack pPoseStack, MultiBufferSource pBuffer, int pCombinedLight, int pCombinedOverlay, BakedModel pModel, CallbackInfo callback)
	{
		Optional<AttachedItem> attachedItem = AttachedItem.get(pItemStack);
		if (attachedItem.isPresent())
		{
			ItemStack attached = attachedItem.get().getAttached();
			if (attached.is(RediscoveredTags.Items.QUIVERS))
			{
				Optional<ResourceKey<Item>> key = attached.getItemHolder().unwrapKey();
				if (key.isPresent())
				{
					ItemRenderer me = (ItemRenderer) (Object) this;
					ResourceLocation modelID = RediscoveredClientEvents.QUIVER_ITEM_MODEL_IDS.computeIfAbsent(key.get().location(), r -> r.withPrefix("item/").withSuffix("_chestplate"));
					BakedModel quiverModel = me.getItemModelShaper().getModelManager().getModel(modelID);
					me.render(attached, pDisplayContext, pLeftHand, pPoseStack, pBuffer, pCombinedLight, pCombinedOverlay, quiverModel);
				}
			}
		}
	}
}
