package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.phys.Vec3;

@Mixin(PathNavigation.class)
public interface PathNavigationAccessor
{
	/**
	 * Protected, cannot be an AT
	 */
	@Invoker("isClearForMovementBetween")
	public static boolean rediscovered$isClearForMovementBetween(Mob pMob, Vec3 pPos1, Vec3 pPos2, boolean pAllowSwimming)
	{
		throw new AssertionError();
	}
}