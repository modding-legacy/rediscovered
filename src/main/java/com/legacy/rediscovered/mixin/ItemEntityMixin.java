package com.legacy.rediscovered.mixin;

import java.util.Optional;
import java.util.stream.Stream;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.item.util.AttachedItem;

import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemUtils;

@Mixin(ItemEntity.class)
public class ItemEntityMixin
{
	// Called right before the item is destroyed, to drop the attached item
	@Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/item/ItemEntity;discard()V"), method = "hurt(Lnet/minecraft/world/damagesource/DamageSource;F)Z")
	private void onHurt(CallbackInfoReturnable<Boolean> callback)
	{
		ItemEntity me = (ItemEntity) (Object) this;
		Optional<AttachedItem> attached = AttachedItem.get(me.getItem());
		if (attached.isPresent())
		{
			ItemUtils.onContainerDestroyed(me, Stream.of(attached.get().getAttached().copy()));
		}
	}
}
