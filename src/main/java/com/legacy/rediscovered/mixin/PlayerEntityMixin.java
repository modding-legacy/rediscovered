package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;

import net.minecraft.world.entity.player.Player;

@Mixin(Player.class)
public class PlayerEntityMixin
{
	@Inject(at = @At("HEAD"), method = "wantsToStopRiding()Z", cancellable = true)
	private void wantsToStopRiding(CallbackInfoReturnable<Boolean> callback)
	{
		Player player = (Player) (Object) this;

		if (player.isPassenger() && player.getRootVehicle()instanceof RedDragonOffspringEntity dragon && (dragon.dismountDelay > 0 || !dragon.onGround()))
			callback.setReturnValue(false);
	}
}
