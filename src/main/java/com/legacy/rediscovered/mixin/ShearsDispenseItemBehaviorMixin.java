package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.event.RediscoveredEvents;

import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.core.dispenser.ShearsDispenseItemBehavior;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.DispenserBlock;

@Mixin(ShearsDispenseItemBehavior.class)
public class ShearsDispenseItemBehaviorMixin
{
	@Inject(at = @At("TAIL"), method = "execute")
	private void executeHook(BlockSource pSource, ItemStack pStack, CallbackInfoReturnable<ItemStack> callback)
	{
		var me = (ShearsDispenseItemBehavior) (Object) this;
		if (!me.isSuccess()) // If all other actions failed, basically run again with our stuff
		{
			ServerLevel level = pSource.level();
			BlockPos shearPos = pSource.pos().relative(pSource.state().getValue(DispenserBlock.FACING));
			me.setSuccess(RediscoveredEvents.shearDoublePlant(level, level.getBlockState(shearPos), shearPos, pStack, null));
			if (me.isSuccess() && pStack.hurt(1, level.getRandom(), (ServerPlayer) null))
			{
				pStack.setCount(0);
			}
		}
	}
}
