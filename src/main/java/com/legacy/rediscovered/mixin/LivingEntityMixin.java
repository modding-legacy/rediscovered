package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.registry.RediscoveredAttributes;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;

@Mixin(LivingEntity.class)
public class LivingEntityMixin
{
	@Inject(at = @At("RETURN"), method = "createLivingAttributes()Lnet/minecraft/world/entity/ai/attributes/AttributeSupplier$Builder;")
	private static void addAttributes(CallbackInfoReturnable<AttributeSupplier.Builder> callback)
	{
		callback.getReturnValue().add(RediscoveredAttributes.UNDEAD_DAMAGE_SCALING.get()).add(RediscoveredAttributes.CRIMSON_VEIL_DAMAGE_SCALING.get()).add(RediscoveredAttributes.EXPLOSION_RESISTANCE.get()).add(RediscoveredAttributes.FIRE_RESISTANCE.get());
	}
}
