package com.legacy.rediscovered.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.rediscovered.registry.RediscoveredAttributes;

import net.minecraft.world.entity.monster.hoglin.Hoglin;

@Mixin(Hoglin.class)
public class HoglinMixin
{
	@Inject(at = @At("HEAD"), method = "isConverting()Z", cancellable = true)
	private void isConverting(CallbackInfoReturnable<Boolean> callback)
	{
		if (RediscoveredAttributes.isConversionImmune((Hoglin) (Object) this))
			callback.setReturnValue(false);
	}
}
