package com.legacy.rediscovered.data_handler;

import java.util.EnumMap;
import java.util.List;
import java.util.Optional;

import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.registry.RediscoveredDataHandlers;
import com.legacy.structure_gel.api.data_handler.handlers.DataHandler;
import com.legacy.structure_gel.api.data_handler.handlers.ProbabilityDataHandler;
import com.legacy.structure_gel.api.data_handler.parsing.DataMap;
import com.legacy.structure_gel.api.data_handler.parsing.DataParser;

import net.minecraft.Util;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.DyeItem;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.Vec3;

public class TailorArmorStandDataHandler extends ProbabilityDataHandler<TailorArmorStandDataHandler>
{
	private static final DataParser PARSER = DataParser.of(RediscoveredDataHandlers.TAILOR_ARMOR_STAND.get(), parser ->
	{
		parser.add("spawn_chance", 1.0F, 0.0F, 1.0F);
	});

	public TailorArmorStandDataHandler(DataMap data)
	{
		super(data, "spawn_chance");
	}

	public static DataParser parser()
	{
		return PARSER;
	}

	@Override
	protected void handle(DataHandler.Context context)
	{
		WorldGenLevel level = context.getLevel();
		BlockState dataHandler = context.getDataHandlerState();
		Vec3 pos = context.getPos();
		RandomSource rand = context.getRandom();

		ArmorStand armorStand = new ArmorStand(level.getLevel(), pos.x, pos.y, pos.z);
		Direction facing = dataHandler.hasProperty(BlockStateProperties.FACING) ? dataHandler.getValue(BlockStateProperties.FACING) : Direction.SOUTH;
		float yRot = facing.getAxis() != Direction.Axis.Y ? facing.toYRot() : armorStand.getYRot();
		armorStand.moveTo(pos.x(), pos.y(), pos.z(), yRot, armorStand.getXRot());

		DyeColor[] colors = new DyeColor[] { DyeColor.PURPLE, DyeColor.MAGENTA, DyeColor.RED, DyeColor.PINK, DyeColor.BLUE, DyeColor.CYAN, DyeColor.LIGHT_GRAY, DyeColor.LIGHT_BLUE, DyeColor.YELLOW };
		EnumMap<EquipmentSlot, TagKey<Item>> armorOptions = new EnumMap<>(EquipmentSlot.class);
		armorOptions.put(EquipmentSlot.HEAD, RediscoveredTags.Items.TAILOR_ARMOR_STAND_HELMETS);
		armorOptions.put(EquipmentSlot.CHEST, RediscoveredTags.Items.TAILOR_ARMOR_STAND_CHESTPLATES);
		armorOptions.put(EquipmentSlot.LEGS, RediscoveredTags.Items.TAILOR_ARMOR_STAND_LEGGINGS);
		armorOptions.put(EquipmentSlot.FEET, RediscoveredTags.Items.TAILOR_ARMOR_STAND_BOOTS);

		boolean skippedAnItem = false;
		for (var entry : Util.toShuffledList(armorOptions.entrySet().stream(), rand))
		{
			if (!skippedAnItem && rand.nextFloat() > 0.8F)
			{
				skippedAnItem = true;
				continue;
			}

			Optional<Holder<Item>> item = RediscoveredUtil.getRandomItem(entry.getValue(), level, rand);
			if (item.isPresent())
			{
				ItemStack stack = item.get().value().getDefaultInstance();
				if (stack.getItem() instanceof DyeableLeatherItem dyeable && rand.nextFloat() < 0.9F)
				{
					List<DyeItem> dyes = List.of(DyeItem.byColor(Util.getRandom(colors, rand)), DyeItem.byColor(Util.getRandom(colors, rand)));
					stack = DyeableLeatherItem.dyeArmor(stack, dyes);
				}

				armorStand.setItemSlot(entry.getKey(), stack);
			}
		}

		level.addFreshEntityWithPassengers(armorStand);
	}
}
