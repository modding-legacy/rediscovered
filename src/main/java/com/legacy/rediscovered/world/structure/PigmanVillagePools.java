package com.legacy.rediscovered.world.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class PigmanVillagePools
{
	public static final ResourceKey<StructureTemplatePool> ROOT = ResourceKey.create(Registries.TEMPLATE_POOL, RediscoveredMod.locate("pigman_village/prison"));

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		Map<String, Integer> islands = new HashMap<>();
		Map<String, Integer> bridges = new HashMap<>();

		for (int i = 1; i <= 8; ++i)
			bridges.put("bridges/bridge_" + i, 1);

		// houses
		int houseWeight = 3;
		for (int i = 1; i <= 5; ++i)
			islands.put("houses/house_" + i, houseWeight);

		// job sites
		int jobWeight = 2;
		for (String s : List.of("houses/metalworker", "houses/technician", "houses/bowyer", "houses/doctor", "houses/tailor"))
			islands.put(s, jobWeight);

		// misc islands
		Map<String, Integer> deadEndIslands = Map.of("islands/dead_end_1", 100, "islands/dead_end_2", 100, "islands/dead_end_3", 50, "islands/dead_end_4", 10, "islands/dead_end_5", 1);

		int baseIslandWeight = 3;
		int biasedIslandWeight = 4;
		for (var entry : Map.of("islands/archer_tower_1", baseIslandWeight, "islands/cross_1", biasedIslandWeight, "islands/i_1", baseIslandWeight, "islands/l_1", baseIslandWeight, "islands/t_1", biasedIslandWeight).entrySet())
			islands.put(entry.getKey(), entry.getValue());

		int decorIslandWeight = 1;
		islands.put("islands/stall_0", decorIslandWeight);
		islands.put("islands/stall_1", decorIslandWeight);
		islands.put("islands/waterfall_0", decorIslandWeight);
		islands.put("islands/tunnel_0", decorIslandWeight);
		islands.put("islands/park_0", decorIslandWeight);
		
		JigsawRegistryHelper registry = new JigsawRegistryHelper(RediscoveredMod.MODID, "pigman_village/", bootstrap);
		final String fallback = "dead_end_island";
		registry.registerBuilder().pools(registry.poolBuilder().names(deadEndIslands).maintainWater(false).build()).register(fallback);
		registry.registerBuilder().pools(registry.poolBuilder().names("prison/prison_1").maintainWater(false).build()).register("prison");
		registry.registerBuilder().pools(registry.poolBuilder().names("prison/bridge_1").maintainWater(false).build()).register("prison_bridge");
		registry.registerBuilder().pools(registry.poolBuilder().names(islands).maintainWater(false)).fallback(fallback).register("island");
		registry.registerBuilder().pools(registry.poolBuilder().names(bridges).maintainWater(false).build()).fallback(fallback).register("bridge");
		registry.registerBuilder().pools(registry.poolBuilder().names("town_centers/center_1").maintainWater(false).build()).register("town_center");
		
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("islands/lower/lower_", 4))).register("lower_island");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("islands/lower/connection_", 3))).register("lower_island_connection");
	}
	
	private static String[] indexed(String prefix, int maxVal)
	{
		List<String> list = new ArrayList<>(maxVal + 1);
		for (int i = 0; i <= maxVal; i++)
			list.add(prefix + i);
		return list.toArray(String[]::new);
	}
}
