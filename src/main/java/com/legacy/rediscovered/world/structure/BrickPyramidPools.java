package com.legacy.rediscovered.world.structure;

import java.util.ArrayList;
import java.util.List;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.world.structure.pool_elements.BrickPyramidIslandPoolElement;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class BrickPyramidPools
{
	public static final ResourceKey<StructureTemplatePool> ROOT = ResourceKey.create(Registries.TEMPLATE_POOL, RediscoveredMod.locate("brick_pyramid/core"));

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{

		JigsawRegistryHelper registry = new JigsawRegistryHelper(RediscoveredMod.MODID, "brick_pyramid/", bootstrap);
		registry.registerBuilder().pools(registry.poolBuilder().names("core").maintainWater(false).build()).register("core");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("edge_", 2)).maintainWater(false).build()).register("edge");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("edge_entrance_", 2)).maintainWater(false).build()).register("edge_entrance");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("corner_", 2)).maintainWater(false).build()).register("corner");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("cap_", 1)).maintainWater(false).build()).register("cap");

		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("mini_pylon_", 4)).maintainWater(false).build()).register("mini_pylon");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("top_pylon_", 3)).maintainWater(false).build()).register("top_pylon");
		
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("center_room_", 3)).maintainWater(false).build()).register("center_room");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("side_room_", 4)).maintainWater(false).build()).register("side_room");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("front_room_", 3)).maintainWater(false).build()).register("front_room");
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("back_room_", 3)).maintainWater(false).build()).register("back_room");
		
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("edge_room_", 3)).maintainWater(false).build()).register("edge_room");

		registry.registerBuilder().pools(List.of(Pair.of(projection -> new BrickPyramidIslandPoolElement(projection), 1))).register("island");
	}

	private static String[] indexed(String prefix, int maxVal)
	{
		List<String> list = new ArrayList<>(maxVal + 1);
		for (int i = 0; i <= maxVal; i++)
			list.add(prefix + i);
		return list.toArray(String[]::new);
	}

	private static String[] indexed_(String prefix, int maxVal)
	{
		return new String[] { prefix + "-1" };
	}
}
