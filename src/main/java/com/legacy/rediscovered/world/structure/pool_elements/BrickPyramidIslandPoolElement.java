package com.legacy.rediscovered.world.structure.pool_elements;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joml.SimplexNoise;

import com.google.common.collect.Lists;
import com.legacy.rediscovered.registry.RediscoveredPoolElementTypes;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.FrontAndTop;
import net.minecraft.core.SectionPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.JigsawBlock;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.JigsawBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElement;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElementType;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool.Projection;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate.StructureBlockInfo;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.minecraft.world.phys.Vec3;

public class BrickPyramidIslandPoolElement extends StructurePoolElement
{
	public static final Codec<BrickPyramidIslandPoolElement> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(projectionCodec()).apply(instance, BrickPyramidIslandPoolElement::new);
	});

	private final CompoundTag defaultJigsawNBT;

	public BrickPyramidIslandPoolElement(Projection projection)
	{
		super(projection);
		this.defaultJigsawNBT = this.fillDefaultJigsawNBT();
	}

	private CompoundTag fillDefaultJigsawNBT()
	{
		// See how FeaturePoolElement works for this
		CompoundTag tag = new CompoundTag();
		tag.putString("pool", "minecraft:empty");
		tag.putString("name", "minecraft:island_top");
		tag.putString("target", "minecraft:pyramid_bottom");
		tag.putString("final_state", "minecraft:air");
		tag.putString("joint", JigsawBlockEntity.JointType.ROLLABLE.getSerializedName());
		return tag;
	}

	@Override
	public List<StructureBlockInfo> getShuffledJigsawBlocks(StructureTemplateManager structureTemplateManager, BlockPos pos, Rotation rotation, RandomSource random)
	{
		List<StructureTemplate.StructureBlockInfo> list = Lists.newArrayList();
		list.add(new StructureTemplate.StructureBlockInfo(pos.offset(0, this.getSize(structureTemplateManager, rotation).getY(), 0), Blocks.JIGSAW.defaultBlockState().setValue(JigsawBlock.ORIENTATION, FrontAndTop.fromFrontAndTop(Direction.UP, Direction.SOUTH)), this.defaultJigsawNBT));
		return list;
	}

	@Override
	public Vec3i getSize(StructureTemplateManager structureTemplateManager, Rotation rotation)
	{
		int xz = 166;
		int y = 66;
		return new Vec3i(xz, y, xz);
	}

	@Override
	public BoundingBox getBoundingBox(StructureTemplateManager structureTemplateManager, BlockPos pos, Rotation rotation)
	{
		Vec3i size = this.getSize(structureTemplateManager, rotation);
		int dx = size.getX() / 2;
		int dy = size.getY() / 2;
		int dz = size.getZ() / 2;
		return BoundingBox.fromCorners(pos.offset(-dx, -dy, -dz), pos.offset(dx, dy, dz));
	}

	@Override
	public boolean place(StructureTemplateManager structureTemplateManager, WorldGenLevel level, StructureManager structureManager, ChunkGenerator chunkGen, BlockPos pos, BlockPos structureOrigin, Rotation rotation, BoundingBox chunkBounds, RandomSource rand, boolean pKeepJigsaws)
	{
		BlockState baseMaterial = Blocks.STONE.defaultBlockState();
		BlockState topMaterial = Blocks.GRASS_BLOCK.defaultBlockState();
		BlockState soilMaterial = Blocks.DIRT.defaultBlockState();

		// Noise scaling
		float horizontalNoiseScale = 4F;
		float verticalNoiseScale = 6F;
		// How blobby the island is
		int deltaDist = 7;
		// How cubic the blob should be. Used to get closer to the beta terrain shape
		float dicingAmount = 0.3F;

		BoundingBox bb = this.getBoundingBox(structureTemplateManager, pos, rotation);
		int maxDist = bb.getXSpan() / 2 - deltaDist;
		Vec3 originVec = structureOrigin.getCenter(); // Center position
		Set<BlockPos> positions = new HashSet<>();
		for (int x = chunkBounds.minX(); x <= chunkBounds.maxX(); x++)
		{
			for (int z = chunkBounds.minZ(); z <= chunkBounds.maxZ(); z++)
			{
				float heightScale = 0.03F;
				int maxY = bb.maxY() + (int) ((SimplexNoise.noise(x * heightScale, z * heightScale) + 1) * 2);

				for (int y = bb.minY(); y <= maxY; y++)
				{
					BlockPos placePos = new BlockPos(x, y, z);
					Vec3 vec = placePos.getCenter();
					int dicing = 9;
					Vec3 dicedVec8 = new BlockPos(x / dicing * dicing, y / dicing * dicing, z / dicing * dicing).getCenter();

					// Get the actual distance between the point and structureOrigin. If it's beyond the max dist, we can skip it to help compute time
					float dist = (float) vec.distanceTo(originVec);
					if (dist > maxDist + deltaDist)
						continue;

					// Get the noise value based on the normal vector from pos to origin, offset by structureOrigin
					Vec3[] vecs = new Vec3[] { vec, dicedVec8 };
					float[] noises = new float[vecs.length];
					for (int i = 0; i < vecs.length; i++)
					{
						Vec3 v = vecs[i];
						Vec3 angle = originVec.subtract(v).normalize().add(originVec);
						noises[i] = SimplexNoise.noise((float) angle.x() * horizontalNoiseScale, (float) angle.y() * verticalNoiseScale, (float) angle.z() * horizontalNoiseScale);
					}

					float n = noises[0] * (1 - dicingAmount) + noises[1] * dicingAmount;

					// Scale the final result based on yLevel to round off the bottom
					float yScaling = (y - bb.minY()) / (float) bb.getYSpan();
					float scale;
					if (yScaling < 0.8335)
						scale = 1 - (float) Math.pow(1 - yScaling, 5);
					else
						scale = (float) Math.sin(yScaling * 2.0);

					// The value dist is compared to, taking noise into account.
					float threshold = (maxDist + (deltaDist * n)) * scale;

					// Store the position as one to place
					if (dist < threshold)
					{
						positions.add(placePos);
					}
				}
			}
		}

		// Actually place the blocks
		if (positions.isEmpty())
			return false;

		ChunkAccess chunk = level.getChunk(new BlockPos(chunkBounds.minX(), chunkBounds.minY(), chunkBounds.minZ()));
		Heightmap surfaceHM = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.WORLD_SURFACE_WG);
		Heightmap oceanHM = chunk.getOrCreateHeightmapUnprimed(Heightmap.Types.OCEAN_FLOOR_WG);

		for (BlockPos placePos : positions)
		{
			BlockState state = baseMaterial;
			if (!positions.contains(placePos.above(1)))
				state = topMaterial;
			else if (!positions.contains(placePos.above(2)))
				state = soilMaterial;
			else if (!positions.contains(placePos.above(3)) && rand.nextBoolean())
				state = soilMaterial;

			level.setBlock(placePos, state, Block.UPDATE_CLIENTS);
			BlockPos hmPos = new BlockPos(SectionPos.sectionRelative(placePos.getX()), placePos.getY() + 1, SectionPos.sectionRelative(placePos.getZ()));
			surfaceHM.update(hmPos.getX(), hmPos.getY(), hmPos.getZ(), state);
			oceanHM.update(hmPos.getX(), hmPos.getY(), hmPos.getZ(), state);
		}

		return true;
	}

	@Override
	public StructurePoolElementType<?> getType()
	{
		return RediscoveredPoolElementTypes.BRICK_PYRAMID_ISLAND.get();
	}

	@Override
	public String toString()
	{
		return String.format("%s[projection=%s]", RediscoveredPoolElementTypes.BRICK_PYRAMID_ISLAND.getName(), this.getProjection().getSerializedName());
	}
}
