package com.legacy.rediscovered.world.structure.legacy;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.BeastBoyEntity;
import com.legacy.rediscovered.entity.BlackSteveEntity;
import com.legacy.rediscovered.entity.RanaEntity;
import com.legacy.rediscovered.entity.SteveEntity;
import com.legacy.rediscovered.entity.pigman.MeleePigmanEntity;
import com.legacy.rediscovered.entity.pigman.PigmanEntity;
import com.legacy.rediscovered.entity.pigman.RangedPigmanEntity;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.structure_gel.api.structure.GelTemplateStructurePiece;
import com.legacy.structure_gel.api.structure.processor.RemoveGelStructureProcessor;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.monster.Illusioner;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockIgnoreProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;

@Deprecated
public class LegacyPigmanVillagePieces
{
	private static final ResourceLocation north_west = locatePiece("north_west");
	private static final ResourceLocation north_west_bottom = locatePiece("north_west_bottom");
	private static final ResourceLocation north_east = locatePiece("north_east");
	private static final ResourceLocation north_east_bottom = locatePiece("north_east_bottom");

	private static final ResourceLocation south_west = locatePiece("south_west");
	private static final ResourceLocation south_west_bottom = locatePiece("south_west_bottom");
	private static final ResourceLocation south_east = locatePiece("south_east");
	private static final ResourceLocation south_east_bottom = locatePiece("south_east_bottom");

	public static void assemble(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder pieces, RandomSource random, RandomState randomState)
	{
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, north_west_bottom, pos, rotation));
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, north_west, pos.offset(0, 32, 0), rotation));
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, north_east_bottom, pos.offset(25, 0, 0), rotation));
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, north_east, pos.offset(25, 32, 0), rotation));

		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, south_west_bottom, pos.offset(0, 0, 26), rotation));
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, south_west, pos.offset(0, 32, 26), rotation));
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, south_east_bottom, pos.offset(25, 0, 26), rotation));
		pieces.addPiece(new LegacyPigmanVillagePieces.Piece(templateManager, south_east, pos.offset(25, 32, 26), rotation));
	}

	/*public static void assemble(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder pieces, RandomSource random, RandomState randomState)
	{
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, base, pos, rotation));
		for (int i = 1; i <= 4; i++)
			pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, segment, pos.above(8 * i), rotation));
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, segmentTop, pos.above(40), rotation));
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, towerTop, pos.offset(-6, 48, -6), rotation));
	
		int i = random.nextInt(3) + 2;
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, bridge, pos.offset(6, (i * 9) - 2 - i, -5), Rotation.COUNTERCLOCKWISE_90));
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, prisonTower, pos.offset(2, (i * 9) - 5 - i, -19), Rotation.COUNTERCLOCKWISE_90));
		i = random.nextInt(3) + 2;
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, bridge, pos.offset(-6, (i * 9) - 2 - i, 7), Rotation.CLOCKWISE_180));
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, studyTower, pos.offset(-19, (i * 9) - 5 - i, 2), Rotation.CLOCKWISE_180));
		i = random.nextInt(3) + 2;
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, bridge, pos.offset(6, (i * 9) - 2 - i, 19), Rotation.CLOCKWISE_90));
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, witchTower, pos.offset(2, (i * 9) - 5 - i, 23), Rotation.CLOCKWISE_90));
		i = random.nextInt(3) + 2;
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, bridge, pos.offset(18, (i * 9) - 2 - i, 7), Rotation.NONE));
		pieces.addPiece(new PigmanVillagePieces.Piece(templateManager, libraryVariants.get(random.nextInt(libraryVariants.size())), pos.offset(23, (i * 9) - 5 - i, 2), Rotation.NONE));
	}*/

	static ResourceLocation locatePiece(String location)
	{
		return RediscoveredMod.locate("village/small/village_" + location);
	}

	public static class Piece extends GelTemplateStructurePiece
	{
		/*public Piece(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			super(RediscoveredStructures.PIGMAN_VILLAGE.getPieceType().get(), 0, structureManager, name, Piece.getPlacementSettings(structureManager, name, pos, rotation), pos);
			this.rotation = rotation;
		}
		
		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(RediscoveredStructures.PIGMAN_VILLAGE.getPieceType().get(), nbt, context.structureTemplateManager(), name -> Piece.getPlacementSettings(context.structureTemplateManager(), name, new BlockPos(nbt.getInt("TPX"), nbt.getInt("TPY"), nbt.getInt("TPZ")), Rotation.valueOf(nbt.getString("Rot"))));
		}
		
		private static StructurePlaceSettings getPlacementSettings(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			Vec3i sizePos = structureManager.get(name).get().getSize();
			BlockPos centerPos = new BlockPos(sizePos.getX() / 2, 0, sizePos.getZ() / 2);
		
			StructurePlaceSettings placementSettings = new StructurePlaceSettings().setKeepLiquids(false).setRotation(rotation).setMirror(Mirror.NONE).setRotationPivot(centerPos);
			placementSettings.addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK);
			placementSettings.addProcessor(RemoveGelStructureProcessor.INSTANCE);
			placementSettings.addProcessor(new RandomBlockSwapProcessor(SkiesBlocks.blinding_stone_bricks, 0.1F, SkiesBlocks.glowing_blinding_stone_bricks));
		
			return placementSettings;
		}*/public Piece(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			super(RediscoveredStructures.PIGMAN_VILLAGE.getPieceType("main").get(), 0, structureManager, name, Piece.getPlacementSettings(structureManager, name, pos, rotation), pos);
			this.rotation = rotation;
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(RediscoveredStructures.PIGMAN_VILLAGE.getPieceType("main").get(), nbt, context.structureTemplateManager(), name -> Piece.getPlacementSettings(context.structureTemplateManager(), name, new BlockPos(nbt.getInt("TPX"), nbt.getInt("TPY"), nbt.getInt("TPZ")), Rotation.valueOf(nbt.getString("Rot"))));
		}

		private static StructurePlaceSettings getPlacementSettings(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			Vec3i sizePos = structureManager.get(name).get().getSize();
			BlockPos centerPos = new BlockPos(sizePos.getX() / 2, 0, sizePos.getZ() / 2);

			StructurePlaceSettings placementSettings = new StructurePlaceSettings().setKeepLiquids(false).setRotationPivot(centerPos).setRotation(rotation).setMirror(Mirror.NONE);
			placementSettings.addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK);
			placementSettings.addProcessor(RemoveGelStructureProcessor.INSTANCE);

			return placementSettings;
		}

		@Override
		protected void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox sbb)
		{
			if ("pigman".equals(key))
			{
				/*StructureStart start = level.getLevel().structureManager().getStructureAt(pos, RediscoveredStructures.PIGMAN_VILLAGE.getStructure().get(level));
				BlockPos startPos = start.isValid() ? start.getChunkPos().getWorldPosition() : pos;*/

				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				PigmanEntity entity = RediscoveredEntityTypes.PIGMAN.create(level.getLevel());
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);

				/*entity.setOrigins(pos, pos.atY(pos.getY()));
				entity.restrictDistance = 45;*/

				level.addFreshEntity(entity);
			}
			else if ("nitwit".equals(key))
			{
				if (rand.nextBoolean() && level.getDifficulty() != Difficulty.PEACEFUL)
				{
					level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
					Illusioner entity = EntityType.ILLUSIONER.create(level.getLevel());
					entity.setPersistenceRequired();
					entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
					level.addFreshEntity(entity);
				}
				else
				{
					level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
					Villager entity = EntityType.VILLAGER.create(level.getLevel());
					entity.setVillagerData(entity.getVillagerData().setType(VillagerType.PLAINS).setProfession(VillagerProfession.NITWIT));
					entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
					level.addFreshEntity(entity);
				}
			}
			else if ("melee_pigman".equals(key))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				MeleePigmanEntity entity = RediscoveredEntityTypes.MELEE_PIGMAN.create(level.getLevel());
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				level.addFreshEntity(entity);
			}
			else if ("ranged_pigman".equals(key))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				RangedPigmanEntity entity = RediscoveredEntityTypes.RANGED_PIGMAN.create(level.getLevel());
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				level.addFreshEntity(entity);
			}
			else if ("steve".equals(key))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				SteveEntity entity = RediscoveredEntityTypes.STEVE.create(level.getLevel());
				entity.setPos(pos.getX() + 1.5D, pos.getY(), pos.getZ() + 0.5D);
				level.addFreshEntity(entity);
			}
			else if ("rana".equals(key))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				RanaEntity entity = RediscoveredEntityTypes.RANA.create(level.getLevel());
				entity.setPos(pos.getX() + 1.5D, pos.getY(), pos.getZ() + 0.5D);
				level.addFreshEntity(entity);
			}
			else if ("black_steve".equals(key))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				BlackSteveEntity entity = RediscoveredEntityTypes.BLACK_STEVE.create(level.getLevel());
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				level.addFreshEntity(entity);
			}
			else if ("beast_boy".equals(key))
			{
				level.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
				BeastBoyEntity entity = RediscoveredEntityTypes.BEAST_BOY.create(level.getLevel());
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				level.addFreshEntity(entity);
			}
			else if ("tower_chest".equals(key))
			{
				level.setBlock(pos, Blocks.CHEST.defaultBlockState().setValue(ChestBlock.FACING, Direction.EAST), 3);

				ChestBlockEntity chest = (ChestBlockEntity) level.getBlockEntity(pos);

				for (int i = 0; i < 15; ++i)
					chest.setItem(rand.nextInt(chest.getContainerSize()), pickCheckTowerLootItem(rand));
			}
			else if ("smith_chest".equals(key))
			{
				level.setBlock(pos, Blocks.CHEST.defaultBlockState().setValue(ChestBlock.FACING, Direction.EAST), 3);

				ChestBlockEntity chest = (ChestBlockEntity) level.getBlockEntity(pos);

				for (int i = 0; i < 15; ++i)
					chest.setItem(rand.nextInt(chest.getContainerSize()), pickCheckForgeLootItem(rand));
			}
		}

	}
	
	@Deprecated
	public static ItemStack pickCheckTowerLootItem(RandomSource random)
	{
		int i = random.nextInt(9);
		if (i == 0)
		{
			return new ItemStack(Items.GOLD_INGOT);
		}
		if (i == 1)
		{
			return new ItemStack(Items.IRON_INGOT, random.nextInt(4) + 1);
		}
		if (i == 2)
		{
			return new ItemStack(Items.CARROT, random.nextInt(3) + 1);
		}
		if (i == 3 && random.nextInt(1) == 0)
		{
			return new ItemStack(Items.BOW);
		}
		if (i == 4 && random.nextInt(1) == 0)
		{
			return new ItemStack(RediscoveredItems.quiver);
		}
		if (i == 5 && random.nextInt(2) == 0)
		{
			return new ItemStack(Items.GOLDEN_CARROT, random.nextInt(4) + 1);
		}
		if (i == 6 && random.nextInt(1) == 0)
		{
			return AttachedItem.attachItem(Items.LEATHER_CHESTPLATE.getDefaultInstance(), RediscoveredItems.quiver.getDefaultInstance());
		}
		if (i == 7 && random.nextInt(40) == 0)
		{
			return new ItemStack(Items.DIAMOND);
		}
		if (i == 8 && random.nextInt(2) == 0)
		{
			return new ItemStack(Items.GOLD_NUGGET, random.nextInt(4) + 1);
		}
		else
		{
			return new ItemStack(Items.STICK);
		}
	}

	@Deprecated
	public static ItemStack pickCheckForgeLootItem(RandomSource random)
	{
		int i = random.nextInt(10);

		if (i == 0)
		{
			return new ItemStack(Items.GOLD_INGOT);
		}

		if (i == 1)
		{
			return new ItemStack(Items.IRON_INGOT, random.nextInt(4) + 1);
		}

		if (i == 2)
		{
			return new ItemStack(Items.CARROT, random.nextInt(3) + 1);
		}

		if (i == 3 && random.nextInt(1) == 0)
		{
			return new ItemStack(RediscoveredBlocks.gear, random.nextInt(4) + 1);
		}

		if (i == 4)
		{
			return new ItemStack(Items.GUNPOWDER, random.nextInt(4) + 1);
		}

		if (i == 5 && random.nextInt(2) == 0)
		{
			return new ItemStack(Items.GOLDEN_CARROT, random.nextInt(4) + 1);
		}

		if (i == 6)
		{
			return new ItemStack(Items.BUCKET);
		}

		if (i == 7 && random.nextInt(50) == 0)
		{
			return new ItemStack(Items.DIAMOND);
		}

		if (i == 8 && random.nextInt(2) == 0)
		{
			return new ItemStack(Items.GOLD_NUGGET, random.nextInt(4) + 1);
		}

		if (i == 9 && random.nextInt(5) == 0)
		{
			return new ItemStack(RediscoveredItems.music_disc_calm4);
		}

		return new ItemStack(Items.STICK);
	}
}
