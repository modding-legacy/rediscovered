package com.legacy.rediscovered.world.structure;

import java.util.Optional;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.data.RediscoveredLootProv;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.dragon.DragonPylonEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonBossEntity;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredJigsawTypes;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.RandomizableContainer;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CandleBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.DecoratedPotBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;
import net.minecraft.world.phys.Vec3;

public class BrickPyramidStructure
{
	public static class Capability implements JigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final Codec<Capability> CODEC = Codec.unit(INSTANCE);

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return RediscoveredJigsawTypes.BRICK_PYRAMID.get();
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag tag)
		{
			super(context, tag);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox bounds)
		{
			if ("mini_pylon".equals(key))
			{
				this.setAir(level, pos);

				level.setBlock(pos, RediscoveredBlocks.mini_dragon_pylon.defaultBlockState(), Block.UPDATE_ALL);
			}
			else if ("pylon".equals(key))
			{
				this.setAir(level, pos);

				Vec3 p = Vec3.atCenterOf(pos);

				RedDragonBossEntity redDragon = RediscoveredEntityTypes.RED_DRAGON_BOSS.create(level.getLevel());
				redDragon.setPos(p.add(0, 30, 0));
				redDragon.homePos = pos;
				redDragon.setPylonTime(20);
				level.addFreshEntity(redDragon);

				DragonPylonEntity pylon = new DragonPylonEntity(level.getLevel(), p.x(), p.y(), p.z());
				pylon.setNoGravity(true);
				pylon.setAttachedDragonUUID(redDragon.getUUID());
				level.addFreshEntity(pylon);

				redDragon.setPylonTime(60);
			}
			else if ("spawner".equals(key))
			{
				this.setAir(level, pos);

				level.setBlock(pos, Blocks.SPAWNER.defaultBlockState(), Block.UPDATE_ALL);
				level.getBlockEntity(pos, BlockEntityType.MOB_SPAWNER).ifPresent(spawner ->
				{
					// TODO 1.20.? Add breeze as an option
					Optional<Holder<EntityType<?>>> randomEntity = RediscoveredUtil.getRandom(Registries.ENTITY_TYPE, RediscoveredTags.Entities.BRICK_PYRAMID_SPAWNER, level.registryAccess(), rand);
					spawner.setEntityId(randomEntity.isPresent() ? randomEntity.get().value() : EntityType.ZOMBIE, rand);
				});
			}
			else if ("decorated_pot".equals(key))
			{
				this.setAir(level, pos);

				level.setBlock(pos, Blocks.DECORATED_POT.defaultBlockState(), Block.UPDATE_ALL);
				level.getBlockEntity(pos, BlockEntityType.DECORATED_POT).ifPresent(pot ->
				{
					int decorationCount = 4;
					Item[] items = new Item[decorationCount];
					Item brick = Items.BRICK;
					for (int i = 0; i < decorationCount; i++)
					{
						items[i] = rand.nextFloat() < 0.33 ? RediscoveredUtil.getRandomItem(RediscoveredTags.Items.BRICK_PYRAMID_POTTERY_SHERDS, level, rand).map(Holder::value).orElse(brick) : brick;
					}

					DecoratedPotBlockEntity.Decorations decorations = new DecoratedPotBlockEntity.Decorations(items[0], items[1], items[2], items[3]);
					ItemStack potItem = Items.DECORATED_POT.getDefaultInstance();
					CompoundTag blockEntityTag = decorations.save(new CompoundTag());
					BlockItem.setBlockEntityData(potItem, BlockEntityType.DECORATED_POT, blockEntityTag);
					pot.setFromItem(potItem);

					RandomizableContainer.setBlockEntityLootTable(level, rand, pos, RediscoveredLootProv.BRICK_PYRAMID_POT);
				});
			}
			else
				RediscoveredMod.LOGGER.error("Brick Pyramid data marker was not handled! Key: {}", key);
		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			if (originalState.is(Blocks.MUD_BRICKS) && rand.nextFloat() < 0.1)
			{
				return Blocks.PACKED_MUD.defaultBlockState();
			}
			else if (originalState.is(RediscoveredBlocks.brittle_mud_bricks) && rand.nextFloat() < 0.1)
			{
				return RediscoveredBlocks.brittle_packed_mud.defaultBlockState();
			}
			else if (originalState.is(Blocks.WHITE_CANDLE))
			{
				// Pick between red and black candle, and then set a random amount. The amount is biased towards 1-2 candles
				float f = rand.nextFloat();
				int count = f < 0.4 ? 1 : f < 0.7 ? 2 : f < 0.85 ? 3 : 4;
				return (rand.nextFloat() < 0.3F ? Blocks.BLACK_CANDLE : Blocks.RED_CANDLE).defaultBlockState().setValue(CandleBlock.CANDLES, count);
			}
			else if (originalState.is(Blocks.ORANGE_CANDLE))
			{
				// Pick between red and black candle, and then set a random amount. The amount is biased towards 1-2 candles
				float f = rand.nextFloat();
				int count = f < 0.4 ? 1 : f < 0.7 ? 2 : f < 0.85 ? 3 : 4;
				return (rand.nextFloat() < 0.3F ? Blocks.BLACK_CANDLE : Blocks.RED_CANDLE).defaultBlockState().setValue(CandleBlock.CANDLES, count).setValue(CandleBlock.LIT, rand.nextFloat() < 0.75);
			}
			return originalState;
		}

		@Override
		public StructurePieceType getType()
		{
			return RediscoveredStructures.BRICK_PYRAMID.getPieceType().get();
		}
	}
}
