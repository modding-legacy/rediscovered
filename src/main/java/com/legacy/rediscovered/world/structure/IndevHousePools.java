package com.legacy.rediscovered.world.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class IndevHousePools
{
	public static final ResourceKey<StructureTemplatePool> DEFAULT = ResourceKey.create(Registries.TEMPLATE_POOL, RediscoveredMod.locate("indev_house/" + IndevHouseStructure.Type.DEFAULT));
	public static final ResourceKey<StructureTemplatePool> MOSSY = ResourceKey.create(Registries.TEMPLATE_POOL, RediscoveredMod.locate("indev_house/" + IndevHouseStructure.Type.MOSSY));
	public static final ResourceKey<StructureTemplatePool> CHERRY = ResourceKey.create(Registries.TEMPLATE_POOL, RediscoveredMod.locate("indev_house/" + IndevHouseStructure.Type.CHERRY));

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(RediscoveredMod.MODID, "indev_house/", bootstrap);

		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("indev", 3, IndevHouseStructure.Type.DEFAULT, 1))).register(DEFAULT);
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("indev", 3, IndevHouseStructure.Type.MOSSY, 1))).register(MOSSY);
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("indev", 3, IndevHouseStructure.Type.CHERRY, 1))).register(CHERRY);
	}

	private static String[] indexed(String prefix, int maxVal, IndevHouseStructure.Type type, int maxValsForType)
	{
		return Stream.concat(Stream.of(indexed(prefix, maxVal)), Stream.of(indexed(type.getSerializedName(), maxValsForType))).toArray(String[]::new);
	}

	private static String[] indexed(String prefix, int maxVal)
	{
		List<String> list = new ArrayList<>(maxVal + 1);
		for (int i = 0; i <= maxVal; i++)
			list.add(prefix + "_" + i);
		return list.toArray(String[]::new);
	}
}