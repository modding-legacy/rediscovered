package com.legacy.rediscovered.world.structure;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;

public class ObsidianWallStructure extends Structure
{
	public static final Codec<ObsidianWallStructure> CODEC = simpleCodec(ObsidianWallStructure::new);

	public ObsidianWallStructure(StructureSettings settings)
	{
		super(settings);
	}

	public Optional<Structure.GenerationStub> findGenerationPoint(Structure.GenerationContext context)
	{
		return onTopOfChunkCenter(context, Heightmap.Types.WORLD_SURFACE_WG, builder -> this.generatePieces(builder, context));
	}

	private void generatePieces(StructurePiecesBuilder builder, Structure.GenerationContext context)
	{
		ChunkPos chunkPos = context.chunkPos();
		WorldgenRandom rand = context.random();
		BlockPos pos = new BlockPos(chunkPos.getMinBlockX(), 90, chunkPos.getMinBlockZ());
		Rotation rotation = Rotation.getRandom(rand);
		ObsidianWallPieces.addPieces(context.structureTemplateManager(), pos, rotation, builder, rand);
	}

	@Override
	public StructureType<?> type()
	{
		return RediscoveredStructures.OBSIDIAN_WALL.getType();
	}
}
