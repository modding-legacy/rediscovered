package com.legacy.rediscovered.world.structure;

import java.util.Optional;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.BeastBoyEntity;
import com.legacy.rediscovered.entity.BlackSteveEntity;
import com.legacy.rediscovered.entity.RanaEntity;
import com.legacy.rediscovered.entity.SteveEntity;
import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;
import com.legacy.rediscovered.entity.pigman.PigmanEntity;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredJigsawTypes;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.GlobalPos;
import net.minecraft.core.Holder;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.RandomizableContainer;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Illusioner;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerType;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.StructureStart;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;

public class PigmanVillageStructure
{
	public static class Capability implements JigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final Codec<Capability> CODEC = Codec.unit(INSTANCE);

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return RediscoveredJigsawTypes.PIGMAN_VILLAGE.get();
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(context, nbt);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox bounds)
		{
			if ("pigman".equals(key))
			{
				// TODO Post-Beta: Search for center correctly
				StructureStart start = level.getLevel().structureManager().getStructureAt(pos, RediscoveredStructures.PIGMAN_VILLAGE.getStructure().get(level));
				BlockPos startPos = start.isValid() ? start.getChunkPos().getWorldPosition() : pos;
				/*startPos = startPos.north(40).east(26);
				startPos.rotate(this.getRotation());*/

				this.setAir(level, pos);
				PigmanEntity entity = RediscoveredEntityTypes.PIGMAN.create(level.getLevel());
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				entity.villageOrigin = GlobalPos.of(level.getLevel().dimension(), startPos.atY(pos.getY()));

				level.addFreshEntity(entity);
			}
			else if ("prisoner".equals(key))
			{
				if (rand.nextFloat() < 0.9F && level.getDifficulty() != Difficulty.PEACEFUL)
				{
					this.setAir(level, pos);
					Illusioner entity = EntityType.ILLUSIONER.create(level.getLevel());
					entity.setPersistenceRequired();
					entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
					entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);

					entity.getAttribute(Attributes.MAX_HEALTH).setBaseValue(50.0D);
					entity.setHealth(entity.getMaxHealth());

					level.addFreshEntity(entity);
				}
				else
				{
					this.setAir(level, pos);
					Villager entity = EntityType.VILLAGER.create(level.getLevel());
					entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
					entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
					entity.setVillagerData(entity.getVillagerData().setType(VillagerType.PLAINS).setProfession(VillagerProfession.NITWIT));
					level.addFreshEntity(entity);
				}
			}
			else if (key.startsWith("guard-"))
			{
				this.setAir(level, pos);
				GuardPigmanEntity entity = null;

				if (key.contains("melee"))
					entity = RediscoveredEntityTypes.MELEE_PIGMAN.create(level.getLevel());
				else if (key.contains("ranged"))
					entity = RediscoveredEntityTypes.RANGED_PIGMAN.create(level.getLevel());

				if (entity != null)
				{
					entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
					entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
					level.addFreshEntity(entity);
				}
				else
					RediscoveredMod.LOGGER.error("Pigman Village guard was not assigned properly! Key: {}", key);
			}
			else if ("steve".equals(key))
			{
				this.setAir(level, pos);
				SteveEntity entity = RediscoveredEntityTypes.STEVE.create(level.getLevel());
				entity.setPos(pos.getX() + 1.5D, pos.getY(), pos.getZ() + 0.5D);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				level.addFreshEntity(entity);
			}
			else if ("rana".equals(key))
			{
				this.setAir(level, pos);
				RanaEntity entity = RediscoveredEntityTypes.RANA.create(level.getLevel());
				entity.setPos(pos.getX() + 1.5D, pos.getY(), pos.getZ() + 0.5D);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				level.addFreshEntity(entity);
			}
			else if ("black_steve".equals(key))
			{
				this.setAir(level, pos);
				BlackSteveEntity entity = RediscoveredEntityTypes.BLACK_STEVE.create(level.getLevel());
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				level.addFreshEntity(entity);
			}
			else if ("beast_boy".equals(key))
			{
				this.setAir(level, pos);
				BeastBoyEntity entity = RediscoveredEntityTypes.BEAST_BOY.create(level.getLevel());
				entity.setPos(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, (SpawnGroupData) null, (CompoundTag) null);
				level.addFreshEntity(entity);
			}
			else if (key.startsWith("chest-"))
			{
				this.setAir(level, pos);
				ResourceLocation lootTable = RediscoveredMod.locate("chests/pigman_village/" + key.substring(key.indexOf('-') + 1));
				RandomizableContainer.setBlockEntityLootTable(level, rand, pos.below(), lootTable);
			}
			else
				RediscoveredMod.LOGGER.error("Pigman Village data marker was not handled! Key: {}", key);

		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			if (originalState.is(Blocks.DEAD_BUSH))
			{
				float f = rand.nextFloat();
				float flowerChance = 0.75F;
				if (f < flowerChance)
				{
					// 1% chance to be a rare flower
					Optional<Holder<Block>> flower = RediscoveredUtil.getRandomBlock(f <= flowerChance * 0.01F ? RediscoveredTags.Blocks.PIGMAN_VILLAGE_RARE_FLOWERS : RediscoveredTags.Blocks.PIGMAN_VILLAGE_FLOWERS, level, rand);
					if (flower.isPresent())
						return flower.get().value().defaultBlockState();
				}
				// 50% chance to be air or grass if not a flower
				return f < flowerChance + ((1.0F - flowerChance) / 2.0F) ? Blocks.AIR.defaultBlockState() : Blocks.SHORT_GRASS.defaultBlockState();
			}
			else if (originalState.is(BlockTags.FENCES) || originalState.is(Blocks.WATER))
			{
				if (!(level instanceof ServerLevel))
					level.getChunk(pos).markPosForPostprocessing(pos);
				return originalState;
			}
			else if (originalState.is(Blocks.COBBLED_DEEPSLATE))
			{
				return rand.nextBoolean() ? Blocks.STONE.defaultBlockState() : Blocks.DIRT.defaultBlockState();
			}

			return originalState;
		}

		@Override
		public StructurePieceType getType()
		{
			return RediscoveredStructures.PIGMAN_VILLAGE.getPieceType().get();
		}
	}
}
