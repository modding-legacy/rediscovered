package com.legacy.rediscovered.world.structure.legacy;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;

@Deprecated
public class LegacyPigmanVillageStructure extends Structure
{
	public static final Codec<LegacyPigmanVillageStructure> CODEC = Structure.simpleCodec(LegacyPigmanVillageStructure::new);

	public LegacyPigmanVillageStructure(Structure.StructureSettings settings)
	{
		super(settings);
	}

	private void generatePieces(StructurePiecesBuilder builder, GenerationContext context, int y)
	{
		ChunkPos chunkPos = context.chunkPos();
		Rotation rotation = Rotation.NONE/*Rotation.values()[context.random().nextInt(Rotation.values().length)]*/;

		LegacyPigmanVillagePieces.assemble(context.structureTemplateManager(), new BlockPos(chunkPos.x * 16 + 8, y, chunkPos.z * 16 + 8), rotation, builder, context.random(), context.randomState());
	}

	@Override
	public Optional<GenerationStub> findGenerationPoint(GenerationContext context)
	{
		ChunkPos chunkpos = context.chunkPos();
		BlockPos blockpos = new BlockPos(chunkpos.getMinBlockX(), 100 + context.random().nextInt(50), chunkpos.getMinBlockZ());
		return Optional.of(new GenerationStub(blockpos, (piecesBuilder) -> generatePieces(piecesBuilder, context, blockpos.getY())));

	}

	@Override
	public StructureType<?> type()
	{
		return RediscoveredStructures.PIGMAN_VILLAGE.getType();
	}
}