package com.legacy.rediscovered.world.structure;

import com.legacy.rediscovered.registry.RediscoveredJigsawTypes;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.structure_gel.api.structure.ExtendedJigsawStructure;
import com.legacy.structure_gel.api.structure.base.IModifyState;
import com.legacy.structure_gel.api.structure.base.IPieceBuilderModifier;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.Structure.GenerationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;

public class IndevHouseStructure
{
	public static record Capability(Type type) implements JigsawCapability
	{
		public static final Codec<Capability> CODEC = RecordCodecBuilder.create(instance -> instance.group(Type.CODEC.fieldOf("house_type").forGetter(Capability::type)).apply(instance, Capability::new));

		@Override
		public boolean canPlace(Structure.GenerationContext generationContext, BlockPos placementPos, ExtendedJigsawStructure.PlaceContext placeContext)
		{
			if (placeContext.projectStartToHeightmap().isEmpty())
				return true;

			var heightmap = placeContext.projectStartToHeightmap().get();
			var heightAccess = generationContext.heightAccessor();
			var rand = generationContext.randomState();
			var chunkGen = generationContext.chunkGenerator();
			int centerHeight = chunkGen.getBaseHeight(placementPos.getX(), placementPos.getZ(), heightmap, heightAccess, rand);
			if (centerHeight < heightAccess.getMinBuildHeight() + 4)
				return false;
			int r = 5;
			int maxVariance = 5;
			for (Direction dir : Direction.Plane.HORIZONTAL)
			{
				BlockPos p = placementPos.relative(dir, r).relative(dir.getClockWise(), r); // get a corner pos
				int cornerHeight = chunkGen.getBaseHeight(p.getX(), p.getZ(), heightmap, heightAccess, rand);
				if (Math.abs(centerHeight - cornerHeight) > maxVariance)
					return false;
			}
			return true;
		}

		@Override
		public void modifyPieceBuilder(StructurePiecesBuilder pieceBuilder, GenerationContext context)
		{
			IPieceBuilderModifier.getPieces(pieceBuilder).removeIf(piece -> piece.getBoundingBox().minY() < context.heightAccessor().getMinBuildHeight() + 4);
		}

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return RediscoveredJigsawTypes.INDEV_HOUSE.get();
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static enum Type implements StringRepresentable
	{
		DEFAULT("default"),
		MOSSY("mossy"),
		CHERRY("cherry");

		public static final Codec<Type> CODEC = StringRepresentable.fromEnum(Type::values);

		final String name;

		Type(String name)
		{
			this.name = name;
		}

		@Override
		public String getSerializedName()
		{
			return this.name;
		}

		@Override
		public String toString()
		{
			return this.name;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag tag)
		{
			super(context, tag);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox bounds)
		{

		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			var cap = this.getCapability(Capability.class);
			if (cap.isEmpty())
				return originalState;

			this.getElement();

			switch (cap.get().type)
			{
			case DEFAULT:
				originalState = toMossy(originalState, rand, 0.6);
				originalState = toCracked(originalState, rand, 0.2);
				originalState = gravelToStone(originalState, rand, 0.5);
				break;
			case MOSSY:
				originalState = toMossy(originalState, rand, 0.85);
				originalState = toCracked(originalState, rand, 0.2);
				originalState = gravelToStone(originalState, rand, 0.5);
				break;
			case CHERRY:
				originalState = toMossy(originalState, rand, 0.45);
				originalState = toCracked(originalState, rand, 0.3);
				originalState = gravelToStone(originalState, rand, 0.5);
				break;
			}
			return originalState;
		}

		private BlockState toMossy(BlockState state, RandomSource rand, double chance)
		{
			if (rand.nextFloat() > chance)
				return state;

			if (state.is(Blocks.COBBLESTONE))
				return Blocks.MOSSY_COBBLESTONE.defaultBlockState();
			if (state.is(Blocks.COBBLESTONE_STAIRS))
				return IModifyState.mergeStates(Blocks.MOSSY_COBBLESTONE_STAIRS.defaultBlockState(), state);
			if (state.is(Blocks.COBBLESTONE_SLAB))
				return IModifyState.mergeStates(Blocks.MOSSY_COBBLESTONE_SLAB.defaultBlockState(), state);
			if (state.is(Blocks.COBBLESTONE_WALL))
				return IModifyState.mergeStates(Blocks.MOSSY_COBBLESTONE_WALL.defaultBlockState(), state);

			if (state.is(Blocks.STONE_BRICKS))
				return Blocks.MOSSY_STONE_BRICKS.defaultBlockState();
			if (state.is(Blocks.STONE_BRICK_STAIRS))
				return IModifyState.mergeStates(Blocks.MOSSY_STONE_BRICK_STAIRS.defaultBlockState(), state);
			if (state.is(Blocks.STONE_BRICK_SLAB))
				return IModifyState.mergeStates(Blocks.MOSSY_STONE_BRICK_SLAB.defaultBlockState(), state);
			if (state.is(Blocks.STONE_BRICK_WALL))
				return IModifyState.mergeStates(Blocks.MOSSY_STONE_BRICK_WALL.defaultBlockState(), state);

			return state;
		}

		private BlockState toCracked(BlockState state, RandomSource rand, double chance)
		{
			if (rand.nextFloat() > chance)
				return state;

			if (state.is(Blocks.STONE_BRICKS))
				return Blocks.CRACKED_STONE_BRICKS.defaultBlockState();

			return state;
		}

		private BlockState gravelToStone(BlockState state, RandomSource rand, double chance)
		{
			if (rand.nextFloat() > chance)
				return state;

			if (state.is(Blocks.GRAVEL))
				return Blocks.COBBLESTONE.defaultBlockState();

			return state;
		}

		@Override
		public StructurePieceType getType()
		{
			return RediscoveredStructures.INDEV_HOUSE.getPieceType().get();
		}
	}
}