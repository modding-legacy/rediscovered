package com.legacy.rediscovered.world.structure;

import java.util.ArrayList;
import java.util.List;

import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredStructures;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.FloatTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.StructurePiece;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;
import net.minecraft.world.phys.Vec2;
import net.minecraft.world.phys.Vec3;

public class ObsidianWallPieces
{
	public static void addPieces(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder builder, WorldgenRandom rand)
	{
		int minBounds = 25;
		int maxBounds = 40;
		BoundingBox bounds = BoundingBox.fromCorners(pos, pos.offset(rand.nextIntBetweenInclusive(minBounds, maxBounds), 10, rand.nextIntBetweenInclusive(minBounds, maxBounds)));
		Vec2 direction = new Vec2((rand.nextFloat() - 0.5F) * 2.0F, (rand.nextFloat() - 0.5F) * 2.0F).normalized();
		float deltaAngle = (rand.nextFloat() - 0.5F) * 360.0F;
		builder.addPiece(new Piece(bounds, direction, deltaAngle));
	}

	public static class Piece extends StructurePiece
	{
		final Vec2 direction;
		final float deltaAngle;

		protected Piece(BoundingBox boundingBox, Vec2 direction, float deltaAngle)
		{
			super(RediscoveredStructures.OBSIDIAN_WALL.getPieceType().get(), 0, boundingBox);
			this.direction = direction;
			this.deltaAngle = deltaAngle;
		}

		private static final String DIRECTION_KEY = "direction", DELTA_ANGLE_KEY = "delta_angle";

		public Piece(StructurePieceSerializationContext context, CompoundTag tag)
		{
			super(RediscoveredStructures.OBSIDIAN_WALL.getPieceType().get(), tag);

			ListTag directionTag = tag.getList(DIRECTION_KEY, Tag.TAG_FLOAT);
			this.direction = directionTag.size() >= 2 ? new Vec2(directionTag.getFloat(0), directionTag.getFloat(1)) : new Vec2(1, 0);
			this.deltaAngle = tag.getFloat(DELTA_ANGLE_KEY);
		}

		@Override
		protected void addAdditionalSaveData(StructurePieceSerializationContext context, CompoundTag tag)
		{
			ListTag directionTag = new ListTag();
			directionTag.add(FloatTag.valueOf(this.direction.x));
			directionTag.add(FloatTag.valueOf(this.direction.y));
			tag.put(DIRECTION_KEY, directionTag);

			tag.putFloat(DELTA_ANGLE_KEY, this.deltaAngle);
		}

		@Override
		public void postProcess(WorldGenLevel level, StructureManager structureManager, ChunkGenerator chunkGen, RandomSource rand, BoundingBox chunkBounds, ChunkPos chunkPos, BlockPos pos)
		{
			List<Vec3> positions = new ArrayList<>();
			Vec3 center = this.boundingBox.getCenter().atY(this.boundingBox.minY()).getCenter();
			positions.add(center);
			Vec3 currentPos = center;
			Vec2 dir = this.direction;
			float s = 0.7F;
			while (this.boundingBox.isInside(BlockPos.containing(currentPos)))
			{
				currentPos = currentPos.add(dir.x, 0, dir.y);
				positions.add(currentPos);
				dir = new Vec2(dir.x + (rand.nextFloat() - 0.5F) * s, dir.y + (rand.nextFloat() - 0.5F) * s).normalized();
			}
			currentPos = center;
			dir = this.direction;
			while (this.boundingBox.isInside(BlockPos.containing(currentPos)))
			{
				currentPos = currentPos.subtract(dir.x, 0, dir.y);
				positions.add(currentPos);
				dir = new Vec2(dir.x + (rand.nextFloat() - 0.5F) * s, dir.y + (rand.nextFloat() - 0.5F) * s).normalized();
			}

			for (Vec3 p : positions)
			{
				BlockPos linePos = BlockPos.containing(p);
				if (chunkBounds.isInside(linePos) && this.boundingBox.isInside(linePos))
				{
					linePos = level.getHeightmapPos(Heightmap.Types.WORLD_SURFACE_WG, linePos).below();
					if (linePos.getY() <= level.getMinBuildHeight())
						continue;
					double distToCenter = (15 - center.distanceTo(p)) * 0.3;
					int height = rand.nextInt(1, 4);
					for (int dy = 0; dy < distToCenter + height; dy++)
						level.setBlock(linePos.above(dy), state(level, rand), Block.UPDATE_CLIENTS);
				}
			}
		}

		private static BlockState state(WorldGenLevel level, RandomSource rand)
		{
			return rand.nextFloat() < 0.40F ? Blocks.OBSIDIAN.defaultBlockState() : RediscoveredBlocks.ancient_crying_obsidian.defaultBlockState();
		}
	}
}
