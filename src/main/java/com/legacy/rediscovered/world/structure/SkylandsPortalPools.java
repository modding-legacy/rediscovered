package com.legacy.rediscovered.world.structure;

import java.util.ArrayList;
import java.util.List;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredProcessorLists;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;

import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class SkylandsPortalPools
{
	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(RediscoveredMod.MODID, "skylands_portal/", bootstrap);
		registry.registerBuilder().pools(registry.poolBuilder().names(indexed("trail_ruins/portal_room_", 2)).processors(RediscoveredProcessorLists.TRAIL_RUINS_PORTAL_ARCHAEOLOGY.getKey()).build()).register("trail_ruins/portal_room");
	}

	private static String[] indexed(String prefix, int maxVal)
	{
		List<String> list = new ArrayList<>(maxVal + 1);
		for (int i = 0; i <= maxVal; i++)
			list.add(prefix + i);
		return list.toArray(String[]::new);
	}

	private static String[] indexed_(String prefix, int maxVal)
	{
		return new String[] { prefix + "-1" };
	}
}
