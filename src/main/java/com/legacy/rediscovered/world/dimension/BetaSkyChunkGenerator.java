package com.legacy.rediscovered.world.dimension;

import java.text.DecimalFormat;
import java.util.List;
import java.util.OptionalInt;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableObject;

import com.legacy.rediscovered.world.dimension.noise.BetaOctavesNoiseGenerator;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.SharedConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.SectionPos;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.LevelHeightAccessor;
import net.minecraft.world.level.NoiseColumn;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.chunk.ChunkGeneratorStructureState;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.GenerationStep.Carving;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseRouter;
import net.minecraft.world.level.levelgen.NoiseRouterData;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.blending.Blender;
import net.minecraft.world.level.levelgen.structure.StructureSet;

public class BetaSkyChunkGenerator extends NoiseBasedChunkGenerator
{
	public static final Codec<BetaSkyChunkGenerator> SKYLANDS_CODEC = RecordCodecBuilder.create((instance) ->
	{
		return instance.group(BiomeSource.CODEC.fieldOf("biome_source").forGetter((chunkGen) ->
		{
			return chunkGen.biomeSource;
		}), NoiseGeneratorSettings.CODEC.fieldOf("settings").forGetter((chunkGen) ->
		{
			return chunkGen.generatorSettings();
		})).apply(instance, instance.stable(BetaSkyChunkGenerator::new));
	});

	private Random genRandom;
	private BetaOctavesNoiseGenerator octaves1, octaves2, octaves3, scaleNoise, octaves7;

	private double[] octaveArr1, octaveArr2, octaveArr3, octaveArr4, octaveArr5;
	private double[] heightNoise;

	public static final int CHUNK_SIZE = 16;

	public BetaSkyChunkGenerator(BiomeSource biomeSource, Holder<NoiseGeneratorSettings> settingsIn)
	{
		super(biomeSource, settingsIn);
	}

	@Override
	public ChunkGeneratorStructureState createState(HolderLookup<StructureSet> structureRegistry, RandomState rand, long serverSeed)
	{
		this.genRandom = new Random(serverSeed);
		this.octaves1 = new BetaOctavesNoiseGenerator(this.genRandom, 16);
		this.octaves2 = new BetaOctavesNoiseGenerator(this.genRandom, 16);
		this.octaves3 = new BetaOctavesNoiseGenerator(this.genRandom, 8);
		this.scaleNoise = new BetaOctavesNoiseGenerator(this.genRandom, 10);
		this.octaves7 = new BetaOctavesNoiseGenerator(this.genRandom, 16);

		return super.createState(structureRegistry, rand, serverSeed);
	}

	@Override
	protected Codec<? extends ChunkGenerator> codec()
	{
		return SKYLANDS_CODEC;
	}

	@Override
	public void buildSurface(WorldGenRegion level, StructureManager structureManager, RandomState randState, ChunkAccess chunkAccess)
	{
		if (!SharedConstants.debugVoidTerrain(chunkAccess.getPos()))
			this.replaceBlocksForBiome(chunkAccess.getPos().x, chunkAccess.getPos().z, chunkAccess, level.getRandom());
	}

	@Override
	public CompletableFuture<ChunkAccess> fillFromNoise(Executor executor, Blender blender, RandomState randomState, StructureManager structureManager, ChunkAccess chunk)
	{
		int x = chunk.getPos().x;
		int z = chunk.getPos().z;
		this.genRandom.setSeed((long) x * 341873128712L + (long) z * 132897987541L);
		this.generateTerrain(chunk.getPos(), (state, pos) ->
		{
			if (state != null)
			{
				chunk.setBlockState(pos, state, false);
			}
		});

		return CompletableFuture.completedFuture(chunk);
	}

	private void generateTerrain(ChunkPos chunkPos, BiConsumer<BlockState, BlockPos> stateConsumer)
	{
		byte byte0 = 2;
		int k = byte0 + 1;
		byte byte1 = 33;
		int l = byte0 + 1;
		heightNoise = octaveGenerator(heightNoise, chunkPos.x * byte0, 0, chunkPos.z * byte0, k, byte1, l);

		for (int i1 = 0; i1 < byte0; i1++)
		{
			for (int j1 = 0; j1 < byte0; j1++)
			{
				for (int k1 = 0; k1 < 32; k1++)
				{
					double d = 0.25D;
					double d1 = heightNoise[((i1 + 0) * l + (j1 + 0)) * byte1 + (k1 + 0)];
					double d2 = heightNoise[((i1 + 0) * l + (j1 + 1)) * byte1 + (k1 + 0)];
					double d3 = heightNoise[((i1 + 1) * l + (j1 + 0)) * byte1 + (k1 + 0)];
					double d4 = heightNoise[((i1 + 1) * l + (j1 + 1)) * byte1 + (k1 + 0)];
					double d5 = (heightNoise[((i1 + 0) * l + (j1 + 0)) * byte1 + (k1 + 1)] - d1) * d;
					double d6 = (heightNoise[((i1 + 0) * l + (j1 + 1)) * byte1 + (k1 + 1)] - d2) * d;
					double d7 = (heightNoise[((i1 + 1) * l + (j1 + 0)) * byte1 + (k1 + 1)] - d3) * d;
					double d8 = (heightNoise[((i1 + 1) * l + (j1 + 1)) * byte1 + (k1 + 1)] - d4) * d;
					for (int l1 = 0; l1 < 4; l1++)
					{
						double d9 = 0.125D;
						double d10 = d1;
						double d11 = d2;
						double d12 = (d3 - d1) * d9;
						double d13 = (d4 - d2) * d9;

						for (int i2 = 0; i2 < 8; ++i2)
						{
							double d14 = 0.125D;
							double d15 = d10;
							double d16 = (d11 - d10) * d14;

							for (int j2 = 0; j2 < 8; ++j2)
							{
								BlockState state = null;

								if (d15 > 0.0D)
								{
									state = this.generatorSettings().value().defaultBlock();
								}

								int x = i2 + i1 * 8;
								int y = l1 + k1 * 4;
								int z = j2 + j1 * 8;

								stateConsumer.accept(state, new BlockPos(x, y, z));

								d15 += d16;
							}

							d10 += d12;
							d11 += d13;
						}

						d1 += d5;
						d2 += d6;
						d3 += d7;
						d4 += d8;
					}

				}

			}

		}
	}

	private double[] octaveGenerator(double values[], int xPos, int zPos, int size1, int size2, int size3, int j1)
	{
		if (values == null)
		{
			values = new double[size2 * size3 * j1];
		}

		double noiseFactor = 684.41200000000003D;
		double d1 = 684.41200000000003D;
		octaveArr4 = scaleNoise.generateNoiseOctaves(octaveArr4, xPos, size1, size2, j1, 1.121D, 1.121D, 0.5D);
		octaveArr5 = octaves7.generateNoiseOctaves(octaveArr5, xPos, size1, size2, j1, 200D, 200D, 0.5D);
		noiseFactor *= 2D;
		octaveArr1 = octaves3.generateNoiseOctaves(octaveArr1, xPos, zPos, size1, size2, size3, j1, noiseFactor / 80D, d1 / 160D, noiseFactor / 80D);
		octaveArr2 = octaves1.generateNoiseOctaves(octaveArr2, xPos, zPos, size1, size2, size3, j1, noiseFactor, d1, noiseFactor);
		octaveArr3 = octaves2.generateNoiseOctaves(octaveArr3, xPos, zPos, size1, size2, size3, j1, noiseFactor, d1, noiseFactor);
		int k1 = 0;
		int l1 = 0;

		for (int j2 = 0; j2 < size2; j2++)
		{
			for (int l2 = 0; l2 < j1; l2++)
			{
				double d3;
				d3 = 0.5D;
				double d4 = 1.0D - d3;
				d4 *= d4;
				d4 *= d4;
				d4 = 1.0D - d4;
				double d5 = (octaveArr4[l1] + 256D) / 512D;
				d5 *= d4;

				if (d5 > 1.0D)
				{
					d5 = 1.0D;
				}

				double d6 = octaveArr5[l1] / 8000D;

				if (d6 < 0.0D)
				{
					d6 = -d6 * 0.29999999999999999D;
				}

				d6 = d6 * 3D - 2D;

				if (d6 > 1.0D)
				{
					d6 = 1.0D;
				}

				d6 /= 8D;
				d6 = 0.0D;

				if (d5 < 0.0D)
				{
					d5 = 0.0D;
				}

				d5 += 0.5D;
				d6 = (d6 * (double) size3) / 16D;
				l1++;
				double d7 = (double) size3 / 2D;

				for (int j3 = 0; j3 < size3; j3++)
				{
					double d8 = 0.0D;
					double d9 = (((double) j3 - d7) * 8D) / d5;

					if (d9 < 0.0D)
					{
						d9 *= -1D;
					}

					double d10 = octaveArr2[k1] / 512D;
					double d11 = octaveArr3[k1] / 512D;
					double d12 = (octaveArr1[k1] / 10D + 1.0D) / 2D;

					if (d12 < 0.0D)
					{
						d8 = d10;
					}
					else if (d12 > 1.0D)
					{
						d8 = d11;
					}
					else
					{
						d8 = d10 + (d11 - d10) * d12;
					}

					d8 -= 8D;
					int k3 = 32;

					if (j3 > size3 - k3)
					{
						double d13 = (float) (j3 - (size3 - k3)) / ((float) k3 - 1.0F);
						d8 = d8 * (1.0D - d13) + -30D * d13;
					}

					k3 = 8;

					if (j3 < k3)
					{
						double d14 = (float) (k3 - j3) / ((float) k3 - 1.0F);
						d8 = d8 * (1.0D - d14) + -30D * d14;
					}
					values[k1] = d8;
					k1++;
				}

			}

		}

		return values;
	}

	private void replaceBlocksForBiome(int chunkX, int chunkZ, ChunkAccess chunk, RandomSource rand)
	{
		BlockState grass = Blocks.GRASS_BLOCK.defaultBlockState();
		BlockState dirt = Blocks.DIRT.defaultBlockState();
		for (int x = 0; x < 16; ++x)
		{
			for (int z = 0; z < 16; ++z)
			{
				int dirtDepth = -1;
				for (int y = 127; y >= 0; --y)
				{
					BlockState state = chunk.getBlockState(new BlockPos(x, y, z));

					if (state.isAir())
					{
						dirtDepth = -1;
					}
					else if (state.getBlock() == Blocks.STONE)
					{
						if (dirtDepth == -1)
						{
							dirtDepth = 2;
							chunk.setBlockState(new BlockPos(x, y, z), grass, false);
						}
						else if (dirtDepth > 0)
						{
							if (dirtDepth > 1 || rand.nextBoolean())
								chunk.setBlockState(new BlockPos(x, y, z), dirt, false);
							dirtDepth--;
						}
					}
				}
			}
		}
	}

	@Override
	public int getBaseHeight(int x, int z, Types heightmap, LevelHeightAccessor level, RandomState rand)
	{
		return this.iterateNoiseColumn(level, rand, x, z, null, heightmap.isOpaque()).orElse(level.getMinBuildHeight());
	}

	@Override
	public NoiseColumn getBaseColumn(int x, int z, LevelHeightAccessor level, RandomState rand)
	{
		MutableObject<NoiseColumn> column = new MutableObject<>();
		this.iterateNoiseColumn(level, rand, x, z, column, null);
		return column.getValue();
	}

	protected OptionalInt iterateNoiseColumn(LevelHeightAccessor level, RandomState rand, int x, int z, @Nullable MutableObject<NoiseColumn> column, @Nullable Predicate<BlockState> stoppingState)
	{
		int minHeight = level.getMinBuildHeight();
		BlockState[] states = new BlockState[level.getHeight()];
		MutableInt lastY = new MutableInt(-1);
		this.generateTerrain(new ChunkPos(SectionPos.blockToSectionCoord(x), SectionPos.blockToSectionCoord(z)), (state, pos) ->
		{
			// pos is a section relative position, while x and z are world positions.
			if (SectionPos.sectionRelative(x) == pos.getX() && SectionPos.sectionRelative(z) == pos.getZ())
			{
				int y = pos.getY() - minHeight;
				states[y] = state == null ? Blocks.AIR.defaultBlockState() : state;
				lastY.increment();
			}
		});

		// The states array may not have all the values filled if the generateTerrain method doesn't reach high enough. In this case, only up to 127 are filled.
		// This splits the array into a new one, containing only the filled values. This new array will be used moving forward.
		BlockState[] stateColumn;
		int j = lastY.intValue();
		if (j > -1)
		{
			stateColumn = new BlockState[j];
			System.arraycopy(states, 0, stateColumn, 0, j);
		}
		else
		{
			stateColumn = states;
		}

		// Store the noise column
		if (column != null)
		{
			column.setValue(new NoiseColumn(minHeight, stateColumn));
		}

		// Find the top block
		if (stoppingState != null)
		{
			for (int i = stateColumn.length - 1; i > -1; i--)
			{
				BlockState state = stateColumn[i];
				if (stoppingState.test(state))
					return OptionalInt.of(i + 1 + minHeight);
			}
			return OptionalInt.of(minHeight);
		}
		return OptionalInt.empty();
	}

	@Override
	public void addDebugScreenInfo(List<String> pInfo, RandomState randomState, BlockPos pos)
	{
		DecimalFormat decimalformat = new DecimalFormat("0.000");
		NoiseRouter noiserouter = randomState.router();
		DensityFunction.SinglePointContext densityfunction$singlepointcontext = new DensityFunction.SinglePointContext(pos.getX(), pos.getY(), pos.getZ());
		double d0 = noiserouter.ridges().compute(densityfunction$singlepointcontext);
		pInfo.add("NoiseRouter T: " + decimalformat.format(noiserouter.temperature().compute(densityfunction$singlepointcontext)) + " V: " + decimalformat.format(noiserouter.vegetation().compute(densityfunction$singlepointcontext)) + " C: " + decimalformat.format(noiserouter.continents().compute(densityfunction$singlepointcontext)) + " E: " + decimalformat.format(noiserouter.erosion().compute(densityfunction$singlepointcontext)) + " D: " + decimalformat.format(noiserouter.depth().compute(densityfunction$singlepointcontext)) + " W: " + decimalformat.format(d0) + " PV: " + decimalformat.format((double) NoiseRouterData.peaksAndValleys((float) d0)) + " AS: " + decimalformat.format(noiserouter.initialDensityWithoutJaggedness().compute(densityfunction$singlepointcontext)) + " N: " + decimalformat.format(noiserouter.finalDensity().compute(densityfunction$singlepointcontext)));

	}

	@Override
	public void applyCarvers(WorldGenRegion pLevel, long pSeed, RandomState pRandom, BiomeManager pBiomeManager, StructureManager pStructureManager, ChunkAccess pChunk, Carving pStep)
	{
	}
}