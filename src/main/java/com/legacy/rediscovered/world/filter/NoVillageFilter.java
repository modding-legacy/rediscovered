package com.legacy.rediscovered.world.filter;

import com.legacy.rediscovered.registry.RediscoveredPlacementModifiers;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.structure_gel.api.structure.StructureAccessHelper;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.levelgen.placement.PlacementContext;
import net.minecraft.world.level.levelgen.placement.PlacementFilter;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;

public class NoVillageFilter extends PlacementFilter
{
	private static final NoVillageFilter INSTANCE = new NoVillageFilter();
	public static Codec<NoVillageFilter> CODEC = Codec.unit(() ->
	{
		return INSTANCE;
	});

	private NoVillageFilter()
	{
	}

	public static NoVillageFilter instance()
	{
		return INSTANCE;
	}

	@Override
	protected boolean shouldPlace(PlacementContext context, RandomSource rand, BlockPos pos)
	{
		return !StructureAccessHelper.isInStructure(context.getLevel(), RediscoveredStructures.PIGMAN_VILLAGE.getStructure().get(context.getLevel()), pos);
	}

	@Override
	public PlacementModifierType<?> type()
	{
		return RediscoveredPlacementModifiers.NO_VILLAGE_FILTER;
	}
}