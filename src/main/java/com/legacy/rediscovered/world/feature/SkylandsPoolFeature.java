package com.legacy.rediscovered.world.feature;

import java.util.Optional;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.structure_gel.api.util.VoxelShapeUtil;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SmallDripleafBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

public class SkylandsPoolFeature extends Feature<SkylandsPoolFeature.Config>
{
	public SkylandsPoolFeature(Codec<Config> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<Config> context)
	{
		var level = context.level();
		var origin = context.origin();
		var rand = context.random();
		Config config = context.config();
		BlockState fluid = config.fluid;
		int radius = config.radius.sample(rand);
		float fluidRadius = radius - 1.5F;
		Optional<BlockState> edge = config.edge;
		Optional<BlockState> bottom = config.bottom;
		Optional<BlockState> wall = config.wall;
		TagKey<Block> replaceable = config.replaceable;
		Direction[] hDirs = VoxelShapeUtil.HORIZONTAL_DIRS.toArray(Direction[]::new);
		float cutoutChance = config.cutoutChance;
		float dripleafChance = config.dripleafChance;

		BlockPos pos = origin;
		for (int x = -radius; x <= radius; x++)
		{
			for (int z = -radius; z <= radius; z++)
			{
				for (int y = radius; y >= -radius; y--)
				{
					int distSqr = x * x + y * y + z * z;
					if (distSqr > radius * radius)
						continue;

					// If the block is replaceable, we will attempt to swap it out
					pos = origin.offset(x, y, z);
					BlockState state = level.getBlockState(pos);
					if (state.is(replaceable))
					{
						BlockState above = level.getBlockState(pos.above());
						// If the block above is air attempt to place fluid/edge
						if (above.isAir())
						{
							// Check if this position is an edge. An edge is defined as having a neighbor that is air.
							boolean isEdge = distSqr > fluidRadius * fluidRadius;
							boolean nextToAir = false;
							if (!isEdge && this.hasNeighbor(level, pos, hDirs, p -> level.getBlockState(p).isAir()))
							{
								nextToAir = true;
								isEdge = true;
							}
							// If this is an edge or the block below where we want to place is air, make this an edge.
							// The check for below prevents pools that don't have a bottom.
							boolean airBelow = level.getBlockState(pos.below()).isAir();
							@Nullable
							BlockState placedState = isEdge ? (!airBelow && nextToAir && rand.nextFloat() < cutoutChance ? Blocks.AIR.defaultBlockState() : edge.orElse(null)) : airBelow ? null : fluid;
							if (placedState != null)
							{
								this.setBlock(level, pos, placedState);
							}
						}
						// If the block above is a fluid, attempt to place a bottom block
						else if (bottom.isPresent() && above.is(fluid.getBlock()))
						{
							this.setBlock(level, pos, bottom.get());
						}
						else if (wall.isPresent() && above.is(edge.get().getBlock()))
						{
							boolean isEdge = this.hasNeighbor(level, pos, hDirs, p -> level.getBlockState(p).is(fluid.getBlock()) || level.getBlockState(p.above()).isAir());
							if (isEdge)
								this.setBlock(level, pos, wall.get());
						}
					}

				}
			}
		}

		if (dripleafChance < 1.0F)
		{
			for (int x = -radius; x <= radius; x++)
			{
				for (int z = -radius; z <= radius; z++)
				{
					for (int y = radius; y >= -radius; y--)
					{
						pos = origin.offset(x, y, z);
						if (rand.nextFloat() < 0.05F && level.getBlockState(pos).canBeReplaced() && level.getBlockState(pos.above()).canBeReplaced() && level.getBlockState(pos.below()).is(BlockTags.SMALL_DRIPLEAF_PLACEABLE))
						{
							BlockState dripleaf = Blocks.SMALL_DRIPLEAF.defaultBlockState().setValue(SmallDripleafBlock.FACING, Util.getRandom(hDirs, rand));
							level.setBlock(pos, dripleaf.setValue(BlockStateProperties.WATERLOGGED, this.inOrNextToWater(level, pos, hDirs)), Block.UPDATE_KNOWN_SHAPE);
							BlockPos abovePos = pos.above();
							level.setBlock(abovePos, dripleaf.setValue(BlockStateProperties.WATERLOGGED, this.inOrNextToWater(level, abovePos, hDirs)).setValue(SmallDripleafBlock.HALF, DoubleBlockHalf.UPPER), Block.UPDATE_KNOWN_SHAPE);
						}
					}
				}
			}
		}

		return true;
	}

	private boolean hasNeighbor(WorldGenLevel level, BlockPos pos, Direction[] offsets, Predicate<BlockPos> test)
	{
		for (Direction dir : offsets)
			if (test.test(pos.relative(dir)))
				return true;
		return false;
	}

	private boolean inOrNextToWater(WorldGenLevel level, BlockPos pos, Direction[] offsets)
	{
		return level.getBlockState(pos).is(Blocks.WATER) || this.hasNeighbor(level, pos, offsets, p -> level.getBlockState(p).is(Blocks.WATER));
	}
	
	private void setBlock(WorldGenLevel level, BlockPos pos, BlockState state)
	{
		if (level.setBlock(pos, state, Block.UPDATE_ALL) && !state.getFluidState().isEmpty())
			level.getChunk(pos).markPosForPostprocessing(pos);
	}

	public static record Config(BlockState fluid, IntProvider radius, Optional<BlockState> edge, float cutoutChance, Optional<BlockState> wall, Optional<BlockState> bottom, float dripleafChance, TagKey<Block> replaceable) implements FeatureConfiguration
	{
		public static final Codec<Config> CODEC = RecordCodecBuilder.create(instance ->
		{
			//@formatter:off
			return instance.group(
					BlockState.CODEC.fieldOf("fluid").forGetter(Config::fluid), 
					IntProvider.NON_NEGATIVE_CODEC.fieldOf("radius").forGetter(Config::radius), 
					BlockState.CODEC.optionalFieldOf("edge").forGetter(Config::edge), 
					ExtraCodecs.POSITIVE_FLOAT.fieldOf("cutout_chance").forGetter(Config::cutoutChance),
					BlockState.CODEC.optionalFieldOf("wall").forGetter(Config::wall), 
					BlockState.CODEC.optionalFieldOf("bottom").forGetter(Config::bottom),
					ExtraCodecs.POSITIVE_FLOAT.fieldOf("dripleaf_chance").forGetter(Config::dripleafChance),
					TagKey.codec(Registries.BLOCK).fieldOf("replaceable").forGetter(Config::replaceable)
				).apply(instance, Config::new);
			//@formatter:on
		});
	}
}
