package com.legacy.rediscovered.world.feature;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SnowyDirtBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

//SnowAndFreezeFeature
public class IceAndSnowSkyFeature<FC extends FeatureConfiguration> extends Feature<FC>
{
	public IceAndSnowSkyFeature(Codec<FC> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<FC> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();

		BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();
		BlockPos.MutableBlockPos blockpos$mutableblockpos1 = new BlockPos.MutableBlockPos();

		for (int i = 0; i < 16; ++i)
		{
			for (int j = 0; j < 16; ++j)
			{
				int k = pos.getX() + i;
				int l = pos.getZ() + j;
				int i1 = level.getHeight(Heightmap.Types.MOTION_BLOCKING, k, l);
				blockpos$mutableblockpos.set(k, i1, l);
				blockpos$mutableblockpos1.set(blockpos$mutableblockpos).move(Direction.DOWN, 1);

				if (i1 > 10 && i1 < 96 && level.getBiome(blockpos$mutableblockpos1).value().shouldSnow(level, blockpos$mutableblockpos) && level.isEmptyBlock(blockpos$mutableblockpos))
				{
					level.setBlock(blockpos$mutableblockpos, Blocks.SNOW.defaultBlockState(), 2);

					BlockState blockstate = level.getBlockState(blockpos$mutableblockpos1);

					if (blockstate.hasProperty(SnowyDirtBlock.SNOWY))
					{
						level.setBlock(blockpos$mutableblockpos1, blockstate.setValue(SnowyDirtBlock.SNOWY, Boolean.valueOf(true)), 2);
					}
				}
			}
		}

		return true;
	}
}