package com.legacy.rediscovered.world.feature;

import java.util.function.BiConsumer;

import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.util.VoxelShapeUtil;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;

public class AncientCherryTreeFeature extends AbstractSkyTreeFeature
{
	public AncientCherryTreeFeature(TreeProperties properties)
	{
		super(properties);
	}

	@Override
	public boolean place(WorldGenLevel level, RandomSource rand, BlockPos pos, int height, BiConsumer<BlockPos, BlockState> changedLogs, BiConsumer<BlockPos, BlockState> changedLeaves)
	{
		int min = 12, max = 15;
		height = min + rand.nextInt(max - min + 1);
		if (!this.isAreaOk(level, pos, height, 1, height - 2, 5))
			return false;

		for (int y = 0; y < height; y++)
			this.setBlockIfOk(changedLogs, level, pos.offset(0, y, 0), Blocks.CHERRY_LOG.defaultBlockState());

		this.placeTopLeaves(level, rand, pos.above(height - 2), changedLogs, changedLeaves);

		for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
			if (rand.nextFloat() < 0.75F)
				this.placeBranch(level, rand, pos.above(Math.max(height - (rand.nextInt(5) + 7), 3)), pos.getY() + height - 4, dir, changedLogs, changedLeaves);

		return true;
	}

	private void placeTopLeaves(WorldGenLevel level, RandomSource rand, BlockPos pos, BiConsumer<BlockPos, BlockState> changedLogs, BiConsumer<BlockPos, BlockState> changedLeaves)
	{
		BlockState leaves = RediscoveredBlocks.ancient_cherry_leaves.defaultBlockState();
		
		this.setBlockIfOk(changedLogs, level, pos.above(), Blocks.CHERRY_LOG.defaultBlockState());
		
		int r = 3;
		for (int x = -r; x <= r; x++)
		{
			for (int z = -r; z <= r; z++)
			{
				if (Math.abs(x) + Math.abs(z) < r + 2 && rand.nextFloat() < 0.8F)
					this.setBlockIfOk(changedLeaves, level, pos.offset(x, -1, z), leaves);
			}
		}

		r = 4;
		for (int x = -r; x <= r; x++)
		{
			for (int z = -r; z <= r; z++)
			{
				for (int y = 0; y <= 1; y++)
				{
					int dist = Math.abs(x) + Math.abs(z);
					if (dist < r + 2 || (dist < r + 3 && rand.nextFloat() < 0.4F))
					{
						if (Math.max(Math.abs(x), Math.abs(z)) != r || rand.nextFloat() < 0.75)
							this.setBlockIfOk(changedLeaves, level, pos.offset(x, y, z), leaves);
					}
				}
			}
		}

		r = 3;
		for (int x = -r; x <= r; x++)
		{
			for (int z = -r; z <= r; z++)
			{
				if (Math.abs(x) + Math.abs(z) < r + 2)
					this.setBlockIfOk(changedLeaves, level, pos.offset(x, 2, z), leaves);
			}
		}

		r = 2;
		for (int x = -r; x <= r; x++)
		{
			for (int z = -r; z <= r; z++)
			{
				if ((Math.abs(x) + Math.abs(z) < 2) || rand.nextFloat() < 0.3F)
					this.setBlockIfOk(changedLeaves, level, pos.offset(x, 3, z), leaves);
			}
		}
	}

	private void placeBranch(WorldGenLevel level, RandomSource rand, BlockPos pos, int terminationHeight, Direction direction, BiConsumer<BlockPos, BlockState> changedLogs, BiConsumer<BlockPos, BlockState> changedLeaves)
	{
		BlockState log = Blocks.CHERRY_LOG.defaultBlockState();
		BlockState sideLog = log.setValue(RotatedPillarBlock.AXIS, direction.getAxis());

		int dist = rand.nextInt(4) + 5;
		for (int i = 0; i < dist && pos.getY() < terminationHeight; i++)
		{
			boolean up = rand.nextBoolean() && i > 1;
			pos = pos.relative(up ? Direction.UP : direction);
			if (i > 0)
				pos = pos.relative(direction.getClockWise(), rand.nextInt(2) - 1);
			this.setBlockIfOk(changedLogs, level, pos, up ? log : sideLog);
		}
		pos = pos.above();
		this.setBlockIfOk(changedLogs, level, pos, log);

		this.placeTopLeaves(level, rand, pos, changedLogs, changedLeaves);
	}
}
