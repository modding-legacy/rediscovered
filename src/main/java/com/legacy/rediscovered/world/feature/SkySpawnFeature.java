package com.legacy.rediscovered.world.feature;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

public class SkySpawnFeature<FC extends FeatureConfiguration> extends Feature<FC>
{
	public SkySpawnFeature(Codec<FC> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<FC> context)
	{
		/*WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		
		int x = pos.getX();
		int y = 80; // pos.getY();
		int z = pos.getZ();
		
		if (!this.doesStartInChunk(pos) || level.getBlockState(new BlockPos(x, y, z)) == Blocks.OAK_PLANKS.defaultBlockState() || level.getBlockState(new BlockPos(x, y, z)).getBlock() == Blocks.OAK_LOG || level.getBlockState(new BlockPos(x, y, z)) == RediscoveredBlocks.slate_blue_wool.defaultBlockState() || level.getBlockState(new BlockPos(x, y, z)) == Blocks.COBBLESTONE.defaultBlockState())
		{
			return false;
		}
		
		Block planks = Blocks.OAK_PLANKS;
		Block bed = Blocks.BLUE_BED;
		Block chair = RediscoveredBlocks.oak_chair;
		Block table = RediscoveredBlocks.oak_table;
		Block glass = Blocks.GLASS;
		Block stone = Blocks.COBBLESTONE;
		Block door = Blocks.OAK_DOOR;
		Block wool = RediscoveredBlocks.slate_blue_wool;
		Block wood = Blocks.OAK_LOG;
		Block grass = Blocks.GRASS_BLOCK;
		Block dirt = Blocks.DIRT;
		Block brick = Blocks.STONE_BRICKS;
		Block fence = Blocks.IRON_BARS;
		Block netherrack = Blocks.NETHERRACK;
		Block fire = Blocks.FIRE;
		
		// Air
		for (int a = 1; a <= 5; a++)
			for (int b = 0; b <= 4; b++)
				for (int c = -1; c <= 3; c++)
					level.setBlock(new BlockPos(x + a, y + b, z + c), Blocks.AIR.defaultBlockState(), 2);
		
		// Grass
		// Front
		level.setBlock(new BlockPos(x + 7, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z - 2), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z - 1), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z + 1), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z + 2), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z + 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z + 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z + 5), grass.defaultBlockState(), 2);
		
		// Right Side
		level.setBlock(new BlockPos(x - 1, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z - 4), grass.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x - 1, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z - 3), grass.defaultBlockState(), 2);
		
		// Left Side
		level.setBlock(new BlockPos(x - 1, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 5), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 7, y - 1, z + 5), grass.defaultBlockState(), 2);
		
		// Back
		level.setBlock(new BlockPos(x - 1, y - 1, z - 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z - 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z - 2), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z - 1), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z + 1), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z + 2), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z + 3), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z + 4), grass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x - 1, y - 1, z + 5), grass.defaultBlockState(), 2);
		
		// Ground
		level.setBlock(new BlockPos(x + 6, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z - 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z - 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z + 1), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z + 2), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z + 3), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 2, z + 4), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 2, z + 4), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 2, z + 4), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 2, z + 4), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 2, z + 4), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 2, z + 4), dirt.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 2, z + 4), dirt.defaultBlockState(), 2);
		
		// Bottom Ring
		
		level.setBlock(new BlockPos(x, y, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y, z), planks.defaultBlockState(), 2);
		
		// Floor
		
		level.setBlock(new BlockPos(x, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 1), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 2), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 3), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 4), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z - 1), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y - 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z - 2), planks.defaultBlockState(), 2);
		
		// Carpet
		level.setBlock(new BlockPos(x + 4, y - 1, z), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 1), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 1), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 1), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y - 1, z + 2), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y - 1, z + 2), wool.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y - 1, z + 2), wool.defaultBlockState(), 2);
		
		// Contents of house
		level.setBlock(new BlockPos(x + 2, y, z + 3), bed.defaultBlockState().setValue(BedBlock.PART, BedPart.FOOT).setValue(BedBlock.FACING, Direction.WEST), 3);
		level.setBlock(new BlockPos(x + 1, y, z + 3), bed.defaultBlockState().setValue(BedBlock.PART, BedPart.HEAD).setValue(BedBlock.FACING, Direction.WEST), 1 + 3);
		level.setBlock(new BlockPos(x + 1, y, z + 2), table.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y, z - 1), chair.defaultBlockState().setValue(ChairBlock.FACING, Direction.SOUTH), 3);
		
		// Second Ring
		
		level.setBlock(new BlockPos(x, y + 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z + 1), glass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y + 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 1, z + 4), glass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 1, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 1, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 1, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 1, z + 2), planks.defaultBlockState(), 2);
		
		// door
		level.setBlock(new BlockPos(x + 6, y, z + 1), door.defaultBlockState().setValue(DoorBlock.HALF, DoubleBlockHalf.LOWER).setValue(DoorBlock.FACING, Direction.WEST), 3);
		level.setBlock(new BlockPos(x + 6, y + 1, z + 1), door.defaultBlockState().setValue(DoorBlock.HALF, DoubleBlockHalf.UPPER).setValue(DoorBlock.FACING, Direction.WEST), 3);
		
		level.setBlock(new BlockPos(x + 6, y + 1, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 1, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y + 1, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 1, z), planks.defaultBlockState(), 2);
		
		// Third Ring
		
		level.setBlock(new BlockPos(x, y + 2, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z + 1), glass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y + 2, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 2, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 2, z + 4), glass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 2, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 2, z + 4), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z + 4), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z + 3), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z + 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z + 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 2, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 2, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 2, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 2, z - 2), glass.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 2, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 1, y + 2, z - 2), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z - 2), wood.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z - 1), planks.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 2, z), planks.defaultBlockState(), 2);
		
		// Roof
		
		level.setBlock(new BlockPos(x + 1, y + 3, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z + 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 2), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y + 3, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z + 3), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y - 1, z + 3), planks.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y + 3, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 3, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 4, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 3, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 3, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z + 4), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z + 4), stone.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y + 3, z - 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z - 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z - 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z - 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z - 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z - 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z - 1), stone.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y + 3, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 3, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 4, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 3, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 3, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z - 2), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z - 2), stone.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y + 3, z), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z), stone.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 1, y + 3, z + 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 4, z + 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z + 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 4, z + 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 5, y + 3, z + 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x, y + 3, z + 1), stone.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 6, y + 3, z + 1), stone.defaultBlockState(), 2);
		
		// Fireplace
		// Fence
		level.setBlock(new BlockPos(x + 2, y, z - 1), fence.defaultBlockState().setValue(IronBarsBlock.EAST, true).setValue(IronBarsBlock.NORTH, true), 2);
		level.setBlock(new BlockPos(x + 3, y, z - 1), fence.defaultBlockState().setValue(IronBarsBlock.EAST, true).setValue(IronBarsBlock.WEST, true), 2);
		level.setBlock(new BlockPos(x + 4, y, z - 1), fence.defaultBlockState().setValue(IronBarsBlock.WEST, true).setValue(IronBarsBlock.NORTH, true), 2);
		// Front
		level.setBlock(new BlockPos(x + 3, y - 1, z - 2), netherrack.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y, z - 2), fire.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y, z - 2), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y, z - 2), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 1, z - 2), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 1, z - 2), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 2, z - 2), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 2, z - 2), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 2, z - 2), brick.defaultBlockState(), 2);
		// back
		level.setBlock(new BlockPos(x + 2, y, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 1, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 1, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 1, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 2, y + 2, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 2, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 4, y + 2, z - 3), brick.defaultBlockState(), 2);
		
		level.setBlock(new BlockPos(x + 3, y + 3, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 4, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 5, z - 3), brick.defaultBlockState(), 2);
		level.setBlock(new BlockPos(x + 3, y + 6, z - 3), brick.defaultBlockState(), 2);*/

		return true;

	}

	public boolean doesStartInChunk(BlockPos pos)
	{
		return pos.getX() >> 4 == 0 >> 4 && pos.getZ() >> 4 == 0 >> 4;
	}
}