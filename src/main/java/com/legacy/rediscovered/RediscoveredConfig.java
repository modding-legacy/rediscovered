package com.legacy.rediscovered;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.structure_gel.api.config.ConfigBuilder;
import com.legacy.structure_gel.api.config.ConfigValueWrapper;

import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.common.ModConfigSpec.ConfigValue;

public class RediscoveredConfig
{
	public static final ModConfigSpec CLIENT_SPEC;
	// public static final ModConfigSpec COMMON_SPEC;
	public static final ModConfigSpec WORLD_SPEC;

	public static final ClientConfig CLIENT;
	// public static final CommonConfig COMMON;
	public static final WorldConfig WORLD;

	static
	{
		{
			Pair<ClientConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		/*{
			Pair<WorldConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(CommonConfig::new);
			COMMON = pair.getLeft();
			COMMON_SPEC = pair.getRight();
		}*/
		{
			Pair<WorldConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(WorldConfig::new);
			WORLD = pair.getLeft();
			WORLD_SPEC = pair.getRight();
		}
	}

	public static class ClientConfig
	{
		/*private final ConfigValue<Boolean> customPanorama;*/
		private final ConfigValue<Boolean> customSkylandsClouds;
		private final ConfigValue<Boolean> overrideVanillaClouds;
		private final ConfigValue<Integer> screenShakeIntensity;
		
		public final ConfigValue<Boolean> allowAprilFools;

		/*private final ConfigValue<RedDragonOffspringEntity.ControlType> redDragonControlType;*/

		public ClientConfig(ModConfigSpec.Builder builder)
		{
			builder.push("Visuals");
			/*this.customPanorama = ConfigBuilder.makeBoolean(builder, "custom_panorama", "Overrides the title screen panorama with a Skylands one. (This can cause incompatibilities when enabled with other menu mods, enable at your own risk.)", false);*/
			this.customSkylandsClouds = ConfigBuilder.makeBoolean(builder, "extra_skylands_clouds", "Replaces the default clouds in the Skylands with more plentiful, and fluffier ones.", true);
			this.overrideVanillaClouds = ConfigBuilder.makeBoolean(builder, "override_vanilla_clouds", "Replaces the default cloud rendering with the fluffy clouds found in the Skylands.", false);
			this.screenShakeIntensity = ConfigBuilder.makeInt(builder, "screen_shake_intensity", "The intensity percentage of the screen shake caused by various things in the mod. Can be disabled if set to 0", 100, 0, 100);
			builder.pop();
			
			builder.push("Holiday");
			this.allowAprilFools = ConfigBuilder.makeBoolean(builder, "allow_april_fools", "Allows April Fool's day effects to appear on April 1st", true);
			builder.pop();

			// TODO Bailey: Post-Beta secondary control option
			/*builder.push("Controls");
			StringBuilder mountString = new StringBuilder();
			mountString.append("Changes how the Red Dragon controls when ridden.");
			mountString.append("\n MOUSE: The Dragon's head follows yours. This is how mounts work normally, and is the default.");
			mountString.append("\n KEYS: The Dragon's direction is controlled by pressing left or right, completely separate from your looking direction. This may make it easier to use projectile weapons such as bows while moving.");
			
			this.redDragonControlType = ConfigBuilder.makeEnum(builder, "red_dragon_control_type", mountString.toString(), RedDragonOffspringEntity.ControlType.MOUSE);
			builder.pop();*/
		}

		/*public boolean isPanoramaCustom()
		{
			return this.customPanorama.get();
		}*/

		public boolean customSkylandsClouds()
		{
			return this.customSkylandsClouds.get();
		}

		public boolean overrideVanillaClouds()
		{
			return this.overrideVanillaClouds.get();
		}
		
		public int screenShakeIntensity()
		{
			return this.screenShakeIntensity.get();
		}

		public boolean allowAprilFools()
		{
			return this.allowAprilFools.get();
		}
		
		/*public RedDragonOffspringEntity.ControlType redDragonControlType()
		{
			return this.redDragonControlType.get();
		}*/
	}

	public static class CommonConfig
	{
		public CommonConfig(ModConfigSpec.Builder builder)
		{
			// var bus = FMLJavaModLoadingContext.get().getModEventBus();
		}
	}

	public static class WorldConfig
	{
		private final ConfigValueWrapper<Integer, Float> zombieHorseSiegePercentage, zombieHorseVillagePercentage;
		private final ConfigValueWrapper<Integer, Float> taggedPlateArmorPercentage;

		private final ConfigValue<Integer> maxGearDistance;
		private final ConfigValue<Boolean> hatchableRedDragon;
		private final ConfigValue<Boolean> redDragonStamina;

		public WorldConfig(ModConfigSpec.Builder builder)
		{
			IEventBus bus = RediscoveredMod.MOD_BUS;
			builder.push("Vanilla Additions");
			this.zombieHorseSiegePercentage = ConfigValueWrapper.create(ConfigBuilder.makeInt(builder, "zombie_horses_siege_spawn_percentage", "Chance that a Zombie in a village siege will spawn mounting a Zombie Horse.", 30, 0, 100), (i) -> i / 100F, bus, RediscoveredMod.MODID);
			this.zombieHorseVillagePercentage = ConfigValueWrapper.create(ConfigBuilder.makeInt(builder, "zombie_horses_village_replace_percentage", "Chance that mobs in the `#" + RediscoveredTags.Entities.BECOMES_ZOMBIE_HORSE_IN_ZOMBIE_VILLAGE.location() + "` tag will become a Zombie Horse when spawned in a zombie village.", 70, 0, 100), (i) -> i / 100F, bus, RediscoveredMod.MODID);
			this.taggedPlateArmorPercentage = ConfigValueWrapper.create(ConfigBuilder.makeInt(builder, "tagged_plate_armor_percentage", "Chance mobs in the `#" + RediscoveredTags.Entities.PLATE_ARMOR_SPAWNS.location() + "` tag will spawn with plate armor.", 5, 0, 100), (i) -> i / 100F, bus, RediscoveredMod.MODID);
			builder.pop();

			builder.push("Tweaks");
			this.maxGearDistance = ConfigBuilder.makeInt(builder, "max_gear_distance", "The maximum distance gears can be powered from a source. Set to -1 for unlimited distance.", 100, -1, 300);

			this.hatchableRedDragon = ConfigBuilder.makeBoolean(builder, "hatchable_red_dragon_egg", "Allow hatching of Red Dragons.", true);
			this.redDragonStamina = ConfigBuilder.makeBoolean(builder, "red_dragon_stamina", "Gives tamed Red Dragons stamina. This limits how far they can fly without touching the ground.\nA bar will be displayed above the rider's hotbar to display how much stamina the dragon has left.", true);
			/*this.redDragonNoClip = ConfigBuilder.makeBoolean(builder, "red_dragon_no_clip", "Allows the Red Dragon to go through walls, similar to the Ender Dragon. This can be dangerous when riding it.", false);*/
			builder.pop();
		}

		public float zombieHorseSiegePercentage()
		{
			return this.zombieHorseSiegePercentage.get();
		}

		public float zombieHorseVillagePercentage()
		{
			return this.zombieHorseVillagePercentage.get();
		}

		public float taggedPlateArmorPercentage()
		{
			return this.taggedPlateArmorPercentage.get();
		}

		public int maxGearDistance()
		{
			return this.maxGearDistance.get();
		}

		public boolean hatchableRedDragon()
		{
			return this.hatchableRedDragon.get();
		}

		public boolean redDragonStamina()
		{
			//return false;
			return this.redDragonStamina.get();
		}
	}
}
