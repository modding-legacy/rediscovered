package com.legacy.rediscovered.event.api;

import com.legacy.rediscovered.block_entities.TableBlockEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.world.item.ItemStack;
import net.neoforged.bus.api.Event;
import net.neoforged.bus.api.ICancellableEvent;

/**
 * Fired when a table renders the item on it. Cancel to handle the rendering
 * yourself.
 */
public class RenderTableItemEvent extends Event implements ICancellableEvent
{
	final TableBlockEntity table;
	final float partialTick;
	final PoseStack poseStack;
	final MultiBufferSource buffer;
	final int packedLight;
	final int packedOverlay;
	final ItemStack stack;

	public RenderTableItemEvent(TableBlockEntity table, float partialTick, PoseStack poseStack, MultiBufferSource buffer, int packedLight, int packedOverlay, ItemStack stack)
	{
		this.table = table;
		this.partialTick = partialTick;
		this.poseStack = poseStack;
		this.buffer = buffer;
		this.packedLight = packedLight;
		this.packedOverlay = packedOverlay;
		this.stack = stack;
	}

	public TableBlockEntity getTable()
	{
		return table;
	}

	public float getPartialTick()
	{
		return partialTick;
	}

	public PoseStack getPoseStack()
	{
		return poseStack;
	}

	public MultiBufferSource getBuffer()
	{
		return buffer;
	}

	public int getPackedLight()
	{
		return packedLight;
	}

	public int getPackedOverlay()
	{
		return packedOverlay;
	}

	public ItemStack getStack()
	{
		return stack;
	}
}
