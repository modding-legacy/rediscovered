package com.legacy.rediscovered.event.api;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.Event;

/**
 * Called when fake fire gets placed to modify what block should actually be
 * placed
 */
public class GetFakeFireEvent extends Event
{
	@Nullable
	final BlockState originalFire;
	@Nullable
	BlockState newFire;
	final BlockGetter level;
	final BlockPos pos;
	final BlockState groundBlock;

	public GetFakeFireEvent(@Nullable BlockState original, BlockGetter level, BlockPos pos, BlockState groundBlock)
	{
		this.originalFire = original;
		this.newFire = original;
		this.level = level;
		this.pos = pos;
		this.groundBlock = groundBlock;
	}

	/**
	 * @return The original fire to place, before modification
	 */
	@Nullable
	public BlockState getOriginal()
	{
		return this.originalFire;
	}

	/**
	 * @return The fire to place, or the original if no modification has been done
	 */
	@Nullable
	public BlockState getNewFire()
	{
		return this.newFire;
	}

	public void setFire(@Nullable BlockState state)
	{
		this.newFire = state;
	}

	public BlockGetter getLevel()
	{
		return this.level;
	}

	public BlockPos getPos()
	{
		return this.pos;
	}

	/**
	 * @return The block we're attempting to place fire on, for soul fire this would
	 *         be soul sand.
	 */
	public BlockState getGroundBlock()
	{
		return this.groundBlock;
	}
}
