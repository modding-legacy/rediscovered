package com.legacy.rediscovered.event;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredBlocks;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntityType;

public class RediscoveredMappingChanges
{
	public static void addBlockAliases(Registry<Block> registry)
	{
		add(registry, "cage_lantern", Blocks.LANTERN);
	}

	public static void addItemAliases(Registry<Item> registry)
	{
		add(registry, "cage_lantern", Items.LANTERN);
		add(registry, "dream_pillow", RediscoveredBlocks.ruby_eye);
	}

	private static void add(Registry<?> registry, ResourceLocation oldName, ResourceLocation newName)
	{
		registry.addAlias(oldName, newName);
	}

	private static void add(Registry<?> registry, String oldName, ResourceLocation newName)
	{
		add(registry, RediscoveredMod.locate(oldName), newName);
	}

	private static void add(Registry<Block> registry, String oldName, Block newValue)
	{
		add(registry, oldName, registry.getKey(newValue));
	}

	private static void add(Registry<Item> registry, String oldName, ItemLike newValue)
	{
		add(registry, oldName, registry.getKey(newValue.asItem()));
	}

	private static void add(Registry<BlockEntityType<?>> registry, String oldName, BlockEntityType<?> newValue)
	{
		add(registry, oldName, registry.getKey(newValue));
	}
}
