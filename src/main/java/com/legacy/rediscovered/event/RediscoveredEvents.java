package com.legacy.rediscovered.event;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.ChairBlock;
import com.legacy.rediscovered.block.SpikeBlock;
import com.legacy.rediscovered.block.util.IShovelFlattenable;
import com.legacy.rediscovered.block.util.ITillable;
import com.legacy.rediscovered.capability.util.QuiverDataHolder;
import com.legacy.rediscovered.client.gui.QuiverMenu;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.ScarecrowEntity;
import com.legacy.rediscovered.entity.ZombiePigmanEntity;
import com.legacy.rediscovered.entity.ai.AvoidScarecrowGoal;
import com.legacy.rediscovered.entity.ai.TargetScarecrowGoal;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.entity.util.IPigman;
import com.legacy.rediscovered.item.RubyFluteItem;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.registry.RediscoveredAttachmentTypes;
import com.legacy.rediscovered.registry.RediscoveredAttributes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredDamageTypes;
import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredEnchantments;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.legacy.rediscovered.util.UUIDHolder;
import com.legacy.structure_gel.api.events.LoadStructureTemplateEvent;
import com.legacy.structure_gel.api.structure.base.IModifyState;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawAccessHelper;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.core.FrontAndTop;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.HolderSet.Named;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.StructureTags;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.MobType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.Pig;
import net.minecraft.world.entity.animal.horse.ZombieHorse;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Giant;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.JigsawBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.levelgen.structure.PoolElementStructurePiece;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructurePiece;
import net.minecraft.world.level.levelgen.structure.StructureStart;
import net.minecraft.world.level.levelgen.structure.pools.SinglePoolElement;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.LogicalSide;
import net.neoforged.neoforge.common.CommonHooks;
import net.neoforged.neoforge.common.ToolActions;
import net.neoforged.neoforge.event.AnvilUpdateEvent;
import net.neoforged.neoforge.event.GrindstoneEvent;
import net.neoforged.neoforge.event.TickEvent.PlayerTickEvent;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.living.LivingAttackEvent;
import net.neoforged.neoforge.event.entity.living.LivingConversionEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.living.LivingGetProjectileEvent;
import net.neoforged.neoforge.event.entity.living.LivingHurtEvent;
import net.neoforged.neoforge.event.entity.living.MobSpawnEvent.FinalizeSpawn;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.neoforged.neoforge.event.level.BlockEvent.BlockToolModificationEvent;

public class RediscoveredEvents
{
	@SubscribeEvent
	public static void anvilUpdate(AnvilUpdateEvent event)
	{
		ItemStack left = event.getLeft();
		if (QuiverData.canAttachQuiver(left) && !AttachedItem.hasAttached(left) && event.getRight().is(RediscoveredTags.Items.QUIVERS))
		{
			ItemStack output = AttachedItem.attachItem(left.copy(), event.getRight().copy());

			if (!event.getName().isEmpty())
				output.setHoverName(Component.literal(event.getName()));

			int c = 0;
			if (left.hasCustomHoverName())
			{
				if (!Component.literal(event.getName()).toString().equals(left.getHoverName().toString()))
					c = 1;
			}
			else
			{
				if (!event.getName().isEmpty())
					c = 1;
			}

			event.setOutput(output);
			event.setCost(3 + c);
		}
	}

	@SubscribeEvent
	public static void grindstonePlace(GrindstoneEvent.OnPlaceItem event)
	{
		// Set the output to the chestplate without the quiver. Reward no xp for this.
		ItemStack top = event.getTopItem();
		ItemStack bottom = event.getBottomItem();

		ItemStack quiverItem = null;
		if (bottom.isEmpty() && AttachedItem.hasAttached(top))
			quiverItem = top;
		else if (top.isEmpty() && AttachedItem.hasAttached(bottom))
			quiverItem = bottom;
		if (quiverItem != null)
		{
			event.setOutput(AttachedItem.removeAttachment(quiverItem.copy()));
			event.setXp(0);
			return; // Skip remaining logic
		}

		// Set the output to a ruby flute without the bound entity
		ItemStack fluteItem = null;
		if (bottom.isEmpty() && top.is(RediscoveredItems.ruby_flute) && RubyFluteItem.getTargetUUID(top) != null)
			fluteItem = top;
		else if (top.isEmpty() && bottom.is(RediscoveredItems.ruby_flute) && RubyFluteItem.getTargetUUID(bottom) != null)
			fluteItem = bottom;
		if (fluteItem != null)
		{
			event.setOutput(RubyFluteItem.setTarget(fluteItem.copy(), null));
			event.setXp(0);
			return; // Skip remaining logic
		}
	}

	@SubscribeEvent
	public static void grindstoneTake(GrindstoneEvent.OnTakeItem event)
	{
		// If we took a quiver off an item, place the original quiver back into the
		// input slot
		ItemStack top = event.getTopItem();
		ItemStack bottom = event.getBottomItem();
		boolean wasTopSlot = true;
		ItemStack quiverItem = null;
		if (bottom.isEmpty() && AttachedItem.hasAttached(top))
		{
			quiverItem = top;
		}
		else if (top.isEmpty() && AttachedItem.hasAttached(bottom))
		{
			quiverItem = bottom;
			wasTopSlot = false;
		}

		if (quiverItem != null)
		{
			if (wasTopSlot)
				event.setNewTopItem(AttachedItem.getOrCreate(top).get().getAttached().copy());
			else
				event.setNewBottomItem(AttachedItem.getOrCreate(bottom).get().getAttached().copy());

			// The crafting player gets set in GrindstoneMenuMixin
			if (CommonHooks.getCraftingPlayer() instanceof ServerPlayer player)
				RediscoveredTriggers.REMOVE_QUIVER.get().trigger(player);
		}
	}

	@SubscribeEvent
	public static void onLivingAttack(LivingAttackEvent event)
	{
		var entity = event.getEntity();
		if (!entity.level().isClientSide() && entity.level().dimension() == RediscoveredDimensions.skylandsKey() && event.getSource() == entity.damageSources().fellOutOfWorld())
		{
			if (entity.getY() <= 10)
				event.setCanceled(SkyTransitionTeleporter.doTeleport(entity, Level.OVERWORLD));
		}
	}

	@SubscribeEvent
	public static void onHurt(LivingHurtEvent event)
	{
		LivingEntity target = event.getEntity();
		DamageSource damageSource = event.getSource();

		/*Reduce damage from undead sources. If the MobType check breaks, it's probably a tag now.*/
		if (damageSource.is(RediscoveredTags.DamageTypes.IS_DEATHLY) || (damageSource.getDirectEntity() instanceof LivingEntity living && living.getMobType() == MobType.UNDEAD))
		{
			float goldAuraDamageScaling = (float) getInvertAttribute(target, RediscoveredAttributes.UNDEAD_DAMAGE_SCALING.get());
			if (goldAuraDamageScaling != 1)
			{
				// System.out.println("Gold " + goldAuraDamageScaling);
				event.setAmount(event.getAmount() * goldAuraDamageScaling);
			}
		}

		// Increase damage recieved when crimson veil is on the target
		float crimsonDamageScaling = (float) getInvertAttribute(target, RediscoveredAttributes.CRIMSON_VEIL_DAMAGE_SCALING.get());
		if (crimsonDamageScaling != 1)
		{
			// System.out.println("Crimson " + crimsonDamageScaling);
			event.setAmount(event.getAmount() * crimsonDamageScaling);
		}

		// Decrease damage from explosions
		if (damageSource.is(DamageTypeTags.IS_EXPLOSION))
		{
			float explosionDamageScaling = (float) getInvertAttribute(target, RediscoveredAttributes.EXPLOSION_RESISTANCE.get());
			if (explosionDamageScaling != 1)
			{
				// System.out.println("Explosion " + explosionDamageScaling);
				event.setAmount(event.getAmount() * explosionDamageScaling);
			}
		}

		// Decrease damage from fire
		if (damageSource.is(DamageTypeTags.IS_FIRE))
		{
			float fireDamageScaling = (float) getInvertAttribute(target, RediscoveredAttributes.FIRE_RESISTANCE.get());
			if (fireDamageScaling != 1)
			{
				// System.out.println("Fire " + fireDamageScaling);
				event.setAmount(event.getAmount() * fireDamageScaling);
			}
		}
	}

	// This is to make the attribute invert so we can have the potion item tooltip
	// look proper but also have the logic work the way it should.
	private static double getInvertAttribute(LivingEntity entity, Attribute attribute)
	{
		if (entity.getAttributes().hasAttribute(attribute)) // Should always pass since every LivingEntity SHOULD have
															// this. But if they don't, return 1 to do nothing.
		{
			double baseVal = entity.getAttributeBaseValue(attribute);
			double val = entity.getAttributeValue(attribute);
			return baseVal - (val - baseVal);
		}
		return 1.0;
	}

	@SubscribeEvent
	public static void onPlayerInteract(PlayerInteractEvent.RightClickBlock event)
	{
		Player player = event.getEntity();
		ItemStack itemStack = event.getItemStack();
		Level level = event.getEntity().level();
		BlockPos pos = event.getPos();
		BlockState state = level.getBlockState(pos);

		if (level.dimension() == RediscoveredDimensions.skylandsKey() && state.is(BlockTags.BEDS))
		{
			event.setCancellationResult(InteractionResult.PASS);
			event.setCanceled(true);
		}

		// Bush Shearing
		if (shearDoublePlant(level, state, pos, itemStack, player))
		{
			event.setCanceled(true);
			player.swing(event.getHand(), true);
			itemStack.hurtAndBreak(1, player, (playerEntity) -> playerEntity.broadcastBreakEvent(event.getHand()));
		}
	}

	public static boolean shearDoublePlant(Level level, BlockState state, BlockPos pos, ItemStack shears, @Nullable Entity actor)
	{
		if (state.getBlock() instanceof DoublePlantBlock && shears.canPerformAction(ToolActions.SHEARS_HARVEST))
		{
			ItemLike item = shearDoublePlant(level, state, pos, shears, actor, Blocks.ROSE_BUSH, RediscoveredBlocks.empty_rose_bush, RediscoveredBlocks.rose);
			if (item == null)
				item = shearDoublePlant(level, state, pos, shears, actor, Blocks.PEONY, RediscoveredBlocks.empty_peony_bush, RediscoveredBlocks.paeonia);

			if (item != null)
			{
				ItemStack flowerStack = new ItemStack(item, level.getRandom().nextInt(3) + 1);
				ItemEntity itemEntity = new ItemEntity(level, pos.getX() + 0.5F, pos.getY() + 0.5F, pos.getZ() + 0.5F, flowerStack);
				itemEntity.setDefaultPickUpDelay();
				level.addFreshEntity(itemEntity);
				level.playSound(null, pos, SoundEvents.SHEEP_SHEAR, SoundSource.BLOCKS);
				return true;
			}
		}
		return false;
	}

	private static ItemLike shearDoublePlant(Level level, BlockState state, BlockPos pos, ItemStack shears, @Nullable Entity actor, Block test, Block empty, ItemLike droppedItem)
	{
		if (state.is(test))
		{
			if (actor instanceof ServerPlayer serverPlayer)
				CriteriaTriggers.ITEM_USED_ON_BLOCK.trigger(serverPlayer, pos, shears);
			level.gameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Context.of(actor, level.getBlockState(pos)));

			int flags = Block.UPDATE_CLIENTS + Block.UPDATE_KNOWN_SHAPE;
			BlockState emptyState = empty.defaultBlockState();
			level.setBlock(pos, IModifyState.mergeStates(emptyState, state), flags);

			if ((state = level.getBlockState(pos.above())).is(test))
				level.setBlock(pos.above(), IModifyState.mergeStates(emptyState, state), flags);

			if ((state = level.getBlockState(pos.below())).is(test))
				level.setBlock(pos.below(), IModifyState.mergeStates(emptyState, state), flags);

			return droppedItem;
		}
		return null;
	}

	@SubscribeEvent
	public static void onToolAction(BlockToolModificationEvent event)
	{
		if (event.getToolAction() == ToolActions.SHOVEL_FLATTEN)
		{
			BlockState state = event.getFinalState();
			if (state.getBlock() instanceof IShovelFlattenable flattenable)
			{
				event.setFinalState(flattenable.getShovelFlattenedState(state, event.getLevel(), event.getPos()));
			}
		}
		else if (event.getToolAction() == ToolActions.HOE_TILL)
		{
			BlockState state = event.getFinalState();
			if (state.getBlock() instanceof ITillable tillable)
			{
				event.setFinalState(tillable.getHoeTilledState(state, event.getLevel(), event.getPos()));
			}
		}
	}

	@SubscribeEvent
	public static void onJoinLevel(EntityJoinLevelEvent event)
	{
		Entity entity = event.getEntity();
		EntityType<?> type = entity.getType();

		if (!type.is(RediscoveredTags.Entities.IGNORES_SCARECROW) && entity instanceof PathfinderMob pfMob)
		{
			if (type.is(RediscoveredTags.Entities.TARGETS_SCARECROW))
			{
				// System.out.println("Targets scarecrow: " + entity);
				pfMob.targetSelector.addGoal(2, new TargetScarecrowGoal<>(pfMob, ScarecrowEntity.class, true));
			}
			else if (type.is(RediscoveredTags.Entities.FEARS_SCARECROW) || (type.getCategory() != MobCategory.MONSTER && pfMob.goalSelector.getAvailableGoals().stream().filter(wrapped -> wrapped.getGoal() instanceof PanicGoal).findAny().isPresent()))
			{
				//RediscoveredMod.LOGGER.info("Fears scarecrow: " + entity);
				pfMob.goalSelector.addGoal(1, new AvoidScarecrowGoal<>(pfMob, ScarecrowEntity.class, 8.0F, 1.0D, 1.0D));
			}
		}
	}

	@SubscribeEvent
	public static void onEntityCheckSpawn(FinalizeSpawn event)
	{
		Mob mob = event.getEntity();
		EntityType<?> type = mob.getType();
		var level = event.getLevel();
		BlockPos pos = BlockPos.containing(event.getX(), event.getY(), event.getZ());

		// --- Zombie Horses in sieges ---
		if (mob.getRandom().nextFloat() < RediscoveredConfig.WORLD.zombieHorseSiegePercentage() && event.getSpawnType() == MobSpawnType.EVENT && level.canSeeSky(pos) && type == EntityType.ZOMBIE)
		{
			ZombieHorse horse = EntityType.ZOMBIE_HORSE.create(mob.level());
			horse.setTamed(true);
			horse.copyPosition(mob);

			level.addFreshEntity(horse);
			mob.startRiding(horse, true);
		}

		// --- Zombie Horses in Zombie Villages ---
		if (event.getSpawnType() == MobSpawnType.STRUCTURE && type.is(RediscoveredTags.Entities.BECOMES_ZOMBIE_HORSE_IN_ZOMBIE_VILLAGE) && mob.getRandom().nextFloat() < RediscoveredConfig.WORLD.zombieHorseVillagePercentage())
		{
			// Tons of "if not null" and "is present" to figure out if we're in a zombie
			// village.
			// Start by figuring out if we're even inside a village or not. If we are, check
			// the PieceContainer of the StructureStart to see if any "zombie" pieces are
			// here. If so, it's probably a zombie village
			StructureManager structureManager = level instanceof ServerLevel sl ? sl.structureManager() : level instanceof WorldGenRegion wg ? level.getLevel().structureManager().forWorldGenRegion(wg) : null;
			if (structureManager != null)
			{
				Optional<Named<Structure>> villageTag = level.registryAccess().registryOrThrow(Registries.STRUCTURE).getTag(StructureTags.VILLAGE);
				if (villageTag.isPresent())
				{
					Iterator<Holder<Structure>> villageIt = villageTag.get().iterator();
					while (villageIt.hasNext())
					{
						Holder<Structure> village = villageIt.next();
						StructureStart start = structureManager.getStructureAt(pos, village.value());
						if (start.isValid())
						{
							int pieceCount = start.getPieces().size();
							int zombiePieces = 0;
							for (StructurePiece piece : start.getPieces())
							{
								try
								{
									if (piece instanceof PoolElementStructurePiece poolElement)
									{
										var element = poolElement.getElement();
										if (element instanceof SinglePoolElement singleElement)
										{
											ResourceLocation name = JigsawAccessHelper.getSingleJigsawPieceLocation(singleElement);
											// System.out.println(name);

											// Look for pieces in the "zombie" folder. Every zombie village is
											// structured this way, so it's the best we can do.
											// To safeguard against random "zombie" buildings from other mods, we
											// increase zombiePieces. If a large enough percentage of the village
											// contains zombie pieces, then it's allowed to replace the animal
											if (name.getPath().contains("zombie/"))
											{
												zombiePieces++;
											}
										}
									}
								}
								catch (Exception e)
								{
									// Silent catch. JigsawAccessHelper.getSingleJigsawPieceLocation is kinda coded
									// badly and _could_ fail. I need to fix that. -David
								}
							}

							// If at least 30% of the pieces in the village are zombie village pieces, so we
							// can replace. In testing, most zombie villages have at least 40% of pieces
							// being zombie pieces
							if (pieceCount > 0 && zombiePieces / (float) pieceCount >= 0.3)
							{
								ZombieHorse horse = EntityType.ZOMBIE_HORSE.create(mob.level());
								horse.setTamed(true);
								horse.copyPosition(mob);

								level.addFreshEntity(horse);
								event.setSpawnCancelled(true);
								// System.out.println("Replaced " + mob.getName().getString() + " with " +
								// horse.getName().getString() + " at /tp " + pos.getX() + " ~ " + pos.getZ());
								break;
							}
						}
					}
				}
			}
		}

		if (mob instanceof PathfinderMob pfMob)
		{
			if (mob instanceof Giant)
			{
				pfMob.setYRot(Mth.wrapDegrees(level.getRandom().nextFloat() * 360.0F));
				pfMob.yHeadRot = pfMob.getYRot();
				pfMob.yBodyRot = pfMob.getYRot();

				pfMob.getAttribute(Attributes.ATTACK_DAMAGE).setBaseValue(10.0F);
				pfMob.getAttribute(Attributes.KNOCKBACK_RESISTANCE).setBaseValue(1F);

				pfMob.goalSelector.addGoal(5, new MeleeAttackGoal(pfMob, 0.5D, true));
				pfMob.goalSelector.addGoal(7, new WaterAvoidingRandomStrollGoal(pfMob, 0.5D));
				pfMob.goalSelector.addGoal(8, new LookAtPlayerGoal(pfMob, Player.class, 8.0F));
				pfMob.goalSelector.addGoal(8, new RandomLookAroundGoal(pfMob));
				pfMob.targetSelector.addGoal(1, (new HurtByTargetGoal(pfMob)));
				pfMob.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(pfMob, Player.class, true));
			}

			if (type == EntityType.ZOMBIFIED_PIGLIN && event.getSpawnType() == MobSpawnType.NATURAL && level.getRandom().nextFloat() < 0.2F && !mob.isPassenger())
			{
				ZombiePigmanEntity pigman = RediscoveredEntityTypes.ZOMBIE_PIGMAN.create(mob.level());
				pigman.copyPosition(mob);
				pigman.finalizeSpawn(event.getLevel(), event.getDifficulty(), event.getSpawnType(), event.getSpawnData(), event.getSpawnTag());
				mob.level().addFreshEntity(pigman);

				event.setSpawnCancelled(true);
				event.setCanceled(true);
			}
		}

		Set<MobSpawnType> plateArmorTypes = Set.of(MobSpawnType.NATURAL, MobSpawnType.CHUNK_GENERATION, MobSpawnType.SPAWN_EGG, MobSpawnType.SPAWNER);
		if (plateArmorTypes.contains(event.getSpawnType()) && mob.getType().is(RediscoveredTags.Entities.PLATE_ARMOR_SPAWNS) && mob.getRandom().nextFloat() < RediscoveredConfig.WORLD.taggedPlateArmorPercentage())
		{
			// Pick 2 random pieces of plate armor
			List<Item> plateArmor = new ArrayList<>(level.registryAccess().registryOrThrow(Registries.ITEM).getTag(RediscoveredTags.Items.PLATE_ARMOR).stream().flatMap(HolderSet::stream).map(Holder::value).toList());
			for (int i = 2; i > 0 && !plateArmor.isEmpty(); i--)
			{
				// Get a random armor item and remove it from the list of options
				ItemStack armor = plateArmor.remove(mob.getRandom().nextInt(plateArmor.size())).getDefaultInstance();
				EquipmentSlot slot = LivingEntity.getEquipmentSlotForItem(armor);
				// Only replace empty slots.
				if (slot != null && mob.getItemBySlot(slot).isEmpty())
					mob.setItemSlot(slot, armor);
			}
		}
	}

	@SubscribeEvent
	public static void onPlayerRightClick(RightClickItem event)
	{
		ItemStack stack = event.getItemStack();
		Player player = event.getEntity();

		if (stack.canApplyAtEnchantingTable(RediscoveredEnchantments.RAPID_SHOT) && stack.getEnchantmentLevel(RediscoveredEnchantments.RAPID_SHOT) > 0)
		{
			stack.getItem().releaseUsing(stack, player.level(), player, 72000 - 11);
			player.swing(event.getHand(), true);
			event.setCanceled(true);
		}

		Optional<AttachedItem> attachedItem = AttachedItem.get(stack);
		if (attachedItem.isPresent())
		{
			ItemStack attached = attachedItem.get().getAttached();
			if (attached.is(RediscoveredTags.Items.QUIVERS) && player.isSecondaryUseActive())
			{
				QuiverData quiverData = QuiverData.getOrCreate(attached);
				quiverData.setAttachedItem(attachedItem.get());
				if (player instanceof ServerPlayer serverPlayer)
				{
					serverPlayer.openMenu(new QuiverMenu.Provider(stack, quiverData));
				}
				event.setCanceled(true);
				event.setCancellationResult(InteractionResult.SUCCESS);
			}
		}
	}

	@SubscribeEvent
	public static void getProjectile(LivingGetProjectileEvent event)
	{
		ItemStack weapon = event.getProjectileWeaponItemStack();
		if (weapon.is(RediscoveredTags.Items.QUIVER_USER))
		{
			LivingEntity entity = event.getEntity();
			ItemStack originalArrow = event.getProjectileItemStack();
			if (!originalArrow.isEmpty() && (originalArrow == entity.getItemInHand(InteractionHand.OFF_HAND) || originalArrow == entity.getItemInHand(InteractionHand.MAIN_HAND)))
				return;
			Optional<QuiverData> quiverData = QuiverData.getFromChestplate(entity.getItemBySlot(EquipmentSlot.CHEST));
			if (quiverData.isPresent())
			{
				QuiverData data = quiverData.get();
				ItemStack ammo = data.getSelectedAmmo(weapon);
				if (!ammo.isEmpty())
				{
					((QuiverDataHolder) (Object) ammo).rediscovered$setQuiverData(data, weapon);
					event.setProjectileItemStack(ammo);
				}
			}
		}
	}

	@SubscribeEvent
	public static void onPlayerTick(PlayerTickEvent event)
	{
		Player player = event.player;
		if (player instanceof ServerPlayer)
		{
			ServerPlayer sp = (ServerPlayer) player;
			if (player.tickCount % 100 == 0)
				RediscoveredTriggers.IN_FARLANDS_TRIGGER.get().trigger(sp);

			if (player.getVehicle() instanceof RedDragonOffspringEntity && player.getY() > player.level().getMaxBuildHeight() + 20 && player.level().dimension() == Level.OVERWORLD)
				SkyTransitionTeleporter.doTeleport(player, RediscoveredDimensions.skylandsKey());

			Optional<UUID> dragonTp = player.getData(RediscoveredAttachmentTypes.DRAGON_TELEPORT).uuid();
			if (dragonTp.isPresent())
			{
				UUID dragonUUID = dragonTp.get();
				if (dragonUUID != null)
				{
					Entity possibleDragon = sp.serverLevel().getEntity(dragonUUID);
					if (possibleDragon != null)
					{
						if (possibleDragon instanceof RedDragonOffspringEntity dragon)
						{
							sp.startRiding(dragon, true);
							dragon.setFlying(true);
							dragon.dismountDelay = 50;
						}
						player.setData(RediscoveredAttachmentTypes.DRAGON_TELEPORT, UUIDHolder.EMPTY);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public static void onRightClickBlock(PlayerInteractEvent.RightClickBlock event)
	{
		Player player = event.getEntity();
		Level world = event.getLevel();
		BlockPos pos = event.getPos();
		BlockState state = world.getBlockState(pos);

		if (!player.isSecondaryUseActive() && state.is(RediscoveredTags.Blocks.CHAIRS))
		{
			if (event.getSide() == LogicalSide.CLIENT)
			{
				event.setCancellationResult(InteractionResult.SUCCESS);
				event.setCanceled(true);
			}

			ChairBlock.tryInteract(player, pos, event.getHand());

			event.setCancellationResult(InteractionResult.SUCCESS);
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public static void onPlayerInteract(PlayerInteractEvent.EntityInteract event)
	{
	}

	@SubscribeEvent
	public static void onEntityDeath(LivingDeathEvent event)
	{
		LivingEntity entity = event.getEntity();
		Level level = entity.level();
		if (entity instanceof IPigman && entity instanceof Mob mob && IPigman.zombify(mob, event.getSource()))
		{
			event.setCanceled(true);
			return;
		}

		if (event.getSource().is(RediscoveredDamageTypes.SPIKES.getKey()) && level instanceof ServerLevel)
			SpikeBlock.tryAwardKillingAdvancement((ServerLevel) level, entity, event.getSource());
	}

	public static BlockPos verifyRespawnCoordinates(Level par0World, BlockPos par1ChunkCoordinates, boolean par2, ServerPlayer player)
	{
		if (player.getServer().getLevel(Level.OVERWORLD).getBlockState(par1ChunkCoordinates).getBlock() instanceof BedBlock)
			return par1ChunkCoordinates;

		return null;
	}

	@SubscribeEvent
	public static void livingConversionPre(LivingConversionEvent.Pre event)
	{
		var entity = event.getEntity();
		if (RediscoveredAttributes.isConversionImmune(entity))
		{
			event.setCanceled(true);
		}

		/*if (event.getEntity() instanceof ZombieVillager zomb && zomb.hasEffect(RediscoveredEffects.GOLDEN_AURA.get()))
		{
			event.setConversionTimer(zomb.villagerConversionTime - 1);
		}*/
	}

	@SubscribeEvent
	public static void livingConversionPost(LivingConversionEvent.Post event)
	{
		if (event.getEntity() instanceof Pig && event.getOutcome() instanceof Mob mob && mob.getType() == EntityType.ZOMBIFIED_PIGLIN)
		{
			ZombiePigmanEntity pigman = mob.convertTo(RediscoveredEntityTypes.ZOMBIE_PIGMAN, false);
			pigman.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(Items.IRON_SWORD));
			mob.level().addFreshEntity(pigman);
			mob.discard();
		}
	}

	@SuppressWarnings("deprecation")
	@SubscribeEvent
	public static void onTemplateLoad(LoadStructureTemplateEvent event) // StructureGel
	{
		if (event.getId().equals(new ResourceLocation("trail_ruins/roads/long_road_end"))) // Add a jigsaw to the end of
																							// a trail ruins large road
																							// end. This piece alwayas
																							// generates behind a tower
		{
			for (int z = 1; z < event.getSize().getZ(); z++)
			{
				event.setBlock(new BlockPos(0, 0, z), Blocks.COBBLESTONE.defaultBlockState(), null);
				event.setBlock(new BlockPos(0, 1, z), Blocks.GRAVEL.defaultBlockState(), null);
				event.setBlock(new BlockPos(0, 2, z), Blocks.GRAVEL.defaultBlockState(), null);
			}

			event.setJigsaw(Blocks.JIGSAW.defaultBlockState().setValue(JigsawBlock.ORIENTATION, FrontAndTop.WEST_UP), event.moveInBounds(new BlockPos(0, 1, 3)), jigsaw ->
			{
				jigsaw.setPool(ResourceKey.create(Registries.TEMPLATE_POOL, RediscoveredMod.locate("skylands_portal/trail_ruins/portal_room")));
				jigsaw.setName(RediscoveredMod.locate("portal_room_connector"));
				jigsaw.setTarget(RediscoveredMod.locate("portal_room"));
				jigsaw.setFinalState(Blocks.GRAVEL.builtInRegistryHolder().key().location().toString());
			});
		}
	}
}
