package com.legacy.rediscovered.event;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.registry.RediscoveredAttachmentTypes;
import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.legacy.rediscovered.util.UUIDHolder;

import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.TicketType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.portal.PortalInfo;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.util.ITeleporter;

public record SkyTransitionTeleporter(boolean atMax) implements ITeleporter
{
	public static final SkyTransitionTeleporter AT_TOP = new SkyTransitionTeleporter(true);
	public static final SkyTransitionTeleporter AT_BOTTOM = new SkyTransitionTeleporter(false);

	@Override
	public PortalInfo getPortalInfo(Entity entity, ServerLevel destWorld, Function<ServerLevel, PortalInfo> defaultPortalInfo)
	{
		// System.out.println("putting at max: " + this.atMax());
		return new PortalInfo(new Vec3(entity.getX(), this.atMax() ? destWorld.getMaxBuildHeight() + 5 : destWorld.getMinBuildHeight() - 50, entity.getZ()), Vec3.ZERO, entity.getYRot(), entity.getXRot());
	}

	public static boolean doTeleport(LivingEntity entity, ResourceKey<Level> dimension)
	{
		if (entity instanceof ServerPlayer)
		{
			ServerPlayer player = (ServerPlayer) entity;
			Entity vehicle = player.getVehicle();
			if (player.getVehicle() instanceof RedDragonOffspringEntity)
			{
				RediscoveredMod.LOGGER.debug("--- Red Dragon Sky Transition ({}) ---", player.getName().getString());

				RedDragonOffspringEntity dragon = (RedDragonOffspringEntity) vehicle;
				boolean goingToSkylands = dimension == RediscoveredDimensions.skylandsKey();
				ITeleporter teleporter = goingToSkylands ? AT_BOTTOM : AT_TOP;

				ServerLevel destLevel = player.getServer().getLevel(dimension);
				ChunkPos destChunk = new ChunkPos(player.blockPosition());
				destLevel.getChunkSource().addRegionTicket(TicketType.POST_TELEPORT, destChunk, 2, dragon.getId());

				List<Player> passengers = new ArrayList<>(2);
				for (var passenger : dragon.getPassengers())
				{
					if (passenger instanceof Player)
						passengers.add((Player) passenger);
					passenger.unRide();
				}
				dragon.unRide();

				List<ServerPlayer> teleportedPlayers = new ArrayList<>(2);
				for (Player passengerPlayer : passengers)
				{
					RediscoveredMod.LOGGER.debug(passengerPlayer.getDisplayName().getString() + "'s Before: + " + player.position());
					Entity teleportedPlayer = passengerPlayer.changeDimension(destLevel, teleporter);
					RediscoveredMod.LOGGER.debug(teleportedPlayer.getDisplayName().getString() + "'s After: + " + teleportedPlayer.position());
					RediscoveredMod.LOGGER.debug("------------------");

					if (teleportedPlayer instanceof ServerPlayer)
						teleportedPlayers.add((ServerPlayer) teleportedPlayer);
				}

				RediscoveredMod.LOGGER.debug("Dragon's Before: + " + dragon.position());
				Entity teleportedDragon = dragon.changeDimension(destLevel, teleporter);
				RediscoveredMod.LOGGER.debug("Dragon's After: + " + teleportedDragon.position());

				if (teleportedDragon instanceof RedDragonOffspringEntity)
				{
					for (ServerPlayer teleportedPlayer : teleportedPlayers)
					{
						teleportedPlayer.setData(RediscoveredAttachmentTypes.DRAGON_TELEPORT, UUIDHolder.of(teleportedDragon.getUUID()));
						if (!goingToSkylands)
							RediscoveredTriggers.DRAGON_TELEPORT.get().trigger(teleportedPlayer);
					}
				}

				return true;
			}
			else
			{
				Vec3 motion = player.getDeltaMovement();
				ServerLevel ow = player.getServer().getLevel(Level.OVERWORLD);
				player.invulnerableTime = 20;
				player.horizontalCollision = false;
				player.teleportTo(ow, player.getX(), ow.getMaxBuildHeight(), player.getZ(), player.getYRot(), player.getXRot());
				player.invulnerableTime = 20;
				player.horizontalCollision = false;
				player.setDeltaMovement(motion);
				player.hurtMarked = true;

				return true;
			}
		}
		else if (entity instanceof RedDragonOffspringEntity dragon)
		{
			if (!dragon.isVehicle() && dragon.level().dimension().equals(RediscoveredDimensions.skylandsKey()))
				dragon.changeDimension(dragon.getServer().getLevel(dimension), AT_TOP);

			return true;
		}

		return false;
	}
}
