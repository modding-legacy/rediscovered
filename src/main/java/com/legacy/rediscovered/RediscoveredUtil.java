package com.legacy.rediscovered;

import java.util.Optional;
import java.util.function.Function;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;

public class RediscoveredUtil
{
	private static Codec<Float> floatRangeWithMessage(float min, float max, Function<Float, String> errorMessage)
	{
		return ExtraCodecs.validate(Codec.FLOAT, f -> f.compareTo(min) >= 0 && f.compareTo(max) <= 0 ? DataResult.success(f) : DataResult.error(() -> errorMessage.apply(f)));
	}

	public static final Codec<Float> NON_NEGATIVE_FLOAT = floatRangeWithMessage(0.0F, Float.MAX_VALUE, f -> "Value must be positive: " + f);

	public static <T> Optional<Holder<T>> getRandom(ResourceKey<Registry<T>> registry, TagKey<T> tag, RegistryAccess registryAccess, RandomSource rand)
	{
		return registryAccess.registryOrThrow(registry).getTag(tag).flatMap(t -> t.getRandomElement(rand));
	}

	public static Optional<Holder<Block>> getRandomBlock(TagKey<Block> tag, ServerLevelAccessor level, RandomSource rand)
	{
		return getRandom(Registries.BLOCK, tag, level.registryAccess(), rand);
	}

	public static Optional<Holder<Item>> getRandomItem(TagKey<Item> tag, ServerLevelAccessor level, RandomSource rand)
	{
		return getRandom(Registries.ITEM, tag, level.registryAccess(), rand);
	}
}
