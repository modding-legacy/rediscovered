package com.legacy.rediscovered.network.s_to_c;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public class SendScreenShakePacket implements PacketHandler.ModPacket<SendScreenShakePacket, SendScreenShakePacket.Handler>
{
	private final float strength;

	public SendScreenShakePacket(float strength)
	{
		this.strength = strength;
	}

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<SendScreenShakePacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("send_screen_shake");
		}

		@Override
		public void encoder(SendScreenShakePacket packet, FriendlyByteBuf buff)
		{
			buff.writeFloat(packet.strength);
		}

		@Override
		public SendScreenShakePacket decoder(FriendlyByteBuf buff)
		{
			return new SendScreenShakePacket(buff.readFloat());
		}

		@Override
		public void handler(SendScreenShakePacket packet, PlayPayloadContext context)
		{
			if (FMLEnvironment.dist == Dist.CLIENT)
				context.workHandler().submitAsync(() -> handlePacket(packet));
		}

		@OnlyIn(Dist.CLIENT)
		private static void handlePacket(SendScreenShakePacket packet)
		{
			try
			{
				com.legacy.rediscovered.client.RediscoveredClientEvents.shakeStrength += packet.strength;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
