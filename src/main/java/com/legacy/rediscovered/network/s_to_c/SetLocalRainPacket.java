package com.legacy.rediscovered.network.s_to_c;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.network.PacketHandler;
import com.legacy.rediscovered.registry.RediscoveredAttachmentTypes;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public class SetLocalRainPacket implements PacketHandler.ModPacket<SetLocalRainPacket, SetLocalRainPacket.Handler>
{
	private final int entity;

	public SetLocalRainPacket(int id)
	{
		this.entity = id;
	}

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<SetLocalRainPacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("set_local_rain");
		}

		public void encoder(SetLocalRainPacket packet, FriendlyByteBuf buff)
		{
			buff.writeInt(packet.entity);
		}

		public SetLocalRainPacket decoder(FriendlyByteBuf buff)
		{
			return new SetLocalRainPacket(buff.readInt());
		}

		@Override
		public void handler(SetLocalRainPacket packet, PlayPayloadContext context)
		{
			if (FMLEnvironment.dist == Dist.CLIENT)
				context.workHandler().submitAsync(() -> handlePacket(packet, context));
		}

		@OnlyIn(Dist.CLIENT)
		private static void handlePacket(SetLocalRainPacket packet, PlayPayloadContext context)
		{
			Level level = net.minecraft.client.Minecraft.getInstance().level;
			Entity entity = level.getEntity(packet.entity);
			if (entity != null)
				entity.setData(RediscoveredAttachmentTypes.RAIN_TIME, level.getGameTime());
		}
	}
}
