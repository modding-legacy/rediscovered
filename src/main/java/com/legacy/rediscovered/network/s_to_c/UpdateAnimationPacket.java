package com.legacy.rediscovered.network.s_to_c;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.util.animation.IAnimated;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public class UpdateAnimationPacket implements PacketHandler.ModPacket<UpdateAnimationPacket, UpdateAnimationPacket.Handler>
{
	private int entityId, animId;
	private boolean shouldPlay;

	public UpdateAnimationPacket(int entityId, int animId, boolean shouldPlay)
	{
		this.entityId = entityId;
		this.animId = animId;
		this.shouldPlay = shouldPlay;
	}

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<UpdateAnimationPacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("update_animation");
		}

		@Override
		public void encoder(UpdateAnimationPacket packet, FriendlyByteBuf buff)
		{
			buff.writeInt(packet.entityId);
			buff.writeInt(packet.animId);
			buff.writeBoolean(packet.shouldPlay);
		}

		@Override
		public UpdateAnimationPacket decoder(FriendlyByteBuf buff)
		{
			return new UpdateAnimationPacket(buff.readInt(), buff.readInt(), buff.readBoolean());
		}

		@Override
		public void handler(UpdateAnimationPacket packet, PlayPayloadContext context)
		{
			if (FMLEnvironment.dist == Dist.CLIENT)
				context.workHandler().submitAsync(() -> handlePacket(packet));

		}

		@SuppressWarnings("resource")
		@OnlyIn(Dist.CLIENT)
		private static void handlePacket(UpdateAnimationPacket packet)
		{
			try
			{
				if (net.minecraft.client.Minecraft.getInstance().level.getEntity(packet.entityId) instanceof IAnimated animated)
				{
					animated.getAnimations().get(packet.animId).forcePlay(packet.shouldPlay);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
