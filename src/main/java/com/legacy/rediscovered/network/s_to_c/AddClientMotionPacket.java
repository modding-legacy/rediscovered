package com.legacy.rediscovered.network.s_to_c;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.client.player.LocalPlayer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

/**
 * A way to add motion on the client. This is done to allow us to not have to
 * use entity.hurtMarked, which basically nullifies user input. This should only
 * be used if a constant change in motion is needed (EX: pulling something
 * towards another entity). If we need to do something like a throw, or a one
 * time motion change, just use hurtMarked.
 */
public class AddClientMotionPacket implements PacketHandler.ModPacket<AddClientMotionPacket, AddClientMotionPacket.Handler>
{
	private double x;
	private double y;
	private double z;

	public AddClientMotionPacket(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<AddClientMotionPacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("add_client_motion");
		}

		@Override
		public void encoder(AddClientMotionPacket packet, FriendlyByteBuf buff)
		{
			buff.writeDouble(packet.x);
			buff.writeDouble(packet.y);
			buff.writeDouble(packet.z);
		}

		@Override
		public AddClientMotionPacket decoder(FriendlyByteBuf buff)
		{
			return new AddClientMotionPacket(buff.readDouble(), buff.readDouble(), buff.readDouble());
		}

		@Override
		public void handler(AddClientMotionPacket packet, PlayPayloadContext context)
		{
			if (FMLEnvironment.dist == Dist.CLIENT)
				context.workHandler().submitAsync(() -> handlePacket(packet));
		}

		@OnlyIn(Dist.CLIENT)
		private static void handlePacket(AddClientMotionPacket packet)
		{
			LocalPlayer player = net.minecraft.client.Minecraft.getInstance().player;

			player.setDeltaMovement(player.getDeltaMovement().add(packet.x, packet.y, packet.z));
		}
	}
}
