package com.legacy.rediscovered.network.s_to_c;

import java.util.UUID;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public class SendBossIdPacket implements PacketHandler.ModPacket<SendBossIdPacket, SendBossIdPacket.Handler>
{
	private final UUID bossId;

	public SendBossIdPacket(UUID uuid)
	{
		this.bossId = uuid;
	}

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<SendBossIdPacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("send_boss_id");
		}

		@Override
		public void encoder(SendBossIdPacket packet, FriendlyByteBuf buff)
		{
			buff.writeUUID(packet.bossId);
		}

		@Override
		public SendBossIdPacket decoder(FriendlyByteBuf buff)
		{
			return new SendBossIdPacket(buff.readUUID());
		}

		@Override
		public void handler(SendBossIdPacket packet, PlayPayloadContext context)
		{
			if (FMLEnvironment.dist == Dist.CLIENT)
				context.workHandler().submitAsync(() -> handlePacket(packet));
		}

		@OnlyIn(Dist.CLIENT)
		private static void handlePacket(SendBossIdPacket packet)
		{
			try
			{
				com.legacy.rediscovered.client.RediscoveredClientEvents.ACTIVE_DRAGON_FIGHTS.add(packet.bossId);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
