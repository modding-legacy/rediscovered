package com.legacy.rediscovered.network;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.network.c_to_s.CycleQuiverPacket;
import com.legacy.rediscovered.network.c_to_s.DragonFlightStatusPacket;
import com.legacy.rediscovered.network.c_to_s.DragonStaminaDecreasePacket;
import com.legacy.rediscovered.network.s_to_c.AddClientMotionPacket;
import com.legacy.rediscovered.network.s_to_c.SendBossIdPacket;
import com.legacy.rediscovered.network.s_to_c.SendScreenShakePacket;
import com.legacy.rediscovered.network.s_to_c.SetLocalRainPacket;
import com.legacy.rediscovered.network.s_to_c.UpdateAnimationPacket;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.common.Mod.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.util.FakePlayer;
import net.neoforged.neoforge.network.event.RegisterPayloadHandlerEvent;
import net.neoforged.neoforge.network.handling.ConfigurationPayloadContext;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;
import net.neoforged.neoforge.network.registration.IPayloadRegistrar;
import net.neoforged.neoforge.server.ServerLifecycleHooks;

@Mod.EventBusSubscriber(bus = Bus.MOD)
public class PacketHandler
{
	@SubscribeEvent
	public static void registerPayloads(RegisterPayloadHandlerEvent event)
	{
		IPayloadRegistrar registrar = event.registrar(RediscoveredMod.MODID).versioned("1").optional();

		// Server -> Client
		playPacket(registrar, new UpdateAnimationPacket.Handler());
		playPacket(registrar, new SendBossIdPacket.Handler());
		playPacket(registrar, new AddClientMotionPacket.Handler());
		playPacket(registrar, new SendScreenShakePacket.Handler());

		// Client -> Server
		playPacket(registrar, new CycleQuiverPacket.Handler());
		playPacket(registrar, new DragonFlightStatusPacket.Handler());
		playPacket(registrar, new DragonStaminaDecreasePacket.Handler());
		playPacket(registrar, new SetLocalRainPacket.Handler());

		// Bi-Directional
	}

	private static <T extends ModPacket<T, ?>> void playPacket(IPayloadRegistrar registrar, PlayHandler<T> handler)
	{
		registrar.play(handler.id(), handler::decoder, handler::handlePacket);
	}

	@SuppressWarnings("unused")
	private static <T extends ModPacket<T, ?>> void configurationPacket(IPayloadRegistrar registrar, ConfigurationHandler<T> handler)
	{
		registrar.configuration(handler.id(), handler::decoder, handler::handlePacket);
	}

	/**
	 * Server -> Client player passed
	 */
	public static void sendToClient(CustomPacketPayload packet, ServerPlayer serverPlayer)
	{
		if (!(serverPlayer instanceof FakePlayer))
			serverPlayer.connection.send(packet);
	}

	/**
	 * Server -> Clients in same world
	 */
	public static void sendToClients(CustomPacketPayload packet, ServerLevel level)
	{
		level.players().forEach(player -> sendToClient(packet, player));
	}

	/**
	 * Server -> Clients in same world within 256 blocks
	 */
	public static void sendToClients(CustomPacketPayload packet, ServerLevel level, BlockPos pos)
	{
		sendToClients(packet, level, pos, 256);
	}

	/**
	 * Server -> Clients in same world within range
	 */
	public static void sendToClients(CustomPacketPayload packet, ServerLevel level, BlockPos pos, int range)
	{
		Vec3 centerPos = Vec3.atCenterOf(pos);
		level.players().forEach(player ->
		{
			if (centerPos.distanceTo(player.position()) <= range)
				sendToClient(packet, player);
		});
	}

	/**
	 * Server -> Clients in every level
	 */
	public static void sendToClients(CustomPacketPayload packet)
	{
		MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
		if (server != null)
			server.getPlayerList().getPlayers().forEach(player -> sendToClient(packet, player));
	}

	/**
	 * Client to server
	 *
	 * @param packet
	 */
	public static void sendToServer(CustomPacketPayload packet)
	{
		net.minecraft.client.Minecraft.getInstance().getConnection().send(packet);
	}

	/**
	 * 
	 * @param <T> The packet's class
	 * @param <H> The packet's handler class
	 */
	public static interface ModPacket<T extends ModPacket<T, ?>, H extends Handler<T, ?>> extends CustomPacketPayload
	{
		H handler();

		@Override
		default ResourceLocation id()
		{
			return this.handler().id();
		}

		@Override
		default void write(FriendlyByteBuf buff)
		{
			this.handler().encoder(this.getSelf(), buff);
		}

		@SuppressWarnings("unchecked")
		default T getSelf()
		{
			return (T) this;
		}
	}

	/**
	 * 
	 * @param <T> The packet's class
	 */
	public static interface PlayHandler<T extends ModPacket<T, ?>> extends Handler<T, PlayPayloadContext>
	{
		private void handlePacket(T packet, PlayPayloadContext context)
		{
			if (context.flow().isServerbound())
				this.handleServerbound(packet, context);
			else
				this.handleClientbound(packet, context);
		}
	}

	/**
	 * 
	 * @param <T> The packet's class
	 */
	public static interface ConfigurationHandler<T extends ModPacket<T, ?>> extends Handler<T, ConfigurationPayloadContext>
	{
		private void handlePacket(T packet, ConfigurationPayloadContext context)
		{
			if (context.flow().isServerbound())
				this.handleServerbound(packet, context);
			else
				this.handleClientbound(packet, context);
		}
	}

	protected static interface Handler<T extends ModPacket<T, ?>, C extends IPayloadContext> extends Serializer<T>
	{
		/**
		 * Handles the packet. Alternatively, implement handleClient or handleServer to
		 * customize behavior for bi-directional packets.
		 */
		void handler(T packet, C context);

		@OnlyIn(Dist.CLIENT)
		default void handleClientbound(T packet, C context)
		{
			this.handler(packet, context);
		}

		default void handleServerbound(T packet, C context)
		{
			this.handler(packet, context);
		}
	}

	protected static interface Serializer<T extends ModPacket<T, ?>>
	{
		ResourceLocation id();

		void encoder(T packet, FriendlyByteBuf buff);

		T decoder(FriendlyByteBuf buff);

		@Nullable
		default MinecraftServer server(IPayloadContext context)
		{
			return context.level().map(Level::getServer).orElseGet(ServerLifecycleHooks::getCurrentServer);
		}

		@Nullable
		default ServerPlayer serverPlayer(IPayloadContext context)
		{
			var player = context.player();
			if (player.isPresent() && player.get()instanceof ServerPlayer s)
				return s;
			return null;
		}

		@Nullable
		default ServerLevel serverLevel(IPayloadContext context)
		{
			var level = context.level();
			if (level.isPresent() && level.get()instanceof ServerLevel s)
				return s;
			return null;
		}
	}
}
