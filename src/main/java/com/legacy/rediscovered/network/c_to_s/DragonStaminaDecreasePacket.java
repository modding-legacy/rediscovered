package com.legacy.rediscovered.network.c_to_s;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public record DragonStaminaDecreasePacket(int dragonId, float staminaDecrease) implements PacketHandler.ModPacket<DragonStaminaDecreasePacket, DragonStaminaDecreasePacket.Handler>
{

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<DragonStaminaDecreasePacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("dragon_stamina_decrease");
		}

		@Override
		public void encoder(DragonStaminaDecreasePacket packet, FriendlyByteBuf buff)
		{
			buff.writeInt(packet.dragonId);
			buff.writeFloat(packet.staminaDecrease);
		}

		@Override
		public DragonStaminaDecreasePacket decoder(FriendlyByteBuf buff)
		{
			return new DragonStaminaDecreasePacket(buff.readInt(), buff.readFloat());
		}

		@Override
		public void handler(DragonStaminaDecreasePacket packet, PlayPayloadContext context)
		{
			context.workHandler().submitAsync(() ->
			{
				ServerPlayer player = this.serverPlayer(context);
				Entity entity = player.level().getEntity(packet.dragonId());

				if (entity instanceof RedDragonOffspringEntity dragon && dragon.isVehicle() && player.getVehicle() == dragon && player == dragon.getOwner() && dragon.getControllingPassenger() == player)
					dragon.setStamina(Mth.clamp(dragon.getStamina() - packet.staminaDecrease(), 0, 100));
			});
		}
	}
}