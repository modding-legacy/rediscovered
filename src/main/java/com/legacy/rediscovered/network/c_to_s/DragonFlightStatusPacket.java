package com.legacy.rediscovered.network.c_to_s;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public record DragonFlightStatusPacket(int dragonId, boolean flying) implements PacketHandler.ModPacket<DragonFlightStatusPacket, DragonFlightStatusPacket.Handler>
{
	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<DragonFlightStatusPacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("dragon_flight_status");
		}

		@Override
		public void encoder(DragonFlightStatusPacket packet, FriendlyByteBuf buff)
		{
			buff.writeInt(packet.dragonId);
			buff.writeBoolean(packet.flying);
		}

		@Override
		public DragonFlightStatusPacket decoder(FriendlyByteBuf buff)
		{
			return new DragonFlightStatusPacket(buff.readInt(), buff.readBoolean());
		}

		@Override
		public void handler(DragonFlightStatusPacket packet, PlayPayloadContext context)
		{
			context.workHandler().submitAsync(() ->
			{
				ServerPlayer player = this.serverPlayer(context);
				Entity entity = player.level().getEntity(packet.dragonId());

				if (entity instanceof RedDragonOffspringEntity dragon && dragon.isVehicle() && player.getVehicle() == dragon)
					dragon.setFlying(packet.flying());
			});
		}
	}
}