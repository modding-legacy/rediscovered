package com.legacy.rediscovered.network.c_to_s;

import java.util.Optional;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.network.PacketHandler;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlot;
import net.neoforged.neoforge.network.handling.PlayPayloadContext;

public record CycleQuiverPacket() implements PacketHandler.ModPacket<CycleQuiverPacket, CycleQuiverPacket.Handler>
{
	public CycleQuiverPacket()
	{
	}

	@Override
	public Handler handler()
	{
		return new Handler();
	}

	public static class Handler implements PacketHandler.PlayHandler<CycleQuiverPacket>
	{
		@Override
		public ResourceLocation id()
		{
			return RediscoveredMod.locate("cycle_quiver");
		}

		public void encoder(CycleQuiverPacket packet, FriendlyByteBuf buff)
		{

		}

		@Override
		public CycleQuiverPacket decoder(FriendlyByteBuf buff)
		{
			return new CycleQuiverPacket();
		}

		@Override
		public void handler(CycleQuiverPacket packet, PlayPayloadContext context)
		{
			context.workHandler().submitAsync(() ->
			{
				Optional<QuiverData> quiverData = QuiverData.getFromChestplate(this.serverPlayer(context).getItemBySlot(EquipmentSlot.CHEST));
				if (quiverData.isPresent())
				{
					quiverData.get().cycleNextSlot(QuiverData.getHeldBow(this.serverPlayer(context)).getSecond());
				}
			});
		}
	}
}