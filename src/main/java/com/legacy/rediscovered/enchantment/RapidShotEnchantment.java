package com.legacy.rediscovered.enchantment;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;

public class RapidShotEnchantment extends Enchantment
{
	public RapidShotEnchantment(Enchantment.Rarity pRarity, EquipmentSlot... pApplicableSlots)
	{
		super(pRarity, EnchantmentCategory.BOW, pApplicableSlots);
	}

	@Override
	public int getMinCost(int pEnchantmentLevel)
	{
		return 12 + (pEnchantmentLevel - 1) * 20;
	}

	@Override
	public int getMaxCost(int pEnchantmentLevel)
	{
		return this.getMinCost(pEnchantmentLevel) + 25;
	}

	@Override
	public boolean isTreasureOnly()
	{
		return true;
	}

	@Override
	public boolean isCurse()
	{
		return false;
	}

	@Override
	public boolean isTradeable()
	{
		return false;
	}

	@Override
	public boolean isDiscoverable()
	{
		return false;
	}
}