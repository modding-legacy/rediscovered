package com.legacy.rediscovered.block;

import javax.annotation.Nullable;

import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.structure.base.IModifyState;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class ShallowDirtSlabBlock extends SlabBlock
{
	public static final MapCodec<ShallowDirtSlabBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(Block.CODEC.fieldOf("parent").forGetter(o -> o.parent), propertiesCodec()).apply(instance, ShallowDirtSlabBlock::new));
	protected static final VoxelShape BOTTOM_AABB_SHALLOW = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 7.0D, 16.0D);
	protected static final VoxelShape TOP_AABB_SHALLOW = Block.box(0.0D, 8.0D, 0.0D, 16.0D, 15.0D, 16.0D);

	private final Block parent;

	public ShallowDirtSlabBlock(Block parent, Properties properties)
	{
		super(properties);
		this.parent = parent;
	}
	
	@Override
	public MapCodec<? extends SlabBlock> codec()
	{
		return CODEC;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		switch (state.getValue(TYPE))
		{
		case DOUBLE:
			return Blocks.DIRT_PATH.defaultBlockState().getShape(level, pos, context);
		case TOP:
			return TOP_AABB_SHALLOW;
		default:
			return BOTTOM_AABB_SHALLOW;
		}
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		if (!this.defaultBlockState().canSurvive(context.getLevel(), context.getClickedPos()))
			state = Block.pushEntitiesUp(this.defaultBlockState(), IModifyState.mergeStates(RediscoveredBlocks.dirt_slab.defaultBlockState(), state), context.getLevel(), context.getClickedPos());
		return state;
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor level, BlockPos pos, BlockPos facingPos)
	{
		if (facing == Direction.UP && !state.canSurvive(level, pos))
		{
			level.scheduleTick(pos, this, 1);
		}

		return super.updateShape(state, facing, facingState, level, pos, facingPos);
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
	{
		if (state.getValue(SlabBlock.TYPE) != SlabType.BOTTOM)
			return this.parent.defaultBlockState().canSurvive(level, pos);
		return true;
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		turnToDirt(null, state, level, pos);
	}

	public static void turnToDirt(@Nullable Entity entity, BlockState state, Level level, BlockPos pos)
	{
		BlockState blockstate = pushEntitiesUp(state, IModifyState.mergeStates(RediscoveredBlocks.dirt_slab.defaultBlockState(), state), level, pos);
		level.setBlockAndUpdate(pos, blockstate);
		level.gameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Context.of(entity, blockstate));
	}
}
