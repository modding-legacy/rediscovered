package com.legacy.rediscovered.block;

import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.BlockHitResult;

public class BrittleBlock extends Block
{
	public static final MapCodec<BrittleBlock> CODEC = simpleCodec(BrittleBlock::new);
	public static final int MIN_STRENGTH = 0, MAX_STRENGTH = 2;
	public static final IntegerProperty STRENGTH = IntegerProperty.create("strength", 0, 2);

	public BrittleBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.stateDefinition.any().setValue(STRENGTH, 2));
	}

	@Override
	protected MapCodec<? extends Block> codec()
	{
		return CODEC;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(STRENGTH);
	}

	@Override
	public void stepOn(Level level, BlockPos pos, BlockState state, Entity entity)
	{
		if (!level.isClientSide())
		{
			if (entity.isSprinting())
				level.destroyBlock(pos, false);
			if (!entity.isSteppingCarefully() && entity instanceof LivingEntity)
				level.scheduleTick(pos, this, 2);
		}
		super.stepOn(level, pos, state, entity);
	}

	@Override
	public void fallOn(Level level, BlockState state, BlockPos pos, Entity entity, float fallDistance)
	{
		super.fallOn(level, state, pos, entity, fallDistance);
		if (!level.isClientSide() && fallDistance >= 0.75)
		{
			level.destroyBlock(pos, false);
			for (Direction dir : Direction.Plane.HORIZONTAL)
			{
				BlockPos offset = pos.relative(dir);
				BlockState neighbor = level.getBlockState(offset);
				if (neighbor.getBlock() instanceof BrittleBlock brittle)
				{
					brittle.weaken(level, neighbor, offset, 1);
				}
			}
		}
	}

	@Override
	public void onProjectileHit(Level level, BlockState state, BlockHitResult hit, Projectile projectile)
	{
		BlockPos hitPos = hit.getBlockPos();
		if (!level.isClientSide() && projectile.mayInteract(level, hitPos) && projectile.mayBreak(level));
		{
			this.weaken(level, state, hitPos, 1);
		}
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		this.weaken(level, state, pos, 1);
	}

	private void weaken(Level level, BlockState state, BlockPos pos, int amount)
	{
		int strength = state.getValue(STRENGTH);
		if (strength > 0)
		{
			level.setBlock(pos, state.setValue(STRENGTH, Mth.clamp(strength - amount, MIN_STRENGTH, MAX_STRENGTH)), UPDATE_ALL);
		}
		else
		{
			level.destroyBlock(pos, false);
		}
	}

	@Override
	public PushReaction getPistonPushReaction(BlockState state)
	{
		return PushReaction.DESTROY;
	}
}
