package com.legacy.rediscovered.block;

import java.util.function.Supplier;

import com.legacy.structure_gel.api.structure.base.IModifyState;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.BonemealableBlock;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.common.util.Lazy;

public class ShearedFlowerBlock extends DoublePlantBlock implements BonemealableBlock
{
	public static final MapCodec<ShearedFlowerBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(propertiesCodec(), Block.CODEC.fieldOf("parent").forGetter(o -> o.parent.get())).apply(instance, (prop, parent) -> new ShearedFlowerBlock(prop, () -> parent)));
	private final Lazy<Block> parent;

	public ShearedFlowerBlock(Properties props, Supplier<Block> parent)
	{
		super(props);
		this.parent = Lazy.of(parent);
	}

	@Override
	public MapCodec<? extends DoublePlantBlock> codec()
	{
		return CODEC;
	}

	@Override
	public boolean isValidBonemealTarget(LevelReader level, BlockPos pos, BlockState state)
	{
		return true;
	}

	@Override
	public boolean isBonemealSuccess(Level level, RandomSource rand, BlockPos pos, BlockState state)
	{
		return true;
	}

	@Override
	public void performBonemeal(ServerLevel level, RandomSource rand, BlockPos pos, BlockState origState)
	{
		int flags = Block.UPDATE_CLIENTS + Block.UPDATE_KNOWN_SHAPE;
		BlockState full = this.parent.get().defaultBlockState();
		BlockState state = origState;

		level.setBlock(pos, IModifyState.mergeStates(full, state), flags);

		if ((state = level.getBlockState(pos.above())).is(this))
			level.setBlock(pos.above(), IModifyState.mergeStates(full, state), flags);

		if ((state = level.getBlockState(pos.below())).is(this))
			level.setBlock(pos.below(), IModifyState.mergeStates(full, state), flags);
	}
}
