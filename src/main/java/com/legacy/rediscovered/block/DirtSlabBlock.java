package com.legacy.rediscovered.block;

import java.util.HashMap;
import java.util.Map;

import com.legacy.rediscovered.block.util.IShovelFlattenable;
import com.legacy.rediscovered.block.util.ITillable;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.structure.base.IModifyState;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.neoforged.jarjar.nio.util.Lazy;

public class DirtSlabBlock extends SlabBlock implements IShovelFlattenable, ITillable
{
	public static final MapCodec<DirtSlabBlock> CODEC = simpleCodec(DirtSlabBlock::new);
	private static final Lazy<Map<Block, Block>> SPREAD_TABLE = Lazy.of(() ->
	{
		Map<Block, Block> map = new HashMap<>();
		map.put(Blocks.GRASS_BLOCK, RediscoveredBlocks.grass_slab);
		map.put(Blocks.MYCELIUM, RediscoveredBlocks.mycelium_slab);
		return map;
	});

	public DirtSlabBlock(Properties properties)
	{
		super(properties);
	}

	@Override
	public MapCodec<? extends SlabBlock> codec()
	{
		return CODEC;
	}

	// Modified from SpreadingSnowyDirtBlock's randomTick, specifically the part that handles spreading
	// This is to avoid modifying the spreading snowy dirt. This only handles spreading from vanilla blocks to modded ones.
	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		if (!canSpreadOn(state)) // Handles preventing rooted/coarse dirt from being spread to
			return;
		if (!level.isAreaLoaded(pos, 3)) // Forge: prevent loading unloaded chunks when checking neighbor's light and spreading
			return;
		if ((state.getValue(SlabBlock.TYPE) == SlabType.BOTTOM ? level.getMaxLocalRawBrightness(pos) : level.getMaxLocalRawBrightness(pos.above())) >= 9)
		{
			BlockState blockstate = this.defaultBlockState();

			for (int i = 0; i < 4; ++i)
			{
				// Vanilla searches for dirt since the grass handles it. Since the dirt slab handles the spreading, it needs to check for grass.
				BlockPos blockpos = pos.offset(rand.nextInt(3) - 1, rand.nextInt(5) - 3, rand.nextInt(3) - 1);
				Block spreadResult = SPREAD_TABLE.get().get(level.getBlockState(blockpos).getBlock());
				if (spreadResult != null && GrassSlabBlock.canPropagate(state, level, pos))
				{
					// Place at my own pose, not blockpos. Get the state from GrassSlabBlock to handle snow layers and slab type
					level.setBlockAndUpdate(pos, GrassSlabBlock.getStateAtPosition(IModifyState.mergeStates(spreadResult.defaultBlockState(), state), level, pos));
				}
			}
		}
	}

	private static boolean canSpreadOn(BlockState state)
	{
		return state.is(RediscoveredBlocks.dirt_slab);
	}

	@Override
	public BlockState getShovelFlattenedState(BlockState state, LevelReader level, BlockPos pos)
	{
		return IModifyState.mergeStates(RediscoveredBlocks.dirt_path_slab.defaultBlockState(), state);
	}

	@Override
	public BlockState getHoeTilledState(BlockState state, LevelReader level, BlockPos pos)
	{
		if (state.is(RediscoveredBlocks.dirt_slab))
			return state;
		return IModifyState.mergeStates(RediscoveredBlocks.dirt_slab.defaultBlockState(), state);
	}
}
