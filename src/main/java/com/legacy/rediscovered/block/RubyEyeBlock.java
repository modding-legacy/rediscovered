package com.legacy.rediscovered.block;

import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class RubyEyeBlock extends Block
{
	public static final MapCodec<RubyEyeBlock> CODEC = simpleCodec(RubyEyeBlock::new);

	public RubyEyeBlock(Properties properties)
	{
		super(properties);
	}

	@Override
	public MapCodec<RubyEyeBlock> codec()
	{
		return CODEC;
	}

	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.INVISIBLE;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		return Shapes.empty();
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
	{
		return SkylandsPortalBlock.inPortalFrame(state, level, pos);
	}

	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean movedByPiston)
	{
		// Light portal or remove self
		Vec3i portalOffset = SkylandsPortalBlock.getPortalOffset(state, level, pos);
		if (portalOffset != null)
		{
			BlockPos centerPos = pos.offset(portalOffset);
			int r = SkylandsPortalBlock.INNER_RADIUS;
			for (int x = -r; x <= r; x++)
				for (int z = -r; z <= r; z++)
					level.setBlock(centerPos.offset(x, 0, z), RediscoveredBlocks.skylands_portal.defaultBlockState(), Block.UPDATE_KNOWN_SHAPE + Block.UPDATE_CLIENTS);
		}
		else
		{
			level.setBlock(pos, Blocks.AIR.defaultBlockState(), Block.UPDATE_ALL);
		}
	}
}
