package com.legacy.rediscovered.block;

import com.mojang.serialization.MapCodec;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;

public class BasicPoiBlock extends Block
{
	public static final MapCodec<BasicPoiBlock> CODEC = simpleCodec(BasicPoiBlock::new);
	public static final BooleanProperty IS_POI = BooleanProperty.create("is_poi");
	
	public BasicPoiBlock(Properties pProperties)
	{
		super(pProperties);
		this.registerDefaultState(this.defaultBlockState().setValue(IS_POI, false));
	}
	
	@Override
	protected MapCodec<? extends Block> codec()
	{
		return CODEC;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> pBuilder)
	{
		pBuilder.add(IS_POI);
	}
}
