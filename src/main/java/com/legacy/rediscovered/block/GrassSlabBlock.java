package com.legacy.rediscovered.block;

import com.legacy.rediscovered.block.util.IShovelFlattenable;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.structure.base.IModifyState;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.SnowLayerBlock;
import net.minecraft.world.level.block.SpreadingSnowyDirtBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.shapes.Shapes;
import net.neoforged.neoforge.common.IPlantable;

public class GrassSlabBlock extends SlabBlock implements IShovelFlattenable
{
	public static final MapCodec<GrassSlabBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(Block.CODEC.fieldOf("parent").forGetter(o -> o.parent), propertiesCodec()).apply(instance, GrassSlabBlock::new));
	private final Block parent;
	private final boolean spreads;

	public GrassSlabBlock(Block parent, Properties properties)
	{
		super(properties);
		this.parent = parent;
		this.spreads = parent instanceof SpreadingSnowyDirtBlock;
		this.registerDefaultState(this.defaultBlockState().setValue(BlockStateProperties.SNOWY, false));
	}

	@Override
	public MapCodec<? extends SlabBlock> codec()
	{
		return CODEC;
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(BlockStateProperties.SNOWY);
	}

	@Override
	public boolean canSustainPlant(BlockState state, BlockGetter level, BlockPos pos, Direction facing, IPlantable plantable)
	{
		if (state.getValue(SlabBlock.TYPE) == SlabType.BOTTOM)
			return false;
		return this.parent.defaultBlockState().canSustainPlant(level, pos, facing, plantable);
	}

	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource rand)
	{
		this.parent.animateTick(state, level, pos, rand);
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor level, BlockPos pos, BlockPos facingPos)
	{
		// Rediscovered: Add slab type check and rework logic to handle the super.updateShape every time (waterlogging stuff happens there)

		if (facing == Direction.UP)
		{
			if (state.getValue(SlabBlock.TYPE) != SlabType.BOTTOM)
				state = state.setValue(BlockStateProperties.SNOWY, isSnowySetting(facingState));
			else if (facingState.getBlock() instanceof IPlantable)
				level.destroyBlock(facingPos, true);
		}
		return super.updateShape(state, facing, facingState, level, pos, facingPos);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		BlockState aboveState = context.getLevel().getBlockState(context.getClickedPos().above());
		return state.setValue(BlockStateProperties.SNOWY, state.getValue(SlabBlock.TYPE) != SlabType.BOTTOM && isSnowySetting(aboveState));
	}

	private static boolean isSnowySetting(BlockState pState)
	{
		return pState.is(BlockTags.SNOW);
	}

	public static BlockState getStateAtPosition(BlockState state, ServerLevel level, BlockPos pos)
	{
		if (state.hasProperty(BlockStateProperties.SNOWY))
			state = state.setValue(BlockStateProperties.SNOWY, isSnowySetting(level.getBlockState(pos.above())));
		return state;
	}

	@Override
	public BlockState getShovelFlattenedState(BlockState state, LevelReader level, BlockPos pos)
	{
		return IModifyState.mergeStates(RediscoveredBlocks.dirt_path_slab.defaultBlockState(), state);
	}

	// -------- Copied from SpreadingSnowyDirtBlock (grass) with notable modifications ---------
	// This needs to be public. DirtSlabBlock references it
	public static boolean canBeGrass(BlockState state, LevelReader level, BlockPos pos)
	{
		BlockPos abovePos = pos.above();
		BlockState aboveState = level.getBlockState(abovePos);
		// Rediscovered: Modify vanilla logic to care about slab half. Bottom slabs should care about waterlogging, but not about the block above them
		SlabType slabType = state.hasProperty(SlabBlock.TYPE) ? state.getValue(SlabBlock.TYPE) : SlabType.DOUBLE;
		boolean isBottom = slabType == SlabType.BOTTOM;
		if (!isBottom && aboveState.is(Blocks.SNOW) && aboveState.getValue(SnowLayerBlock.LAYERS) == 1)
		{
			return true;
		}
		else if (!isBottom && aboveState.getFluidState().getAmount() == FluidState.AMOUNT_FULL)
		{
			return false;
		}
		else if (isBottom && state.getValue(SlabBlock.WATERLOGGED))
		{
			return false;
		}
		else
		{
			// Use shape occlusion logic instead
			return !Shapes.blockOccudes(state.getOcclusionShape(level, pos), aboveState.getOcclusionShape(level, abovePos), Direction.UP);

			/*int light = LightEngine.getLightBlockInto(level, state.getBlock().defaultBlockState().setValue(SlabBlock.TYPE, SlabType.DOUBLE), pos, aboveState, abovePos, Direction.UP, aboveState.getLightBlock(level, abovePos));
			return light < level.getMaxLightLevel();*/
		}
	}

	// This needs to be public. DirtSlabBlock references it
	public static boolean canPropagate(BlockState state, LevelReader level, BlockPos pos)
	{
		BlockPos abovePos = pos.above();
		return canBeGrass(state, level, pos) && !level.getFluidState(abovePos).is(FluidTags.WATER);
	}

	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		// Don't attempt to spread if the block in question shouldn't be able to, such as Podzol.
		if (!this.spreads)
			return;

		if (!canBeGrass(state, level, pos))
		{
			if (!level.isAreaLoaded(pos, 1))
				return; // Forge: prevent loading unloaded chunks when checking neighbor's light and spreading
			level.setBlockAndUpdate(pos, IModifyState.mergeStates(RediscoveredBlocks.dirt_slab.defaultBlockState(), state)); // Rediscovered: Use dirt slab instead
		}
		else
		{
			if (!level.isAreaLoaded(pos, 3))
				return; // Forge: prevent loading unloaded chunks when checking neighbor's light and spreading
			if (level.getMaxLocalRawBrightness(pos.above()) >= 9)
			{
				for (int i = 0; i < 4; ++i)
				{
					BlockPos blockpos = pos.offset(rand.nextInt(3) - 1, rand.nextInt(5) - 3, rand.nextInt(3) - 1);

					// Modify spread logic to handle spreading to dirt and dirt slabs
					BlockState target = level.getBlockState(blockpos);
					BlockState result = null;
					if (target.is(Blocks.DIRT))
						result = this.parent.defaultBlockState();
					else if (target.is(RediscoveredBlocks.dirt_slab))
						result = this.defaultBlockState();

					if (result != null && canPropagate(target, level, blockpos))
					{
						level.setBlockAndUpdate(blockpos, IModifyState.mergeStates(getStateAtPosition(result, level, blockpos), target));
					}
				}
			}

		}
	}
}
