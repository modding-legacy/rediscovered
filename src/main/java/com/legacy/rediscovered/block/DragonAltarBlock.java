package com.legacy.rediscovered.block;

import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.EnumProperty;

public class DragonAltarBlock extends Block
{
	public static final MapCodec<DragonAltarBlock> CODEC = simpleCodec(DragonAltarBlock::new);
	public static final EnumProperty<Direction> FACING = BlockStateProperties.HORIZONTAL_FACING;
	public static final EnumProperty<Shape> SHAPE = EnumProperty.create("shape", Shape.class);

	public DragonAltarBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.stateDefinition.any().setValue(SHAPE, Shape.EDGE).setValue(FACING, Direction.NORTH));
	}

	@Override
	public MapCodec<? extends Block> codec()
	{
		return CODEC;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(FACING, SHAPE);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		LevelAccessor level = context.getLevel();
		BlockPos pos = context.getClickedPos();
		Direction dir = context.getHorizontalDirection();
		BlockState state = this.defaultBlockState();
		if (level.getBlockState(pos.relative(dir)).is(this))
			state = state.setValue(SHAPE, Shape.CORNER);
		if (level.getBlockState(pos.relative(Direction.NORTH)).is(this) && level.getBlockState(pos.relative(Direction.SOUTH)).is(this) && level.getBlockState(pos.relative(Direction.EAST)).is(this) && level.getBlockState(pos.relative(Direction.WEST)).is(this))
			state = state.setValue(SHAPE, Shape.CENTER);
		Direction facing = dir.getOpposite();
		if (level.getBlockState(pos.relative(dir.getClockWise())).is(this) && state.getValue(SHAPE) == Shape.CORNER)
			facing = facing.getClockWise();

		state = state.setValue(FACING, facing);

		return state;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		return state.setValue(FACING, rotation.rotate(state.getValue(FACING)));
	}

	@SuppressWarnings("deprecation")
	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		if (mirror == Mirror.NONE)
		{
			return state;
		}
		else if (state.getValue(SHAPE) != Shape.CORNER)
		{
			return state.rotate(mirror.getRotation(state.getValue(FACING)));
		}
		else
		{
			Direction facing = state.getValue(FACING);
			Direction.Axis axis = mirror == Mirror.LEFT_RIGHT ? Direction.Axis.Z : Direction.Axis.X;
			Direction newFace = facing.getAxis() == axis ? facing.getCounterClockWise() : facing.getClockWise();
			return state.setValue(FACING, newFace);
		}
	}

	public static enum Shape implements StringRepresentable
	{
		EDGE("edge"),
		CORNER("corner"),
		CENTER("center");

		final String name;

		Shape(String name)
		{
			this.name = name;
		}

		@Override
		public String getSerializedName()
		{
			return this.name;
		}

		public int getLightLevel()
		{
			return this == CORNER ? 0 : this == CENTER ? 15 : 7;
		}
	}
}
