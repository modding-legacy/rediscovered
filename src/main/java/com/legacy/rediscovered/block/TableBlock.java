package com.legacy.rediscovered.block;

import java.util.Map;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.rediscovered.block_entities.TableBlockEntity;
import com.legacy.rediscovered.block_entities.TableBlockEntity.TableShape;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.util.VoxelShapeUtil;
import com.mojang.serialization.MapCodec;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Containers;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class TableBlock extends BaseEntityBlock implements SimpleWaterloggedBlock
{
	public static final MapCodec<TableBlock> CODEC = simpleCodec(TableBlock::new);
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

	public static final BooleanProperty NORTH = BlockStateProperties.NORTH;
	public static final BooleanProperty SOUTH = BlockStateProperties.SOUTH;
	public static final BooleanProperty EAST = BlockStateProperties.EAST;
	public static final BooleanProperty WEST = BlockStateProperties.WEST;
	public static final BooleanProperty NW = BooleanProperty.create("north_west");
	public static final BooleanProperty NE = BooleanProperty.create("north_east");
	public static final BooleanProperty SW = BooleanProperty.create("south_west");
	public static final BooleanProperty SE = BooleanProperty.create("south_east");
	public static final BooleanProperty SINGLE = BooleanProperty.create("single");

	private static final VoxelShape TABLE_TOP = Block.box(2, 9, 2, 14, 11, 14);
	private static final VoxelShape TABLE_LEG = Block.box(2, 0, 2, 4, 9, 4);
	private static final VoxelShape TABLE_EDGE = Block.box(2, 9, 0, 14, 11, 2);
	private static final VoxelShape TABLE_CORNER = Block.box(0, 9, 0, 2, 11, 2);
	private static final Function<BlockState, VoxelShape> SHAPE_FUNC = Util.memoize(state ->
	{
		boolean n = state.getValue(NORTH);
		boolean s = state.getValue(SOUTH);
		boolean e = state.getValue(EAST);
		boolean w = state.getValue(WEST);
		boolean ne = state.getValue(NE);
		boolean nw = state.getValue(NW);
		boolean se = state.getValue(SE);
		boolean sw = state.getValue(SW);

		VoxelShape shape = TABLE_TOP;
		if (!n && !w)
			shape = Shapes.or(shape, TABLE_LEG);
		if (nw)
			shape = Shapes.or(shape, TABLE_CORNER);
		if (n)
			shape = Shapes.or(shape, TABLE_EDGE);

		if (!e && !n)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_LEG, Direction.EAST));
		if (ne)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_CORNER, Direction.EAST));
		if (e)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_EDGE, Direction.EAST));

		if (!s && !e)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_LEG, Direction.SOUTH));
		if (se)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_CORNER, Direction.SOUTH));
		if (s)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_EDGE, Direction.SOUTH));

		if (!w && !s)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_LEG, Direction.WEST));
		if (sw)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_CORNER, Direction.WEST));
		if (w)
			shape = Shapes.or(shape, VoxelShapeUtil.rotate(TABLE_EDGE, Direction.WEST));

		return shape;
	});

	public TableBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(super.defaultBlockState().setValue(WATERLOGGED, false).setValue(NORTH, false).setValue(SOUTH, false).setValue(EAST, false).setValue(WEST, false).setValue(NW, false).setValue(NE, false).setValue(SW, false).setValue(SE, false).setValue(SINGLE, false));
	}
	
	@Override
	protected MapCodec<? extends BaseEntityBlock> codec()
	{
		return CODEC;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		return SHAPE_FUNC.apply(state);
	}

	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : Fluids.EMPTY.defaultFluidState();
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		if (level.getBlockEntity(pos) instanceof TableBlockEntity table)
		{
			if (table.setShape(TableShape.compute(level, pos, state)))
			{
				for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
				{
					BlockPos neighborPos = pos.relative(dir);
					BlockState neighbor = level.getBlockState(neighborPos);
					if (neighbor.is(state.getBlock()))
					{
						level.scheduleTick(neighborPos, neighbor.getBlock(), 0);
					}
				}
			}
		}
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction neighbor, BlockState neighborState, LevelAccessor level, BlockPos pos, BlockPos neighborPos)
	{
		if (state.getValue(WATERLOGGED))
			level.scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(level));
		level.scheduleTick(pos, state.getBlock(), 0);
		return computeState(level, pos, state);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Level level = context.getLevel();
		BlockPos pos = context.getClickedPos();
		BlockState state = this.defaultBlockState();
		BlockPos neighborPos = pos.relative(context.getClickedFace().getOpposite());
		BlockState neighborState = level.getBlockState(neighborPos);
		boolean placeAgainstTable = context.getClickedFace().getAxis().isHorizontal() && neighborState.is(this);
		if (placeAgainstTable)
		{
			level.setBlock(neighborPos, neighborState.setValue(SINGLE, false), Block.UPDATE_ALL);
		}
		return computeState(level, pos, state.setValue(SINGLE, context.isSecondaryUseActive() && !placeAgainstTable)).setValue(WATERLOGGED, level.getFluidState(pos).is(Fluids.WATER));
	}

	public BlockState computeState(LevelAccessor level, BlockPos pos, BlockState state)
	{
		if (state.getValue(SINGLE))
			return state;

		boolean n = canConnect(level.getBlockState(pos.relative(Direction.NORTH)));
		boolean s = canConnect(level.getBlockState(pos.relative(Direction.SOUTH)));
		boolean e = canConnect(level.getBlockState(pos.relative(Direction.EAST)));
		boolean w = canConnect(level.getBlockState(pos.relative(Direction.WEST)));

		//@formatter:off
		return state.setValue(NORTH, n)
				.setValue(SOUTH, s)
				.setValue(EAST, e)
				.setValue(WEST, w)
				.setValue(NW, n && w && canConnect(level.getBlockState(pos.relative(Direction.NORTH).relative(Direction.WEST))))
				.setValue(NE, n && e && canConnect(level.getBlockState(pos.relative(Direction.NORTH).relative(Direction.EAST))))
				.setValue(SW, s && w && canConnect(level.getBlockState(pos.relative(Direction.SOUTH).relative(Direction.WEST))))
				.setValue(SE, s && e && canConnect(level.getBlockState(pos.relative(Direction.SOUTH).relative(Direction.EAST))));
		//@formatter:on
	}

	private boolean canConnect(BlockState state)
	{
		return state.is(this) && !state.getValue(SINGLE);
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(WATERLOGGED, NORTH, SOUTH, EAST, WEST, NW, NE, SW, SE, SINGLE);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState)
	{
		return new TableBlockEntity(pPos, pState);
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type)
	{
		return level.isClientSide ? null : createTickerHelper(type, RediscoveredBlockEntityTypes.TABLE, TableBlockEntity::serverTick);
	}

	@Override
	public RenderShape getRenderShape(BlockState pState)
	{
		return RenderShape.MODEL;
	}

	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit)
	{
		if (level.getBlockEntity(pos) instanceof TableBlockEntity table)
		{
			ItemStack stack = player.getItemInHand(hand);
			if (stack.isEmpty() && table.getItem().isEmpty())
				return InteractionResult.PASS; // Try offhand next
			if (!level.isClientSide && table.interact(player, hand, player.getAbilities().instabuild ? stack.copy() : stack, player.getDirection()))
			{
				return InteractionResult.SUCCESS;
			}

			return InteractionResult.CONSUME;
		}

		return InteractionResult.PASS;
	}

	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
	{
		return true;
	}

	@Override
	public int getAnalogOutputSignal(BlockState state, Level level, BlockPos pos)
	{
		return AbstractContainerMenu.getRedstoneSignalFromBlockEntity(level.getBlockEntity(pos));
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if (!state.is(newState.getBlock()))
		{
			if (level.getBlockEntity(pos) instanceof TableBlockEntity table)
			{
				Containers.dropContents(level, pos, table);
			}
			super.onRemove(state, level, pos, newState, isMoving);
		}
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		if (rotation == Rotation.NONE)
			return state;

		Map<Direction, BooleanProperty> cardinals = Map.of(Direction.NORTH, NORTH, Direction.EAST, EAST, Direction.SOUTH, SOUTH, Direction.WEST, WEST);
		Map<Direction, BooleanProperty> diagonals = Map.of(Direction.NORTH, NW, Direction.EAST, NE, Direction.SOUTH, SE, Direction.WEST, SW);
		BlockState newState = this.defaultBlockState();
		for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
		{
			BooleanProperty cardinal = cardinals.get(dir);
			BooleanProperty diagonal = diagonals.get(dir);
			Direction rotatedDir = rotation.rotate(dir);
			BooleanProperty rotatedCardinal = cardinals.get(rotatedDir);
			BooleanProperty rotatedDiagonal = diagonals.get(rotatedDir);

			newState = newState.setValue(rotatedCardinal, state.getValue(cardinal)).setValue(rotatedDiagonal, state.getValue(diagonal));
		}
		return newState;

	}

	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		if (mirror == Mirror.FRONT_BACK)
		{
			boolean e = state.getValue(EAST);
			boolean w = state.getValue(WEST);
			boolean ne = state.getValue(NE);
			boolean nw = state.getValue(NW);
			boolean se = state.getValue(SE);
			boolean sw = state.getValue(SW);
			return state.setValue(EAST, w).setValue(WEST, e).setValue(NW, ne).setValue(NE, nw).setValue(SW, se).setValue(SE, sw);
		}
		else if (mirror == Mirror.LEFT_RIGHT)
		{
			boolean n = state.getValue(NORTH);
			boolean s = state.getValue(SOUTH);
			boolean ne = state.getValue(NE);
			boolean nw = state.getValue(NW);
			boolean se = state.getValue(SE);
			boolean sw = state.getValue(SW);
			return state.setValue(NORTH, s).setValue(SOUTH, n).setValue(NW, sw).setValue(NE, se).setValue(SW, nw).setValue(SE, ne);
		}
		return state;
	}
	
	@Override
	public BlockPathTypes getBlockPathType(BlockState state, BlockGetter level, BlockPos pos, @Nullable Mob mob)
	{
		return RediscoveredBlocks.FURNITURE_PATH_TYPE;
	}
}
