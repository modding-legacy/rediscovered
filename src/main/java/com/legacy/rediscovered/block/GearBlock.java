package com.legacy.rediscovered.block;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.ImmutableList;
import com.legacy.rediscovered.block_entities.GearBlockEntity;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.structure_gel.api.util.VoxelShapeUtil;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class GearBlock extends BaseEntityBlock implements SimpleWaterloggedBlock
{
	public static final MapCodec<GearBlock> CODEC = simpleCodec(GearBlock::new);
	
	public GearBlock(Properties properties)
	{
		super(properties);

		BlockState defaultState = this.defaultBlockState();
		for (GearFace enumFace : GearFace.values())
			defaultState = defaultState.setValue(enumFace.stateProperty, GearState.NONE);
		this.registerDefaultState(defaultState.setValue(BlockStateProperties.WATERLOGGED, false));
	}
	
	@Override
	protected MapCodec<? extends BaseEntityBlock> codec()
	{
		return CODEC;
	}

	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.defaultFluidState() : Fluids.EMPTY.defaultFluidState();
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
	{
		for (GearFace enumFace : GearFace.values())
			if (state.getValue(enumFace.stateProperty).exists() && !isSolid(level, pos, enumFace.direction.getOpposite()))
				return false;

		return true;
	}

	// Checks if a block face is solid so the gear can stay
	public static boolean isSolid(LevelReader level, BlockPos pos, Direction direction)
	{
		BlockPos offsetPos = pos.relative(direction.getOpposite());
		return level.getBlockState(offsetPos).isFaceSturdy(level, offsetPos, direction);
	}

	@Override
	public void neighborChanged(BlockState state, Level level, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving)
	{
		level.scheduleTick(pos, this, 0);

		if (level instanceof ServerLevel serverLevel && level.getBlockEntity(pos) instanceof GearBlockEntity gear)
		{
			gear.onNeighborUpdated(serverLevel, state, pos);
		}
	}

	private void notifyWrapAroundNeighbors(BlockState state, Level level, BlockPos pos)
	{
		Set<BlockPos> positionsToNotify = new HashSet<>();
		positionsToNotify.add(pos);
		for (GearFace gearFace : GearFace.values())
		{
			if (state.getValue(gearFace.stateProperty).exists())
			{
				for (Direction dir : Direction.values())
				{
					if (gearFace.direction.getAxis() == dir.getAxis())
						continue;

					// Wrap around block
					positionsToNotify.add(pos.relative(dir).relative(gearFace.direction));
				}
			}
		}
		for (BlockPos notifyPos : positionsToNotify)
		{
			BlockState notifyState = level.getBlockState(notifyPos);
			if (level instanceof ServerLevel serverLevel && notifyState.is(this) && level.getBlockEntity(notifyPos) instanceof GearBlockEntity gear)
			{
				gear.onNeighborUpdated(serverLevel, notifyState, notifyPos);
			}
		}
	}

	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean movedByPiston)
	{
		this.notifyWrapAroundNeighbors(state, level, pos);
		super.onRemove(state, level, pos, newState, movedByPiston);

	}

	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean movedByPiston)
	{
		this.notifyWrapAroundNeighbors(state, level, pos);
		super.onPlace(state, level, pos, oldState, movedByPiston);
	}

	@Override
	public void tick(BlockState state, ServerLevel serverLevel, BlockPos pos, RandomSource random)
	{		
		// Check the faces of the gear and remove any that can't stay. Remove the gear
		// block if no gears exist in it.
		for (GearFace enumFace : GearFace.values())
		{
			if (state.getBlock() != this)
				return;
			if (state.getValue(enumFace.stateProperty).exists() && !isSolid(serverLevel, pos, enumFace.direction.getOpposite()))
			{
				dropResources(this.defaultBlockState().setValue(enumFace.stateProperty, GearState.GEAR), serverLevel, pos);
				serverLevel.setBlockAndUpdate(pos, removeGear(state, enumFace));
			}
		}

	}

	@Override
	public BlockState updateShape(BlockState state, Direction direction, BlockState neighborState, LevelAccessor level, BlockPos pos, BlockPos neighborPos)
	{
		if (state.getValue(BlockStateProperties.WATERLOGGED))
		{
			level.scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(level));
		}
		return state;
	}

	// Removes a gear face from the blockstate. Returns air if no faces remain.
	public BlockState removeGear(BlockState state, GearFace gearFace)
	{
		state = state.setValue(gearFace.stateProperty, GearState.NONE);

		// If it has any faces, keep it. If not, return air.
		for (GearFace enumFace : GearFace.values())
			if (state.getValue(enumFace.stateProperty).exists())
				return state;

		return Blocks.AIR.defaultBlockState();
	}

	// Let the player place multiple gears in the same block space
	@Override
	@Nullable
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Level level = context.getLevel();
		BlockPos clickedPos = context.getClickedPos();
		Direction faceToPlace = context.getClickedFace().getOpposite();

		BlockState oldState = level.getBlockState(clickedPos);
		if (oldState.is(this))
			return oldState.setValue(GearFace.get(faceToPlace).stateProperty, GearState.GEAR);
		else if (this.defaultBlockState().canSurvive(level, clickedPos))
			return this.defaultBlockState().setValue(GearFace.get(faceToPlace).stateProperty, GearState.GEAR).setValue(BlockStateProperties.WATERLOGGED, level.getFluidState(clickedPos).is(Fluids.WATER));
		else
			return null;
	}

	// Needed to let players place multiple gears in a space
	@Override
	public boolean canBeReplaced(BlockState state, BlockPlaceContext useContext)
	{
		if (useContext.getItemInHand().is(this.asItem()) && !state.getValue(GearFace.get(useContext.getClickedFace().getOpposite()).stateProperty).exists())
			if (useContext.replacingClickedOnBlock())
				return true;
			else
				return true;
		else
			return false;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		VoxelShape shape = Shapes.empty();

		for (GearFace enumFace : GearFace.values())
			if (state.getValue(enumFace.stateProperty).exists())
				shape = Shapes.or(shape, enumFace.shape);

		return shape;
	}

	// Called when broken. Used to tell spinning gears to stop spinning.
	@Override
	public BlockState playerWillDestroy(Level level, BlockPos pos, BlockState state, Player player)
	{
		return super.playerWillDestroy(level, pos, state, player);
	}

	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public boolean isPathfindable(BlockState state, BlockGetter level, BlockPos pos, PathComputationType type)
	{
		return true;
	}

	@Override
	public PushReaction getPistonPushReaction(BlockState state)
	{
		return PushReaction.DESTROY;
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter level, BlockPos pos)
	{
		return true;
	}

	@Override
	public boolean useShapeForLightOcclusion(BlockState state)
	{
		return true;
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> states)
	{
		for (GearFace gearFace : GearFace.values())
			states.add(gearFace.stateProperty);
		states.add(BlockStateProperties.WATERLOGGED);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new GearBlockEntity(pos, state);
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> blockEntityType)
	{
		return level.isClientSide ? createTickerHelper(blockEntityType, RediscoveredBlockEntityTypes.GEAR, GearBlockEntity::clientTick) : null;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		if (rotation != Rotation.NONE)
		{
			Map<Direction, GearState> gears = new HashMap<>();
			for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
				gears.put(dir, state.getValue(GearFace.get(dir).stateProperty));

			for (Direction dir : VoxelShapeUtil.HORIZONTAL_DIRS)
			{
				Direction rotatedDir = rotation.rotate(dir);
				state = state.setValue(GearFace.get(rotatedDir).stateProperty, gears.get(dir));
			}
		}
		return state;
	}

	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		if (mirror == Mirror.FRONT_BACK)
		{
			GearState e = state.getValue(GearFace.EAST.stateProperty);
			GearState w = state.getValue(GearFace.WEST.stateProperty);
			return state.setValue(GearFace.EAST.stateProperty, w).setValue(GearFace.WEST.stateProperty, e);	
		}
		else if (mirror == Mirror.LEFT_RIGHT)
		{
			GearState n = state.getValue(GearFace.NORTH.stateProperty);
			GearState s = state.getValue(GearFace.SOUTH.stateProperty);
			return state.setValue(GearFace.NORTH.stateProperty, s).setValue(GearFace.SOUTH.stateProperty, n);
		}
		return state;
	}

	public static enum GearState implements StringRepresentable
	{
		NONE("none"),
		GEAR("gear");

		final String name;

		GearState(String name)
		{
			this.name = name;
		}

		public boolean exists()
		{
			return this != NONE;
		}

		@Override
		public String getSerializedName()
		{
			return this.name;
		}
	}

	public static enum GearFace
	{
		DOWN(Direction.DOWN, Block.box(0.0D, 0.0D, 0.0D, 16.0D, 1.0D, 16.0D)),
		UP(Direction.UP, Block.box(0.0D, 15.0D, 0.0D, 16.0D, 16.0D, 16.0D)),
		NORTH(Direction.NORTH, Block.box(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 1.0D)),
		SOUTH(Direction.SOUTH, Block.box(0.0D, 0.0D, 15.0D, 16.0D, 16.0D, 16.0D)),
		WEST(Direction.WEST, Block.box(0.0D, 0.0D, 0.0D, 1.0D, 16.0D, 16.0D)),
		EAST(Direction.EAST, Block.box(15.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D));

		public final Direction direction;
		public final EnumProperty<GearState> stateProperty;
		public final VoxelShape shape;
		public List<Pair<BlockPos, GearFace>> possibleConnections;

		GearFace(Direction direction, VoxelShape shape)
		{
			this.direction = direction;
			this.stateProperty = EnumProperty.create(direction.getSerializedName() + "_gear", GearState.class);
			this.shape = shape;
			if (this.ordinal() != direction.ordinal())
				throw new IllegalStateException("GearFace ordinal does not match Direction ordinal");
		}

		public List<Pair<BlockPos, GearFace>> getPossibleConnections(BlockPos gearPos)
		{
			if (this.possibleConnections == null)
			{
				ImmutableList.Builder<Pair<BlockPos, GearFace>> connections = ImmutableList.builder();
				for (Direction dir : Direction.values())
				{
					BlockPos origin = BlockPos.ZERO;

					// Ignore self and the gear across from me
					if (dir.getAxis() == this.direction.getAxis())
						continue;

					// Within same block
					connections.add(Pair.of(origin, get(dir)));

					// Wrap around block
					connections.add(Pair.of(origin.relative(dir).relative(this.direction), get(dir.getOpposite())));

					// Adjacent gears
					connections.add(Pair.of(origin.relative(dir), this));
				}
				this.possibleConnections = connections.build();
			}
			return this.possibleConnections.stream().map(p -> Pair.of(p.getLeft().offset(gearPos), p.getRight())).toList();
		}

		public static GearFace get(Direction direction)
		{
			return GearFace.values()[direction.ordinal()];
		}
	}
}
