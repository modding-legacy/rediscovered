package com.legacy.rediscovered.block;

import com.legacy.rediscovered.block_entities.NetherReactorBlockEntity;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;

public class NetherReactorBlock extends BaseEntityBlock
{
	public static final MapCodec<NetherReactorBlock> CODEC = simpleCodec(NetherReactorBlock::new);
	public static final BooleanProperty ACTIVE = BooleanProperty.create("active");

	public NetherReactorBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.defaultBlockState().setValue(ACTIVE, false));
	}

	@Override
	protected MapCodec<? extends BaseEntityBlock> codec()
	{
		return CODEC;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(ACTIVE);
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type)
	{
		return level.isClientSide ? createTickerHelper(type, RediscoveredBlockEntityTypes.NETHER_REACTOR, NetherReactorBlockEntity::clientTick) : createTickerHelper(type, RediscoveredBlockEntityTypes.NETHER_REACTOR, NetherReactorBlockEntity::serverTick);
	}

	@Override
	public RenderShape getRenderShape(BlockState pState)
	{
		return RenderShape.MODEL;
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new NetherReactorBlockEntity(pos, state);
	}

	@Override
	public void setPlacedBy(Level level, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack)
	{
		super.setPlacedBy(level, pos, state, placer, stack);
		if (!level.isClientSide)
			level.getBlockEntity(pos, RediscoveredBlockEntityTypes.NETHER_REACTOR).ifPresent(NetherReactorBlockEntity::onPlace);
	}
}
