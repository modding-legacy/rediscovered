package com.legacy.rediscovered.block;

import java.util.List;

import com.mojang.serialization.MapCodec;

import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;

/*
 * Works similar to the copper bulb, toggling state when powered. Also emits light based on the signal that powered it, making it function as a memory cell.
 */
public class ObsidianBulbBlock extends Block
{
	public static final MapCodec<ObsidianBulbBlock> CODEC = simpleCodec(ObsidianBulbBlock::new);
	public static final IntegerProperty LIGHT_LEVEL = BlockStateProperties.LEVEL;
	public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

	public ObsidianBulbBlock(Properties props)
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(LIGHT_LEVEL, 0).setValue(POWERED, false));
	}

	@Override
	protected MapCodec<? extends Block> codec()
	{
		return CODEC;
	}

	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean movedByPiston)
	{
		if (state.getBlock() != oldState.getBlock() && level instanceof ServerLevel)
		{
			this.placeUpdated(state, level, pos);
		}
	}

	@Override
	public void neighborChanged(BlockState state, Level level, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving)
	{
		if (!level.isClientSide)
		{
			level.scheduleTick(pos, this, 0);
		}
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		this.placeUpdated(state, level, pos);
	}

	public void placeUpdated(BlockState state, Level level, BlockPos pos)
	{
		int lightLevel = state.getValue(LIGHT_LEVEL);
		boolean powered = state.getValue(POWERED);
		int signal = level.getBestNeighborSignal(pos);

		if (powered != signal > 0)
		{
			BlockState newState = state.setValue(POWERED, signal > 0);
			if (signal > 0)
				newState = newState.setValue(LIGHT_LEVEL, lightLevel == 0 ? signal : 0);
			if (newState != state)
				level.setBlock(pos, newState, Block.UPDATE_ALL);
		}
	}

	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
	{
		return true;
	}

	@Override
	public int getAnalogOutputSignal(BlockState state, Level level, BlockPos pos)
	{
		return state.getValue(LIGHT_LEVEL);
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(LIGHT_LEVEL, POWERED);
	}

	@Override
	public ItemStack getCloneItemStack(LevelReader level, BlockPos pos, BlockState state)
	{
		return setLightOnStack(new ItemStack(this), state.getValue(LIGHT_LEVEL));
	}

	@Override
	public void appendHoverText(ItemStack stack, BlockGetter level, List<Component> tooltip, TooltipFlag flag)
	{
		int light = getLightFromStack(stack);
		if (light > 0)
			tooltip.add(Component.literal("Light: " + light).withStyle(ChatFormatting.GRAY));
	}

	private static final String BLOCK_STATE_TAG = "BlockStateTag";

	public static ItemStack setLightOnStack(ItemStack stack, int light)
	{
		if (light != 0)
		{
			CompoundTag blockStateTag = new CompoundTag();
			blockStateTag.putString(LIGHT_LEVEL.getName(), String.valueOf(light));
			stack.addTagElement(BLOCK_STATE_TAG, blockStateTag);
		}
		return stack;
	}

	public static int getLightFromStack(ItemStack stack)
	{
		CompoundTag tag = stack.getTag();
		if (tag != null && tag.contains(BLOCK_STATE_TAG, Tag.TAG_COMPOUND))
		{
			CompoundTag bsTag = tag.getCompound(BLOCK_STATE_TAG);
			if (bsTag.contains(LIGHT_LEVEL.getName(), Tag.TAG_STRING))
			{
				try
				{
					return Integer.parseInt(bsTag.getString(LIGHT_LEVEL.getName()));
				}
				catch (NumberFormatException e)
				{
					return 0;
				}
			}
		}
		return 0;
	}
}
