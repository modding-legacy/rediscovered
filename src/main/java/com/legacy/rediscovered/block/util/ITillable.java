package com.legacy.rediscovered.block.util;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.state.BlockState;

public interface ITillable
{
	BlockState getHoeTilledState(BlockState state, LevelReader level, BlockPos pos);
}
