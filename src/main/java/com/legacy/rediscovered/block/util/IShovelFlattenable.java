package com.legacy.rediscovered.block.util;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.state.BlockState;

public interface IShovelFlattenable
{
	BlockState getShovelFlattenedState(BlockState state, LevelReader level, BlockPos pos);
}
