package com.legacy.rediscovered.block;

import java.util.Comparator;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.rediscovered.block_entities.MultiBlockPattern;
import com.legacy.rediscovered.block_entities.MultiBlockPattern.MultiBlockPatternBuilder;
import com.legacy.rediscovered.block_entities.MultiBlockPattern.PatternBuilder;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.legacy.rediscovered.registry.RediscoveredPoiTypes;
import com.legacy.structure_gel.api.block.GelPortalBlock;
import com.legacy.structure_gel.api.dimension.portal.GelTeleporter;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.TicketType;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiRecord;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.portal.PortalInfo;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class SkylandsPortalBlock extends GelPortalBlock
{
	public static final MapCodec<SkylandsPortalBlock> CODEC = simpleCodec(SkylandsPortalBlock::new);
	public static final int INNER_RADIUS = 1;
	public static final MultiBlockPattern PORTAL_PATTERN = createPortalPattern(false);
	public static final MultiBlockPattern FULL_PORTAL_PATTERN = createPortalPattern(true);

	protected static final VoxelShape SHAPE = Block.box(0.0D, 6.0D, 0.0D, 16.0D, 10.0D, 16.0D);

	public SkylandsPortalBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	public MapCodec<? extends GelPortalBlock> codec()
	{
		return CODEC;
	}

	@Override
	public GelTeleporter getTeleporter(ServerLevel serverLevel, GelPortalBlock portal)
	{
		return new Teleporter(serverLevel, portal);
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> pBuilder)
	{
		super.createBlockStateDefinition(pBuilder); // Included since that's easier. It does nothing.
	}

	@Override
	public BlockState rotate(BlockState pState, Rotation pRot)
	{
		return pState;
	}

	@Override
	public BlockState mirror(BlockState pState, Mirror pMirror)
	{
		return pState;
	}

	private static MultiBlockPattern createPortalPattern(boolean full)
	{
		// Allow upside-down portals as well
		MultiBlockPatternBuilder builder = MultiBlockPattern.builder();
		for (int i : new int[] { -1, 1 })
		{
			PatternBuilder pattern = builder.pattern();
			pattern.validator('f', RediscoveredTags.Blocks.SKYLANDS_PORTAL_FRAME);
			pattern.validator('b', RediscoveredTags.Blocks.SKYLANDS_PORTAL_BASE);
			if (full)
				pattern.validator('a', (self, state) -> state.is(RediscoveredBlocks.skylands_portal));
			else
				pattern.validator('a', (self, state) -> state.isAir() || state.is(RediscoveredBlocks.skylands_portal) || state.is(RediscoveredBlocks.ruby_eye));

			int r = INNER_RADIUS;
			for (int x = -r; x <= r; x++)
				for (int z = -r; z <= r; z++)
					pattern.state(x, 0, z, 'a');

			for (int x = -r; x <= r; x++)
				for (int z = -r; z <= r; z++)
					pattern.state(x, i, z, 'b');

			int f = r + 1;
			for (int x = -f; x <= f; x++)
			{
				for (int z = -f; z <= f; z++)
				{
					int ax = Math.abs(x);
					int az = Math.abs(z);
					if ((ax == f || az == f) && ax != az)
						pattern.state(x, 0, z, 'f');
				}
			}
			pattern.end();
		}
		return builder.build();
	}

	protected static boolean inPortalFrame(BlockState state, LevelReader level, BlockPos pos)
	{
		return inPortalFrame(state, level, pos, SkylandsPortalBlock.PORTAL_PATTERN);
	}

	@Nullable
	protected static Vec3i getPortalOffset(BlockState state, LevelReader level, BlockPos pos)
	{
		return getPortalOffset(state, level, pos, SkylandsPortalBlock.PORTAL_PATTERN);
	}

	protected static boolean inPortalFrame(BlockState state, LevelReader level, BlockPos pos, MultiBlockPattern pattern)
	{
		return getPortalOffset(state, level, pos, pattern) != null;
	}

	@Nullable
	protected static Vec3i getPortalOffset(BlockState state, LevelReader level, BlockPos pos, MultiBlockPattern pattern)
	{
		int r = SkylandsPortalBlock.INNER_RADIUS;
		for (int x = -r; x <= r; x++)
			for (int z = -r; z <= r; z++)
				if (pattern.isValid(level, pos.offset(x, 0, z), state))
					return new Vec3i(x, 0, z);
		return null;
	}

	@Override
	public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext)
	{
		return SHAPE;
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor level, BlockPos currentPos, BlockPos facingPos)
	{
		if (inPortalFrame(state, level, currentPos, FULL_PORTAL_PATTERN))
			return state;
		return Blocks.AIR.defaultBlockState();
	}

	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource rand)
	{
		if (rand.nextInt(100) == 0)
		{
			level.playLocalSound(pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D, SoundEvents.PORTAL_AMBIENT, SoundSource.BLOCKS, 0.5F, rand.nextFloat() * 0.4F + 0.8F, false);
		}

		int verticalInversion = level.getBlockState(pos.below()).is(RediscoveredTags.Blocks.SKYLANDS_PORTAL_BASE) ? 1 : -1;
		for (int i = 0; i < 1; i++)
		{
			double x = rand.nextFloat() * 0.90F + 0.05F;
			double y = 0.5 + (0.127 * verticalInversion);
			double z = rand.nextFloat() * 0.90F + 0.05F;
			double horizontalSpeed = 0.001;
			double dx = (rand.nextFloat() - 0.5) * horizontalSpeed;
			double dy = (rand.nextFloat() - 0.5) * 0.0002 + 0.0001;
			double dz = (rand.nextFloat() - 0.5) * horizontalSpeed;
			level.addParticle(RediscoveredParticles.SKYLANDS_PORTAL, pos.getX() + x, pos.getY() + y, pos.getZ() + z, dx, dy * verticalInversion, dz);
		}
	}

	@OnlyIn(Dist.CLIENT)
	public net.minecraft.client.resources.sounds.SoundInstance getTriggerSound()
	{
		return net.minecraft.client.resources.sounds.SimpleSoundInstance.forLocalAmbience(SoundEvents.PORTAL_TRIGGER, new Random().nextFloat() * 0.4F + 0.8F, 0.25F);
	}

	@OnlyIn(Dist.CLIENT)
	public net.minecraft.client.resources.sounds.SoundInstance getTravelSound()
	{
		return net.minecraft.client.resources.sounds.SimpleSoundInstance.forLocalAmbience(SoundEvents.PORTAL_TRAVEL, new Random().nextFloat() * 0.4F + 0.8F, 0.25F);
	}

	public static class Teleporter extends GelTeleporter
	{
		public Teleporter(ServerLevel serverLevel, GelPortalBlock portal)
		{
			super(serverLevel, () -> RediscoveredDimensions.skylandsKey(), () -> Level.OVERWORLD, () -> RediscoveredPoiTypes.SKYLANDS_PORTAL.get(), () -> portal, () -> RediscoveredBlocks.large_bricks.defaultBlockState(), CreatePortalBehavior.NETHER);
		}

		@Override
		public PortalInfo getPortalInfo(Entity entity, ServerLevel destLevel, Function<ServerLevel, PortalInfo> defaultPortalInfo)
		{
			// Scale position
			BlockPos scaledPos = this.scalePosition(entity, destLevel);

			// Get info about current portal
			BlockPos portalPos = this.getPortalEntrancePos(entity);
			BlockState portalState = entity.level().getBlockState(portalPos);
			Vec3 offset = this.getPositionInPortal(entity, portalPos, portalState);

			// Find or create a new portal
			Optional<BlockPos> result = this.findPortalAround(scaledPos, portalState, entity);
			if (entity instanceof ServerPlayer && !result.isPresent())
				result = this.createPortal(scaledPos);
			if (!result.isPresent())
				return null;

			// Get info from new portal
			PortalInfo portalInfo = new PortalInfo(Vec3.atBottomCenterOf(result.get()).add(offset), entity.getDeltaMovement(), entity.getYRot(), entity.getXRot());
			return portalInfo;
		}

		protected Vec3 getPositionInPortal(Entity entity, BlockPos portalPos, BlockState portalState)
		{
			Level currentLevel = entity.level();
			Vec3i portalOffset = SkylandsPortalBlock.getPortalOffset(portalState, currentLevel, portalPos);
			if (portalOffset != null)
			{
				Vec3 entityOffset = entity.position().subtract(portalPos.getCenter());
				return new Vec3(entityOffset.x() - portalOffset.getX(), 0, entityOffset.z() - portalOffset.getZ());
			}

			return Vec3.ZERO;
		}

		/**
		 * Returns the center of the portal
		 */
		protected Optional<BlockPos> findPortalAround(BlockPos startPos, BlockState portalState, Entity entity)
		{
			PoiManager poiManager = this.getLevel().getPoiManager();
			int dist = (int) Math.max(DimensionType.getTeleportationScale(this.getLevel().getServer().getLevel(this.getOpposite()).dimensionType(), this.getLevel().dimensionType()) * 16, 16);
			poiManager.ensureLoadedAndValid(this.getLevel(), startPos, dist);

			// Find nearest portal poi
			//@formatter:off
			Optional<PoiRecord> optional = poiManager.getInSquare(poiType -> poiType.value() == this.getPortalPOI().get(), startPos, dist, PoiManager.Occupancy.ANY)
					.filter(poi -> poiManager.getType(poi.getPos().below()).orElse(null) != poi.getPoiType())
					.min(Comparator.<PoiRecord>comparingDouble(poi -> poi.getPos().distSqr(startPos)).thenComparingInt(poi -> poi.getPos().getY()));
			//@formatter:on

			return optional.map(poi ->
			{
				BlockPos poiPos = poi.getPos();
				this.getLevel().getChunkSource().addRegionTicket(TicketType.PORTAL, new ChunkPos(poiPos), 3, poiPos);
				BlockState poiState = this.getLevel().getBlockState(poiPos);

				Vec3i portalOffset = SkylandsPortalBlock.getPortalOffset(poiState, this.getLevel(), poiPos);
				BlockPos teleportTo = portalOffset != null ? poiPos.offset(portalOffset) : poiPos;
				if (!this.getLevel().getBlockState(teleportTo.above()).isAir() && this.getLevel().getBlockState(teleportTo.below()).isAir())
				{
					double entityHeight = Math.ceil(entity.getBoundingBox().getYsize()) - 1.0;
					teleportTo = BlockPos.containing(Vec3.atBottomCenterOf(teleportTo).add(0, -entityHeight, 0));
				}
				return teleportTo;
			});
		}

		protected Optional<BlockPos> createPortal(BlockPos startPos)
		{
			ServerLevel destLevel = this.getLevel();
			BlockPos portalPos = this.findPortalPosition(destLevel, startPos);
			this.makePortal(this.getLevel(), portalPos);
			return Optional.of(portalPos);
		}

		protected BlockPos findPortalPosition(ServerLevel destLevel, BlockPos origin)
		{
			int x = origin.getX();
			int y = destLevel.getHeight();
			int z = origin.getZ();

			// Calculate position - find heighest valid block
			BlockPos.MutableBlockPos mutablePos = new BlockPos.MutableBlockPos(x, y, z);
			int i = y;
			int minHeight = destLevel.getMinBuildHeight();
			BlockState state = destLevel.getBlockState(mutablePos);
			while (i > minHeight && this.shouldIgnoreBlock(state, mutablePos))
			{
				state = destLevel.getBlockState(mutablePos.move(Direction.DOWN));
				i--;
			}

			if (i <= minHeight)
				y = this.getDefaultHeight();
			else
				y = i + 1;

			return new BlockPos(x, y, z);
		}

		protected void makePortal(ServerLevel destLevel, BlockPos origin)
		{
			// Make portal
			int r = INNER_RADIUS;
			for (int x = -r; x <= r; x++)
				for (int z = -r; z <= r; z++)
					destLevel.setBlock(origin.offset(x, -1, z), RediscoveredBlocks.glowing_obsidian.defaultBlockState(), Block.UPDATE_ALL);

			int f = r + 1;
			for (int x = -f; x <= f; x++)
			{
				for (int z = -f; z <= f; z++)
				{
					int ax = Math.abs(x);
					int az = Math.abs(z);
					if (ax == f || az == f)
						destLevel.setBlock(origin.offset(x, 0, z), RediscoveredBlocks.large_bricks.defaultBlockState(), Block.UPDATE_ALL);
				}
			}

			for (int x = -r; x <= r; x++)
				for (int z = -r; z <= r; z++)
					destLevel.setBlock(origin.offset(x, 0, z), RediscoveredBlocks.skylands_portal.defaultBlockState(), Block.UPDATE_KNOWN_SHAPE + Block.UPDATE_CLIENTS);

			// Place air above the portal
			for (int x = -f; x <= f; x++)
				for (int z = -f; z <= f; z++)
					for (int y = 1; y <= 2; y++)
						destLevel.setBlock(origin.offset(x, y, z), Blocks.AIR.defaultBlockState(), Block.UPDATE_ALL);
		}
	}
}
