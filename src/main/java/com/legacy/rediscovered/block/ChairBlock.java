package com.legacy.rediscovered.block;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.util.MountableBlockEntity;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.structure_gel.api.util.VoxelShapeUtil;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

/*
 * This class is used to create the properties of the chair, and does
 * not actually allow the player to sit on it. In order to do that, you must
 * tag it as a chair. Refer to RediscoveredTags for that.
 * 
 * This means if you're making your own chair, you don't need to copy this class into your own mod to make that work.
 */
public class ChairBlock extends HorizontalDirectionalBlock implements SimpleWaterloggedBlock
{
	public static final MapCodec<ChairBlock> CODEC = simpleCodec(ChairBlock::new);
	private static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;

	private static final Map<Direction, VoxelShape> SHAPES = VoxelShapeUtil.createRotations(Shapes.or(Block.box(2, 8, 12, 14, 16, 14), Block.box(2, 0, 12, 4, 6, 14), Block.box(12, 0, 2, 14, 6, 4), Block.box(12, 0, 12, 14, 6, 14), Block.box(2, 0, 2, 4, 6, 4), Block.box(2, 6, 2, 14, 8, 14)));

	public ChairBlock(Properties builder)
	{
		super(builder);
		this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(WATERLOGGED, false));
	}

	@Override
	protected MapCodec<? extends HorizontalDirectionalBlock> codec()
	{
		return null;
	}

	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult hit)
	{
		return InteractionResult.PASS;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
	{
		return SHAPES.get(state.getValue(FACING));
	}

	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : Fluids.EMPTY.defaultFluidState();
	}

	@Override
	public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor worldIn, BlockPos currentPos, BlockPos facingPos)
	{
		if (stateIn.getValue(WATERLOGGED))
			worldIn.scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickDelay(worldIn));

		return stateIn;
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockPos blockpos = context.getClickedPos();
		BlockState blockstate = context.getLevel().getBlockState(blockpos);

		if (blockstate.getBlock() == this)
			return blockstate.setValue(WATERLOGGED, Boolean.valueOf(false));

		FluidState ifluidstate = context.getLevel().getFluidState(blockpos);
		BlockState blockstate1 = this.defaultBlockState().setValue(WATERLOGGED, Boolean.valueOf(ifluidstate.getType() == Fluids.WATER)).setValue(FACING, context.getHorizontalDirection().getOpposite());
		return blockstate1;
	}

	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
	{
		return true;
	}

	@Override
	public int getAnalogOutputSignal(BlockState state, Level level, BlockPos pos)
	{
		// If the chair has a passenger, comparators can read it
		if (!level.getEntitiesOfClass(MountableBlockEntity.class, AABB.ofSize(pos.getCenter(), 0.3, 0.3, 0.3), entity -> !entity.getPassengers().isEmpty()).isEmpty())
			return 15;
		return 0;
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(FACING, WATERLOGGED);
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rotation)
	{
		return state.setValue(FACING, rotation.rotate(state.getValue(FACING)));
	}

	@Override
	public BlockState mirror(BlockState state, Mirror mirror)
	{
		return this.rotate(state, mirror.getRotation(state.getValue(FACING)));
	}

	@Override
	public BlockPathTypes getBlockPathType(BlockState state, BlockGetter level, BlockPos pos, @Nullable Mob mob)
	{
		return RediscoveredBlocks.FURNITURE_PATH_TYPE;
	}

	public static boolean canMount(Level level, BlockPos pos, boolean dismountNonPlayers)
	{
		if (!level.getBlockState(pos).is(RediscoveredTags.Blocks.CHAIRS))
			return false;

		List<MountableBlockEntity> listEMB = level.getEntitiesOfClass(MountableBlockEntity.class, new AABB(pos));

		if (!listEMB.isEmpty())
		{
			if (dismountNonPlayers)
				listEMB.forEach(mount -> mount.ejectPassengers());

			return false;
		}

		return true;
	}

	public static boolean tryInteract(LivingEntity entity, BlockPos pos, @Nullable InteractionHand hand)
	{
		Level level = entity.level();

		if (!canMount(level, pos, entity instanceof Player))
			return false;

		float mountingX = pos.getX() + 0.5F;
		float mountingY = pos.getY();
		float mountingZ = pos.getZ() + 0.5F;

		BlockState state = level.getBlockState(pos);

		MountableBlockEntity mount = new MountableBlockEntity(level, pos.getX(), pos.getY(), pos.getZ(), mountingX, mountingY, mountingZ);

		if (state.hasProperty(ChairBlock.FACING))
		{
			mount.setYRot(state.getValue(ChairBlock.FACING).toYRot());
			mount.setYHeadRot(mount.getYRot());
			mount.setYBodyRot(mount.getYRot());
			mount.yRotO = mount.getYRot();
		}

		level.addFreshEntity(mount);

		entity.swing(InteractionHand.MAIN_HAND);
		mount.interactBasic(entity);

		return true;
	}
}
