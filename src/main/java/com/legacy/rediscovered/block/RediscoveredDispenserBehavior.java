package com.legacy.rediscovered.block;

import java.util.function.Consumer;

import com.legacy.rediscovered.entity.PurpleArrowEntity;
import com.legacy.rediscovered.entity.ScarecrowEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.core.dispenser.AbstractProjectileDispenseBehavior;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraft.world.phys.AABB;

public class RediscoveredDispenserBehavior
{
	public static void init()
	{
		// Shears working on rose bushes are handled in ShearsDispenseItemBehaviorMixin

		DispenserBlock.registerBehavior(RediscoveredItems.purple_arrow, new AbstractProjectileDispenseBehavior()
		{
			@Override
			protected Projectile getProjectile(Level level, Position pos, ItemStack stack)
			{
				PurpleArrowEntity arrow = new PurpleArrowEntity(level, pos.x(), pos.y(), pos.z(), stack.copyWithCount(1));
				arrow.pickup = AbstractArrow.Pickup.ALLOWED;
				return arrow;
			}
		});

		DispenserBlock.registerBehavior(RediscoveredItems.scarecrow, new DefaultDispenseItemBehavior()
		{
			@Override
			public ItemStack execute(BlockSource level, ItemStack stack)
			{
				Direction direction = level.state().getValue(DispenserBlock.FACING);
				BlockPos pos = level.pos().relative(direction);
				ServerLevel serverLevel = level.level();
				Consumer<ScarecrowEntity> consumer = EntityType.appendDefaultStackConfig(sc -> sc.setYRot(direction.toYRot()), serverLevel, stack, (Player) null);
				ScarecrowEntity scarecrow = RediscoveredEntityTypes.SCARECROW.spawn(serverLevel, stack.getTag(), consumer, pos, MobSpawnType.DISPENSER, false, false);
				if (scarecrow != null)
					stack.shrink(1);
				return stack;
			}
		});

		DispenserBlock.registerBehavior(RediscoveredItems.dragon_armor, new OptionalDispenseItemBehavior()
		{
			@Override
			protected ItemStack execute(BlockSource level, ItemStack stack)
			{
				BlockPos pos = level.pos().relative(level.state().getValue(DispenserBlock.FACING));

				var dragons = level.level().getEntitiesOfClass(RedDragonOffspringEntity.class, new AABB(pos), dragon -> dragon.isAlive());
				if (!dragons.isEmpty())
				{
					for (RedDragonOffspringEntity dragon : dragons)
					{
						if (dragon.getArmor().isEmpty() && dragon.isTame())
						{
							dragon.equipArmor(null, stack.split(1));
							this.setSuccess(true);
							return stack;
						}
					}
				}
				return super.execute(level, stack);
			}
		});
	}
}
