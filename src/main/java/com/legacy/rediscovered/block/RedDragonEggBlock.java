package com.legacy.rediscovered.block;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block_entities.RedDragonEggBlockEntity;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class RedDragonEggBlock extends FallingBlock implements EntityBlock
{
	public static final MapCodec<RedDragonEggBlock> CODEC = simpleCodec(RedDragonEggBlock::new);
	// Used for rendering the egg properly while falling
	public static final BooleanProperty FALLING = BooleanProperty.create("falling");

	protected static final VoxelShape SHAPE = Shapes.or(Block.box(6, 15, 6, 10, 16, 10), Block.box(5, 14, 5, 11, 15, 11), Block.box(5, 13, 5, 11, 14, 11), Block.box(3, 11, 3, 13, 13, 13), Block.box(2, 8, 2, 14, 11, 14), Block.box(1, 3, 1, 15, 8, 15), Block.box(2, 1, 2, 14, 3, 14), Block.box(3, 0, 3, 13, 1, 13));
	public static final String HATCH_DISABLED_KEY = RediscoveredMod.MODID + ".message.dragon_hatch_disabled";

	public RedDragonEggBlock(Block.Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.stateDefinition.any().setValue(FALLING, false));
	}
	@Override
	protected MapCodec<? extends FallingBlock> codec()
	{
		return CODEC;
	}
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(FALLING);
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		return SHAPE;
	}

	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return state.getValue(FALLING) ? RenderShape.MODEL : RenderShape.ENTITYBLOCK_ANIMATED;
	}

	@Override
	public ItemStack getCloneItemStack(LevelReader level, BlockPos pos, BlockState state)
	{
		ItemStack stack = new ItemStack(this);
		level.getBlockEntity(pos, RediscoveredBlockEntityTypes.RED_DRAGON_EGG).ifPresent(egg ->
		{
			if (egg.hasCustomName())
				stack.setHoverName(egg.getName());
		});
		return stack;
	}

	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hitResult)
	{
		// Disable hatching if the config is off, and display a message
		if (!RediscoveredConfig.WORLD.hatchableRedDragon())
		{
			player.displayClientMessage(Component.translatable(RedDragonEggBlock.HATCH_DISABLED_KEY), true);
			return InteractionResult.SUCCESS;
		}

		RedDragonEggBlockEntity egg = level.getBlockEntity(pos, RediscoveredBlockEntityTypes.RED_DRAGON_EGG).orElse(null);
		if (egg != null)
		{
			// Using dragon breath on the egg starts the hatching process
			ItemStack heldItem = player.getItemInHand(hand);
			if (heldItem.is(Items.DRAGON_BREATH))
			{
				// This is where the owner of the dragon is set. If it's already set, this does nothing
				if (egg.startHatching(player))
				{
					if (!player.isCreative())
					{
						ItemStack container = heldItem.getCraftingRemainingItem();
						heldItem.shrink(1);
						if (heldItem.isEmpty()) // If the stack was emptied, put the empty bottle in their hand
							player.setItemInHand(hand, container);
						else // Place the empty bottle in their inventory
							player.addItem(container);
					}
					level.playSound(null, pos, RediscoveredSounds.BLOCK_RED_DRAGON_EGG_FERTILIZE, SoundSource.PLAYERS);
					if (level instanceof ServerLevel sl)
					{
						Vec3 p = Vec3.atCenterOf(pos).add(0, 0.3, 0);
						float offset = 0.3F;
						sl.sendParticles(RediscoveredParticles.DRAGON_BREATH_POUR, p.x, p.y, p.z, 60, offset, 0.15F, offset, 0.01F);
					}
					return InteractionResult.SUCCESS;
				}
			}

			// Creative mode players can hatch a dragon egg early, but the process has to be started by the person who should own it
			if (player.isCreative() && egg.canHatch() && egg.hatchDragon(level, state, pos, true))
				return InteractionResult.SUCCESS;
		}

		return InteractionResult.PASS;
	}

	@Override
	public void setPlacedBy(Level level, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack)
	{
		level.getBlockEntity(pos, RediscoveredBlockEntityTypes.RED_DRAGON_EGG).ifPresent(egg -> egg.setPlacedBy(stack, placer));
	}

	@Override
	public void tick(BlockState pState, ServerLevel pLevel, BlockPos pPos, RandomSource pRandom)
	{
		// Copied from FallingBlock, with the addition of saving the BlockEntity's tag before it falls so we can store it in the falling block. Why doesn't vanilla do this??
		if (isFree(pLevel.getBlockState(pPos.below())) && pPos.getY() >= pLevel.getMinBuildHeight())
		{
			CompoundTag tag = pLevel.getBlockEntity(pPos, RediscoveredBlockEntityTypes.RED_DRAGON_EGG).map(egg -> egg.saveWithoutMetadata()).orElse(null);
			// The falling state is set to true so it can use a block model and not a block entity render
			FallingBlockEntity fallingblockentity = FallingBlockEntity.fall(pLevel, pPos, pState.setValue(FALLING, true));
			if (tag != null)
				fallingblockentity.blockData = tag;
			this.falling(fallingblockentity);
		}
	}

	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, level, pos, oldState, isMoving);
		if (state.getValue(FALLING))
			level.setBlock(pos, state.setValue(FALLING, false), Block.UPDATE_CLIENTS);
	}

	// was tickRate?
	@Override
	public int getDelayAfterPlace()
	{
		return 5;
	}

	@Override
	public boolean isPathfindable(BlockState state, BlockGetter level, BlockPos pos, PathComputationType type)
	{
		return false;
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return RediscoveredBlockEntityTypes.RED_DRAGON_EGG.create(pos, state);
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type)
	{
		return createTickerHelper(type, RediscoveredBlockEntityTypes.RED_DRAGON_EGG, level.isClientSide ? RedDragonEggBlockEntity::clientTick : RedDragonEggBlockEntity::serverTick);
	}

	@SuppressWarnings("unchecked")
	@Nullable
	protected static <E extends BlockEntity, A extends BlockEntity> BlockEntityTicker<A> createTickerHelper(BlockEntityType<A> pServerType, BlockEntityType<E> pClientType, BlockEntityTicker<? super E> pTicker)
	{
		return pClientType == pServerType ? (BlockEntityTicker<A>) pTicker : null;
	}
}