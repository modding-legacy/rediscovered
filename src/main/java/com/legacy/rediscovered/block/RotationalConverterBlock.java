package com.legacy.rediscovered.block;

import org.jetbrains.annotations.Nullable;

import com.legacy.rediscovered.block.GearBlock.GearFace;
import com.legacy.rediscovered.block_entities.GearBlockEntity;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.SignalGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;

public class RotationalConverterBlock extends DirectionalBlock
{
	public static final MapCodec<RotationalConverterBlock> CODEC = simpleCodec(RotationalConverterBlock::new);
	public static enum Mode implements StringRepresentable
	{
		GEAR_TO_REDSTONE,
		REDSTONE_TO_GEAR;

		@Override
		public String getSerializedName()
		{
			return this.toString().toLowerCase();
		}
	}

	public static final EnumProperty<Mode> MODE = EnumProperty.create("mode", Mode.class);
	public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

	public RotationalConverterBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.defaultBlockState().setValue(POWERED, false).setValue(MODE, Mode.GEAR_TO_REDSTONE).setValue(DirectionalBlock.FACING, Direction.NORTH));
	}
	
	@Override
	protected MapCodec<? extends DirectionalBlock> codec()
	{
		return CODEC;
	}

	@Override
	public boolean useShapeForLightOcclusion(BlockState state)
	{
		return false;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}

	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn)
	{
		return this.rotate(state, mirrorIn.getRotation(state.getValue(FACING)));
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		Direction facing = state.getValue(FACING);

		// Convert gear rotation to a redstone signal
		if (state.getValue(MODE) == Mode.GEAR_TO_REDSTONE)
		{
			if (level.getBlockEntity(pos.relative(facing)) instanceof GearBlockEntity gear)
			{
				Direction redstoneSide = facing.getOpposite();
				GearFace gearFace = GearFace.get(redstoneSide);
				gear.stopSpinning(level, pos, pos.relative(facing), gearFace);

				level.setBlock(pos, state.setValue(POWERED, gear.isPowered(gearFace)), 3);
			}
			else
			{
				if (state.getValue(POWERED))
					level.setBlockAndUpdate(pos, state.setValue(POWERED, false));
			}

			this.updateNeighborsOnFaces(level, pos, state);
		}
		// Convert a redstone signal into rotation
		else
		{
			Direction redstoneSide = facing.getOpposite();
			GearFace gearFace = GearFace.get(redstoneSide);
			int redstoneStrength = level.getSignal(pos.relative(redstoneSide), redstoneSide);
			boolean powered = level.hasSignal(pos.relative(facing.getOpposite()), facing.getOpposite());
			if (powered != state.getValue(POWERED))
			{
				level.setBlockAndUpdate(pos, state.setValue(POWERED, powered));
				
			}
			BlockPos gearPos = pos.relative(facing);
			if (level.getBlockEntity(gearPos) instanceof GearBlockEntity gear && gear.getPower(gearFace).getPower() != redstoneStrength) // We don't need Math.abs on the gear's power because a gear from a source should always be positive
			{
				gear.stopSpinning(level, pos, pos.relative(facing), gearFace);
				if (powered)
					gear.startSpinning(level, pos, pos.relative(facing), gearFace, redstoneStrength, 0);
			}
		}
	}

	@Override
	public void neighborChanged(BlockState state, Level level, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving)
	{
		if (!level.isClientSide())
			level.scheduleTick(pos, this, 2);
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor level, BlockPos currentPos, BlockPos facingPos)
	{
		return state;
	}

	@Override
	public boolean isSignalSource(BlockState state)
	{
		return false; // See canConnectRedstone
	}

	@Override
	public boolean canConnectRedstone(BlockState state, BlockGetter level, BlockPos pos, @Nullable Direction direction)
	{
		return state.getValue(FACING) == direction ? true : super.canConnectRedstone(state, level, pos, direction);
	}

	// Calls getWeakPower
	@Override
	public int getDirectSignal(BlockState state, BlockGetter level, BlockPos pos, Direction side)
	{
		return state.getSignal(level, pos, side);
	}

	// Get's the power strength from the connected to it when in the correct mode. 0
	// otherwise.
	@Override
	public int getSignal(BlockState state, BlockGetter level, BlockPos pos, Direction side)
	{
		if (state.getValue(MODE) == Mode.GEAR_TO_REDSTONE && state.getValue(POWERED) && state.getValue(FACING) == side)
		{
			if (level.getBlockEntity(pos.relative(side)) instanceof GearBlockEntity gear)
			{
				return Math.abs(gear.getPower(GearFace.get(side.getOpposite())).getPower());
			}
			return 0;
		}
		else
			return 0;
	}

	// Only check for weak redstone power on the correct side in the correct mode
	@Override
	public boolean shouldCheckWeakPower(BlockState state, SignalGetter level, BlockPos pos, Direction side)
	{
		return side == state.getValue(FACING).getOpposite() && state.getValue(MODE) == Mode.REDSTONE_TO_GEAR;
	}

	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		if (state.getBlock() != oldState.getBlock())
		{
			if (!level.isClientSide() && state.getValue(POWERED) && !level.getBlockTicks().hasScheduledTick(pos, this))
			{
				BlockState blockstate = state.setValue(POWERED, Boolean.valueOf(false));
				level.setBlock(pos, blockstate, 18);
				this.updateNeighborsOnFaces(level, pos, blockstate);
			}

		}
		state.updateNeighbourShapes(level, pos, 3);

	}

	@Override
	public void onRemove(BlockState state, Level level, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if (state.getBlock() != newState.getBlock())
		{
			if (!level.isClientSide && state.getValue(POWERED) && level.getBlockTicks().hasScheduledTick(pos, this))
			{
				this.updateNeighborsOnFaces(level, pos, state.setValue(POWERED, Boolean.valueOf(false)));
			}

		}
		state.updateNeighbourShapes(level, pos, 3);

	}

	@Override
	public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult hit)
	{
		// Let players place the gear on the gear side
		if (player.getItemInHand(handIn).getItem() == RediscoveredBlocks.gear.asItem() && state.getValue(FACING) == hit.getDirection())
			return InteractionResult.FAIL;
		// Change the mode of this to the opposite
		if (!level.isClientSide())
		{
			BlockState newState = state.cycle(MODE);
			level.setBlockAndUpdate(pos, newState); // cycle
			float f = newState.getValue(POWERED) ? 0.6F : 0.5F;
			level.playSound((Player) null, pos, RediscoveredSounds.BLOCK_ROTATIONAL_CONVERTER_CLICK, SoundSource.BLOCKS, 0.3F, f);
			this.updateNeighbors(newState, level, pos);
		}

		return InteractionResult.SUCCESS;
	}

	protected void updateNeighborsOnFaces(Level level, BlockPos pos, BlockState state)
	{
		for (Direction direction : new Direction[] { state.getValue(FACING), state.getValue(FACING).getOpposite() })
		{
			BlockPos blockpos = pos.relative(direction.getOpposite());
			level.neighborChanged(blockpos, this, pos);
			level.updateNeighborsAtExceptFromFacing(blockpos, this, direction);
		}
	}

	private void updateNeighbors(BlockState state, Level level, BlockPos pos)
	{
		level.updateNeighborsAt(pos, this);
		Direction facing = state.getValue(DirectionalBlock.FACING);
		level.updateNeighborsAt(pos.relative(facing), this);
		level.updateNeighborsAt(pos.relative(facing.getOpposite()), this);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return this.defaultBlockState().setValue(FACING, context.getNearestLookingDirection().getOpposite());
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(MODE, POWERED, DirectionalBlock.FACING);
	}

}
