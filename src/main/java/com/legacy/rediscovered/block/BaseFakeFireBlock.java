package com.legacy.rediscovered.block;

import javax.annotation.Nullable;

import com.legacy.rediscovered.event.api.GetFakeFireEvent;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoulFireBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.neoforged.neoforge.common.NeoForge;

public abstract class BaseFakeFireBlock extends BaseFireBlock
{
	protected final float damage;

	public BaseFakeFireBlock(Properties properties, float damage)
	{
		super(properties, damage);
		this.damage = damage;
	}

	@Override
	@Nullable
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return getState(context.getLevel(), context.getClickedPos());
	}

	@Nullable
	public static BlockState getState(BlockGetter level, BlockPos pos)
	{
		BlockPos belowPos = pos.below();
		BlockState belowState = level.getBlockState(belowPos);
		var event = new GetFakeFireEvent(SoulFireBlock.canSurviveOnBlock(belowState) ? RediscoveredBlocks.fake_soul_fire.defaultBlockState() : RediscoveredBlocks.fake_fire.defaultBlockState(), level, pos, belowState);
		NeoForge.EVENT_BUS.post(event);
		return event.getNewFire();
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState neighborState, LevelAccessor level, BlockPos pos, BlockPos neighborPos)
	{
		return state.canSurvive(level, pos) ? getState(level, pos) : Blocks.AIR.defaultBlockState();
	}

	@Override
	public void entityInside(BlockState state, Level level, BlockPos pos, Entity entity)
	{
		// Don't burn items
		if (entity instanceof ItemEntity || entity instanceof ExperienceOrb)
			return;
		super.entityInside(state, level, pos, entity);
	}

	@Override
	public void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, level, pos, oldState, isMoving);
		level.scheduleTick(pos, this, this.getFireTickDelay(level.random));
	}

	protected int getFireTickDelay(RandomSource rand)
	{
		return 30 + rand.nextInt(10);
	}

	protected boolean isNearRain(Level level, BlockPos pos)
	{
		return level.isRaining() && (level.isRainingAt(pos) || level.isRainingAt(pos.west()) || level.isRainingAt(pos.east()) || level.isRainingAt(pos.north()) || level.isRainingAt(pos.south()));
	}

	@Override
	protected boolean canBurn(BlockState state)
	{
		return true;
	}

	public static class FakeFireBlock extends BaseFakeFireBlock
	{
		public static final MapCodec<FakeFireBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(propertiesCodec(), Codec.FLOAT.fieldOf("damage").forGetter(o -> o.damage)).apply(instance, FakeFireBlock::new));
		public static final IntegerProperty AGE = BlockStateProperties.AGE_4;

		public FakeFireBlock(Properties properties, float damage)
		{
			super(properties, damage);
			this.registerDefaultState(this.defaultBlockState().setValue(AGE, 0));
		}

		@Override
		protected MapCodec<? extends BaseFireBlock> codec()
		{
			return CODEC;
		}

		protected int maxAge()
		{
			return 4;
		}

		protected IntegerProperty ageProperty()
		{
			return AGE;
		}

		@Override
		protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
		{
			builder.add(this.ageProperty());
		}

		@Override
		public BlockState updateShape(BlockState state, Direction neighborDir, BlockState neighborState, LevelAccessor level, BlockPos pos, BlockPos neighborPos)
		{
			return this.canSurvive(state, level, pos) ? this.getStateWithAge(level, pos, state.getValue(this.ageProperty())) : Blocks.AIR.defaultBlockState();
		}

		protected BlockState getStateWithAge(LevelAccessor level, BlockPos pos, int age)
		{
			BlockState state = getState(level, pos);
			return state.hasProperty(this.ageProperty()) ? state.setValue(this.ageProperty(), age) : state;
		}

		@Override
		public boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
		{
			return state.getValue(this.ageProperty()) < this.maxAge() && level.getBlockState(pos.below()).isFaceSturdy(level, pos.below(), Direction.UP);
		}

		// Fire is full of "unusual" code and this tries to replicate it. Idk anymore.
		@Override
		public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
		{
			// Forge passes in the position of the fire instead of the block it's on for some reason. This may need fixed in the future.
			level.scheduleTick(pos, this, this.getFireTickDelay(rand));
			if (level.getGameRules().getBoolean(GameRules.RULE_DOFIRETICK))
			{
				BlockPos groundPos = pos.below();
				BlockState groundState = level.getBlockState(groundPos);
				boolean onInfiniburn = groundState.isFireSource(level, pos, Direction.UP);
				int age = state.getValue(this.ageProperty());

				if (!onInfiniburn && this.isNearRain(level, pos) && rand.nextFloat() < 0.2F + age * 0.03F)
				{
					// Make rain remove the fire
					level.removeBlock(pos, false);
				}
				else
				{
					// When on infiniburn blocks, don't age
					int newAge = onInfiniburn ? age : Math.min(this.maxAge(), age + rand.nextInt(3) / 2);

					// Extinguish if we're too old
					if (newAge >= this.maxAge())
					{
						level.removeBlock(pos, false);
						return;
					}
					// Increase age
					else if (age != newAge)
					{
						state = state.setValue(this.ageProperty(), newAge);
						level.setBlock(pos, state, Block.UPDATE_INVISIBLE);
					}
				}
			}
		}
	}

	public static class FakeSoulFireBlock extends BaseFakeFireBlock
	{
		public static final MapCodec<FakeSoulFireBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(propertiesCodec(), Codec.FLOAT.fieldOf("damage").forGetter(o -> o.damage)).apply(instance, FakeSoulFireBlock::new));

		public FakeSoulFireBlock(Properties properties, float damage)
		{
			super(properties, damage);
		}
		
		@Override
		protected MapCodec<? extends BaseFireBlock> codec()
		{
			return CODEC;
		}

		@Override
		public boolean canSurvive(BlockState state, LevelReader level, BlockPos pos)
		{
			return SoulFireBlock.canSurviveOnBlock(level.getBlockState(pos.below()));
		}
	}
}
