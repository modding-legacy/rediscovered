package com.legacy.rediscovered.capability.util;

import javax.annotation.Nullable;

import com.legacy.rediscovered.item.util.QuiverData;

import net.minecraft.world.item.ItemStack;

public interface QuiverDataHolder
{
	@Nullable
	QuiverData rediscovered$getQuiverData();

	void rediscovered$setQuiverData(@Nullable QuiverData data, ItemStack weapon);
}
