package com.legacy.rediscovered.data;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredLootModifiers;

import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.EntityTypePredicate;
import net.minecraft.data.PackOutput;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemKilledByPlayerCondition;
import net.neoforged.neoforge.common.data.GlobalLootModifierProvider;

public class RediscoveredLootModifierProv extends GlobalLootModifierProvider
{
	public RediscoveredLootModifierProv(PackOutput output)
	{
		super(output, RediscoveredMod.MODID);
	}

	@Override
	protected void start()
	{
		this.add("giant_golden_apple", new RediscoveredLootModifiers.AddItemLootModifier(new LootItemCondition[] { LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, EntityPredicate.Builder.entity().entityType(EntityTypePredicate.of(EntityType.GIANT))).build(), LootItemKilledByPlayerCondition.killedByPlayer().build() }, 0.05F, Items.GOLDEN_APPLE.getDefaultInstance()));
	}

}
