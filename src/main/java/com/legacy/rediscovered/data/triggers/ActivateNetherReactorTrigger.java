package com.legacy.rediscovered.data.triggers;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;

public class ActivateNetherReactorTrigger extends SimpleCriterionTrigger<ActivateNetherReactorTrigger.TriggerInstance>
{
	@Override
	public Codec<TriggerInstance> codec()
	{
		return TriggerInstance.CODEC;
	}

	public void trigger(ServerPlayer player, boolean isFullPower)
	{
		this.trigger(player, instance -> instance.test(isFullPower));
	}

	public static record TriggerInstance(Optional<ContextAwarePredicate> player, Optional<Boolean> isFullPower) implements SimpleCriterionTrigger.SimpleInstance
	{
		public static final Codec<TriggerInstance> CODEC = RecordCodecBuilder.create(instance -> instance.group(ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "player").forGetter(TriggerInstance::player), ExtraCodecs.strictOptionalField(Codec.BOOL, "is_full_power").forGetter(TriggerInstance::isFullPower)).apply(instance, TriggerInstance::new));

		private boolean test(boolean isFullPower)
		{
			return this.isFullPower.isEmpty() || this.isFullPower.get() == isFullPower;
		}

		public static Criterion<TriggerInstance> any()
		{
			return RediscoveredTriggers.ACTIVATE_NETHER_REACTOR.get().createCriterion(new ActivateNetherReactorTrigger.TriggerInstance(Optional.empty(), Optional.empty()));
		}

		public static Criterion<TriggerInstance> fullPower()
		{
			return RediscoveredTriggers.ACTIVATE_NETHER_REACTOR.get().createCriterion(new ActivateNetherReactorTrigger.TriggerInstance(Optional.empty(), Optional.of(Boolean.TRUE)));
		}
	}
}