package com.legacy.rediscovered.data.triggers;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class InFarlandsTrigger extends SimpleCriterionTrigger<InFarlandsTrigger.TriggerInstance>
{
	@Override
	public Codec<TriggerInstance> codec()
	{
		return TriggerInstance.CODEC;
	}

	public void trigger(ServerPlayer player)
	{
		this.trigger(player, instance -> instance.test(player.position(), player.level().dimension()));
	}

	public static record TriggerInstance(Optional<ContextAwarePredicate> player, int minX, int maxX, int minZ, int maxZ, Optional<ResourceKey<Level>> dimension) implements SimpleCriterionTrigger.SimpleInstance
	{

		//@formatter:off
		public static final Codec<TriggerInstance> CODEC = RecordCodecBuilder.create(instance -> instance.group(
				ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "player").forGetter(TriggerInstance::player),
				Codec.INT.fieldOf("min_x").forGetter(TriggerInstance::minX),
				Codec.INT.fieldOf("max_x").forGetter(TriggerInstance::maxX),
				Codec.INT.fieldOf("min_z").forGetter(TriggerInstance::minZ),
				Codec.INT.fieldOf("max_z").forGetter(TriggerInstance::maxZ), 
				ExtraCodecs.strictOptionalField(ResourceKey.codec(Registries.DIMENSION), "dimension").forGetter(TriggerInstance::dimension)).apply(instance, TriggerInstance::new));
		//@formatter:on
		public static final int MIN_FARLANDS = -12550824, MAX_FARLANDS = 12550817;

		public static Criterion<TriggerInstance> skylands()
		{
			return RediscoveredTriggers.IN_FARLANDS_TRIGGER.get().createCriterion(new InFarlandsTrigger.TriggerInstance(Optional.empty(), MIN_FARLANDS, MAX_FARLANDS, MIN_FARLANDS, MAX_FARLANDS, Optional.of(RediscoveredDimensions.skylandsKey())));
		}

		private boolean test(Vec3 pos, ResourceKey<Level> dimension)
		{
			return (pos.x <= this.minX || pos.x >= this.maxX || pos.z <= this.minZ || pos.z >= this.maxZ) && (this.dimension.isEmpty() || this.dimension.get().equals(dimension));
		}
	}
}