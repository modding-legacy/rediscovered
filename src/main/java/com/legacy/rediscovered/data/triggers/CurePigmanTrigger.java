package com.legacy.rediscovered.data.triggers;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.storage.loot.LootContext;

public class CurePigmanTrigger extends SimpleCriterionTrigger<CurePigmanTrigger.TriggerInstance>
{
	@Override
	public Codec<TriggerInstance> codec()
	{
		return TriggerInstance.CODEC;
	}

	public void trigger(ServerPlayer player, Entity pigman)
	{
		LootContext lootContext = EntityPredicate.createContext(player, pigman);
		this.trigger(player, instance -> instance.test(player, lootContext, pigman));
	}

	public static record TriggerInstance(Optional<ContextAwarePredicate> player, Optional<ContextAwarePredicate> pigman) implements SimpleCriterionTrigger.SimpleInstance
	{
		public static final Codec<TriggerInstance> CODEC = RecordCodecBuilder.create(instance -> instance.group(ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "player").forGetter(TriggerInstance::player), ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "pigman").forGetter(TriggerInstance::pigman)).apply(instance, TriggerInstance::new));

		public static Criterion<TriggerInstance> any()
		{
			return RediscoveredTriggers.CURE_PIGMAN.get().createCriterion(new CurePigmanTrigger.TriggerInstance(Optional.empty(), Optional.empty()));
		}

		public static Criterion<TriggerInstance> pigman(EntityPredicate.Builder pigman)
		{
			return RediscoveredTriggers.CURE_PIGMAN.get().createCriterion(new CurePigmanTrigger.TriggerInstance(Optional.empty(), Optional.of(EntityPredicate.wrap(pigman.build()))));
		}

		public boolean test(ServerPlayer player, LootContext lootContext, Entity entity)
		{
			return this.pigman.isEmpty() || this.pigman.get().matches(lootContext);
		}
	}
}