package com.legacy.rediscovered.data.triggers;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.storage.loot.LootContext;

public class GiveRanaItemTrigger extends SimpleCriterionTrigger<GiveRanaItemTrigger.TriggerInstance>
{
	@Override
	public Codec<TriggerInstance> codec()
	{
		return TriggerInstance.CODEC;
	}

	public void trigger(ServerPlayer player, Entity rana)
	{
		LootContext lootContext = EntityPredicate.createContext(player, rana);
		this.trigger(player, instance -> instance.test(player, lootContext, rana));
	}

	public static record TriggerInstance(Optional<ContextAwarePredicate> player, Optional<ContextAwarePredicate> rana) implements SimpleCriterionTrigger.SimpleInstance
	{
		public static final Codec<TriggerInstance> CODEC = RecordCodecBuilder.create(instance -> instance.group(ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "player").forGetter(TriggerInstance::player), ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "rana").forGetter(TriggerInstance::rana)).apply(instance, TriggerInstance::new));

		public static Criterion<TriggerInstance> any()
		{
			return RediscoveredTriggers.GIVE_RANA_ITEM.get().createCriterion(new GiveRanaItemTrigger.TriggerInstance(Optional.empty(), Optional.empty()));
		}

		public static Criterion<TriggerInstance> rana(EntityPredicate.Builder rana)
		{
			return RediscoveredTriggers.GIVE_RANA_ITEM.get().createCriterion(new GiveRanaItemTrigger.TriggerInstance(Optional.empty(), Optional.of(EntityPredicate.wrap(rana.build()))));
		}

		public boolean test(ServerPlayer player, LootContext lootContext, Entity entity)
		{
			return this.rana.isEmpty() || this.rana.get().matches(lootContext);
		}
	}
}