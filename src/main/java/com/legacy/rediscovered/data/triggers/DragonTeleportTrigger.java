package com.legacy.rediscovered.data.triggers;

import java.util.Optional;

import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;

public class DragonTeleportTrigger extends SimpleCriterionTrigger<DragonTeleportTrigger.TriggerInstance>
{
	@Override
	public Codec<TriggerInstance> codec()
	{
		return TriggerInstance.CODEC;
	}

	public void trigger(ServerPlayer player)
	{
		this.trigger(player, instance -> true);
	}

	public static record TriggerInstance(Optional<ContextAwarePredicate> player) implements SimpleCriterionTrigger.SimpleInstance
	{
		public static final Codec<TriggerInstance> CODEC = RecordCodecBuilder.create(instance -> instance.group(ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "player").forGetter(TriggerInstance::player)).apply(instance, TriggerInstance::new));

		public static Criterion<TriggerInstance> any()
		{
			return RediscoveredTriggers.DRAGON_TELEPORT.get().createCriterion(new DragonTeleportTrigger.TriggerInstance(Optional.empty()));
		}
	}
}