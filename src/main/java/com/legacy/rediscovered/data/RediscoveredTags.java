package com.legacy.rediscovered.data;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.Structure;

public class RediscoveredTags
{
	public static interface Blocks
	{
		TagKey<Block> ORES_RUBY = BlockTags.create(new ResourceLocation("forge", "ores/ruby"));
		TagKey<Block> STORAGE_BLOCKS_RUBY = BlockTags.create(new ResourceLocation("forge", "storage_blocks/ruby"));

		TagKey<Block> TABLES = tag("tables");
		TagKey<Block> CHAIRS = tag("chairs");

		TagKey<Block> NETHER_REACTOR_POWER_BLOCK = tag("nether_reactor_power_block");
		TagKey<Block> NETHER_REACTOR_STRONG_POWER_BLOCK = tag("nether_reactor_strong_power_block");
		TagKey<Block> NETHER_REACTOR_BASE_BLOCK = tag("nether_reactor_base_block");

		TagKey<Block> SKYLANDS_PORTAL_FRAME = tag("skylands_portal_frame");
		TagKey<Block> SKYLANDS_PORTAL_BASE = tag("skylands_portal_base");

		TagKey<Block> PIGMAN_VILLAGE_FLOWERS = tag("pigman_village_flowers");
		TagKey<Block> PIGMAN_VILLAGE_RARE_FLOWERS = tag("pigman_village_rare_flowers");

		TagKey<Block> SKYLANDS_POOL_REPLACEABLE = tag("skylands_pool_replaceable");

		private static TagKey<Block> tag(String name)
		{
			return BlockTags.create(RediscoveredMod.locate(name));
		}
	}

	public static interface Items
	{
		TagKey<Item> STORAGE_BLOCKS_RUBY = ItemTags.create(new ResourceLocation("forge", "storage_blocks/ruby"));
		TagKey<Item> ORES_RUBY = ItemTags.create(new ResourceLocation("forge", "ores/ruby"));
		TagKey<Item> GEMS_RUBY = ItemTags.create(new ResourceLocation("forge", "gems/ruby"));

		TagKey<Item> PLATE_ARMOR = tag("armors/plate");
		TagKey<Item> STUDDED_ARMOR = tag("armors/studded");
		TagKey<Item> QUIVERS = tag("armors/quivers");
		TagKey<Item> QUIVER_APPLICABLE = tag("armors/quiver_applicable");
		TagKey<Item> QUIVER_INAPPLICABLE = tag("armors/quiver_inapplicable");
		TagKey<Item> QUIVER_USER = tag("tools/quiver_user");
		TagKey<Item> QUIVER_AMMO = tag("tools/quiver_ammo");
		TagKey<Item> QUIVER_AUTO_ADD = tag("tools/quiver_auto_add");

		TagKey<Item> TABLES = tag("tables");
		TagKey<Item> CHAIRS = tag("chairs");

		TagKey<Item> PIGMAN_GUARD_PAYMENTS = tag("pigman_guard_payments");
		TagKey<Item> RANA_CURRENCY = tag("rana_currency");

		TagKey<Item> TAILOR_ARMOR_STAND_HELMETS = tag("tailor_armor_stand/helmets");
		TagKey<Item> TAILOR_ARMOR_STAND_CHESTPLATES = tag("tailor_armor_stand/chestplates");
		TagKey<Item> TAILOR_ARMOR_STAND_LEGGINGS = tag("tailor_armor_stand/leggings");
		TagKey<Item> TAILOR_ARMOR_STAND_BOOTS = tag("tailor_armor_stand/boots");
		TagKey<Item> TAILOR_UNSELLABLE_FLOWERS = tag("tailor_unsellable_flowers");

		TagKey<Item> BOWYER_UNSELLABLE_ARROWS = tag("bowyer_unsellable_arrows");

		TagKey<Item> BRICK_PYRAMID_POTTERY_SHERDS = tag("brick_pyramid_pottery_sherds");

		TagKey<Item> DRAGON_ARMOR_MATERIALS = tag("dragon_armor_materials");
		
		TagKey<Item> QUIVER_REPAIR_MATERIALS = tag("quiver_repair_materials");
		TagKey<Item> STUDDED_ARMOR_REPAIR_MATERIALS = tag("studded_armor_repair_materials");
		TagKey<Item> PLATE_ARMOR_REPAIR_MATERIALS = tag("plate_armor_repair_materials");

		private static TagKey<Item> tag(String name)
		{
			return ItemTags.create(RediscoveredMod.locate(name));
		}
	}

	public static interface Entities
	{
		TagKey<EntityType<?>> PLATE_ARMOR_SPAWNS = tag("plate_armor_spawns");

		TagKey<EntityType<?>> MD3 = tag("md3");
		
		TagKey<EntityType<?>> PIGMAN = tag("pigman");
		TagKey<EntityType<?>> PIGMAN_ALLIES = tag("pigman_allies");
		TagKey<EntityType<?>> PIGMAN_VILLAGE_GUARDS = tag("pigman_village_guards");

		TagKey<EntityType<?>> GOLDEN_AURA_APPLICABLE = tag("golden_aura_applicable");
		TagKey<EntityType<?>> CRIMSON_VEIL_AFFECTED = tag("crimson_veil_affected");

		TagKey<EntityType<?>> BRICK_PYRAMID_SPAWNER = tag("brick_pyramid_spawner");

		TagKey<EntityType<?>> BECOMES_ZOMBIE_HORSE_IN_ZOMBIE_VILLAGE = tag("becomes_zombie_horse_in_zombie_village");
		
		TagKey<EntityType<?>> TARGETS_SCARECROW = tag("targets_scarecrow");
		TagKey<EntityType<?>> FEARS_SCARECROW = tag("fears_scarecrow");
		TagKey<EntityType<?>> IGNORES_SCARECROW = tag("ignores_scarecrow");

		private static TagKey<EntityType<?>> tag(String key)
		{
			return TagKey.create(Registries.ENTITY_TYPE, RediscoveredMod.locate(key));
		}
	}

	public static interface Biomes
	{
		TagKey<Biome> IS_SKYLANDS = tag("is_skylands");

		TagKey<Biome> HAS_PIGMAN_VILLAGE = tag("has_pigman_village");
		TagKey<Biome> HAS_BRICK_PYRAMID = tag("has_brick_pyramid");
		TagKey<Biome> HAS_OBSIDIAN_WALL = tag("has_obsidian_wall");
		TagKey<Biome> HAS_DEFAULT_INDEV_HOUSE = tag("has_default_indev_house");
		TagKey<Biome> HAS_CHERRY_INDEV_HOUSE = tag("has_cherry_indev_house");
		TagKey<Biome> HAS_MOSSY_INDEV_HOUSE = tag("has_mossy_indev_house");

		TagKey<Biome> HAS_RUBY_ORE = tag("has_ruby_ore");
		TagKey<Biome> HAS_FISH_SPAWNS = tag("has_fish_spawns");

		private static TagKey<Biome> tag(String key)
		{
			return TagKey.create(Registries.BIOME, RediscoveredMod.locate(key));
		}
	}

	public static interface Poi
	{
		TagKey<PoiType> APPLICABLE_PIGMAN_WORKSTATION = tag("applicable_pigman_workstation");

		private static TagKey<PoiType> tag(String name)
		{
			return TagKey.create(Registries.POINT_OF_INTEREST_TYPE, RediscoveredMod.locate(name));
		}
	}

	public static interface Structures
	{
		TagKey<Structure> INDEV_HOUSE = tag("indev_house");

		TagKey<Structure> PIGMAN_VILLAGE_AVOIDS = tag("pigman_village_avoids");
		TagKey<Structure> OBSIDIAN_WALL_AVOIDS = tag("obsidian_wall_avoids");

		private static TagKey<Structure> tag(String key)
		{
			return TagKey.create(Registries.STRUCTURE, RediscoveredMod.locate(key));
		}
	}

	public static interface DamageTypes
	{
		TagKey<DamageType> IS_DEATHLY = tag("is_dealthly");
		TagKey<DamageType> IS_SPIKES = tag("is_spikes");

		private static TagKey<DamageType> tag(String name)
		{
			return TagKey.create(Registries.DAMAGE_TYPE, RediscoveredMod.locate(name));
		}
	}
}
