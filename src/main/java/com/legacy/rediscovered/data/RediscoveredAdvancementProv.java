package com.legacy.rediscovered.data;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.item_predicates.AttachedQuiverItemPredicate;
import com.legacy.rediscovered.data.triggers.ActivateNetherReactorTrigger;
import com.legacy.rediscovered.data.triggers.CurePigmanTrigger;
import com.legacy.rediscovered.data.triggers.DragonCatchTrigger;
import com.legacy.rediscovered.data.triggers.DragonTeleportTrigger;
import com.legacy.rediscovered.data.triggers.GiveRanaItemTrigger;
import com.legacy.rediscovered.data.triggers.InFarlandsTrigger;
import com.legacy.rediscovered.data.triggers.LocatePortalTrigger;
import com.legacy.rediscovered.data.triggers.RemoveQuiverTrigger;
import com.legacy.rediscovered.item.RubyEyeItem;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.AdvancementType;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.BlockPredicate;
import net.minecraft.advancements.critereon.ChangeDimensionTrigger;
import net.minecraft.advancements.critereon.DamageSourcePredicate;
import net.minecraft.advancements.critereon.DistancePredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.ItemUsedOnLocationTrigger;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.advancements.critereon.StartRidingTrigger;
import net.minecraft.advancements.critereon.SummonedEntityTrigger;
import net.minecraft.advancements.critereon.TagPredicate;
import net.minecraft.advancements.critereon.TradeTrigger;
import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.neoforged.neoforge.common.data.AdvancementProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class RediscoveredAdvancementProv extends AdvancementProvider
{
	public RediscoveredAdvancementProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookup, ExistingFileHelper existingFileHelper)
	{
		super(packOutput, lookup, existingFileHelper, List.of(new Advancements()));
	}

	protected static class Advancements implements AdvancementGenerator
	{
		protected static AdvancementHolder getRuby, locatePortal, enterSkylands;
		protected static AdvancementHolder rideZombieHorse, shearRoseBush, allFlowers, craftScarecrow;
		protected static AdvancementHolder craftQuiver, attachQuiver, removeQuiver, purpleArrowSnipe, craftGears,
				spikySpkies;
		protected static AdvancementHolder craftNetherReactor, activateNetherReactor, activateFullNetherReactor,
				curePigman, curePigmanMelee, curePigmanRanged;
		protected static AdvancementHolder skylandsRoot, enterBrickPyramid, slayRedDragon, hatchRedDragon,
				redDragonCatch, redDragonTeleport, summonRedDragon, enterPigmanVillage, tradeWithPigman, giveRanaGift, reachFarlands;

		private String section = "";

		@Override
		public void generate(HolderLookup.Provider lookup, Consumer<AdvancementHolder> saver, ExistingFileHelper existingFileHelper)
		{
			AdvancementHolder ironPick = this.referenceOnly("story/iron_tools");
			AdvancementHolder husbandryRoot = this.referenceOnly("husbandry/root");
			AdvancementHolder tameAnimal = this.referenceOnly("husbandry/tame_an_animal");
			AdvancementHolder seedyPlace = this.referenceOnly("husbandry/plant_seed");
			AdvancementHolder adventureRoot = this.referenceOnly("adventure/root");
			AdvancementHolder sniperDuel = this.referenceOnly("adventure/sniper_duel");
			AdvancementHolder netherRoot = this.referenceOnly("nether/root");
			AdvancementHolder witheringHeights = this.referenceOnly("nether/summon_wither");

			this.setSection("story");
			getRuby = this.builder(RediscoveredItems.ruby, "find_ruby", AdvancementType.TASK).parent(ironPick).addCriterion("ruby", hasItems(RediscoveredItems.ruby)).save(saver, locate("find_ruby"));
			locatePortal = this.builder(RediscoveredBlocks.ruby_eye, "locate_skylands_portal", AdvancementType.TASK).parent(getRuby).addCriterion("find_portal", LocatePortalTrigger.TriggerInstance.any()).save(saver, locate("find_skylands_portal"));
			enterSkylands = this.builder(RediscoveredBlocks.glowing_obsidian, "enter_skylands", AdvancementType.TASK).parent(locatePortal).addCriterion("enter_skylands", ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(RediscoveredDimensions.skylandsKey())).save(saver, locate("enter_skylands"));

			this.setSection("husbandry");
			rideZombieHorse = this.builder(Items.SADDLE, "ride_zombie_horse", AdvancementType.GOAL).parent(tameAnimal).addCriterion("ride_zombie_horse", StartRidingTrigger.TriggerInstance.playerStartsRiding(EntityPredicate.Builder.entity().vehicle(EntityPredicate.Builder.entity().of(EntityType.ZOMBIE_HORSE)))).save(saver, locate("ride_zombie_horse"));
			shearRoseBush = this.builder(RediscoveredBlocks.rose, "shear_rose_bush", AdvancementType.TASK).parent(husbandryRoot).addCriterion("shear_rose_bush", ItemUsedOnLocationTrigger.TriggerInstance.itemUsedOnBlock(LocationPredicate.Builder.location().setBlock(BlockPredicate.Builder.block().of(Blocks.ROSE_BUSH)), ItemPredicate.Builder.item().of(Items.SHEARS))).save(saver, locate("shear_rose_bush"));
			allFlowers = this.builder(RediscoveredBlocks.cyan_rose, "has_all_flowers", AdvancementType.TASK).parent(shearRoseBush).addCriterion("rose", hasItems(RediscoveredBlocks.rose)).addCriterion("paeonia", hasItems(RediscoveredBlocks.paeonia)).addCriterion("cyan_rose", hasItems(RediscoveredBlocks.cyan_rose)).save(saver, locate("has_all_flowers"));
			craftScarecrow = this.builder(RediscoveredItems.scarecrow, "craft_scarecrow", AdvancementType.TASK).parent(seedyPlace).addCriterion("scarecrow", hasItems(RediscoveredItems.scarecrow)).save(saver, locate("craft_scarecrow"));

			this.setSection("adventure");
			craftQuiver = this.builder(RediscoveredItems.quiver, "craft_quiver", AdvancementType.TASK).parent(adventureRoot).addCriterion("quiver", hasItems(RediscoveredItems.quiver)).save(saver, locate("craft_quiver"));
			attachQuiver = this.builder(AttachedItem.attachItem(Items.IRON_CHESTPLATE.getDefaultInstance(), RediscoveredItems.quiver.getDefaultInstance()), "attach_quiver", AdvancementType.TASK).parent(craftQuiver).addCriterion("has_attached_quiver", InventoryChangeTrigger.TriggerInstance.hasItems(new ItemPredicate(AttachedQuiverItemPredicate.INSTANCE))).save(saver, locate("attach_quiver"));
			removeQuiver = this.builder(Items.GRINDSTONE, "remove_quiver", AdvancementType.TASK).parent(attachQuiver).addCriterion("remove_quiver", RemoveQuiverTrigger.TriggerInstance.any()).save(saver, locate("remove_quiver"));
			purpleArrowSnipe = this.builder(RediscoveredItems.purple_arrow, "purple_arrow_snipe", AdvancementType.CHALLENGE).parent(sniperDuel).rewards(AdvancementRewards.Builder.experience(50)).addCriterion("snipe", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().distance(DistancePredicate.horizontal(MinMaxBounds.Doubles.atLeast(100.0D))), DamageSourcePredicate.Builder.damageType().tag(TagPredicate.is(DamageTypeTags.IS_PROJECTILE)).direct(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.PURPLE_ARROW)))).save(saver, locate("purple_arrow_snipe"));
			craftGears = this.builder(RediscoveredBlocks.gear, "craft_gears", AdvancementType.TASK).parent(adventureRoot).addCriterion("gear", hasItems(RediscoveredBlocks.gear)).addCriterion("converter", hasItems(RediscoveredBlocks.rotational_converter)).save(saver, locate("craft_gears"));
			spikySpkies = this.builder(RediscoveredBlocks.spikes, "spiky_spikes", AdvancementType.TASK).parent(adventureRoot).addCriterion("spike_death", RediscoveredTriggers.KILL_WITH_SPIKES.get().createCriterion(new KilledTrigger.TriggerInstance(Optional.empty(), Optional.of(EntityPredicate.wrap(EntityPredicate.Builder.entity().of(EntityType.GUARDIAN).build())), Optional.empty()))).save(saver, locate("spiky_spikes"));

			this.setSection("nether");
			craftNetherReactor = this.builder(RediscoveredBlocks.nether_reactor_core, "craft_nether_reactor", AdvancementType.TASK).parent(witheringHeights).addCriterion("craft_reactor", hasItems(RediscoveredBlocks.nether_reactor_core)).save(saver, locate("craft_nether_reactor"));
			activateNetherReactor = this.builder(RediscoveredBlocks.nether_reactor_core, "activate_nether_reactor", AdvancementType.GOAL).parent(craftNetherReactor).addCriterion("activate_reactor", ActivateNetherReactorTrigger.TriggerInstance.any()).save(saver, locate("activate_nether_reactor"));
			activateFullNetherReactor = this.builder(Blocks.NETHERITE_BLOCK, "activate_full_nether_reactor", AdvancementType.CHALLENGE).parent(activateNetherReactor).rewards(AdvancementRewards.Builder.experience(50)).addCriterion("activate_reactor", ActivateNetherReactorTrigger.TriggerInstance.fullPower()).save(saver, locate("activate_full_nether_reactor"));
			curePigman = this.builder(Items.GOLDEN_CARROT, "cure_pigman", AdvancementType.TASK).parent(netherRoot).addCriterion("cure", CurePigmanTrigger.TriggerInstance.pigman(EntityPredicate.Builder.entity().of(RediscoveredTags.Entities.PIGMAN))).save(saver, locate("cure_pigman"));
			curePigmanMelee = this.builder(Items.IRON_SWORD, "cure_pigman_melee", AdvancementType.GOAL).parent(curePigman).addCriterion("cure", CurePigmanTrigger.TriggerInstance.pigman(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.MELEE_PIGMAN))).save(saver, locate("cure_pigman_melee"));
			curePigmanRanged = this.builder(Items.BOW, "cure_pigman_ranged", AdvancementType.GOAL).parent(curePigman).addCriterion("cure", CurePigmanTrigger.TriggerInstance.pigman(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.RANGED_PIGMAN))).save(saver, locate("cure_pigman_ranged"));

			this.setSection("skylands");
			skylandsRoot = this.rootBuilder(RubyEyeItem.setTextureIndex(new ItemStack(RediscoveredBlocks.ruby_eye), 0), "root", RediscoveredMod.locate("textures/gui/advancements/backgrounds/skylands.png")).addCriterion("enter_skylands", ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(RediscoveredDimensions.skylandsKey())).save(saver, locate("root"));

			enterPigmanVillage = this.builder(Items.CHERRY_PLANKS, "enter_pigman_village", AdvancementType.TASK).parent(skylandsRoot).addCriterion("in_structure", inStructure(RediscoveredStructures.PIGMAN_VILLAGE.getStructure().getKey())).save(saver, locate("enter_pigman_village"));
			tradeWithPigman = this.builder(RediscoveredItems.ruby, "trade_with_pigman", AdvancementType.TASK).parent(enterPigmanVillage).addCriterion("trade", CriteriaTriggers.TRADE.createCriterion(new TradeTrigger.TriggerInstance(Optional.empty(), Optional.of(EntityPredicate.wrap(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.PIGMAN).build())), Optional.empty()))).save(saver, locate("trade_with_pigman"));
			giveRanaGift = this.builder(RediscoveredBlocks.cyan_rose, "give_rana_gift", AdvancementType.TASK).parent(tradeWithPigman).addCriterion("give_gift", GiveRanaItemTrigger.TriggerInstance.any()).save(saver, locate("give_rana_gift"));

			enterBrickPyramid = this.builder(Items.BRICKS, "enter_brick_pyramid", AdvancementType.TASK).parent(skylandsRoot).addCriterion("in_structure", inStructure(RediscoveredStructures.BRICK_PYRAMID.getStructure().getKey())).save(saver, locate("enter_brick_pyramid"));
			slayRedDragon = this.builder(RediscoveredBlocks.red_dragon_egg, "slay_red_dragon", AdvancementType.TASK).parent(enterBrickPyramid).addCriterion("slay_dragon", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.RED_DRAGON_BOSS))).save(saver, locate("slay_red_dragon"));
			hatchRedDragon = this.builder(Items.DRAGON_BREATH, "hatch_red_dragon", AdvancementType.GOAL).parent(slayRedDragon).addCriterion("hatch_dragon", SummonedEntityTrigger.TriggerInstance.summonedEntity(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.RED_DRAGON_OFFSPRING))).save(saver, locate("hatch_red_dragon"));
			redDragonCatch = this.builder(RediscoveredItems.ruby_flute, "red_dragon_catch", AdvancementType.CHALLENGE).parent(hatchRedDragon).addCriterion("call_dragon", DragonCatchTrigger.TriggerInstance.any()).save(saver, locate("red_dragon_catch"));
			redDragonTeleport = this.builder(Items.GRASS_BLOCK, "red_dragon_teleport", AdvancementType.TASK).parent(hatchRedDragon).addCriterion("teleport", DragonTeleportTrigger.TriggerInstance.any()).save(saver, locate("red_dragon_teleport"));
			summonRedDragon = this.builder(RediscoveredBlocks.mini_dragon_pylon, "summon_red_dragon", AdvancementType.TASK).parent(slayRedDragon).addCriterion("summon_dragon", SummonedEntityTrigger.TriggerInstance.summonedEntity(EntityPredicate.Builder.entity().of(RediscoveredEntityTypes.RED_DRAGON_BOSS))).save(saver, locate("summon_red_dragon"));

			reachFarlands = this.builder(Items.LEATHER_BOOTS, "reach_farlands", AdvancementType.CHALLENGE).parent(skylandsRoot).rewards(AdvancementRewards.Builder.experience(173)).addCriterion("in_farlands", InFarlandsTrigger.TriggerInstance.skylands()).save(saver, locate("reach_farlands"));
		}

		private MutableComponent translate(String key)
		{
			return Component.translatable(RediscoveredMod.MODID + ":advancements" + (section.equals("") ? "" : "." + section) + "." + key);
		}

		// Used for translation stuff
		private void setSection(String name)
		{
			this.section = name;
		}

		private static Criterion<InventoryChangeTrigger.TriggerInstance> hasItems(ItemLike... items)
		{
			return InventoryChangeTrigger.TriggerInstance.hasItems(items);
		}

		private static Criterion<InventoryChangeTrigger.TriggerInstance> hasItems(ItemPredicate... items)
		{
			return InventoryChangeTrigger.TriggerInstance.hasItems(items);
		}

		private static Criterion<PlayerTrigger.TriggerInstance> inStructure(ResourceKey<Structure> structure)
		{
			return PlayerTrigger.TriggerInstance.located(LocationPredicate.Builder.inStructure(structure));
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		private static Advancement.Builder inAnyStructure(Advancement.Builder builder, StructureRegistrar<?> structure)
		{
			structure.getStructures().forEach((name, holder) ->
			{
				builder.addCriterion("in_" + (name.isEmpty() ? "structure" : name), inStructure((ResourceKey) holder.getKey()));
			});
			builder.requirements(AdvancementRequirements.Strategy.OR);
			return builder;
		}

		private String locate(String key)
		{
			return RediscoveredMod.locate((section.isBlank() ? "" : section + "/") + key).toString();
		}

		private Advancement.Builder rootBuilder(ItemLike displayItem, String name, ResourceLocation background)
		{
			return this.rootBuilder(new ItemStack(displayItem), name, background);
		}

		private Advancement.Builder rootBuilder(ItemStack displayItem, String name, ResourceLocation background)
		{
			return this.builder(displayItem, name, background, AdvancementType.TASK, false, false, false);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return this.builder(new ItemStack(displayItem), name, background, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, translate(name + ".title"), translate(name + ".description"), background, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return this.builder(displayItem, name, (ResourceLocation) null, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return this.builder(displayItem, name, (ResourceLocation) null, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, AdvancementType AdvancementType)
		{
			return this.builder(displayItem, name, AdvancementType, true, true, false);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, AdvancementType AdvancementType)
		{
			return this.builder(displayItem, name, AdvancementType, true, true, false);
		}

		/**
		 * Gets an advancement for reference use only. Use to parent other advancements
		 * off of this.
		 */
		private AdvancementHolder referenceOnly(String name)
		{
			return this.referenceOnly(new ResourceLocation(name));
		}

		/**
		 * Gets an advancement for reference use only. Use to parent other advancements
		 * off of this.
		 */
		private AdvancementHolder referenceOnly(ResourceLocation name)
		{
			return this.builder(Items.STONE, name.getPath(), AdvancementType.TASK).addCriterion("trigger", KilledTrigger.TriggerInstance.playerKilledEntity()).build(name);
		}
	}
}
