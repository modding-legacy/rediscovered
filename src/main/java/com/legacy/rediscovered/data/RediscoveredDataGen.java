package com.legacy.rediscovered.data;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.data.providers.NestedDataProvider;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.DetectedVersion;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod.EventBusSubscriber;
import net.neoforged.fml.common.Mod.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.data.event.GatherDataEvent;

@EventBusSubscriber(modid = RediscoveredMod.MODID, bus = Bus.MOD)
public class RediscoveredDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		PackOutput output = gen.getPackOutput();
		ExistingFileHelper fileHelper = event.getExistingFileHelper();
		boolean server = event.includeServer();
		boolean client = event.includeClient();

		DatapackBuiltinEntriesProvider registrarProv = new DatapackBuiltinEntriesProvider(output, event.getLookupProvider(), RegistrarHandler.injectRegistries(new RegistrySetBuilder()), Set.of(RediscoveredMod.MODID));
		gen.addProvider(server, registrarProv);
		CompletableFuture<HolderLookup.Provider> lookup = registrarProv.getRegistryProvider();

		BlockTagsProvider blockTagProv = new RediscoveredTagProv.BlockProv(gen, fileHelper, lookup);
		gen.addProvider(server, blockTagProv);
		gen.addProvider(server, new RediscoveredTagProv.ItemProv(gen, blockTagProv.contentsGetter(), fileHelper, lookup));
		gen.addProvider(server, new RediscoveredTagProv.EntityProv(gen, fileHelper, lookup));
		gen.addProvider(server, new RediscoveredTagProv.BiomeProv(gen, fileHelper, lookup));
		gen.addProvider(server, new RediscoveredTagProv.PoiProv(gen, fileHelper, lookup));
		gen.addProvider(server, new RediscoveredTagProv.StructureProv(gen, fileHelper, lookup));
		gen.addProvider(server, new RediscoveredTagProv.DamageTypeProv(gen, fileHelper, lookup));
		gen.addProvider(server, new RediscoveredTagProv.PaintingVariantProv(output, lookup, fileHelper));
		gen.addProvider(server, new RediscoveredAdvancementProv(output, lookup, fileHelper));

		gen.addProvider(server, new RediscoveredRecipeProv(output));
		gen.addProvider(server, new RediscoveredLootProv(output));
		gen.addProvider(server, new RediscoveredLootModifierProv(output));

		gen.addProvider(server, packMcmeta(output, "Rediscovered Mod's resources"));

		PackOutput legacyPackOutput = gen.getPackOutput("assets/rediscovered/legacy_pack");
		gen.addProvider(server, packMcmeta(legacyPackOutput, "Original look of the Rediscovered Mod (optional)"));

		gen.addProvider(client, new RediscoveredModelProv.States(output, fileHelper));
		gen.addProvider(client, new RediscoveredModelProv.ItemModels(output, fileHelper));

		gen.addProvider(client, new RediscoveredSoundProv(output, fileHelper));
		gen.addProvider(client, new RediscoveredLangProv(output, lookup));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}
}
