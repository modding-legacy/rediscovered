package com.legacy.rediscovered.data;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.item.crafting.SmithingDragonArmorRecipeBuilder;
import com.legacy.rediscovered.item.util.DragonArmorTrim;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.SmithingTrimRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.BlastingRecipe;
import net.minecraft.world.item.crafting.CampfireCookingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.item.crafting.SmokingRecipe;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.neoforged.neoforge.common.Tags;

public class RediscoveredRecipeProv extends VanillaRecipeProvider
{
	public static final ImmutableList<ItemLike> RUBY_SMELTABLES = ImmutableList.of(RediscoveredBlocks.ruby_ore, RediscoveredBlocks.deepslate_ruby_ore);
	private static final String HAS_ITEM = "has_item";

	public RediscoveredRecipeProv(PackOutput output)
	{
		super(output);
	}

	@Override
	protected CompletableFuture<?> buildAdvancement(CachedOutput output, AdvancementHolder advancement)
	{
		// Don't generate root recipe advancement
		if (RecipeBuilder.ROOT_RECIPE_ADVANCEMENT.equals(advancement.id()))
			return CompletableFuture.completedFuture(null);
		return super.buildAdvancement(output, advancement);
	}

	@Override
	protected void buildRecipes(RecipeOutput saver)
	{
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RediscoveredBlocks.nether_reactor_core).define('I', Tags.Items.INGOTS_IRON).define('D', Tags.Items.GEMS_DIAMOND).define('N', Items.NETHER_STAR).pattern("IDI").pattern("INI").pattern("IDI").unlockedBy("has_nether_star", has(Items.NETHER_STAR)).save(saver);

		nineBlockStorageRecipesRecipesWithCustomUnpacking(saver, RecipeCategory.MISC, RediscoveredItems.ruby, RecipeCategory.BUILDING_BLOCKS, RediscoveredBlocks.ruby_block, find("ruby_from_ruby_block"), find("ruby"));

		oneToOneConversionRecipe(saver, Items.RED_DYE, RediscoveredBlocks.rose, "red_dye", 1);
		oneToOneConversionRecipe(saver, Items.PINK_DYE, RediscoveredBlocks.paeonia, "pink_dye", 1);
		oneToOneConversionRecipe(saver, Items.CYAN_DYE, RediscoveredBlocks.cyan_rose, "cyan_dye", 1);

		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, RediscoveredBlocks.ancient_crying_obsidian).define('L', Items.LAPIS_LAZULI).define('O', Blocks.OBSIDIAN).pattern(" L ").pattern("LOL").pattern(" L ").unlockedBy("has_obsidian", has(Blocks.OBSIDIAN)).save(saver);
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, RediscoveredBlocks.spikes, 3).define('I', Tags.Items.INGOTS_IRON).define('W', ItemTags.WOODEN_SLABS).pattern("I I").pattern("WWW").unlockedBy("has_iron", has(Tags.Items.INGOTS_IRON)).save(saver);
		ShapedRecipeBuilder.shaped(RecipeCategory.REDSTONE, RediscoveredBlocks.obsidian_bulb).define('O', RediscoveredBlocks.glowing_obsidian).define('R', Tags.Items.DUSTS_REDSTONE).define('Q', Tags.Items.GEMS_QUARTZ).pattern(" R ").pattern("QOQ").pattern(" R ").unlockedBy("has_glowing_obsidian", has(RediscoveredBlocks.glowing_obsidian)).save(saver);

		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, RediscoveredItems.quiver).define('F', Items.FEATHER).define('L', Items.LEATHER).define('S', Items.STRING).pattern("FLL").pattern("SLL").pattern("SLL").unlockedBy("has_leather", has(Items.LEATHER)).group("quiver").save(saver);
		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, RediscoveredItems.purple_arrow, 8).define('A', Items.ARROW).define('M', Items.PHANTOM_MEMBRANE).pattern("AAA").pattern("AMA").pattern("AAA").unlockedBy("has_arrow", has(Items.ARROW)).unlockedBy("has_membrane", has(Items.PHANTOM_MEMBRANE)).save(saver);

		ShapedRecipeBuilder.shaped(RecipeCategory.REDSTONE, RediscoveredBlocks.rotational_converter).define('C', Tags.Items.COBBLESTONE).define('R', Blocks.COMPARATOR).define('L', Blocks.LEVER).define('G', RediscoveredBlocks.gear).pattern("CCC").pattern("RLG").pattern("CCC").unlockedBy("has_gear", has(RediscoveredBlocks.gear)).unlockedBy("has_comparator", has(Blocks.COMPARATOR)).save(saver);
		ShapedRecipeBuilder.shaped(RecipeCategory.REDSTONE, RediscoveredBlocks.gear, 4).define('I', Tags.Items.NUGGETS_IRON).define('Q', Tags.Items.GEMS_QUARTZ).pattern("III").pattern("IQI").pattern("III").unlockedBy("has_quartz", has(Tags.Items.GEMS_QUARTZ)).save(saver);

		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RediscoveredItems.scarecrow).define('I', Items.STICK).define('H', Blocks.HAY_BLOCK).define('#', Blocks.CARVED_PUMPKIN).pattern(" # ").pattern("IHI").pattern(" I ").unlockedBy("has_pumpkin", has(Blocks.CARVED_PUMPKIN)).unlockedBy("has_hay", has(Blocks.HAY_BLOCK)).save(saver);

		variants(saver, Blocks.GRASS_BLOCK, RediscoveredBlocks.grass_slab, null, null);
		variants(saver, Blocks.PODZOL, RediscoveredBlocks.podzol_slab, null, null);
		variants(saver, Blocks.MYCELIUM, RediscoveredBlocks.mycelium_slab, null, null);
		variants(saver, Blocks.DIRT_PATH, RediscoveredBlocks.dirt_path_slab, null, null);
		variants(saver, Blocks.DIRT, RediscoveredBlocks.dirt_slab, null, null); // You normally can't get dirt paths, so this recipe exists in case you can
		variants(saver, Blocks.COARSE_DIRT, RediscoveredBlocks.coarse_dirt_slab, null, null);
		variants(saver, Blocks.ROOTED_DIRT, RediscoveredBlocks.rooted_dirt_slab, null, null);
		ShapedRecipeBuilder.shaped(RecipeCategory.BUILDING_BLOCKS, Blocks.PETRIFIED_OAK_SLAB, 6).define('P', Blocks.OAK_PLANKS).define('C', Tags.Items.COBBLESTONE).pattern("PCP").unlockedBy("has_cobble", has(Tags.Items.COBBLESTONE)).unlockedBy("has_planks", has(Blocks.OAK_PLANKS)).save(saver, find("petrified_oak_slab"));

		studdedArmor(RediscoveredItems.studded_helmet, Items.LEATHER_HELMET, Items.CHAINMAIL_HELMET, saver);
		studdedArmor(RediscoveredItems.studded_chestplate, Items.LEATHER_CHESTPLATE, Items.CHAINMAIL_CHESTPLATE, saver);
		studdedArmor(RediscoveredItems.studded_leggings, Items.LEATHER_LEGGINGS, Items.CHAINMAIL_LEGGINGS, saver);
		studdedArmor(RediscoveredItems.studded_boots, Items.LEATHER_BOOTS, Items.CHAINMAIL_BOOTS, saver);

		SimpleCookingRecipeBuilder.smelting(Ingredient.of(RediscoveredItems.raw_fish), RecipeCategory.FOOD, RediscoveredItems.cooked_fish, 0.35F, 200).unlockedBy("has_fish", has(RediscoveredItems.raw_fish)).save(saver);
		simpleCookingRecipe(saver, "smoking", RecipeSerializer.SMOKING_RECIPE, SmokingRecipe::new, 100, RediscoveredItems.raw_fish, RediscoveredItems.cooked_fish, 0.35F);
		simpleCookingRecipe(saver, "campfire_cooking", RecipeSerializer.CAMPFIRE_COOKING_RECIPE, CampfireCookingRecipe::new, 600, RediscoveredItems.raw_fish, RediscoveredItems.cooked_fish, 0.35F);

		oreSmelting(saver, RUBY_SMELTABLES, RecipeCategory.MISC, RediscoveredItems.ruby, 1.0F, 200, "ruby");
		oreBlasting(saver, RUBY_SMELTABLES, RecipeCategory.MISC, RediscoveredItems.ruby, 1.0F, 100, "ruby");

		carpet(saver, RediscoveredBlocks.bright_green_carpet, RediscoveredBlocks.bright_green_wool);
		carpet(saver, RediscoveredBlocks.lavender_carpet, RediscoveredBlocks.lavender_wool);
		carpet(saver, RediscoveredBlocks.rose_carpet, RediscoveredBlocks.rose_wool);
		carpet(saver, RediscoveredBlocks.sky_blue_carpet, RediscoveredBlocks.sky_blue_wool);
		carpet(saver, RediscoveredBlocks.slate_blue_carpet, RediscoveredBlocks.slate_blue_wool);
		carpet(saver, RediscoveredBlocks.spring_green_carpet, RediscoveredBlocks.spring_green_wool);

		furniture(saver, RediscoveredBlocks.oak_chair, RediscoveredBlocks.oak_table, Blocks.OAK_PLANKS);
		furniture(saver, RediscoveredBlocks.spruce_chair, RediscoveredBlocks.spruce_table, Blocks.SPRUCE_PLANKS);
		furniture(saver, RediscoveredBlocks.birch_chair, RediscoveredBlocks.birch_table, Blocks.BIRCH_PLANKS);
		furniture(saver, RediscoveredBlocks.jungle_chair, RediscoveredBlocks.jungle_table, Blocks.JUNGLE_PLANKS);
		furniture(saver, RediscoveredBlocks.acacia_chair, RediscoveredBlocks.acacia_table, Blocks.ACACIA_PLANKS);
		furniture(saver, RediscoveredBlocks.dark_oak_chair, RediscoveredBlocks.dark_oak_table, Blocks.DARK_OAK_PLANKS);
		furniture(saver, RediscoveredBlocks.mangrove_chair, RediscoveredBlocks.mangrove_table, Blocks.MANGROVE_PLANKS);
		furniture(saver, RediscoveredBlocks.bamboo_chair, RediscoveredBlocks.bamboo_table, Blocks.BAMBOO_PLANKS);
		furniture(saver, RediscoveredBlocks.cherry_chair, RediscoveredBlocks.cherry_table, Blocks.CHERRY_PLANKS);
		furniture(saver, RediscoveredBlocks.warped_chair, RediscoveredBlocks.warped_table, Blocks.WARPED_PLANKS);
		furniture(saver, RediscoveredBlocks.crimson_chair, RediscoveredBlocks.crimson_table, Blocks.CRIMSON_PLANKS);

		List<ItemLike> largeBrickVariants = List.of(RediscoveredBlocks.large_bricks, RediscoveredBlocks.large_brick_slab, RediscoveredBlocks.large_brick_stairs, RediscoveredBlocks.large_brick_wall);
		stoneCutting(saver, RecipeCategory.BUILDING_BLOCKS, Items.BRICKS, largeBrickVariants);
		stoneCutting(saver, RecipeCategory.BUILDING_BLOCKS, RediscoveredBlocks.large_bricks, largeBrickVariants);
		variants(saver, RediscoveredBlocks.large_bricks, RediscoveredBlocks.large_brick_slab, RediscoveredBlocks.large_brick_stairs, RediscoveredBlocks.large_brick_wall);

		armorMelting(saver, RediscoveredTags.Items.PLATE_ARMOR, Items.IRON_NUGGET);
		armorMelting(saver, RediscoveredItems.dragon_armor, Items.IRON_NUGGET);
		
		simpleCookingRecipe(saver, "blasting", RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new, 100, Blocks.PACKED_MUD, RediscoveredBlocks.brittle_packed_mud, 0.01F);
		simpleCookingRecipe(saver, "blasting", RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new, 100, Blocks.MUD_BRICKS, RediscoveredBlocks.brittle_mud_bricks, 0.01F);

		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, RediscoveredBlocks.ruby_eye).requires(RediscoveredTags.Items.GEMS_RUBY).requires(Items.ENDER_PEARL).unlockedBy("has_ruby", has(RediscoveredTags.Items.GEMS_RUBY)).unlockedBy("has_ender_pearl", has(Items.ENDER_PEARL)).save(saver);

		smithingTrim(RediscoveredItems.draconic_trim, RediscoveredBlocks.large_bricks, saver);

		ShapedRecipeBuilder.shaped(RecipeCategory.TOOLS, RediscoveredItems.ruby_flute).define('R', RediscoveredTags.Items.GEMS_RUBY).define('B', Items.BAMBOO).pattern("  R").pattern(" B ").pattern("B  ").unlockedBy("has_ruby", has(RediscoveredTags.Items.GEMS_RUBY)).save(saver);
		ShapedRecipeBuilder.shaped(RecipeCategory.MISC, RediscoveredBlocks.mini_dragon_pylon).define('B', Items.BLAZE_ROD).define('G', Tags.Items.GLASS_COLORLESS).define('R', RediscoveredTags.Items.GEMS_RUBY).pattern("BGB").pattern("GRG").pattern("BGB").unlockedBy("has_ruby", has(RediscoveredTags.Items.GEMS_RUBY)).save(saver);

		dragonArmorSmithingTemplate(RediscoveredItems.dragon_armor_chain_smithing_template, Items.OBSIDIAN).save(saver);
		dragonArmorSmithingTemplate(RediscoveredItems.dragon_armor_plating_smithing_template, RediscoveredBlocks.glowing_obsidian).save(saver);
		dragonArmorSmithingTemplate(RediscoveredItems.dragon_armor_inlay_smithing_template, RediscoveredBlocks.ancient_crying_obsidian).save(saver);

		dragonSmithingTrim(RediscoveredItems.dragon_armor_chain_smithing_template, DragonArmorTrim.Decoration.CHAIN, saver);
		dragonSmithingTrim(RediscoveredItems.dragon_armor_plating_smithing_template, DragonArmorTrim.Decoration.PLATING, saver);
		dragonSmithingTrim(RediscoveredItems.dragon_armor_inlay_smithing_template, DragonArmorTrim.Decoration.INLAY, saver);
	}

	private void studdedArmor(ItemLike result, ItemLike leather, ItemLike chain, RecipeOutput saver)
	{
		ResourceLocation id = RecipeBuilder.getDefaultRecipeId(result);
		String group = id.getPath();
		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, result).group(group).define('L', leather).define('I', Tags.Items.INGOTS_IRON).define('N', Tags.Items.NUGGETS_IRON).pattern("NNN").pattern("ILI").pattern("NNN").unlockedBy(HAS_ITEM, has(leather)).save(saver, id.withSuffix("_from_iron"));
		ShapelessRecipeBuilder.shapeless(RecipeCategory.COMBAT, result).group(group).requires(leather).requires(chain).unlockedBy("has_leather", has(leather)).unlockedBy("has_chain", has(chain)).save(saver, id.withSuffix("_from_chainmail"));

	}

	private ShapedRecipeBuilder dragonArmorSmithingTemplate(ItemLike result, ItemLike centerItem)
	{
		return ShapedRecipeBuilder.shaped(RecipeCategory.MISC, result).define('R', RediscoveredTags.Items.GEMS_RUBY).define('D', Items.DRAGON_BREATH).define('O', centerItem).pattern("RDR").pattern("ROR").pattern("RRR").unlockedBy("has_center", has(centerItem)).unlockedBy("has_breath", has(Items.DRAGON_BREATH));
	}

	private void smithingTrim(ItemLike templateItem, @Nullable ItemLike copyItem, RecipeOutput consumer)
	{
		SmithingTrimRecipeBuilder.smithingTrim(Ingredient.of(templateItem), Ingredient.of(ItemTags.TRIMMABLE_ARMOR), Ingredient.of(ItemTags.TRIM_MATERIALS), RecipeCategory.MISC).unlocks("has_smithing_trim_template", has(templateItem)).save(consumer, RediscoveredMod.locate(getItemName(templateItem) + "_smithing_trim"));
		if (copyItem != null)
			copySmithingTemplate(consumer, templateItem, copyItem);
	}

	private void dragonSmithingTrim(ItemLike templateItem, DragonArmorTrim.Decoration decoration, RecipeOutput consumer)
	{
		SmithingDragonArmorRecipeBuilder.smithingTrim(Ingredient.of(templateItem), decoration, Ingredient.of(RediscoveredItems.dragon_armor), Ingredient.of(RediscoveredTags.Items.DRAGON_ARMOR_MATERIALS), RecipeCategory.MISC).unlocks("has_smithing_trim_template", has(templateItem)).save(consumer, RediscoveredMod.locate(getItemName(templateItem) + "_armor_trim"));
		;
	}

	protected static void stoneCutting(RecipeOutput cons, RecipeCategory category, ItemLike ingredient, List<ItemLike> results)
	{
		results.forEach(result ->
		{
			SingleItemRecipeBuilder.stonecutting(Ingredient.of(ingredient), category, result, result instanceof SlabBlock ? 2 : 1).unlockedBy(HAS_ITEM, has(ingredient)).save(cons, RediscoveredMod.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_stonecutting_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
		});
	}

	protected static void variants(RecipeOutput cons, ItemLike ingredient, @Nullable ItemLike slab, @Nullable ItemLike stairs, @Nullable ItemLike wall)
	{
		if (stairs != null)
			stairBuilder(stairs, Ingredient.of(ingredient)).unlockedBy(HAS_ITEM, has(ingredient)).save(cons);
		if (slab != null)
			slabBuilder(RecipeCategory.BUILDING_BLOCKS, slab, Ingredient.of(ingredient)).unlockedBy(HAS_ITEM, has(ingredient)).save(cons);
		if (wall != null)
			wallBuilder(RecipeCategory.BUILDING_BLOCKS, wall, Ingredient.of(ingredient)).unlockedBy(HAS_ITEM, has(ingredient)).save(cons);
		;
	}

	protected static void furniture(RecipeOutput consumer, ItemLike chair, ItemLike table, ItemLike material)
	{
		ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, chair, 4).define('#', material).pattern("#  ").pattern("###").pattern("# #").group("chair").unlockedBy(getHasName(material), has(material)).save(consumer);
		ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, table, 4).define('#', material).pattern("###").pattern("# #").group("table").unlockedBy(getHasName(material), has(material)).save(consumer);
	}

	/*VANILLA COPY START (only change being adding find() to make the files stay in the rediscovered namespace*/
	protected static void oneToOneConversionRecipe(RecipeOutput pFinishedRecipeConsumer, ItemLike pResult, ItemLike pIngredient, @Nullable String pGroup, int pResultCount)
	{
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, pResult, pResultCount).requires(pIngredient).group(pGroup).unlockedBy(getHasName(pIngredient), has(pIngredient)).save(pFinishedRecipeConsumer, find(getConversionRecipeName(pResult, pIngredient)));
	}
	
	protected static void armorMelting(RecipeOutput consumer, TagKey<Item> armor, ItemLike result)
	{
		String resultName = getItemName(result);
		String inputName = armor.location().getPath().replace("/", "_");
		SimpleCookingRecipeBuilder.generic(Ingredient.of(armor), RecipeCategory.MISC, result, 0.1F, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new).unlockedBy("has_armor", has(armor)).save(consumer, find(resultName + "_from_smelting_" + inputName));
		SimpleCookingRecipeBuilder.generic(Ingredient.of(armor), RecipeCategory.MISC, result, 0.1F, 100, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new).unlockedBy("has_armor", has(armor)).save(consumer, find(resultName + "_from_blasting_" + inputName));
	}
	
	protected static void armorMelting(RecipeOutput consumer, ItemLike armor, ItemLike result)
	{
		String resultName = getItemName(result);
		String inputName = armor.asItem().builtInRegistryHolder().key().location().getPath().replace("/", "_");
		SimpleCookingRecipeBuilder.generic(Ingredient.of(armor), RecipeCategory.MISC, result, 0.1F, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new).unlockedBy("has_armor", has(armor)).save(consumer, find(resultName + "_from_smelting_" + inputName));
		SimpleCookingRecipeBuilder.generic(Ingredient.of(armor), RecipeCategory.MISC, result, 0.1F, 100, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new).unlockedBy("has_armor", has(armor)).save(consumer, find(resultName + "_from_blasting_" + inputName));
	}

	protected static <T extends AbstractCookingRecipe> void simpleCookingRecipe(RecipeOutput pFinishedRecipeConsumer, String pCookingMethod, RecipeSerializer<T> pCookingSerializer, AbstractCookingRecipe.Factory<T> pFactory, int pCookingTime, ItemLike pIngredient, ItemLike pResult, float pExperience)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(pIngredient), RecipeCategory.FOOD, pResult, pExperience, pCookingTime, pCookingSerializer, pFactory).unlockedBy(getHasName(pIngredient), has(pIngredient)).save(pFinishedRecipeConsumer, find(getItemName(pResult) + "_from_" + pCookingMethod));
	}

	protected static void oreSmelting(RecipeOutput pFinishedRecipeConsumer, List<ItemLike> pIngredients, RecipeCategory pCategory, ItemLike pResult, float pExperience, int pCookingTIme, String pGroup)
	{
		modOreCooking(pFinishedRecipeConsumer, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new, pIngredients, pCategory, pResult, pExperience, pCookingTIme, pGroup, "_from_smelting");
	}

	protected static void oreBlasting(RecipeOutput pFinishedRecipeConsumer, List<ItemLike> pIngredients, RecipeCategory pCategory, ItemLike pResult, float pExperience, int pCookingTime, String pGroup)
	{
		modOreCooking(pFinishedRecipeConsumer, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new, pIngredients, pCategory, pResult, pExperience, pCookingTime, pGroup, "_from_blasting");
	}

	protected static <T extends AbstractCookingRecipe> void modOreCooking(RecipeOutput pFinishedRecipeConsumer, RecipeSerializer<T> pCookingSerializer, AbstractCookingRecipe.Factory<T> pFactory, List<ItemLike> pIngredients, RecipeCategory pCategory, ItemLike pResult, float pExperience, int pCookingTime, String pGroup, String pRecipeName)
	{
		for (ItemLike itemlike : pIngredients)
			SimpleCookingRecipeBuilder.generic(Ingredient.of(itemlike), pCategory, pResult, pExperience, pCookingTime, pCookingSerializer, pFactory).group(pGroup).unlockedBy(getHasName(itemlike), has(itemlike)).save(pFinishedRecipeConsumer, find(getItemName(pResult) + pRecipeName + "_" + getItemName(itemlike)));
	}

	protected static void nineBlockStorageRecipesRecipesWithCustomUnpacking(RecipeOutput pFinishedRecipeConsumer, RecipeCategory pUnpackedCategory, ItemLike pUnpacked, RecipeCategory pPackedCategory, ItemLike pPacked, String pUnpackedName, String pUnpackedGroup)
	{
		nineBlockStorageRecipes(pFinishedRecipeConsumer, pUnpackedCategory, pUnpacked, pPackedCategory, pPacked, getSimpleRecipeName(pPacked), (String) null, pUnpackedName, pUnpackedGroup);
	}

	protected static void nineBlockStorageRecipes(RecipeOutput pFinishedRecipeConsumer, RecipeCategory pUnpackedCategory, ItemLike pUnpacked, RecipeCategory pPackedCategory, ItemLike pPacked, String pPackedName, @Nullable String pPackedGroup, String pUnpackedName, @Nullable String pUnpackedGroup)
	{
		ShapelessRecipeBuilder.shapeless(pUnpackedCategory, pUnpacked, 9).requires(pPacked).group(pUnpackedGroup).unlockedBy(getHasName(pPacked), has(pPacked)).save(pFinishedRecipeConsumer, pUnpackedName);
		ShapedRecipeBuilder.shaped(pPackedCategory, pPacked).define('#', pUnpacked).pattern("###").pattern("###").pattern("###").group(pPackedGroup).unlockedBy(getHasName(pUnpacked), has(pUnpacked)).save(pFinishedRecipeConsumer, RediscoveredMod.locate(pPackedName));
	}
	// VANILLA COPY END

	private static String find(String key)
	{
		return RediscoveredMod.find(key);
	}
}
