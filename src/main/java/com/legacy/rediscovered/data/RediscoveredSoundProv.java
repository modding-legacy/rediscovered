package com.legacy.rediscovered.data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.common.data.SoundDefinition;
import net.neoforged.neoforge.common.data.SoundDefinition.Sound;
import net.neoforged.neoforge.common.data.SoundDefinition.SoundType;
import net.neoforged.neoforge.common.data.SoundDefinitionsProvider;

public class RediscoveredSoundProv extends SoundDefinitionsProvider
{
	public RediscoveredSoundProv(PackOutput output, ExistingFileHelper helper)
	{
		super(output, RediscoveredMod.MODID, helper);
	}

	@Override
	public void registerSounds()
	{
		String block = "nether_reactor";
		this.add(RediscoveredSounds.BLOCK_NETHER_REACTOR_ACTIVATE, blockDef(block, "activate", 1));
		this.add(RediscoveredSounds.BLOCK_NETHER_REACTOR_IDLE, blockDef(block, "idle", 1, 5));
		this.add(RediscoveredSounds.BLOCK_NETHER_REACTOR_DEACTIVATE, blockDef(block, "deactivate", 1));
		block = "red_dragon_egg";
		this.add(RediscoveredSounds.BLOCK_RED_DRAGON_EGG_FERTILIZE, blockDef(block, "fertilize", 1));
		this.add(RediscoveredSounds.BLOCK_RED_DRAGON_EGG_SHAKE, definition().with(event(SoundEvents.STONE_STEP)).subtitle(blockSub(block + ".shake")));
		this.add(RediscoveredSounds.BLOCK_RED_DRAGON_EGG_HATCH, blockDef(block, "hatch", 1));

		this.add(RediscoveredSounds.BLOCK_ROTATIONAL_CONVERTER_CLICK, definition().with(event(SoundEvents.LEVER_CLICK)).subtitle(blockSub("rotational_converter.click")));

		this.add(RediscoveredSounds.BLOCK_TABLE_ADD_ITEM, definition().with(event(SoundEvents.ITEM_FRAME_ADD_ITEM)).subtitle(blockSub("table.add_item")));
		this.add(RediscoveredSounds.BLOCK_TABLE_REMOVE_ITEM, definition().with(event(SoundEvents.ITEM_FRAME_REMOVE_ITEM)).subtitle(blockSub("table.remove_item")));

		this.add(RediscoveredSounds.ITEM_ARMOR_EQUIP_STUDDED, definition().with(event(SoundEvents.ARMOR_EQUIP_CHAIN)).subtitle(itemSub("armor.equip_studded")));
		this.add(RediscoveredSounds.ITEM_ARMOR_EQUIP_PLATE, definition().with(event(SoundEvents.ARMOR_EQUIP_NETHERITE)).subtitle(itemSub("armor.equip_plate")));

		String entity = "pigman";
		this.add(RediscoveredSounds.ENTITY_PIGMAN_IDLE, definition().with(event(SoundEvents.PIG_AMBIENT)).subtitle(entitySub(entity + ".idle")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_HURT, entityDef(entity, "hurt", 2));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_DEATH, definition().with(event(SoundEvents.PIG_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_SCARED, definition().with(event(RediscoveredSounds.ENTITY_PIGMAN_HURT)).subtitle(entitySub(entity + ".scared")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_ZOMBIFY, entityDef(entity, "zombify", 2));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_AGREE, entityDef(entity, "agree", 1));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_DISAGREE, entityDef(entity, "disagree", 2));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_METALWORKER, definition().with(event(SoundEvents.VILLAGER_WORK_TOOLSMITH)).subtitle(entitySub(entity + ".work_metalworker")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_BOWYER, definition().with(event(SoundEvents.VILLAGER_WORK_FLETCHER)).subtitle(entitySub(entity + ".work_bowyer")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_TECHNICIAN, definition().with(event(SoundEvents.AXE_SCRAPE)).subtitle(entitySub(entity + ".work_technician")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_TAILOR, definition().with(event(SoundEvents.VILLAGER_WORK_SHEPHERD)).subtitle(entitySub(entity + ".work_tailor")));
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_DOCTOR, definition().with(event(SoundEvents.VILLAGER_WORK_CLERIC)).subtitle(entitySub(entity + ".work_doctor")));

		entity = "zombie_pigman";
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_IDLE, definition().with(event(SoundEvents.ZOMBIFIED_PIGLIN_AMBIENT)).subtitle(entitySub(entity + ".idle")));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_HURT, definition().with(event(SoundEvents.ZOMBIFIED_PIGLIN_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_DEATH, definition().with(event(SoundEvents.ZOMBIFIED_PIGLIN_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_ANGRY, definition().with(event(SoundEvents.ZOMBIFIED_PIGLIN_ANGRY)).subtitle(entitySub(entity + ".angry")));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_CURE, definition().with(event(SoundEvents.ZOMBIE_VILLAGER_CURE)).subtitle(entitySub(entity + ".cure")));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_FINISH_CURE, definition().with(event(SoundEvents.ZOMBIE_VILLAGER_CONVERTED)).subtitle(entitySub(entity + ".finish_cure")));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_SELECT_MELEE, entityDef(entity, "select_melee", 1));
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_SELECT_RANGED, entityDef(entity, "select_ranged", 1));

		entity = "steve";
		this.add(RediscoveredSounds.ENTITY_STEVE_HURT, entityDef(entity, "hurt", 1));
		this.add(RediscoveredSounds.ENTITY_STEVE_DEATH, definition().with(event(RediscoveredSounds.ENTITY_STEVE_HURT)).subtitle(entitySub(entity + ".death")));

		entity = "black_steve";
		this.add(RediscoveredSounds.ENTITY_BLACK_STEVE_HURT, definition().with(event(RediscoveredSounds.ENTITY_STEVE_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(RediscoveredSounds.ENTITY_BLACK_STEVE_DEATH, definition().with(event(RediscoveredSounds.ENTITY_STEVE_HURT)).subtitle(entitySub(entity + ".death")));

		entity = "beast_boy";
		this.add(RediscoveredSounds.ENTITY_BEAST_BOY_HURT, definition().with(event(RediscoveredSounds.ENTITY_STEVE_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(RediscoveredSounds.ENTITY_BEAST_BOY_DEATH, definition().with(event(RediscoveredSounds.ENTITY_STEVE_HURT)).subtitle(entitySub(entity + ".death")));

		entity = "rana";
		this.add(RediscoveredSounds.ENTITY_RANA_HURT, definition().with(event(SoundEvents.GENERIC_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(RediscoveredSounds.ENTITY_RANA_DEATH, definition().with(event(SoundEvents.GENERIC_HURT)).subtitle(entitySub(entity + ".death")));

		entity = "fish";
		this.add(RediscoveredSounds.ENTITY_FISH_HURT, definition().with(event(SoundEvents.COD_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(RediscoveredSounds.ENTITY_FISH_DEATH, definition().with(event(SoundEvents.COD_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(RediscoveredSounds.ENTITY_FISH_FLOP, definition().with(event(SoundEvents.COD_FLOP)).subtitle(entitySub(entity + ".flop")));

		entity = "pylon_burst";
		this.add(RediscoveredSounds.ENTITY_PYLON_BURST_DESTROYED, definition().with(event(SoundEvents.SHULKER_BULLET_HURT)).subtitle(entitySub(entity + ".destroyed")));
		this.add(RediscoveredSounds.ENTITY_PYLON_BURST_EXPLODE, definition().with(event(SoundEvents.SHULKER_BULLET_HIT)).subtitle(entitySub(entity + ".explode")));

		entity = "dragon_pylon";
		this.add(RediscoveredSounds.ENTITY_DRAGON_PYLON_SHIELD_LOST, definition().with(event(SoundEvents.RESPAWN_ANCHOR_DEPLETE.value())).subtitle(entitySub(entity + ".shield_lost")));
		this.add(RediscoveredSounds.ENTITY_DRAGON_PYLON_HURT, entityDef(entity, "hurt", 3));
		this.add(RediscoveredSounds.ENTITY_DRAGON_PYLON_DEATH, definition().with(event(SoundEvents.GENERIC_EXPLODE)).subtitle(entitySub(entity + ".death")));

		entity = "red_dragon";
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_IDLE, definition().with(event(SoundEvents.ENDER_DRAGON_AMBIENT)).subtitle(entitySub(entity + ".idle")));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_IDLE_CALM, entityDef(entity, "idle_calm", 2));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_HURT, definition().with(event(SoundEvents.ENDER_DRAGON_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_DEATH, definition().with(event(SoundEvents.ENDER_DRAGON_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_FLAP, definition().with(event(SoundEvents.ENDER_DRAGON_FLAP)).subtitle(entitySub(entity + ".flap")));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_STEP, entityDef(entity, "step", 4).subtitle("subtitles.block.generic.footsteps"));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_SHIELD_DOWN, entityDef(entity, "shield_down", 1));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_BOLT_BALL_CHARGE, entityDef(entity, "bolt_ball_charge", 1));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_BOLT_BALL_SHOOT, entityDef(entity, "bolt_ball_shoot", 2));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_PREPARE_WIND_BLOW, entityDef(entity, "prepare_wind_blow", 1));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_WIND_BLOW, entityDef(entity, "wind", 4));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_SHED_BURSTS, definition().with(event(SoundEvents.SHULKER_SHOOT)).subtitle(entitySub(entity + ".shed_bursts")));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_EQUIP_SADDLE, definition().with(event(SoundEvents.HORSE_SADDLE)).subtitle(entitySub(entity + ".equip_saddle")));
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_EQUIP_ARMOR, definition().with(event(SoundEvents.HORSE_ARMOR)).subtitle(entitySub(entity + ".equip_armor")));

		String record = "records/";
		this.add(RediscoveredSounds.RECORDS_MAGNETIC_CIRCUIT, definition().with(make(record.concat("calm4")).stream()));
	}

	protected static SoundDefinition def(Dir dir, String name, String subtitle)
	{
		return def(dir, name, 1, subtitle);
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle)
	{
		return def(dir, name, count, subtitle, s ->
		{
		});
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle, Consumer<Sound> extra)
	{
		var def = count > 1 ? definition().with(make(dir.folderName.concat(name), count, extra)) : definition().with(make(dir.folderName.concat(name), extra));
		return def.subtitle(dir == Dir.ENTITY ? entitySub(subtitle) : dir == Dir.BLOCKS ? blockSub(subtitle) : dir == Dir.ITEMS ? itemSub(subtitle) : subtitle);
	}

	protected static SoundDefinition.Sound make(final String name)
	{
		return make(name, s ->
		{
		});
	}

	protected static SoundDefinition.Sound make(final String name, Consumer<Sound> extra)
	{
		var sound = sound(RediscoveredMod.locate(name));
		extra.accept(sound);
		return sound;
	}

	protected static Sound[] make(String name, int count)
	{
		return make(name, count, s ->
		{
		});
	}

	protected static Sound[] make(String name, int count, Consumer<Sound> extra)
	{
		List<Sound> sounds = new ArrayList<>();

		for (int i = 1; i <= count; ++i)
		{
			Sound sound = make(name.concat("_" + i));
			extra.accept(sound);
			sounds.add(sound);
		}
		return sounds.toArray(new Sound[count]);
	}

	protected static SoundDefinition.Sound event(final SoundEvent event)
	{
		return sound(BuiltInRegistries.SOUND_EVENT.getKey(event), SoundType.EVENT);
	}

	protected static String sub(final String name)
	{
		return "subtitles.".concat(RediscoveredMod.MODID + "." + name);
	}

	protected static String blockSub(final String name)
	{
		return sub("block.".concat(name));
	}

	protected static String itemSub(final String name)
	{
		return sub("item.".concat(name));
	}

	protected static String entitySub(final String name)
	{
		return sub("entity.".concat(name));
	}

	protected static SoundDefinition musicDef(final String name)
	{
		return musicDef(name, false);
	}

	protected static SoundDefinition musicDef(final String name, boolean preload)
	{
		String music = "music/";
		return definition().with(make(music.concat(name)).stream().preload(preload));
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count, String subtitle)
	{
		return def(Dir.ENTITY, entityName + "/" + soundName, count, entityName + "." + subtitle);
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count)
	{
		return entityDef(entityName, soundName, count, soundName);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count)
	{
		return blockDef(blockName, soundName, count, 0);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count, final int attenuationDistance)
	{
		String defName = (blockName + "/" + soundName);
		String defSubtitle = blockName + "." + soundName;

		return def(Dir.BLOCKS, defName, count, defSubtitle, s ->
		{
			if (attenuationDistance > 0)
				s.attenuationDistance(attenuationDistance);
		});
	}

	protected static SoundDefinition itemDef(String itemName, String soundName, int count, String subtitle)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName + "." + subtitle);
	}

	protected static SoundDefinition itemDef(String itemName, String soundName, int count)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName);
	}

	private enum Dir
	{
		BLOCKS("block/"), ITEMS("item/"), ENTITY("entity/"), MUSIC("music/");

		public final String folderName;

		Dir(String folderName)
		{
			this.folderName = folderName;
		}
	}
}
