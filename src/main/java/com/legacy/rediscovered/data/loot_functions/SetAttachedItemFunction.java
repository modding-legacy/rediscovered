package com.legacy.rediscovered.data.loot_functions;

import java.util.List;

import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.registry.RediscoveredLootFunctions;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;

public class SetAttachedItemFunction extends LootItemConditionalFunction
{
	public static final Codec<SetAttachedItemFunction> CODEC = RecordCodecBuilder.create(instance ->
	{
		return commonFields(instance).and(ItemStack.CODEC.fieldOf("attached_item").forGetter(o -> o.attachedItem)).apply(instance, SetAttachedItemFunction::new);
	});
	private final ItemStack attachedItem;

	protected SetAttachedItemFunction(List<LootItemCondition> conditions, ItemStack attachedItem)
	{
		super(conditions);
		this.attachedItem = attachedItem;
	}

	public LootItemFunctionType getType()
	{
		return RediscoveredLootFunctions.SET_ATTACHED_ITEM.get();
	}

	public ItemStack run(ItemStack stack, LootContext context)
	{
		return AttachedItem.attachItem(stack, this.attachedItem);
	}

	public static LootItemConditionalFunction.Builder<?> attachItem(ItemStack attachedItem)
	{
		return simpleBuilder(condition -> new SetAttachedItemFunction(condition, attachedItem));
	}
}