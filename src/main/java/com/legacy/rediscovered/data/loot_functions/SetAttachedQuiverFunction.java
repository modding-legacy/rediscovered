package com.legacy.rediscovered.data.loot_functions;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;

import com.legacy.rediscovered.registry.RediscoveredLootFunctions;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;

public class SetAttachedQuiverFunction extends LootItemConditionalFunction
{
	public static final Codec<SetAttachedQuiverFunction> CODEC = RecordCodecBuilder.create(instance ->
	{
		return commonFields(instance).and(ItemStack.CODEC.fieldOf("attached_item").forGetter(o -> o.attachedItem)).and(ExtraCodecs.strictOptionalField(ResourceLocation.CODEC, "loot_table").forGetter(o -> o.lootTable)).apply(instance, SetAttachedQuiverFunction::new);
	});

	private final ItemStack attachedItem;
	private final Optional<ResourceLocation> lootTable;

	protected SetAttachedQuiverFunction(List<LootItemCondition> conditions, ItemStack attachedItem, Optional<ResourceLocation> lootTable)
	{
		super(conditions);
		this.attachedItem = attachedItem;
		this.lootTable = lootTable;
	}

	public LootItemFunctionType getType()
	{
		return RediscoveredLootFunctions.SET_ATTACHED_QUIVER.get();
	}

	public ItemStack run(ItemStack stack, LootContext context)
	{
		var setContents = new SetQuiverContentsFunction(this.predicates, this.lootTable);
		return new SetAttachedItemFunction(this.predicates, setContents.apply(this.attachedItem.copy(), context)).apply(stack, context);
	}

	public static LootItemConditionalFunction.Builder<?> attachQuiver(ItemStack attachedItem, @Nullable ResourceLocation lootTable)
	{
		return simpleBuilder(condition -> new SetAttachedQuiverFunction(condition, attachedItem, Optional.ofNullable(lootTable)));
	}
}
