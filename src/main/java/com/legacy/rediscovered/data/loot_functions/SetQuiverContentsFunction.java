package com.legacy.rediscovered.data.loot_functions;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.registry.RediscoveredLootFunctions;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;

public class SetQuiverContentsFunction extends LootItemConditionalFunction
{
	public static final Codec<SetQuiverContentsFunction> CODEC = RecordCodecBuilder.create(instance ->
	{
		return commonFields(instance).and(ExtraCodecs.strictOptionalField(ResourceLocation.CODEC, "loot_table").forGetter(o -> o.lootTable)).apply(instance, SetQuiverContentsFunction::new);
	});

	private final Optional<ResourceLocation> lootTable;

	protected SetQuiverContentsFunction(List<LootItemCondition> conditions, Optional<ResourceLocation> lootTable)
	{
		super(conditions);
		this.lootTable = lootTable;
	}

	public LootItemFunctionType getType()
	{
		return RediscoveredLootFunctions.SET_QUIVER_CONTENTS.get();
	}

	public ItemStack run(ItemStack stack, LootContext context)
	{
		if (stack.is(RediscoveredTags.Items.QUIVERS) && this.lootTable.isPresent())
		{
			SimpleContainer container = new SimpleContainer(QuiverData.QUIVER_SIZE);
			context.getResolver().getLootTable(this.lootTable.get()).fill(container, new LootParams.Builder(context.getLevel()).withLuck(context.getLuck()).create(LootContextParamSets.EMPTY), context.getRandom().nextLong());
			QuiverData quiver = QuiverData.getOrCreate(stack);
			quiver.injectData(container.items);
		}
		return stack;
	}

	public static LootItemConditionalFunction.Builder<?> withContents(@Nullable ResourceLocation lootTable)
	{
		return simpleBuilder(condition -> new SetQuiverContentsFunction(condition, Optional.ofNullable(lootTable)));
	}
}
