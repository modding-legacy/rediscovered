package com.legacy.rediscovered.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.BrittleBlock;
import com.legacy.rediscovered.block.ChairBlock;
import com.legacy.rediscovered.block.DragonAltarBlock;
import com.legacy.rediscovered.block.DragonAltarBlock.Shape;
import com.legacy.rediscovered.block.NetherReactorBlock;
import com.legacy.rediscovered.block.ObsidianBulbBlock;
import com.legacy.rediscovered.block.ShallowDirtSlabBlock;
import com.legacy.rediscovered.block.TableBlock;
import com.legacy.rediscovered.item.RubyEyeItem;
import com.legacy.rediscovered.item.util.DragonArmorTrim.Decoration;
import com.legacy.rediscovered.registry.RediscoveredArmorTrims;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.Util;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.models.ItemModelGenerators;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.LadderBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.neoforged.neoforge.client.model.generators.BlockModelBuilder;
import net.neoforged.neoforge.client.model.generators.BlockStateProvider;
import net.neoforged.neoforge.client.model.generators.ConfiguredModel;
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder;
import net.neoforged.neoforge.client.model.generators.ItemModelProvider;
import net.neoforged.neoforge.client.model.generators.ModelFile;
import net.neoforged.neoforge.client.model.generators.ModelProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class RediscoveredModelProv
{
	private static final String GENERATED = "item/generated";
	private static final String HANDHELD = "item/handheld";

	protected static List<Block> getAllBlocks()
	{
		return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(RediscoveredMod.MODID)).toList();
	}

	protected static List<Item> getAllItems(Function<Item, Boolean> condition)
	{
		return BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(RediscoveredMod.MODID) && condition.apply(item)).toList();
	}

	protected static List<Item> getAllItems()
	{
		return getAllItems(i -> true);
	}

	public static class ItemModels extends ItemModelProvider
	{
		public ItemModels(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void registerModels()
		{
			Item converter = RediscoveredBlocks.rotational_converter.asItem();
			this.withExistingParent(this.name(converter), this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + this.name(converter)));

			Item spikes = RediscoveredBlocks.spikes.asItem();
			this.withExistingParent(this.name(spikes), this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + this.name(spikes)));

			this.basicItem(RediscoveredBlocks.gear.asItem());
			this.basicItem(RediscoveredItems.ruby);
			this.basicItem(RediscoveredItems.purple_arrow);
			this.basicItem(RediscoveredItems.raw_fish);
			this.basicItem(RediscoveredItems.cooked_fish);
			this.basicItem(RediscoveredItems.fish_bucket);
			this.basicItem(RediscoveredItems.scarecrow);
			this.basicItem(RediscoveredItems.draconic_trim);
			this.basicItem(RediscoveredItems.dragon_armor_chain_smithing_template);
			this.basicItem(RediscoveredItems.dragon_armor_plating_smithing_template);
			this.basicItem(RediscoveredItems.dragon_armor_inlay_smithing_template);
			this.basicItem(RediscoveredItems.music_disc_calm4);

			this.flute(RediscoveredItems.ruby_flute);

			this.tintedOverlay(RediscoveredItems.quiver);
			this.tintedOverlay(RediscoveredItems.quiver.builtInRegistryHolder().unwrapKey().get().location().withSuffix("_chestplate"));

			this.trimmableArmorItem(RediscoveredItems.studded_helmet);
			this.trimmableArmorItem(RediscoveredItems.studded_chestplate);
			this.trimmableArmorItem(RediscoveredItems.studded_leggings);
			this.trimmableArmorItem(RediscoveredItems.studded_boots);

			this.trimmableArmorItem(RediscoveredItems.plate_helmet);
			this.trimmableArmorItem(RediscoveredItems.plate_chestplate);
			this.trimmableArmorItem(RediscoveredItems.plate_leggings);
			this.trimmableArmorItem(RediscoveredItems.plate_boots);
			
			this.dragonArmor(RediscoveredItems.dragon_armor);

			/*getBuilder(item.toString()).parent(new ModelFile.UncheckedModelFile("item/chestplate_quiver")).texture("layer0", item.withPrefix("item/").texture("layer1", item.withPrefix("item/"));*/

			// basic spawn eggs
			getAllItems(i -> i instanceof SpawnEggItem).forEach(item -> this.withExistingParent(BuiltInRegistries.ITEM.getKey(item).getPath(), new ResourceLocation("item/template_spawn_egg")));

		}

		public ItemModelBuilder flute(Item item)
		{
			String name = this.name(item);
			ResourceLocation texture = this.texLoc(name);
			//@formatter:off
			ItemModelBuilder playing = this.withExistingParent(name + "_playing", GENERATED).texture("layer0", texture).transforms()
					.transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).rotation(5, -56, 51).translation(-3.75F, -0.25F, -1.75F).scale(0.5F).end()
					.transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).rotation(5, -56 + 180, 51 * -1).translation(-3.75F, -0.25F, -1.75F).scale(0.5F).end()
					.transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).rotation(-45, 4, 36).translation(-4F, -6F, -11F).scale(1.0F).end()
					.transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).rotation(-45, -176, -36).translation(-4F, -6F, -11F).scale(1.0F).end()
					.end();
			
			ItemModelBuilder model = this.withExistingParent(name, GENERATED).texture("layer0", texture).transforms()
					.transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).rotation(0, 180, 0).translation(0, 3, 1).scale(0.55F).end()
					.transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).rotation(0, 0, 0).translation(0, 3, 1).scale(0.55F).end()
					.transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).rotation(0, -90, 25).translation(1.13F, 3.2F, 1.13F).scale(0.68F).end()
					.transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).rotation(0, 90, -25).translation(1.13F, 3.2F, 1.13F).scale(0.68F).end()
					.end()
					.override().predicate(RediscoveredMod.locate("playing"), 1.0F).model(playing).end();
			//@formatter:on

			return model;
		}

		public ItemModelBuilder tintedOverlay(Item item)
		{
			return tintedOverlay(BuiltInRegistries.ITEM.getKey(item));
		}

		public ItemModelBuilder tintedOverlay(ResourceLocation id)
		{
			ResourceLocation texture = new ResourceLocation(id.getNamespace(), "item/" + id.getPath());
			return getBuilder(id.toString()).parent(new ModelFile.UncheckedModelFile(GENERATED)).texture("layer0", texture).texture("layer1", texture.withSuffix("_overlay"));
		}

		public ItemModelBuilder handheldItem(Item item)
		{
			return handheldItem(Objects.requireNonNull(BuiltInRegistries.ITEM.getKey(item)));
		}

		public ItemModelBuilder handheldItem(ResourceLocation item)
		{
			return getBuilder(item.toString()).parent(new ModelFile.UncheckedModelFile(HANDHELD)).texture("layer0", item.withPrefix("item/"));
		}

		public ItemModelBuilder basicItemInLoc(String name, ResourceLocation item)
		{
			return getBuilder(name.replace("block/", "item/")).parent(new ModelFile.UncheckedModelFile(GENERATED)).texture("layer0", item);
		}

		public ItemModelBuilder basicItemInLoc(ResourceLocation item)
		{
			return this.basicItemInLoc(item.toString(), item);
		}

		public ResourceLocation texLoc(String key)
		{
			return this.modLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}

		public ResourceLocation mcTexLoc(String key)
		{
			return this.mcLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}

		/**
		 * From vanilla's model provider. Use TRIM_MATERIALS below.
		 */
		@Deprecated
		private static final List<TrimModelData> GENERATED_TRIM_MATERIALS = List.of(new TrimModelData("quartz", 0.1F, Map.of()), new TrimModelData("iron", 0.2F, Map.of(ArmorMaterials.IRON, "iron_darker")), new TrimModelData("netherite", 0.3F, Map.of(ArmorMaterials.NETHERITE, "netherite_darker")), new TrimModelData("redstone", 0.4F, Map.of()), new TrimModelData("copper", 0.5F, Map.of()), new TrimModelData("gold", 0.6F, Map.of(ArmorMaterials.GOLD, "gold_darker")), new TrimModelData("emerald", 0.7F, Map.of()), new TrimModelData("diamond", 0.8F, Map.of(ArmorMaterials.DIAMOND, "diamond_darker")), new TrimModelData("lapis", 0.9F, Map.of()), new TrimModelData("amethyst", 1.0F, Map.of()));
		private static final List<TrimModelData> TRIM_MATERIALS = Util.make(() ->
		{
			List<TrimModelData> list = new ArrayList<>();
			// Add vanilla trims
			list.addAll(GENERATED_TRIM_MATERIALS);
			// Add modded trim materials to support
			list.add(TrimModelData.modded(RediscoveredArmorTrims.Materials.RUBY, 0.400173F, Map.of()));

			// Sort in order of model index
			list.sort((a, b) -> Float.compare(a.itemModelIndex, b.itemModelIndex));
			return list;
		});

		static record TrimModelData(String name, float itemModelIndex, Map<ArmorMaterial, String> overrideArmorMaterials)
		{
			public String name(ArmorMaterial armorMaterial)
			{
				return this.overrideArmorMaterials.getOrDefault(armorMaterial, this.name);
			}

			public static TrimModelData modded(ResourceKey<TrimMaterial> key, float itemModelIndex, Map<ArmorMaterial, String> overrideArmorMaterials)
			{
				return new TrimModelData(RediscoveredArmorTrims.Materials.assetID(key), itemModelIndex, overrideArmorMaterials);
			}
		}

		public ItemModelBuilder trimmableArmorItem(Item item)
		{
			if (!(item instanceof ArmorItem armorItem))
				throw new IllegalArgumentException(item + " is not an ArmorItem");

			String name = this.name(armorItem);
			ArmorItem.Type armorType = armorItem.getType();
			ArmorMaterial material = armorItem.getMaterial();

			// Overrides
			Map<String, ModelFile> overrides = new HashMap<>();
			for (TrimModelData t : TRIM_MATERIALS)
			{
				int layer = 0;
				String trimPalette = t.overrideArmorMaterials.getOrDefault(material, t.name);
				ItemModelBuilder builder = this.withExistingParent(name + "_" + trimPalette + "_trim", "item/generated").texture("layer" + layer++, this.texLoc(this.name(armorItem)));
				if (armorItem.getMaterial() == ArmorMaterials.LEATHER || armorItem instanceof DyeableLeatherItem)
					builder.texture("layer" + layer++, this.texLoc(this.name(armorItem) + "_overlay"));

				ResourceLocation trimTexture = new ResourceLocation("trims/items/" + armorType.getName() + "_trim_" + trimPalette);
				this.existingFileHelper.trackGenerated(trimTexture, ModelProvider.TEXTURE);
				builder.texture("layer" + layer++, trimTexture);

				overrides.put(t.name, builder);
			}

			// Base model with overrides
			int layer = 0;
			ItemModelBuilder builder = this.withExistingParent(name, "item/generated").texture("layer" + layer++, this.texLoc(this.name(armorItem)));
			if (armorItem.getMaterial() == ArmorMaterials.LEATHER || armorItem instanceof DyeableLeatherItem)
				builder.texture("layer" + layer++, this.texLoc(this.name(armorItem) + "_overlay"));

			for (TrimModelData t : TRIM_MATERIALS)
			{
				builder.override().predicate(ItemModelGenerators.TRIM_TYPE_PREDICATE_ID, t.itemModelIndex).model(overrides.get(t.name));
			}

			return builder;
		}

		public ItemModelBuilder dragonArmor(Item item)
		{
			String name = this.name(item);
			ItemModelBuilder builder = this.withExistingParent(name, GENERATED);
			int layer = 0;
			for (Decoration dec : Decoration.values())
				builder.texture("layer" + layer++, this.texLoc(name + "_" + dec.getSerializedName()));
			return builder;
		}

		private ResourceLocation key(Item item)
		{
			return BuiltInRegistries.ITEM.getKey(item);
		}

		private String name(Item item)
		{
			return key(item).getPath();
		}
	}

	public static class States extends BlockStateProvider
	{
		public States(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, RediscoveredMod.MODID, existingFileHelper);
		}

		private static final String GRASS_SLAB_SNOW = "grass_slab_snow", GRASS_SLAB_TOP_SNOW = "grass_slab_top_snow";

		@Override
		protected void registerStatesAndModels()
		{
			this.leaves((LeavesBlock) RediscoveredBlocks.ancient_cherry_leaves, null);
			this.cross(RediscoveredBlocks.ancient_cherry_sapling);
			this.pottedCross(RediscoveredBlocks.potted_ancient_cherry_sapling, null);

			this.cross(RediscoveredBlocks.paeonia);
			this.pottedCross(RediscoveredBlocks.potted_paeonia, null);
			this.cross(RediscoveredBlocks.rose);
			this.pottedCross(RediscoveredBlocks.potted_rose, null);
			this.cross(RediscoveredBlocks.cyan_rose);
			this.pottedCross(RediscoveredBlocks.potted_cyan_rose, null);

			this.tallCross(RediscoveredBlocks.empty_peony_bush);
			this.tallCross(RediscoveredBlocks.empty_rose_bush);

			/*Block emptyPeony = RediscoveredBlocks.empty_peony_bush;
			this.cross(emptyPeony, this.name(emptyPeony) + "_top", false);
			this.cross(emptyPeony, this.name(emptyPeony) + "_bottom", false);
			this.item(emptyPeony, this.extend(BuiltInRegistries.ITEM.getKey(emptyPeony.asItem()), "_top"), true);
			
			Block emptyRose = RediscoveredBlocks.empty_rose_bush;
			this.cross(emptyRose, this.name(emptyRose) + "_top", false);
			this.cross(emptyRose, this.name(emptyRose) + "_bottom", false);
			this.item(emptyRose, this.extend(BuiltInRegistries.ITEM.getKey(emptyRose.asItem()), "_top"), true);*/

			this.simpleBlock(RediscoveredBlocks.ruby_block);
			this.simpleBlock(RediscoveredBlocks.ruby_ore);
			this.simpleBlock(RediscoveredBlocks.deepslate_ruby_ore);

			this.simpleBlock(RediscoveredBlocks.ancient_crying_obsidian);
			this.simpleBlock(RediscoveredBlocks.glowing_obsidian);
			this.obsidianLamp(RediscoveredBlocks.obsidian_bulb);

			this.simpleBlock(RediscoveredBlocks.bright_green_wool);
			this.simpleBlock(RediscoveredBlocks.spring_green_wool);
			this.simpleBlock(RediscoveredBlocks.sky_blue_wool);
			this.simpleBlock(RediscoveredBlocks.slate_blue_wool);
			this.simpleBlock(RediscoveredBlocks.lavender_wool);
			this.simpleBlock(RediscoveredBlocks.rose_wool);
			this.simpleBlock(RediscoveredBlocks.large_bricks);

			this.dragonAltar(RediscoveredBlocks.dragon_altar);

			this.carpet(RediscoveredBlocks.bright_green_carpet, RediscoveredBlocks.bright_green_wool);
			this.carpet(RediscoveredBlocks.spring_green_carpet, RediscoveredBlocks.spring_green_wool);
			this.carpet(RediscoveredBlocks.sky_blue_carpet, RediscoveredBlocks.sky_blue_wool);
			this.carpet(RediscoveredBlocks.slate_blue_carpet, RediscoveredBlocks.slate_blue_wool);
			this.carpet(RediscoveredBlocks.lavender_carpet, RediscoveredBlocks.lavender_wool);
			this.carpet(RediscoveredBlocks.rose_carpet, RediscoveredBlocks.rose_wool);

			this.stairs(RediscoveredBlocks.large_brick_stairs, RediscoveredBlocks.large_bricks);

			this.models().slab(GRASS_SLAB_SNOW, this.modLoc(ModelProvider.BLOCK_FOLDER + "/grass_slab_snow"), this.blockTexture(Blocks.DIRT), this.blockTexture(Blocks.SNOW));
			this.models().slabTop(GRASS_SLAB_TOP_SNOW, this.blockTexture(Blocks.GRASS_BLOCK).withSuffix("_snow"), this.blockTexture(Blocks.DIRT), this.blockTexture(Blocks.SNOW));
			this.slab(RediscoveredBlocks.dirt_slab, Blocks.DIRT);
			this.slab(RediscoveredBlocks.coarse_dirt_slab, Blocks.COARSE_DIRT);
			this.slab(RediscoveredBlocks.rooted_dirt_slab, Blocks.ROOTED_DIRT);
			this.grassSlab(RediscoveredBlocks.grass_slab, Blocks.GRASS_BLOCK, true, Blocks.DIRT);
			this.grassSlab(RediscoveredBlocks.mycelium_slab, Blocks.MYCELIUM, false, Blocks.DIRT);
			this.grassSlab(RediscoveredBlocks.podzol_slab, Blocks.PODZOL, false, Blocks.DIRT);
			this.grassSlab(RediscoveredBlocks.dirt_path_slab, Blocks.DIRT_PATH, false, Blocks.DIRT);

			this.slab(RediscoveredBlocks.large_brick_slab, RediscoveredBlocks.large_bricks);

			this.wall(RediscoveredBlocks.large_brick_wall, RediscoveredBlocks.large_bricks);

			this.furniture(RediscoveredBlocks.oak_chair, RediscoveredBlocks.oak_table, Blocks.OAK_PLANKS);
			this.furniture(RediscoveredBlocks.spruce_chair, RediscoveredBlocks.spruce_table, Blocks.SPRUCE_PLANKS);
			this.furniture(RediscoveredBlocks.birch_chair, RediscoveredBlocks.birch_table, Blocks.BIRCH_PLANKS);
			this.furniture(RediscoveredBlocks.jungle_chair, RediscoveredBlocks.jungle_table, Blocks.JUNGLE_PLANKS);
			this.furniture(RediscoveredBlocks.acacia_chair, RediscoveredBlocks.acacia_table, Blocks.ACACIA_PLANKS);
			this.furniture(RediscoveredBlocks.dark_oak_chair, RediscoveredBlocks.dark_oak_table, Blocks.DARK_OAK_PLANKS);
			this.furniture(RediscoveredBlocks.mangrove_chair, RediscoveredBlocks.mangrove_table, Blocks.MANGROVE_PLANKS);
			this.furniture(RediscoveredBlocks.cherry_chair, RediscoveredBlocks.cherry_table, Blocks.CHERRY_PLANKS);
			this.furniture(RediscoveredBlocks.bamboo_chair, RediscoveredBlocks.bamboo_table, Blocks.BAMBOO_PLANKS);
			this.furniture(RediscoveredBlocks.crimson_chair, RediscoveredBlocks.crimson_table, Blocks.CRIMSON_PLANKS);
			this.furniture(RediscoveredBlocks.warped_chair, RediscoveredBlocks.warped_table, Blocks.WARPED_PLANKS);

			this.redDragonEgg();
			this.netherReactor(RediscoveredBlocks.nether_reactor_core);

			this.fakeFire(RediscoveredBlocks.fake_fire, Blocks.FIRE, 2);
			this.fakeFire(RediscoveredBlocks.fake_soul_fire, Blocks.SOUL_FIRE, 2);
			this.rubyEye(RediscoveredBlocks.ruby_eye);

			this.horizontalPortal(RediscoveredBlocks.skylands_portal);

			this.brittleBlock(RediscoveredBlocks.brittle_packed_mud);
			this.brittleBlock(RediscoveredBlocks.brittle_mud_bricks);

			this.dragonPylon(RediscoveredBlocks.mini_dragon_pylon, RediscoveredMod.locate("entity/dragon_pylon/core_particle"));

			// generate default items
			for (Block block : this.registeredBlocks.keySet())
			{
				boolean contained = !this.itemModels().generatedModels.containsKey(this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + this.name(block))) && !this.itemModels().generatedModels.containsKey(this.modLoc(ModelProvider.ITEM_FOLDER + "/" + this.name(block)));
				var possibleItem = BuiltInRegistries.ITEM.get(this.key(block));

				if (possibleItem != null && possibleItem != Blocks.AIR.asItem() && contained)
				{
					try
					{
						/*System.out.println("Generating " + this.name(block));*/
						this.itemModels().withExistingParent(this.name(block), this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + this.name(block)));
					}
					catch (Exception e)
					{
						/*System.out.println("Skipped " + this.name(block));*/
					}
				}
			}
		}

		public void dragonAltar(Block block)
		{
			String name = this.name(block);
			var topEdgeTex = this.blockTexture(block).withSuffix("_top_edge");
			var topCornerTex = this.blockTexture(block).withSuffix("_top_corner");
			var sideTex = this.blockTexture(block).withSuffix("_side");
			var copperTex = this.blockTexture(Blocks.COPPER_BLOCK);

			var edgeModel = this.cubeColumnMatchingEnds(name + "_edge", sideTex, copperTex, topEdgeTex);
			var cornerModel = this.cubeColumnMatchingEnds(name + "_corner", copperTex, copperTex, topCornerTex);
			var centerModel = this.models().getExistingFile(this.blockFolder(this.name(RediscoveredBlocks.glowing_obsidian)));

			var variantBuilder = this.getVariantBuilder(block);

			for (DragonAltarBlock.Shape shape : DragonAltarBlock.SHAPE.getPossibleValues())
			{
				var model = shape == Shape.CORNER ? cornerModel : shape == Shape.CENTER ? centerModel : edgeModel;
				for (Direction facing : DragonAltarBlock.FACING.getPossibleValues())
				{
					variantBuilder.partialState().with(DragonAltarBlock.SHAPE, shape).with(DragonAltarBlock.FACING, facing).addModels(ConfiguredModel.builder().modelFile(model).rotationY(shape == Shape.CENTER ? 0 : (int) facing.toYRot()).build());
				}
			}

			this.itemModels().withExistingParent(name, edgeModel.getLocation());
		}

		public BlockModelBuilder cubeColumnMatchingEnds(String name, ResourceLocation sideTexture, ResourceLocation frontTexture, ResourceLocation endTexture)
		{
			//@formatter:off
			return this.models().withExistingParent(name, this.mcBlockFolder("block"))
					.texture("end", endTexture)
					.texture("side", sideTexture)
					.texture("front", frontTexture)
					.texture("particle", "#end")
					.element()
					.from(0, 0, 0).to(16, 16, 16)
					.face(Direction.NORTH).texture("#side").uvs(0, 0, 16, 16).end()
					.face(Direction.SOUTH).texture("#side").uvs(0, 0, 16, 16).end()
					.face(Direction.EAST).texture("#front").uvs(0, 0, 16, 16).end()
					.face(Direction.WEST).texture("#front").uvs(0, 0, 16, 16).end()
					.face(Direction.UP).texture("#end").uvs(0, 0, 16, 16).end()
					.face(Direction.DOWN).texture("#end").uvs(0, 16, 16, 0).end()
					.end();
			//@formatter:on
		}

		public void dragonPylon(Block block, ResourceLocation texture)
		{
			var model = this.models().cubeAll(name(block), texture);
			this.simpleBlock(block, model);
		}

		public void rubyEye(Block block)
		{
			this.simpleBlock(block, this.models().getBuilder(this.name(block)));

			Item item = block.asItem();
			String name = this.name(item);
			ItemModelBuilder builder = this.itemModels().withExistingParent(name, GENERATED).texture("layer0", this.itemFolder(name).withSuffix("_" + RubyEyeItem.TEXTURE_COUNT));
			for (int i = 0; i <= RubyEyeItem.TEXTURE_COUNT; i++)
			{
				builder.override().predicate(RediscoveredMod.locate("eye_glow"), i / (float) RubyEyeItem.TEXTURE_COUNT).model(this.itemModels().withExistingParent(name + "_" + i, GENERATED).texture("layer0", this.itemFolder(name).withSuffix("_" + i)));
			}
		}

		public void brittleBlock(Block block)
		{
			String name = this.name(block);

			var variantBuilder = this.getVariantBuilder(block);
			int size = BrittleBlock.STRENGTH.getPossibleValues().size();
			BlockModelBuilder[] models = new BlockModelBuilder[size];
			for (int i = 0; i < size; i++)
			{
				String suffix = "_" + i;
				BlockModelBuilder model = this.models().cubeAll(name + suffix, this.blockTexture(block).withSuffix(suffix));
				models[i] = model;
				variantBuilder.partialState().with(BrittleBlock.STRENGTH, i).addModels(new ConfiguredModel(model));
			}

			this.itemModels().withExistingParent(name, models[0].getLocation());
		}

		public void horizontalPortal(Block block)
		{
			String name = this.name(block);
			ResourceLocation texture = this.blockTexture(block);
			String portal = "portal";
			String portalKey = "#" + portal;
			//@formatter:off
			var model = this.models().getBuilder(name)
			.texture(portal, texture)
			.texture("particle", texture)
			.renderType("translucent")
			.ao(false)
			.element()
				.from(0, 6, 0)
				.to(16, 10, 16)
				.face(Direction.UP).texture(portalKey).end()
				.face(Direction.DOWN).texture(portalKey).end()
				.end();
			//@formatter:on

			this.getVariantBuilder(block).partialState().addModels(new ConfiguredModel(model));
		}

		public void obsidianLamp(Block block)
		{
			String name = this.name(block);

			ResourceLocation texture = this.blockTexture(block);
			BlockModelBuilder unpowered = this.models().cubeAll(name, texture);
			BlockModelBuilder unpowered_lit = this.models().cubeAll(name + "_lit", texture.withSuffix("_lit"));
			BlockModelBuilder powered = this.models().cubeAll(name + "_powered", texture.withSuffix("_powered"));
			BlockModelBuilder powered_lit = this.models().cubeAll(name + "_powered_lit", texture.withSuffix("_powered_lit"));

			var variantBuilder = this.getVariantBuilder(block);

			for (int light : ObsidianBulbBlock.LIGHT_LEVEL.getPossibleValues())
			{
				boolean lit = light > 0;
				variantBuilder.partialState().with(ObsidianBulbBlock.POWERED, false).with(ObsidianBulbBlock.LIGHT_LEVEL, light).addModels(new ConfiguredModel(lit ? unpowered_lit : unpowered));
				variantBuilder.partialState().with(ObsidianBulbBlock.POWERED, true).with(ObsidianBulbBlock.LIGHT_LEVEL, light).addModels(new ConfiguredModel(lit ? powered_lit : powered));
			}

			this.itemModels().withExistingParent(name, unpowered.getLocation());
		}

		public void fakeFire(Block fakeFire, Block fire, int variants)
		{
			// Models
			String name = this.name(fakeFire);
			ResourceLocation tempFloor = new ResourceLocation("block/template_fire_floor");
			ResourceLocation tempSide = new ResourceLocation("block/template_fire_side");
			ResourceLocation tempSideAlt = new ResourceLocation("block/template_fire_side_alt");
			List<BlockModelBuilder> floorModels = new ArrayList<>();
			List<BlockModelBuilder> sideModels = new ArrayList<>();
			List<BlockModelBuilder> sideAltModels = new ArrayList<>();
			for (int variant = 0; variant < variants; variant++)
			{
				String renderType = "cutout";
				String v = variants > 0 ? Integer.toString(variant) : "";
				ResourceLocation texture = this.blockTexture(fire).withSuffix(v.isEmpty() ? "" : "_" + v);
				floorModels.add(this.models().withExistingParent(name + "_floor" + v, tempFloor).texture("fire", texture).renderType(renderType));
				sideModels.add(this.models().withExistingParent(name + "_side" + v, tempSide).texture("fire", texture).renderType(renderType));
				sideAltModels.add(this.models().withExistingParent(name + "_side_alt" + v, tempSideAlt).texture("fire", texture).renderType(renderType));
			}

			// Blockstate with texture/model variants
			var multiPart = this.getMultipartBuilder(fakeFire);
			var floorBuilder = multiPart.part();
			for (int i = 0; i < variants; i++)
			{
				floorBuilder = floorBuilder.modelFile(floorModels.get(i));
				if (i < variants - 1)
					floorBuilder = floorBuilder.nextModel();
			}
			floorBuilder.addModel();

			for (int rotation : new int[] { 0, 90, 180, 270 })
			{
				var sideBuilder = multiPart.part();
				for (int i = 0; i < variants; i++)
				{
					sideBuilder = sideBuilder.modelFile(sideModels.get(i)).rotationY(rotation).nextModel().modelFile(sideAltModels.get(i)).rotationY(rotation);
					if (i < variants - 1)
						sideBuilder = sideBuilder.nextModel();
				}
				sideBuilder.addModel();
			}
		}

		public void netherReactor(Block block)
		{
			String name = this.name(block);
			ResourceLocation texture = this.blockTexture(block);
			BlockModelBuilder model = this.models().cubeAll(name, texture);
			BlockModelBuilder activeModel = this.models().cubeAll(name + "_active", texture.withSuffix("_active"));
			this.getVariantBuilder(block).partialState().with(NetherReactorBlock.ACTIVE, false).addModels(new ConfiguredModel(model)).partialState().with(NetherReactorBlock.ACTIVE, true).addModels(new ConfiguredModel(activeModel));

			this.itemModels().withExistingParent(name, model.getLocation());
		}

		/**
		 * @param block
		 * @param topTextureParent
		 *            Used for "wood" blocks, this will set the top texture to that of
		 *            another block
		 */
		public void pillarBlock(Block block, Block topTextureParent)
		{
			this.pillarBlock(block, topTextureParent, null);
		}

		public void pillarBlock(Block block, Block textureParent, @Nullable String renderType)
		{
			ResourceLocation baseName = this.blockTexture(textureParent != null ? textureParent : block);

			if (block instanceof RotatedPillarBlock pillar)
			{
				var side = extend(baseName, "_side");
				axisBlockWithRenderType(pillar, side, textureParent != null ? side : extend(baseName, "_top"), renderType != null ? renderType : "solid");
			}
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non pillar block: {}", baseName);
		}

		public void door(Block block, Item item, @Nullable String renderType)
		{
			if (block instanceof DoorBlock door)
			{
				this.doorBlockWithRenderType(door, this.extend(this.blockTexture(door), "_lower"), this.extend(this.blockTexture(door), "_upper"), renderType != null ? renderType : "cutout");
				this.itemModels().basicItem(item);
			}
		}

		public void trapDoor(Block block, boolean orientable)
		{
			if (block instanceof TrapDoorBlock door)
			{
				this.trapdoorBlockWithRenderType(door, this.blockTexture(door), orientable, "cutout");
				this.itemModels().withExistingParent(this.name(door), this.extend(this.blockTexture(door), "_bottom"));
			}
		}

		public void trapDoor(Block block, boolean orientable, @Nullable String renderType)
		{
			if (block instanceof TrapDoorBlock door)
			{
				this.trapdoorBlockWithRenderType(door, this.blockTexture(door), orientable, renderType != null ? renderType : "cutout");
				this.itemModels().withExistingParent(this.name(door), this.extend(this.blockTexture(door), "_bottom"));
			}
		}

		public void vine(Block b)
		{
			var vineModel = this.models().withExistingParent(name(b), "block/vine").texture("vine", blockTexture(b)).texture("particle", "#vine").renderType("cutout");
			var vineStateBuilder = this.getMultipartBuilder(b);

			for (Direction dir : VineBlock.PROPERTY_BY_DIRECTION.keySet().stream().sorted().toList())
			{
				int y = (dir.getOpposite().get2DDataValue() * 90);
				vineStateBuilder.part().modelFile(vineModel).uvLock(true).rotationX(dir == Direction.UP ? 270 : 0).rotationY(y % 360).addModel().condition(VineBlock.PROPERTY_BY_DIRECTION.get(dir), true).end();
			}

			this.item(b.asItem(), true);
		}

		public void cross(Block block)
		{
			this.cross(block, this.name(block), true);
		}

		public void cross(Block block, String name, boolean item)
		{
			this.simpleBlock(block, this.models().withExistingParent(name, this.mcBlockFolder("cross")).texture("cross", this.blockFolder(name)).renderType("cutout"));

			if (item)
				this.item(block.asItem(), true);
		}

		public void pottedCross(Block block, ResourceLocation texture)
		{
			if (texture == null)
			{
				texture = this.blockTexture(block);
				texture = texture.withPath(texture.getPath().replace("potted_", ""));
			}

			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("flower_pot_cross")).texture("plant", texture).renderType("cutout");

			this.simpleBlock(block, model);
		}

		public void tallCross(Block block)
		{
			BlockModelBuilder top = this.models().withExistingParent(this.name(block).concat("_top"), this.mcBlockFolder("cross")).renderType("cutout");
			BlockModelBuilder bottom = this.models().withExistingParent(this.name(block).concat("_bottom"), this.mcBlockFolder("cross")).renderType("cutout");

			var topTex = this.extend(this.blockTexture(block), "_top");
			var bottomTex = this.extend(this.blockTexture(block), "_bottom");

			top = top.texture("cross", topTex);
			bottom = bottom.texture("cross", bottomTex);

			BlockModelBuilder topFinal = top;
			BlockModelBuilder bottomFinal = bottom;

			getVariantBuilder(block).forAllStatesExcept(state -> ConfiguredModel.builder().modelFile(state.getValue(DoublePlantBlock.HALF) == DoubleBlockHalf.UPPER ? topFinal : bottomFinal).build());

			this.item(block.asItem(), this.extend(BuiltInRegistries.ITEM.getKey(block.asItem()), "_top"), true);
		}

		public void stairs(Block block, Block parent)
		{
			if (block instanceof StairBlock stairs)
			{
				var tex = blockTexture(parent);
				this.stairsBlock(stairs, tex);
			}
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non stairs block: {}", this.key(block));
		}

		public void slab(Block block, Block parent)
		{
			if (block instanceof SlabBlock slab)
			{
				super.slabBlock(slab, this.blockTexture(parent), this.blockTexture(parent));
			}
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non slab block: {}", this.key(block));
		}

		public void grassSlab(Block block, Block parent, boolean hasOverlay, Block bottom)
		{
			this.grassSlab(block, parent, hasOverlay, block, parent, bottom, parent);
		}

		public void grassSlab(Block block, Block parent, boolean hasOverlay, Block lowerSide, Block side, Block bottom, Block top)
		{
			if (block instanceof SlabBlock slab)
			{
				ResourceLocation lowerSideTex = this.blockTexture(lowerSide).withSuffix("_side");
				ResourceLocation sideTex = this.blockTexture(side).withSuffix("_side");
				ResourceLocation bottomTex = this.blockTexture(bottom);
				ResourceLocation topTex = this.blockTexture(top).withSuffix("_top");

				if (hasOverlay)
					this.grassSlab(slab, slabWithOverlay(name(block), lowerSideTex, bottomTex, topTex), slabTopWithOverlay(name(block) + "_top", sideTex, bottomTex, topTex), models().withExistingParent(this.name(block) + "_double", this.blockTexture(parent)).renderType("cutout"));
				else if (block instanceof ShallowDirtSlabBlock)
					this.grassSlab(slab, slabShallow(name(block), lowerSideTex, bottomTex, topTex), slabTopShallow(name(block) + "_top", sideTex, bottomTex, topTex), models().getExistingFile(this.blockTexture(parent)));
				else
					this.grassSlab(slab, models().slab(name(block), lowerSideTex, bottomTex, topTex), models().slabTop(name(block) + "_top", sideTex, bottomTex, topTex), models().getExistingFile(this.blockTexture(parent)));
			}
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non slab block: {}", this.key(block));
		}

		public void grassSlab(SlabBlock block, ModelFile bottom, ModelFile top, ModelFile doubleslab)
		{
			if (block.defaultBlockState().hasProperty(BlockStateProperties.SNOWY))
			{
				var builder = getVariantBuilder(block);
				builder.partialState().with(SlabBlock.TYPE, SlabType.BOTTOM).with(BlockStateProperties.SNOWY, false).addModels(new ConfiguredModel(bottom));
				builder.partialState().with(SlabBlock.TYPE, SlabType.TOP).with(BlockStateProperties.SNOWY, false).addModels(new ConfiguredModel(top));
				builder.partialState().with(SlabBlock.TYPE, SlabType.DOUBLE).with(BlockStateProperties.SNOWY, false).addModels(new ConfiguredModel(doubleslab));

				builder.partialState().with(SlabBlock.TYPE, SlabType.BOTTOM).with(BlockStateProperties.SNOWY, true).addModels(new ConfiguredModel(this.models().getExistingFile(this.modLoc(GRASS_SLAB_SNOW))));
				builder.partialState().with(SlabBlock.TYPE, SlabType.TOP).with(BlockStateProperties.SNOWY, true).addModels(new ConfiguredModel(this.models().getExistingFile(this.modLoc(GRASS_SLAB_TOP_SNOW))));
				builder.partialState().with(SlabBlock.TYPE, SlabType.DOUBLE).with(BlockStateProperties.SNOWY, true).addModels(new ConfiguredModel(this.models().getExistingFile(this.mcLoc("grass_block_snow"))));
			}
			else
			{
				var builder = getVariantBuilder(block);
				builder.partialState().with(SlabBlock.TYPE, SlabType.BOTTOM).addModels(new ConfiguredModel(bottom));
				builder.partialState().with(SlabBlock.TYPE, SlabType.TOP).addModels(new ConfiguredModel(top));
				builder.partialState().with(SlabBlock.TYPE, SlabType.DOUBLE).addModels(new ConfiguredModel(doubleslab));
			}
		}

		public BlockModelBuilder slabShallow(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top)
		{
			BlockModelBuilder builder = this.models().withExistingParent(name, "block/block");
			builder.renderType("solid");
			builder.texture("particle", bottom);
			builder.texture("bottom", bottom);
			builder.texture("top", top);
			builder.texture("side", side);
			String b = "#bottom";
			String t = "#top";
			String s = "#side";
			//@formatter:off
			builder.element().from(0, 0, 0).to(16, 7, 16)
			.face(Direction.DOWN).uvs(0, 0, 16, 16).texture(b).cullface(Direction.DOWN).end()
			.face(Direction.UP).uvs(0, 0, 16, 16).texture(t).end()
			.face(Direction.NORTH).uvs(0, 9, 16, 16).texture(s).cullface(Direction.NORTH).end()
			.face(Direction.SOUTH).uvs(0, 9, 16, 16).texture(s).cullface(Direction.SOUTH).end()
			.face(Direction.WEST).uvs(0, 9, 16, 16).texture(s).cullface(Direction.WEST).end()
			.face(Direction.EAST).uvs(0, 9, 16, 16).texture(s).cullface(Direction.EAST).end()
			.end();
			//@formatter:on
			return builder;
		}

		public BlockModelBuilder slabTopShallow(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top)
		{
			BlockModelBuilder builder = this.models().withExistingParent(name, "block/block");
			builder.renderType("solid");
			builder.texture("particle", bottom);
			builder.texture("bottom", bottom);
			builder.texture("top", top);
			builder.texture("side", side);
			String b = "#bottom";
			String t = "#top";
			String s = "#side";
			//@formatter:off
			builder.element().from(0, 8, 0).to(16, 15, 16)
			.face(Direction.DOWN).uvs(0, 0, 16, 16).texture(b).end()
			.face(Direction.UP).uvs(0, 0, 16, 16).texture(t).cullface(Direction.UP).end()
			.face(Direction.NORTH).uvs(0, 1, 16, 8).texture(s).cullface(Direction.NORTH).end()
			.face(Direction.SOUTH).uvs(0, 1, 16, 8).texture(s).cullface(Direction.SOUTH).end()
			.face(Direction.WEST).uvs(0, 1, 16, 8).texture(s).cullface(Direction.WEST).end()
			.face(Direction.EAST).uvs(0, 1, 16, 8).texture(s).cullface(Direction.EAST).end()
			.end();
			//@formatter:on
			return builder;
		}

		public BlockModelBuilder slabWithOverlay(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top)
		{
			BlockModelBuilder builder = this.models().withExistingParent(name, "block/block");
			builder.renderType("cutout");
			builder.texture("particle", bottom);
			builder.texture("bottom", bottom);
			builder.texture("top", top);
			builder.texture("side", side);
			builder.texture("overlay", side.withSuffix("_overlay"));
			String b = "#bottom";
			String t = "#top";
			String s = "#side";
			String o = "#overlay";
			//@formatter:off
			builder.element().from(0, 0, 0).to(16, 8, 16)
			.face(Direction.DOWN).uvs(0, 0, 16, 16).texture(b).cullface(Direction.DOWN).end()
			.face(Direction.UP).uvs(0, 0, 16, 16).texture(t).tintindex(0).end()
			.face(Direction.NORTH).uvs(0, 8, 16, 16).texture(s).cullface(Direction.NORTH).end()
			.face(Direction.SOUTH).uvs(0, 8, 16, 16).texture(s).cullface(Direction.SOUTH).end()
			.face(Direction.WEST).uvs(0, 8, 16, 16).texture(s).cullface(Direction.WEST).end()
			.face(Direction.EAST).uvs(0, 8, 16, 16).texture(s).cullface(Direction.EAST).end()
			.end();
			builder.element().from(0, 0, 0).to(16, 8, 16)
			.face(Direction.NORTH).uvs(0, 8, 16, 16).texture(o).tintindex(0).cullface(Direction.NORTH).end()
			.face(Direction.SOUTH).uvs(0, 8, 16, 16).texture(o).tintindex(0).cullface(Direction.SOUTH).end()
			.face(Direction.WEST).uvs(0, 8, 16, 16).texture(o).tintindex(0).cullface(Direction.WEST).end()
			.face(Direction.EAST).uvs(0, 8, 16, 16).texture(o).tintindex(0).cullface(Direction.EAST).end()
			.end();
			//@formatter:on
			return builder;
		}

		public BlockModelBuilder slabTopWithOverlay(String name, ResourceLocation side, ResourceLocation bottom, ResourceLocation top)
		{
			BlockModelBuilder builder = this.models().withExistingParent(name, "block/block");
			builder.renderType("cutout");
			builder.texture("particle", bottom);
			builder.texture("bottom", bottom);
			builder.texture("top", top);
			builder.texture("side", side);
			builder.texture("overlay", side.withSuffix("_overlay"));
			String b = "#bottom";
			String t = "#top";
			String s = "#side";
			String o = "#overlay";
			//@formatter:off
			builder.element().from(0, 8, 0).to(16, 16, 16)
			.face(Direction.DOWN).uvs(0, 0, 16, 16).texture(b).end()
			.face(Direction.UP).uvs(0, 0, 16, 16).texture(t).tintindex(0).cullface(Direction.UP).end()
			.face(Direction.NORTH).uvs(0, 0, 16, 8).texture(s).cullface(Direction.NORTH).end()
			.face(Direction.SOUTH).uvs(0, 0, 16, 8).texture(s).cullface(Direction.SOUTH).end()
			.face(Direction.WEST).uvs(0, 0, 16, 8).texture(s).cullface(Direction.WEST).end()
			.face(Direction.EAST).uvs(0, 0, 16, 8).texture(s).cullface(Direction.EAST).end()
			.end();
			builder.element().from(0, 8, 0).to(16, 16, 16)
			.face(Direction.NORTH).uvs(0, 0, 16, 8).texture(o).tintindex(0).cullface(Direction.NORTH).end()
			.face(Direction.SOUTH).uvs(0, 0, 16, 8).texture(o).tintindex(0).cullface(Direction.SOUTH).end()
			.face(Direction.WEST).uvs(0, 0, 16, 8).texture(o).tintindex(0).cullface(Direction.WEST).end()
			.face(Direction.EAST).uvs(0, 0, 16, 8).texture(o).tintindex(0).cullface(Direction.EAST).end()
			.end();
			//@formatter:on
			return builder;
		}

		public void fences(Block block, Block gate, Block parent)
		{
			var tex = this.blockTexture(parent);

			if (block instanceof FenceBlock fence)
			{
				this.fenceBlock(fence, tex);
				this.itemModels().fenceInventory(this.name(fence), tex);
			}
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non fence block: {}", this.key(block));

			if (gate instanceof FenceGateBlock fence)
				this.fenceGateBlock(fence, tex);
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non gate block: {}", this.key(gate));
		}

		public void wall(Block block, Block parent)
		{
			this.wall(block, this.blockTexture(parent), null);
		}

		public void wall(Block block, ResourceLocation tex, @Nullable String renderType)
		{
			if (block instanceof WallBlock wall)
			{
				if (renderType != null)
					this.wallBlockWithRenderType(wall, tex, renderType);
				else
					this.wallBlock(wall, tex);

				this.itemModels().wallInventory(this.name(wall), tex);
			}
			else
				RediscoveredMod.LOGGER.error("Tried to datagen model for non wall block: {}", this.key(block));
		}

		public void button(ButtonBlock block, ResourceLocation texture, @Nullable String renderType)
		{
			this.buttonBlock(block, texture);
			this.itemModels().buttonInventory(this.name(block), texture);
		}

		public void sign(StandingSignBlock signBlock, WallSignBlock wallSignBlock, ResourceLocation texture)
		{
			this.signBlock(signBlock, wallSignBlock, texture);
			this.item(signBlock.asItem(), false);
		}

		public void craftingTable(Block block, Block bottom)
		{
			var texture = this.blockTexture(block);
			this.simpleBlock(block, this.models().orientableWithBottom(this.name(block), this.extend(texture, "_side"), this.extend(texture, "_front"), this.blockTexture(bottom), this.extend(texture, "_top")));
		}

		public void ladder(Block block)
		{
			var texture = this.blockTexture(block);

			var vineModel = this.models().withExistingParent(name(block), "block/ladder").texture("texture", texture).texture("particle", "#texture").renderType("cutout");
			var ladderStateBuilder = this.getMultipartBuilder(block);

			for (Direction dir : Arrays.stream(Direction.values()).filter(d -> d.getAxis() != Axis.Y).toArray(Direction[]::new))
			{
				int y = (dir.getOpposite().get2DDataValue() * 90);
				ladderStateBuilder.part().modelFile(vineModel).uvLock(true).rotationX(0).rotationY(y % 360).addModel().condition(LadderBlock.FACING, dir).end();
			}

			this.item(block.asItem(), true);
		}

		public void pressurePlate(PressurePlateBlock block, ResourceLocation texture, @Nullable String renderType)
		{
			BlockModelBuilder pressurePlate = models().pressurePlate(name(block), texture);
			BlockModelBuilder pressurePlateDown = models().pressurePlateDown(name(block) + "_down", texture);

			if (renderType != null)
			{
				pressurePlate.renderType(renderType);
				pressurePlateDown.renderType(renderType);
			}

			pressurePlate(block, pressurePlate, pressurePlateDown);
		}

		public void pressurePlate(PressurePlateBlock block, ModelFile pressurePlate, ModelFile pressurePlateDown)
		{
			getVariantBuilder(block).partialState().with(PressurePlateBlock.POWERED, true).addModels(new ConfiguredModel(pressurePlateDown)).partialState().with(PressurePlateBlock.POWERED, false).addModels(new ConfiguredModel(pressurePlate));
		}

		public void blockEntity(Block block, Block parent)
		{
			this.simpleBlock(block, this.models().withExistingParent(this.name(block), this.key(parent)));
			this.itemModels().withExistingParent(this.name(block), this.key(Blocks.CHEST));
		}

		public void leaves(LeavesBlock block, String renderType)
		{
			var texture = this.blockTexture(block);
			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("leaves")).texture("all", texture);

			if (renderType != null)
				model.renderType(renderType);

			this.simpleBlock(block, model);
		}

		public void carpet(Block block, Block parent)
		{
			var texture = this.blockTexture(parent);
			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("carpet")).texture("wool", texture).texture("particle", texture);

			this.simpleBlock(block, model);
		}

		public static final Direction[] HORIZONTAL = Arrays.stream(Direction.values()).filter(d -> d.getAxis() != Axis.Y).toArray(Direction[]::new);

		public void furniture(Block chair, Block table, Block parent)
		{
			var texture = this.blockTexture(parent);

			// Chair
			var chairModel = this.models().withExistingParent(this.name(chair), this.blockFolder("template/chair_base")).texture("texture", texture);
			var chairStateBuilder = this.getMultipartBuilder(chair);
			for (Direction dir : HORIZONTAL)
			{
				int y = (dir.getOpposite().get2DDataValue() * 90);
				chairStateBuilder.part().modelFile(chairModel).rotationX(0).rotationY(y % 360).addModel().condition(ChairBlock.FACING, dir).end();
			}

			// Table item
			this.models().withExistingParent(this.name(table), this.blockFolder("template/table_base")).texture("texture", texture);

			// Table block
			var tableTop = this.models().withExistingParent(this.name(table) + "_top", this.blockFolder("template/table_top")).texture("texture", texture);
			var tableCorner = this.models().withExistingParent(this.name(table) + "_corner_nw", this.blockFolder("template/table_corner_nw")).texture("texture", texture);
			var tableEdge = this.models().withExistingParent(this.name(table) + "_edge_n", this.blockFolder("template/table_edge_n")).texture("texture", texture);
			var tableLeg = this.models().withExistingParent(this.name(table) + "_leg_nw", this.blockFolder("template/table_leg_nw")).texture("texture", texture);

			var multiPart = this.getMultipartBuilder(table);
			multiPart.part().modelFile(tableTop).addModel();

			multiPart.part().modelFile(tableLeg).addModel().condition(TableBlock.NORTH, false).condition(TableBlock.WEST, false);
			multiPart.part().modelFile(tableCorner).addModel().condition(TableBlock.NW, true);
			multiPart.part().modelFile(tableEdge).addModel().condition(TableBlock.NORTH, true);

			multiPart.part().modelFile(tableLeg).uvLock(true).rotationY(90).addModel().condition(TableBlock.EAST, false).condition(TableBlock.NORTH, false);
			multiPart.part().modelFile(tableCorner).uvLock(true).rotationY(90).addModel().condition(TableBlock.NE, true);
			multiPart.part().modelFile(tableEdge).uvLock(true).rotationY(90).addModel().condition(TableBlock.EAST, true);

			multiPart.part().modelFile(tableLeg).uvLock(true).rotationY(180).addModel().condition(TableBlock.SOUTH, false).condition(TableBlock.EAST, false);
			multiPart.part().modelFile(tableCorner).uvLock(true).rotationY(180).addModel().condition(TableBlock.SE, true);
			multiPart.part().modelFile(tableEdge).uvLock(true).rotationY(180).addModel().condition(TableBlock.SOUTH, true);

			multiPart.part().modelFile(tableLeg).uvLock(true).rotationY(270).addModel().condition(TableBlock.WEST, false).condition(TableBlock.SOUTH, false);
			multiPart.part().modelFile(tableCorner).uvLock(true).rotationY(270).addModel().condition(TableBlock.SW, true);
			multiPart.part().modelFile(tableEdge).uvLock(true).rotationY(270).addModel().condition(TableBlock.WEST, true);
		}

		public void redDragonEgg()
		{
			Block block = RediscoveredBlocks.red_dragon_egg;
			var texture = this.blockTexture(block);
			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("dragon_egg")).texture("all", texture).texture("particle", texture);

			this.simpleBlock(block, model);
		}

		/*public void appleLeaves(AppleLeavesBlock block, String renderType)
		{
			var texture = this.blockTexture(block);
			var model = this.models().withExistingParent(this.name(block), this.mcTexLoc("leaves")).texture("all", texture);
			var matureModel = this.models().withExistingParent(this.name(block) + "_mature", this.mcTexLoc("leaves")).texture("all", this.extend(texture, "_mature"));
			var goldenModel = this.models().withExistingParent(this.name(block) + "_golden", this.mcTexLoc("leaves")).texture("all", this.extend(texture, "_golden"));
		
			if (renderType != null)
			{
				model.renderType(renderType);
				matureModel.renderType(renderType);
				goldenModel.renderType(renderType);
			}
		
			getVariantBuilder(block).forAllStatesExcept(state -> ConfiguredModel.builder().modelFile(state.getValue(AppleLeavesBlock.MATURE) ? (state.getValue(AppleLeavesBlock.GOLDEN) ? goldenModel : matureModel) : model).build(), LeavesBlock.DISTANCE, LeavesBlock.PERSISTENT, LeavesBlock.WATERLOGGED, AppleLeavesBlock.CAN_MATURE);
		}*/

		public BlockModelBuilder cube(Block block)
		{
			return this.cube(block, blockTexture(block));
		}

		public BlockModelBuilder cube(Block block, ResourceLocation tex)
		{
			return models().cubeAll(name(block), tex);
		}

		public void bookshelf(Block block, Block plank)
		{
			var plankTex = blockTexture(plank);
			var model = this.models().cubeBottomTop(this.name(block), blockTexture(block), plankTex, plankTex);
			this.simpleBlock(block, model);
		}

		@Override
		public void simpleBlock(Block block)
		{
			this.simpleBlock(block, (String) null);
		}

		public void simpleBlock(Block block, @Nullable String renderType)
		{
			var model = this.cubeAll(block);

			if (renderType != null && model instanceof BlockModelBuilder b)
				b.renderType(renderType);

			super.simpleBlock(block, model);
		}

		private ResourceLocation key(Block block)
		{
			return BuiltInRegistries.BLOCK.getKey(block);
		}

		private String name(Block block)
		{
			return key(block).getPath();
		}

		private ResourceLocation key(Item item)
		{
			return BuiltInRegistries.ITEM.getKey(item);
		}

		private String name(Item item)
		{
			return key(item).getPath();
		}

		public ResourceLocation itemFolder(String key)
		{
			return this.modLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}

		private ResourceLocation extend(ResourceLocation rl, String suffix)
		{
			return new ResourceLocation(rl.getNamespace(), rl.getPath() + suffix);
		}

		public ResourceLocation blockFolder(String key)
		{
			return this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + key);
		}

		public ResourceLocation mcBlockFolder(String key)
		{
			return this.mcLoc(ModelProvider.BLOCK_FOLDER + "/" + key);
		}

		public ItemModelBuilder item(ItemLike item, boolean blockFolder)
		{
			if (item == null || item == Blocks.AIR.asItem() || item.asItem() == null)
				return null;

			return item(item, BuiltInRegistries.ITEM.getKey(item.asItem()), blockFolder);
		}

		public ItemModelBuilder item(ItemLike item, ResourceLocation texture, boolean blockFolder)
		{
			if (item == null || item == Blocks.AIR.asItem())
				return null;

			String dir = blockFolder ? ModelProvider.BLOCK_FOLDER : ModelProvider.ITEM_FOLDER;
			var key = BuiltInRegistries.ITEM.getKey(item.asItem());
			return this.itemModels().getBuilder(key.toString()).parent(new ModelFile.UncheckedModelFile(GENERATED)).texture("layer0", new ResourceLocation(texture.getNamespace(), dir + "/" + texture.getPath()));
		}

	}
}
