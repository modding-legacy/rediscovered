package com.legacy.rediscovered.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import org.jetbrains.annotations.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.BaseFakeFireBlock;
import com.legacy.rediscovered.block.ChairBlock;
import com.legacy.rediscovered.block.DirtSlabBlock;
import com.legacy.rediscovered.block.GrassSlabBlock;
import com.legacy.rediscovered.block.ShallowDirtSlabBlock;
import com.legacy.rediscovered.block.TableBlock;
import com.legacy.rediscovered.registry.RediscoveredBiomes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredDamageTypes;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredPaintings;
import com.legacy.rediscovered.registry.RediscoveredStructures;
import com.legacy.rediscovered.world.structure.IndevHouseStructure;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.DamageTypeTagsProvider;
import net.minecraft.data.tags.EntityTypeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.PaintingVariantTagsProvider;
import net.minecraft.data.tags.PoiTypeTagsProvider;
import net.minecraft.data.tags.StructureTagsProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.PaintingVariantTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.village.poi.PoiTypes;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.TallFlowerBlock;
import net.minecraft.world.level.block.WallBlock;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class RediscoveredTagProv
{
	public static class BlockProv extends BlockTagsProvider
	{
		public BlockProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			vanilla();
			forge();
			rediscovered();
		}

		void vanilla()
		{
			addMatching(BlockTags.FLOWER_POTS, b -> b instanceof FlowerPotBlock);
			addMatching(BlockTags.SLABS, b -> b instanceof SlabBlock);
			addMatching(BlockTags.STAIRS, b -> b instanceof StairBlock);
			addMatching(BlockTags.WALLS, b -> b instanceof WallBlock);
			addMatching(BlockTags.SMALL_FLOWERS, b -> b instanceof FlowerBlock);
			addMatching(BlockTags.TALL_FLOWERS, b -> b instanceof TallFlowerBlock);
			addMatching(BlockTags.VALID_SPAWN, b -> b instanceof GrassBlock);

			this.tag(BlockTags.LEAVES).add(RediscoveredBlocks.ancient_cherry_leaves);
			this.tag(BlockTags.SAPLINGS).add(RediscoveredBlocks.ancient_cherry_sapling);

			this.tag(BlockTags.NEEDS_STONE_TOOL).add(RediscoveredBlocks.gear, RediscoveredBlocks.nether_reactor_core);
			this.tag(BlockTags.NEEDS_IRON_TOOL).add(RediscoveredBlocks.ruby_ore, RediscoveredBlocks.deepslate_ruby_ore, RediscoveredBlocks.ruby_block);
			this.tag(BlockTags.NEEDS_DIAMOND_TOOL).add(RediscoveredBlocks.ancient_crying_obsidian, RediscoveredBlocks.glowing_obsidian);

			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(RediscoveredBlocks.ruby_ore, RediscoveredBlocks.deepslate_ruby_ore, RediscoveredBlocks.ruby_block, RediscoveredBlocks.ancient_crying_obsidian, RediscoveredBlocks.glowing_obsidian, RediscoveredBlocks.obsidian_bulb, RediscoveredBlocks.gear, RediscoveredBlocks.rotational_converter, RediscoveredBlocks.nether_reactor_core, RediscoveredBlocks.large_bricks, RediscoveredBlocks.large_brick_stairs, RediscoveredBlocks.large_brick_slab, RediscoveredBlocks.large_brick_wall, RediscoveredBlocks.brittle_packed_mud, RediscoveredBlocks.brittle_mud_bricks);
			this.tag(BlockTags.MINEABLE_WITH_AXE).add(RediscoveredBlocks.spikes);
			addMatching(BlockTags.MINEABLE_WITH_AXE, b -> b instanceof ChairBlock || b instanceof TableBlock);
			addMatching(BlockTags.MINEABLE_WITH_SHOVEL, b -> b instanceof DirtSlabBlock || b instanceof GrassSlabBlock || b instanceof ShallowDirtSlabBlock);

			this.tag(BlockTags.DRAGON_IMMUNE).add(RediscoveredBlocks.red_dragon_egg, RediscoveredBlocks.ancient_crying_obsidian, RediscoveredBlocks.glowing_obsidian, RediscoveredBlocks.obsidian_bulb, RediscoveredBlocks.dragon_altar);
			this.tag(BlockTags.WITHER_IMMUNE).add(RediscoveredBlocks.dragon_altar);
			this.tag(BlockTags.FEATURES_CANNOT_REPLACE).add(RediscoveredBlocks.dragon_altar);
			this.tag(BlockTags.LAVA_POOL_STONE_CANNOT_REPLACE).add(RediscoveredBlocks.dragon_altar);

			this.tag(BlockTags.BEACON_BASE_BLOCKS).add(RediscoveredBlocks.ruby_block);

			this.tag(BlockTags.WOOL).add(RediscoveredBlocks.bright_green_wool, RediscoveredBlocks.rose_wool, RediscoveredBlocks.lavender_wool, RediscoveredBlocks.sky_blue_wool, RediscoveredBlocks.slate_blue_wool, RediscoveredBlocks.spring_green_wool);
			this.tag(BlockTags.WOOL_CARPETS).add(RediscoveredBlocks.bright_green_carpet, RediscoveredBlocks.rose_carpet, RediscoveredBlocks.lavender_carpet, RediscoveredBlocks.sky_blue_carpet, RediscoveredBlocks.slate_blue_carpet, RediscoveredBlocks.spring_green_carpet);

			this.addMatching(BlockTags.REPLACEABLE, b -> b.defaultBlockState().canBeReplaced());
			this.addMatching(BlockTags.FIRE, b -> b instanceof BaseFakeFireBlock);
			this.tag(BlockTags.PIGLIN_REPELLENTS).add(RediscoveredBlocks.fake_soul_fire);

			this.tag(BlockTags.PORTALS).add(RediscoveredBlocks.skylands_portal);

			this.tag(BlockTags.MUSHROOM_GROW_BLOCK).add(RediscoveredBlocks.podzol_slab, RediscoveredBlocks.mycelium_slab);
			addMatching(BlockTags.DIRT, b -> b instanceof DirtSlabBlock || b instanceof GrassSlabBlock);
		}

		void forge()
		{
			this.tag(RediscoveredTags.Blocks.STORAGE_BLOCKS_RUBY).add(RediscoveredBlocks.ruby_block);
			this.tag(RediscoveredTags.Blocks.ORES_RUBY).add(RediscoveredBlocks.ruby_ore);

			this.tag(Tags.Blocks.STORAGE_BLOCKS).addTag(RediscoveredTags.Blocks.STORAGE_BLOCKS_RUBY);
			this.tag(Tags.Blocks.ORES).addTag(RediscoveredTags.Blocks.ORES_RUBY);
		}

		void rediscovered()
		{
			addMatching(RediscoveredTags.Blocks.CHAIRS, b -> b instanceof ChairBlock);
			addMatching(RediscoveredTags.Blocks.TABLES, b -> b instanceof TableBlock);

			this.tag(RediscoveredTags.Blocks.NETHER_REACTOR_STRONG_POWER_BLOCK).addTag(Tags.Blocks.STORAGE_BLOCKS_NETHERITE);
			this.tag(RediscoveredTags.Blocks.NETHER_REACTOR_POWER_BLOCK).addTag(Tags.Blocks.STORAGE_BLOCKS_GOLD).addTag(RediscoveredTags.Blocks.NETHER_REACTOR_STRONG_POWER_BLOCK);
			this.tag(RediscoveredTags.Blocks.NETHER_REACTOR_BASE_BLOCK).addTag(Tags.Blocks.COBBLESTONE_NORMAL).addTag(Tags.Blocks.COBBLESTONE_DEEPSLATE).add(Blocks.BLACKSTONE, RediscoveredBlocks.glowing_obsidian);

			this.tag(RediscoveredTags.Blocks.SKYLANDS_PORTAL_FRAME).add(RediscoveredBlocks.large_bricks, Blocks.BRICKS);
			this.tag(RediscoveredTags.Blocks.SKYLANDS_PORTAL_BASE).add(RediscoveredBlocks.glowing_obsidian, RediscoveredBlocks.dragon_altar);

			this.tag(RediscoveredTags.Blocks.PIGMAN_VILLAGE_FLOWERS).add(RediscoveredBlocks.rose, RediscoveredBlocks.paeonia);
			this.tag(RediscoveredTags.Blocks.PIGMAN_VILLAGE_RARE_FLOWERS).add(RediscoveredBlocks.cyan_rose);

			this.tag(RediscoveredTags.Blocks.SKYLANDS_POOL_REPLACEABLE).addTag(BlockTags.OVERWORLD_CARVER_REPLACEABLES).addTag(Tags.Blocks.ORES);
		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(RediscoveredMod.MODID) && condition.apply(block));
		}

		private void addMatching(TagKey<Block> blockTag, Function<Block, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(blockTag)::add);
		}

		@Override
		public String getName()
		{
			return RediscoveredMod.MODID + " Block Tags";
		}
	}

	public static class ItemProv extends ItemTagsProvider
	{
		public ItemProv(DataGenerator gen, CompletableFuture<TagLookup<Block>> blockTagProv, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(gen.getPackOutput(), lookup, blockTagProv, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			this.rediscovered();
			vanilla();
			forge();
		}

		void rediscovered()
		{
			this.copy(RediscoveredTags.Blocks.CHAIRS, RediscoveredTags.Items.CHAIRS);
			this.copy(RediscoveredTags.Blocks.TABLES, RediscoveredTags.Items.TABLES);

			this.tag(RediscoveredTags.Items.PLATE_ARMOR).add(RediscoveredItems.plate_boots, RediscoveredItems.plate_leggings, RediscoveredItems.plate_chestplate, RediscoveredItems.plate_helmet);
			this.tag(RediscoveredTags.Items.STUDDED_ARMOR).add(RediscoveredItems.studded_boots, RediscoveredItems.studded_leggings, RediscoveredItems.studded_chestplate, RediscoveredItems.studded_helmet);
			this.tag(RediscoveredTags.Items.QUIVERS).add(RediscoveredItems.quiver);
			this.tag(RediscoveredTags.Items.QUIVER_APPLICABLE).addTag(Tags.Items.ARMORS_CHESTPLATES);
			this.tag(RediscoveredTags.Items.QUIVER_INAPPLICABLE).addTag(RediscoveredTags.Items.QUIVERS);
			this.tag(RediscoveredTags.Items.QUIVER_USER).addTag(Tags.Items.TOOLS_BOWS).addTag(Tags.Items.TOOLS_CROSSBOWS);
			this.tag(RediscoveredTags.Items.QUIVER_AMMO).addTag(ItemTags.ARROWS).add(Items.FIREWORK_ROCKET);
			this.tag(RediscoveredTags.Items.QUIVER_AUTO_ADD).addTag(ItemTags.ARROWS);

			this.tag(RediscoveredTags.Items.PIGMAN_GUARD_PAYMENTS).addTag(RediscoveredTags.Items.GEMS_RUBY);
			this.tag(RediscoveredTags.Items.RANA_CURRENCY).add(RediscoveredBlocks.cyan_rose.asItem());

			this.tag(RediscoveredTags.Items.TAILOR_ARMOR_STAND_HELMETS).add(RediscoveredItems.studded_helmet, Items.LEATHER_HELMET, Items.CHAINMAIL_HELMET);
			this.tag(RediscoveredTags.Items.TAILOR_ARMOR_STAND_CHESTPLATES).add(RediscoveredItems.studded_chestplate, Items.LEATHER_CHESTPLATE, Items.CHAINMAIL_CHESTPLATE);
			this.tag(RediscoveredTags.Items.TAILOR_ARMOR_STAND_LEGGINGS).add(RediscoveredItems.studded_leggings, Items.LEATHER_LEGGINGS, Items.CHAINMAIL_LEGGINGS);
			this.tag(RediscoveredTags.Items.TAILOR_ARMOR_STAND_BOOTS).add(RediscoveredItems.studded_boots, Items.LEATHER_BOOTS, Items.CHAINMAIL_BOOTS);

			this.tag(RediscoveredTags.Items.TAILOR_UNSELLABLE_FLOWERS).add(Blocks.WITHER_ROSE.asItem(), Blocks.TORCHFLOWER.asItem());

			this.tag(RediscoveredTags.Items.BOWYER_UNSELLABLE_ARROWS);

			this.tag(RediscoveredTags.Items.BRICK_PYRAMID_POTTERY_SHERDS).add(Items.FRIEND_POTTERY_SHERD, Items.HEARTBREAK_POTTERY_SHERD, Items.PRIZE_POTTERY_SHERD, Items.ARCHER_POTTERY_SHERD, Items.BLADE_POTTERY_SHERD);

			this.tag(RediscoveredTags.Items.DRAGON_ARMOR_MATERIALS).addTag(ItemTags.TRIM_MATERIALS);

			this.tag(RediscoveredTags.Items.QUIVER_REPAIR_MATERIALS).addTag(Tags.Items.LEATHER);
			this.tag(RediscoveredTags.Items.STUDDED_ARMOR_REPAIR_MATERIALS).addTag(Tags.Items.INGOTS_IRON);
			this.tag(RediscoveredTags.Items.PLATE_ARMOR_REPAIR_MATERIALS).addTag(Tags.Items.INGOTS_IRON);
		}

		void vanilla()
		{
			this.copy(BlockTags.SLABS, ItemTags.SLABS);
			this.copy(BlockTags.STAIRS, ItemTags.STAIRS);
			this.copy(BlockTags.WALLS, ItemTags.WALLS);
			this.copy(BlockTags.SMALL_FLOWERS, ItemTags.SMALL_FLOWERS);
			this.copy(BlockTags.WOOL, ItemTags.WOOL);
			this.copy(BlockTags.WOOL_CARPETS, ItemTags.WOOL_CARPETS);
			this.copy(BlockTags.SAPLINGS, ItemTags.SAPLINGS);

			this.tag(ItemTags.FISHES).add(RediscoveredItems.raw_fish, RediscoveredItems.cooked_fish);
			this.tag(ItemTags.ARROWS).add(RediscoveredItems.purple_arrow);
			this.tag(ItemTags.MUSIC_DISCS).add(RediscoveredItems.music_disc_calm4);
			this.tag(ItemTags.BEACON_PAYMENT_ITEMS).add(RediscoveredItems.ruby);

			this.tag(ItemTags.FREEZE_IMMUNE_WEARABLES).addTag(RediscoveredTags.Items.STUDDED_ARMOR);
			this.tag(ItemTags.TRIMMABLE_ARMOR).addTag(RediscoveredTags.Items.STUDDED_ARMOR).addTag(RediscoveredTags.Items.PLATE_ARMOR);
			this.tag(ItemTags.TRIM_TEMPLATES).add(RediscoveredItems.draconic_trim);
			this.tag(ItemTags.TRIM_MATERIALS).add(RediscoveredItems.ruby);
		}

		void forge()
		{
			this.tag(RediscoveredTags.Items.GEMS_RUBY).add(RediscoveredItems.ruby);
			this.copy(RediscoveredTags.Blocks.STORAGE_BLOCKS_RUBY, RediscoveredTags.Items.STORAGE_BLOCKS_RUBY);
			this.copy(RediscoveredTags.Blocks.ORES_RUBY, RediscoveredTags.Items.ORES_RUBY);

			this.tag(Tags.Items.GEMS).addTag(RediscoveredTags.Items.GEMS_RUBY);

			this.tag(Tags.Items.ARMORS_HELMETS).add(RediscoveredItems.studded_helmet, RediscoveredItems.plate_helmet);
			this.tag(Tags.Items.ARMORS_CHESTPLATES).add(RediscoveredItems.studded_chestplate, RediscoveredItems.plate_chestplate).addTag(RediscoveredTags.Items.QUIVERS);
			;
			this.tag(Tags.Items.ARMORS_LEGGINGS).add(RediscoveredItems.studded_leggings, RediscoveredItems.plate_leggings);
			this.tag(Tags.Items.ARMORS_BOOTS).add(RediscoveredItems.studded_boots, RediscoveredItems.plate_boots);
		}

		@Override
		public String getName()
		{
			return RediscoveredMod.MODID + " Item Tags";
		}
	}

	public static class EntityProv extends EntityTypeTagsProvider
	{
		public EntityProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		@SuppressWarnings("unchecked")
		protected void addTags(Provider prov)
		{
			this.tag(RediscoveredTags.Entities.PLATE_ARMOR_SPAWNS).add(EntityType.ZOMBIE, EntityType.HUSK, EntityType.SKELETON, EntityType.STRAY, RediscoveredEntityTypes.ZOMBIE_PIGMAN);

			this.tag(RediscoveredTags.Entities.MD3).add(RediscoveredEntityTypes.STEVE, RediscoveredEntityTypes.BLACK_STEVE, RediscoveredEntityTypes.BEAST_BOY, RediscoveredEntityTypes.RANA);

			this.tag(RediscoveredTags.Entities.PIGMAN).add(RediscoveredEntityTypes.PIGMAN, RediscoveredEntityTypes.MELEE_PIGMAN, RediscoveredEntityTypes.RANGED_PIGMAN);
			this.tag(RediscoveredTags.Entities.PIGMAN_VILLAGE_GUARDS).add(RediscoveredEntityTypes.MELEE_PIGMAN, RediscoveredEntityTypes.RANGED_PIGMAN);
			this.tag(RediscoveredTags.Entities.PIGMAN_ALLIES).addTags(RediscoveredTags.Entities.PIGMAN, RediscoveredTags.Entities.MD3);

			this.tag(RediscoveredTags.Entities.GOLDEN_AURA_APPLICABLE).add(EntityType.VILLAGER, EntityType.PIGLIN, EntityType.PIGLIN_BRUTE, EntityType.HOGLIN, RediscoveredEntityTypes.PIGMAN, RediscoveredEntityTypes.MELEE_PIGMAN, RediscoveredEntityTypes.RANGED_PIGMAN);
			this.tag(RediscoveredTags.Entities.CRIMSON_VEIL_AFFECTED).add(EntityType.PIGLIN, EntityType.PIGLIN_BRUTE, EntityType.HOGLIN);

			this.tag(RediscoveredTags.Entities.BRICK_PYRAMID_SPAWNER).add(EntityType.ZOMBIE, EntityType.SKELETON);

			this.tag(RediscoveredTags.Entities.BECOMES_ZOMBIE_HORSE_IN_ZOMBIE_VILLAGE).add(EntityType.HORSE, EntityType.DONKEY, EntityType.MULE, EntityType.COW, EntityType.PIG, EntityType.SHEEP);

			this.tag(RediscoveredTags.Entities.TARGETS_SCARECROW).add(EntityType.ZOMBIE, EntityType.ZOMBIE_VILLAGER, EntityType.DROWNED, EntityType.HUSK, EntityType.ZOGLIN, EntityType.HOGLIN, EntityType.GIANT, EntityType.GOAT, EntityType.RAVAGER);
			this.addOptional(RediscoveredTags.Entities.TARGETS_SCARECROW, "alexsmobs", "tusklin");

			this.addOptional(RediscoveredTags.Entities.FEARS_SCARECROW, "alexsmobs", "seagull", "crow");

			this.tag(RediscoveredTags.Entities.IGNORES_SCARECROW).add(EntityType.POLAR_BEAR, EntityType.PANDA, EntityType.SKELETON_HORSE, EntityType.ZOMBIE_HORSE, EntityType.TURTLE, EntityType.TADPOLE, EntityType.WANDERING_TRADER, EntityType.VILLAGER, RediscoveredEntityTypes.RED_DRAGON_OFFSPRING);
			this.tag(RediscoveredTags.Entities.IGNORES_SCARECROW).addTags(RediscoveredTags.Entities.PIGMAN, RediscoveredTags.Entities.MD3, Tags.EntityTypes.BOSSES, EntityTypeTags.RAIDERS);
			this.addOptional(RediscoveredTags.Entities.IGNORES_SCARECROW, "alexsmobs", "grizzly_bear", "cockroach", "tiger");
			this.addOptionalTags(RediscoveredTags.Entities.IGNORES_SCARECROW, "irons_spellbooks", "summons");

			this.tag(EntityTypeTags.ARROWS).add(RediscoveredEntityTypes.PURPLE_ARROW);
			this.tag(EntityTypeTags.FALL_DAMAGE_IMMUNE).add(RediscoveredEntityTypes.RED_DRAGON_BOSS, RediscoveredEntityTypes.RED_DRAGON_OFFSPRING);
			this.tag(EntityTypeTags.DISMOUNTS_UNDERWATER).add(RediscoveredEntityTypes.RED_DRAGON_OFFSPRING);
			this.tag(EntityTypeTags.ZOMBIES).add(RediscoveredEntityTypes.ZOMBIE_PIGMAN);

			this.tag(Tags.EntityTypes.BOSSES).add(RediscoveredEntityTypes.RED_DRAGON_BOSS);

			this.tag(new ResourceLocation("aether", "pigs")).addTag(RediscoveredTags.Entities.PIGMAN);
		}

		private IntrinsicTagAppender<EntityType<?>> tag(ResourceLocation tag)
		{
			return this.tag(TagKey.create(Registries.ENTITY_TYPE, tag));
		}

		private IntrinsicTagAppender<EntityType<?>> addOptional(TagKey<EntityType<?>> tag, String modid, String... values)
		{
			var appender = this.tag(tag);
			for (String v : values)
				appender.addOptional(new ResourceLocation(modid, v));
			return appender;
		}

		private IntrinsicTagAppender<EntityType<?>> addOptionalTags(TagKey<EntityType<?>> tag, String modid, String... values)
		{
			var appender = this.tag(tag);
			for (String v : values)
				appender.addOptionalTag(new ResourceLocation(modid, v));
			return appender;
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		@Override
		public String getName()
		{
			return RediscoveredMod.MODID + " EntityType Tags";
		}
	}

	public static class BiomeProv extends BiomeTagsProvider
	{
		public BiomeProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			rediscovered();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		@SuppressWarnings("unchecked")
		private void rediscovered()
		{
			this.tag(RediscoveredTags.Biomes.IS_SKYLANDS).add(RediscoveredBiomes.SKYLANDS.getKey(), RediscoveredBiomes.COLD_SKYLANDS.getKey(), RediscoveredBiomes.WOODED_SKYLANDS.getKey(), RediscoveredBiomes.RAINFOREST.getKey());

			this.tag(RediscoveredTags.Biomes.HAS_PIGMAN_VILLAGE).add(RediscoveredBiomes.SKYLANDS.getKey(), RediscoveredBiomes.WOODED_SKYLANDS.getKey());
			this.tag(RediscoveredTags.Biomes.HAS_BRICK_PYRAMID).add(RediscoveredBiomes.RAINFOREST.getKey(), RediscoveredBiomes.WOODED_SKYLANDS.getKey());
			this.tag(RediscoveredTags.Biomes.HAS_OBSIDIAN_WALL).addTag(RediscoveredTags.Biomes.IS_SKYLANDS);
			this.tag(RediscoveredTags.Biomes.HAS_DEFAULT_INDEV_HOUSE).add(RediscoveredBiomes.SKYLANDS.getKey(), RediscoveredBiomes.COLD_SKYLANDS.getKey());
			this.tag(RediscoveredTags.Biomes.HAS_MOSSY_INDEV_HOUSE).add(RediscoveredBiomes.RAINFOREST.getKey());
			this.tag(RediscoveredTags.Biomes.HAS_CHERRY_INDEV_HOUSE).add(RediscoveredBiomes.WOODED_SKYLANDS.getKey());

			this.tag(RediscoveredTags.Biomes.HAS_RUBY_ORE).addTag(BiomeTags.IS_JUNGLE).addTag(BiomeTags.IS_SAVANNA);
			this.tag(RediscoveredTags.Biomes.HAS_FISH_SPAWNS).addTags(BiomeTags.IS_OCEAN, BiomeTags.IS_RIVER);
		}

		private void vanilla()
		{
		}

		private void forge()
		{
		}

		@Override
		public String getName()
		{
			return RediscoveredMod.MODID + " Biome Tags";
		}
	}

	public static class PoiProv extends PoiTypeTagsProvider
	{
		public PoiProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			this.tag(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION).add(PoiTypes.TOOLSMITH, PoiTypes.FLETCHER, PoiTypes.LIBRARIAN, PoiTypes.SHEPHERD, PoiTypes.CLERIC);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		@Override
		public String getName()
		{
			return RediscoveredMod.MODID + " PoiType Tags";
		}
	}

	public static class StructureProv extends StructureTagsProvider
	{
		public StructureProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			this.tag(RediscoveredTags.Structures.INDEV_HOUSE).add(RediscoveredStructures.INDEV_HOUSE.getStructure(IndevHouseStructure.Type.DEFAULT.getSerializedName()).getKey(), RediscoveredStructures.INDEV_HOUSE.getStructure(IndevHouseStructure.Type.MOSSY.getSerializedName()).getKey(), RediscoveredStructures.INDEV_HOUSE.getStructure(IndevHouseStructure.Type.CHERRY.getSerializedName()).getKey());
			this.tag(RediscoveredTags.Structures.PIGMAN_VILLAGE_AVOIDS).add(RediscoveredStructures.BRICK_PYRAMID.getStructure().getKey());
			this.tag(RediscoveredTags.Structures.OBSIDIAN_WALL_AVOIDS).add(RediscoveredStructures.BRICK_PYRAMID.getStructure().getKey(), RediscoveredStructures.PIGMAN_VILLAGE.getStructure().getKey());
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			return super.run(cache);
		}

		@Override
		public String getName()
		{
			return RediscoveredMod.MODID + " Structure Tags";
		}
	}

	public static class DamageTypeProv extends DamageTypeTagsProvider
	{
		public DamageTypeProv(DataGenerator generatorIn, @Nullable ExistingFileHelper existingFileHelper, CompletableFuture<Provider> pLookupProvider)
		{
			super(generatorIn.getPackOutput(), pLookupProvider, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider provider)
		{
			this.tag(RediscoveredTags.DamageTypes.IS_DEATHLY).add(DamageTypes.WITHER, DamageTypes.WITHER_SKULL);
			this.tag(RediscoveredTags.DamageTypes.IS_SPIKES).add(RediscoveredDamageTypes.SPIKES.getKey());

			this.tag(DamageTypeTags.BYPASSES_SHIELD).addTag(RediscoveredTags.DamageTypes.IS_SPIKES);
			this.tag(DamageTypeTags.BYPASSES_ARMOR).addTag(RediscoveredTags.DamageTypes.IS_SPIKES);
		}
	}

	public static class PaintingVariantProv extends PaintingVariantTagsProvider
	{
		public PaintingVariantProv(PackOutput output, CompletableFuture<Provider> lookupProvider, @Nullable ExistingFileHelper existingFileHelper)
		{
			super(output, lookupProvider, RediscoveredMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider lookupProvider)
		{
			this.tag(PaintingVariantTags.PLACEABLE).add(RediscoveredPaintings.FOG.getKey());
		}
	}
}
