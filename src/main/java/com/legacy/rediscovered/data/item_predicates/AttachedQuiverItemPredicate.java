package com.legacy.rediscovered.data.item_predicates;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.item.util.QuiverData;
import com.mojang.serialization.Codec;

import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.common.advancements.critereon.ICustomItemPredicate;

public class AttachedQuiverItemPredicate implements ICustomItemPredicate
{
	public static final AttachedQuiverItemPredicate INSTANCE = new AttachedQuiverItemPredicate();
	public static final Codec<AttachedQuiverItemPredicate> CODEC = Codec.unit(INSTANCE);

	private AttachedQuiverItemPredicate()
	{
	}

	@Override
	public boolean test(ItemStack stack)
	{
		return QuiverData.canAttachQuiver(stack) && AttachedItem.get(stack).map(attached -> attached.getAttached().is(RediscoveredTags.Items.QUIVERS)).orElse(Boolean.FALSE);
	}

	@Override
	public Codec<? extends ICustomItemPredicate> codec()
	{
		return CODEC;
	}
}
