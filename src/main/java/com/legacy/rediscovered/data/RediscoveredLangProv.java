package com.legacy.rediscovered.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.RedDragonEggBlock;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.gui.GuardPigmanInventoryScreen;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.entity.pigman.data.PigmanData;
import com.legacy.rediscovered.entity.pigman.data.PigmanData.Profession;
import com.legacy.rediscovered.item.RubyFluteItem;
import com.legacy.rediscovered.item.util.DragonArmorTrim;
import com.legacy.rediscovered.item.util.DragonArmorTrim.Decoration;
import com.legacy.rediscovered.registry.RediscoveredAttributes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredDamageTypes;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredEnchantments;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredPaintings;
import com.legacy.rediscovered.registry.RediscoveredStats;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;

import net.minecraft.Util;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.enchantment.Enchantment;
import net.neoforged.neoforge.common.data.LanguageProvider;

public class RediscoveredLangProv extends LanguageProvider
{
	public static final String GOLDEN_AURA_EFFECT = "potion." + RediscoveredMod.MODID + ".golden_aura_effect";
	public static final String CRIMSON_VEIL_EFFECT = "potion." + RediscoveredMod.MODID + ".crimson_veil_effect";

	public static final String DRAGON_ARMOR_SMITHING_TEMPLATE_CHAIN = Util.makeDescriptionId("item", RediscoveredMod.locate("smithing_template.dragon_smithing_template." + Decoration.CHAIN.getSerializedName()));
	public static final String DRAGON_ARMOR_SMITHING_TEMPLATE_PLATING = Util.makeDescriptionId("item", RediscoveredMod.locate("smithing_template.dragon_smithing_template." + Decoration.PLATING.getSerializedName()));
	public static final String DRAGON_ARMOR_SMITHING_TEMPLATE_INLAY = Util.makeDescriptionId("item", RediscoveredMod.locate("smithing_template.dragon_smithing_template." + Decoration.INLAY.getSerializedName()));
	public static final String DRAGON_ARMOR_SMITHING_TEMPLATE_APPLIES_TO = Util.makeDescriptionId("item", RediscoveredMod.locate("smithing_template.dragon_smithing_template.applies_to"));
	public static final String DRAGON_ARMOR_SMITHING_TEMPLATE_BASE_SLOT_DESCRIPTION = Util.makeDescriptionId("item", RediscoveredMod.locate("smithing_template.dragon_smithing_template.base_slot_description"));

	private final CompletableFuture<HolderLookup.Provider> lookup;

	public RediscoveredLangProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(packOutput, RediscoveredMod.MODID, "en_us");
		this.lookup = lookup;
	}

	@Override
	protected void addTranslations()
	{
		this.add(RediscoveredItems.music_disc_calm4, "Lost Music Disc");
		this.add(RediscoveredItems.music_disc_calm4.getDescriptionId() + ".desc", "Notch - Magnetic Circuit");

		this.add(RediscoveredBlocks.ruby_block, "Block of Ruby");

		this.add(RediscoveredItems.fish_bucket, "Bucket of Fish");

		this.addPainting(RediscoveredPaintings.FOG.getKey(), "Fog", "Silver_David");

		this.addDamageType(RediscoveredDamageTypes.SPIKES.getKey(), "%1$s was impaled by spikes", "%1$s landed in spikes whilst trying to escape %2$s");

		this.addPotion(RediscoveredEffects.GOLDEN_AURA);
		this.addPotion(RediscoveredEffects.CRIMSON_VEIL);

		this.add(GOLDEN_AURA_EFFECT, "Prevents zombification");
		this.add(CRIMSON_VEIL_EFFECT, "Grants a Nether atmosphere");

		this.addAttribute(RediscoveredAttributes.UNDEAD_DAMAGE_SCALING, "Undead Resistance");
		this.addAttribute(RediscoveredAttributes.CRIMSON_VEIL_DAMAGE_SCALING, "Damage Resistance");
		this.addAttribute(RediscoveredAttributes.EXPLOSION_RESISTANCE, "Explosion Resistance");
		this.addAttribute(RediscoveredAttributes.FIRE_RESISTANCE, "Fire Resistance");

		String pigmanId = RediscoveredEntityTypes.PIGMAN.getDescriptionId();
		this.add(pigmanId, Profession.METALWORKER, "Metalworker");
		this.add(pigmanId, Profession.BOWYER, "Bowyer");
		this.add(pigmanId, Profession.TECHNICIAN, "Technician");
		this.add(pigmanId, Profession.TAILOR, "Tailor");
		this.add(pigmanId, Profession.DOCTOR, "Doctor");

		this.addDefault(Registries.BLOCK, Map.of(RediscoveredBlocks.mini_dragon_pylon.builtInRegistryHolder().key().location(), "Dragon Pylon"));
		this.addDefault(Registries.STRUCTURE, Map.of());
		Map<Item, String> itemOverrides = Util.make(new HashMap<>(), m ->
		{
			String smithingTemplate = "Smithing Template";
			m.put(RediscoveredItems.draconic_trim, smithingTemplate);
			m.put(RediscoveredItems.dragon_armor_chain_smithing_template, smithingTemplate);
			m.put(RediscoveredItems.dragon_armor_plating_smithing_template, smithingTemplate);
			m.put(RediscoveredItems.dragon_armor_inlay_smithing_template, smithingTemplate);
		});

		this.addDefault(Registries.ITEM, itemOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));
		this.addDefault(Registries.ENTITY_TYPE, Map.of(RediscoveredEntityTypes.RED_DRAGON_OFFSPRING.builtInRegistryHolder().key().location(), "Red Dragon"));
		this.addDefault(Registries.BIOME, Map.of());
		this.addDefault(Registries.CHUNK_GENERATOR, Map.of());
		this.addDefault(Registries.ENCHANTMENT, Map.of());
		this.addDefault(Registries.CUSTOM_STAT, Map.of(RediscoveredStats.INTERACT_WITH_TABLE, "Interactions with Table"));
		this.addDefault(Registries.MOB_EFFECT, Map.of());
		this.addDefault(Registries.TRIM_PATTERN, Map.of());
		this.addDefault(Registries.TRIM_MATERIAL, Map.of());

		this.addEnchantDesc(RediscoveredEnchantments.RAPID_SHOT, "Quickly shoot arrows without needing to pull back the bow. They don't go very far though.");

		this.addAdvancement(RediscoveredAdvancementProv.Advancements.getRuby, "Red Like a Rose", "Obtain a Ruby");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.locatePortal, "Ancient Investigation", "Follow a Ruby Eye to the Trail Ruins to rediscover the Skylands Portal within");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.enterSkylands, "We Need to Go Higher!", "Enter the Skylands Portal");

		this.addAdvancement(RediscoveredAdvancementProv.Advancements.rideZombieHorse, "A Vengeful Steed", "Find and ride a Zombie Horse");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.shearRoseBush, "By Any Other Name", "Use Shears to get the Roses from a Rose Bush");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.allFlowers, "Something Old, New, And Blue", "Obtain all the forgotten flowers");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.craftScarecrow, "Without a Brain", "Craft a Scarecrow to keep animals away from your crops");

		this.addAdvancement(RediscoveredAdvancementProv.Advancements.craftQuiver, "Long Range Utility", "Craft a Quiver to hold your arrows");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.attachQuiver, "Ready For Action", "Attach a Quiver to a Chestplate with an Anvil");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.removeQuiver, "Back to Fashion", "Remove a Quiver from a Chestplate with a Grindstone");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.purpleArrowSnipe, "No Scope Needed", "Land a killing blow from as least 100 meters away");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.craftGears, "Spinning Power", "Craft a Gear and a Rotational Converter");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.spikySpkies, "Spiky Spikes", "Use Spikes to kill a Guardian");

		this.addAdvancement(RediscoveredAdvancementProv.Advancements.craftNetherReactor, "Reactive", "Craft a Nether Reactor Core");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.activateNetherReactor, "Pocket Biome", "Construct and activate a Nether Reactor");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.activateFullNetherReactor, "Lethal Region", "Construct and activate a fully powered Nether Reactor");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.curePigman, "An Ancient Ally", "Weaken and then cure a Zombie Pigman");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.curePigmanMelee, "The Loyal Guard", "Give a Zombie Pigman a sword while curing to encourage melee combat");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.curePigmanRanged, "Boar and Arrow", "Give a Zombie Pigman a bow while curing to encourage ranged combat");

		this.addAdvancement(RediscoveredAdvancementProv.Advancements.skylandsRoot, "Skylands", "A world far above the clouds");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.enterBrickPyramid, "Bygone Ruins", "Enter a Brick Pyramid");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.slayRedDragon, "Out With The Old...", "Slay the Red Dragon");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.hatchRedDragon, "In With The New", "Pour Dragon Breath over a Red Dragon Egg and wait for it to hatch");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.redDragonCatch, "Falling in Style", "Use a Ruby Flute to summon your Red Dragon for a mid-air catch");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.redDragonTeleport, "Trust the Process", "Fly below the Skylands and into the Overworld on your Red Dragon");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.summonRedDragon, "Calling The Storm", "Place 4 Dragon Pylons on the altar atop the Brick Pyramid to summon a Red Dragon");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.enterPigmanVillage, "Can They Fly?", "Enter a Pigman Village");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.tradeWithPigman, "A Different Currency", "Use rubies to trade with a Pigman");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.giveRanaGift, "Smells Just As Sweet", "Gift Rana a Cyan Rose");
		this.addAdvancement(RediscoveredAdvancementProv.Advancements.reachFarlands, "An Extensive Journey", "Venture off into the far lands");

		this.add(RediscoveredSounds.BLOCK_NETHER_REACTOR_ACTIVATE, "Nether Reactor activates");
		this.add(RediscoveredSounds.BLOCK_NETHER_REACTOR_IDLE, "Nether Reactor warbles");
		this.add(RediscoveredSounds.BLOCK_NETHER_REACTOR_DEACTIVATE, "Nether Reactor deactivates");

		this.add(RediscoveredSounds.BLOCK_RED_DRAGON_EGG_FERTILIZE, "Dragon Breath pours");
		this.add(RediscoveredSounds.BLOCK_RED_DRAGON_EGG_SHAKE, "Red Dragon Egg shakes");
		this.add(RediscoveredSounds.BLOCK_RED_DRAGON_EGG_HATCH, "Red Dragon Egg hatches");

		this.add(RediscoveredSounds.BLOCK_ROTATIONAL_CONVERTER_CLICK, "Rotational Converter clicks");

		this.add(RediscoveredSounds.BLOCK_TABLE_ADD_ITEM, "Item placed on table");
		this.add(RediscoveredSounds.BLOCK_TABLE_REMOVE_ITEM, "Item removed from table");

		this.add(RediscoveredSounds.ITEM_ARMOR_EQUIP_STUDDED, "Studded armor jingles");
		this.add(RediscoveredSounds.ITEM_ARMOR_EQUIP_PLATE, "Plate armor clanks");

		this.add(RediscoveredSounds.ENTITY_PIGMAN_IDLE, "Pigman oinks");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_HURT, "Pigman hurts");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_DEATH, "Pigman dies");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_ANGRY, "Pigman growls");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_SCARED, "Pigman cries");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_AGREE, "Pigman agrees");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_DISAGREE, "Pigman disagrees");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_ZOMBIFY, "Pigman converts to Zombie Pigman");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_METALWORKER, "Metalworker works");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_BOWYER, "Bowyer works");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_TECHNICIAN, "Technician works");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_TAILOR, "Tailor works");
		this.add(RediscoveredSounds.ENTITY_PIGMAN_WORK_DOCTOR, "Doctor works");

		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_IDLE, "Zombie Pigman groans");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_HURT, "Zombie Pigman hurts");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_DEATH, "Zombie Pigman dies");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_ANGRY, "Zombie Pigman grunts angrily");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_CURE, "Zombie Pigman snuffles");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_FINISH_CURE, "Zombie Pigman vociferates");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_SELECT_MELEE, "Zombie Pigman accepts melee weapon");
		this.add(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_SELECT_RANGED, "Zombie Pigman accepts ranged weapon");

		this.add(RediscoveredSounds.ENTITY_STEVE_HURT, "Steve hurts");
		this.add(RediscoveredSounds.ENTITY_STEVE_DEATH, "Steve dies");

		this.add(RediscoveredSounds.ENTITY_BLACK_STEVE_HURT, "Black Steve hurts");
		this.add(RediscoveredSounds.ENTITY_BLACK_STEVE_DEATH, "Black Steve dies");

		this.add(RediscoveredSounds.ENTITY_BEAST_BOY_HURT, "Beast Boy hurts");
		this.add(RediscoveredSounds.ENTITY_BEAST_BOY_DEATH, "Beast Boy dies");

		this.add(RediscoveredSounds.ENTITY_RANA_HURT, "Rana hurts");
		this.add(RediscoveredSounds.ENTITY_RANA_DEATH, "Rana dies");

		this.add(RediscoveredSounds.ENTITY_FISH_HURT, "Fish hurts");
		this.add(RediscoveredSounds.ENTITY_FISH_DEATH, "Fish dies");
		this.add(RediscoveredSounds.ENTITY_FISH_FLOP, "Fish flops");

		this.add(RediscoveredSounds.ENTITY_PYLON_BURST_DESTROYED, "Pylon Burst destroyed");
		this.add(RediscoveredSounds.ENTITY_PYLON_BURST_EXPLODE, "Pylon Burst explodes");

		this.add(RediscoveredSounds.ENTITY_DRAGON_PYLON_SHIELD_LOST, "Dragon Pylon drops shield");
		this.add(RediscoveredSounds.ENTITY_DRAGON_PYLON_HURT, "Dragon Pylon damaged");
		this.add(RediscoveredSounds.ENTITY_DRAGON_PYLON_DEATH, "Dragon Pylon destroyed");

		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_IDLE, "Red Dragon roars");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_IDLE_CALM, "Red Dragon huffs");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_HURT, "Red Dragon hurts");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_DEATH, "Red Dragon dies");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_FLAP, "Red Dragon flaps");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_SHIELD_DOWN, "Red Dragon roars angrily");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_BOLT_BALL_CHARGE, "Red Dragon charges");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_BOLT_BALL_SHOOT, "Red Dragon shoots");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_PREPARE_WIND_BLOW, "Red Dragon prepares winds");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_WIND_BLOW, "Harsh wind blows");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_SHED_BURSTS, "Dragon Pylon generates bursts");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_EQUIP_SADDLE, "Red Dragon saddle equips");
		this.add(RediscoveredSounds.ENTITY_RED_DRAGON_EQUIP_ARMOR, "Red Dragon armor equips");

		this.add(RedDragonEggBlock.HATCH_DISABLED_KEY, "Red Dragon hatching has been disabled in this world.");
		this.add(RubyFluteItem.DRAGON_NOT_LOADED_KEY, "The notes echoed, but there was no response...");
		this.add(RubyFluteItem.DRAGON_STUCK_KEY, "%s was unable to reach your location.");
		this.add(RubyFluteItem.DRAGON_TIRED_KEY, "%s is too tired out to be able to travel.");
		this.add(RedDragonOffspringEntity.MOUNT_DRAGON_KEY, "Right-Click or Press %s While Grounded to Dismount");

		this.add(GuardPigmanInventoryScreen.GUARDING_STATUS_KEY, "Guarding");
		this.add(GuardPigmanInventoryScreen.SEARCHING_STATUS_KEY, "Searching for Safety");
		this.add(GuardPigmanInventoryScreen.IDLE_STATUS_KEY, "Idle");
		this.add(GuardPigmanInventoryScreen.START_GUARD_KEY, "Start guarding");
		this.add(GuardPigmanInventoryScreen.STOP_GUARD_KEY, "Dismiss early");

		// Smithing template description
		this.add(DRAGON_ARMOR_SMITHING_TEMPLATE_CHAIN, "Dragon Armor Chain");
		this.add(DRAGON_ARMOR_SMITHING_TEMPLATE_PLATING, "Dragon Armor Plating");
		this.add(DRAGON_ARMOR_SMITHING_TEMPLATE_INLAY, "Dragon Armor Inlay");
		// Smithing template tooltip
		this.add(DRAGON_ARMOR_SMITHING_TEMPLATE_APPLIES_TO, "Dragon Armor");
		// Smithing table gui
		this.add(DRAGON_ARMOR_SMITHING_TEMPLATE_BASE_SLOT_DESCRIPTION, "Add %s");

		// Dragon armor description
		this.add(DragonArmorTrim.Decoration.CHAIN.localeKey(), "Chain");
		this.add(DragonArmorTrim.Decoration.PLATING.localeKey(), "Plating");
		this.add(DragonArmorTrim.Decoration.INLAY.localeKey(), "Inlay");
	}

	// This is for an enchantment description mod
	public void addEnchantDesc(Enchantment enchantment, String desc)
	{
		this.addEnchantDesc(BuiltInRegistries.ENCHANTMENT.getResourceKey(enchantment).get(), desc);
	}

	// This is for an enchantment description mod
	public void addEnchantDesc(ResourceKey<Enchantment> enchantment, String desc)
	{
		ResourceLocation key = enchantment.location();
		this.add("enchantment." + key.getNamespace() + "." + key.getPath() + ".desc", desc);
	}

	public void addPainting(ResourceKey<PaintingVariant> painting, String title, String author)
	{
		ResourceLocation key = painting.location();
		String s = "painting." + key.getNamespace() + "." + key.getPath() + ".";
		this.add(s + "title", title);
		this.add(s + "author", author);
	}

	public void addAttribute(Supplier<Attribute> attribute, String name)
	{
		this.add(attribute.get().getDescriptionId(), name);
	}

	public void addPotion(Supplier<MobEffect> potion)
	{
		ResourceLocation key = BuiltInRegistries.MOB_EFFECT.getKey(potion.get());
		String path = key.getPath();
		String name = this.toName(path);
		this.add("item.minecraft.potion.effect." + path, "Potion of " + name);
		this.add("item.minecraft.splash_potion.effect." + path, "Splash Potion of " + name);
		this.add("item.minecraft.lingering_potion.effect." + path, "Lingering Potion of " + name);
		this.add("item.minecraft.tipped_arrow.effect." + path, "Arrow of " + name);
	}

	public static String mapName(StructureRegistrar<?> structure)
	{
		return "filled_map." + structure.getRegistryName();
	}

	private void addDamageType(ResourceKey<DamageType> damageType, String deathMessage, String playerKillMessage)
	{
		try
		{
			String messageID = "death.attack." + this.lookup.get().lookupOrThrow(Registries.DAMAGE_TYPE).getOrThrow(damageType).value().msgId();
			this.add(messageID, deathMessage);
			this.add(messageID + ".player", playerKillMessage);
		}
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
	}

	private void addAdvancement(AdvancementHolder advancement, String title, String desc)
	{
		advancement.value().display().ifPresent(display ->
		{
			this.add(display.getTitle().getString(), title);
			this.add(display.getDescription().getString(), desc);
		});
	}

	private <T> void addDefault(ResourceKey<Registry<T>> registry, Map<ResourceLocation, String> overrides)
	{
		this.addDefault_(registry, overrides.entrySet().stream().collect(Collectors.toMap(e -> ResourceKey.create(registry, e.getKey()), e -> e.getValue())));
	}

	private <T> void addDefault_(ResourceKey<Registry<T>> registry, Map<ResourceKey<T>, String> overrides)
	{
		try
		{
			this.lookup.get().lookupOrThrow(registry).listElementIds().distinct().filter(key -> RediscoveredMod.MODID.equals(key.location().getNamespace())).filter(key -> !overrides.containsKey(key)).forEach(this::add);
		}
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
		overrides.forEach(this::add);
	}

	private void add(ResourceKey<?> key)
	{
		this.add(key, this.toName(key));
	}

	private void add(ResourceKey<?> key, String translation)
	{
		this.add(this.makeDescriptionID(key), translation);
	}

	private void add(Supplier<SoundEvent> sound, String translation)
	{
		this.add(sound.get(), translation);
	}

	private void add(SoundEvent sound, String translation)
	{
		this.add("subtitles." + RediscoveredMod.MODID + "." + sound.getLocation().getPath(), translation);
	}

	private void addItemInfo(Supplier<Item> item, String key, String translation)
	{
		var resourceKey = BuiltInRegistries.ITEM.getResourceKey(item.get()).get();
		ResourceLocation location = resourceKey.location();
		this.add(Util.makeDescriptionId(resourceKey.registry().getPath().replace('/', '.'), new ResourceLocation(location.getNamespace(), location.getPath() + "." + key)), translation);
	}

	// Converts camel case to a proper name. snowy_temple -> Snowy Temple
	private String toName(ResourceKey<?> key)
	{
		String suffix;
		if (key.registry().equals(Registries.TRIM_MATERIAL.location()))
			suffix = " Material";
		else if (key.registry().equals(Registries.TRIM_PATTERN.location()))
			suffix = " Armor Trim";
		else
			suffix = "";

		return this.toName(key.location().getPath()) + suffix;
	}

	private String toName(String key)
	{
		String[] words = key.split("_");
		for (int i = words.length - 1; i > -1; i--)
			words[i] = words[i].substring(0, 1).toUpperCase(Locale.ENGLISH) + words[i].substring(1).toLowerCase(Locale.ENGLISH);
		return String.join(" ", words);
	}

	private String makeDescriptionID(ResourceKey<?> resourceKey)
	{
		String registryPath = resourceKey.registry().getPath();
		if (registryPath.equals("custom_stat"))
			registryPath = "stat";
		else if (registryPath.equals(Registries.BIOME.location().getPath()))
			registryPath = "biome";
		return Util.makeDescriptionId(registryPath.replace('/', '.'), resourceKey.location()).replace("entity_type", "entity").replace("mob_effect", "effect");
	}

	private Set<String> existing = new HashSet<>();

	public void add(String key, String value)
	{
		if (existing.add(key))
			super.add(key, value);
	}

	public void add(String key, PigmanData.Profession profession, String value)
	{
		this.add(key + "." + profession.profName, value);
	}
}
