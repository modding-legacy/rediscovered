package com.legacy.rediscovered.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.BaseFakeFireBlock;
import com.legacy.rediscovered.block.BrittleBlock;
import com.legacy.rediscovered.block.GearBlock;
import com.legacy.rediscovered.block.GrassSlabBlock;
import com.legacy.rediscovered.block.RedDragonEggBlock;
import com.legacy.rediscovered.block_entities.RedDragonEggBlockEntity;
import com.legacy.rediscovered.data.loot_functions.SetAttachedQuiverFunction;
import com.legacy.rediscovered.data.loot_functions.SetQuiverContentsFunction;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.Util;
import net.minecraft.advancements.critereon.EnchantmentPredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.MinMaxBounds;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.data.loot.packs.VanillaBlockLoot;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.block.state.properties.SlabType;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.saveddata.maps.MapDecoration;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootDataId;
import net.minecraft.world.level.storage.loot.LootDataType;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.LootTable.Builder;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.functions.CopyNameFunction;
import net.minecraft.world.level.storage.loot.functions.CopyNbtFunction;
import net.minecraft.world.level.storage.loot.functions.CopyNameFunction.NameSource;
import net.minecraft.world.level.storage.loot.functions.EnchantRandomlyFunction;
import net.minecraft.world.level.storage.loot.functions.EnchantWithLevelsFunction;
import net.minecraft.world.level.storage.loot.functions.ExplorationMapFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootingEnchantFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemDamageFunction;
import net.minecraft.world.level.storage.loot.functions.SetNbtFunction;
import net.minecraft.world.level.storage.loot.functions.SetPotionFunction;
import net.minecraft.world.level.storage.loot.functions.SmeltItemFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.BonusLevelTableCondition;
import net.minecraft.world.level.storage.loot.predicates.ExplosionCondition;
import net.minecraft.world.level.storage.loot.predicates.InvertedLootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemKilledByPlayerCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceWithLootingCondition;
import net.minecraft.world.level.storage.loot.predicates.MatchTool;
import net.minecraft.world.level.storage.loot.providers.nbt.ContextNbtProvider;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class RediscoveredLootProv extends LootTableProvider
{
	public RediscoveredLootProv(PackOutput output)
	{
		//@formatter:off
		super(output, Set.of(), List.of(
				new LootTableProvider.SubProviderEntry(ChestLoot::new, LootContextParamSets.CHEST), 
				new LootTableProvider.SubProviderEntry(EntityLoot::new, LootContextParamSets.ENTITY),
				new LootTableProvider.SubProviderEntry(BlockLoot::new, LootContextParamSets.BLOCK), 
				new LootTableProvider.SubProviderEntry(TradeLoot::new, LootContextParamSets.PIGLIN_BARTER),
				new LootTableProvider.SubProviderEntry(ArchaeologyLoot::new, LootContextParamSets.ARCHAEOLOGY)));
		//@formatter:on
	}

	@Override
	protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext context)
	{
		// Vanilla method except the part where it screams about missing builtin loot
		// tables
		map.forEach((name, lootTable) ->
		{
			lootTable.validate(context.setParams(lootTable.getParamSet()).enterElement("{" + name + "}", new LootDataId<>(LootDataType.TABLE, name)));
		});
	}

	public static final ResourceLocation PIGMAN_VILLAGE_WATCHTOWER = RediscoveredMod.locate("chests/pigman_village/watchtower");
	public static final ResourceLocation PIGMAN_VILLAGE_METALWORKER = RediscoveredMod.locate("chests/pigman_village/metalworker");
	public static final ResourceLocation PIGMAN_VILLAGE_BOWYER = RediscoveredMod.locate("chests/pigman_village/bowyer");
	public static final ResourceLocation PIGMAN_VILLAGE_TECHNICIAN = RediscoveredMod.locate("chests/pigman_village/technician");
	public static final ResourceLocation PIGMAN_VILLAGE_TAILOR = RediscoveredMod.locate("chests/pigman_village/tailor");
	public static final ResourceLocation PIGMAN_VILLAGE_DOCTOR = RediscoveredMod.locate("chests/pigman_village/doctor");
	public static final ResourceLocation PIGMAN_VILLAGE_FISH_STALL = RediscoveredMod.locate("chests/pigman_village/fish_stall");

	public static final ResourceLocation QUIVER_CONTENTS = RediscoveredMod.locate("chests/quiver_contents");

	public static final ResourceLocation RANA_TRADING = RediscoveredMod.locate("gameplay/rana_trading");

	public static final ResourceLocation TRAIL_RUINS_PORTAL_ARCHAEOLOGY_COMMON = RediscoveredMod.locate("archaeology/trail_ruins_portal_common");
	public static final ResourceLocation TRAIL_RUINS_PORTAL_ARCHAEOLOGY_RARE = RediscoveredMod.locate("archaeology/trail_ruins_portal_rare");

	public static final ResourceLocation BRICK_PYRAMID_COMMON = RediscoveredMod.locate("chests/brick_pyramid_common");
	public static final ResourceLocation BRICK_PYRAMID_RARE = RediscoveredMod.locate("chests/brick_pyramid_rare");
	public static final ResourceLocation BRICK_PYRAMID_POT = RediscoveredMod.locate("chests/brick_pyramid_pot");

	public static final ResourceLocation PURPLE_ARROW_DISPENSER = RediscoveredMod.locate("chests/purple_arrow_dispenser");
	public static final ResourceLocation FIRE_CHARGE_DISPENSER = RediscoveredMod.locate("chests/fire_charge_dispenser");

	private static class ChestLoot implements LootPoolUtil, LootTableSubProvider
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, Builder> output)
		{
			//@formatter:off
			
			output.accept(QUIVER_CONTENTS, this.table(this.pool(
					item(Items.ARROW, 2, 3).setWeight(2),
					item(RediscoveredItems.purple_arrow)
				).setRolls(UniformGenerator.between(1, 3))));
			
			output.accept(PIGMAN_VILLAGE_WATCHTOWER, this.table(this.pool(
					item(RediscoveredItems.ruby, 0, 1).when(randomChance(0.5F)),
					item(Items.CARROT, 1, 3),
					item(Items.IRON_INGOT, 1, 4),
					item(Items.BOW).when(randomChance(0.5F)),
					item(RediscoveredItems.quiver).apply(SetQuiverContentsFunction.withContents(QUIVER_CONTENTS)).when(randomChance(0.5F)),
					item(Items.ARROW, 1, 4),
					item(Items.LEATHER_CHESTPLATE).apply(SetAttachedQuiverFunction.attachQuiver(RediscoveredItems.quiver.getDefaultInstance(), QUIVER_CONTENTS)).when(randomChance(0.5F)),
					item(Items.DIAMOND).when(randomChance(0.025F)),
					item(Items.GOLD_NUGGET, 1, 4).when(randomChance(0.333F)),
					item(Items.STICK)
				).setRolls(UniformGenerator.between(8, 11))));
			
			output.accept(PIGMAN_VILLAGE_METALWORKER, this.table(this.pool(
					item(RediscoveredItems.ruby, 1, 2).when(randomChance(0.5F)),
					item(Items.CARROT, 1, 3),
					item(Items.IRON_INGOT, 1, 4),
					item(RediscoveredBlocks.gear, 1, 4).when(randomChance(0.5F)),
					item(Items.GUNPOWDER, 1, 2),
					item(Items.GOLDEN_CARROT, 1, 2).when(randomChance(0.333F)),
					item(Items.BUCKET),
					item(Items.DIAMOND).when(randomChance(0.025F)),
					item(Items.GOLD_NUGGET, 1, 4).when(randomChance(0.333F))
				).setRolls(UniformGenerator.between(9, 11))));
			
			output.accept(PIGMAN_VILLAGE_BOWYER, this.table(this.pool(
					item(RediscoveredItems.ruby, 0, 1).when(randomChance(0.5F)),
					item(Items.CARROT, 1, 4),
					item(Items.FLINT, 1, 2),
					item(Items.BOW).when(randomChance(0.5F)),
					item(Items.CROSSBOW).when(randomChance(0.5F)),
					item(Items.ARROW, 4, 9),
					item(RediscoveredItems.purple_arrow, 3, 5),
					item(RediscoveredItems.quiver).apply(SetQuiverContentsFunction.withContents(QUIVER_CONTENTS)),
					item(Items.STRING, 1, 4).when(randomChance(0.333F)),
					item(Items.STICK)
				).setRolls(UniformGenerator.between(9, 11))));
			
			output.accept(PIGMAN_VILLAGE_TECHNICIAN, this.table(this.pool(
					item(RediscoveredItems.ruby, 0, 1).when(randomChance(0.5F)),
					item(Items.CARROT, 1, 3),
					item(Items.QUARTZ, 1, 2),
					item(RediscoveredBlocks.gear, 2, 6).when(randomChance(0.5F)),
					item(Items.REDSTONE, 2, 4),
					item(Items.REPEATER),
					item(RediscoveredBlocks.rotational_converter),
					item(Items.IRON_INGOT, 1, 4),
					item(Items.CHERRY_PLANKS, 1, 4),
					item(Items.STICK, 1, 2),
					item(RediscoveredItems.music_disc_calm4).when(randomChance(0.5F))
				).setRolls(UniformGenerator.between(9, 11))));
			
			float tailorStuddedChance = 0.15F;
			output.accept(PIGMAN_VILLAGE_TAILOR, this.table(this.pool(
					item(RediscoveredItems.ruby, 0, 1).when(randomChance(0.5F)),
					item(Items.CARROT, 1, 4),
					item(RediscoveredItems.studded_helmet).when(randomChance(tailorStuddedChance)),
					item(RediscoveredItems.studded_chestplate).when(randomChance(tailorStuddedChance)),
					item(RediscoveredItems.studded_leggings).when(randomChance(tailorStuddedChance)),
					item(RediscoveredItems.studded_boots).when(randomChance(tailorStuddedChance)),
					item(Items.LEATHER_HELMET),
					item(Items.LEATHER_CHESTPLATE),
					item(Items.LEATHER_LEGGINGS),
					item(Items.LEATHER_BOOTS),
					item(Items.LEATHER, 2, 3),
					item(Items.WHITE_WOOL, 2, 4),
					item(RediscoveredBlocks.bright_green_wool),
					item(RediscoveredBlocks.lavender_wool),
					item(RediscoveredBlocks.slate_blue_wool),
					item(Items.STRING, 2, 5),
					item(Items.RED_DYE),
					item(Items.PINK_DYE),
					item(Items.CYAN_DYE).when(randomChance(0.333F))
				).setRolls(UniformGenerator.between(9, 11))));
			
			output.accept(PIGMAN_VILLAGE_DOCTOR, this.table(this.pool(
					item(RediscoveredItems.ruby, 0, 1).when(randomChance(0.3F)),
					item(Items.CARROT, 1, 3),
					item(Items.GLOWSTONE_DUST, 1, 2),
					item(Items.REDSTONE, 1, 2),
					item(RediscoveredBlocks.rose, 1, 2),
					item(RediscoveredBlocks.cyan_rose).when(randomChance(0.10F)),
					item(Items.GOLD_NUGGET, 3, 5),
					item(Items.GLASS_BOTTLE, 1, 3),
					item(Items.POTION).apply(SetPotionFunction.setPotion(RediscoveredEffects.GOLDEN_AURA_POTION.get())),
					item(Items.GOLDEN_CARROT, 3, 6)
				).setRolls(UniformGenerator.between(5, 7))));

			output.accept(PIGMAN_VILLAGE_FISH_STALL, this.table(this.pool(
					item(RediscoveredItems.raw_fish, 1, 2).setWeight(12),
					item(RediscoveredItems.cooked_fish, 1, 2).setWeight(20),
					item(Items.STRING, 1, 2).setWeight(9),
					item(Items.BUCKET).setWeight(4),
					item(RediscoveredItems.ruby)
				).setRolls(UniformGenerator.between(5, 7))));
			
			
			output.accept(BRICK_PYRAMID_COMMON, this.table(this.pool(
					item(Items.BRICK, 1, 2).setWeight(7),
					item(Items.ARROW, 1, 2).setWeight(6),
					item(Items.COPPER_INGOT, 1, 3).setWeight(6),
					item(Items.IRON_NUGGET, 1, 3).setWeight(6),
					item(Items.BONE, 1, 2).setWeight(3),
					item(Items.GUNPOWDER, 1, 2).setWeight(3),
					item(RediscoveredBlocks.glowing_obsidian, 1, 2).setWeight(3).setQuality(3),
					item(Items.ENDER_PEARL).setWeight(2).setQuality(3).setQuality(3),
					item(Items.FRIEND_POTTERY_SHERD).setWeight(1).setQuality(2),
					item(Items.BLADE_POTTERY_SHERD).setWeight(1).setQuality(2),
					item(Items.HEARTBREAK_POTTERY_SHERD).setWeight(1).setQuality(2),
					item(RediscoveredItems.ruby).setWeight(1).setQuality(3),
					item(RediscoveredItems.draconic_trim).setWeight(2).setQuality(3),
					item(RediscoveredItems.dragon_armor).setWeight(1).setQuality(3),
					item(RediscoveredItems.dragon_armor_chain_smithing_template).setWeight(1).setQuality(2),
					item(RediscoveredItems.dragon_armor_plating_smithing_template).setWeight(1).setQuality(2),
					item(RediscoveredItems.dragon_armor_inlay_smithing_template).setWeight(1).setQuality(2)
				).setRolls(UniformGenerator.between(5, 7))));
			
			output.accept(BRICK_PYRAMID_RARE, this.table(this.pool(
					item(Items.BRICK, 1, 2).setWeight(5),
					item(RediscoveredItems.purple_arrow, 1, 2).setWeight(5),
					item(RediscoveredBlocks.ancient_crying_obsidian, 2, 4).setWeight(8).setQuality(3),
					item(RediscoveredBlocks.glowing_obsidian, 2, 4).setWeight(8),
					item(Items.IRON_INGOT, 1, 3).setWeight(8),
					item(Items.QUARTZ, 2, 4).setWeight(8).setQuality(3),
					item(RediscoveredItems.ruby).setWeight(4).setQuality(3),
					item(RediscoveredBlocks.ruby_eye, 2, 3).setWeight(2).setQuality(3),
					item(RediscoveredItems.draconic_trim).setWeight(2).setQuality(4),
					item(RediscoveredItems.dragon_armor).setWeight(2).setQuality(3),
					item(RediscoveredItems.dragon_armor_chain_smithing_template).setWeight(2).setQuality(2),
					item(RediscoveredItems.dragon_armor_plating_smithing_template).setWeight(2).setQuality(2),
					item(RediscoveredItems.dragon_armor_inlay_smithing_template).setWeight(2).setQuality(2)
				).setRolls(UniformGenerator.between(5, 7))));
			
			// 10% chance to have any of these items
			output.accept(BRICK_PYRAMID_POT, this.table(this.pool(
					item(RediscoveredItems.ruby).setWeight(5),
					item(RediscoveredBlocks.ancient_crying_obsidian).setWeight(5),
					item(Items.QUARTZ).setWeight(5),
					item(Items.IRON_INGOT).setWeight(5),
					item(Items.GUNPOWDER).setWeight(5),
					item(Items.ENDER_PEARL).setWeight(5),
					item(RediscoveredItems.draconic_trim).setQuality(2),
					item(RediscoveredItems.dragon_armor_chain_smithing_template).setWeight(1).setQuality(2),
					item(RediscoveredItems.dragon_armor_plating_smithing_template).setWeight(1).setQuality(2),
					item(RediscoveredItems.dragon_armor_inlay_smithing_template).setWeight(1).setQuality(2)
				).when(randomChance(0.25F))));
			
			output.accept(PURPLE_ARROW_DISPENSER, this.table(this.pool(
					item(RediscoveredItems.purple_arrow, 4, 9)
				)));
			
			output.accept(FIRE_CHARGE_DISPENSER, this.table(this.pool(
					item(Items.FIRE_CHARGE, 3, 12)
				)));
	
			//@formatter:on
		}
	}

	private static class EntityLoot extends VanillaEntityLoot implements LootPoolUtil
	{
		@Override
		public void generate()
		{
			this.add(RediscoveredEntityTypes.FISH, this.table(this.pool(RediscoveredItems.raw_fish).apply(this.smeltItem(ENTITY_ON_FIRE)), this.pool(Items.BONE_MEAL, 1, 1, 0.05F)));

			this.add(RediscoveredEntityTypes.PIGMAN, this.table(this.lootingPool(Items.PORKCHOP, 1, 2, 0, 2).apply(this.smeltItem(ENTITY_ON_FIRE))));
			this.add(RediscoveredEntityTypes.MELEE_PIGMAN, this.table(this.lootingPool(Items.PORKCHOP, 1, 2, 0, 2).apply(this.smeltItem(ENTITY_ON_FIRE))));
			this.add(RediscoveredEntityTypes.RANGED_PIGMAN, this.table(this.lootingPool(Items.PORKCHOP, 1, 2, 0, 2).apply(this.smeltItem(ENTITY_ON_FIRE))));

			// Iron drop rates are half of vanilla piglin gold rates
			this.add(RediscoveredEntityTypes.ZOMBIE_PIGMAN, this.table(this.lootingPool(Items.ROTTEN_FLESH, 0, 1, 0, 1), this.lootingPool(Items.IRON_NUGGET, 0, 1, 0, 1).when(this.randomChance(0.5F)), this.pool(Items.IRON_INGOT).when(this.playerKill()).when(LootItemRandomChanceWithLootingCondition.randomChanceAndLootingBoost(0.0125F, 0.01F))));

			var stevePool = this.pool(this.item(Items.GUNPOWDER, 0, 2), this.item(Items.FEATHER, 0, 2), this.item(Items.STRING, 0, 2)).apply(this.looting(0, 2));
			this.add(RediscoveredEntityTypes.STEVE, this.table(stevePool));
			this.add(RediscoveredEntityTypes.BLACK_STEVE, this.table(stevePool));
			this.add(RediscoveredEntityTypes.BEAST_BOY, this.table(stevePool));
			this.add(RediscoveredEntityTypes.RANA, this.table(this.lootingPool(Items.APPLE, 0, 2, 0, 1), this.lootingPool(RediscoveredBlocks.rose, 0, 1, 0, 1), this.pool(RediscoveredBlocks.cyan_rose, 1, 1, 0.01F)));

			this.add(RediscoveredEntityTypes.RED_DRAGON_BOSS, this.table());
			this.add(RediscoveredEntityTypes.RED_DRAGON_OFFSPRING, this.table());
			this.add(RediscoveredEntityTypes.SCARECROW, this.table(this.pool(RediscoveredItems.scarecrow).apply(CopyNameFunction.copyName(NameSource.THIS))));
		}

		@Override
		public Stream<EntityType<?>> getKnownEntityTypes()
		{
			return super.getKnownEntityTypes().filter(e -> BuiltInRegistries.ENTITY_TYPE.getKey(e).getNamespace().equals(modID()));
		}

		@Override
		protected boolean canHaveLootTable(EntityType<?> type)
		{
			return super.canHaveLootTable(type) || type == RediscoveredEntityTypes.SCARECROW;
		}
	}

	private static class BlockLoot extends VanillaBlockLoot implements LootPoolUtil
	{
		private void blockLoot(Block block)
		{
			if (block == RediscoveredBlocks.ancient_cherry_leaves)
				add(block, b -> this.leaves(b, RediscoveredBlocks.ancient_cherry_sapling, Items.STICK));
			else if (block == RediscoveredBlocks.ruby_ore || block == RediscoveredBlocks.deepslate_ruby_ore)
				add(block, b -> createOreDrop(b, RediscoveredItems.ruby));
			else if (block == RediscoveredBlocks.gear)
				add(block, b ->
				{
					List<LootPool.Builder> pools = new ArrayList<>(6);
					for (GearBlock.GearFace face : GearBlock.GearFace.values())
						pools.add(this.pool(b).when(ExplosionCondition.survivesExplosion()).when(InvertedLootItemCondition.invert(LootItemBlockStatePropertyCondition.hasBlockStateProperties(b).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(face.stateProperty, GearBlock.GearState.NONE.getSerializedName())))));
					return this.table(pools);
				});

			else if (block instanceof ChestBlock)
				add(block, this.createNameableBlockEntityTable(block));
			else if (block instanceof SlabBlock)
			{
				if (block instanceof GrassSlabBlock) // Drops self if silk touch, dirt slab otherwise
				{
					Block other = RediscoveredBlocks.dirt_slab;
					//@formatter:off
					LootTable.Builder lootTable = LootTable.lootTable().withPool(LootPool.lootPool()
					.setRolls(ConstantValue.exactly(1.0F))
					.add(this.applyExplosionDecay(block, LootItem.lootTableItem(block)
							.when(SILK_TOUCH)
							.apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0F))
									.when(LootItemBlockStatePropertyCondition.hasBlockStateProperties(block).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(SlabBlock.TYPE, SlabType.DOUBLE)))))
							.otherwise(this.applyExplosionDecay(other, LootItem.lootTableItem(other)
									.apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0F))
											.when(LootItemBlockStatePropertyCondition.hasBlockStateProperties(block).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(SlabBlock.TYPE, SlabType.DOUBLE))))))));
					//@formatter:on
					add(block, lootTable);
				}
				else if (block == RediscoveredBlocks.dirt_path_slab) // Always drops dirt slab
				{
					Block other = RediscoveredBlocks.dirt_slab;
					//@formatter:off
					LootTable.Builder lootTable = LootTable.lootTable().withPool(LootPool.lootPool()
					.setRolls(ConstantValue.exactly(1.0F))
					.add(this.applyExplosionDecay(other, LootItem.lootTableItem(other)
							.apply(SetItemCountFunction.setCount(ConstantValue.exactly(2.0F))
									.when(LootItemBlockStatePropertyCondition.hasBlockStateProperties(block).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(SlabBlock.TYPE, SlabType.DOUBLE)))))));
					//@formatter:on
					add(block, lootTable);
				}
				else // Normal slab logic
					add(block, this.createSlabItemTable(block));
			}
			else if (block instanceof DoorBlock)
				add(block, b -> createSinglePropConditionTable(b, DoorBlock.HALF, DoubleBlockHalf.LOWER));
			else if (block instanceof FlowerPotBlock)
				dropPottedContents(block);
			else if (block instanceof BaseFakeFireBlock)
				add(block, b -> noDrop());
			else if (block instanceof BrittleBlock)
				silkOrElse(block, Items.AIR);
			else if (block instanceof RedDragonEggBlock)
				add(block, b -> this.table(this.pool(b).apply(CopyNameFunction.copyName(CopyNameFunction.NameSource.BLOCK_ENTITY)).apply(copyBlockEntityTags(RedDragonEggBlockEntity.AGE_KEY, RedDragonEggBlockEntity.CAN_HATCH_KEY, RedDragonEggBlockEntity.OWNER_KEY))));
			else if (block == RediscoveredBlocks.dragon_altar)
				add(block, this.table());
			else
				this.dropSelf(block);
		}
		
		public CopyNbtFunction.Builder copyBlockEntityTags(String... tagPaths)
		{
			var builder = CopyNbtFunction.copyData(ContextNbtProvider.BLOCK_ENTITY);
			for (String tag : tagPaths)
				builder.copy(tag, "BlockEntityTag." + tag);
			return builder;
		}

		@Override
		protected void generate()
		{
			this.getKnownBlocks().forEach(this::blockLoot);
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return StreamSupport.stream(super.getKnownBlocks().spliterator(), false).filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(modID()) && !b.getLootTable().equals(BuiltInLootTables.EMPTY))::iterator;
		}

		private void silkOrElse(Block withSilk, ItemLike without)
		{
			this.add(withSilk, b -> createSingleItemTableWithSilkTouch(b, without));
		}

		private LootTable.Builder leaves(Block block, ItemLike sapling, ItemLike stick)
		{
			return createSilkTouchOrShearsDispatchTable(block, applyExplosionCondition(block, LootItem.lootTableItem(sapling)).when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, DEFAULT_SAPLING_DROP_RATES))).withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).when(NOT_SILK_TOUCH_OR_SHEARS).add(applyExplosionDecay(block, LootItem.lootTableItem(stick).apply(setCount(1.0F, 2.0F))).when(BonusLevelTableCondition.bonusLevelFlatChance(Enchantments.BLOCK_FORTUNE, 0.02F, 0.022222223F, 0.025F, 0.033333335F, 0.1F))));
		}
	}

	private static class TradeLoot implements LootTableSubProvider, LootPoolUtil
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, Builder> output)
		{
			//@formatter:off
			output.accept(RANA_TRADING, this.table(this.pool(
					item(Items.ENCHANTED_GOLDEN_APPLE).setWeight(1).when(randomChance(0.05F)),
					item(Items.GOLDEN_APPLE).setWeight(1),
					item(RediscoveredItems.ruby, 1, 2).setWeight(5),
					item(Items.GOLD_INGOT, 1, 2).setWeight(10),
					item(Items.APPLE, 3, 5).setWeight(16)
				)));
			//@formatter:on
		}
	}

	private static class ArchaeologyLoot implements LootTableSubProvider, LootPoolUtil
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, Builder> output)
		{
			//@formatter:off
			
			// Copies of their respective vanilla archaeology loot tables with some modifications
			output.accept(TRAIL_RUINS_PORTAL_ARCHAEOLOGY_COMMON, LootTable.lootTable()
					.withPool(
							LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(RediscoveredItems.ruby).setWeight(2))
							.add(LootItem.lootTableItem(Items.WHEAT).setWeight(2))
							.add(LootItem.lootTableItem(RediscoveredItems.purple_arrow).setWeight(2))
							.add(LootItem.lootTableItem(Items.CLAY).setWeight(2))
							.add(LootItem.lootTableItem(Items.BRICK).setWeight(2))
							.add(LootItem.lootTableItem(Items.RED_DYE).setWeight(2))
							.add(LootItem.lootTableItem(Items.PINK_DYE).setWeight(2))
							.add(LootItem.lootTableItem(Items.CYAN_DYE).setWeight(2))
							.add(LootItem.lootTableItem(Items.WHITE_DYE).setWeight(2))
							.add(LootItem.lootTableItem(Items.ORANGE_DYE).setWeight(2))
							.add(LootItem.lootTableItem(Items.RED_CANDLE).setWeight(2))
							.add(LootItem.lootTableItem(Items.PINK_CANDLE).setWeight(2))
							.add(LootItem.lootTableItem(Items.PURPLE_CANDLE).setWeight(2))
							.add(LootItem.lootTableItem(Items.CYAN_CANDLE).setWeight(2))
							.add(LootItem.lootTableItem(Items.MAGENTA_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.PINK_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.BLUE_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.CYAN_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.RED_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.YELLOW_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.PURPLE_STAINED_GLASS_PANE))
							.add(LootItem.lootTableItem(Items.CHERRY_HANGING_SIGN))
							.add(LootItem.lootTableItem(Items.OAK_HANGING_SIGN))
							.add(LootItem.lootTableItem(Items.IRON_NUGGET))
							.add(LootItem.lootTableItem(Items.COAL))
							.add(LootItem.lootTableItem(Items.WHEAT_SEEDS))
							.add(LootItem.lootTableItem(Items.BEETROOT_SEEDS))
							.add(LootItem.lootTableItem(Items.DEAD_BUSH))
							.add(LootItem.lootTableItem(Items.FLOWER_POT))
							.add(LootItem.lootTableItem(Items.LEATHER))
							.add(LootItem.lootTableItem(Items.LEAD))));

			
			output.accept(TRAIL_RUINS_PORTAL_ARCHAEOLOGY_RARE, LootTable.lootTable()
					.withPool(
							LootPool.lootPool()
							.setRolls(ConstantValue.exactly(1.0F))
							.add(LootItem.lootTableItem(Items.BURN_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.DANGER_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.FRIEND_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.HEART_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.HEARTBREAK_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.HOWL_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.SHEAF_POTTERY_SHERD))
							.add(LootItem.lootTableItem(Items.WAYFINDER_ARMOR_TRIM_SMITHING_TEMPLATE))
							.add(LootItem.lootTableItem(Items.RAISER_ARMOR_TRIM_SMITHING_TEMPLATE))
							.add(LootItem.lootTableItem(Items.SHAPER_ARMOR_TRIM_SMITHING_TEMPLATE))
							.add(LootItem.lootTableItem(Items.HOST_ARMOR_TRIM_SMITHING_TEMPLATE))
							.add(LootItem.lootTableItem(RediscoveredBlocks.ancient_crying_obsidian))));
			//@formatter:on
		}
	}

	private static interface LootPoolUtil
	{
		LootItemCondition.Builder SILK_TOUCH = MatchTool.toolMatches(ItemPredicate.Builder.item().hasEnchantment(new EnchantmentPredicate(Enchantments.SILK_TOUCH, MinMaxBounds.Ints.atLeast(1))));
		LootItemCondition.Builder SHEARS = MatchTool.toolMatches(ItemPredicate.Builder.item().of(Items.SHEARS));
		LootItemCondition.Builder SILK_TOUCH_OR_SHEARS = SHEARS.or(SILK_TOUCH);
		LootItemCondition.Builder NOT_SILK_TOUCH_OR_SHEARS = SILK_TOUCH_OR_SHEARS.invert();
		float[] DEFAULT_SAPLING_DROP_RATES = new float[] { 0.05F, 0.0625F, 0.083333336F, 0.1F };

		default String modID()
		{
			return RediscoveredMod.MODID;
		}

		/**
		 * Creates a table from the given loot pools.
		 * 
		 * @param pools
		 * @return
		 */
		default LootTable.Builder table(List<LootPool.Builder> pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			pools.forEach(pool -> table.withPool(pool));
			return table;
		}

		/**
		 * Creates a table from the given loot pool.
		 * 
		 * @param pool
		 * @return
		 */
		default LootTable.Builder table(LootPool.Builder pool)
		{
			return LootTable.lootTable().withPool(pool);
		}

		default LootTable.Builder table(LootPool.Builder... pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			for (var pool : pools)
				table.withPool(pool);
			return table;
		}

		default LootPool.Builder pool(ItemLike item, int min, int max, float chance)
		{
			return LootPool.lootPool().add(item(item, min, max).when(this.randomChance(chance)));
		}

		default LootPool.Builder pool(ItemLike item, int min, int max)
		{
			return LootPool.lootPool().add(item(item, min, max));
		}

		default LootPool.Builder pool(ItemLike item)
		{
			return LootPool.lootPool().add(item(item));
		}

		/**
		 * Creates a loot pool with multiple entries. One of these entries will be
		 * picked at random each time the pool rolls.
		 */
		default LootPool.Builder pool(LootPoolEntryContainer.Builder<?>... lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			for (var entry : lootEntries)
				pool.add(entry);
			return pool;
		}

		default LootPool.Builder pool(Collection<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			for (var entry : lootEntries)
				pool.add(entry);
			return pool;
		}

		/**
		 * Creates a loot pool that will give a random item from the list.
		 */
		default LootPool.Builder randomItemPool(List<ItemLike> items)
		{
			return pool(items.stream().map((i) -> item(i)).toArray(LootPoolEntryContainer.Builder[]::new));
		}

		/**
		 * Multiplies the item count by `rand(minLooting to maxLooting) * lootingLevel`
		 */
		default LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting)
		{
			return pool(item, min, max).apply(this.looting(minLooting, maxLooting));
		}

		default LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting, float chance)
		{
			return lootingPool(item, min, max, minLooting, maxLooting).when(this.randomChance(chance));
		}

		/**
		 * Creates a loot entry for the given item. Gives an amount between the min and
		 * max.
		 */
		default LootItem.Builder<?> item(ItemLike item, int min, int max)
		{
			return item(item).apply(setCount(min, max));
		}

		/**
		 * Creates a loot entry for the given item.
		 */
		default LootItem.Builder<?> item(ItemLike item, int count)
		{
			return LootItem.lootTableItem(item).apply(setCount(count));
		}

		/**
		 * Creates a loot entry for the given item. Will only give one item.
		 */
		default LootItem.Builder<?> item(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}

		default LootItem.Builder<?> tagEntry(TagKey<Item> tag, int min, int max)
		{
			return TagEntry.expandTag(tag).apply(setCount(min, max));
		}

		default LootItem.Builder<?> tagEntry(TagKey<Item> tag, int count)
		{
			return TagEntry.expandTag(tag).apply(setCount(count));
		}

		default LootItem.Builder<?> tagEntry(TagKey<Item> tag)
		{
			return tagEntry(tag, 1);
		}

		/**
		 * Sets the damage of the item (percentage)
		 * 
		 * @param min
		 *            0 - 100
		 * @param max
		 *            0 - 100
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setDamage(int min, int max)
		{
			return SetItemDamageFunction.setDamage(UniformGenerator.between(min / 100F, max / 100F));
		}

		/**
		 * Cooks the item if the predicate passes
		 * 
		 * @param predicate
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> smeltItem(EntityPredicate.Builder predicate)
		{
			return SmeltItemFunction.smelted().when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, predicate));
		}

		/**
		 * Enchants the item randomly between the levels provided
		 * 
		 * @param minLevel
		 * @param maxLevel
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(int minLevel, int maxLevel)
		{
			return EnchantWithLevelsFunction.enchantWithLevels(UniformGenerator.between(minLevel, maxLevel));
		}

		/**
		 * Enchants the item randomly with the enchantments passed
		 * 
		 * @param enchantments
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(Enchantment... enchantments)
		{
			EnchantRandomlyFunction.Builder func = new EnchantRandomlyFunction.Builder();
			for (Enchantment enchantment : enchantments)
				func.withEnchantment(enchantment);
			return func;
		}

		/**
		 * Sets the nbt of the item
		 * 
		 * @param nbt
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setNbt(Consumer<CompoundTag> nbt)
		{
			return SetNbtFunction.setTag(Util.make(new CompoundTag(), nbt));
		}

		default LootItemConditionalFunction.Builder<?> map(TagKey<Structure> structure)
		{
			return ExplorationMapFunction.makeExplorationMap().setDestination(structure).setMapDecoration(MapDecoration.Type.RED_X).setZoom((byte) 1).setSkipKnownStructures(false);
		}

		default LootItemConditionalFunction.Builder<?> setCount(float min, float max)
		{
			if (min == max)
				return setCount(min);
			return SetItemCountFunction.setCount(UniformGenerator.between(min, max));
		}

		default LootItemConditionalFunction.Builder<?> setCount(float count)
		{
			return SetItemCountFunction.setCount(ConstantValue.exactly(count));
		}

		default LootItemCondition.Builder playerKill()
		{
			return LootItemKilledByPlayerCondition.killedByPlayer();
		}

		default LootItemCondition.Builder randomChance(float chance)
		{
			return LootItemRandomChanceCondition.randomChance(chance);
		}

		default LootingEnchantFunction.Builder looting(float minLooting, float maxLooting)
		{
			return LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(minLooting, maxLooting));
		}
	}
}
