package com.legacy.rediscovered.client;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;

public class RediscoveredSounds
{
	public static final SoundEvent BLOCK_ROTATIONAL_CONVERTER_CLICK = create("block.rotational_converter.click");

	public static final SoundEvent BLOCK_NETHER_REACTOR_ACTIVATE = create("block.nether_reactor.activate");
	public static final SoundEvent BLOCK_NETHER_REACTOR_IDLE = create("block.nether_reactor.idle");
	public static final SoundEvent BLOCK_NETHER_REACTOR_DEACTIVATE = create("block.nether_reactor.deactivate");

	public static final SoundEvent BLOCK_RED_DRAGON_EGG_FERTILIZE = create("block.red_dragon_egg.fertilize");
	public static final SoundEvent BLOCK_RED_DRAGON_EGG_SHAKE = create("block.red_dragon_egg.shake");
	public static final SoundEvent BLOCK_RED_DRAGON_EGG_HATCH = create("block.red_dragon_egg.hatch");

	public static final SoundEvent BLOCK_TABLE_ADD_ITEM = create("block.table.add_item");
	public static final SoundEvent BLOCK_TABLE_REMOVE_ITEM = create("block.table.remove_item");

	public static final SoundEvent ENTITY_PIGMAN_IDLE = create("entity.pigman.idle");
	public static final SoundEvent ENTITY_PIGMAN_HURT = create("entity.pigman.hurt");
	public static final SoundEvent ENTITY_PIGMAN_DEATH = create("entity.pigman.death");
	public static final SoundEvent ENTITY_PIGMAN_ANGRY = create("entity.pigman.angry");
	public static final SoundEvent ENTITY_PIGMAN_SCARED = create("entity.pigman.scared");
	public static final SoundEvent ENTITY_PIGMAN_AGREE = create("entity.pigman.agree");
	public static final SoundEvent ENTITY_PIGMAN_DISAGREE = create("entity.pigman.disagree");
	public static final SoundEvent ENTITY_PIGMAN_WORK_METALWORKER = create("entity.pigman.work_metalworker");
	public static final SoundEvent ENTITY_PIGMAN_WORK_BOWYER = create("entity.pigman.work_bowyer");
	public static final SoundEvent ENTITY_PIGMAN_WORK_TECHNICIAN = create("entity.pigman.work_technician");
	public static final SoundEvent ENTITY_PIGMAN_WORK_TAILOR = create("entity.pigman.work_tailor");
	public static final SoundEvent ENTITY_PIGMAN_WORK_DOCTOR = create("entity.pigman.work_doctor");
	public static final SoundEvent ENTITY_PIGMAN_ZOMBIFY = create("entity.pigman.zombify");

	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_IDLE = create("entity.zombie_pigman.idle");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_HURT = create("entity.zombie_pigman.hurt");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_DEATH = create("entity.zombie_pigman.death");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_ANGRY = create("entity.zombie_pigman.angry");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_CURE = create("entity.zombie_pigman.cure");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_FINISH_CURE = create("entity.zombie_pigman.finish_cure");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_SELECT_MELEE = create("entity.zombie_pigman.select_melee");
	public static final SoundEvent ENTITY_ZOMBIE_PIGMAN_SELECT_RANGED = create("entity.zombie_pigman.select_ranged");

	public static final SoundEvent ENTITY_STEVE_HURT = create("entity.steve.hurt");
	public static final SoundEvent ENTITY_STEVE_DEATH = create("entity.steve.death");

	public static final SoundEvent ENTITY_RANA_HURT = create("entity.rana.hurt");
	public static final SoundEvent ENTITY_RANA_DEATH = create("entity.rana.death");

	public static final SoundEvent ENTITY_BLACK_STEVE_HURT = create("entity.black_steve.hurt");
	public static final SoundEvent ENTITY_BLACK_STEVE_DEATH = create("entity.black_steve.death");

	public static final SoundEvent ENTITY_BEAST_BOY_HURT = create("entity.beast_boy.hurt");
	public static final SoundEvent ENTITY_BEAST_BOY_DEATH = create("entity.beast_boy.death");

	/*public static final SoundEvent ENTITY_FISH_IDLE = create("entity.fish.idle");*/
	public static final SoundEvent ENTITY_FISH_HURT = create("entity.fish.hurt");
	public static final SoundEvent ENTITY_FISH_DEATH = create("entity.fish.death");
	public static final SoundEvent ENTITY_FISH_FLOP = create("entity.fish.flop");

	public static final SoundEvent ENTITY_DRAGON_PYLON_SHIELD_LOST = create("entity.dragon_pylon.shield_lost");
	public static final SoundEvent ENTITY_DRAGON_PYLON_HURT = create("entity.dragon_pylon.hurt");
	public static final SoundEvent ENTITY_DRAGON_PYLON_DEATH = create("entity.dragon_pylon.death");

	public static final SoundEvent ENTITY_PYLON_BURST_DESTROYED = create("entity.pylon_burst.destroyed");
	public static final SoundEvent ENTITY_PYLON_BURST_EXPLODE = create("entity.pylon_burst.explode");

	public static final SoundEvent ENTITY_RED_DRAGON_IDLE = create("entity.red_dragon.idle");
	public static final SoundEvent ENTITY_RED_DRAGON_IDLE_CALM = create("entity.red_dragon.idle_calm");
	public static final SoundEvent ENTITY_RED_DRAGON_HURT = create("entity.red_dragon.hurt");
	public static final SoundEvent ENTITY_RED_DRAGON_DEATH = create("entity.red_dragon.death");
	public static final SoundEvent ENTITY_RED_DRAGON_STEP = create("entity.red_dragon.step");
	public static final SoundEvent ENTITY_RED_DRAGON_FLAP = create("entity.red_dragon.flap");
	public static final SoundEvent ENTITY_RED_DRAGON_SHIELD_DOWN = create("entity.red_dragon.shield_down");
	public static final SoundEvent ENTITY_RED_DRAGON_BOLT_BALL_CHARGE = create("entity.red_dragon.bolt_ball_charge");
	public static final SoundEvent ENTITY_RED_DRAGON_BOLT_BALL_SHOOT = create("entity.red_dragon.bolt_ball_shoot");
	public static final SoundEvent ENTITY_RED_DRAGON_PREPARE_WIND_BLOW = create("entity.red_dragon.prepare_wind_blow");
	public static final SoundEvent ENTITY_RED_DRAGON_WIND_BLOW = create("entity.red_dragon.wind");
	public static final SoundEvent ENTITY_RED_DRAGON_PREPARE_THUNDER_CLOUDS = create("entity.red_dragon.prepare_thunder_clouds");
	public static final SoundEvent ENTITY_RED_DRAGON_SHED_BURSTS = create("entity.red_dragon.shed_bursts");

	public static final SoundEvent ENTITY_RED_DRAGON_EQUIP_SADDLE = create("entity.red_dragon.equip_saddle");
	public static final SoundEvent ENTITY_RED_DRAGON_EQUIP_ARMOR = create("entity.red_dragon.equip_armor");

	public static final SoundEvent ITEM_ARMOR_EQUIP_STUDDED = create("item.armor.equip_studded");
	public static final SoundEvent ITEM_ARMOR_EQUIP_PLATE = create("item.armor.equip_plate");

	public static final SoundEvent RECORDS_MAGNETIC_CIRCUIT = create("records.magnetic_circuit");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = RediscoveredMod.locate(name);
		SoundEvent sound = SoundEvent.createVariableRangeEvent(location);
		Registry.register(BuiltInRegistries.SOUND_EVENT, location, sound);
		return sound;
	}

}