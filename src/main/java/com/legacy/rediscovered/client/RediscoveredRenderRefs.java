package com.legacy.rediscovered.client;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.client.model.geom.ModelLayerLocation;

public class RediscoveredRenderRefs
{
	public static final ModelLayerLocation PIGMAN = layer("pigman");
	public static final ModelLayerLocation PIGMAN_INNER_ARMOR = layer("pigman", "inner_armor");
	public static final ModelLayerLocation PIGMAN_OUTER_ARMOR = layer("pigman", "outer_armor");

	public static final ModelLayerLocation ZOMBIE_PIGMAN = layer("zombie_pigman");
	public static final ModelLayerLocation ZOMBIE_PIGMAN_PROFESSION = layer("zombie_pigman", "profession");

	public static final ModelLayerLocation STEVE = layer("steve");
	public static final ModelLayerLocation RANA = layer("rana");

	public static final ModelLayerLocation FISH = layer("fish");

	public static final ModelLayerLocation SCARECROW = layer("scarecrow");

	public static final ModelLayerLocation QUIVER = layer("quiver");

	public static final ModelLayerLocation GEAR = layer("gear");

	public static final ModelLayerLocation NETHER_REACTOR_PULSE = layer("nether_reactor_pulse");

	public static final ModelLayerLocation RED_DRAGON = layer("red_dragon");
	public static final ModelLayerLocation RED_DRAGON_ARMOR = layer("red_dragon", "armor");
	public static final ModelLayerLocation RED_DRAGON_SADDLE = layer("red_dragon", "saddle");
	public static final ModelLayerLocation DRAGON_PYLON = layer("dragon_pylon");
	public static final ModelLayerLocation BOLT_BALL_WIND = layer("bolt_ball_wind");
	public static final ModelLayerLocation THUNDER_CLOUD = layer("thunder_cloud");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(RediscoveredMod.locate(name), layer);
	}
}
