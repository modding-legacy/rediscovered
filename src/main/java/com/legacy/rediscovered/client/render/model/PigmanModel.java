package com.legacy.rediscovered.client.render.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;
import com.legacy.rediscovered.entity.util.IPigmanDataHolder;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.PlayerModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.monster.Zombie;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PigmanModel<T extends Mob> extends HumanoidModel<T>
{
	final boolean isProfessionLayer;
	public final ModelPart leftSleeve;
	public final ModelPart rightSleeve;
	public final ModelPart leftPants;
	public final ModelPart rightPants;
	public final ModelPart jacket;

	public PigmanModel(ModelPart root)
	{
		this(root, false);
	}
	
	public PigmanModel(ModelPart root, boolean isProfessionLayer)
	{
		super(root);

		this.leftSleeve = root.getChild("left_sleeve");
		this.rightSleeve = root.getChild("right_sleeve");
		this.leftPants = root.getChild("left_pants");
		this.rightPants = root.getChild("right_pants");
		this.jacket = root.getChild("jacket");
		
		this.isProfessionLayer = isProfessionLayer;
	}

	public static LayerDefinition createBodyLayer(CubeDeformation headScale)
	{
		MeshDefinition mesh = PlayerModel.createMesh(CubeDeformation.NONE, false);
		PartDefinition root = mesh.getRoot();

		root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, headScale).texOffs(32, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, headScale.extend(0.5F)).texOffs(24, 0).addBox(-2.0F, -4.0F, -5.0F, 4.0F, 3.0F, 1.0F, headScale), PartPose.offset(0.0F, 0.0F, 0.0F));
		root.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(32, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, headScale.extend(0.5F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(mesh, 64, 64);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return Iterables.concat(super.bodyParts(), ImmutableList.of(this.leftPants, this.rightPants, this.leftSleeve, this.rightSleeve, this.jacket));
	}

	@Override
	public void prepareMobModel(T entity, float limbSwing, float limbSwingAmount, float partialTicks)
	{
		this.rightArmPose = !entity.getItemInHand(entity.isLeftHanded() ? InteractionHand.OFF_HAND : InteractionHand.MAIN_HAND).isEmpty() ? HumanoidModel.ArmPose.ITEM : HumanoidModel.ArmPose.EMPTY;
		this.leftArmPose = !entity.getItemInHand(!entity.isLeftHanded() ? InteractionHand.OFF_HAND : InteractionHand.MAIN_HAND).isEmpty() ? HumanoidModel.ArmPose.ITEM : HumanoidModel.ArmPose.EMPTY;

		if (entity instanceof GuardPigmanEntity guard)
		{
			ArmPose pose = null;

			if (guard.getArmPose() == GuardPigmanEntity.PigmanArmPose.BOW_AND_ARROW)
				pose = ArmPose.BOW_AND_ARROW;
			/*else if (guard.getArmPose() == GuardPigmanEntity.PigmanArmPose.CROSSBOW_HOLD)
				pose = ArmPose.CROSSBOW_HOLD;
			else if (guard.getArmPose() == GuardPigmanEntity.PigmanArmPose.CROSSBOW_CHARGE)
				pose = ArmPose.CROSSBOW_CHARGE;*/

			if (pose != null)
			{
				if (entity.getMainArm() == HumanoidArm.RIGHT)
					this.rightArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
				else
					this.leftArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
			}
		}

		super.prepareMobModel(entity, limbSwing, limbSwingAmount, partialTicks);
	}

	@Override
	public void setupAnim(T pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
		super.setupAnim(pEntity, pLimbSwing, pLimbSwingAmount, pAgeInTicks, pNetHeadYaw, pHeadPitch);

		/*ItemStack itemstack = pEntity.getMainHandItem();
		if (pEntity instanceof Mob mob && mob.isAggressive() && (itemstack.isEmpty() || !itemstack.is(Items.BOW)))
		{
			float f = Mth.sin(this.attackTime * (float) Math.PI);
			float f1 = Mth.sin((1.0F - (1.0F - this.attackTime) * (1.0F - this.attackTime)) * (float) Math.PI);
			this.rightArm.zRot = 0.0F;
			this.leftArm.zRot = 0.0F;
			this.rightArm.yRot = -(0.1F - f * 0.6F);
			this.leftArm.yRot = 0.1F - f * 0.6F;
			this.rightArm.xRot = (-(float) Math.PI / 2F);
			this.leftArm.xRot = (-(float) Math.PI / 2F);
			this.rightArm.xRot -= f * 1.2F - f1 * 0.4F;
			this.leftArm.xRot -= f * 1.2F - f1 * 0.4F;
			AnimationUtils.bobArms(this.rightArm, this.leftArm, pAgeInTicks);
		}
		else */if (pEntity instanceof Zombie zomb)
		{
			AnimationUtils.animateZombieArms(this.leftArm, this.rightArm, zomb.isAggressive(), this.attackTime, pAgeInTicks);
		}
		else if (pEntity instanceof GuardPigmanEntity guard)
		{
			GuardPigmanEntity.PigmanArmPose pose = guard.getArmPose();
			/*if (pose == GuardPigmanEntity.PigmanArmPose.BOW_AND_ARROW)
			{
				this.rightArm.yRot = -0.1F + this.head.yRot;
				this.rightArm.xRot = (-(float) Math.PI / 2F) + this.head.xRot;
				this.leftArm.xRot = -0.9424779F + this.head.xRot;
				this.leftArm.yRot = this.head.yRot - 0.4F;
				this.leftArm.zRot = ((float) Math.PI / 2F);
			}
			else */if (pose == GuardPigmanEntity.PigmanArmPose.CROSSBOW_HOLD)
				AnimationUtils.animateCrossbowHold(this.rightArm, this.leftArm, this.head, !guard.isLeftHanded());
			else if (pose == GuardPigmanEntity.PigmanArmPose.CROSSBOW_CHARGE)
				AnimationUtils.animateCrossbowCharge(this.rightArm, this.leftArm, pEntity, !guard.isLeftHanded());
		}

		boolean skipPants = !this.isProfessionLayer && pEntity.getType() == RediscoveredEntityTypes.ZOMBIE_PIGMAN && pEntity instanceof IPigmanDataHolder pigmanData && !pigmanData.getPigmanData().getProfession().renderZombiePigmanLoincloth;
		this.leftPants.skipDraw = skipPants;
		this.rightPants.skipDraw = skipPants;
		this.leftPants.copyFrom(this.leftLeg);
		this.rightPants.copyFrom(this.rightLeg);

		this.leftSleeve.copyFrom(this.leftArm);
		this.rightSleeve.copyFrom(this.rightArm);
		this.jacket.copyFrom(this.body);

		this.jacket.visible = true;
	}

	public void setupLevelLayer(T entity)
	{
		boolean onJacket = entity instanceof IPigmanDataHolder pigmanData && pigmanData.getPigmanData().getProfession().renderLevelOnJacket;
		this.body.skipDraw = onJacket;
		this.jacket.skipDraw = !onJacket;
	}

	public void resetLevelLayer()
	{
		this.body.skipDraw = false;
		this.jacket.skipDraw = false;
	}
}