package com.legacy.rediscovered.client.render.entity.layer;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.render.model.PigmanModel;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Mob;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PigmanPantsLayer<T extends Mob, M extends EntityModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation BASE_TEXTURE = RediscoveredMod.locate("textures/entity/pigman/pigman_base.png");
	private final PigmanModel<T> layerModel;

	public PigmanPantsLayer(RenderLayerParent<T, M> pRenderer, EntityModelSet pModelSet, ModelLayerLocation layer)
	{
		super(pRenderer);
		this.layerModel = new PigmanModel<>(pModelSet.bakeLayer(layer), true);
	}

	@Override
	public void render(PoseStack pMatrixStack, MultiBufferSource pBuffer, int pPackedLight, T entity, float pLimbSwing, float pLimbSwingAmount, float pPartialTicks, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
		coloredCutoutModelCopyLayerRender(this.getParentModel(), this.layerModel, BASE_TEXTURE, pMatrixStack, pBuffer, pPackedLight, entity, pLimbSwing, pLimbSwingAmount, pAgeInTicks, pNetHeadYaw, pHeadPitch, pPartialTicks, 1.0F, 1.0F, 1.0F);
	}
}