package com.legacy.rediscovered.client.render.entity.layer;

import java.util.Optional;

import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.ItemStack;

public class QuiverArmorLayer<T extends LivingEntity, M extends HumanoidModel<T>> extends RenderLayer<T, M>
{
	private final HumanoidModel<T> quiverModel;

	public QuiverArmorLayer(RenderLayerParent<T, M> renderer, EntityRendererProvider.Context context)
	{
		super(renderer);
		this.quiverModel = new HumanoidModel<>(context.bakeLayer(RediscoveredRenderRefs.QUIVER));
	}

	@Override
	public void render(PoseStack poseStack, MultiBufferSource buffSource, int packedLight, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		Optional<AttachedItem> attachedItem = AttachedItem.get(entity.getItemBySlot(EquipmentSlot.CHEST));
		if (attachedItem.isPresent())
		{
			ItemStack quiverStack = attachedItem.get().getAttached();
			if (!quiverStack.isEmpty())
			{
				ResourceLocation itemID = quiverStack.getItemHolder().unwrapKey().map(ResourceKey::location).orElse(null);
				if (itemID != null)
				{
					this.getParentModel().copyPropertiesTo(this.quiverModel);
					this.quiverModel.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
					poseStack.pushPose();
					poseStack.scale(1.05F, 1.05F, 1.05F);
					if (quiverStack.getItem() instanceof DyeableLeatherItem dyeable)
					{
						int color = dyeable.getColor(quiverStack);
						float r = (color >> 16 & 255) / 255.0F;
						float g = (color >> 8 & 255) / 255.0F;
						float b = (color & 255) / 255.0F;
						this.renderModel(poseStack, buffSource, packedLight, r, g, b, itemID, "");
						this.renderModel(poseStack, buffSource, packedLight, 1.0F, 1.0F, 1.0F, itemID, "_overlay");
					}
					else
					{
						this.renderModel(poseStack, buffSource, packedLight, 1.0F, 1.0F, 1.0F, itemID, "");
					}
					poseStack.popPose();
				}
			}
		}
	}

	private void renderModel(PoseStack poseStack, MultiBufferSource buffSource, int packedLight, float r, float g, float b, ResourceLocation itemID, String suffix)
	{
		VertexConsumer vertexConsumer = buffSource.getBuffer(RenderType.armorCutoutNoCull(new ResourceLocation(String.format("%s:textures/models/armor/%s_layer_1%s.png", itemID.getNamespace(), itemID.getPath(), suffix))));
		this.quiverModel.renderToBuffer(poseStack, vertexConsumer, packedLight, OverlayTexture.NO_OVERLAY, r, g, b, 1.0F);
	}
}
