package com.legacy.rediscovered.client.render;

import com.legacy.rediscovered.client.render.md3.MD3Renderer;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;

public interface IMD3Render
{
	<E extends Entity> void render(E entityIn, float rotationYawIn, float partialTicks, PoseStack matrix, MultiBufferSource buffer, int packedLightIn, double xIn, double yIn, double zIn);

	ResourceLocation getMD3Texture();

	MD3Renderer getMD3Renderer();

	default float getSpeedMultiplier()
	{
		return 1.0F;
	}
}