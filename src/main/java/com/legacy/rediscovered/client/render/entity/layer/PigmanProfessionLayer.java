package com.legacy.rediscovered.client.render.entity.layer;

import java.util.EnumMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.render.model.PigmanModel;
import com.legacy.rediscovered.entity.pigman.data.PigmanData;
import com.legacy.rediscovered.entity.util.IPigmanDataHolder;
import com.mojang.blaze3d.vertex.PoseStack;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.Util;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Mob;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PigmanProfessionLayer<T extends Mob & IPigmanDataHolder, M extends EntityModel<T>> extends RenderLayer<T, M>
{
	private final EnumMap<PigmanData.Profession, ResourceLocation> professionTextures;
	private final Map<Integer, ResourceLocation> levelTextures;
	private final PigmanModel<T> layerModel;

	public PigmanProfessionLayer(RenderLayerParent<T, M> pRenderer, EntityModelSet pModelSet, ModelLayerLocation layer, boolean zombie)
	{
		super(pRenderer);
		this.layerModel = new PigmanModel<>(pModelSet.bakeLayer(layer), true);
		this.professionTextures = Util.make(new EnumMap<>(PigmanData.Profession.class), map ->
		{
			for (var prof : PigmanData.Profession.values())
				if (prof != PigmanData.Profession.NONE)
					map.put(prof, this.texture(zombie, "profession/" + prof.profName));
		});
		this.levelTextures = Util.make(new Int2ObjectOpenHashMap<>(), map ->
		{
			map.put(1, "stone");
			map.put(2, "iron");
			map.put(3, "gold");
			map.put(4, "ruby");
			map.put(5, "diamond");
		}).int2ObjectEntrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> this.texture(zombie, "profession_level/" + e.getValue())));
	}

	public void render(PoseStack pMatrixStack, MultiBufferSource pBuffer, int pPackedLight, T entity, float pLimbSwing, float pLimbSwingAmount, float pPartialTicks, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
		PigmanData pigmanData = entity.getPigmanData();
		PigmanData.Profession prof = pigmanData.getProfession();
		if (prof != PigmanData.Profession.NONE)
		{
			coloredCutoutModelCopyLayerRender(this.getParentModel(), this.layerModel, this.professionTextures.get(prof), pMatrixStack, pBuffer, pPackedLight, entity, pLimbSwing, pLimbSwingAmount, pAgeInTicks, pNetHeadYaw, pHeadPitch, pPartialTicks, 1.0F, 1.0F, 1.0F);
			
			this.layerModel.setupLevelLayer(entity);
			coloredCutoutModelCopyLayerRender(this.getParentModel(), this.layerModel, this.levelTextures.get(pigmanData.getLevel()), pMatrixStack, pBuffer, pPackedLight, entity, pLimbSwing, pLimbSwingAmount, pAgeInTicks, pNetHeadYaw, pHeadPitch, pPartialTicks, 1.0F, 1.0F, 1.0F);
			this.layerModel.resetLevelLayer();
		}
	}
	
	private ResourceLocation texture(boolean zombie, String texture)
	{
		return RediscoveredMod.locate("textures/entity/" + (zombie ? "zombie_pigman/" : "pigman/") + texture + ".png");
	}
}