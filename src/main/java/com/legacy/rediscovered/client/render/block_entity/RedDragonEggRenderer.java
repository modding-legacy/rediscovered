package com.legacy.rediscovered.client.render.block_entity;

import com.legacy.rediscovered.block.RedDragonEggBlock;
import com.legacy.rediscovered.block_entities.RedDragonEggBlockEntity;
import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.block.BlockRenderDispatcher;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.BlockHitResult;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.client.model.data.ModelData;

@OnlyIn(Dist.CLIENT)
public class RedDragonEggRenderer<T extends RedDragonEggBlockEntity> implements BlockEntityRenderer<T>
{
	private final BlockRenderDispatcher blockRenderer;

	public RedDragonEggRenderer(BlockEntityRendererProvider.Context context)
	{
		this.blockRenderer = context.getBlockRenderDispatcher();
	}

	@Override
	public void render(T egg, float partialTicks, PoseStack poseStack, MultiBufferSource buffSource, int packedLight, int packedOverlay)
	{
		if (egg.getBlockState().hasProperty(RedDragonEggBlock.FALLING) && egg.getBlockState().getValue(RedDragonEggBlock.FALLING))
			return;
		
		poseStack.pushPose();
		BakedModel model = this.blockRenderer.getBlockModel(egg.getBlockState());

		// Offset so we can apply rotations/scaling from the center of the model. It can be moved back later.
		poseStack.translate(0.5, 0.5, 0.5);

		// Handle scale and rotations here
		if (egg.canHatch())
		{
			if (egg.isShaking())
			{
				float hatchProgress = egg.getHatchProgress();
				//hatchProgress = 1;
				hatchProgress = hatchProgress * hatchProgress * hatchProgress * hatchProgress;
				float bounces = 1.0F;
				float bounceHeight = 0.1F * hatchProgress;
				float period = Mth.TWO_PI / (egg.maxShakeTime() / bounces);
				float bounce = (float) Math.sin(period * egg.clientShakeTime(partialTicks)) * bounceHeight;
				if (bounce > 0)
				{
					// In degrees
					float shakeAmount = 9.0F * hatchProgress;
					float turnAmount = 15.0F * hatchProgress;
					poseStack.mulPose(Axis.XP.rotationDegrees(((float) Math.sin(period * 2 * egg.clientShakeTime(partialTicks)) * shakeAmount) * egg.clientXShakeDir));
					poseStack.mulPose(Axis.ZP.rotationDegrees(((float) Math.sin(period * 2 * egg.clientShakeTime(partialTicks)) * shakeAmount) * egg.clientZShakeDir));
					poseStack.mulPose(Axis.YP.rotationDegrees(((float) Math.sin(period * 2 * egg.clientShakeTime(partialTicks)) * turnAmount) * egg.clientYShakeDir));

					poseStack.translate(0, bounce, 0);
				}

			}
		}

		// Shift back to center AFTER rotations so that rotations can be applied from the center of the model
		poseStack.translate(-0.5, -0.5, -0.5);
		this.blockRenderer.getModelRenderer().renderModel(poseStack.last(), buffSource.getBuffer(Sheets.solidBlockSheet()), egg.getBlockState(), model, 1.0F, 1.0F, 1.0F, packedLight, packedOverlay, ModelData.EMPTY, RenderType.solid());

		// Renders the outline bounding box
		if (Minecraft.getInstance().hitResult instanceof BlockHitResult blockHit && blockHit.getBlockPos().equals(egg.getBlockPos()) && !Minecraft.getInstance().options.hideGui)
		{
			poseStack.pushPose();
			VertexConsumer outline = buffSource.getBuffer(RenderType.lines());
			// Camera pos and block position are zero because this is already translated to the position of the block entity.
			RediscoveredRendering.renderHitOutline(poseStack, outline, Minecraft.getInstance().cameraEntity, 0, 0, 0, BlockPos.ZERO, egg.getBlockState());
			poseStack.popPose();
		}

		poseStack.popPose();
	}

	@Override
	public int getViewDistance()
	{
		// Allow seeing this from farther away. There won't be many of them, so we can get away with this
		return Minecraft.getInstance().options.renderDistance().get() * 16;
	}
}