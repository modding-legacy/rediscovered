package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.FishModel;
import com.legacy.rediscovered.entity.FishEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class FishRenderer extends MobRenderer<FishEntity, FishModel<FishEntity>>
{
	private static final ResourceLocation FISH_LOCATION = RediscoveredMod.locate("textures/entity/fish.png");

	public FishRenderer(EntityRendererProvider.Context manager)
	{
		super(manager, new FishModel<>(manager.bakeLayer(RediscoveredRenderRefs.FISH)), 0.3F);
	}

	@Override
	protected void scale(FishEntity entity, PoseStack pose, float partialTicks)
	{
		pose.translate(0, 0, !entity.isInWater() ? 0.15F : 0.07F);
	}

	@Override
	public ResourceLocation getTextureLocation(FishEntity entity)
	{
		return FISH_LOCATION;
	}

	@Override
	protected void setupRotations(FishEntity entity, PoseStack pose, float ageInTicks, float rotationYaw, float partialTicks)
	{
		super.setupRotations(entity, pose, ageInTicks, rotationYaw, partialTicks);

		if (!entity.isInWater())
		{
			float f = 7.0F * Mth.sin((0.6F * 2) * ageInTicks);
			pose.mulPose(Axis.YP.rotationDegrees(f));

			pose.translate((double) 0.1F, (double) 0.1F, (double) -0.1F);
			pose.mulPose(Axis.ZP.rotationDegrees(90.0F));
		}

		float a = 1.0F;

		if (!entity.isInWater())
			a = 1.5F;

		float f = a * 4.3F * Mth.cos(0.6F * ageInTicks);
		pose.mulPose(Axis.YP.rotationDegrees(f));

	}
}