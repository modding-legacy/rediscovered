package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.legacy.rediscovered.entity.dragon.BoltBallEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.resources.model.Material;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.UniformFloat;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BoltBallRenderer<T extends BoltBallEntity> extends EntityRenderer<T>
{
	public static final Material WIND_TEXTURE = new Material(TextureAtlas.LOCATION_BLOCKS, RediscoveredMod.locate("entity/bolt_ball/wind"));

	private final ModelPart wind;
	private final RediscoveredRendering.Bolt bolt = createBolt();

	public BoltBallRenderer(EntityRendererProvider.Context pContext)
	{
		super(pContext);
		this.wind = pContext.bakeLayer(RediscoveredRenderRefs.BOLT_BALL_WIND);
	}

	public static RediscoveredRendering.Bolt createBolt()
	{
		//@formatter:off
		return RediscoveredRendering.bolt()
		.layers(2)
		.bolts(10)
		.startScale(0.06F)
		.endScale(0.001F)
		.centerSpacing(0)
		.length(1.0F, 1.5F)
		.turns(3, 5)
		.color(UniformFloat.of(0.2F, 0.6F), UniformFloat.of(0.65F, 0.85F), UniformFloat.of(0.80F, 1.0F), ConstantFloat.of(0.30F));
		//@formatter:on
	}

	@Override
	protected int getBlockLightLevel(T pEntity, BlockPos pPos)
	{
		return 15;
	}

	@Override
	public void render(T entity, float entityYaw, float partialTicks, PoseStack pose, MultiBufferSource buffSource, int packedLight)
	{
		float t = entity.tickCount + partialTicks;
		float centerY = entity.getBbHeight() / 2.0F;
		
		// Lightning effect
		pose.pushPose();
		pose.translate(0, centerY, 0);
		this.bolt.render(pose, buffSource, entity.getSeed());
		pose.popPose();

		float windScale = 2.0F;
		float wobbleSpeed = 0.5F;
		float wobbleScale = 0.3F;
		float windSpeed = 23.0F;
		float windAlpha = 0.6F;

		// Center wind ball
		pose.pushPose();
		pose.translate(0, centerY, 0);
		pose.scale(windScale + (float) Math.sin(t * wobbleSpeed) * wobbleScale, windScale, windScale + (float) Math.cos(t * wobbleSpeed) * wobbleScale);
		VertexConsumer vertexconsumer = WIND_TEXTURE.buffer(buffSource, RediscoveredRenderType::sortedEntityTranslucent);
		pose.mulPose(Axis.ZP.rotationDegrees((float) Math.sin(t) * 5F));
		pose.mulPose(Axis.YP.rotationDegrees(t * windSpeed));
		this.wind.render(pose, vertexconsumer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, windAlpha);
		pose.popPose();

		windScale = 1.3F;
		float offset = 0.7F;
		
		// Top wind ball
		pose.pushPose();
		pose.translate(0, centerY + offset, 0);
		pose.scale(windScale + (float) Math.sin(t * wobbleSpeed) * wobbleScale, windScale, windScale + (float) Math.cos(t * wobbleSpeed) * wobbleScale);
		pose.mulPose(Axis.ZP.rotationDegrees((float) Math.cos(t) * 5F));
		pose.mulPose(Axis.YP.rotationDegrees(t * -windSpeed * 1.3F));
		pose.mulPose(Axis.XP.rotationDegrees(180));
		this.wind.render(pose, vertexconsumer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, windAlpha);
		pose.popPose();

		// Bottom wind ball
		pose.pushPose();
		pose.translate(0, centerY - offset, 0);
		pose.scale(windScale + (float) Math.sin(t * wobbleSpeed) * wobbleScale, windScale, windScale + (float) Math.cos(t * wobbleSpeed) * wobbleScale);
		pose.mulPose(Axis.ZP.rotationDegrees((float) Math.cos(t) * 5F));
		pose.mulPose(Axis.YP.rotationDegrees(t * -windSpeed * 1.2F));
		pose.mulPose(Axis.XP.rotationDegrees(180));
		this.wind.render(pose, vertexconsumer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, windAlpha);
		pose.popPose();

		super.render(entity, entityYaw, partialTicks, pose, buffSource, packedLight);
	}

	public static LayerDefinition createWindLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();
		partdefinition.addOrReplaceChild("wind", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -8.0F, -8.0F, 16.0F, 16.0F, 16.0F), PartPose.ZERO);
		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	public ResourceLocation getTextureLocation(T pEntity)
	{
		return TextureAtlas.LOCATION_BLOCKS;
	}
}