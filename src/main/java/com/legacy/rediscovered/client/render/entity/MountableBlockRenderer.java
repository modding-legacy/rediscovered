package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.entity.util.MountableBlockEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class MountableBlockRenderer extends EntityRenderer<MountableBlockEntity>
{
	public MountableBlockRenderer(EntityRendererProvider.Context renderer)
	{
		super(renderer);
	}

	@Override
	public void render(MountableBlockEntity entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
	{
	}

	@Override
	public boolean shouldRender(MountableBlockEntity livingEntityIn, Frustum camera, double camX, double camY, double camZ)
	{
		return false;
	}

	@Override
	public ResourceLocation getTextureLocation(MountableBlockEntity entity)
	{
		return new ResourceLocation("textures/entity/pig/pig.png");
	}
}