package com.legacy.rediscovered.client.render;

import org.joml.Matrix4f;

import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.FloatProvider;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.util.valueproviders.UniformFloat;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class RediscoveredRendering
{
	public static Bolt bolt()
	{
		return new Bolt();
	}

	public static class Bolt
	{
		IntProvider boltsRange;
		FloatProvider centerSpacingRange;
		FloatProvider lengthRange;
		IntProvider turnsRange;
		int layers;
		FloatProvider startScaleRange;
		FloatProvider endScaleRange;
		FloatProvider yRot, xRot, zRot;
		FloatProvider r, g, b, a;
		boolean limitedChaos;
		boolean recenterAtTip;

		private Bolt()
		{
			boltsRange = UniformInt.of(9, 11);
			centerSpacingRange = ConstantFloat.ZERO;
			lengthRange = UniformFloat.of(1, 2);
			turnsRange = UniformInt.of(3, 4);
			layers = 3;
			startScaleRange = UniformFloat.of(0.15F, 0.25F);
			endScaleRange = UniformFloat.of(0.4F, 0.6F);
			xRot = UniformFloat.of(0, 360);
			yRot = UniformFloat.of(0, 360);
			zRot = UniformFloat.of(0, 360);
			r = ConstantFloat.of(1.0F);
			g = ConstantFloat.of(1.0F);
			b = ConstantFloat.of(1.0F);
			a = ConstantFloat.of(0.25F);
			limitedChaos = false;
			recenterAtTip = false;
		}

		public Bolt bolts(int min, int max)
		{
			boltsRange = min == max ? ConstantInt.of(min) : UniformInt.of(min, max);
			return this;
		}

		public Bolt bolts(int val)
		{
			boltsRange = ConstantInt.of(val);
			return this;
		}

		public Bolt centerSpacing(float min, float max)
		{
			centerSpacingRange = min == max ? ConstantFloat.of(min) : UniformFloat.of(min, max);
			return this;
		}

		public Bolt centerSpacing(float val)
		{
			centerSpacingRange = ConstantFloat.of(val);
			return this;
		}

		public Bolt length(float min, float max)
		{
			lengthRange = min == max ? ConstantFloat.of(min) : UniformFloat.of(min, max);
			return this;
		}

		public Bolt length(float val)
		{
			lengthRange = ConstantFloat.of(val);
			return this;
		}

		public Bolt turns(int min, int max)
		{
			turnsRange = min == max ? ConstantInt.of(min) : UniformInt.of(min, max);
			return this;
		}

		public Bolt turns(int val)
		{
			turnsRange = ConstantInt.of(val);
			return this;
		}

		public Bolt layers(int layers)
		{
			this.layers = layers;
			return this;
		}

		public Bolt startScale(float min, float max)
		{
			startScaleRange = min == max ? ConstantFloat.of(min) : UniformFloat.of(min, max);
			return this;
		}

		public Bolt startScale(float val)
		{
			startScaleRange = ConstantFloat.of(val);
			return this;
		}

		public Bolt endScale(float min, float max)
		{
			endScaleRange = min == max ? ConstantFloat.of(min) : UniformFloat.of(min, max);
			return this;
		}

		public Bolt endScale(float val)
		{
			endScaleRange = ConstantFloat.of(val);
			return this;
		}

		public Bolt rotationDegrees(FloatProvider x, FloatProvider y, FloatProvider z)
		{
			this.xRot = x;
			this.yRot = y;
			this.zRot = z;
			return this;
		}

		public Bolt color(FloatProvider r, FloatProvider g, FloatProvider b, FloatProvider a)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
			return this;
		}

		/**
		 * CHAOS CONTROL! -Obama
		 */
		public Bolt limitedChaos()
		{
			this.limitedChaos = true;
			return this;
		}

		public Bolt recenterAtTip()
		{
			this.recenterAtTip = true;
			return this;
		}

		public void render(PoseStack poseStack, MultiBufferSource buffSource, long seed)
		{
			this.render(poseStack, buffSource.getBuffer(RediscoveredRenderType.bolt()), seed);
		}

		public void render(PoseStack poseStack, VertexConsumer buff, long seed)
		{
			poseStack.pushPose();
			RandomSource rand = RandomSource.create(seed);
			int bolts = this.boltsRange.sample(rand);

			for (int l = 1; l <= this.layers; l++)
			{
				for (int i = 0; i < bolts; i++)
				{
					poseStack.pushPose();
					RandomSource boltRand = RandomSource.create(seed + i);
					int turns = this.turnsRange.sample(boltRand);
					float length = this.lengthRange.sample(boltRand);
					float scale = this.startScaleRange.sample(boltRand);
					float endScale = this.endScaleRange.sample(boltRand);
					float centerSpacing = this.centerSpacingRange.sample(boltRand);
					float r = this.r.sample(boltRand);
					float g = this.g.sample(boltRand);
					float b = this.b.sample(boltRand);
					float a = this.a.sample(boltRand);

					float chaos = length / turns;

					Vec3[] points = new Vec3[turns + 1];
					Vec3 lastEnd = Vec3.ZERO.add(0, centerSpacing, 0);
					float destY = (float) lastEnd.y + length;
					points[0] = lastEnd;
					for (int t = 1; t <= turns; t++)
					{
						boolean tip = t == turns;
						float c = this.limitedChaos ? chaos * (1.0F - (t / (float) turns)) : chaos;
						Vec3 end = new Vec3(tip && this.recenterAtTip ? 0 : lastEnd.x() + (boltRand.nextFloat() - 0.5F) * c, tip ? destY : lastEnd.y + (boltRand.nextFloat() * chaos * 1.75), tip && this.recenterAtTip ? 0 : lastEnd.z() + (boltRand.nextFloat() - 0.5F) * c);
						points[t] = end;
						lastEnd = end;
					}

					poseStack.mulPose(Axis.YP.rotationDegrees(this.yRot.sample(boltRand)));
					poseStack.mulPose(Axis.ZP.rotationDegrees(this.zRot.sample(boltRand)));
					poseStack.mulPose(Axis.XP.rotationDegrees(this.xRot.sample(boltRand)));

					float scaling = scale;
					for (int t = 0; t < turns; t++)
					{
						float eScale = Mth.lerp(t / (float) turns, scaling, endScale);
						Vec3 start = points[t];
						Vec3 end = points[t + 1];
						makeTube(poseStack, buff, start, end, scaling * l, eScale * l, r, g, b, a);
						scaling = eScale;
					}
					poseStack.popPose();
				}
			}
			poseStack.popPose();
		}

		public static final ParticleRenderType PARTICLE_RENDER_TYPE = new ParticleRenderType()
		{
			@Override
			public void begin(BufferBuilder buffer, TextureManager textureManager)
			{
				RenderType type = RediscoveredRenderType.bolt();
				type.setupRenderState();
				buffer.begin(type.mode(), type.format());
			}

			@Override
			public void end(Tesselator tesselator)
			{
				tesselator.end();
				RediscoveredRenderType.bolt().clearRenderState();
			}
		};
	}

	public static void makeTube(PoseStack poseStack, VertexConsumer buffer, Vec3 start, Vec3 end, float width, float r, float g, float b, float a)
	{
		makeTube(poseStack, buffer, start, end, width, width, r, g, b, a);
	}

	public static void makeTube(PoseStack poseStack, VertexConsumer buffer, Vec3 start, Vec3 end, float startWidth, float endWidth, float r, float g, float b, float a)
	{
		makeTube(poseStack, buffer, (float) start.x(), (float) start.y(), (float) start.z(), (float) end.x(), (float) end.y(), (float) end.z(), startWidth, endWidth, r, g, b, a);
	}

	public static void makeTube(PoseStack poseStack, VertexConsumer buffer, float nX, float nY, float nZ, float xX, float xY, float xZ, float startWidth, float endWidth, float r, float g, float b, float a)
	{
		float sw = startWidth * 0.5F;
		float ew = endWidth * 0.5F;
		Matrix4f matrix = poseStack.last().pose();

		// Negative X
		buffer.vertex(matrix, nX - sw, nY, nZ - sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, nX - sw, nY, nZ + sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX - ew, xY, xZ + ew).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX - ew, xY, xZ - ew).color(r, g, b, a).endVertex();
		// Positive Z
		buffer.vertex(matrix, nX - sw, nY, nZ + sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, nX + sw, nY, nZ + sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX + ew, xY, xZ + ew).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX - ew, xY, xZ + ew).color(r, g, b, a).endVertex();
		// Positive X
		buffer.vertex(matrix, nX + sw, nY, nZ + sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, nX + sw, nY, nZ - sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX + ew, xY, xZ - ew).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX + ew, xY, xZ + ew).color(r, g, b, a).endVertex();
		// Negative Z
		buffer.vertex(matrix, nX + sw, nY, nZ - sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, nX - sw, nY, nZ - sw).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX - ew, xY, xZ - ew).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, xX + ew, xY, xZ - ew).color(r, g, b, a).endVertex();
	}

	public static void makeBox(PoseStack poseStack, VertexConsumer buffer, float mnX, float mnY, float mnZ, float mxX, float mxY, float mxZ, float r, float g, float b, float a)
	{
		Matrix4f matrix = poseStack.last().pose();
		// Negative X
		buffer.vertex(matrix, mnX, mnY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mnY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mxY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mxY, mnZ).color(r, g, b, a).endVertex();
		// Positive Z
		buffer.vertex(matrix, mnX, mnY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mnY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mxY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mxY, mxZ).color(r, g, b, a).endVertex();
		// Positive X
		buffer.vertex(matrix, mxX, mnY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mnY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mxY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mxY, mxZ).color(r, g, b, a).endVertex();
		// Negative Z
		buffer.vertex(matrix, mxX, mnY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mnY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mxY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mxY, mnZ).color(r, g, b, a).endVertex();
		// Bottom
		buffer.vertex(matrix, mnX, mnY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mnY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mnY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mnY, mxZ).color(r, g, b, a).endVertex();
		// Top
		buffer.vertex(matrix, mnX, mxY, mnZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mnX, mxY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mxY, mxZ).color(r, g, b, a).endVertex();
		buffer.vertex(matrix, mxX, mxY, mnZ).color(r, g, b, a).endVertex();
	}

	// This is exactly the code used in LevelRenderer for showing the wireframe around a block
	public static void renderHitOutline(PoseStack poseStack, VertexConsumer vertexConsumer, Entity camerEntity, double camX, double camY, double camZ, BlockPos blockPos, BlockState state)
	{
		renderShape(poseStack, vertexConsumer, state.getShape(Minecraft.getInstance().level, blockPos, CollisionContext.of(camerEntity)), blockPos.getX() - camX, blockPos.getY() - camY, blockPos.getZ() - camZ, 0.0F, 0.0F, 0.0F, 0.4F);
	}

	// This is exactly the code used in LevelRenderer for showing the wireframe around a block
	public static void renderShape(PoseStack poseStack, VertexConsumer pConsumer, VoxelShape pShape, double pX, double pY, double pZ, float pRed, float pGreen, float pBlue, float pAlpha)
	{
		PoseStack.Pose posestack$pose = poseStack.last();
		pShape.forAllEdges((p_234280_, p_234281_, p_234282_, p_234283_, p_234284_, p_234285_) ->
		{
			float f = (float) (p_234283_ - p_234280_);
			float f1 = (float) (p_234284_ - p_234281_);
			float f2 = (float) (p_234285_ - p_234282_);
			float f3 = Mth.sqrt(f * f + f1 * f1 + f2 * f2);
			f /= f3;
			f1 /= f3;
			f2 /= f3;
			pConsumer.vertex(posestack$pose.pose(), (float) (p_234280_ + pX), (float) (p_234281_ + pY), (float) (p_234282_ + pZ)).color(pRed, pGreen, pBlue, pAlpha).normal(posestack$pose.normal(), f, f1, f2).endVertex();
			pConsumer.vertex(posestack$pose.pose(), (float) (p_234283_ + pX), (float) (p_234284_ + pY), (float) (p_234285_ + pZ)).color(pRed, pGreen, pBlue, pAlpha).normal(posestack$pose.normal(), f, f1, f2).endVertex();
		});
	}
}
