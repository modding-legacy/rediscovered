package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.BlackSteveEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class BlackSteveRenderer<T extends BlackSteveEntity> extends AbstractSteveRenderer<T>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/black_steve.png");

	public BlackSteveRenderer(EntityRendererProvider.Context manager)
	{
		super(manager);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}
