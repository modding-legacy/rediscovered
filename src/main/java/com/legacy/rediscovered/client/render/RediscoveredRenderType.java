package com.legacy.rediscovered.client.render;

import java.io.IOException;
import java.util.function.BiFunction;

import com.legacy.rediscovered.RediscoveredMod;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.blaze3d.vertex.VertexFormat.Mode;

import net.minecraft.Util;
import net.minecraft.client.renderer.RenderStateShard;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.ShaderInstance;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceProvider;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.RegisterShadersEvent;

public abstract class RediscoveredRenderType extends RenderType // Extends to gain access to protected fields/methods
{

	public RediscoveredRenderType(String pName, VertexFormat pFormat, Mode pMode, int pBufferSize, boolean pAffectsCrumbling, boolean pSortOnUpload, Runnable pSetupState, Runnable pClearState)
	{
		super(pName, pFormat, pMode, pBufferSize, pAffectsCrumbling, pSortOnUpload, pSetupState, pClearState);
	}

	public static void init(IEventBus modBus)
	{
		modBus.addListener(RediscoveredRenderType::registerShaders);
	}

	protected static void registerShaders(final RegisterShadersEvent event)
	{
		try
		{
			ResourceProvider prov = event.getResourceProvider();
			event.registerShader(new ShaderInstance(prov, RediscoveredMod.locate("fluffy_clouds"), DefaultVertexFormat.POSITION_TEX_COLOR_NORMAL), shader -> rendertypeFluffyCloudsShader = shader);

		}
		catch (IOException e)
		{
			RediscoveredMod.LOGGER.error("Failed to register shaders", e);
			e.printStackTrace();
		}
	}

	private static ShaderInstance rendertypeFluffyCloudsShader;

	/*Copy of RenderType.ENTITY_TRANSLUCENT with .setOutputState(ITEM_ENTITY_TARGET)*/
	private static final BiFunction<ResourceLocation, Boolean, RenderType> SORTED_ENTITY_TRANSLUCENT = Util.memoize((texture, affectsOutline) ->
	{
		RenderType.CompositeState rendertype$compositestate = RenderType.CompositeState.builder().setShaderState(RENDERTYPE_ENTITY_TRANSLUCENT_SHADER).setTextureState(new RenderStateShard.TextureStateShard(texture, false, false)).setTransparencyState(TRANSLUCENT_TRANSPARENCY).setCullState(NO_CULL).setOutputState(ITEM_ENTITY_TARGET).setLightmapState(LIGHTMAP).setOverlayState(OVERLAY).createCompositeState(affectsOutline);
		return create(name("sorted_entity_translucent"), DefaultVertexFormat.NEW_ENTITY, VertexFormat.Mode.QUADS, 256, true, false, rendertype$compositestate);
	});

	private static final BiFunction<ResourceLocation, Boolean, RenderType> SORTED_ENTITY_TRANSLUCENT_CULL = Util.memoize((texture, affectsOutline) ->
	{
		RenderType.CompositeState rendertype$compositestate = RenderType.CompositeState.builder().setShaderState(RENDERTYPE_ENTITY_TRANSLUCENT_SHADER).setTextureState(new RenderStateShard.TextureStateShard(texture, false, false)).setTransparencyState(TRANSLUCENT_TRANSPARENCY).setOutputState(ITEM_ENTITY_TARGET).setLightmapState(LIGHTMAP).setOverlayState(OVERLAY).createCompositeState(affectsOutline);
		return create(name("sorted_entity_translucent_cull"), DefaultVertexFormat.NEW_ENTITY, VertexFormat.Mode.QUADS, 256, true, false, rendertype$compositestate);
	});

	/*Copy of RenderType.ENTITY_TRANSLUCENT with .setOutputState(ITEM_ENTITY_TARGET) and .setTransparencyState(LIGHTNING_TRANSPARENCY)*/
	private static final BiFunction<ResourceLocation, Boolean, RenderType> ENERGY = Util.memoize((texture, affectsOutline) ->
	{
		RenderType.CompositeState rendertype$compositestate = RenderType.CompositeState.builder().setShaderState(RENDERTYPE_ENTITY_TRANSLUCENT_SHADER).setTextureState(new RenderStateShard.TextureStateShard(texture, false, false)).setTransparencyState(LIGHTNING_TRANSPARENCY).setCullState(NO_CULL).setOutputState(ITEM_ENTITY_TARGET).setLightmapState(LIGHTMAP).setOverlayState(OVERLAY).createCompositeState(affectsOutline);
		return create(name("energy"), DefaultVertexFormat.NEW_ENTITY, VertexFormat.Mode.QUADS, 256, true, false, rendertype$compositestate);
	});
	/*Copy of RenderType.LIGHTNING with vertex sorting set to false*/
	private static final RenderType BOLT = create(name("bolt"), DefaultVertexFormat.POSITION_COLOR, VertexFormat.Mode.QUADS, 256, false, false, RenderType.CompositeState.builder().setShaderState(RENDERTYPE_LIGHTNING_SHADER).setWriteMaskState(COLOR_DEPTH_WRITE).setTransparencyState(LIGHTNING_TRANSPARENCY).setOutputState(WEATHER_TARGET).createCompositeState(false));

	public static RenderType sortedEntityTranslucent(ResourceLocation texture)
	{
		return SORTED_ENTITY_TRANSLUCENT.apply(texture, false);
	}

	public static RenderType sortedEntityTranslucentCull(ResourceLocation texture)
	{
		return SORTED_ENTITY_TRANSLUCENT_CULL.apply(texture, false);
	}

	public static RenderType energy(ResourceLocation texture)
	{
		return ENERGY.apply(texture, false);
	}

	public static RenderType bolt()
	{
		return BOLT;
	}

	public static ShaderInstance getFluffyCloudsShader()
	{
		return rendertypeFluffyCloudsShader;
	}

	private static String name(String name)
	{
		return RediscoveredMod.MODID + "_" + name;
	}
}
