package com.legacy.rediscovered.client.render.block_entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.GearBlock.GearFace;
import com.legacy.rediscovered.block_entities.GearBlockEntity;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.GearModel;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.RedStoneWireBlock;

public class GearRenderer implements BlockEntityRenderer<GearBlockEntity>
{
	private final GearModel model;
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/block/gear.png");

	public GearRenderer(BlockEntityRendererProvider.Context context)
	{
		this.model = new GearModel(context.bakeLayer(RediscoveredRenderRefs.GEAR));
	}

	@Override
	public void render(GearBlockEntity gear, float partialTicks, PoseStack poseStack, MultiBufferSource buffSource, int packedLight, int overlay)
	{
		Minecraft mc = Minecraft.getInstance();

		// Render gears
		poseStack.pushPose();
		VertexConsumer gearBuffer = buffSource.getBuffer(RenderType.entityCutout(TEXTURE));
		poseStack.translate(0.5, 0.5, 0.5);
		float gearScale = 1.1F;
		poseStack.scale(gearScale, gearScale, gearScale);

		for (GearFace face : GearFace.values())
		{
			poseStack.pushPose();
			this.rotateToFace(poseStack, face.direction);
			poseStack.mulPose(Axis.YP.rotationDegrees(gear.getAngle(face.direction, partialTicks)));

			// offset to fix z-fighting. Rotate to make pretty
			BlockPos pos = gear.getBlockPos();
			boolean checkerboard = pos.getX() % 2 == 0 ^ pos.getZ() % 2 == 0;
			if (pos.getY() % 2 == 0 ? checkerboard : !checkerboard)
			{
				poseStack.translate(0, 0.001, 0);
				poseStack.mulPose(Axis.YP.rotationDegrees(22.5F));
			}
			if (face.direction.getAxis().isHorizontal())
			{
				poseStack.mulPose(Axis.YP.rotationDegrees(22.5F));
			}
			
			if (gear.getBlockState().getValue(face.stateProperty).exists())
				this.model.renderToBuffer(poseStack, gearBuffer, packedLight, overlay, 1.0F, 1.0F, 1.0F, 1.0F);

			poseStack.popPose();
		}
		poseStack.popPose();

		if (mc.getDebugOverlay().showDebugScreen() && mc.player.isCreative())
		{
			for (GearFace face : GearFace.values())
			{
				if (gear.getBlockState().getValue(face.stateProperty).exists())
				{
					poseStack.pushPose();
					float textScale = 0.02F;
					Vec3i normal = face.direction.getNormal();
					poseStack.translate(0.5, 0.5, 0.5);
					double offset = 0.4;
					poseStack.translate(normal.getX() * offset, normal.getY() * offset, normal.getZ() * offset);
					poseStack.scale(textScale, textScale, textScale);
					this.rotateToFace(poseStack, face.direction);
					poseStack.mulPose(Axis.XP.rotationDegrees(90));
					int power = gear.getPower(face).getPower();
					String powerStr = Integer.toString(power);
					mc.font.drawInBatch(powerStr, mc.font.width(powerStr) * -0.5F, mc.font.lineHeight * -0.5F, RedStoneWireBlock.getColorForPower(Math.abs(power)), false, poseStack.last().pose(), buffSource, Font.DisplayMode.NORMAL, 0, packedLight);
		
					poseStack.popPose();
				}
			}
		}

	}

	private void rotateToFace(PoseStack poseStack, Direction direction)
	{
		switch (direction)
		{
		case NORTH:
			poseStack.mulPose(Axis.XP.rotationDegrees(90));
			break;
		case SOUTH:
			poseStack.mulPose(Axis.ZP.rotationDegrees(180));
			poseStack.mulPose(Axis.XP.rotationDegrees(-90));
			break;
		case EAST:
			poseStack.mulPose(Axis.YP.rotationDegrees(-90));
			poseStack.mulPose(Axis.XP.rotationDegrees(90));

			break;
		case WEST:
			poseStack.mulPose(Axis.YP.rotationDegrees(-90));
			poseStack.mulPose(Axis.ZP.rotationDegrees(180));
			poseStack.mulPose(Axis.XP.rotationDegrees(-90));
			break;
		case UP:
			poseStack.mulPose(Axis.XP.rotationDegrees(180));
			break;
		default:
			break;
		}
	}
}
