package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.entity.layer.PigmanPantsLayer;
import com.legacy.rediscovered.client.render.model.PigmanModel;
import com.legacy.rediscovered.entity.pigman.RangedPigmanEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.resources.ResourceLocation;

public class RangedPigmanRenderer<T extends RangedPigmanEntity, M extends HumanoidModel<T>> extends HumanoidMobRenderer<T, M>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/pigman/pigman.png");

	@SuppressWarnings("unchecked")
	public RangedPigmanRenderer(EntityRendererProvider.Context renderer)
	{
		super(renderer, (M) new PigmanModel<T>(renderer.bakeLayer(RediscoveredRenderRefs.PIGMAN)), 0.5F);
		this.addLayer(new PigmanPantsLayer<>(this, renderer.getModelSet(), RediscoveredRenderRefs.PIGMAN));
		this.addLayer(new HumanoidArmorLayer<>(this, new HumanoidModel<>(renderer.bakeLayer(RediscoveredRenderRefs.PIGMAN_INNER_ARMOR)), new HumanoidModel<>(renderer.bakeLayer(RediscoveredRenderRefs.PIGMAN_OUTER_ARMOR)), renderer.getModelManager()));
	}

	@Override
	protected void scale(T entity, PoseStack stack, float partialTickTime)
	{
		/*if (entity.isAggressive() && entity.getMainHandItem().getItem() instanceof BowItem)
		{
			this.getModel().leftArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
			this.getModel().rightArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
		}
		else
		{
			if (entity.getMainArm() == HumanoidArm.RIGHT)
			{
				this.getModel().rightArmPose = HumanoidModel.ArmPose.ITEM;
				this.getModel().leftArmPose = HumanoidModel.ArmPose.EMPTY;
			}
			else
			{
				this.getModel().rightArmPose = HumanoidModel.ArmPose.EMPTY;
				this.getModel().leftArmPose = HumanoidModel.ArmPose.ITEM;
			}
		}*/
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}

	@Override
	protected boolean isShaking(T entity)
	{
		return super.isShaking(entity) || entity.getConvertTime() > 0;
	}
}