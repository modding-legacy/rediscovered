package com.legacy.rediscovered.client.render.entity;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.ScarecrowModel;
import com.legacy.rediscovered.entity.ScarecrowEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ScarecrowRenderer extends LivingEntityRenderer<ScarecrowEntity, ScarecrowModel<ScarecrowEntity>>
{
	public static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/scarecrow.png");

	public ScarecrowRenderer(EntityRendererProvider.Context manager)
	{
		super(manager, new ScarecrowModel<>(manager.bakeLayer(RediscoveredRenderRefs.SCARECROW)), 0.3F);
	}

	@Override
	public ResourceLocation getTextureLocation(ScarecrowEntity entity)
	{
		return TEXTURE;
	}
	
	@Override
	public void render(ScarecrowEntity entity, float entityYaw, float partialTicks, PoseStack poseStack, MultiBufferSource buffSource, int packedLight)
	{
		super.render(entity, entityYaw, partialTicks, poseStack, buffSource, packedLight);
	
		//this.renderNameTag(entity, Component.literal("Health: " + entity.getHealth()), poseStack, buffSource, packedLight);
	}

	@Override
	protected void setupRotations(ScarecrowEntity entity, PoseStack poseStack, float ageInTicks, float rotationYaw, float partialTicks)
	{
		poseStack.mulPose(Axis.YP.rotationDegrees(180.0F - rotationYaw));
		float f = (float) entity.level().getGameTime() - entity.lastHit + partialTicks;
		if (f < 5.0F)
		{
			poseStack.mulPose(Axis.YP.rotationDegrees(Mth.sin(f / 1.5F * (float) Math.PI) * 2.0F));
		}

	}

	@Override
	protected boolean shouldShowName(ScarecrowEntity entity)
	{
		double d0 = this.entityRenderDispatcher.distanceToSqr(entity);
		float f = entity.isCrouching() ? 32.0F : 64.0F;
		return d0 >= (double) (f * f) ? false : entity.isCustomNameVisible();
	}

	@Nullable
	@Override
	protected RenderType getRenderType(ScarecrowEntity entity, boolean isBodyVisible, boolean isTranslucent, boolean isGlowing)
	{
		return super.getRenderType(entity, isBodyVisible, isTranslucent, isGlowing);
	}
}