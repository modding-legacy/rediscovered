package com.legacy.rediscovered.client.render.entity;

import org.lwjgl.opengl.GL11;

import com.legacy.rediscovered.client.render.IMD3Render;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.phys.Vec3;

public abstract class MD3EntityRenderer<T extends LivingEntity> extends EntityRenderer<T> implements IMD3Render
{
	public MD3EntityRenderer(EntityRendererProvider.Context renderer)
	{
		super(renderer);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return this.getMD3Texture();
	}

	@Override
	public void render(T entityIn, float yaw, float partialTicks, PoseStack matrix, MultiBufferSource buffer, int packedLightIn)
	{
		Vec3 vec3d = Minecraft.getInstance().gameRenderer.getMainCamera().getPosition();
		double camX = vec3d.x();
		double camY = vec3d.y();
		double camZ = vec3d.z();

		double d0 = Mth.lerp((double) partialTicks, entityIn.xOld, entityIn.getX());
		double d1 = Mth.lerp((double) partialTicks, entityIn.yOld, entityIn.getY());
		double d2 = Mth.lerp((double) partialTicks, entityIn.zOld, entityIn.getZ());

		this.render(entityIn, yaw, partialTicks, matrix, buffer, packedLightIn, d0 - camX, d1 - camY, d2 - camZ);
	}

	@SuppressWarnings("deprecation")
	@Override
	public <E extends Entity> void render(E entityIn, float rotationYawIn, float partialTicks, PoseStack matrix, MultiBufferSource buffer, int packedLightIn, double xIn, double yIn, double zIn)
	{
		if (!(entityIn instanceof LivingEntity))
			return;

		LivingEntity steve = (LivingEntity) entityIn;

		Minecraft mc = Minecraft.getInstance();
		Camera renderInfo = mc.gameRenderer.getMainCamera();

		GL11.glPushMatrix();
		double camX = renderInfo.getPosition().x();
		double camY = renderInfo.getPosition().y();
		double camZ = renderInfo.getPosition().z();
		double entityX = entityIn.getX();
		double entityY = entityIn.getY();
		double entityZ = entityIn.getZ();

		// Got it
		GL11.glRotated(renderInfo.getXRot(), 1, 0, 0);
		GL11.glRotated(renderInfo.getYRot(), 0, 1, 0);
		GL11.glTranslated(camX - entityX, -(camY - entityY), camZ - entityZ);
		// Yep. That was literally it. God damnit I broke out my trig book just to do
		// this.

		float f5 = steve.yBodyRotO + (steve.yBodyRot - steve.yBodyRotO) * partialTicks;

		mc.textureManager.bindForSetup(this.getMD3Texture());

		if (steve.deathTime > 0)
		{
			float f = ((float) steve.deathTime + partialTicks - 1.0F) / 20.0F * 1.6F;
			f = Mth.sqrt(f);

			if (f > 1.0F)
			{
				f = 1.0F;
			}

			GL11.glRotatef(f * 90, 1.0F, 0.0F, 0.0F);
		}

		GL11.glRotatef(-f5, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-90F, 1F, 0F, 0F);
		GL11.glScalef(0.02F, -0.02F, 0.02F);

		float rotation = steve.tickCount + partialTicks * getSpeedMultiplier();

		try
		{
			GlStateManager._enableBlend();
			//GlStateManager._disableAlphaTest();
			GlStateManager._enableDepthTest();
			RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			GlStateManager._depthMask(!entityIn.isInvisible());

			int frame1 = (int) rotation % this.getMD3Renderer().getAnimFrames();
			int frame2 = (frame1 + 1) % this.getMD3Renderer().getAnimFrames();

			GlStateManager._depthMask(true);
			GL11.glShadeModel(GL11.GL_SMOOTH);
			GL11.glEnable(GL11.GL_NORMALIZE);

			if (steve.hurtTime > 0 || steve.deathTime > 0)
				GL11.glColor3d(1.0D, 0.5D, 0.5D);

			this.getMD3Renderer().render(frame1, frame2, rotation - (int) rotation);
			GL11.glDisable(GL11.GL_NORMALIZE);

			GlStateManager._depthMask(true);
			GlStateManager._disableBlend();
			//GlStateManager._enableAlphaTest();
			GlStateManager._disableDepthTest();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		GL11.glPopMatrix();
	}
}
