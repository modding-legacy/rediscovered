package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.legacy.rediscovered.entity.dragon.ThunderCloudEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.UniformFloat;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ThunderCloudRenderer<T extends ThunderCloudEntity> extends EntityRenderer<T>
{
	public static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/thunder_cloud/thunder_cloud.png");
	public static final ResourceLocation TEXTURE_MAP = RediscoveredMod.locate("textures/entity/thunder_cloud/thunder_cloud_map.png");

	private final ModelPart cloud;

	public ThunderCloudRenderer(EntityRendererProvider.Context pContext)
	{
		super(pContext);
		this.cloud = pContext.bakeLayer(RediscoveredRenderRefs.THUNDER_CLOUD);
	}

	public static RediscoveredRendering.Bolt createBolt(float length, boolean centered)
	{
		//@formatter:off
		var bolt = RediscoveredRendering.bolt()
		.layers(2)
		.bolts(1)
		.startScale(centered ? 0.3F : 0.2F)
		.endScale(centered ? 0.1F : 0.05F)
		.centerSpacing(0)
		.length(length)
		.turns(5, 7)
		.rotationDegrees(ConstantFloat.ZERO, UniformFloat.of(0, 360), ConstantFloat.of(180))
		.color(UniformFloat.of(0.2F, 0.6F), UniformFloat.of(0.65F, 0.85F), UniformFloat.of(0.80F, 1.0F), ConstantFloat.of(0.30F));
		//@formatter:on
		if (centered)
			bolt.limitedChaos().recenterAtTip();
		return bolt;
	}

	@Override
	protected int getBlockLightLevel(T pEntity, BlockPos pPos)
	{
		return 15;
	}

	@Override
	public void render(T entity, float entityYaw, float partialTicks, PoseStack pose, MultiBufferSource buffSource, int packedLight)
	{
		float boltLength = entity.getStrikeDistance();
		if (boltLength != 0 && entity.getStrikeTime() > 0)
		{
			int boltSeed = (int) (entity.tickCount + entity.getUUID().hashCode()) / 2 * 10;
			createBolt(boltLength / 3, false).render(pose, buffSource, boltSeed * 10);
			createBolt(boltLength / 2, false).render(pose, buffSource, boltSeed * 30);
			createBolt(boltLength, true).render(pose, buffSource, boltSeed);
		}

		float ageInTicks = (float) entity.tickCount + partialTicks;
		pose.pushPose();
		float enterScale = 0.0F + Math.min(1.0F, ageInTicks * 0.03F);
		pose.scale(enterScale, enterScale, enterScale);

		for (int c = 0; c < entity.renderOffsets.size(); ++c)
		{
			Vec3 offset = entity.renderOffsets.get(c);
			int seed = Math.abs(offset.hashCode());
			float offsetScale = 1.0F * Math.min(1.0F, ageInTicks * 0.02F);
			pose.pushPose();
			pose.translate(offset.x * offsetScale, ((entity.getBbHeight() / 2) + offset.y) * offsetScale, offset.z * offsetScale);
			float s = 1F + ((float) (seed % 4) * 0.3F);

			float speed = ageInTicks * (0.2F + ((seed % 8) * 0.02F));
			float speedSeed = seed % 10;
			float distance = 0.2F;
			pose.translate(Math.sin(speed - speedSeed) * distance, Math.sin(speed + speedSeed) * distance, Math.cos(speed - speedSeed) * distance);
			pose.scale(1 + (float) Math.sin((speed / 2) - speedSeed) * distance, 1 + (float) Math.sin((speed / 2) + speedSeed) * distance, 1 + (float) Math.cos((speed / 2) - speedSeed) * distance);

			pose.scale(s, s, s);

			float a = 0.5F;
			this.cloud.render(pose, buffSource.getBuffer(RediscoveredRenderType.sortedEntityTranslucentCull(TEXTURE)), packedLight, OverlayTexture.NO_OVERLAY, 1, 1, 1, a);
			pose.popPose();
		}

		pose.pushPose();
		float scale = 3F;
		float xz = Mth.cos(0.0F + ageInTicks * 0.2F) * 0.1F;
		float y = Mth.sin(0.0F + ageInTicks * 0.2F) * 0.1F;

		float base = 1.0F;
		pose.scale(base + xz, base + y, base + xz);

		pose.scale(scale, scale, scale);

		float alpha = 0.5F;
		this.cloud.render(pose, buffSource.getBuffer(RediscoveredRenderType.sortedEntityTranslucentCull(TEXTURE)), packedLight, OverlayTexture.NO_OVERLAY, 1, 1, 1, alpha);
		pose.popPose();
		pose.popPose();

		super.render(entity, entityYaw, partialTicks, pose, buffSource, packedLight);
	}

	@Override
	public ResourceLocation getTextureLocation(T pEntity)
	{
		return TEXTURE;
	}
}