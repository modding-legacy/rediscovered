package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.SteveEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class SteveRenderer<T extends SteveEntity> extends AbstractSteveRenderer<T>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/steve.png");

	public SteveRenderer(EntityRendererProvider.Context manager)
	{
		super(manager);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}
