package com.legacy.rediscovered.client.render.model;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;

public class SteveModel<T extends LivingEntity> extends HierarchicalModel<T> implements ArmedModel
{
	private final ModelPart root;
	private final ModelPart head;
	private final ModelPart body;
	private final ModelPart right_arm;
	private final ModelPart left_arm;
	private final ModelPart left_leg, lower_left_leg, left_boot;
	private final ModelPart right_leg, lower_right_leg, right_boot;

	public SteveModel(ModelPart root)
	{
		this.root = root;
		this.head = root.getChild("head");
		this.body = root.getChild("body");
		this.right_arm = root.getChild("right_arm");
		this.left_arm = root.getChild("left_arm");
		this.left_leg = root.getChild("left_leg");
		this.lower_left_leg = this.left_leg.getChild("lower_left_leg");
		this.left_boot = this.lower_left_leg.getChild("left_boot");
		this.right_leg = root.getChild("right_leg");
		this.lower_right_leg = this.right_leg.getChild("lower_right_leg");
		this.right_boot = this.lower_right_leg.getChild("right_boot");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-10.0F, -20.0F, -8.5F, 20.0F, 20.0F, 17.0F, new CubeDeformation(0.0F)).texOffs(0, 37).addBox(-10.0F, -20.0F, -8.5F, 20.0F, 20.0F, 17.0F, new CubeDeformation(0.25F)).texOffs(57, 0).addBox(10.0F, -10.0F, -1.0F, 2.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)).texOffs(57, 0).addBox(-12.0F, -10.0F, -1.0F, 2.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -5.0F, 0.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(100, 0).addBox(-5.0F, -6.3333F, -2.1667F, 10.0F, 14.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(100, 18).addBox(-5.0F, -6.3333F, -2.1667F, 10.0F, 14.0F, 4.0F, new CubeDeformation(0.499F)).texOffs(57, 10).addBox(-3.0F, -9.3333F, -1.6667F, 6.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 3.3333F, 0.1667F));

		PartDefinition right_arm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(112, 37).mirror().addBox(-4.0F, -2.0F, -2.0F, 4.0F, 16.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false).texOffs(112, 57).mirror().addBox(-4.0F, -2.0F, -2.0F, 4.0F, 16.0F, 4.0F, new CubeDeformation(0.5F)).mirror(false), PartPose.offset(-5.0F, -1.0F, 0.0F));

		PartDefinition left_arm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(112, 37).addBox(0.0F, -2.0F, -2.0F, 4.0F, 16.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(112, 57).addBox(0.0F, -2.0F, -2.0F, 4.0F, 16.0F, 4.0F, new CubeDeformation(0.5F)), PartPose.offset(5.0F, -1.0F, 0.0F));

		PartDefinition left_leg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(95, 37).addBox(-2.5F, -1.25F, -1.5F, 4.0F, 8.0F, 4.0F, new CubeDeformation(0.001F)), PartPose.offset(3.5F, 11.25F, -0.5F));

		PartDefinition lower_left_leg = left_leg.addOrReplaceChild("lower_left_leg", CubeListBuilder.create().texOffs(95, 49).addBox(-2.0F, 0.0F, 0.0F, 4.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.5F, 6.75F, -1.5F));

		PartDefinition left_boot = lower_left_leg.addOrReplaceChild("left_boot", CubeListBuilder.create().texOffs(95, 59).addBox(1.0F, -6.0F, -2.0F, 4.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(97, 69).addBox(1.0F, -2.0F, -4.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, 6.0F, 2.0F));

		PartDefinition right_leg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(95, 37).mirror().addBox(-1.5F, -1.25F, -1.5F, 4.0F, 8.0F, 4.0F, new CubeDeformation(0.001F)).mirror(false), PartPose.offset(-3.5F, 11.25F, -0.5F));

		PartDefinition lower_right_leg = right_leg.addOrReplaceChild("lower_right_leg", CubeListBuilder.create().texOffs(95, 49).mirror().addBox(-2.0F, 0.0F, 0.0F, 4.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.5F, 6.75F, -1.5F));

		PartDefinition right_boot = lower_right_leg.addOrReplaceChild("right_boot", CubeListBuilder.create().texOffs(97, 69).mirror().addBox(-5.0F, -2.0F, -4.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false).texOffs(95, 59).mirror().addBox(-5.0F, -6.0F, -2.0F, 4.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(3.0F, 6.0F, 2.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.head.resetPose();

		head.yRot = netHeadYaw / 57.29578F;
		head.xRot = headPitch / 57.29578F;

		this.body.resetPose();

		this.right_leg.resetPose();
		this.left_leg.resetPose();
		this.lower_right_leg.resetPose();
		this.lower_left_leg.resetPose();
		this.right_arm.resetPose();
		this.left_arm.resetPose();

		float bootScale = 1.25F;

		this.left_boot.x = -3.75F;
		this.left_boot.y = 6.50F;
		this.left_boot.xScale = bootScale;
		this.left_boot.yScale = bootScale;
		this.left_boot.zScale = bootScale;

		this.right_boot.x = 3.75F;
		this.right_boot.y = 6.50F;
		this.right_boot.xScale = bootScale;
		this.right_boot.yScale = bootScale;
		this.right_boot.zScale = bootScale;

		/*limbSwing = ageInTicks * 0.35F;
		limbSwingAmount = 1.0F;*/

		this.right_arm.zRot += 0.1F;
		this.left_arm.zRot -= 0.1F;
		AnimationUtils.bobArms(this.right_arm, this.left_arm, ageInTicks);

		float range = 0.6662F;
		right_leg.xRot += Mth.cos(limbSwing * range) * limbSwingAmount;
		left_leg.xRot += Mth.cos(limbSwing * range + Mth.PI) * limbSwingAmount;

		float offset = 0.2F;
		lower_right_leg.xRot += Math.max(0, Mth.sin(offset + limbSwing * range) * limbSwingAmount);
		lower_left_leg.xRot += Math.max(0, Mth.sin(offset + limbSwing * range + Mth.PI) * limbSwingAmount);

		left_arm.xRot += Mth.cos(limbSwing * range) * limbSwingAmount;
		right_arm.xRot += Mth.cos(limbSwing * range + Mth.PI) * limbSwingAmount;

		float swing = limbSwingAmount / 2;
		left_arm.zRot += -swing + (Mth.cos(limbSwing * range + Mth.PI) * swing) * -swing;
		right_arm.zRot += swing + (Mth.cos(limbSwing * range) * swing) * swing;

		this.body.xRot += limbSwingAmount * 0.2F;
		this.body.z -= limbSwingAmount * 1.5F;
		this.head.z -= limbSwingAmount * 2.5F;

		boolean rightHanded = entity.getMainArm() == HumanoidArm.RIGHT;

		ItemStack leftItem = rightHanded ? entity.getOffhandItem() : entity.getMainHandItem();
		ItemStack rightItem = rightHanded ? entity.getMainHandItem() : entity.getOffhandItem();

		if (!rightItem.isEmpty())
			this.right_arm.xRot -= 0.2F;

		if (!leftItem.isEmpty())
			this.left_arm.xRot -= 0.2F;

		if (entity.swingingArm == InteractionHand.MAIN_HAND && rightHanded || entity.swingingArm == InteractionHand.OFF_HAND && !rightHanded)
			setupAttackAnimation(entity, ageInTicks, this.right_arm);
		else
			setupAttackAnimation(entity, ageInTicks, this.left_arm);

		this.right_arm.z -= limbSwingAmount * 2.5F;
		this.left_arm.z -= limbSwingAmount * 2.5F;

		this.right_arm.y += limbSwingAmount * 0.5F;
		this.left_arm.y += limbSwingAmount * 0.5F;
	}

	@Override
	public void translateToHand(HumanoidArm pSide, PoseStack pose)
	{
		boolean left = pSide == HumanoidArm.LEFT;
		ModelPart arm = left ? this.left_arm : this.right_arm;

		arm.translateAndRotate(pose);
		pose.translate(0, 0.1F, -0.02F);

		float itemScale = 1.2F;
		pose.scale(itemScale, itemScale, itemScale);
	}

	protected void setupAttackAnimation(T pLivingEntity, float pAgeInTicks, ModelPart modelpart)
	{
		float f = this.attackTime;
		this.body.yRot = Mth.sin(Mth.sqrt(f) * ((float) Math.PI * 2F)) * 0.2F;
		if (modelpart == this.left_arm)
		{
			this.body.yRot *= -1.0F;
		}

		this.right_arm.z = Mth.sin(this.body.yRot) * 5.0F;
		this.right_arm.x = -Mth.cos(this.body.yRot) * 5.0F;
		this.left_arm.z = -Mth.sin(this.body.yRot) * 5.0F;
		this.left_arm.x = Mth.cos(this.body.yRot) * 5.0F;
		this.right_arm.yRot += this.body.yRot;
		this.left_arm.yRot += this.body.yRot;
		this.left_arm.xRot += this.body.yRot;
		f = 1.0F - this.attackTime;
		f *= f;
		f *= f;
		f = 1.0F - f;
		float f1 = Mth.sin(f * (float) Math.PI);
		float f2 = Mth.sin(this.attackTime * (float) Math.PI) * -(this.head.xRot - 0.7F) * 0.75F;
		modelpart.xRot -= f1 * 1.2F + f2;
		modelpart.yRot += this.body.yRot * 2.0F;
		modelpart.zRot += Mth.sin(this.attackTime * (float) Math.PI) * -0.4F;
	}

}