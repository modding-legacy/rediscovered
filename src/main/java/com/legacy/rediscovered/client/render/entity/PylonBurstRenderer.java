package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.client.render.model.DragonPylonRenderer;
import com.legacy.rediscovered.entity.dragon.PylonBurstEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PylonBurstRenderer<T extends PylonBurstEntity> extends EntityRenderer<T>
{
	private final ModelPart shieldPart;

	public PylonBurstRenderer(EntityRendererProvider.Context context)
	{
		super(context);
		this.shieldPart = context.bakeLayer(RediscoveredRenderRefs.DRAGON_PYLON).getChild("cube");
	}

	@Override
	protected int getBlockLightLevel(T pEntity, BlockPos pPos)
	{
		return 15;
	}

	@Override
	public void render(T entity, float pEntityYaw, float partialTicks, PoseStack pose, MultiBufferSource buffSource, int packedLight)
	{
		VertexConsumer pylonCons = DragonPylonRenderer.ENERGY_SHIELD_TEXTURE.buffer(buffSource, RediscoveredRenderType::energy);

		pose.pushPose();
		int hash = Math.abs(entity.getUUID().hashCode());
		float rot = ((float) entity.tickCount + (hash % 360) + partialTicks) * 15.0F;
		float rotSpeed = 1.0F / ((hash % 2) + 1);
		float glassScale = 3;
		pose.translate(0, 2, 0);
		pose.scale(glassScale, glassScale, glassScale);
		DragonPylonRenderer.renderPylonShields(this.shieldPart, pose, pylonCons, LightTexture.FULL_BRIGHT, rot * rotSpeed, 1, 1.07F, 4);
		pose.popPose();

		super.render(entity, pEntityYaw, partialTicks, pose, buffSource, packedLight);
	}

	@Override
	public ResourceLocation getTextureLocation(T pEntity)
	{
		return TextureAtlas.LOCATION_BLOCKS;
	}
}