package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.entity.layer.PigmanProfessionLayer;
import com.legacy.rediscovered.client.render.model.PigmanModel;
import com.legacy.rediscovered.entity.ZombiePigmanEntity;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.resources.ResourceLocation;

public class ZombiePigmanRenderer<T extends ZombiePigmanEntity, M extends PigmanModel<T>> extends HumanoidMobRenderer<T, M>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/zombie_pigman/zombie_pigman.png");

	@SuppressWarnings("unchecked")
	public ZombiePigmanRenderer(EntityRendererProvider.Context renderer)
	{
		super(renderer, (M) new PigmanModel<T>(renderer.bakeLayer(RediscoveredRenderRefs.PIGMAN)), 0.5F);
		this.addLayer(new PigmanProfessionLayer<>(this, renderer.getModelSet(), RediscoveredRenderRefs.ZOMBIE_PIGMAN_PROFESSION, true));
		this.addLayer(new HumanoidArmorLayer<>(this, new HumanoidModel<>(renderer.bakeLayer(RediscoveredRenderRefs.PIGMAN_INNER_ARMOR)), new HumanoidModel<>(renderer.bakeLayer(RediscoveredRenderRefs.PIGMAN_OUTER_ARMOR)), renderer.getModelManager()));
	}

	@Override
	protected boolean isShaking(T pEntity)
	{
		return super.isShaking(pEntity) || pEntity.isConverting();
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}