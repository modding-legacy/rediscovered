package com.legacy.rediscovered.client.render.model;

import com.legacy.rediscovered.entity.FishEntity;

import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class FishModel<T extends FishEntity> extends HierarchicalModel<T>
{
	public ModelPart root;

	public ModelPart body;
	public ModelPart upper_fin;
	public ModelPart back_fin;
	public ModelPart left_fin;
	public ModelPart right_fin;

	public FishModel(ModelPart model)
	{
		this.root = model;
		this.body = model.getChild("body");
		this.upper_fin = model.getChild("upper_fin");
		this.back_fin = model.getChild("back_fin");
		this.left_fin = model.getChild("left_fin");
		this.right_fin = model.getChild("right_fin");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshDefinition = new MeshDefinition();
		PartDefinition partDefinition = meshDefinition.getRoot();

		partDefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0f, -2.0f, 0.0f, 2.0f, 4.0f, 8.0f), PartPose.offsetAndRotation(0.0f, 22.0f, -5.0f, 0.0f, 0.0f, 0.0f));

		partDefinition.addOrReplaceChild("upper_fin", CubeListBuilder.create().texOffs(0, 10).addBox(0.0f, -2.0f, -3.0f, 0.0f, 2.0f, 7.0f), PartPose.offsetAndRotation(0.0f, 20.0f, -1.0f, 0.0f, 0.0f, 0.0f));

		partDefinition.addOrReplaceChild("back_fin", CubeListBuilder.create().texOffs(0, 15).addBox(0.0f, -2.0f, 0.0f, 0.0f, 4.0f, 5.0f), PartPose.offsetAndRotation(0.0f, 22.0f, 3.0f, 0.0f, 0.0f, 0.0f));

		partDefinition.addOrReplaceChild("left_fin", CubeListBuilder.create().texOffs(0, 10).addBox(-0.0002f, -1.0f, 0.0f, 0.0f, 2.0f, 4.0f), PartPose.offsetAndRotation(1.0f, 22.0f, -2.0f, 0.0f, 0.0873f, 0.0f));

		partDefinition.addOrReplaceChild("right_fin", CubeListBuilder.create().texOffs(0, 10).addBox(0.0001f, -1.0f, 0.0f, 0.0f, 2.0f, 4.0f), PartPose.offsetAndRotation(-1.0f, 22.0f, -2.0f, 0.0f, -0.0873f, 0.0f));

		return LayerDefinition.create(meshDefinition, 64, 32);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		float a = 1.0F;

		if (!entity.isInWater())
			a = 1.5F;

		this.back_fin.yRot = -a * 0.35F * Mth.sin(0.6F * ageInTicks);

		this.right_fin.yRot = -a * 0.15F * Mth.cos(0.3F * ageInTicks) - 0.25F;
		this.left_fin.yRot = -this.right_fin.yRot;
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}
}
