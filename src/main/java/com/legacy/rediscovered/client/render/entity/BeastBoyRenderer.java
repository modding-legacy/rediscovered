package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.BeastBoyEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

public class BeastBoyRenderer<T extends BeastBoyEntity> extends AbstractSteveRenderer<T>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/beast_boy.png");

	public BeastBoyRenderer(EntityRendererProvider.Context manager)
	{
		super(manager);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}
