package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.RanaModel;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Mob;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RanaRenderer<T extends Mob> extends MobRenderer<T, RanaModel<T>>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/rana.png");

	public RanaRenderer(EntityRendererProvider.Context context)
	{
		super(context, new RanaModel<>(context.bakeLayer(RediscoveredRenderRefs.RANA)), 0.7F);

		this.addLayer(new ItemInHandLayer<T, RanaModel<T>>(this, context.getItemInHandRenderer()));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float partialTicks)
	{
		float s = 0.8F;
		pose.scale(s, s, s);
		pose.translate(0, -0.05F, 0);
	}

	@Override
	public ResourceLocation getTextureLocation(T pEntity)
	{
		return TEXTURE;
	}
}