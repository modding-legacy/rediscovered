package com.legacy.rediscovered.client.render.block_entity;

import org.joml.Matrix4f;

import com.legacy.rediscovered.block.TableBlock;
import com.legacy.rediscovered.block_entities.TableBlockEntity;
import com.legacy.rediscovered.event.api.RenderTableItemEvent;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.MapItem;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RedStoneWireBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.saveddata.maps.MapItemSavedData;
import net.minecraft.world.phys.AABB;
import net.neoforged.neoforge.common.NeoForge;

public class TableRenderer implements BlockEntityRenderer<TableBlockEntity>
{
	private static final RenderType MAP_BACKGROUND_RENDERTYPE = RenderType.text(new ResourceLocation("textures/map/map_background_checkerboard.png"));
	private final ItemRenderer itemRenderer;

	public TableRenderer(BlockEntityRendererProvider.Context context)
	{
		this.itemRenderer = context.getItemRenderer();
	}

	@Override
	public void render(TableBlockEntity table, float partialTick, PoseStack poseStack, MultiBufferSource buffer, int packedLight, int packedOverlay)
	{
		ItemStack stack = table.getItem();
		if (!stack.isEmpty())
		{
			BlockPos pos = table.getBlockPos();
			Direction facing = table.getFacing();
			float r = -facing.toYRot();
			Level level = Minecraft.getInstance().level;

			poseStack.pushPose();

			if (NeoForge.EVENT_BUS.post(new RenderTableItemEvent(table, partialTick, poseStack, buffer, packedLight, packedOverlay, stack)).isCanceled())
			{
				// Event ran and was cancelled
			}
			else if (stack.is(Items.FILLED_MAP))
			{
				// Map rendering
				boolean increasedScale = false;
				float xOffset = 0.0F, zOffset = 0.0F;
				if (!table.getShape().isSingle())
				{
					BlockState state = table.getBlockState();
					float mapOffset = 2.75F / 16F; // Magic number
					if (state.getValue(TableBlock.NORTH))
						zOffset -= mapOffset;
					if (state.getValue(TableBlock.SOUTH))
						zOffset += mapOffset;
					if (state.getValue(TableBlock.WEST))
						xOffset -= mapOffset;
					if (state.getValue(TableBlock.EAST))
						xOffset += mapOffset;

					increasedScale = table.getShape().growItem();
				}

				float tableScale = (increasedScale ? 13.25F : 10.5F) / 16.0F;
				poseStack.translate(0.5F + xOffset, 0.70F, 0.5F + zOffset);
				poseStack.mulPose(Axis.YP.rotationDegrees(r + 180));
				poseStack.translate(-0.5F * tableScale, 0.0F, -0.5F * tableScale);
				poseStack.mulPose(Axis.XP.rotationDegrees(90.0F));

				float scale = 0.0078125F * tableScale;
				poseStack.scale(scale, scale, scale);

				VertexConsumer backgroundConsumer = buffer.getBuffer(MAP_BACKGROUND_RENDERTYPE);
				Matrix4f projMatrix = poseStack.last().pose();

				boolean xEven = pos.getX() % 2 == 0;
				boolean zEven = pos.getZ() % 2 == 0;
				float backgroundOffset = 0.5F;
				if (xEven && zEven)
					backgroundOffset += 0.1F;
				else if (!xEven && !zEven)
					backgroundOffset -= 0.1F;
				else if (xEven && !zEven)
					backgroundOffset += 0.05F;
				else if (!xEven && zEven)
					backgroundOffset -= 0.05F;

				backgroundConsumer.vertex(projMatrix, -7.0F, 135.0F, backgroundOffset).color(255, 255, 255, 255).uv(0.0F, 1.0F).uv2(packedLight).endVertex();
				backgroundConsumer.vertex(projMatrix, 135.0F, 135.0F, backgroundOffset).color(255, 255, 255, 255).uv(1.0F, 1.0F).uv2(packedLight).endVertex();
				backgroundConsumer.vertex(projMatrix, 135.0F, -7.0F, backgroundOffset).color(255, 255, 255, 255).uv(1.0F, 0.0F).uv2(packedLight).endVertex();
				backgroundConsumer.vertex(projMatrix, -7.0F, -7.0F, backgroundOffset).color(255, 255, 255, 255).uv(0.0F, 0.0F).uv2(packedLight).endVertex();

				MapItemSavedData mapData = MapItem.getSavedData(stack, level);
				Integer mapID = MapItem.getMapId(stack);
				if (mapData != null && mapID != null)
				{
					Minecraft.getInstance().gameRenderer.getMapRenderer().render(poseStack, buffer, mapID, mapData, true, packedLight);
				}
			}
			else
			{
				int seed = (int) pos.asLong();
				var model = this.itemRenderer.getModel(stack, level, null, seed);
				// Render 3d models, like blocks
				if (model.isGui3d() && !stack.is(Items.TRIDENT) && !stack.is(Items.SPYGLASS))
				{
					boolean bed = stack.is(ItemTags.BEDS);
					poseStack.translate(0.5F, 0.81F, 0.5F);
					if (bed)
						poseStack.translate(0.0F, -0.06F, 0.0F);
					poseStack.mulPose(Axis.YP.rotationDegrees(r));
					if (bed)
						poseStack.mulPose(Axis.XP.rotationDegrees(90));
					float scale = 0.5F;
					poseStack.scale(scale, scale, scale);
					this.itemRenderer.renderStatic(stack, ItemDisplayContext.FIXED, packedLight, packedOverlay, poseStack, buffer, table.getLevel(), seed);

				}
				// Render generic items
				else
				{
					int maxStack = stack.getMaxStackSize();
					int count = Math.min(maxStack, 4 * stack.getCount() / maxStack + 1);
					for (int i = 0; i < count; i++)
					{
						float xOffset = i > 0 ? (float) Math.sin(i * 23) * 0.04F : 0;
						float zOffset = i > 0 ? (float) Math.cos(i * 51) * 0.03F : 0;
						poseStack.pushPose();
						poseStack.translate(0.5F + xOffset, 0.7F + (0.031 * i), 0.5F + zOffset);
						poseStack.mulPose(Axis.YP.rotationDegrees(r + (xOffset * 180)));
						poseStack.mulPose(Axis.XP.rotationDegrees(90.0F));
						float scale = 0.5F;
						poseStack.scale(scale, scale, scale);
						this.itemRenderer.renderStatic(stack, ItemDisplayContext.FIXED, packedLight, packedOverlay, poseStack, buffer, table.getLevel(), seed);
						poseStack.popPose();
					}
				}
			}
			poseStack.popPose();
		}
		Minecraft mc = Minecraft.getInstance();
		
		if (mc.getDebugOverlay().showDebugScreen() && mc.player.isCreative())
		{
			poseStack.pushPose();
			Direction facing = table.getFacing();
			float textScale = 0.02F;
			Vec3i normal = facing.getNormal();
			double offset = 0.3;
			poseStack.translate(-normal.getX() * offset + (normal.getX() == 0 ? 0.7 : 0.5), 0.7, -normal.getZ() * offset + (normal.getZ() == 0 ? 0.3 : 0.5));
			poseStack.scale(textScale, textScale, textScale);
		
			poseStack.mulPose(Axis.XP.rotationDegrees(90));
			poseStack.mulPose(Axis.ZP.rotationDegrees(facing.toYRot() - 180));
			String countStr = Integer.toString(stack.getCount());
			mc.font.drawInBatch(countStr, mc.font.width(countStr) * -0.5F, mc.font.lineHeight * -0.5F, RedStoneWireBlock.getColorForPower(8), false, poseStack.last().pose(), buffer, Font.DisplayMode.NORMAL, 0, packedLight);
			poseStack.popPose();
		}
	}
	
	@Override
	public AABB getRenderBoundingBox(TableBlockEntity blockEntity)
	{
		return new AABB(blockEntity.getBlockPos());
	}
}
