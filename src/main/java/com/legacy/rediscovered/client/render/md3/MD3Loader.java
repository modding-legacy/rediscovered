package com.legacy.rediscovered.client.render.md3;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceLocation;

public class MD3Loader
{
    /**
     * The MD3 files are now located under assets/rediscovered/models/entity/*.md3
     * They've been renamed to not include any capital letters so Minecraft doesn't complain.
     * This is done so that the md3 files are not ignored by Gradle at build time.
     *
     * @param md3Location ResourceLocation contianing the path to the MD3 file.
     * @return The MD3 model to be used for the entity.
     * @throws IOException Thrown if the file could not be found by Minecraft
     */
    public final MD3Model load(ResourceLocation md3Location) throws IOException
    {
        InputStream md3Model = Minecraft.getInstance().getResourceManager().getResource(md3Location).get().open();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] byteData = new byte[4096];
        int byteLength;
        while ((byteLength = md3Model.read(byteData)) >= 0)
        {
            byteArrayOutputStream.write(byteData, 0, byteLength);
        }

        md3Model.close();
        byteArrayOutputStream.close();
        ByteBuffer finalMd3Model = ByteBuffer.wrap(byteArrayOutputStream.toByteArray());
        return load(finalMd3Model);
    }

    @SuppressWarnings("unchecked")
    private MD3Model load(ByteBuffer md3Model) throws IOException
    {
        // Vec3Pool vec3Pool = new Vec3Pool(300, 2000);
        md3Model.order(ByteOrder.LITTLE_ENDIAN);
        if (!readString(md3Model, 4).equals("IDP3"))
        {
            throw new IOException("Not a valid MD3 file (bad magic number)");
        }
        MD3Model model = new MD3Model();
        md3Model.getInt();
        readString(md3Model, 64);
        md3Model.getInt();
        int frames = md3Model.getInt();
        System.out.println(frames + " frames");
        int tags = md3Model.getInt();
        int surfaces = md3Model.getInt();
        md3Model.getInt();
        int frameOffset = md3Model.getInt();
        md3Model.getInt();
        int surfaceOffset = md3Model.getInt();
        md3Model.getInt();
        model.animFrames = frames;
        model.frames = new MD3Frame[frames];
        model.tags = new HashMap<String, MD3Tag>();
        model.surfaces = new MD3Surface[surfaces];
        md3Model.position(frameOffset);

        for (int i = 0; i < frames; i++)
        {
            MD3Frame frame = new MD3Frame();
            // frame.min = nextVec3(vec3Pool, var1);
            // frame.max = nextVec3(vec3Pool, var1);
            // frame.origin = nextVec3(vec3Pool, var1);
            frame.radius = Float.valueOf(md3Model.getFloat());
            frame.name = readString(md3Model, 16);
            model.frames[i] = frame;
        }

        MD3Tag[] tagList = new MD3Tag[tags];

        for (int i = 0; i < tags; i++)
        {
            tagList[i] = new MD3Tag(frames);
        }

        for (int i = 0; i < frames; i++)
        {
            for (int j = 0; j < tags; j++)
            {
                MD3Tag var11 = tagList[j];
                var11.name = readString(md3Model, 64);
                // var11.coords[var8] = nextVec3(vec3Pool, var1);
                // var11.c[var8] = nextVec3(vec3Pool, var1);
                // var11.d[var8] = nextVec3(vec3Pool, var1);
                // var11.e[var8] = nextVec3(vec3Pool, var1);
            }
        }

        for (int i = 0; i < tags; i++)
        {
            model.tags.put(tagList[i].name, tagList[i]);
        }

        md3Model.position(surfaceOffset);

        for (int i = 0; i < surfaces; i++)
        {
            model.surfaces[i] = loadSurface(md3Model);
        }

        return model;
    }

    private MD3Surface loadSurface(ByteBuffer md3Model) throws IOException
    {
        int position = md3Model.position();
        if (!readString(md3Model, 4).equals("IDP3"))
        {
            throw new IOException("Not a valid MD3 file (bad surface magic number)");
        }
        String name = readString(md3Model, 64);
        System.out.println("Name: " + name);
        md3Model.getInt();
        int frames = md3Model.getInt();
        int shaderCount = md3Model.getInt();
        int verts = md3Model.getInt();
        int triangles = md3Model.getInt();
        MD3Surface surface = new MD3Surface(triangles, verts, frames);
        int ofsTriangles = md3Model.getInt() + position;
        int ofsShaders = md3Model.getInt() + position;
        int ofsSt = md3Model.getInt() + position;
        position += md3Model.getInt();
        md3Model.getInt();
        surface.verts = verts;
        surface.shaders = new MD3Shader[shaderCount];
        System.out.println("Triangles: " + triangles);
        System.out.println("OFS_SHADERS: " + ofsShaders + " (current location: " + md3Model.position() + ")");
        md3Model.position(ofsShaders);

        for (int i = 0; i < shaderCount; i++)
        {
            MD3Shader shader = new MD3Shader();
            readString(md3Model, 64);
            md3Model.getInt();
            surface.shaders[i] = shader;
        }

        System.out.println("OFS_TRIANGLES: " + ofsTriangles + " (current location: " + md3Model.position() + ")");
        md3Model.position(ofsTriangles);

        for (int i = 0; i < triangles * 3; i++)
        {
            surface.triangles.put(md3Model.getInt());
        }

        System.out.println("OFS_ST: " + ofsSt + " (current location: " + md3Model.position() + ")");
        md3Model.position(ofsSt);

        for (int i = 0; i < verts << 1; i++)
        {
            surface.d.put(md3Model.getFloat());
        }

        System.out.println("OFS_XYZ_NORMAL: " + position + " (current location: " + md3Model.position() + ")");
        md3Model.position(position);

        for (int i = 0; i < verts * frames; i++)
        {
            surface.vertices.put(md3Model.getShort() / 64.0F);
            surface.vertices.put(md3Model.getShort() / 64.0F);
            surface.vertices.put(md3Model.getShort() / 64.0F);
            double var15 = (md3Model.get() & 0xFF) * 3.141592653589793D * 2.0D / 255.0D;
            double var17 = (md3Model.get() & 0xFF) * 3.141592653589793D * 2.0D / 255.0D;
            float var19 = (float) (Math.cos(var17) * Math.sin(var15));
            float var21 = (float) (Math.sin(var17) * Math.sin(var15));
            float var22 = (float) Math.cos(var15);
            surface.normals.put(var19);
            surface.normals.put(var21);
            surface.normals.put(var22);
        }

        return surface;
    }

    // private static Vec3 nextVec3(Vec3Pool vec3Pool, ByteBuffer var0)
    // {
    // float var1 = var0.getFloat();
    // float var2 = var0.getFloat();
    // float var3 = var0.getFloat();
    // return vec3Pool.getVecFromPool(var1, var2, var3);
    // }

    /**
     * Used to read a string from raw data. ONLY USE FOR MD3 MODELS!
     *
     * @param md3Model The MD3 model file to read.
     * @param bytePos The byte position.
     * @return The string from the file.
     */
    private static String readString(ByteBuffer md3Model, int bytePos)
    {
        byte[] bytes = new byte[bytePos];
        md3Model.get(bytes);

        for (int i = 0; i < bytes.length; i++)
        {
            if (bytes[i] == 0)
            {
                return new String(bytes, 0, i);
            }
        }

        return new String(bytes);
    }
}