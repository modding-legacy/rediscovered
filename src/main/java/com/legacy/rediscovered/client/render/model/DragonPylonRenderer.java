package com.legacy.rediscovered.client.render.model;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Quaternionf;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.entity.dragon.DragonPylonEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonBossEntity;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.resources.model.Material;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class DragonPylonRenderer<T extends DragonPylonEntity> extends EntityRenderer<T>
{
	private static final ResourceLocation PYLON_TEXTURE = RediscoveredMod.locate("textures/entity/dragon_pylon/core.png");
	private static final ResourceLocation PYLON_BEAM_TEXTURE = RediscoveredMod.locate("textures/entity/dragon_pylon/beam.png");
	public static final RenderType RENDER_TYPE = RediscoveredRenderType.sortedEntityTranslucent(PYLON_TEXTURE);
	public static final Material ENERGY_SHIELD_TEXTURE = new Material(TextureAtlas.LOCATION_BLOCKS, RediscoveredMod.locate("entity/dragon_pylon/energy_shield"));
	private static final RenderType BEAM_RENDER_TYPE = RediscoveredRenderType.energy(PYLON_BEAM_TEXTURE);
	public static final float SIN_45 = Mth.sin(Mth.PI / 4F);
	private final ModelPart cube;

	public DragonPylonRenderer(EntityRendererProvider.Context context)
	{
		super(context);
		this.shadowRadius = 0.5F;
		ModelPart modelpart = context.bakeLayer(RediscoveredRenderRefs.DRAGON_PYLON);
		this.cube = modelpart.getChild("cube");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();
		partdefinition.addOrReplaceChild("cube", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -4.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.ZERO);
		return LayerDefinition.create(meshdefinition, 32, 16);
	}

	@Override
	public void render(T pylon, float entityYaw, float partialTicks, PoseStack pose, MultiBufferSource buffSource, int packedLight)
	{
		int overlay = pylon.hurtTime > 0 || pylon.isDeadOrDying() ? OverlayTexture.RED_OVERLAY_V : OverlayTexture.NO_OVERLAY;
		float y = getY(pylon.time, partialTicks);
		float yRenderOffset = y / 2.0F;

		float rot = (pylon.time + partialTicks) * 3.0F;
		float scale = 4.0F;
		VertexConsumer solidBuffer = buffSource.getBuffer(RENDER_TYPE);
		pose.pushPose();
		pose.translate(0.0F, 1.5F + yRenderOffset, 0.0F);
		pose.scale(scale, scale, scale);

		float cubeScale = 0.5F;
		pose.scale(cubeScale, cubeScale, cubeScale);
		pose.mulPose(new Quaternionf().setAngleAxis((Mth.PI / 3F), SIN_45, 0.0F, SIN_45));
		pose.mulPose(Axis.YP.rotationDegrees(rot));
		pose.mulPose(Axis.XP.rotationDegrees(rot * 2));
		pose.mulPose(Axis.ZP.rotationDegrees(rot * 3));
		this.cube.render(pose, solidBuffer, LightTexture.FULL_BRIGHT, overlay);

		pose.popPose();

		RedDragonBossEntity dragon = pylon.getAttachedDragon();
		if (dragon != null)
		{
			BlockPos top = pylon.blockPosition().above(6);
			boolean atDungeon = pylon.level().getBlockState(top).is(RediscoveredBlocks.obsidian_bulb);

			double yOff = (atDungeon ? 5 : 0);
			double f3 = Mth.lerp(partialTicks, dragon.xOld, dragon.getX());
			double f4 = Mth.lerp(partialTicks, dragon.yOld, dragon.getY());
			double f5 = Mth.lerp(partialTicks, dragon.zOld, dragon.getZ());
			float f6 = (float) ((double) f3 - pylon.getX());
			float f7 = (float) ((double) f4 - (pylon.getY() + yOff));
			float f8 = (float) ((double) f5 - pylon.getZ());

			// beam that attaches to the dragon
			pose.pushPose();
			pose.translate(f6, f7 + yOff, f8);
			renderPylonBeam(-f6, -f7 + (atDungeon ? 0 : y), -f8, partialTicks, pylon.time, pose, buffSource, LightTexture.FULL_BRIGHT, 270F, 2.5F, 0.3F, 1.0F);
			pose.popPose();

			// beam that attaches to the top from the pylon
			if (atDungeon)
			{
				pose.pushPose();

				f3 = top.getX() + 0.5F;
				f4 = top.getY();
				f5 = top.getZ() + 0.5F;

				f6 = (float) ((double) f3 - pylon.getX());
				f7 = (float) ((double) f4 - pylon.getY());
				f8 = (float) ((double) f5 - pylon.getZ());
				pose.translate(f6, f7, f8);
				renderPylonBeam(-f6, -f7 + yRenderOffset - 0.4F, -f8, partialTicks, pylon.time, pose, buffSource, LightTexture.FULL_BRIGHT, 90F);
				pose.popPose();
			}

		}

		VertexConsumer energyBuffer = ENERGY_SHIELD_TEXTURE.buffer(buffSource, RediscoveredRenderType::energy);
		pose.pushPose();
		pose.translate(0.0F, 1.5F + yRenderOffset, 0.0F);
		pose.scale(scale, scale, scale);

		float glassScale = 1;
		float glassScaleMul = 1.18F;
		int glassLayers = pylon.getLayerCount();
		renderPylonShields(this.cube, pose, energyBuffer, LightTexture.FULL_BRIGHT, rot, glassScale, glassScaleMul, glassLayers);

		pose.popPose();

		super.render(pylon, entityYaw, partialTicks, pose, buffSource, packedLight);
	}

	public static void renderPylonShields(ModelPart cube, PoseStack poseStack, VertexConsumer buffer, int pPackedLight, float rotation, float scale, float scaleMul, int layers)
	{
		renderPylonShields(cube, poseStack, buffer, pPackedLight, rotation, scale, scaleMul, layers, 1.0F);
	}
	
	public static void renderPylonShields(ModelPart cube, PoseStack poseStack, VertexConsumer buffer, int pPackedLight, float rotation, float scale, float scaleMul, int layers, float alpha)
	{
		poseStack.pushPose();
		for (int i = 0; i < layers; i++)
		{
			poseStack.scale(scale, scale, scale);
			poseStack.mulPose(new Quaternionf().setAngleAxis(((float) Math.PI / 3F), SIN_45, 0.0F, SIN_45));
			poseStack.mulPose(Axis.YP.rotationDegrees(-rotation));
			poseStack.mulPose(Axis.XP.rotationDegrees(rotation * 0.4F));
			poseStack.mulPose(Axis.ZP.rotationDegrees(rotation * -0.1F));
			cube.render(poseStack, buffer, pPackedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, alpha);
			scale *= scaleMul;
		}
		poseStack.popPose();
	}

	public static void renderPylonBeam(float x, float y, float z, float partialTick, int tickCount, PoseStack poseStack, MultiBufferSource buffSource, int packedLight, float zRotation)
	{
		renderPylonBeam(x, y, z, partialTick, tickCount, poseStack, buffSource, packedLight, zRotation, 0.2F, 0.8F, 1.0F);
	}

	public static void renderPylonBeam(float x, float y, float z, float partialTick, int tickCount, PoseStack poseStack, MultiBufferSource buffSource, int packedLight, float zRotation, float topWidth, float bottomWidth, float endAlpha)
	{
		float f = Mth.sqrt(x * x + z * z);
		float f1 = Mth.sqrt(x * x + y * y + z * z);
		poseStack.pushPose();
		poseStack.translate(0.0F, 2.0F, 0.0F);
		poseStack.mulPose(Axis.YP.rotation((float) (-Math.atan2(z, x)) - Mth.HALF_PI));
		poseStack.mulPose(Axis.XP.rotation((float) (-Math.atan2(f, y)) - Mth.HALF_PI));
		poseStack.mulPose(Axis.ZP.rotation(zRotation));
		VertexConsumer buffer = buffSource.getBuffer(BEAM_RENDER_TYPE);
		float speed = 0.03F;
		float motion = (tickCount + partialTick) * speed;
		float f2 = 0.0F + motion;
		float f3 = Mth.sqrt(x * x + y * y + z * z) / 32.0F + motion;
		int sides = 6;
		// float topWidth = 0.2F;
		// float bottomWidth = 0.8F;
		float f4 = 0.0F;
		float f5 = 0.75F;
		float f6 = 0.0F;
		PoseStack.Pose posestack$pose = poseStack.last();
		Matrix4f matrix4f = posestack$pose.pose();
		Matrix3f matrix3f = posestack$pose.normal();
		float startColor = 1.0F;
		float startAlpha = 1.0F;
		float endColor = 0.75F;
		// float endAlpha = 1.0F;

		for (int j = 1; j <= sides; ++j)
		{
			float f7 = Mth.sin(j * Mth.TWO_PI / sides) * 0.75F;
			float f8 = Mth.cos(j * Mth.TWO_PI / sides) * 0.75F;
			float f9 = (float) j / sides;
			buffer.vertex(matrix4f, f4 * topWidth, f5 * topWidth, 0.0F).color(endColor, endColor, endColor, endAlpha).uv(f6, f2).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packedLight).normal(matrix3f, 0.0F, -1.0F, 0.0F).endVertex();
			buffer.vertex(matrix4f, f4 * bottomWidth, f5 * bottomWidth, f1).color(startColor, startColor, startColor, startAlpha).uv(f6, f3).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packedLight).normal(matrix3f, 0.0F, -1.0F, 0.0F).endVertex();
			buffer.vertex(matrix4f, f7 * bottomWidth, f8 * bottomWidth, f1).color(startColor, startColor, startColor, startAlpha).uv(f9, f3).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packedLight).normal(matrix3f, 0.0F, -1.0F, 0.0F).endVertex();
			buffer.vertex(matrix4f, f7 * topWidth, f8 * topWidth, 0.0F).color(endColor, endColor, endColor, endAlpha).uv(f9, f2).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packedLight).normal(matrix3f, 0.0F, -1.0F, 0.0F).endVertex();
			f4 = f7;
			f5 = f8;
			f6 = f9;
		}

		poseStack.popPose();
	}

	public static float getY(int time, float pPartialTick)
	{
		float f = (float) time + pPartialTick;
		float f1 = Mth.sin(f * 0.1F) / 2.0F + 1.7F;
		f1 = (f1 * f1 + f1) * 0.2F;
		return f1 - 1.4F;
	}

	@Override
	public ResourceLocation getTextureLocation(T pEntity)
	{
		return PYLON_TEXTURE;
	}

	@Override
	public boolean shouldRender(T pLivingEntity, Frustum pCamera, double pCamX, double pCamY, double pCamZ)
	{
		return super.shouldRender(pLivingEntity, pCamera, pCamX, pCamY, pCamZ) || pLivingEntity.getAttachedDragonUUID().isPresent();
	}
}