package com.legacy.rediscovered.client.render.block_entity;

import org.joml.Quaternionf;

import com.legacy.rediscovered.block_entities.MiniDragonPylonBlockEntity;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.legacy.rediscovered.client.render.model.DragonPylonRenderer;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class MiniDragonPylonRenderer<T extends MiniDragonPylonBlockEntity> implements BlockEntityRenderer<T>
{
	private final ModelPart cube;

	public MiniDragonPylonRenderer(BlockEntityRendererProvider.Context context)
	{
		this.cube = context.bakeLayer(RediscoveredRenderRefs.DRAGON_PYLON).getChild("cube");
	}

	@SuppressWarnings("resource")
	@Override
	public void render(T pylon, float partialTicks, PoseStack poseStack, MultiBufferSource buffSource, int packedLight, int packedOverlay)
	{
		int overlay = OverlayTexture.NO_OVERLAY;
		float y = DragonPylonRenderer.getY(pylon.ticks, partialTicks);
		float yRenderOffset = y / 2.0F;

		float rot = (pylon.ticks + partialTicks) * 3.0F;
		float scale = 3.0F * 0.5F;
		VertexConsumer vertexconsumer = buffSource.getBuffer(DragonPylonRenderer.RENDER_TYPE);
		poseStack.pushPose();
		poseStack.translate(0.5F, 0.7F + yRenderOffset, 0.5F);
		poseStack.scale(scale, scale, scale);
		poseStack.mulPose(new Quaternionf().setAngleAxis((Mth.PI / 3F), DragonPylonRenderer.SIN_45, 0.0F, DragonPylonRenderer.SIN_45));
		poseStack.mulPose(Axis.YP.rotationDegrees(rot));
		poseStack.mulPose(Axis.XP.rotationDegrees(rot * 2));
		poseStack.mulPose(Axis.ZP.rotationDegrees(rot * 3));
		this.cube.render(poseStack, vertexconsumer, LightTexture.FULL_BRIGHT, overlay);

		// Renders the outline bounding box
		if (Minecraft.getInstance().hitResult instanceof BlockHitResult blockHit && blockHit.getBlockPos().equals(pylon.getBlockPos()) && !Minecraft.getInstance().options.hideGui)
		{
			poseStack.pushPose();
			float inversedScale = -1.0F / scale;
			poseStack.scale(inversedScale, inversedScale, inversedScale);
			poseStack.translate(-0.5F, -0.5F, -0.5F);

			VertexConsumer outline = buffSource.getBuffer(RenderType.lines());
			// Camera pos and block position are zero because this is already translated to the position of the block entity.
			RediscoveredRendering.renderHitOutline(poseStack, outline, Minecraft.getInstance().cameraEntity, 0, 0, 0, BlockPos.ZERO, pylon.getBlockState());

			poseStack.popPose();
		}

		poseStack.popPose();

		BlockPos beamPos = pylon.getBeamPos();
		if (beamPos != null)
		{
			poseStack.pushPose();
			Vec3 beamStart = beamPos.getCenter().subtract(pylon.getBlockPos().getCenter()).add(0.5, 0.5, 0.5);
			poseStack.translate(beamStart.x, beamStart.y, beamStart.z);
			DragonPylonRenderer.renderPylonBeam((float) -beamStart.x + 0.5F, (float) -beamStart.y + yRenderOffset - 1.5F, (float) -beamStart.z + 0.5F, partialTicks, pylon.ticks, poseStack, buffSource, packedLight, (pylon.ticks + partialTicks) * 0.11F, 0.7F, 0.8F, 0.5F);
			poseStack.popPose();

			if (pylon.isRespawnMaster())
			{
				poseStack.pushPose();
				float progress = Mth.clamp(pylon.getRespawnProgress(partialTicks) * 3.0F, 0.0F, 1.0F);
				float beamLength = pylon.getRespawnHeight() * progress;
				poseStack.translate(beamStart.x, beamLength + 2, beamStart.z);
				DragonPylonRenderer.renderPylonBeam(0F, -beamLength + 1.0F, 0F, partialTicks, pylon.ticks, poseStack, buffSource, packedLight, (pylon.ticks + partialTicks) * 0.11F);
				poseStack.popPose();
				
				poseStack.pushPose();
				poseStack.translate(beamStart.x, pylon.getRespawnHeight() + 4.0F, beamStart.z);
				float shieldScale = pylon.getRespawnProgress(partialTicks) * 5.0F;
				poseStack.scale(shieldScale, shieldScale, shieldScale);
				VertexConsumer pylonCons = DragonPylonRenderer.ENERGY_SHIELD_TEXTURE.buffer(buffSource, RediscoveredRenderType::energy);
				float shieldRot = ((float) pylon.ticks + partialTicks) * 4.0F;
				DragonPylonRenderer.renderPylonShields(this.cube, poseStack, pylonCons, packedLight, shieldRot, 1.2F, 1.0F, 4);
				poseStack.popPose();
			}
		}
	}
	
	@Override
	public boolean shouldRenderOffScreen(T pylon)
	{
		return pylon.getBeamPos() != null;
	}
	
	@Override
	public int getViewDistance()
	{
		return BlockEntityRenderer.super.getViewDistance() * 3;
	}
	
	@Override
	public AABB getRenderBoundingBox(T pylon)
	{
		if (pylon.getBeamPos() != null)
			return new AABB(pylon.getBlockPos()).inflate(2);
		return INFINITE_EXTENT_AABB;
	}
}