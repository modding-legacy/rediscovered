package com.legacy.rediscovered.client.render.entity;

import java.util.Random;

import org.joml.Matrix4f;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.legacy.rediscovered.client.render.model.DragonPylonRenderer;
import com.legacy.rediscovered.client.render.model.RedDragonModel;
import com.legacy.rediscovered.entity.dragon.RedDragonBossEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.UniformFloat;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RedDragonBossRenderer<T extends RedDragonBossEntity> extends EntityRenderer<T>
{
	private static final ResourceLocation DRAGON_TEXTURES = RediscoveredMod.locate("textures/entity/red_dragon/red_dragon.png");
	private static final ResourceLocation DRAGON_EXPLODING_TEXTURES = new ResourceLocation("textures/entity/enderdragon/dragon_exploding.png");
	private static final ResourceLocation EYES = RediscoveredMod.locate("textures/entity/red_dragon/red_dragon_eyes.png");
	private static final ResourceLocation GLOW_1 = RediscoveredMod.locate("textures/entity/red_dragon/red_dragon_glow_1.png");
	private static final ResourceLocation GLOW_2 = RediscoveredMod.locate("textures/entity/red_dragon/red_dragon_glow_2.png");

	private static final RenderType TEXTURE_CUTOUT = RenderType.entityCutoutNoCull(DRAGON_TEXTURES);
	private static final RenderType TEXTURE_DECAL = RenderType.entityDecal(DRAGON_TEXTURES);
	private static final RenderType EMISSIVE_EYES = RenderType.eyes(EYES);
	private static final RenderType EMISSIVE_GLOW_1 = RenderType.eyes(GLOW_1);
	private static final RenderType EMISSIVE_GLOW_2 = RenderType.eyes(GLOW_2);

	private final RedDragonModel<T> model;
	private final ModelPart shieldPart;

	private RediscoveredRendering.Bolt bolt = createPassiveBolt(), chargeBolt = BoltBallRenderer.createBolt();

	private static final float HALF_SQRT_3 = (float) (Math.sqrt(3.0D) / 2.0D);

	public RedDragonBossRenderer(EntityRendererProvider.Context context)
	{
		super(context);

		this.model = new RedDragonModel<T>(context.bakeLayer(RediscoveredRenderRefs.RED_DRAGON));
		this.shieldPart = context.bakeLayer(RediscoveredRenderRefs.DRAGON_PYLON).getChild("cube");
	}

	@Override
	public void render(T entity, float entityYaw, float partialTicks, PoseStack pose, MultiBufferSource buffSource, int packedLight)
	{
		float boltCharge = entity.boltChargeAnim.getValue(partialTicks);
		float boltShoot = entity.boltShootAnim.getValue(partialTicks);
		float windBlow = entity.windBlowAnim.getValue(partialTicks);
		float cloudPrep = entity.cloudPrepAnim.getValue(partialTicks);

		// bolt = createPassiveBolt();

		if (entity.getPylonTime() <= 0 && entity.passiveBoltSeed > 0)
		{
			pose.pushPose();
			pose.translate(0, 2.5F, 0);
			this.bolt.render(pose, buffSource, entity.passiveBoltSeed);
			pose.popPose();
		}

		pose.pushPose();
		float f = (float) entity.getLatencyPos(7, partialTicks)[0];
		float f1 = (float) (entity.getLatencyPos(5, partialTicks)[1] - entity.getLatencyPos(10, partialTicks)[1]);

		// tilts
		f1 += boltCharge * 3;
		f1 += windBlow * 7;
		f1 += cloudPrep * 4;

		if (boltCharge > 0 && entity.boltChargeAnim.isPlaying())
		{
			float boltScale = boltCharge - boltShoot;
			pose.pushPose();
			pose.mulPose(Axis.YP.rotationDegrees(-f));

			float ua = 3.7F, upward = (ua * boltCharge) - (ua * boltShoot);
			float ba = 2.2F, backward = (ba * boltCharge) - (ba * boltShoot);

			pose.translate(0, 2.1F + upward, 7.7F - (backward * 1.7F));
			pose.scale(boltScale, boltScale, boltScale);

			this.chargeBolt.render(pose, buffSource, entity.chargeBoltSeed);
			pose.popPose();
		}

		pose.mulPose(Axis.YP.rotationDegrees(180F));

		pose.mulPose(Axis.YP.rotationDegrees(-f));
		pose.mulPose(Axis.XP.rotationDegrees(f1 * 10.0F));
		pose.translate(0.0D, 0.0D, 1.0D);
		pose.scale(-1.0F, -1.0F, 1.0F);
		pose.translate(0.0D, (double) -1.501F, 0.0D);
		boolean flag = entity.hurtTime > 0;
		this.model.prepareMobModel(entity, 0, 0, partialTicks);

		/*if (entity instanceof RedDragonBossEntity boss && boss.deathTicks > 0)
		{
			// float f2 = (float) entity.deathTicks / 200.0F;
			VertexConsumer ivertexbuilder = buffer.getBuffer(RenderType.dragonExplosionAlpha(DRAGON_EXPLODING_TEXTURES, f2));
			this.model.renderToBuffer(pose, ivertexbuilder, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
			VertexConsumer ivertexbuilder1 = buffer.getBuffer(TEXTURE_DECAL);
			this.model.renderToBuffer(pose, ivertexbuilder1, packedLight, OverlayTexture.pack(0.0F, flag), 1.0F, 1.0F, 1.0F, 1.0F);
		}
		else*/
		{
			VertexConsumer ivertexbuilder3 = buffSource.getBuffer(TEXTURE_CUTOUT);
			this.model.renderToBuffer(pose, ivertexbuilder3, packedLight, OverlayTexture.pack(0.0F, flag), 1.0F, 1.0F, 1.0F, 1.0F);
		}

		VertexConsumer eyeBuffer = buffSource.getBuffer(EMISSIVE_EYES);
		this.model.renderToBuffer(pose, eyeBuffer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);

		float glow1 = ((float) Math.sin((entity.tickCount + partialTicks) / 1.5) + 1) / 2.0F;
		float glowAlpha = Mth.clamp(boltCharge + boltShoot + windBlow + cloudPrep, 0, 1);
		// glowAlpha = 1.0F;
		float ga1 = glowAlpha * glow1;
		float ga2 = glowAlpha * (1 - glow1);

		// glowAlpha = 1.0F;
		VertexConsumer glowBuffer = buffSource.getBuffer(EMISSIVE_GLOW_2);
		this.model.renderToBuffer(pose, glowBuffer, packedLight, OverlayTexture.NO_OVERLAY, ga1, ga1, ga1, ga1);
		glowBuffer = buffSource.getBuffer(EMISSIVE_GLOW_1);
		this.model.renderToBuffer(pose, glowBuffer, packedLight, OverlayTexture.NO_OVERLAY, ga2, ga2, ga2, ga2);

		this.model.scaleSize = 1.0F + glowAlpha;
		glowAlpha *= 0.8F;
		this.model.renderToBuffer(pose, glowBuffer, packedLight, OverlayTexture.NO_OVERLAY, glowAlpha, glowAlpha, glowAlpha, glowAlpha);
		this.model.scaleSize = -1.0F;

		if (entity.dragonDeathTicks > 0)
		{
			float f5 = ((float) entity.dragonDeathTicks + partialTicks) / 200.0F;
			float f7 = 0.0F;
			if (f5 > 0.8F)
			{
				f7 = (f5 - 0.8F) / 0.2F;
			}

			Random random = new Random(773L);
			VertexConsumer ivertexbuilder2 = buffSource.getBuffer(RenderType.lightning());
			pose.pushPose();
			pose.translate(0.0D, -1.0D, -2.0D);

			for (int i = 0; (float) i < (f5 + f5 * f5) / 2.0F * 60.0F; ++i)
			{
				pose.mulPose(Axis.XP.rotationDegrees(random.nextFloat() * 360.0F));
				pose.mulPose(Axis.YP.rotationDegrees(random.nextFloat() * 360.0F));
				pose.mulPose(Axis.ZP.rotationDegrees(random.nextFloat() * 360.0F));
				pose.mulPose(Axis.XP.rotationDegrees(random.nextFloat() * 360.0F));
				pose.mulPose(Axis.YP.rotationDegrees(random.nextFloat() * 360.0F));
				pose.mulPose(Axis.ZP.rotationDegrees(random.nextFloat() * 360.0F + f5 * 90.0F));
				float f3 = random.nextFloat() * 20.0F + 5.0F + f7 * 10.0F;
				float f4 = random.nextFloat() * 2.0F + 1.0F + f7 * 2.0F;
				Matrix4f matrix4f = pose.last().pose();
				int j = (int) (255.0F * (1.0F - f7));
				defaultVertex(ivertexbuilder2, matrix4f, j);
				vertex2(ivertexbuilder2, matrix4f, f3, f4);
				vertex3(ivertexbuilder2, matrix4f, f3, f4);
				defaultVertex(ivertexbuilder2, matrix4f, j);
				vertex3(ivertexbuilder2, matrix4f, f3, f4);
				vertex4(ivertexbuilder2, matrix4f, f3, f4);
				defaultVertex(ivertexbuilder2, matrix4f, j);
				vertex4(ivertexbuilder2, matrix4f, f3, f4);
				vertex2(ivertexbuilder2, matrix4f, f3, f4);
			}

			pose.popPose();
		}

		pose.popPose();

		if (entity.getPylonTime() > 0)
		{
			VertexConsumer pylonCons = DragonPylonRenderer.ENERGY_SHIELD_TEXTURE.buffer(buffSource, RediscoveredRenderType::energy);

			pose.pushPose();
			float rot = ((float) entity.tickCount + partialTicks) * 4.0F;
			float glassScale = 20;
			pose.translate(0, 2, 0);
			pose.scale(glassScale, glassScale, glassScale);
			DragonPylonRenderer.renderPylonShields(this.shieldPart, pose, pylonCons, LightTexture.FULL_BRIGHT, rot, 1, 1.2F, 2);
			pose.popPose();
		}

		super.render(entity, entityYaw, partialTicks, pose, buffSource, packedLight);
	}

	private static void defaultVertex(VertexConsumer vertexBuilder, Matrix4f matrix, int p_229061_2_)
	{
		vertexBuilder.vertex(matrix, 0.0F, 0.0F, 0.0F).color(255, 255, 255, p_229061_2_).endVertex();
		vertexBuilder.vertex(matrix, 0.0F, 0.0F, 0.0F).color(255, 255, 255, p_229061_2_).endVertex();
	}

	private static void vertex2(VertexConsumer vertexBuilder, Matrix4f matrix, float p_229060_2_, float p_229060_3_)
	{
		vertexBuilder.vertex(matrix, -HALF_SQRT_3 * p_229060_3_, p_229060_2_, -0.5F * p_229060_3_).color(0, 255, 255, 0).endVertex();
	}

	private static void vertex3(VertexConsumer vertexBuilder, Matrix4f matrix, float p_229062_2_, float p_229062_3_)
	{
		vertexBuilder.vertex(matrix, HALF_SQRT_3 * p_229062_3_, p_229062_2_, -0.5F * p_229062_3_).color(0, 255, 255, 0).endVertex();
	}

	private static void vertex4(VertexConsumer vertexBuilder, Matrix4f matrix, float p_229063_2_, float p_229063_3_)
	{
		vertexBuilder.vertex(matrix, 0.0F, p_229063_2_, 1.0F * p_229063_3_).color(0, 255, 255, 0).endVertex();
	}

	public static RediscoveredRendering.Bolt createPassiveBolt()
	{
		//@formatter:off
		return RediscoveredRendering.bolt()
		.layers(3)
		.bolts(3)
		.startScale(0.08F)
		.endScale(0.02F)
		.centerSpacing(2)
		.length(4.0F, 7.0F)
		.turns(3, 5)
		.color(UniformFloat.of(0.2F, 0.6F), UniformFloat.of(0.65F, 0.85F), UniformFloat.of(0.80F, 1.0F), ConstantFloat.of(0.30F));
		//@formatter:on
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return DRAGON_TEXTURES;
	}

	public static float easeInOutExpo(float x)
	{
		return (float) (x == 0 ? 0 : x == 1 ? 1 : x < 0.5 ? Math.pow(2, 20 * x - 10) / 2 : (2 - Math.pow(2, -20 * x + 10)) / 2);

	}

}