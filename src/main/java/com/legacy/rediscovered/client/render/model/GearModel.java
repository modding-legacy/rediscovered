package com.legacy.rediscovered.client.render.model;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;

public class GearModel extends Model
{
	private final ModelPart down;

	public GearModel(ModelPart model)
	{
		super(RenderType::entityCutoutNoCull);

		this.down = model.getChild("down");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshDefinition = new MeshDefinition();
		PartDefinition partDefinition = meshDefinition.getRoot();

		partDefinition.addOrReplaceChild("down", CubeListBuilder.create().texOffs(0, 0).addBox(-8, -6.5f, -8, 16, 0, 16), PartPose.ZERO);

		return LayerDefinition.create(meshDefinition, 16, 16);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
	{
		this.down.render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);

	}
}
