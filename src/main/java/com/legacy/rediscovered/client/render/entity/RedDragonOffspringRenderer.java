package com.legacy.rediscovered.client.render.entity;

import java.util.Optional;

import org.joml.Matrix4f;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.RedDragonModel;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.item.util.DragonArmorTrim;
import com.legacy.rediscovered.item.util.DragonArmorTrim.Decoration;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.structure_gel.core.client.ClientUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.texture.MissingTextureAtlasSprite;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.FastColor;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RedDragonOffspringRenderer<T extends RedDragonOffspringEntity> extends EntityRenderer<T>
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/entity/red_dragon/red_dragon.png");
	private static final ResourceLocation EYES = RediscoveredMod.locate("textures/entity/red_dragon/red_dragon_eyes.png");

	private static final RenderType TEXTURE_CUTOUT = RenderType.entityCutoutNoCull(TEXTURE);
	private static final RenderType EMISSIVE_EYES = RenderType.eyes(EYES);
	private static final ResourceLocation SADDLE = RediscoveredMod.locate("entity/red_dragon/saddle");
	private static final ResourceLocation ARMOR_STRAPS = RediscoveredMod.locate("entity/red_dragon/armor/straps");
	private static final ResourceLocation DEFAULT_ARMOR_CHAIN = RediscoveredMod.locate("entity/red_dragon/armor/default_" + Decoration.CHAIN.getSerializedName());
	private static final ResourceLocation DEFAULT_ARMOR_PLATING = RediscoveredMod.locate("entity/red_dragon/armor/default_" + Decoration.PLATING.getSerializedName());
	private static final ResourceLocation DEFAULT_ARMOR_INLAY = RediscoveredMod.locate("entity/red_dragon/armor/default_" + Decoration.INLAY.getSerializedName());

	private final RedDragonModel<T> model, armorModel, saddleModel;
	private final TextureAtlas armorTrimAtlas;

	public RedDragonOffspringRenderer(EntityRendererProvider.Context context)
	{
		super(context);
		this.model = new RedDragonModel<T>(context.bakeLayer(RediscoveredRenderRefs.RED_DRAGON));
		this.armorModel = new RedDragonModel<T>(context.bakeLayer(RediscoveredRenderRefs.RED_DRAGON_ARMOR));
		this.saddleModel = new RedDragonModel<T>(context.bakeLayer(RediscoveredRenderRefs.RED_DRAGON_SADDLE));
		this.armorTrimAtlas = context.getModelManager().getAtlas(Sheets.ARMOR_TRIMS_SHEET);
	}

	@Override
	public boolean shouldRender(T pLivingEntity, Frustum pCamera, double pCamX, double pCamY, double pCamZ)
	{
		if (super.shouldRender(pLivingEntity, pCamera, pCamX, pCamY, pCamZ))
		{
			return true;
		}
		else
		{
			Entity entity = pLivingEntity.getLeashHolder();
			return entity != null ? pCamera.isVisible(entity.getBoundingBoxForCulling()) : false;
		}
	}

	@SuppressWarnings("resource")
	@Override
	public void render(T entity, float entityYaw, float partialTicks, PoseStack pose, MultiBufferSource buffSource, int packedLight)
	{
		boolean shouldSit = false;
		float walkSpeed = 0.0F;
		float walkPosition = 0.0F;
		if (!shouldSit && entity.isAlive())
		{
			walkSpeed = entity.walkAnimation.speed(partialTicks);
			walkPosition = entity.walkAnimation.position(partialTicks);

			if (entity.isBaby())
				walkPosition *= 3.0F;

			if (walkSpeed > 1.0F)
				walkSpeed = 1.0F;
		}

		float fly = entity.flyingAnim.getValue(partialTicks);
		float flyInv = 1.0F - fly;
		float hover = entity.hoverAnim.getValue(partialTicks) * fly;
		float hoverInv = 1.0F - hover;

		pose.pushPose();
		float ageScale = entity.isBaby() ? 0.4F : 1.0F;
		pose.scale(ageScale, ageScale, ageScale);

		pose.mulPose(Axis.YP.rotationDegrees(180F));

		pose.pushPose();
		float f = (float) Mth.wrapDegrees(entity.getLatencyPos(7, partialTicks)[0]);
		float f1 = (float) (entity.getLatencyPos(5, partialTicks)[1] - entity.getLatencyPos(10, partialTicks)[1]);

		float bodyRot = Mth.wrapDegrees(Mth.rotLerp(partialTicks, entity.yBodyRotO, entity.yBodyRot));

		pose.mulPose(Axis.YP.rotationDegrees((-bodyRot * flyInv) + (-f * fly)));
		pose.mulPose(Axis.XP.rotationDegrees(Mth.clamp(f1 * 10.0F * fly * hoverInv, -90, 90)));

		pose.translate(0.0D, 0.0D, 1.0D);
		pose.scale(-1.0F, -1.0F, 1.0F);
		pose.translate(0.0D, (double) -1.501F, 0.0D);
		boolean isHurt = entity.hurtTime > 0;
		this.model.prepareMobModel(entity, walkPosition, walkSpeed, partialTicks);
		this.armorModel.prepareMobModel(entity, walkPosition, walkSpeed, partialTicks);
		this.saddleModel.prepareMobModel(entity, walkPosition, walkSpeed, partialTicks);

		VertexConsumer modelBuffer = buffSource.getBuffer(TEXTURE_CUTOUT);
		this.model.renderToBuffer(pose, modelBuffer, packedLight, OverlayTexture.pack(0.0F, isHurt), 1.0F, 1.0F, 1.0F, 1.0F);

		VertexConsumer eyeBuffer = buffSource.getBuffer(EMISSIVE_EYES);
		this.model.renderToBuffer(pose, eyeBuffer, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);

		ItemStack armor = entity.getArmor();
		if (armor.is(RediscoveredItems.dragon_armor))
		{
			DragonArmorTrim trim = DragonArmorTrim.getTrim(Minecraft.getInstance().level.registryAccess(), armor).orElse(DragonArmorTrim.EMPTY);
			this.armorModel.renderToBuffer(pose, this.armorTrimAtlas.getSprite(ARMOR_STRAPS).wrap(buffSource.getBuffer(Sheets.armorTrimsSheet(false))), packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
			this.renderArmor(Decoration.CHAIN, trim.chain(), DEFAULT_ARMOR_CHAIN, pose, buffSource, packedLight, isHurt);
			this.renderArmor(Decoration.PLATING, trim.plating(), DEFAULT_ARMOR_PLATING, pose, buffSource, packedLight, isHurt);
			this.renderArmor(Decoration.INLAY, trim.inlay(), DEFAULT_ARMOR_INLAY, pose, buffSource, packedLight, isHurt);
		}

		if (entity.isSaddled())
		{
			this.saddleModel.renderToBuffer(pose, this.armorTrimAtlas.getSprite(SADDLE).wrap(buffSource.getBuffer(Sheets.armorTrimsSheet(false))), packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}

		pose.mulPose(Axis.YN.rotationDegrees(180F));
		pose.popPose();
		pose.popPose();

		super.render(entity, entityYaw, partialTicks, pose, buffSource, packedLight);

		Entity leashHolder = entity.getLeashHolder();

		if (leashHolder != null)
			this.renderLeash(entity, partialTicks, pose, buffSource, leashHolder);
	}

	private void renderArmor(Decoration layer, Optional<Holder<TrimMaterial>> material, ResourceLocation defaultTexture, PoseStack pose, MultiBufferSource buffSource, int packedLight, boolean flag)
	{
		// Relight the model based on Structure Gel's armor trim lighting system
		int light = packedLight;
		if (material.isPresent())
		{
			var key = material.get().unwrapKey();
			int customBlockLight = key.isPresent() ? ClientUtil.materialLight.getOrDefault(key.get(), -1) : -1;
			if (customBlockLight > -1)
				light = LightTexture.pack(Math.max(customBlockLight, LightTexture.block(packedLight)), LightTexture.sky(packedLight));
		}

		// Get the sprite from the armor layer and material used
		ResourceLocation texture = material.isPresent() ? RediscoveredMod.locate("entity/red_dragon/armor/" + layer.getSerializedName() + "_" + material.get().value().assetName()) : defaultTexture;
		TextureAtlasSprite sprite = this.armorTrimAtlas.getSprite(texture);

		// Detect missing texture. Use the uncolored white texture, and apply the armor
		// trim material's text color to it
		float r = 1.0F;
		float g = 1.0F;
		float b = 1.0F;
		if (sprite.contents().name().equals(MissingTextureAtlasSprite.getLocation()))
		{
			sprite = this.armorTrimAtlas.getSprite(RediscoveredMod.locate("entity/red_dragon/armor/" + layer.getSerializedName()));
			if (material.isPresent())
			{
				int color = material.get().value().description().getStyle().getColor().getValue();
				r = FastColor.ARGB32.red(color) / 255.0F;
				g = FastColor.ARGB32.green(color) / 255.0F;
				b = FastColor.ARGB32.blue(color) / 255.0F;
			}
		}

		VertexConsumer buff = sprite.wrap(buffSource.getBuffer(Sheets.armorTrimsSheet(false)));
		this.armorModel.renderToBuffer(pose, buff, light, OverlayTexture.NO_OVERLAY, r, g, b, 1.0F);
	}

	@Override
	protected boolean shouldShowName(T dragon)
	{
		// Show name if it's forced or you're looking at it, and you aren't riding it
		return (dragon.shouldShowName() || dragon.hasCustomName() && dragon == this.entityRenderDispatcher.crosshairPickEntity) && !dragon.getPassengers().contains(Minecraft.getInstance().player);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}

	private <E extends Entity> void renderLeash(T pEntityLiving, float pPartialTicks, PoseStack pPoseStack, MultiBufferSource pBuffer, E pLeashHolder)
	{
		pPoseStack.pushPose();
		Vec3 vec3 = pLeashHolder.getRopeHoldPosition(pPartialTicks);
		double d0 = (double) (Mth.lerp(pPartialTicks, pEntityLiving.yBodyRotO, pEntityLiving.yBodyRot) * ((float) Math.PI / 180F)) + (Math.PI / 2D);
		Vec3 vec31 = pEntityLiving.getLeashOffset(pPartialTicks);
		double d1 = Math.cos(d0) * vec31.z + Math.sin(d0) * vec31.x;
		double d2 = Math.sin(d0) * vec31.z - Math.cos(d0) * vec31.x;
		double d3 = Mth.lerp((double) pPartialTicks, pEntityLiving.xo, pEntityLiving.getX()) + d1;
		double d4 = Mth.lerp((double) pPartialTicks, pEntityLiving.yo, pEntityLiving.getY()) + vec31.y;
		double d5 = Mth.lerp((double) pPartialTicks, pEntityLiving.zo, pEntityLiving.getZ()) + d2;
		pPoseStack.translate(d1, vec31.y, d2);
		float f = (float) (vec3.x - d3);
		float f1 = (float) (vec3.y - d4);
		float f2 = (float) (vec3.z - d5);
		VertexConsumer vertexconsumer = pBuffer.getBuffer(RenderType.leash());
		Matrix4f matrix4f = pPoseStack.last().pose();
		float f4 = Mth.invSqrt(f * f + f2 * f2) * 0.025F / 2.0F;
		float f5 = f2 * f4;
		float f6 = f * f4;
		BlockPos blockpos = BlockPos.containing(pEntityLiving.getEyePosition(pPartialTicks));
		BlockPos blockpos1 = BlockPos.containing(pLeashHolder.getEyePosition(pPartialTicks));
		int i = this.getBlockLightLevel(pEntityLiving, blockpos);

		/*copied directly from MobRenderer, using this instead since the one method is protected*/
		int j = LightTexture.block(this.entityRenderDispatcher.getRenderer(pLeashHolder).getPackedLightCoords(pLeashHolder, pPartialTicks));
		int k = pEntityLiving.level().getBrightness(LightLayer.SKY, blockpos);
		int l = pEntityLiving.level().getBrightness(LightLayer.SKY, blockpos1);

		for (int i1 = 0; i1 <= 24; ++i1)
		{
			MobRenderer.addVertexPair(vertexconsumer, matrix4f, f, f1, f2, i, j, k, l, 0.025F, 0.025F, f5, f6, i1, false);
		}

		for (int j1 = 24; j1 >= 0; --j1)
			MobRenderer.addVertexPair(vertexconsumer, matrix4f, f, f1, f2, i, j, k, l, 0.025F, 0.0F, f5, f6, j1, true);

		pPoseStack.popPose();
	}
}