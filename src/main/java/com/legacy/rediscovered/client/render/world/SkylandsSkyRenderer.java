package com.legacy.rediscovered.client.render.world;

import java.util.Random;

import org.joml.Matrix4f;

import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.BufferBuilder.RenderedBuffer;
import com.mojang.blaze3d.vertex.BufferUploader;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexBuffer;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Axis;

import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.FogRenderer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.ShaderInstance;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.CubicSampler;
import net.minecraft.util.Mth;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.phys.Vec3;

public class SkylandsSkyRenderer
{
	private static final ResourceLocation SUN_TEXTURE = new ResourceLocation("textures/environment/sun.png");
	private static final ResourceLocation MOON_TEXTURES = new ResourceLocation("textures/environment/moon_phases.png");

	private VertexBuffer starBuffer, skyBuffer;

	public static final SkylandsSkyRenderer INSTANCE = new SkylandsSkyRenderer();

	public SkylandsSkyRenderer()
	{
		this.generateStars();
		this.generateSky();
	}

	public float sunriseColors(long timeIn)
	{
		double d0 = Mth.frac((double) timeIn / 24000.0D - 0.25D);
		double d1 = 0.5D - Math.cos(d0 * Math.PI) / 2.0D;
		return (float) (d0 * 2.0D + d1) / 3.0F;
	}

	public static float getOverworldBias(float cameraY, float minBuildHeight)
	{
		if (cameraY <= minBuildHeight)
			return Math.min((cameraY - minBuildHeight) / -64.0F, 1.0F);
		return 0.0F;
	}

	public static Vec3 modifyFogColor(Vec3 fogColor, float fogBrightness)
	{
		Vec3 color = fogColor.multiply(fogBrightness * 0.94F + 0.06F, fogBrightness * 0.94F + 0.06F, fogBrightness * 0.91F + 0.09F);

		Minecraft mc = Minecraft.getInstance();
		float overworldBias = getOverworldBias((float) mc.gameRenderer.getMainCamera().getPosition().y, (float) mc.level.getMinBuildHeight());
		if (overworldBias > 0)
		{
			Biome plainsBiome = mc.level.registryAccess().registryOrThrow(Registries.BIOME).get(Biomes.PLAINS);
			Vec3 plainsColor = Vec3.fromRGB24(plainsBiome != null ? plainsBiome.getFogColor() : 12638463); // The normal plains fog color is the default
			float timeModifier = Mth.clamp(Mth.cos(mc.level.getTimeOfDay(0.0F) * ((float) Math.PI * 2F)) * 2.0F + 0.5F, 0.0F, 1.0F);
			float skylandsBias = 1.0F - overworldBias;
			float m = overworldBias * timeModifier;
			color = color.multiply(skylandsBias, skylandsBias, skylandsBias).add(plainsColor.multiply(m, m, m));
		}

		return color;
	}

	// More or less vanilla with injection. From ClientLevel. This could be a mixin in 1.20.4
	public static Vec3 getSkyColor(Vec3 pPos, float pPartialTick, ClientLevel level)
	{
		float f = level.getTimeOfDay(pPartialTick);
		Vec3 vec3 = pPos.subtract(2.0D, 2.0D, 2.0D).scale(0.25D);
		BiomeManager biomemanager = level.getBiomeManager();
		Vec3 vec31 = CubicSampler.gaussianSampleVec3(vec3, (x, y, z) ->
		{
			return Vec3.fromRGB24(biomemanager.getNoiseBiomeAtQuart(x, y, z).value().getSkyColor());
		});
		// --- INJECT START ---
		float overworldBias = getOverworldBias((float) pPos.y, (float) level.getMinBuildHeight());
		if (overworldBias > 0)
		{
			Biome plainsBiome = level.registryAccess().registryOrThrow(Registries.BIOME).get(Biomes.PLAINS);
			Vec3 plainsColor = Vec3.fromRGB24(plainsBiome != null ? plainsBiome.getSkyColor() : 7907327); // The normal plains biome color is the default
			float skylandsBias = 1.0F - overworldBias;
			vec31 = vec31.multiply(skylandsBias, skylandsBias, skylandsBias).add(plainsColor.multiply(overworldBias, overworldBias, overworldBias));
		}
		// --- INJECT END ---
		
		float f1 = Mth.cos(f * ((float) Math.PI * 2F)) * 2.0F + 0.5F;
		f1 = Mth.clamp(f1, 0.0F, 1.0F);
		float f2 = (float) vec31.x * f1;
		float f3 = (float) vec31.y * f1;
		float f4 = (float) vec31.z * f1;
		float f5 = level.getRainLevel(pPartialTick);
		if (f5 > 0.0F)
		{
			float f6 = (f2 * 0.3F + f3 * 0.59F + f4 * 0.11F) * 0.6F;
			float f7 = 1.0F - f5 * 0.75F;
			f2 = f2 * f7 + f6 * (1.0F - f7);
			f3 = f3 * f7 + f6 * (1.0F - f7);
			f4 = f4 * f7 + f6 * (1.0F - f7);
		}

		float f9 = level.getThunderLevel(pPartialTick);
		if (f9 > 0.0F)
		{
			float f10 = (f2 * 0.3F + f3 * 0.59F + f4 * 0.11F) * 0.2F;
			float f8 = 1.0F - f9 * 0.75F;
			f2 = f2 * f8 + f10 * (1.0F - f8);
			f3 = f3 * f8 + f10 * (1.0F - f8);
			f4 = f4 * f8 + f10 * (1.0F - f8);
		}

		int i = level.getSkyFlashTime();
		if (i > 0)
		{
			float f11 = (float) i - pPartialTick;
			if (f11 > 1.0F)
			{
				f11 = 1.0F;
			}

			f11 *= 0.45F;
			f2 = f2 * (1.0F - f11) + 0.8F * f11;
			f3 = f3 * (1.0F - f11) + 0.8F * f11;
			f4 = f4 * (1.0F - f11) + 1.0F * f11;
		}

		return new Vec3((double) f2, (double) f3, (double) f4);
	}

	public void render(int ticks, float partialTicks, PoseStack pose, ClientLevel level, Camera camera, Matrix4f matrix, Runnable fog)
	{
		fog.run();

		Vec3 skyColor = getSkyColor(camera.getPosition(), partialTicks, level);

		float skyR = (float) skyColor.x;
		float skyG = (float) skyColor.y;
		float skyB = (float) skyColor.z;
		FogRenderer.levelFogColor();
		BufferBuilder bufferbuilder = Tesselator.getInstance().getBuilder();
		RenderSystem.depthMask(false);
		RenderSystem.setShaderColor(skyR, skyG, skyB, 1.0F);
		ShaderInstance shaderinstance = RenderSystem.getShader();
		this.skyBuffer.bind();
		this.skyBuffer.drawWithShader(pose.last().pose(), matrix, shaderinstance);
		VertexBuffer.unbind();
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		float[] afloat = level.effects().getSunriseColor(level.getTimeOfDay(partialTicks), partialTicks);

		if (afloat != null)
		{
			RenderSystem.setShader(GameRenderer::getPositionColorShader);
			RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
			pose.pushPose();
			pose.mulPose(Axis.XP.rotationDegrees(90.0F));
			float f3 = Mth.sin(level.getSunAngle(partialTicks)) < 0.0F ? 180.0F : 0.0F;
			pose.mulPose(Axis.ZP.rotationDegrees(f3));
			pose.mulPose(Axis.ZP.rotationDegrees(90.0F));
			float f4 = afloat[0];
			float f5 = afloat[1];
			float f6 = afloat[2];
			Matrix4f matrix4f = pose.last().pose();
			bufferbuilder.begin(VertexFormat.Mode.TRIANGLE_FAN, DefaultVertexFormat.POSITION_COLOR);
			bufferbuilder.vertex(matrix4f, 0.0F, 100.0F, 0.0F).color(f4, f5, f6, afloat[3]).endVertex();

			for (int j = 0; j <= 16; ++j)
			{
				float f7 = (float) j * ((float) Math.PI * 2F) / 16.0F;
				float f8 = Mth.sin(f7);
				float f9 = Mth.cos(f7);
				bufferbuilder.vertex(matrix4f, f8 * 120.0F, f9 * 120.0F, -f9 * 40.0F * afloat[3]).color(afloat[0], afloat[1], afloat[2], 0.0F).endVertex();
			}

			BufferUploader.drawWithShader(bufferbuilder.end());
			pose.popPose();
		}

		RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		pose.pushPose();
		float rainAlpha = 1.0F - level.getRainLevel(partialTicks);
		RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, rainAlpha);
		pose.mulPose(Axis.YP.rotationDegrees(-90.0F));
		pose.mulPose(Axis.XP.rotationDegrees(level.getTimeOfDay(partialTicks) * 360.0F));
		Matrix4f matrix4f1 = pose.last().pose();
		float f12 = 30.0F;
		RenderSystem.setShader(GameRenderer::getPositionTexShader);
		RenderSystem.setShaderTexture(0, SUN_TEXTURE);
		bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
		bufferbuilder.vertex(matrix4f1, -f12, 100.0F, -f12).uv(0.0F, 0.0F).endVertex();
		bufferbuilder.vertex(matrix4f1, f12, 100.0F, -f12).uv(1.0F, 0.0F).endVertex();
		bufferbuilder.vertex(matrix4f1, f12, 100.0F, f12).uv(1.0F, 1.0F).endVertex();
		bufferbuilder.vertex(matrix4f1, -f12, 100.0F, f12).uv(0.0F, 1.0F).endVertex();
		BufferUploader.drawWithShader(bufferbuilder.end());

		f12 = 20.0F;

		RenderSystem.setShaderTexture(0, MOON_TEXTURES);
		int k = level.getMoonPhase(); // 8 full
		int l = k % 4;
		int i1 = k / 4 % 2;
		float f13 = (float) (l + 0) / 4.0F;
		float f14 = (float) (i1 + 0) / 2.0F;
		float f15 = (float) (l + 1) / 4.0F;
		float f16 = (float) (i1 + 1) / 2.0F;

		bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
		bufferbuilder.vertex(matrix4f1, -f12, -100.0F, f12).uv(f15, f16).endVertex();
		bufferbuilder.vertex(matrix4f1, f12, -100.0F, f12).uv(f13, f16).endVertex();
		bufferbuilder.vertex(matrix4f1, f12, -100.0F, -f12).uv(f13, f14).endVertex();
		bufferbuilder.vertex(matrix4f1, -f12, -100.0F, -f12).uv(f15, f14).endVertex();
		BufferUploader.drawWithShader(bufferbuilder.end());

		float f10 = level.getStarBrightness(partialTicks) * rainAlpha; // star brightness

		if (f10 > 0.0F)
		{
			RenderSystem.setShaderColor(f10, f10, f10, f10);
			FogRenderer.setupNoFog();
			this.starBuffer.bind();
			this.starBuffer.drawWithShader(pose.last().pose(), matrix, GameRenderer.getPositionShader());
			VertexBuffer.unbind();
			fog.run();
		}

		RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.disableBlend();
		pose.popPose();
		RenderSystem.setShaderColor(0.0F, 0.0F, 0.0F, 1.0F);

		if (level.effects().hasGround())
			RenderSystem.setShaderColor(skyR * 0.2F + 0.04F, skyG * 0.2F + 0.04F, skyB * 0.6F + 0.1F, 1.0F);
		else
			RenderSystem.setShaderColor(skyR, skyG, skyB, 1.0F);

		RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.depthMask(true);
	}

	private void generateStars()
	{
		Tesselator tesselator = Tesselator.getInstance();
		BufferBuilder bufferbuilder = tesselator.getBuilder();
		RenderSystem.setShader(GameRenderer::getPositionShader);

		if (this.starBuffer != null)
			this.starBuffer.close();

		this.starBuffer = new VertexBuffer(VertexBuffer.Usage.STATIC);
		BufferBuilder.RenderedBuffer box = this.renderStars(bufferbuilder);
		this.starBuffer.bind();
		this.starBuffer.upload(box);
		VertexBuffer.unbind();
	}

	private RenderedBuffer renderStars(BufferBuilder bufferBuilderIn)
	{
		Random random = new Random("Stormister".hashCode());
		bufferBuilderIn.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION);

		for (int i = 0; i < 1500; ++i)
		{
			double d0 = (double) (random.nextFloat() * 2.0F - 1.0F);
			double d1 = (double) (random.nextFloat() * 2.0F - 1.0F);
			double d2 = (double) (random.nextFloat() * 2.0F - 1.0F);
			double d3 = (double) (0.15F + random.nextFloat() * 0.1F);
			double d4 = d0 * d0 + d1 * d1 + d2 * d2;
			if (d4 < 1.0D && d4 > 0.01D)
			{
				d4 = 1.0D / Math.sqrt(d4);
				d0 = d0 * d4;
				d1 = d1 * d4;
				d2 = d2 * d4;
				double d5 = d0 * 100.0D;
				double d6 = d1 * 100.0D;
				double d7 = d2 * 100.0D;
				double d8 = Math.atan2(d0, d2);
				double d9 = Math.sin(d8);
				double d10 = Math.cos(d8);
				double d11 = Math.atan2(Math.sqrt(d0 * d0 + d2 * d2), d1);
				double d12 = Math.sin(d11);
				double d13 = Math.cos(d11);
				double d14 = random.nextDouble() * Math.PI * 2.0D;
				double d15 = Math.sin(d14);
				double d16 = Math.cos(d14);

				for (int j = 0; j < 4; ++j)
				{
					double d18 = (double) ((j & 2) - 1) * d3;
					double d19 = (double) ((j + 1 & 2) - 1) * d3;
					double d21 = d18 * d16 - d19 * d15;
					double d22 = d19 * d16 + d18 * d15;
					double d23 = d21 * d12 + 0.0D * d13;
					double d24 = 0.0D * d12 - d21 * d13;
					double d25 = d24 * d9 - d22 * d10;
					double d26 = d22 * d9 + d24 * d10;
					bufferBuilderIn.vertex(d5 + d25, d6 + d23, d7 + d26).endVertex();
				}
			}
		}

		return bufferBuilderIn.end();
	}

	private void generateSky()
	{
		Tesselator tesselator = Tesselator.getInstance();
		BufferBuilder bufferbuilder = tesselator.getBuilder();

		if (this.skyBuffer != null)
			this.skyBuffer.close();

		this.skyBuffer = new VertexBuffer(VertexBuffer.Usage.STATIC);
		RenderedBuffer box = buildSkybox(bufferbuilder, 16.0F);
		this.skyBuffer.bind();
		this.skyBuffer.upload(box);
		VertexBuffer.unbind();
	}

	private RenderedBuffer buildSkybox(BufferBuilder bufferBuilderIn, float posY)
	{
		float f = Math.signum(posY) * 512.0F;
		RenderSystem.setShader(GameRenderer::getPositionShader);
		bufferBuilderIn.begin(VertexFormat.Mode.TRIANGLE_FAN, DefaultVertexFormat.POSITION);
		bufferBuilderIn.vertex(0.0D, (double) posY, 0.0D).endVertex();

		for (int i = -180; i <= 180; i += 45)
			bufferBuilderIn.vertex((double) (f * Mth.cos((float) i * ((float) Math.PI / 180F))), (double) posY, (double) (512.0F * Mth.sin((float) i * ((float) Math.PI / 180F)))).endVertex();

		return bufferBuilderIn.end();
	}

	public Float modifyVoidDarkness()
	{
		Level level = Minecraft.getInstance().level;
		if (level.dimension().equals(RediscoveredDimensions.skylandsKey()))
		{
			// Modifies the value returned from ClientLevelData.getClearColorScale so that the math works out to making final the value 1.
			// originalLogic is the value that would normally be multiplied by ClientLevelData.getClearColorScale. By making the equasion (originalLogic * 1 / originalLogic), we're left with 1 for the value that would get used to set the void darkness.
			float originalLogic = (float) Minecraft.getInstance().gameRenderer.getMainCamera().getPosition().y - (float) level.getMinBuildHeight();
			return 1.0F / originalLogic;
		}
		return null;
	}
}
