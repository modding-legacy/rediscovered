package com.legacy.rediscovered.client.render.md3;

import net.minecraft.world.phys.Vec3;

public class MD3Frame
{
    Vec3 min;
    Vec3 max;
    Vec3 origin;
    Float radius;
    String name;
}