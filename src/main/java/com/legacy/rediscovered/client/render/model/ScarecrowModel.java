package com.legacy.rediscovered.client.render.model;

import com.legacy.rediscovered.entity.ScarecrowEntity;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class ScarecrowModel<T extends ScarecrowEntity> extends HierarchicalModel<T>
{
	private final ModelPart root, base, body, head, hat, leftArm, rightArm;

	public ScarecrowModel(ModelPart root)
	{
		this.root = root;
		this.base = root.getChild("base");
		this.body = this.base.getChild("body");
		this.head = this.body.getChild("head");
		this.hat = this.head.getChild("hat");
		this.leftArm = this.body.getChild("left_arm");
		this.rightArm = this.body.getChild("right_arm");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition base = partdefinition.addOrReplaceChild("base", CubeListBuilder.create().texOffs(0, 44).addBox(-1.0F, -16.0F, -1.0F, 2.0F, 16.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition body = base.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 16).addBox(-5.5F, -8.0F, -3.0F, 11.0F, 8.0F, 6.0F, new CubeDeformation(0.0F)).texOffs(0, 34).addBox(-5.0F, 0.0F, -2.5F, 10.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -15.0F, 0.0F));

		PartDefinition left_arm = body.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(34, 21).addBox(-0.5F, -1.025F, -1.0F, 13.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(34, 25).addBox(-0.5F, -1.975F, -2.0F, 9.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(5.5F, -5.975F, 0.0F));

		PartDefinition right_arm = body.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(34, 21).mirror().addBox(-12.5F, -1.025F, -1.0F, 13.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false).texOffs(34, 25).mirror().addBox(-8.5F, -1.975F, -2.0F, 9.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-5.5F, -5.975F, 0.0F));

		PartDefinition head = body.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -8.0F, 0.0F));

		PartDefinition hat = head.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(32, 0).addBox(-4.5F, -1.75F, -4.5F, 9.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)).texOffs(53, 0).addBox(-7.5F, 1.25F, -7.5F, 15.0F, 0.0F, 15.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -6.75F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 64);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float age, float netHeadYaw, float headPitch)
	{
		float d = Mth.DEG_TO_RAD;

		this.base.xRot = -0.2F * d;
		this.base.yRot = 0.0F;
		this.base.zRot = -0.1F * d;
		this.base.y = 24;

		this.body.xRot = 0.15F * d;
		this.body.yRot = 0.0F;
		this.body.zRot = 0.1F * d;
		this.body.y = -15;

		this.head.xRot = 6F * d;
		this.head.yRot = 3F * d;
		this.head.zRot = -3F * d;

		this.hat.xRot = -7F * d;

		this.leftArm.zRot = 5.1F * d;
		this.leftArm.yRot = 1.1F * d;
		this.rightArm.zRot = -5F * d;
		this.rightArm.yRot = -1F * d;

		float partialTicks = age - entity.tickCount;

		if (entity.shouldDance)
		{
			float dance = partialTicks + Minecraft.getInstance().player.tickCount;
			this.base.xRot = (float) Math.sin(dance * 0.07) * 0.2F;
			this.base.zRot = (float) Math.sin(dance * 0.2) * 0.5F;

			this.body.xRot = (float) Math.cos(dance * 0.15) * 0.55F;
			this.body.zRot = -this.base.zRot * 0.4F;

			this.leftArm.zRot = (float) Math.sin(dance * 0.5) * 0.8F;
			this.rightArm.zRot = this.leftArm.zRot;

			this.head.zRot = (float) Math.sin(dance * 0.2) * 0.4F;
			this.head.xRot = (float) Math.sin(dance * 0.3) * 0.2F;

			this.base.y = this.base.y + 1 + (float) Math.sin(dance * 0.4) * 2;
			this.body.y = this.body.y + (float) Math.sin(dance * 0.4) * 2;
		}

		float hit = (float) entity.level().getGameTime() - entity.lastHit + partialTicks;
		if (hit < 5.0F)
		{
			this.base.yRot -= Mth.sin(hit / 1.5F * Mth.PI) * Mth.DEG_TO_RAD * 2.0F;

			float armR = Mth.sin(hit / 3.0F * Mth.PI) * Mth.DEG_TO_RAD * 2.0F;
			this.leftArm.zRot -= armR;
			this.rightArm.zRot += armR;

			this.head.zRot += armR;
		}
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}
}