package com.legacy.rediscovered.client.render.world;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.joml.Matrix4f;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexBuffer;
import com.mojang.blaze3d.vertex.VertexFormat;

import net.minecraft.client.CloudStatus;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.FogRenderer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.ShaderInstance;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;

public class SkylandsCloudRenderer
{
	// Lowest to highest layer
	private static final List<SkylandsCloudRenderer> RENDERERS = new ArrayList<>(List.of(/*Mimics overworld clouds*/new SkylandsCloudRenderer(0, -192, 0, 0.03F, true, -1), new SkylandsCloudRenderer(0, -192.1F, 0, 0.03F, false, 1), new SkylandsCloudRenderer(-80, -40.0F, 120, 0.02F), new SkylandsCloudRenderer(0, 0.0F, 0, 0.03F), new SkylandsCloudRenderer(30, 215, 60, 0.07F), new SkylandsCloudRenderer(-30, 280, -60, 0.05F)));

	private static final SkylandsCloudRenderer VANILLA_OVERRIDE = new SkylandsCloudRenderer(0, 0, 0, 0.03F, true, 0)
	{
		@Override
		public float getHeight()
		{
			return mc().level.effects().getCloudHeight();
		}
	};

	private static final ResourceLocation VANILLA_CLOUDS = new ResourceLocation("textures/environment/clouds.png");
	private static final ResourceLocation CLOUD_SCREEN = RediscoveredMod.locate("textures/environment/cloud_screen.png");
	private static final ResourceLocation CLOUD_FLUFF = RediscoveredMod.locate("textures/environment/fluff.png");

	private final float xOffset, yHeight, zOffset, speedModifier;
	private final boolean fluffy;
	private final int fadeState;

	@Nullable
	private VertexBuffer cloudBuffer;
	private int prevCloudX = Integer.MIN_VALUE;
	private int prevCloudY = Integer.MIN_VALUE;
	private int prevCloudZ = Integer.MIN_VALUE;
	private Vec3 prevCloudColor = Vec3.ZERO;
	@Nullable
	private CloudStatus prevCloudsType;
	private boolean generateClouds = true;

	public SkylandsCloudRenderer(float xOffset, float yHeight, float zOffset, float speedModifier, boolean fluffy, /*-1 fade out, 1 fade in, 0 nothing*/int fadeState)
	{
		this.xOffset = xOffset;
		this.yHeight = yHeight;
		this.zOffset = zOffset;
		this.speedModifier = speedModifier;
		this.fluffy = fluffy;
		this.fadeState = fadeState;
	}

	public SkylandsCloudRenderer(float xOffset, float yHeight, float zOffset, float speedModifier)
	{
		this(xOffset, yHeight, zOffset, speedModifier, true, 0);
	}

	private static boolean wasInMiddle = false;

	public static void renderLayers(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, double camX, double camY, double camZ, Matrix4f projectionMatrix)
	{
		boolean inMiddle = camY < 200 && camY > 15;

		// Sort renderers to draw the furthest first. If we're in the middle, we can ignore this
		if (!inMiddle || inMiddle != wasInMiddle)
			RENDERERS.sort((o1, o2) -> Double.compare(Math.abs(camY - o2.yHeight), Math.abs(camY - o1.yHeight)));

		for (var renderer : RENDERERS)
			renderer.renderClouds(level, ticks, poseStack, projectionMatrix, partialTick, camX, camY, camZ);

		wasInMiddle = inMiddle;
	}

	public boolean useFluffyClouds()
	{
		return (this.fluffy || RediscoveredConfig.CLIENT.overrideVanillaClouds()) && RediscoveredRenderType.getFluffyCloudsShader() != null;
	}

	public static boolean renderCloudOverride(PoseStack poseStack, Matrix4f projectionMatrix, float partialTick, double camX, double camY, double camZ)
	{
		if (RediscoveredConfig.CLIENT.overrideVanillaClouds())
		{
			Minecraft mc = mc();
			VANILLA_OVERRIDE.renderClouds(mc.level, mc.levelRenderer.getTicks(), poseStack, projectionMatrix, partialTick, camX, camY, camZ);
			return true;
		}
		return false;
	}

	public ShaderInstance getShader()
	{
		return this.useFluffyClouds() ? RediscoveredRenderType.getFluffyCloudsShader() : GameRenderer.getPositionTexColorNormalShader();
	}

	public void setShaderTextures()
	{
		if (this.useFluffyClouds())
		{
			RenderSystem.setShaderTexture(0, CLOUD_SCREEN);
			RenderSystem.setShaderTexture(1, CLOUD_FLUFF);
		}
		else
		{
			RenderSystem.setShaderTexture(0, VANILLA_CLOUDS);
		}
	}

	public float getHeight()
	{
		return this.yHeight;
	}

	@SuppressWarnings("resource")
	public void renderClouds(ClientLevel level, int ticks, PoseStack poseStack, Matrix4f projectionMatrix, float partialTick, double camX, double camY, double camZ)
	{
		float height = this.getHeight();
		if (!Float.isNaN(height))
		{
			float xOffset = this.xOffset;
			float zOffset = this.zOffset;
			RenderSystem.disableCull();
			RenderSystem.enableBlend();
			RenderSystem.enableDepthTest();
			RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
			RenderSystem.depthMask(true);
			float f1 = 12.0F;
			float f2 = 4.0F;
			double d0 = 2.0E-4D;
			double d1 = (double) (((float) ticks + partialTick) * this.speedModifier);
			double d2 = (xOffset + camX + d1) / 12.0D;
			double d3 = (double) (height - (float) camY + 0.33F);
			double d4 = zOffset + camZ / 12.0D + (double) 0.33F;
			d2 -= (double) (Mth.floor(d2 / 2048.0D) * 2048);
			d4 -= (double) (Mth.floor(d4 / 2048.0D) * 2048);
			float f3 = (float) (d2 - (double) Mth.floor(d2));
			float f4 = (float) (d3 / 4.0D - (double) Mth.floor(d3 / 4.0D)) * f2;
			float f5 = (float) (d4 - (double) Mth.floor(d4));
			Vec3 vec3 = level.getCloudColor(partialTick);
			int i = (int) Math.floor(d2);
			int j = (int) Math.floor(d3 / 4.0D);
			int k = (int) Math.floor(d4);
			if (i != this.prevCloudX || j != this.prevCloudY || k != this.prevCloudZ || mc().options.getCloudsType() != this.prevCloudsType || this.prevCloudColor.distanceToSqr(vec3) > d0)
			{
				this.prevCloudX = i;
				this.prevCloudY = j;
				this.prevCloudZ = k;
				this.prevCloudColor = vec3;
				this.prevCloudsType = mc().options.getCloudsType();
				this.generateClouds = true;
			}

			if (this.generateClouds)
			{
				this.generateClouds = false;
				BufferBuilder bufferbuilder = Tesselator.getInstance().getBuilder();
				if (this.cloudBuffer != null)
				{
					this.cloudBuffer.close();
				}

				this.cloudBuffer = new VertexBuffer(VertexBuffer.Usage.STATIC);
				BufferBuilder.RenderedBuffer bufferbuilder$renderedbuffer = buildClouds(bufferbuilder, d2, d3, d4, vec3);
				this.cloudBuffer.bind();
				this.cloudBuffer.upload(bufferbuilder$renderedbuffer);
				VertexBuffer.unbind();
			}

			if (this.cloudBuffer != null)
			{
				poseStack.pushPose();
				poseStack.scale(f1, 1.0F, f1);
				poseStack.translate(-f3, f4, -f5);

				float alpha = 1.0F;
				double startFade = mc().level.getMinBuildHeight();
				if (this.fadeState != 0 && camY <= startFade)
				{
					double endFade = startFade - 44;

					double y = camY - startFade;
					double percent = y / (endFade - startFade);

					if (this.fadeState <= -1)
					{
						alpha = 1.0F * (1.0F - (float) percent);
					}
					else if (this.fadeState >= 1)
					{
						alpha = 1.0F * (float) percent;
					}
				}
				else if (this.fadeState == 1 && camY > startFade)
				{
					alpha = 0.0F;
				}
				alpha = Mth.clamp(alpha, 0.0F, 1.0F);

				if (alpha > 0)
				{
					this.cloudBuffer.bind();
					int l = this.prevCloudsType == CloudStatus.FANCY ? 0 : 1;

					RenderSystem.setShader(this::getShader);
					this.setShaderTextures();
					FogRenderer.levelFogColor();
					RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, alpha);
					for (int i1 = l; i1 < 2; ++i1)
					{
						if (i1 == 0)
						{
							RenderSystem.colorMask(false, false, false, false);
						}
						else
						{
							RenderSystem.colorMask(true, true, true, true);
						}

						ShaderInstance shader = RenderSystem.getShader();
						if (shader != null)
						{
							this.cloudBuffer.drawWithShader(poseStack.last().pose(), projectionMatrix, shader);
						}
					}

					VertexBuffer.unbind();
					RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
				}
				poseStack.popPose();
			}
			RenderSystem.enableCull();
			RenderSystem.disableBlend();
			RenderSystem.defaultBlendFunc();
		}
	}

	public BufferBuilder.RenderedBuffer buildClouds(BufferBuilder pBuilder, double pX, double pY, double pZ, Vec3 pCloudColor)
	{
		float f = 4.0F;
		float f1 = 0.00390625F;
		int i = 8;
		int j = 4;
		float f2 = 9.765625E-4F;
		float f3 = (float) Mth.floor(pX) * 0.00390625F;
		float f4 = (float) Mth.floor(pZ) * 0.00390625F;
		float f5 = (float) pCloudColor.x;
		float f6 = (float) pCloudColor.y;
		float f7 = (float) pCloudColor.z;
		float f8 = f5 * 0.9F;
		float f9 = f6 * 0.9F;
		float f10 = f7 * 0.9F;
		float bottomColorScale = 0.9F; // Was 0.7 but that's pretty dark
		float bottomR = f5 * bottomColorScale;
		float bottomG = f6 * bottomColorScale;
		float bottomB = f7 * bottomColorScale;
		float f14 = f5 * 0.8F;
		float f15 = f6 * 0.8F;
		float f16 = f7 * 0.8F;
		RenderSystem.setShader(this::getShader);
		pBuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX_COLOR_NORMAL);
		float f17 = (float) Math.floor(pY / 4.0D) * 4.0F;
		if (this.prevCloudsType == CloudStatus.FANCY)
		{
			for (int k = -7; k <= 8; ++k)
			{
				for (int l = -7; l <= 8; ++l)
				{
					float f18 = (float) (k * 8);
					float f19 = (float) (l * 8);
					if (f17 > -5.0F)
					{
						// Bottom of clouds
						pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 0.0F), (double) (f19 + 8.0F)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(bottomR, bottomG, bottomB, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
						pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 0.0F), (double) (f19 + 8.0F)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(bottomR, bottomG, bottomB, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
						pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 0.0F), (double) (f19 + 0.0F)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(bottomR, bottomG, bottomB, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
						pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 0.0F), (double) (f19 + 0.0F)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(bottomR, bottomG, bottomB, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
					}

					if (f17 <= 5.0F)
					{
						// Top of clouds
						pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 4.0F - f2), (double) (f19 + 8.0F)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, 1.0F, 0.0F).endVertex();
						pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 4.0F - f2), (double) (f19 + 8.0F)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, 1.0F, 0.0F).endVertex();
						pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 4.0F - f2), (double) (f19 + 0.0F)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, 1.0F, 0.0F).endVertex();
						pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 4.0F - f2), (double) (f19 + 0.0F)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, 1.0F, 0.0F).endVertex();
					}

					if (k > -1)
					{
						// West
						for (int i1 = 0; i1 < 8; ++i1)
						{
							pBuilder.vertex((double) (f18 + (float) i1 + 0.0F), (double) (f17 + 0.0F), (double) (f19 + 8.0F)).uv((f18 + (float) i1 + 0.5F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(-1.0F, 0.0F, 0.0F).endVertex();
							pBuilder.vertex((double) (f18 + (float) i1 + 0.0F), (double) (f17 + 4.0F), (double) (f19 + 8.0F)).uv((f18 + (float) i1 + 0.5F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(-1.0F, 0.0F, 0.0F).endVertex();
							pBuilder.vertex((double) (f18 + (float) i1 + 0.0F), (double) (f17 + 4.0F), (double) (f19 + 0.0F)).uv((f18 + (float) i1 + 0.5F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(-1.0F, 0.0F, 0.0F).endVertex();
							pBuilder.vertex((double) (f18 + (float) i1 + 0.0F), (double) (f17 + 0.0F), (double) (f19 + 0.0F)).uv((f18 + (float) i1 + 0.5F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(-1.0F, 0.0F, 0.0F).endVertex();
						}
					}

					if (k <= 1)
					{
						// East
						for (int j2 = 0; j2 < 8; ++j2)
						{
							pBuilder.vertex((double) (f18 + (float) j2 + 1.0F - f2), (double) (f17 + 0.0F), (double) (f19 + 8.0F)).uv((f18 + (float) j2 + 0.5F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(1.0F, 0.0F, 0.0F).endVertex();
							pBuilder.vertex((double) (f18 + (float) j2 + 1.0F - f2), (double) (f17 + 4.0F), (double) (f19 + 8.0F)).uv((f18 + (float) j2 + 0.5F) * 0.00390625F + f3, (f19 + 8.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(1.0F, 0.0F, 0.0F).endVertex();
							pBuilder.vertex((double) (f18 + (float) j2 + 1.0F - f2), (double) (f17 + 4.0F), (double) (f19 + 0.0F)).uv((f18 + (float) j2 + 0.5F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(1.0F, 0.0F, 0.0F).endVertex();
							pBuilder.vertex((double) (f18 + (float) j2 + 1.0F - f2), (double) (f17 + 0.0F), (double) (f19 + 0.0F)).uv((f18 + (float) j2 + 0.5F) * 0.00390625F + f3, (f19 + 0.0F) * 0.00390625F + f4).color(f8, f9, f10, 0.8F).normal(1.0F, 0.0F, 0.0F).endVertex();
						}
					}

					if (l > -1)
					{
						// North
						for (int k2 = 0; k2 < 8; ++k2)
						{
							pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 4.0F), (double) (f19 + (float) k2 + 0.0F)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + (float) k2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, -1.0F).endVertex();
							pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 4.0F), (double) (f19 + (float) k2 + 0.0F)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + (float) k2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, -1.0F).endVertex();
							pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 0.0F), (double) (f19 + (float) k2 + 0.0F)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + (float) k2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, -1.0F).endVertex();
							pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 0.0F), (double) (f19 + (float) k2 + 0.0F)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + (float) k2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, -1.0F).endVertex();
						}
					}

					if (l <= 1)
					{
						// South
						for (int l2 = 0; l2 < 8; ++l2)
						{
							pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 4.0F), (double) (f19 + (float) l2 + 1.0F - f2)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + (float) l2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, 1.0F).endVertex();
							pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 4.0F), (double) (f19 + (float) l2 + 1.0F - f2)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + (float) l2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, 1.0F).endVertex();
							pBuilder.vertex((double) (f18 + 8.0F), (double) (f17 + 0.0F), (double) (f19 + (float) l2 + 1.0F - f2)).uv((f18 + 8.0F) * 0.00390625F + f3, (f19 + (float) l2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, 1.0F).endVertex();
							pBuilder.vertex((double) (f18 + 0.0F), (double) (f17 + 0.0F), (double) (f19 + (float) l2 + 1.0F - f2)).uv((f18 + 0.0F) * 0.00390625F + f3, (f19 + (float) l2 + 0.5F) * 0.00390625F + f4).color(f14, f15, f16, 0.8F).normal(0.0F, 0.0F, 1.0F).endVertex();
						}
					}
				}
			}
		}
		else
		{
			int j1 = 1;
			int k1 = 32;

			for (int l1 = -32; l1 < 32; l1 += 32)
			{
				for (int i2 = -32; i2 < 32; i2 += 32)
				{
					pBuilder.vertex((double) (l1 + 0), (double) f17, (double) (i2 + 32)).uv((float) (l1 + 0) * 0.00390625F + f3, (float) (i2 + 32) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
					pBuilder.vertex((double) (l1 + 32), (double) f17, (double) (i2 + 32)).uv((float) (l1 + 32) * 0.00390625F + f3, (float) (i2 + 32) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
					pBuilder.vertex((double) (l1 + 32), (double) f17, (double) (i2 + 0)).uv((float) (l1 + 32) * 0.00390625F + f3, (float) (i2 + 0) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
					pBuilder.vertex((double) (l1 + 0), (double) f17, (double) (i2 + 0)).uv((float) (l1 + 0) * 0.00390625F + f3, (float) (i2 + 0) * 0.00390625F + f4).color(f5, f6, f7, 0.8F).normal(0.0F, -1.0F, 0.0F).endVertex();
				}
			}
		}

		return pBuilder.end();
	}

	private static final Minecraft mc()
	{
		return Minecraft.getInstance();
	}
}
