package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.PurpleArrowEntity;

import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PurpleArrowRenderer extends ArrowRenderer<PurpleArrowEntity>
{
	public static final ResourceLocation ARROW = RediscoveredMod.locate("textures/entity/purple_arrow.png");

	public PurpleArrowRenderer(EntityRendererProvider.Context manager)
	{
		super(manager);
	}

	@Override
	public ResourceLocation getTextureLocation(PurpleArrowEntity entity)
	{
		return ARROW;
	}
}