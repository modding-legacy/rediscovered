package com.legacy.rediscovered.client.render.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.joml.Vector3f;

import com.legacy.rediscovered.entity.dragon.AbstractRedDragonEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonBossEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class RedDragonModel<T extends AbstractRedDragonEntity> extends EntityModel<T>
{
	private final ModelPart head;
	private final ModelPart neck;
	private final ModelPart jaw;
	private final ModelPart body;
	private final ModelPart leftWing;
	private final ModelPart leftWingTip;
	private final ModelPart leftFrontLeg;
	private final ModelPart leftFrontLegTip;
	private final ModelPart leftFrontFoot;
	private final ModelPart leftRearLeg;
	private final ModelPart leftRearLegTip;
	private final ModelPart leftRearFoot;
	private final ModelPart rightWing;
	private final ModelPart rightWingTip;
	private final ModelPart rightFrontLeg;
	private final ModelPart rightFrontLegTip;
	private final ModelPart rightFrontFoot;
	private final ModelPart rightRearLeg;
	private final ModelPart rightRearLegTip;
	private final ModelPart rightRearFoot;

	@Nullable
	private T entity;
	private float partialTicks, limbSwing, limbSwingAmount;
	public float scaleSize = -1.0F;
	public boolean scaleWings = false;

	private final List<ModelPart> scales, wings;

	public RedDragonModel(ModelPart pRoot)
	{
		this.head = pRoot.getChild("head");
		this.jaw = this.head.getChild("jaw");
		this.neck = pRoot.getChild("neck");
		this.body = pRoot.getChild("body");
		this.leftWing = pRoot.getChild("left_wing");
		this.leftWingTip = this.leftWing.getChild("left_wing_tip");
		this.leftFrontLeg = pRoot.getChild("left_front_leg");
		this.leftFrontLegTip = this.leftFrontLeg.getChild("left_front_leg_tip");
		this.leftFrontFoot = this.leftFrontLegTip.getChild("left_front_foot");
		this.leftRearLeg = pRoot.getChild("left_hind_leg");
		this.leftRearLegTip = this.leftRearLeg.getChild("left_hind_leg_tip");
		this.leftRearFoot = this.leftRearLegTip.getChild("left_hind_foot");
		this.rightWing = pRoot.getChild("right_wing");
		this.rightWingTip = this.rightWing.getChild("right_wing_tip");
		this.rightFrontLeg = pRoot.getChild("right_front_leg");
		this.rightFrontLegTip = this.rightFrontLeg.getChild("right_front_leg_tip");
		this.rightFrontFoot = this.rightFrontLegTip.getChild("right_front_foot");
		this.rightRearLeg = pRoot.getChild("right_hind_leg");
		this.rightRearLegTip = this.rightRearLeg.getChild("right_hind_leg_tip");
		this.rightRearFoot = this.rightRearLegTip.getChild("right_hind_foot");

		this.scales = new ArrayList<>();

		this.scales.add(this.neck.getChild("scale1"));

		for (int i = 1; i <= 2; ++i)
			this.scales.add(this.head.getChild("scale" + i));

		for (int i = 1; i <= 3; ++i)
			this.scales.add(this.body.getChild("scale" + i));

		this.wings = List.of(this.rightWing, this.rightWingTip, this.leftWing, this.leftWingTip);
	}

	public static LayerDefinition createBodyLayer(CubeDeformation bodyScale, CubeDeformation scale)
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();
		PartDefinition head = root.addOrReplaceChild("head", CubeListBuilder.create().addBox("upperlip", -6.0F, -1.0F, -24.0F, 12, 5, 16, scale, 176, 44).addBox("upperhead", -8.0F, -8.0F, -10.0F, 16, 16, 16, scale, 112, 30).mirror(true).addBox("scale", -5.0F, -12.0F, -4.0F, 2, 4, 6, 0, 0).addBox("nostril", -5.0F, -3.0F, -22.0F, 2, 2, 4, scale, 112, 0).mirror(false).addBox("scale", 3.0F, -12.0F, -4.0F, 2, 4, 6, 0, 0).addBox("nostril", 3.0F, -3.0F, -22.0F, 2, 2, 4, scale, 112, 0), PartPose.ZERO);
		head.addOrReplaceChild("jaw", CubeListBuilder.create().addBox("jaw", -6.0F, 0.0F, -16.0F, 12, 4, 16, scale, 176, 65), PartPose.offset(0.0F, 4.0F, -8.0F));

		/*set the origins to negative (half, exact, half). Putting the y to half would center it. These are */
		// -5.0F, -12.0F, -4.0F
		head.addOrReplaceChild("scale1", CubeListBuilder.create().mirror().addBox("scale", -1.0F, -4.0F, -3.0F, 2, 4, 6, 0, 0), PartPose.offset(-4.0F, -8F, -1));

		// 3.0F, -12.0F, -4.0F
		head.addOrReplaceChild("scale2", CubeListBuilder.create().addBox("scale", -1.0F, -4.0F, -3.0F, 2, 4, 6, 0, 0), PartPose.offset(4F, -8F, -1));

		var neck = root.addOrReplaceChild("neck", CubeListBuilder.create().addBox("box", -5.0F, -5.0F, -5.0F, 10, 10, 10, scale, 192, 104).addBox("scale", -1.0F, -9.0F, -3.0F, 2, 4, 6, 48, 0), PartPose.ZERO);
		neck.addOrReplaceChild("scale1", CubeListBuilder.create().addBox("scale", -1.0F, -4F, -3.0F, 2, 4, 6, 48, 0), PartPose.offset(0, -5F, 0));

		var body = root.addOrReplaceChild("body", CubeListBuilder.create().addBox("body", -12.0F, 0.0F, -16.0F, 24, 24, 64, bodyScale, 0, 0).addBox("scale", -1.0F, -6.0F, -10.0F, 2, 6, 12, 220, 53).addBox("scale", -1.0F, -6.0F, 10.0F, 2, 6, 12, 220, 53).addBox("scale", -1.0F, -6.0F, 30.0F, 2, 6, 12, 220, 53), PartPose.offset(0.0F, 4.0F, 8.0F));
		body.addOrReplaceChild("scale1", CubeListBuilder.create().addBox("scale", -1.0F, -6.0F, -6.0F, 2, 6, 12, 220, 53), PartPose.offset(0, 0, -4));
		body.addOrReplaceChild("scale2", CubeListBuilder.create().addBox("scale", -1.0F, -6.0F, -6.0F, 2, 6, 12, 220, 53), PartPose.offset(0F, 0, 16));
		body.addOrReplaceChild("scale3", CubeListBuilder.create().addBox("scale", -1.0F, -6.0F, -6.0F, 2, 6, 12, 220, 53), PartPose.offset(0F, 0, 36));

		float wingScale = 0.01F;
		PartDefinition leftWing = root.addOrReplaceChild("left_wing", CubeListBuilder.create().mirror().addBox("bone", 0.0F, -4.0F, -4.0F, 56, 8, 8, scale, 112, 88).addBox("skin", 0.0F, 0.0F, 2.0F, 56, 0, 56, new CubeDeformation(wingScale), -56, 88), PartPose.offset(12.0F, 5.0F, 2.0F));
		leftWing.addOrReplaceChild("left_wing_tip", CubeListBuilder.create().mirror().addBox("bone", 0.0F, -2.0F, -2.0F, 56, 4, 4, scale, 112, 187 /* This was 136 moved for scaling issue */).addBox("skin", 0.0F, 0.0F, 2.0F, 56, 0, 56, new CubeDeformation(wingScale), -56, 144), PartPose.offset(56.0F, 0.0F, 0.0F));
		PartDefinition leftFrontLeg = root.addOrReplaceChild("left_front_leg", CubeListBuilder.create().addBox("main", -4.0F, -4.0F, -4.0F, 8, 24, 8, scale, 112, 104), PartPose.offset(12.0F, 20.0F, 2.0F));
		PartDefinition leftFrontLegTip = leftFrontLeg.addOrReplaceChild("left_front_leg_tip", CubeListBuilder.create().addBox("main", -3.0F, -1.0F, -3.0F, 6, 24, 6, scale, 226, 138), PartPose.offset(0.0F, 20.0F, -1.0F));
		leftFrontLegTip.addOrReplaceChild("left_front_foot", CubeListBuilder.create().addBox("main", -4.0F, 0.0F, -12.0F, 8, 4, 16, scale, 144, 104), PartPose.offset(0.0F, 23.0F, 0.0F));
		PartDefinition leftHindLeg = root.addOrReplaceChild("left_hind_leg", CubeListBuilder.create().addBox("main", -8.0F, -4.0F, -8.0F, 16, 32, 16, scale, 0, 0), PartPose.offset(16.0F, 16.0F, 42.0F));
		PartDefinition leftHindLegTip = leftHindLeg.addOrReplaceChild("left_hind_leg_tip", CubeListBuilder.create().addBox("main", -6.0F, -2.0F, 0.0F, 12, 32, 12, scale, 196, 0), PartPose.offset(0.0F, 32.0F, -4.0F));
		leftHindLegTip.addOrReplaceChild("left_hind_foot", CubeListBuilder.create().addBox("main", -9.0F, 0.0F, -20.0F, 18, 6, 24, 112, 0), PartPose.offset(0.0F, 31.0F, 4.0F));
		PartDefinition rightWing = root.addOrReplaceChild("right_wing", CubeListBuilder.create().addBox("bone", -56.0F, -4.0F, -4.0F, 56, 8, 8, scale, 112, 88).addBox("skin", -56.0F, 0.0F, 2.0F, 56, 0, 56, new CubeDeformation(wingScale), -56, 88), PartPose.offset(-12.0F, 5.0F, 2.0F));
		rightWing.addOrReplaceChild("right_wing_tip", CubeListBuilder.create().addBox("bone", -56.0F, -2.0F, -2.0F, 56, 4, 4, scale, 112, 187 /* This was 136 moved for scaling issue */).addBox("skin", -56.0F, 0.0F, 2.0F, 56, 0, 56, new CubeDeformation(wingScale), -56, 144), PartPose.offset(-56.0F, 0.0F, 0.0F));
		PartDefinition rightFrontLeg = root.addOrReplaceChild("right_front_leg", CubeListBuilder.create().mirror().addBox("main", -4.0F, -4.0F, -4.0F, 8, 24, 8, scale, 112, 104), PartPose.offset(-12.0F, 20.0F, 2.0F));
		PartDefinition rightFrontLegTip = rightFrontLeg.addOrReplaceChild("right_front_leg_tip", CubeListBuilder.create().mirror().addBox("main", -3.0F, -1.0F, -3.0F, 6, 24, 6, scale, 226, 138), PartPose.offset(0.0F, 20.0F, -1.0F));
		rightFrontLegTip.addOrReplaceChild("right_front_foot", CubeListBuilder.create().mirror().addBox("main", -4.0F, 0.0F, -12.0F, 8, 4, 16, scale, 144, 104), PartPose.offset(0.0F, 23.0F, 0.0F));
		PartDefinition rightHindLeg = root.addOrReplaceChild("right_hind_leg", CubeListBuilder.create().mirror().addBox("main", -8.0F, -4.0F, -8.0F, 16, 32, 16, scale, 0, 0), PartPose.offset(-16.0F, 16.0F, 42.0F));
		PartDefinition rightHindLegTip = rightHindLeg.addOrReplaceChild("right_hind_leg_tip", CubeListBuilder.create().mirror().addBox("main", -6.0F, -2.0F, 0.0F, 12, 32, 12, scale, 196, 0), PartPose.offset(0.0F, 32.0F, -4.0F));
		rightHindLegTip.addOrReplaceChild("right_hind_foot", CubeListBuilder.create().mirror().addBox("main", -9.0F, 0.0F, -20.0F, 18, 6, 24, scale, 112, 0), PartPose.offset(0.0F, 31.0F, 4.0F));
		return LayerDefinition.create(mesh, 256, 256);
	}

	@Override
	public void prepareMobModel(T pEntity, float pLimbSwing, float pLimbSwingAmount, float pPartialTick)
	{

		this.entity = pEntity;
		this.partialTicks = pPartialTick;
		this.limbSwing = pLimbSwing;
		this.limbSwingAmount = pLimbSwingAmount;

		/*if (pEntity instanceof RedDragonOffspringEntity tamed)
		{
			float rev = tamed.reverseAnim.getValue(pPartialTick);
			limbSwing += (pLimbSwingAmount - limbSwing) * (-2 * rev);
		}*/
	}

	@Override
	public void setupAnim(T pEntity, float pLimbSwing, float pLimbSwingAmount, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
	}

	@Override
	public void renderToBuffer(PoseStack pose, VertexConsumer pBuffer, int pPackedLight, int pPackedOverlay, float red, float green, float blue, float pAlpha)
	{
		float ageInTicks = (float) entity.tickCount + partialTicks;
		float fly = this.entity instanceof RedDragonOffspringEntity pet ? pet.flyingAnim.getValue(partialTicks) : 1;
		float flyInv = this.entity instanceof RedDragonOffspringEntity pet ? 1.0F - fly : 0;
		float fall = this.entity.isNoAi() ? 0.0F : (this.entity instanceof RedDragonOffspringEntity pet ? pet.fallingAnim.getValue(partialTicks) * flyInv : 1);
		float hover = this.entity instanceof RedDragonOffspringEntity pet ? pet.hoverAnim.getValue(partialTicks) * fly : 0;
		float tired = this.entity instanceof RedDragonOffspringEntity pet ? pet.tiredAnim.getValue(partialTicks) : 0;
		/*float tiredInv = this.entity instanceof RedDragonOffspringEntity pet ? 1.0F - tired : 0;*/
		float suffering = this.entity instanceof RedDragonOffspringEntity pet ? pet.sufferingAnim.getValue(partialTicks) : 0;

		float limbSwingAmount = this.limbSwingAmount;
		limbSwingAmount *= 1.0F - fall;

		float bodyRot = Mth.rotLerp(this.partialTicks, entity.yBodyRotO, entity.yBodyRot);
		float headRot = Mth.rotLerp(this.partialTicks, entity.yHeadRotO, entity.yHeadRot);

		float extraMul = this.entity.isVehicle() ? 0 : flyInv;
		float headYaw = Mth.wrapDegrees((headRot - bodyRot) * (1 + ((entity.isBaby() ? 0.2F : 0.5F) * extraMul)));
		float headPitch = Mth.wrapDegrees(Mth.lerp(this.partialTicks, entity.xRotO, entity.getXRot()) * (1 + (0.6F * extraMul)));

		bodyRot = Mth.wrapDegrees(bodyRot);
		// set test to 1 to always have leg animations.
		// if left alone, this transitions the animation between stopped and moving
		/*float test = Mth.clamp(1 - Mth.sin(ageInTicks * 0.02F) * 2, 0, 1);
		test = 1.0F;
		limbSwing = ageInTicks * 0.75F;
		limbSwing *= 1.0F;
		limbSwingAmount = 1.0F * test;*/

		for (ModelPart wing : this.wings)
		{
			wing.resetPose();
		}

		boolean affect = this.scaleSize >= 0;
		float amount = affect ? this.scaleSize : 1;

		for (ModelPart scale : this.scales)
		{
			scale.visible = affect;
			scale.resetPose();
			scale.xScale = amount;
			scale.yScale = amount;
			scale.zScale = Math.max(1, amount * 0.8F);
		}

		for (int i = 1; i <= 3; ++i)
			this.spikeGlowAnim(this.body.getChild("scale" + i), -i - 1, 1);

		for (int i = 1; i <= 2; ++i)
			this.spikeGlowAnim(this.head.getChild("scale" + i), i, 1);

		float windCharge = this.entity instanceof RedDragonBossEntity boss ? boss.windBlowAnim.getValue(partialTicks) : 0;
		float boltCharge = this.entity instanceof RedDragonBossEntity boss ? boss.boltChargeAnim.getValue(partialTicks) : 0;
		float boltShoot = this.entity instanceof RedDragonBossEntity boss ? boss.boltShootAnim.getValue(partialTicks) : 0;
		float cloudPrep = this.entity instanceof RedDragonBossEntity boss ? boss.cloudPrepAnim.getValue(partialTicks) : 0;

		float neckAngle = this.entity.isDeadOrDying() ? -3 : 0;
		float headMul = this.entity.isDeadOrDying() ? 3F : 1.5F;

		neckAngle += boltCharge * 3 / 4;
		neckAngle -= boltCharge * 2.5F * (1.0F - boltShoot);
		neckAngle += windCharge * (2.4F + (Mth.cos(ageInTicks * 0.2F) * 0.4F));

		neckAngle += cloudPrep * (-0.8F + (Mth.cos(ageInTicks * 2F) * 0.1F));

		neckAngle += 1.4F * hover;
		float headAngle = (neckAngle) + (Mth.sin(ageInTicks * 2.5F) * (0.1F * boltShoot));

		// headAngle += cloudPrep * 0.8F;

		if (this.entity instanceof RedDragonBossEntity boss)
		{
			float tilt = (Mth.lerp(this.partialTicks, boss.getOldXRotModifier(), boss.getXRotModifier())) * boltShoot;
			neckAngle += tilt;
			headAngle += tilt;
		}

		if (this.entity.isDeadOrDying())
		{
			neckAngle = -3F;
			headAngle = -0.6F;
		}
		else if (this.entity instanceof RedDragonOffspringEntity pet)
		{
			neckAngle += -2F * suffering;
			headAngle += -0.4F * suffering;
		}

		limbSwingAmount *= flyInv;

		// neckAngle += 0.2F * fly;
		// headAngle = 0;
		/*1 + (Mth.sin((entity.tickCount + partialTicks) * 0.1F) * 0.4F)*/

		pose.pushPose();
		float f = Mth.lerp(this.partialTicks, this.entity.prevAnimTime, this.entity.animTime);

		this.jaw.xRot = (float) (Math.sin((double) (f * (Mth.PI * 2F))) + 1.0D) * 0.2F;

		this.jaw.xRot *= 1.0F - boltCharge;
		this.jaw.xRot += boltCharge * 0.75F;

		this.jaw.xRot *= 1.0F - cloudPrep;
		this.jaw.xRot += 0.8F * cloudPrep;

		this.jaw.xRot *= fly;

		float groundedJawAnim = (0.3F - Mth.sin(ageInTicks * 0.1F) * 0.2F) * flyInv;
		this.jaw.xRot += groundedJawAnim;
		this.jaw.xRot += groundedJawAnim * (0.4F * tired);

		float f1 = (float) (Math.sin((double) (f * (Mth.PI * 2F) - 1.0F)) + 1.0D) + (4 * boltCharge);
		f1 = (f1 * f1 + f1 * 2.0F) * 0.05F;

		/*if (this.entity instanceof RedDragonBossEntity boss)
		{
			System.out.println(fly + " " + flyInv);
		}
		else
			System.out.println(fly + " " + flyInv);*/

		pose.translate(0.0F, ((f1 - 2.0F) * fly) + (-1.64F * flyInv), -3.0F);

		pose.translate(0.0F, (f1 * 5) * windCharge, (Mth.sin(ageInTicks * 0.25F) * 0.5F) * -windCharge);

		float hoverOffset = 2.3F;
		pose.translate(0, -0.4F * hover, 0.5F * hover);
		pose.mulPose(Axis.XP.rotationDegrees(((hoverOffset * 2 * 10) * -hover)));

		pose.mulPose(Axis.XP.rotationDegrees(f1 * 2.0F * fly));
		float f2 = 0.0F;
		float f3 = 20.0F;
		float f4 = -12.0F;
		double[] adouble = this.entity.getLatencyPos(6, this.partialTicks);
		float f6 = Mth.wrapDegrees((float) (this.entity.getLatencyPos(5, this.partialTicks)[0] - this.entity.getLatencyPos(10, this.partialTicks)[0]));
		float f7 = Mth.wrapDegrees((float) (this.entity.getLatencyPos(5, this.partialTicks)[0] + (double) (f6 / 2.0F)));
		float f8 = f * ((float) Math.PI * 2F);

		float spinMul = this.entity instanceof RedDragonOffspringEntity mount && mount.isVehicle() ? 0.3F * fly : 1 * fly;

		float recoilSpeed = 0.3F;
		int neckCount = 5;

		// headPitch *= 1.0F - fall;
		for (int i = 0; i < neckCount; ++i)
		{
			double[] adouble1 = this.entity.getLatencyPos(5 - i, this.partialTicks);
			float f9 = (float) Math.cos((double) ((float) i * 0.45F + f8)) * 0.15F;

			// f9 += -1 * fly;

			f9 += -Mth.sin(i - ageInTicks * 0.1F) * (0.2F * flyInv);
			this.neck.yRot = Mth.wrapDegrees((float) (adouble1[0] - adouble[0])) * ((float) Math.PI / 180F) * (1.5F * fly);
			this.neck.yRot += (headYaw * (i * 0.25F)) * Mth.DEG_TO_RAD;

			this.neck.xRot = (0.1F * tired) + f9 + (neckAngle * i) * Mth.DEG_TO_RAD * 1.5F * 5.0F * fly; // angle neck
			this.neck.xRot += (headPitch * (i * 0.25F)) * Mth.DEG_TO_RAD;
			// this.neck.xRot += ((45F * fall) * (i * 0.25F)) * Mth.DEG_TO_RAD;

			this.neck.zRot = (-Mth.wrapDegrees((float) (adouble1[0] - (double) f7)) * ((float) Math.PI / 180F) * 1.5F) * spinMul;

			this.neck.xRot += (-0.2F + Mth.sin(limbSwing * 0.3F) * (i * 0.05F)) * flyInv * limbSwingAmount;
			this.neck.yRot += (Mth.cos(limbSwing * recoilSpeed / 2) * (i * 0.025F)) * flyInv * limbSwingAmount;

			this.neck.y = f3;
			this.neck.z = f4;
			this.neck.x = f2;

			// Modify neck scale to fix z-fightning
			float s = i % 2 == 0 ? 1.0F : 0.99F;
			this.neck.xScale = s;
			this.neck.yScale = s;
			this.neck.zScale = s;

			this.spikeGlowAnim(this.neck.getChild("scale1"), i, 1);

			f3 += Mth.sin(this.neck.xRot) * 10.0F;
			f4 -= Mth.cos(this.neck.yRot) * Mth.cos(this.neck.xRot) * 10.0F;
			f2 -= Mth.sin(this.neck.yRot) * Mth.cos(this.neck.xRot) * 10.0F;
			this.neck.render(pose, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
		}

		this.partScale(this.head, this.entity.isBaby() ? 1.6F : 1.0F);

		this.head.y = f3;
		this.head.z = f4;
		this.head.x = f2;
		double[] adouble2 = this.entity.getLatencyPos(0, this.partialTicks);
		this.head.yRot = Mth.wrapDegrees((float) (adouble2[0] - adouble[0])) * Mth.DEG_TO_RAD * fly;
		this.head.yRot += (headYaw) * Mth.DEG_TO_RAD;

		this.head.xRot = Mth.wrapDegrees(headAngle * (neckCount + 1)) * Mth.DEG_TO_RAD * headMul * (5.0F * fly); /*angle head, was multiplied by 1.5 instead of 3*/
		this.head.xRot += (headPitch) * Mth.DEG_TO_RAD;
		this.head.xRot += (0.5F + Mth.cos(ageInTicks * 0.1F) * 0.2F) * tired;

		this.head.zRot = -Mth.wrapDegrees((float) (adouble2[0] - (double) f7)) * Mth.DEG_TO_RAD * fly;

		this.head.yRot -= (Mth.cos(limbSwing * recoilSpeed / 2) * 0.025F) * flyInv * limbSwingAmount;

		this.head.zRot += Mth.sin(this.head.yRot) * hover * fly;

		this.head.render(pose, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
		pose.pushPose();
		pose.translate(0.0F, 1.0F, 0.0F);

		float spin = -f6 * 1.5F;

		if (this.entity instanceof RedDragonOffspringEntity mount && mount.isVehicle())
			spin = Mth.clamp(spin * 0.3F, -30, 30);

		pose.mulPose(Axis.ZP.rotationDegrees(spin * fly));
		pose.translate(0.0F, -1.0F, 0.0F);
		this.body.zRot = 0.0F;
		this.body.render(pose, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
		float f10 = f * ((float) Math.PI * 2F);
		this.leftWing.xRot = 0.125F - (float) Math.cos((double) f10) * (0.2F + (0.4F * windCharge));
		this.leftWing.yRot = -0.25F + (0.25F * windCharge);
		this.leftWing.zRot = -((float) (Math.sin((double) f10) + 0.125D)) * (0.8F + (0.4F * windCharge));
		this.leftWingTip.zRot = (float) (Math.sin((double) (f10 + 2.0F)) + 0.5D) * 0.75F;
		this.rightWing.xRot = this.leftWing.xRot;
		this.rightWing.yRot = -this.leftWing.yRot;
		this.rightWing.zRot = -this.leftWing.zRot;
		this.rightWingTip.zRot = -this.leftWingTip.zRot;
		this.renderSide(false, limbSwingAmount, pose, pBuffer, pPackedLight, pPackedOverlay, f1, this.leftWing, this.leftWingTip, this.leftFrontLeg, this.leftFrontLegTip, this.leftFrontFoot, this.leftRearLeg, this.leftRearLegTip, this.leftRearFoot, red, green, blue, pAlpha);
		this.renderSide(true, limbSwingAmount, pose, pBuffer, pPackedLight, pPackedOverlay, f1, this.rightWing, this.rightWingTip, this.rightFrontLeg, this.rightFrontLegTip, this.rightFrontFoot, this.rightRearLeg, this.rightRearLegTip, this.rightRearFoot, red, green, blue, pAlpha);
		pose.popPose();
		float baseTailX = 0;// -Mth.sin(f * ((float) Math.PI * 2F)) * 0.0F;
		f8 = f * ((float) Math.PI * 2F);
		f3 = 10.0F;
		f4 = 60.0F;
		f2 = 0.0F;
		adouble = this.entity.getLatencyPos(11, this.partialTicks);

		for (int j = 0; j < 12; ++j)
		{
			adouble2 = this.entity.getLatencyPos(12 + j, this.partialTicks);
			baseTailX += Mth.sin((float) -j * 0.45F + f8) * -0.05F;

			/*f11 += -Mth.sin(j - ageInTicks * (0.1F * (j * 0.2F))) * (0.2F * flyInv);*/

			float sin = Mth.sin((j * 0.2F) - (ageInTicks * 0.1F));

			baseTailX += -sin * ((j * 0.03F) * flyInv);

			if (j == 0)
				baseTailX -= 0.3F * flyInv;

			baseTailX -= 0.1F * hover * fly;

			this.neck.yRot = ((Mth.wrapDegrees((float) (adouble2[0] - adouble[0])) * 1.5F + 180.0F) * Mth.DEG_TO_RAD * fly) + (180F * Mth.DEG_TO_RAD * flyInv);
			this.neck.yRot += Mth.cos((j * 0.2F) - (ageInTicks * 0.05F)) * flyInv * ((j + 1) * 0.1F);

			this.neck.xRot = baseTailX + ((float) (adouble2[1] - adouble[1]) * Mth.DEG_TO_RAD * (1.5F * spinMul) * (5.0F * fly) - ((0.2F * j) * boltCharge) - ((0.2F * j) * windCharge) - ((0.2F * j) * cloudPrep) * fly);
			this.neck.zRot = Mth.wrapDegrees((float) (adouble2[0] - (double) f7)) * Mth.DEG_TO_RAD * (1.5F * spinMul) * fly;
			this.neck.y = f3;
			this.neck.z = f4;
			this.neck.x = f2;

			this.spikeGlowAnim(this.neck.getChild("scale1"), -j, 1);
			/*this.neck.getChild("scale1").xScale = Math.max(this.neck.getChild("scale1").xScale, 1);*/

			f3 += Mth.sin(this.neck.xRot) * 10.0F;
			f4 -= Mth.cos(this.neck.yRot) * Mth.cos(this.neck.xRot) * 10.0F;
			f2 -= Mth.sin(this.neck.yRot) * Mth.cos(this.neck.xRot) * 10.0F;
			this.neck.render(pose, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
		}

		pose.popPose();
	}

	@SuppressWarnings("unused")
	private void renderSide(boolean right, float limbSwingAmount, PoseStack pPoseStack, VertexConsumer pBuffer, int pPackedLight, int pPackedOverlay, float p_173982_, ModelPart wing, ModelPart wingTip, ModelPart frontLeg, ModelPart frontLegTip, ModelPart frontFoot, ModelPart rearLeg, ModelPart rearLegTip, ModelPart rearFoot, float red, float green, float blue, float pAlpha)
	{
		float ageInTicks = this.entity.tickCount + this.partialTicks;
		// limbSwingAmount = 1;
		/*if (right)
			return;*/

		rearLeg.resetPose();
		rearLegTip.resetPose();
		rearFoot.resetPose();

		frontLeg.resetPose();
		frontLegTip.resetPose();
		frontFoot.resetPose();

		float fly = this.entity instanceof RedDragonOffspringEntity pet ? pet.flyingAnim.getValue(partialTicks) : 1;
		float flyInv = 1.0F - fly;
		float fall = this.entity.isNoAi() ? 0.0F : (this.entity instanceof RedDragonOffspringEntity pet ? pet.fallingAnim.getValue(partialTicks) : 1);
		float fallInv = 1.0F - fall;
		float hover = this.entity instanceof RedDragonOffspringEntity pet ? pet.hoverAnim.getValue(partialTicks) * fly : 0;
		// float hoverInv = 1.0F - hover;
		float tired = this.entity instanceof RedDragonOffspringEntity pet ? pet.tiredAnim.getValue(partialTicks) : 1;
		float tiredInv = this.entity instanceof RedDragonOffspringEntity pet ? 1.0F - tired : 0;

		float windCharge = this.entity instanceof RedDragonBossEntity boss ? boss.windBlowAnim.getValue(partialTicks) : 0;
		float boltCharge = this.entity instanceof RedDragonBossEntity boss ? boss.boltChargeAnim.getValue(partialTicks) : 0;
		float boltShoot = this.entity instanceof RedDragonBossEntity boss ? boss.boltShootAnim.getValue(partialTicks) : 0;
		float cloudPrep = this.entity instanceof RedDragonBossEntity boss ? boss.cloudPrepAnim.getValue(partialTicks) : 0;

		float f = Mth.lerp(this.partialTicks, this.entity.prevAnimTime, this.entity.animTime);
		float sin = -Mth.sin((f * (Mth.PI * 2F)));

		rearLeg.xRot = 1.0F + p_173982_ * 0.1F - (0.5F + sin * (0.2F * boltShoot)) * boltShoot;
		rearLegTip.xRot = 0.5F + p_173982_ * 0.1F;
		rearFoot.xRot = 0.75F + p_173982_ * 0.1F;

		float wingTicks = (f * (Mth.PI * 2F)) * 1;
		rearLeg.xRot += Mth.cos(wingTicks) * (cloudPrep + windCharge) * (0.3F - (0.15F * cloudPrep));
		rearLegTip.xRot += Mth.sin(wingTicks) * (cloudPrep + windCharge) * (0.3F - (0.15F * cloudPrep));

		// offspring hovering
		rearLeg.xRot += (Mth.cos(wingTicks) - 3F) * (hover * 0.2F);
		rearLegTip.xRot += (Mth.sin(wingTicks) + 2F) * (hover * 0.2F);

		rearLeg.xRot += 0.5F * cloudPrep;

		frontLeg.xRot = 1.3F + p_173982_ * 0.1F;
		frontLegTip.xRot = -0.5F - p_173982_ * 0.1F;
		frontFoot.xRot = 0.75F + p_173982_ * 0.1F;

		frontLeg.xRot -= ((3.2F - (0.8F * (boltShoot * boltShoot))) + (-Mth.sin((f * (Mth.PI * 2F)) * 3) * boltShoot * 0.05F)) * boltCharge * boltCharge;
		frontLegTip.xRot += 1.0F * boltCharge;
		frontFoot.xRot += -0.3F * boltCharge;

		frontLeg.xRot -= (-Mth.sin(wingTicks) * windCharge * 0.2F) + (0.7F * windCharge);
		frontFoot.xRot -= 0.4F * windCharge;
		frontLeg.zRot = -0.3F * windCharge;

		frontLeg.yRot = -0.4F * boltCharge;
		frontLeg.zRot += -0.2F * boltCharge;

		frontLeg.xRot -= 2.2F * cloudPrep;
		frontLeg.yRot += (-0.4F + Mth.sin((ageInTicks) * 2) * 0.1F) * cloudPrep;
		frontLeg.zRot -= (-0.2F + Mth.sin((ageInTicks) * 2) * 0.1F) * cloudPrep;

		frontLeg.xRot += (-0.9F + (Mth.cos(1 - wingTicks) * 0.1F)) * hover;
		frontLeg.zRot -= (0.5F + (Mth.sin(1 - wingTicks) * 0.1F)) * hover;
		frontLegTip.xRot -= (Mth.sin(1 - wingTicks) * 0.2F) * hover;

		if (right)
		{
			frontLeg.yRot = -frontLeg.yRot;
			frontLeg.zRot = -frontLeg.zRot;
		}

		rearLeg.xRot *= fly;
		rearLegTip.xRot *= fly;
		rearFoot.xRot *= fly;
		frontLeg.xRot *= fly;
		frontLegTip.xRot *= fly;
		frontFoot.xRot *= fly;

		wing.xRot *= fly;
		wing.yRot *= fly;
		wing.zRot *= fly;
		wingTip.xRot *= fly;
		wingTip.yRot *= fly;
		wingTip.zRot *= fly;

		rearLeg.z += 8 * flyInv;

		rearLeg.xRot -= 1.0F * flyInv;
		rearLegTip.xRot += 2F * flyInv;
		rearLegTip.z += 3 * flyInv;
		// rearFoot.xRot -= (rearLeg.xRot + rearLegTip.xRot) * inv;

		frontLeg.z -= 3 * flyInv * limbSwingAmount;

		float footHeight = 1.75F, diff = -3F;
		rearFoot.y -= (footHeight - diff) * flyInv;
		rearFoot.z += (footHeight + diff) * flyInv;

		float frontLegAngle = 1.0F * flyInv, frontTipAngle = 1.88F * flyInv;

		frontLeg.xRot += frontLegAngle - (0.0F * limbSwingAmount);
		frontLegTip.xRot -= frontTipAngle;
		/*frontFoot.xRot -= (frontLeg.xRot + frontLegTip.xRot) * inv;*/

		float groundedWingMul = flyInv * fallInv;
		float fallingWingMul = flyInv * fall;

		// falling leg animations
		frontLeg.xRot -= (0.5F - (Mth.sin((ageInTicks) * 0.3F) * 0.1F)) * fallingWingMul;
		frontLegTip.xRot += (0.3F - (Mth.cos((ageInTicks) * 0.3F) * 0.1F)) * fallingWingMul;
		frontFoot.xRot += -0.1F * fallingWingMul;

		rearLeg.xRot -= (0.2F - (Mth.sin((entity.tickCount + partialTicks) * 0.3F) * 0.1F)) * fallingWingMul;
		rearLegTip.xRot -= (0.2F - (Mth.cos((entity.tickCount + partialTicks) * 0.3F) * 0.1F)) * fallingWingMul;
		rearFoot.xRot -= 0.2F * fallingWingMul;

		if (right)
		{
			groundedWingMul = -groundedWingMul;
			fallingWingMul = -fallingWingMul;
		}

		frontLeg.x += 3F * fallingWingMul;
		frontLeg.yRot -= 0.3F * fallingWingMul;

		// rearLeg.x += 3F * fallingWingMul;
		rearLeg.yRot -= 0.5F * fallingWingMul;

		// grounded wing animations
		wing.yRot += -0.2F * groundedWingMul;
		wing.zRot += ((0.5F * tired) + -1.2F + Mth.cos((ageInTicks) * 0.1F) * 0.1F) * groundedWingMul;
		wingTip.zRot += ((-0.8F * tired) + 2.3F + Mth.sin((ageInTicks) * 0.1F) * 0.1F) * groundedWingMul;

		wing.zRot += (Mth.sin(limbSwing * 0.3F) * 0.1F) * groundedWingMul * limbSwingAmount;
		wingTip.zRot += (Mth.cos(limbSwing * 0.3F) * 0.1F) * groundedWingMul * limbSwingAmount;

		// falling wing animations
		wing.yRot += -0.1F * fallingWingMul;
		wing.zRot += (-0.7F + Mth.cos((ageInTicks) * 0.5F) * 0.1F) * fallingWingMul;
		wingTip.zRot += (0.5F + Mth.sin((ageInTicks) * 0.5F) * 0.2F) * fallingWingMul;

		float legRange = 0.6F, limbSpeed = 0.3F;

		// BACK LEGS
		{
			rearLeg.y -= 1.5F * limbSwingAmount;
			float thighSin = -0.2F + Mth.sin(limbSwing * limbSpeed + (!right ? Mth.PI : 0)) * legRange * limbSwingAmount;
			float frontThighRot = -thighSin;
			rearLeg.xRot += frontThighRot * limbSwingAmount;

			float tipSin = Mth.cos(limbSwing * limbSpeed + (right ? Mth.PI : 0)) * legRange * limbSwingAmount * 0.5F;
			float frontTipRot = -tipSin;
			rearLegTip.xRot += frontTipRot * limbSwingAmount;

			// Point downward (0 for the purposes of math)
			// frontLeg.xRot = 30F * Mth.DEG_TO_RAD;
			float thighUnit = 33;
			float thighTheta = rearLeg.xRot - Mth.HALF_PI;
			float kneeX = thighUnit * Mth.cos(thighTheta);
			float kneeY = thighUnit * Mth.sin(thighTheta);
			// Knee
			// System.out.printf("Knee: %.2f, %.2f\n", kneeX, kneeY);
			// Minecraft.getInstance().level.addParticle(ParticleTypes.NOTE, entity.getX() +
			// 1.1, this.entity.getY() + kneeY / 16.0F + 2.25, this.entity.getZ() + -kneeX /
			// 16.0F + 2.1, 0, 0, 0);

			// Point downward (0 for the purposes of math)
			// frontLegTip.xRot = -frontLeg.xRot;
			float shinUnit = 30;
			float shinTheta = rearLeg.xRot + rearLegTip.xRot - Mth.HALF_PI;
			float heelX = shinUnit * Mth.cos(shinTheta) + kneeX;
			float heelY = shinUnit * Mth.sin(shinTheta) + kneeY;

			float minHeelY = -35.5F;
			boolean footOnGround = heelY < minHeelY;
			float footPressure = heelY / minHeelY - 1.0F;
			if (footOnGround && fly == 0 && fall == 0) // Don't do this if flying
			{
				// System.out.println("-------");
				// System.out.println("Thigh " + (thighTheta * Mth.RAD_TO_DEG));
				// System.out.println("Shin " + (shinTheta * Mth.RAD_TO_DEG));
				float groundDist = minHeelY - kneeY;
				// System.out.println("Depth from knee " + groundDist);
				// System.out.println(shinTheta * Mth.RAD_TO_DEG);
				float newHeelX = (float) Math.sqrt(shinUnit * shinUnit - groundDist * groundDist);
				Vector3f v = new Vector3f(newHeelX, groundDist, 0.0F).rotateZ(-rearLeg.xRot);
				float newShinTheta = (float) Math.atan2(v.y(), v.x());
				// float newShinTheta = (float) Math.atan2(minKneeY, newHeelX);
				// System.out.println("Shin Old " + (shinTheta * Mth.RAD_TO_DEG));
				// System.out.println("Shin New " + ((newShinTheta + Mth.HALF_PI) *
				// Mth.RAD_TO_DEG));

				rearLegTip.xRot = newShinTheta + Mth.HALF_PI;

				// frontLegTip.xRot = 0;
				// Heel
				// System.out.printf("Heel: %.2f, %.2f\n", newHeelX, minHeelY);
				// Minecraft.getInstance().level.addParticle(ParticleTypes.NOTE, entity.getX() +
				// 1.1, this.entity.getY() + minHeelY / 16.0F + 2.25, this.entity.getZ() +
				// (-newHeelX - kneeX) / 16.0F + 2.3, 0.2, 0, 0);
			}
			/*else
			{
				// Heel
				//System.out.printf("Heel: %.2f, %.2f\n", heelX, heelY);
				//Minecraft.getInstance().level.addParticle(ParticleTypes.NOTE, entity.getX() + 1.1, this.entity.getY() + heelY / 16.0F + 2.25, this.entity.getZ() + -heelX / 16.0F + 2.3, 0, 0, 0);
			}*/

			float rot = ((Mth.HALF_PI * Mth.DEG_TO_RAD) - (rearLeg.xRot + rearLegTip.xRot)) * flyInv;
			float footRot = Mth.cos((-limbSwing * limbSpeed + (right ? Mth.PI : 0) + (Mth.PI - 1.5F)) * 2.0F) * legRange * limbSwingAmount * 0.2F + 0.3F * limbSwingAmount;
			float apexFootRotModifier = -Mth.cos((-limbSwing * limbSpeed + (right ? Mth.PI : 0) + (Mth.PI - 1.5F)) * 1.0F) * legRange * limbSwingAmount * 0.7F + 0.1F * limbSwingAmount;
			footRot -= apexFootRotModifier;
			if (footOnGround)
			{
				footRot = footRot * Mth.clamp(1.0F - footPressure * 5.0F, 0.0F, 1.0F);
			}

			rearFoot.xRot += Math.max(-1.5835F, rot + footRot) * flyInv;
		}

		// FRONT LEGS
		{
			// reset leg tip to 90 degrees
			/*frontLegTip.xRot *= 1.0F - limbSwingAmount;
			frontLegTip.xRot += ((-90F * Mth.DEG_TO_RAD) - frontLeg.xRot) * limbSwingAmount;*/

			// reset leg tip to 90 degrees
			/*frontLegTip.xRot *= 1.0F - limbSwingAmount;
			frontLegTip.xRot += ((-90F * Mth.DEG_TO_RAD) - frontLeg.xRot) * limbSwingAmount;*/

			float thighSin = -Mth.sin(-limbSwing * limbSpeed + (!right ? Mth.PI : 0)) * legRange * limbSwingAmount;
			float frontThighRot = /*-29.48F * Mth.DEG_TO_RAD + */thighSin;
			frontLeg.xRot += frontThighRot * limbSwingAmount;

			float tipSin = -Mth.cos(-limbSwing * limbSpeed + (right ? Mth.PI : 0)) * legRange * limbSwingAmount;
			float frontTipRot = /*(32.9F * Mth.DEG_TO_RAD) +*/ tipSin;
			frontLegTip.xRot += frontTipRot * limbSwingAmount;

			// Point downward (0 for the purposes of math)
			// frontLeg.xRot = 30F * Mth.DEG_TO_RAD;
			float thighUnit = 24;
			float thighTheta = frontLeg.xRot - Mth.HALF_PI;
			float kneeX = thighUnit * Mth.cos(thighTheta);
			float kneeY = thighUnit * Mth.sin(thighTheta);
			// Knee
			// System.out.printf("Knee: %.2f, %.2f\n", kneeX, kneeY);
			// Minecraft.getInstance().level.addParticle(ParticleTypes.NOTE, entity.getX() +
			// 1.1, this.entity.getY() + kneeY / 16.0F + 2.25, this.entity.getZ() + -kneeX /
			// 16.0F + 2.1, 0, 0, 0);

			// Point downward (0 for the purposes of math)
			// frontLegTip.xRot = -frontLeg.xRot;
			float shinUnit = 28;
			float shinTheta = frontLeg.xRot + frontLegTip.xRot - Mth.HALF_PI;
			float heelX = shinUnit * Mth.cos(shinTheta) + kneeX;
			float heelY = shinUnit * Mth.sin(shinTheta) + kneeY;

			float minHeelY = -31.5F;
			boolean footOnGround = heelY < minHeelY;
			float footPressure = heelY / minHeelY - 1.0F;
			if (footOnGround && fly == 0 && fall == 0) // Don't do this if flying
			{
				// System.out.println("-------");
				// System.out.println("Thigh " + (thighTheta * Mth.RAD_TO_DEG));
				// System.out.println("Shin " + (shinTheta * Mth.RAD_TO_DEG));
				float groundDist = minHeelY - kneeY;
				// System.out.println("Depth from knee " + groundDist);
				// System.out.println(shinTheta * Mth.RAD_TO_DEG);
				float newHeelX = (float) -Math.sqrt(shinUnit * shinUnit - groundDist * groundDist);
				Vector3f v = new Vector3f(newHeelX, groundDist, 0.0F).rotateZ(-frontLeg.xRot);
				float newShinTheta = (float) Math.atan2(v.y(), v.x());
				// float newShinTheta = (float) Math.atan2(minKneeY, newHeelX);
				// System.out.println("Shin Old " + (shinTheta * Mth.RAD_TO_DEG));
				// System.out.println("Shin New " + ((newShinTheta + Mth.HALF_PI) *
				// Mth.RAD_TO_DEG));

				frontLegTip.xRot = newShinTheta + Mth.HALF_PI;

				// frontLegTip.xRot = 0;
				// Heel
				// System.out.printf("Heel: %.2f, %.2f\n", newHeelX, minHeelY);
				// Minecraft.getInstance().level.addParticle(ParticleTypes.NOTE, entity.getX() +
				// 1.1, this.entity.getY() + minHeelY / 16.0F + 2.25, this.entity.getZ() +
				// (-newHeelX - kneeX) / 16.0F + 2.3, 0.2, 0, 0);
			}
			/*else
			{
				// Heel
				//System.out.printf("Heel: %.2f, %.2f\n", heelX, heelY);
				//Minecraft.getInstance().level.addParticle(ParticleTypes.NOTE, entity.getX() + 1.1, this.entity.getY() + heelY / 16.0F + 2.25, this.entity.getZ() + -heelX / 16.0F + 2.3, 0, 0, 0);
			}*/

			// subtracting them to keep the foot flat (we'll have to mess with this later)
			frontFoot.xRot -= (frontLeg.xRot + frontLegTip.xRot) * flyInv;
			float footRot = Mth.cos((-limbSwing * limbSpeed + (right ? Mth.PI : 0) + (Mth.PI - 0.4F)) * 1F) * legRange * limbSwingAmount * 0.3F;
			if (footOnGround)
				footRot = footRot * (1.0F - footPressure * 3.0F);
			frontFoot.xRot += footRot;
		}

		wing.render(pPoseStack, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
		frontLeg.render(pPoseStack, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
		rearLeg.render(pPoseStack, pBuffer, pPackedLight, pPackedOverlay, red, green, blue, pAlpha);
	}

	public void spikeGlowAnim(ModelPart part, int offset, float mod)
	{
		float speed = 0.7F;
		part.yScale += Mth.cos(((offset * Mth.HALF_PI) + ((float) entity.tickCount + partialTicks)) * speed) * 0.3F;
		part.xScale += Mth.sin(((offset * Mth.HALF_PI) + ((float) entity.tickCount + partialTicks)) * speed * 2) * 0.6F;

		part.yScale = Math.max(part.yScale, 1);
		part.xScale = Math.max(part.xScale, 1);
	}

	public void partScale(ModelPart part, float scale)
	{
		part.xScale = part.yScale = part.zScale = scale;
	}
}