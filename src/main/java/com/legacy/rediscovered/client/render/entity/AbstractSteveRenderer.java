package com.legacy.rediscovered.client.render.entity;

import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.SteveModel;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.world.entity.Mob;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public abstract class AbstractSteveRenderer<T extends Mob> extends MobRenderer<T, SteveModel<T>>
{
	public AbstractSteveRenderer(EntityRendererProvider.Context context)
	{
		super(context, new SteveModel<>(context.bakeLayer(RediscoveredRenderRefs.STEVE)), 0.6F);
		
		this.addLayer(new ItemInHandLayer<T, SteveModel<T>>(this, context.getItemInHandRenderer()));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float partialTicks)
	{
		float s = 0.8F;
		pose.scale(s, s, s);
		pose.translate(0, -0.05F, 0);
	}
}