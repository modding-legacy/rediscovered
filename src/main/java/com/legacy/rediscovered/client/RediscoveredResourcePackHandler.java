package com.legacy.rediscovered.client;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.PathPackResources;
import net.minecraft.server.packs.repository.BuiltInPackSource;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackSource;
import net.neoforged.fml.ModList;
import net.neoforged.neoforge.event.AddPackFindersEvent;
import net.neoforged.neoforgespi.locating.IModFile;

public class RediscoveredResourcePackHandler
{
	public static final Pack LEGACY_PACK = createPack(Component.literal("Rediscovered Programmer Art"), "legacy_pack").hidden();

	public static void packRegistry(AddPackFindersEvent event)
	{
		if (event.getPackType() == PackType.CLIENT_RESOURCES)
		{
		}
	}

	@SuppressWarnings("unused")
	private static void register(AddPackFindersEvent event, MutableComponent name, String folder)
	{
		event.addRepositorySource((consumer) -> consumer.accept(createPack(name, folder)));
	}

	public static Pack createPack(MutableComponent name, String folder)
	{
		IModFile file = ModList.get().getModFileById(RediscoveredMod.MODID).getFile();
		PathPackResources packResources = new PathPackResources(RediscoveredMod.find(folder), file.findResource("assets/" + RediscoveredMod.MODID + "/" + folder), true);
		return Pack.readMetaAndCreate(RediscoveredMod.find(folder), name, false, BuiltInPackSource.fixedResources(packResources), PackType.CLIENT_RESOURCES, Pack.Position.TOP, PackSource.BUILT_IN);
	}
}
