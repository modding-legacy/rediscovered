package com.legacy.rediscovered.client.gui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;
import com.legacy.rediscovered.entity.util.IPigman.Type;

import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.screens.inventory.CyclingSlotBackground;
import net.minecraft.client.gui.screens.inventory.InventoryScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.InventoryMenu;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class GuardPigmanInventoryScreen extends AbstractContainerScreen<GuardPigmanInventoryMenu>
{
	public static final String START_GUARD_KEY = key("start_guarding"), STOP_GUARD_KEY = key("stop_guarding");
	public static final String GUARDING_STATUS_KEY = key("guarding_status"),
			SEARCHING_STATUS_KEY = key("searching_status"), IDLE_STATUS_KEY = key("idle_status");

	private static final ResourceLocation GUARD_PIGMAN_TEXTURE = RediscoveredMod.locate("textures/gui/guard_pigman.png");

	private static final List<ResourceLocation> EMPTY_SLOTS_MELEE = List.of(new ResourceLocation("item/empty_slot_sword"), new ResourceLocation("item/empty_slot_axe")),
			EMPTY_SLOTS_MELEE_OFFHAND = List.of(InventoryMenu.EMPTY_ARMOR_SLOT_SHIELD);
	private static final List<ResourceLocation> EMPTY_SLOTS_RANGED = List.of(RediscoveredMod.locate("item/empty_slot_bow"), RediscoveredMod.locate("item/empty_slot_crossbow")),
			EMPTY_SLOTS_RANGED_OFFHAND = List.of(RediscoveredMod.locate("item/empty_slot_arrow"));

	private static final ResourceLocation GUARD = sprite("guard"), GUARD_HIGHLIGHTED = sprite("guard_highlighted"),
			GUARD_DISABLED = sprite("guard_disabled");
	private static final ResourceLocation STOP = sprite("stop"), STOP_HIGHLIGHTED = sprite("stop_highlighted"),
			STOP_DISABLED = sprite("stop_disabled");

	private final GuardPigmanEntity pigman;
	private Button startGuardingButton, stopGuardingButton;
	private float mousePosX;
	private float mousePosY;

	private final CyclingSlotBackground mainHandIcon = new CyclingSlotBackground(36),
			offHandIcon = new CyclingSlotBackground(37);

	public GuardPigmanInventoryScreen(GuardPigmanInventoryMenu container, Inventory playerInv, Component text)
	{
		super(container, playerInv, container.pigman.getDisplayName());
		this.pigman = container.pigman;
		this.imageHeight += GuardPigmanInventoryMenu.TOTAL_MARGIN;
		this.inventoryLabelY += GuardPigmanInventoryMenu.TOTAL_MARGIN;
	}

	@Override
	protected void containerTick()
	{
		super.containerTick();

		Type type = this.pigman.getPigmanType();
		this.mainHandIcon.tick(type == Type.MELEE ? EMPTY_SLOTS_MELEE : type == Type.RANGED ? EMPTY_SLOTS_RANGED : List.of());
		this.offHandIcon.tick(type == Type.MELEE ? EMPTY_SLOTS_MELEE_OFFHAND : type == Type.RANGED ? EMPTY_SLOTS_RANGED_OFFHAND : List.of());
	}

	@Override
	protected void init()
	{
		super.init();

		this.startGuardingButton = new GuardPigmanButton(Component.translatable(START_GUARD_KEY), this.leftPos + this.menu.rubySlot.x + 21, this.topPos + this.menu.rubySlot.y - 2, GUARD, GUARD_HIGHLIGHTED, GUARD_DISABLED, b ->
		{
			this.sendButtonClick((byte) 0);
			this.toggleActionButtons(true);
		});
		this.addRenderableWidget(this.startGuardingButton);

		this.stopGuardingButton = new GuardPigmanButton(Component.translatable(STOP_GUARD_KEY), startGuardingButton.getX(), startGuardingButton.getY(), STOP, STOP_HIGHLIGHTED, STOP_DISABLED, b ->
		{
			this.sendButtonClick((byte) 1);
			this.toggleActionButtons(false);
		});
		this.stopGuardingButton.active = this.pigman.getGuardedUUID().isPresent() && this.pigman.getGuardedUUID().get().equals(this.minecraft.player.getUUID());
		this.addRenderableWidget(this.stopGuardingButton);

		this.toggleActionButtons(false);
	}

	private void toggleActionButtons(boolean setGuarding)
	{
		this.stopGuardingButton.visible = setGuarding;
		this.startGuardingButton.visible = !setGuarding;

		this.stopGuardingButton.active = this.pigman.getGuardedUUID().isPresent() && this.pigman.getGuardedUUID().get().equals(this.minecraft.player.getUUID());
	}

	/**
	 * 0 = take payment and start guarding, 1 = stop guarding
	 */
	private void sendButtonClick(byte data)
	{
		this.minecraft.gameMode.handleInventoryButtonClick(this.menu.containerId, data);
	}

	@Override
	protected void renderBg(GuiGraphics graphics, float partialTicks, int x, int y)
	{
		int i = this.leftPos;
		int j = this.topPos;

		graphics.blit(GUARD_PIGMAN_TEXTURE, i, j, 0, 0, this.imageWidth, this.imageHeight);

		this.renderHealth(graphics, x, y, partialTicks);
		this.mainHandIcon.render(this.menu, graphics, partialTicks, this.leftPos, this.topPos);
		this.offHandIcon.render(this.menu, graphics, partialTicks, this.leftPos, this.topPos);

		InventoryScreen.renderEntityInInventoryFollowsMouse(graphics, i + 26, j + 18, i + 75, j + 88, 30, 0.0625F, (float) this.mousePosX, (float) (20) + this.mousePosY, this.pigman);

	}

	@Override
	public void render(GuiGraphics graphics, int mouseX, int mouseY, float partialTicks)
	{
		this.startGuardingButton.active = this.menu.rubySlot.hasItem();
		this.toggleActionButtons(this.pigman.getGuardedUUID().isPresent());

		this.mousePosX = (float) mouseX;
		this.mousePosY = (float) mouseY;
		super.render(graphics, mouseX, mouseY, partialTicks);

		int l = this.leftPos;
		int t = this.topPos;
		int statusLabelX = l + this.menu.rubySlot.x + 20;
		int statusLabelY = t + this.menu.rubySlot.y + 26;

		int color = this.pigman.getGuardedUUID().isPresent() ? ChatFormatting.GREEN.getColor() : this.pigman.isDismissedUnsafely() ? ChatFormatting.GOLD.getColor() : ChatFormatting.AQUA.getColor();
		Component statusText = Component.translatable(this.pigman.getGuardedUUID().isPresent() ? GUARDING_STATUS_KEY : this.pigman.isDismissedUnsafely() ? SEARCHING_STATUS_KEY : IDLE_STATUS_KEY);
		int statusOffset = 27 + 5;

		this.renderScrolling(graphics, statusText, statusLabelX - statusOffset, statusLabelY, (statusLabelX - statusOffset) + 54 + 10, statusLabelY + 10, color);

		if (this.pigman.getGuardedUUID().isPresent())
		{
			Component timeText = Component.literal(millisToDate(((this.pigman.timeToBodyguard() + 1) * 1000) - ((this.pigman.level().getGameTime() - this.pigman.getTimePaid()) * 50)));
			graphics.drawString(this.font, timeText, statusLabelX - this.font.width(timeText.getString()) / 2, statusLabelY + 12, 14737632, false);
		}

		this.renderTooltip(graphics, mouseX, mouseY);
	}

	private void renderScrolling(GuiGraphics pGuiGraphics, Component pText, int pMinX, int pMinY, int pMaxX, int pMaxY, int pColor)
	{
		int i = this.font.width(pText);
		int j = (pMinY + pMaxY - 9) / 2 + 1;
		int k = pMaxX - pMinX;
		if (i > k)
		{
			int l = i - k;
			double d0 = (double) Util.getMillis() / 400.0D;
			double d1 = Math.max((double) l * 0.5D, 2.0D);
			double d2 = Math.sin((Math.PI / 2D) * Math.cos((Math.PI * 2D) * d0 / d1)) / 2.0D + 0.5D;
			double d3 = Mth.lerp(d2, 0.0D, (double) l);
			pGuiGraphics.enableScissor(pMinX, pMinY, pMaxX, pMaxY);
			pGuiGraphics.drawString(this.font, pText, pMinX - (int) d3, j, pColor);
			pGuiGraphics.disableScissor();
		}
		else
		{
			pGuiGraphics.drawCenteredString(this.font, pText, (pMinX + pMaxX) / 2, j, pColor);
		}

	}

	private void renderHealth(GuiGraphics graphics, int mouseX, int mouseY, float partialTicks)
	{
		float health = this.pigman.getHealth() / this.pigman.getMaxHealth();
		int width = (int) (health * 81);
		int y = this.topPos + 23;
		int x = this.leftPos + 79 + 10;

		graphics.blit(GUARD_PIGMAN_TEXTURE, x - 10, y - 2, 0, 198, 9, 9);

		graphics.blit(GUARD_PIGMAN_TEXTURE, x, y, 0, 187, 81, 5);

		if (width > 0)
			graphics.blit(GUARD_PIGMAN_TEXTURE, x, y, 0, 192, width, 5);
	}

	public static String millisToDate(long milliseconds)
	{
		Date date = new Date(milliseconds);
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
		return sdf.format(date);
	}

	private static String key(String key)
	{
		return "gui." + RediscoveredMod.MODID + ".guard_pigman." + key;
	}

	private static class GuardPigmanButton extends Button
	{
		private final ResourceLocation sprite, highlightedSprite, disabledSprite;
		private boolean selected;
		private final Button.OnPress onPress;

		protected GuardPigmanButton(Component tooltip, int pX, int pY, ResourceLocation sprite, ResourceLocation highlighted, ResourceLocation disabled, Button.OnPress onPress)
		{
			super(Button.builder(tooltip, onPress).bounds(pX, pY, 20, 19).tooltip(Tooltip.create(tooltip)));
			this.sprite = sprite;
			this.highlightedSprite = highlighted;
			this.disabledSprite = disabled;
			this.onPress = onPress;
		}

		@Override
		public void renderWidget(GuiGraphics pGuiGraphics, int pMouseX, int pMouseY, float pPartialTick)
		{
			ResourceLocation resourcelocation = this.sprite;

			if (!this.active)
				resourcelocation = this.disabledSprite;
			else if (this.isHoveredOrFocused() || this.selected)
				resourcelocation = this.highlightedSprite;

			pGuiGraphics.blitSprite(resourcelocation, this.getX(), this.getY(), this.width, this.height);
		}

		@Override
		public void updateWidgetNarration(NarrationElementOutput pNarrationElementOutput)
		{
			this.defaultButtonNarrationText(pNarrationElementOutput);
		}

		@Override
		public void onPress()
		{
			this.onPress.onPress(this);
		}
	}

	private static ResourceLocation sprite(String sprite)
	{
		return RediscoveredMod.locate("container/guard_pigman/" + sprite);
	}
}