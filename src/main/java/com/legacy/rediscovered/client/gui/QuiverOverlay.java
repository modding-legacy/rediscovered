package com.legacy.rediscovered.client.gui;

import java.util.Optional;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.item.QuiverItem;
import com.legacy.rediscovered.item.util.QuiverData;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.datafixers.util.Pair;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.ChatScreen;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;

public class QuiverOverlay
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/gui/quiver_overlay.png");
	private static long openTime = 0;
	private static long closeTime = 0;
	
	private static boolean wasHoldingBow = false;
	private static long currentProjectileStart = 0;
	private static long currentProjectileEnd = 0;
	
	public static void render(ExtendedGui gui, GuiGraphics graphics, float partialTicks, int screenWidth, int screenHeight)
	{
		Minecraft mc = Minecraft.getInstance();
		if (mc.player != null && !mc.options.hideGui && (mc.screen == null || mc.screen instanceof ChatScreen))
		{
			Optional<QuiverData> quiverData = QuiverData.getFromChestplate(mc.player.getItemBySlot(EquipmentSlot.CHEST));
			if (quiverData.isPresent())
			{
				Pair<InteractionHand, ItemStack> heldBow = QuiverData.getHeldBow(mc.player);
				if (!QuiverData.isHoldingAmmo(mc.player, heldBow.getSecond()))
				{
					long currentTime = System.currentTimeMillis();
					boolean isHoldingBow = !heldBow.getSecond().isEmpty();
					if (isHoldingBow && !wasHoldingBow) // Detects when you start holding a bow
					{
						if (currentTime >= currentProjectileEnd)
							currentProjectileStart = currentTime;
						currentProjectileEnd = currentTime + 1500;
					}
					if (!isHoldingBow && wasHoldingBow) // Detects when you stop holding a bow
					{
						currentProjectileEnd = currentTime;
						closeTime = currentTime;
					}
					// The amount of time to render the tooltip for
					long projectileTime = currentTime - currentProjectileStart;
					int maxProjectileDuration = (int) (currentProjectileEnd - currentProjectileStart);
					// The > 100 helps prevent the arrow from displaying when you scroll past the bow
					if (projectileTime > 100 && projectileTime <= maxProjectileDuration)
					{
						ItemStack selectedArrow = quiverData.get().getSelectedAmmo(heldBow.getSecond());
						int x = screenWidth / 2 + 10;
						int y = screenHeight / 2 + 10;
						RenderSystem.enableBlend();
						float alpha = 1.0F;
						int fadeInDuration = 75;
						if (projectileTime <= fadeInDuration)
						{
							alpha = (float) projectileTime / fadeInDuration;
						}

						int fadeOutStart = maxProjectileDuration - 500;
						if (projectileTime >= fadeOutStart)
						{
							alpha = (1.0F - ((projectileTime - fadeOutStart) / ((float) maxProjectileDuration - fadeOutStart)));
						}
						
						RenderSystem.setShaderColor(1, 1, 1, alpha * 0.75F);
						graphics.renderItem(selectedArrow, x, y);
						graphics.renderItemDecorations(gui.getFont(), selectedArrow, x, y);
						RenderSystem.disableBlend();
						RenderSystem.setShaderColor(1, 1, 1, 1);
					}
					wasHoldingBow = isHoldingBow;

					long time = currentTime - openTime;
					int maxDuration = (int) (closeTime - openTime);
					if (time <= maxDuration)
					{
						int x = screenWidth / 2 - 46;
						int y = screenHeight / 2 + 40;
						float alpha = 1.0F;

						int fadeInDuration = 75;
						if (time <= fadeInDuration)
						{
							alpha = (float) time / fadeInDuration;
						}

						int fadeOutStart = maxDuration - 500;
						if (time >= fadeOutStart)
						{
							alpha = (1.0F - ((time - fadeOutStart) / ((float) maxDuration - fadeOutStart)));
						}

						ItemStack quiver = quiverData.get().getQuiver();
						int color = quiver.getItem() instanceof DyeableLeatherItem dyeable ? dyeable.getColor(quiver) : QuiverItem.DEFAULT_COLOR;
						ClientQuiverTooltip.render(gui.getFont(), x, y, graphics, TEXTURE, quiverData.get(), color, alpha);
					}
				}
			}
		}
	}

	public static void setAccessed()
	{
		long time = System.currentTimeMillis();
		if (time >= closeTime)
			openTime = time;
		closeTime = time + 1500;
		
		if (time >= currentProjectileEnd)
			currentProjectileStart = time;
		currentProjectileEnd = time + 1500;
	}
}
