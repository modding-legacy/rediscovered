package com.legacy.rediscovered.client.gui;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;

public class RedDragonOverlay
{
	private static final ResourceLocation DRAGON_STAMINA_BAR_TEXTURES = RediscoveredMod.locate("textures/gui/dragon_stamina_bars.png");
	private static float lastStamina = 1.0F;
	private static boolean isDecreasing = true;

	@SuppressWarnings("resource")
	public static void render(ExtendedGui gui, GuiGraphics graphics, float partialTicks, int screenWidth, int screenHeight)
	{
		if (!gui.getMinecraft().options.hideGui && gui.getMinecraft().player.getVehicle()instanceof RedDragonOffspringEntity dragon && dragon.getOwner() == gui.getMinecraft().player)
		{
			if (RediscoveredConfig.WORLD.redDragonStamina())
			{
				gui.setupOverlayRenderState(true, false);

				float stamina = dragon.lerpedStamina(partialTicks);
				int width = (int) (stamina * 182);
				int y = screenHeight - 32 + 3;
				int x = screenWidth / 2 - 91;

				int barOffset = 0;
				graphics.blit(DRAGON_STAMINA_BAR_TEXTURES, x, y, 0, barOffset, 182, 5);

				if (width > 0)
				{
					// Handled this way because it flickers when stamina == lastStamina
					if (stamina > lastStamina)
						isDecreasing = false;
					else if (stamina < lastStamina)
						isDecreasing = true;

					int bar;
					if (isDecreasing)
					{
						// While the bar is decreasing, set the bar color to red at 10%, orange at 20%,
						// and green otherwise
						bar = stamina < 0.1F ? 15 : (stamina < 0.2F ? 10 : 5);
					}
					else
					{
						// While the bar is increasing, set it to red if the dragon is tired. Green
						// otherwise.
						bar = dragon.isTired() ? 15 : 5;
					}
					graphics.blit(DRAGON_STAMINA_BAR_TEXTURES, x, y, 0, barOffset + bar, width, 5);
				}
				if (lastStamina != stamina)
					lastStamina = stamina;
			}
		}
		else
		{
			lastStamina = 1.0F;
		}
	}
}
