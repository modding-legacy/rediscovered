package com.legacy.rediscovered.client.gui;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.item.util.QuiverTooltip;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.tooltip.ClientTooltipComponent;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.core.NonNullList;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.FastColor;
import net.minecraft.world.item.ItemStack;

public class ClientQuiverTooltip implements ClientTooltipComponent
{
	private static final ResourceLocation TEXTURE = RediscoveredMod.locate("textures/gui/quiver_tooltip.png");
	private static final int DARK_OVERLAY_COLOR = FastColor.ARGB32.color(120, 0, 0, 0);
	private final QuiverData data;
	private final int color;

	public ClientQuiverTooltip(QuiverTooltip tooltip)
	{
		this.data = tooltip.data();
		this.color = tooltip.color();
	}

	@Override
	public void renderImage(Font font, int x, int y, GuiGraphics graphics)
	{
		render(font, x, y, graphics, TEXTURE, this.data, this.color, 1.0F);
	}

	public static void render(Font font, int x, int y, GuiGraphics graphics, ResourceLocation texture, QuiverData data, int selectedSlotColor, float alpha)
	{
		setupTransparency(alpha);
		graphics.blit(texture, x, y, 0, 0, 0, 92, 20, 128, 128);
		resetTransparency();

		int selectedSlot = data.getSelectedSlot();
		int fillX = x + 2 + 18 * selectedSlot, fillY = y + 2;
		setupTransparency(alpha);
		graphics.fill(fillX, fillY, fillX + 16, fillY + 16, FastColor.ARGB32.color(128, FastColor.ARGB32.red(selectedSlotColor), FastColor.ARGB32.green(selectedSlotColor), FastColor.ARGB32.blue(selectedSlotColor)));
		resetTransparency();

		NonNullList<ItemStack> allAmmo = data.getAmmo();
		for (int i = 0; i < QuiverData.QUIVER_SIZE; i++)
		{
			ItemStack ammo = allAmmo.get(i);
			boolean isAmmo = ammo.isEmpty() || QuiverData.isAmmo(ammo, QuiverData.getHeldBow(Minecraft.getInstance().player).getSecond());
			setupTransparency(alpha);
			int itemX = x + 2 + (18 * i);
			int itemY = y + 2;
			if (!isAmmo)
				graphics.fill(RenderType.guiOverlay(), itemX, itemY, itemX + 16, itemY + 16, DARK_OVERLAY_COLOR);
			graphics.renderItem(ammo, itemX, itemY);
			graphics.renderItemDecorations(font, ammo, itemX, itemY);
			if (!isAmmo)
				graphics.fill(RenderType.guiGhostRecipeOverlay(), itemX, itemY, itemX + 16, itemY + 16, DARK_OVERLAY_COLOR);
			resetTransparency();
		}
	}

	private static void setupTransparency(float alpha)
	{
		RenderSystem.enableBlend();
		RenderSystem.setShaderColor(1, 1, 1, alpha);
	}

	private static void resetTransparency()
	{
		RenderSystem.disableBlend();
		RenderSystem.setShaderColor(1, 1, 1, 1);
	}

	@Override
	public int getHeight()
	{
		return 26;
	}

	@Override
	public int getWidth(Font font)
	{
		return 92;
	}

}
