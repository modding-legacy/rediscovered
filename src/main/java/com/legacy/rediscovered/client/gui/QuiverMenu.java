package com.legacy.rediscovered.client.gui;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.registry.RediscoveredMenuType;

import net.minecraft.network.chat.Component;
import net.minecraft.world.Container;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class QuiverMenu extends AbstractContainerMenu
{
	private final QuiverContainer quiverContainer;

	public QuiverMenu(int id, Inventory playerInventory)
	{
		this(id, playerInventory, ItemStack.EMPTY, new QuiverData().setQuiver(ItemStack.EMPTY));
	}

	public QuiverMenu(int id, Inventory playerInventory, ItemStack ownerStack, QuiverData quiverData)
	{
		super(RediscoveredMenuType.QUIVER.get(), id);
		this.quiverContainer = new QuiverContainer(ownerStack, quiverData);
		checkContainerSize(this.quiverContainer, QuiverData.QUIVER_SIZE);
		this.quiverContainer.startOpen(playerInventory.player);

		// Arrow slots
		for (int j = 0; j < 5; ++j)
		{
			this.addSlot(new ArrowSlot(this.quiverContainer, j, 44 + j * 18, 20));
		}

		for (int l = 0; l < 3; ++l)
		{
			for (int k = 0; k < 9; ++k)
			{
				this.addSlot(new Slot(playerInventory, k + l * 9 + 9, 8 + k * 18, l * 18 + 51));
			}
		}

		for (int i1 = 0; i1 < 9; ++i1)
		{
			this.addSlot(new Slot(playerInventory, i1, 8 + i1 * 18, 109));
		}

	}

	@Override
	public boolean stillValid(Player player)
	{
		return this.quiverContainer.stillValid(player);
	}

	@Override
	public ItemStack quickMoveStack(Player player, int index)
	{
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.slots.get(index);
		if (slot != null && slot.hasItem())
		{
			ItemStack itemstack1 = slot.getItem();
			itemstack = itemstack1.copy();
			if (index < this.quiverContainer.getContainerSize())
			{
				if (!this.moveItemStackTo(itemstack1, this.quiverContainer.getContainerSize(), this.slots.size(), true))
				{
					return ItemStack.EMPTY;
				}
			}
			else if (!this.moveItemStackTo(itemstack1, 0, this.quiverContainer.getContainerSize(), false))
			{
				return ItemStack.EMPTY;
			}

			if (itemstack1.isEmpty())
			{
				slot.setByPlayer(ItemStack.EMPTY);
			}
			else
			{
				slot.setChanged();
			}
		}

		return itemstack;
	}

	@Override
	public void removed(Player player)
	{
		super.removed(player);
		this.quiverContainer.stopOpen(player);
	}

	private static class QuiverContainer extends SimpleContainer
	{
		private final ItemStack ownerStack;
		private final QuiverData data;

		public QuiverContainer(ItemStack ownerStack, QuiverData data)
		{
			super(data.getAmmo().toArray(ItemStack[]::new));
			this.ownerStack = ownerStack;
			this.data = data;
		}

		@Override
		public void stopOpen(Player player)
		{
			this.save();
		}

		public void save()
		{
			this.data.injectData(this.items);
		}

		@Override
		public boolean stillValid(Player player)
		{
			return !this.ownerStack.isEmpty() && player.getInventory().contains(this.ownerStack);
		}
	}

	private class ArrowSlot extends Slot
	{
		public ArrowSlot(Container container, int slotID, int x, int y)
		{
			super(container, slotID, x, y);
		}

		@Override
		public boolean mayPlace(ItemStack stack)
		{
			return stack.is(RediscoveredTags.Items.QUIVER_AMMO);
		}

		@Override
		public void setChanged()
		{
			super.setChanged();
			QuiverMenu.this.quiverContainer.save();
		}
	}

	public static class Provider implements MenuProvider
	{
		private final ItemStack stack;
		private final QuiverData quiverData;
		private final Component displayName;

		public Provider(ItemStack stack, QuiverData quiverData)
		{
			this.stack = stack;
			this.quiverData = quiverData;
			this.displayName = AttachedItem.get(stack).map(a -> a.getAttached().getHoverName()).orElseGet(() -> stack.getHoverName());
		}

		@Override
		public AbstractContainerMenu createMenu(int containerID, Inventory playerInventory, Player player)
		{
			return new QuiverMenu(containerID, playerInventory, this.stack, this.quiverData);
		}

		@Override
		public Component getDisplayName()
		{
			return this.displayName;
		}
	}
}