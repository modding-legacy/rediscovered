package com.legacy.rediscovered.client.gui;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.screens.inventory.InventoryScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class DragonInventoryScreen extends AbstractContainerScreen<DragonInventoryMenu>
{
	private static final ResourceLocation HORSE_GUI_TEXTURES = new ResourceLocation("textures/gui/container/horse.png");
	private static final ResourceLocation SADDLE_SLOT_SPRITE = RediscoveredMod.locate("container/red_dragon/saddle_slot");
	private static final ResourceLocation ARMOR_SLOT_SPRITE = RediscoveredMod.locate("container/red_dragon/armor_slot");

	private final RedDragonOffspringEntity mountEntity;

	private float mousePosX;
	private float mousePosY;

	public DragonInventoryScreen(DragonInventoryMenu container, Inventory playerInv, Component text)
	{
		super(container, playerInv, container.mount.getDisplayName());
		this.mountEntity = container.mount;
	}

	@Override
	protected void renderBg(GuiGraphics graphics, float partialTicks, int x, int y)
	{
		int i = this.leftPos;
		int j = this.topPos;
		graphics.blit(HORSE_GUI_TEXTURES, i, j, 0, 0, this.imageWidth, this.imageHeight);

		if (this.mountEntity.isTame() && !this.mountEntity.isBaby())
		{
			graphics.blitSprite(SADDLE_SLOT_SPRITE, i + 7, j + 35 - 18, 18, 18);
			graphics.blitSprite(ARMOR_SLOT_SPRITE, i + 7, j + 35, 18, 18);
		}

		InventoryScreen.renderEntityInInventoryFollowsMouse(graphics, i + 26, j + 18, i + 78, j + 70, 7, 0.25F, (float) this.mousePosX, (float) this.mousePosY, this.mountEntity);
	}

	@Override
	public void render(GuiGraphics graphics, int mouseX, int mouseY, float partialTicks)
	{
		this.mousePosX = (float) mouseX;
		this.mousePosY = (float) mouseY;
		super.render(graphics, mouseX, mouseY, partialTicks);
		this.renderTooltip(graphics, mouseX, mouseY);
	}
}