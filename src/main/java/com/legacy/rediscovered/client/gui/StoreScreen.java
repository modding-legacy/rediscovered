package com.legacy.rediscovered.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.mojang.blaze3d.platform.InputConstants;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.datafixers.util.Pair;
import com.mojang.math.Axis;

import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.components.WidgetSprites;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.network.chat.ClickEvent;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.HoverEvent;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.FastColor.ARGB32;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.entity.vehicle.Minecart;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.phys.Vec3;
import net.neoforged.fml.ModList;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.neoforge.client.gui.overlay.ExtendedGui;

public class StoreScreen extends Screen
{
	public static final float OPEN_CHANCE = 0.025F;
	// 427 and 240 are the screen width and height when at auto gui scale at the default game resolution (small window)
	static final int DEFAULT_WIDTH = 427, DEFAULT_HEIGHT = 240;
	private static final ResourceLocation GUI = RediscoveredMod.locate("textures/gui/store/minecraft_store.png");
	private static final ResourceLocation GUI_OVERLAY = RediscoveredMod.locate("textures/gui/store/minecraft_store_overlay.png");

	public static final ResourceLocation STEVE_VILLAGER = RediscoveredMod.locate("textures/entity/store/steve_villager.png");
	public static final ResourceLocation HORSE_SADDLE = RediscoveredMod.locate("textures/entity/store/horse_saddle.png");

	final int imageWidth, imageHeight;
	int leftPos = 0, topPos = 0, balance = 0, totalCost = 0, tick = 0;
	Entity cart;
	List<StoreButton> products = new ArrayList<>();
	CheckoutButton checkoutButton;

	public static boolean dyedPlanks = false, steveVillager = false, horseSaddle = false, showFireflies = false,
			showLock = false, rtxOn = false;

	public static void configRefresh(ModConfigEvent.Reloading event)
	{
		var config = event.getConfig();
		if (config.getModId().equals(RediscoveredMod.MODID) && config.getType().equals(ModConfig.Type.CLIENT))
		{
			Boolean enabled = config.getConfigData().get(RediscoveredConfig.CLIENT.allowAprilFools.getPath());
			if (enabled != null && !enabled)
			{
				dyedPlanks = false;
				steveVillager = false;
				horseSaddle = false;
				showFireflies = false;
				showLock = false;
				rtxOn = false;
			}
		}
	}

	public StoreScreen()
	{
		super(Component.literal("Minecraft Store"));
		this.imageWidth = 142;
		this.imageHeight = 218;
	}

	@Override
	public boolean isPauseScreen()
	{
		return false;
	}

	@Override
	public void onClose()
	{
		// Allows the chest lid to close
		this.minecraft.player.closeContainer();
		super.onClose();
	}

	@Override
	public void resize(Minecraft pMinecraft, int pWidth, int pHeight)
	{
		int oldBal = this.balance;
		super.resize(pMinecraft, pWidth, pHeight);
		this.balance = oldBal;
	}

	@Override
	public void tick()
	{
		this.checkoutButton.active = this.totalCost <= this.balance && this.products.stream().anyMatch(s -> s.isSelected);
		this.tick++;
		this.cart.tick();
		if (this.tick / 20 > 120 && this.minecraft.level.random.nextFloat() < 0.02F)
			((Minecart) cart).activateMinecart(0, 0, 0, true);
		super.tick();
	}

	@Override
	protected void init()
	{
		super.init();
		this.products.clear();
		this.leftPos = (this.width - this.imageWidth) / 2;
		this.topPos = (this.height - this.imageHeight) / 2;
		this.cart = new Minecart(EntityType.MINECART, this.minecraft.level);
		this.balance = 100 + this.minecraft.level.random.nextInt(301);

		int x = 0;
		int y = 0;
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "key", "Lock and Key DLC", 3, () ->
		{
			if (showLock && this.minecraft.level.random.nextFloat() < 0.25F)
			{
				this.sendChat("Looks like this key fits!");
				showLock = false;
			}
			else
				this.sendChat("Keys are now " + this.minecraft.level.random.nextInt(100) + "% longer!");
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "key_dlc", "Lock and Key DLC+", 12, () ->
		{
			if (showLock && this.minecraft.level.random.nextFloat() < 0.75F)
			{
				this.sendChat("One of these was bound to work!");
				showLock = false;
			}
			else
				this.sendChat("Keys fit in locks " + this.minecraft.level.random.nextInt(100) + "% easier!");
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "miners_helmet", "Miner's Helmet", 23, () ->
		{
			String url = "https://www.curseforge.com/minecraft/mc-mods/miners-helmet"; // miner's helmet download
			//@formatter:off
			this.sendChat(
					ModList.get().isLoaded("mining_helmet") ? 
						Component.literal("But... mining helmets already exist? Ok then.") : 
						Component.literal("You can download Miner's Helmet here! (some Minecraft version restrictions may apply)").withStyle(s -> s.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Component.literal(url))).withClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url))));
			//@formatter:on
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "dyed_planks", "Dyed Planks", 120, () ->
		{
			dyedPlanks = !dyedPlanks;
			this.sendChat(dyedPlanks ? "A limited selection of planks have been dyed!" : "The planks are scrubbed clean!");
		})));
		x = 0;
		y++;

		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "horse_saddle", "Horse Saddles", 161, () ->
		{
			horseSaddle = !horseSaddle;
			this.sendChat(horseSaddle ? "All saddle, no horse!" : "Horses are so back!");
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "steve_villager", "Steve Villagers", 389, () ->
		{
			steveVillager = !steveVillager;
			this.sendChat(steveVillager ? "\"I... am Steve\" - Some villager" : "Villagers have returned to their bald form!");
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "mod_suggestion", "Suggest Mod!", RediscoveredItems.ruby, RediscoveredBlocks.ruby_block.asItem(), 1337, () ->
		{
			this.sendChat("Nice try");
		}).inactive()));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "fireflies", "Firefly Jar", 32, () ->
		{
			showFireflies = !showFireflies;
			this.sendChat(showFireflies ? "Oh no! Some of the fireflies escaped!" : "You manage to catch the fireflies!");
		})));
		x = 0;
		y++;

		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "crab", "Vote: Crab", 41, () ->
		{
			String url = "https://www.youtube.com/watch?v=LDU_Txk06tM"; // crab rave
			this.sendChat(Component.literal("Your vote could not be processed at this time. But we found some cool crabs that want to hang out!").withStyle(s -> s.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, Component.literal(url))).withClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url))));
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "copper_golem", "Vote: Copper Golem", 35, () ->
		{
			this.sendChat("Is iron not good enough for you?");
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "mob_d", "Vote: Mob D", 39, () ->
		{
			this.sendChat("You're... really late to the party on this one. Maybe check Minecraft Dungeons?");
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "rtx_on", "RTX ON", 299, () ->
		{
			rtxOn = !rtxOn;
			this.sendChat(rtxOn ? "Now with enhanced graphics!" : "Back to boring old visuals");
		})));
		x = 0;
		y++;

		String soon = "Coming Soon!";
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "locked", soon, 26, () ->
		{

		}).inactive()));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "locked", soon, 89, () ->
		{

		}).inactive()));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "locked", "Regular Padlock", 12, () ->
		{
			this.sendChat(showLock ? "Despite your best efforts, another padlock will not fit on the crosshair." : "Your crosshair is securely locked in place!");
			showLock = true;
		})));
		this.products.add(this.addRenderableWidget(new StoreButton(x++, y, "locked", soon, 1499, () ->
		{

		}).inactive()));

		this.checkoutButton = this.addRenderableWidget(new CheckoutButton(this.leftPos + 87, this.topPos + 38, 54, 11, b -> this.checkout()));

	}

	void sendChat(MutableComponent message)
	{
		this.minecraft.player.displayClientMessage(Component.literal("[Minecraft Store] ").withStyle(s -> s.withColor(ChatFormatting.DARK_GREEN)).append(message.withStyle(s -> s.withColor(ChatFormatting.GREEN))), false);
	}

	void sendChat(String message)
	{
		sendChat(Component.literal(message));
	}

	void checkout()
	{
		this.minecraft.level.playSound(this.minecraft.player, this.minecraft.player.blockPosition(), SoundEvents.PLAYER_LEVELUP, SoundSource.PLAYERS);
		for (var p : this.products)
			p.checkout();
	}

	static ItemStack getCurrency()
	{
		return !rtxOn ? Items.EMERALD.getDefaultInstance() : Items.EMERALD_BLOCK.getDefaultInstance();
	}

	@Override
	public void render(GuiGraphics graphics, int mouseX, int mouseY, float partialTick)
	{
		PoseStack pose = graphics.pose();
		pose.pushPose();
		// Upscale GUI to fill the screen
		float scale = this.calculateScale();
		float translateScale = (scale - 1) * 0.5F;
		pose.translate(-this.width * translateScale, -this.height * translateScale, 0);
		pose.scale(scale, scale, scale);

		// Rescale where the mouse is for rendering widgets properly
		var scaleMouse = this.scaleMouse(mouseX, mouseY);
		mouseX = scaleMouse.getFirst();
		mouseY = scaleMouse.getSecond();

		this.renderBackground(graphics, mouseX, mouseY, partialTick);
		for (Renderable renderable : this.renderables)
			renderable.render(graphics, mouseX, mouseY, partialTick);

		if (cart != null)
			this.renderEntity(graphics, cart, partialTick, 30.0F, new Vec3(this.leftPos + 30, this.topPos + 45, 0), new Vec3(-20.5, -45.0, -22.5));

		pose.pushPose();
		pose.translate(this.width / 2, this.topPos + 4, 100);
		float textScale = 0.85F;
		pose.scale(textScale, textScale, textScale);
		graphics.drawCenteredString(this.font, Component.literal("Welcome to the Minecraft Store!"), 0, 0, ARGB32.color(255, 255, 255, 255));
		pose.popPose();

		pose.pushPose();
		pose.translate(this.leftPos + 68, this.topPos + 25, 100);
		textScale = 0.45F;
		pose.scale(textScale, textScale, textScale);
		graphics.drawCenteredString(this.font, Component.literal("Your Cart!"), 0, 0, ARGB32.color(255, 255, 255, 255));
		pose.popPose();

		Vec3 staticCurrencyRot = this.minecraft.getItemRenderer().getModel(getCurrency(), this.minecraft.level, null, 0).isGui3d() ? new Vec3(20, 20, -8) : new Vec3(0, 0, 0);

		this.renderCurrency(graphics, getCurrency(), 2.6F, new Vec3(this.leftPos + 74, this.topPos + 48, 0), staticCurrencyRot);
		pose.pushPose();
		pose.translate(this.leftPos + 71, this.topPos + 46, 100);
		textScale = 0.75F;
		pose.scale(textScale, textScale, textScale);
		graphics.drawCenteredString(this.font, Component.literal(String.valueOf(totalCost)), 0, 0, ARGB32.color(255, 255, 255, 255));
		pose.popPose();

		this.renderCurrency(graphics, getCurrency(), 1.6F, new Vec3(this.leftPos + 128, this.topPos + 26, 0), staticCurrencyRot);
		pose.pushPose();
		pose.translate(this.leftPos + 90, this.topPos + 25, 100);
		textScale = 0.40F;
		pose.scale(textScale, textScale, textScale);
		graphics.drawString(this.font, Component.literal("Your Balance!"), 10, 0, ARGB32.color(255, 255, 255, 255));
		graphics.drawCenteredString(this.font, Component.literal(String.valueOf(balance)), 96, 7, ARGB32.color(255, 255, 255, 255));
		pose.popPose();

		pose.popPose();
	}

	float calculateScale()
	{
		return Math.max(this.width / (float) DEFAULT_WIDTH, this.height / (float) DEFAULT_HEIGHT);
	}

	Pair<Double, Double> scaleMouse(double mouseX, double mouseY)
	{
		float scale = this.calculateScale();
		mouseX = ((mouseX - this.width / 2.0F) / scale + this.width / 2.0F);
		mouseY = ((mouseY - this.height / 2.0F) / scale + this.height / 2.0F);
		return Pair.of(mouseX, mouseY);
	}

	Pair<Integer, Integer> scaleMouse(int mouseX, int mouseY)
	{
		Pair<Double, Double> scaled = scaleMouse((double) mouseX, (double) mouseY);
		return scaled.mapFirst(Double::intValue).mapSecond(Double::intValue);
	}

	@Override
	public void renderBackground(GuiGraphics graphics, int mouseX, int mouseY, float partialTick)
	{
		super.renderBackground(graphics, mouseX, mouseY, partialTick);
		int i = (this.width - this.imageWidth) / 2;
		int j = (this.height - this.imageHeight) / 2;
		int imageWidth = 256;
		//System.out.println(this.minecraft.options.guiScale().get());
		graphics.blit(GUI, i, j, 0, 0, this.imageWidth, this.imageHeight, imageWidth, imageWidth);
		if (dyedPlanks)
		{
			int colorSpeed = 25;
			int tickDiv = tick / colorSpeed;
			int dyeLength = DyeColor.values().length;
			int color = tickDiv % dyeLength;
			int nextColor = (tickDiv + 1) % dyeLength;
			float f3 = ((float) (tick % colorSpeed) + partialTick) / (float) colorSpeed;
			float[] colorVals = Sheep.getColorArray(DyeColor.byId(color));
			float[] nextColorVals = Sheep.getColorArray(DyeColor.byId(nextColor));
			float r = colorVals[0] * (1.0F - f3) + nextColorVals[0] * f3;
			float g = colorVals[1] * (1.0F - f3) + nextColorVals[1] * f3;
			float b = colorVals[2] * (1.0F - f3) + nextColorVals[2] * f3;
			RenderSystem.setShaderColor(r, g, b, 1.0F);
			graphics.blit(GUI_OVERLAY, i, j, 0, 0, this.imageWidth, this.imageHeight, imageWidth, imageWidth);
			RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		}
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button)
	{
		var scaleMouse = this.scaleMouse(mouseX, mouseY);
		mouseX = scaleMouse.getFirst();
		mouseY = scaleMouse.getSecond();
		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button)
	{
		var scaleMouse = this.scaleMouse(mouseX, mouseY);
		mouseX = scaleMouse.getFirst();
		mouseY = scaleMouse.getSecond();
		return super.mouseReleased(mouseX, mouseY, button);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers)
	{
		InputConstants.Key mouseKey = InputConstants.getKey(keyCode, scanCode);
		if (super.keyPressed(keyCode, scanCode, modifiers))
		{
			return true;
		}
		else if (this.minecraft.options.keyInventory.isActiveAndMatches(mouseKey))
		{
			this.onClose();
			return true;
		}
		return true;
	}

	public void renderEntity(GuiGraphics graphics, Entity entity, float partialTick, float scale, Vec3 translate, Vec3 rotation)
	{
		PoseStack pose = graphics.pose();
		pose.pushPose();
		pose.translate(translate.x, translate.y, translate.z + 50);
		pose.mulPoseMatrix(new Matrix4f().scaling((float) scale, (float) scale, (float) (-scale)));
		pose.mulPose(Axis.YP.rotationDegrees((float) rotation.y));
		pose.mulPose(Axis.ZP.rotationDegrees((float) rotation.z + 180));
		pose.mulPose(Axis.XP.rotationDegrees((float) rotation.x));
		Lighting.setupForEntityInInventory();
		this.minecraft.getEntityRenderDispatcher().render(entity, 0, 0, 0, 0, partialTick, pose, graphics.bufferSource(), LightTexture.pack(15, 15));
		graphics.flush();
		Lighting.setupFor3DItems();
		pose.popPose();
	}

	public void renderCurrency(GuiGraphics graphics, ItemStack stack, float scale, Vec3 translate, Vec3 rotation)
	{
		PoseStack pose = graphics.pose();
		pose.pushPose();

		pose.translate(translate.x, translate.y, translate.z + 50);
		pose.mulPoseMatrix(new Matrix4f().scaling((float) scale, (float) scale, (float) (-scale)));
		pose.scale(16, 16, 16);
		BakedModel model = this.minecraft.getItemRenderer().getModel(stack, this.minecraft.level, null, 0);
		pose.mulPose(Axis.YP.rotationDegrees((float) rotation.y - 180));
		pose.mulPose(Axis.ZP.rotationDegrees((float) rotation.z - 180));
		pose.mulPose(Axis.XP.rotationDegrees((float) rotation.x));
		Lighting.setupForEntityInInventory();
		this.minecraft.getItemRenderer().render(stack, ItemDisplayContext.GROUND, false, pose, graphics.bufferSource(), LightTexture.FULL_BRIGHT, OverlayTexture.NO_OVERLAY, model);
		graphics.flush();
		Lighting.setupFor3DItems();
		pose.popPose();
	}

	class StoreButton extends Button
	{
		final ResourceLocation sprite;
		final WidgetSprites overlaySprites;
		final int price;
		final ItemStack currency, rtxCurrency;
		final int tickOffset;
		final String product;
		boolean isSelected = false;
		final Runnable onCheckout;

		public StoreButton(int x, int y, String sprite, String product, int price, Runnable onCheckout)
		{
			this(x, y, sprite, product, Items.EMERALD, Items.EMERALD_BLOCK, price, onCheckout);
		}

		public StoreButton(int x, int y, String sprite, String product, Item currency, Item rtxCurrency, int price, Runnable onCheckout)
		{
			super(StoreScreen.this.leftPos + 8 + (x > 1 ? 1 : 0) + x * 33, StoreScreen.this.topPos + 82 + y * 32, 26, 26, CommonComponents.EMPTY, b ->
			{
			}, DEFAULT_NARRATION);
			this.sprite = RediscoveredMod.locate("store/" + sprite);
			ResourceLocation base = RediscoveredMod.locate("store/store_button");
			ResourceLocation highlight = base.withSuffix("_highlighted");
			this.overlaySprites = new WidgetSprites(base, highlight);
			this.product = product;
			this.price = price;
			this.currency = currency.getDefaultInstance();
			this.rtxCurrency = rtxCurrency.getDefaultInstance();
			this.tickOffset = StoreScreen.this.minecraft.level.random.nextInt(360);
			this.onCheckout = onCheckout;
		}

		StoreButton inactive()
		{
			this.active = false;
			return this;
		}

		@Override
		public void renderWidget(GuiGraphics graphics, int mouseX, int mouseY, float partialTick)
		{
			PoseStack pose = graphics.pose();
			RenderSystem.enableBlend();
			graphics.blitSprite(sprite, this.getX(), this.getY(), this.width, this.height);
			ResourceLocation overlay = this.overlaySprites.get(this.isActive(), this.isHovered() || this.isSelected);
			graphics.blitSprite(overlay, this.getX(), this.getY(), this.width, this.height);
			RenderSystem.disableBlend();

			double tick = parent().tick + this.tickOffset + partialTick;
			parent().renderCurrency(graphics, rtxOn ? this.rtxCurrency : this.currency, 2.0F, new Vec3(this.getX() + 24, this.getY() + 24 + Math.sin(tick * 0.15), 0), new Vec3(Math.sin(tick * 0.1) * 2, tick * 3, Math.sin(tick * 0.05) * 3));

			pose.pushPose();
			pose.translate(this.getX() + 24.5, this.getY() + 18 + Math.sin(tick * 0.15), 100);
			float priceTextScale = 0.75F;
			pose.scale(priceTextScale, priceTextScale, priceTextScale);
			graphics.drawCenteredString(parent().font, Component.literal(String.valueOf(this.price)), 0, 0, ARGB32.color(255, 255, 255, 255));
			pose.popPose();

			pose.pushPose();
			var lines = parent().font.split(Component.literal(product).setStyle(Style.EMPTY.withBold(true)), 90);
			float productTextScale = lines.size() > 1 ? 0.28F : 0.37F;
			pose.translate(this.getX() + 13, this.getY() + (lines.size() > 1 ? 26.5 : 27.5), 100);
			pose.scale(productTextScale, productTextScale, productTextScale);
			for (int i = 0; i < lines.size(); i++)
				graphics.drawCenteredString(parent().font, lines.get(i), 0, i * 10, this.isSelected ? ARGB32.color(255, 0, 255, 0) : ARGB32.color(255, 255, 255, 255));
			pose.popPose();
		}

		@Override
		public void onPress()
		{
			this.isSelected = !this.isSelected;
			if (this.isSelected)
				parent().totalCost += this.price;
			else
				parent().totalCost -= this.price;
			super.onPress();
		}

		void checkout()
		{
			if (this.isSelected && this.isActive())
			{
				this.onCheckout.run();
				parent().totalCost -= this.price;
				parent().balance -= this.price;
				this.isSelected = false;
			}
		}

		StoreScreen parent()
		{
			return StoreScreen.this;
		}
	}

	class CheckoutButton extends Button
	{
		public CheckoutButton(int pX, int pY, int pWidth, int pHeight, Button.OnPress pOnPress)
		{
			super(pX, pY, pWidth, pHeight, Component.literal("Proceed to Checkout"), pOnPress, DEFAULT_NARRATION);
		}

		@Override
		protected void renderWidget(GuiGraphics graphics, int mouseX, int mouseY, float partialTick)
		{
			boolean canPurchase = parent().totalCost <= parent().balance;
			PoseStack pose = graphics.pose();
			pose.pushPose();
			pose.translate(this.getX() + 4, this.getY() + this.height / 2, 100);
			float textScale = 0.40F;
			pose.scale(textScale, textScale, textScale);
			var text = Component.literal("Proceed To Checkout! >");
			if (!canPurchase)
				text = text.withStyle(ChatFormatting.STRIKETHROUGH);
			else if (this.isHovered() && this.isActive())
				text = text.withStyle(ChatFormatting.UNDERLINE);
			graphics.drawString(parent().font, text, 0, 0, canPurchase ? ARGB32.color(255, 255, 255, 255) : ARGB32.color(255, 255, 0, 0));
			pose.popPose();
		}

		StoreScreen parent()
		{
			return StoreScreen.this;
		}
	}

	public static class StoreOverlay
	{
		static final ResourceLocation FIREFLY = RediscoveredMod.locate("store/firefly");
		static final ResourceLocation FIREFLY_DARK = RediscoveredMod.locate("store/firefly_dark");
		static final ResourceLocation PADLOCK = RediscoveredMod.locate("store/locked");
		static final ResourceLocation RTX_ON = RediscoveredMod.locate("store/rtx_on_overlay");

		static Firefly[] fireflies;
		static long lastTime = 0;

		public static void renderFirefly(ExtendedGui gui, GuiGraphics graphics, float partialTicks, int screenWidth, int screenHeight)
		{
			if (StoreScreen.showFireflies)
			{
				RandomSource rand = gui.getMinecraft().level.random;
				if (fireflies == null)
				{
					int count = 32;
					fireflies = new Firefly[count];
					for (int i = 0; i < count; i++)
						fireflies[i] = new Firefly(screenWidth, screenHeight, rand);
					lastTime = System.currentTimeMillis();
				}

				long time = System.currentTimeMillis();
				float deltaSecond = (time - lastTime) / 1000F;
				for (var f : fireflies)
					f.render(graphics, rand, time, deltaSecond, screenWidth, screenHeight);
				lastTime = time;
			}
			else if (fireflies != null)
			{
				fireflies = null;
			}
		}

		public static void renderLock(ExtendedGui gui, GuiGraphics graphics, float partialTicks, int screenWidth, int screenHeight)
		{
			if (StoreScreen.showLock)
			{
				int scale = 2;
				int padlockSize = 26 * scale;
				PoseStack pose = graphics.pose();
				pose.pushPose();
				pose.translate(screenWidth / 2 - padlockSize / 2, screenHeight / 2 - padlockSize / 2 - 10, 0);
				graphics.blitSprite(PADLOCK, 0, 0, padlockSize, padlockSize);
				pose.popPose();
			}
		}

		public static void renderRtx(ExtendedGui gui, GuiGraphics graphics, float partialTicks, int screenWidth, int screenHeight)
		{
			if (StoreScreen.rtxOn)
			{
				int scale = 1;
				int rtxWidth = 118 * scale;
				int rtxHeight = 40 * scale;
				PoseStack pose = graphics.pose();
				pose.pushPose();
				pose.translate(10, 10, 0);
				graphics.blitSprite(RTX_ON, 0, 0, rtxWidth, rtxHeight);
				pose.popPose();
			}
		}

		static final class Firefly
		{
			float x, y;
			float vx, vy;
			long lastCalc, nextCalc;
			int scale;
			int seed;

			Firefly(int screenWidth, int screenHeight, RandomSource rand)
			{
				this.scale = 5 + rand.nextInt(6);
				this.x = screenWidth / 2;
				this.y = screenHeight / 2;
				this.seed = rand.nextInt(360);
				this.generateNewVelocity(rand);
			}

			void generateNewVelocity(RandomSource rand)
			{
				float maxSpeed = 30.0F;
				this.vx = ((rand.nextFloat() - 0.5F) * 2.0F) * maxSpeed;
				this.vy = ((rand.nextFloat() - 0.5F) * 2.0F) * maxSpeed;
				this.lastCalc = System.currentTimeMillis();
				this.nextCalc = lastCalc + rand.nextInt(2 * 1000);
			}

			void render(GuiGraphics graphics, RandomSource rand, long time, float deltaSecond, int screenWidth, int screenHeight)
			{
				int width = 2 * scale;
				int height = 1 * scale;
				this.x = Mth.clamp(x + ((screenWidth * vx / (float) DEFAULT_WIDTH) * deltaSecond), 0, screenWidth - width);
				this.y = Mth.clamp(y + ((screenHeight * vy / (float) DEFAULT_HEIGHT) * deltaSecond), 0, screenHeight - height);
				PoseStack pose = graphics.pose();
				pose.pushPose();
				pose.translate(x, y, 0);
				graphics.blitSprite(FIREFLY_DARK, 0, 0, width, height);
				RenderSystem.enableBlend();
				// Make the firefly blink
				RenderSystem.setShaderColor(1, 1, 1, (float) Mth.clamp(Math.sin(time * 0.001 + seed) + 0.5, 0, 1));
				graphics.blitSprite(FIREFLY, 0, 0, width, height);
				RenderSystem.disableBlend();
				RenderSystem.setShaderColor(1, 1, 1, 1);
				pose.popPose();
				if (time >= nextCalc)
				{
					this.generateNewVelocity(rand);
				}
			}
		}
	}
}
