package com.legacy.rediscovered.client.gui;

import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.item.DragonArmorItem;
import com.legacy.rediscovered.registry.RediscoveredMenuType;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class DragonInventoryMenu extends AbstractContainerMenu
{
	private final Container dragonInventory, playerInventory;
	public final RedDragonOffspringEntity mount;

	public DragonInventoryMenu(int id, Inventory playerInventory, FriendlyByteBuf buffer)
	{
		this(id, playerInventory, (RedDragonOffspringEntity) playerInventory.player.level().getEntity(buffer.readInt()));
	}

	public DragonInventoryMenu(int id, Inventory playerInventory, final RedDragonOffspringEntity mount)
	{
		super(RediscoveredMenuType.DRAGON_INVENTORY.get(), id);
		this.mount = mount;
		this.dragonInventory = mount.getDragonInventory();
		this.playerInventory = playerInventory;

		this.playerInventory.startOpen(playerInventory.player);
		this.dragonInventory.startOpen(playerInventory.player);

		for (int i = 0; i < 9; ++i)
			this.addSlot(new Slot(playerInventory, i, 8 + i * 18, 142));

		for (int y = 0; y < 3; ++y)
			for (int x = 0; x < 9; ++x)
				this.addSlot(new Slot(playerInventory, x + (y + 1) * 9, 8 + x * 18, 84 + y * 18));

		this.addSlot(new Slot(this.dragonInventory, 0, 8, 18)
		{
			@Override
			public boolean mayPlace(ItemStack stack)
			{
				return !mount.isBaby() && stack.is(Items.SADDLE) && !this.hasItem() && playerInventory.player == mount.getOwner();
			}

			@Override
			public boolean mayPickup(Player pPlayer)
			{
				return pPlayer == mount.getOwner();
			}

			@OnlyIn(Dist.CLIENT)
			@Override
			public boolean isActive()
			{
				return mount.isTame() && mount.isSaddleable();
			}

			@Override
			public int getMaxStackSize()
			{
				return 1;
			}
		});

		this.addSlot(new Slot(this.dragonInventory, 1, 8, 36)
		{
			@Override
			public boolean mayPlace(ItemStack armor)
			{
				// allow switching between armors if they aren't the same item
				return !mount.isBaby() && armor.getItem() instanceof DragonArmorItem && armor != this.getItem() && playerInventory.player == mount.getOwner();
			}

			@Override
			public boolean mayPickup(Player pPlayer)
			{
				return pPlayer == mount.getOwner();
			}

			@Override
			public boolean isActive()
			{
				return mount.isTame() && mount.isSaddleable();
			}

			@Override
			public int getMaxStackSize()
			{
				return 1;
			}
		});
	}

	@Override
	public boolean stillValid(Player playerIn)
	{
		return this.dragonInventory.stillValid(playerIn) && this.mount.isAlive() && this.mount.distanceTo(playerIn) < 8.0F;
	}

	@Override
	public ItemStack quickMoveStack(Player player, int index)
	{
		ItemStack clickedStack = ItemStack.EMPTY;
		Slot slot = this.slots.get(index);
		if (slot != null && slot.hasItem())
		{
			ItemStack slotStack = slot.getItem();
			clickedStack = slotStack.copy();

			// Slot IDs
			//
			// hotbar : 0 - 8
			// inventory : 9 - 35
			// saddle : 36
			// armor : 37

			// inventory/hotbar -> saddle/armor
			if (this.moveItemStackTo(slotStack, 36, 38, false))
			{
				return ItemStack.EMPTY;
			}
			// hotbar -> inventory
			if (index >= 0 && index <= 8)
			{
				if (!this.moveItemStackTo(slotStack, 9, 36, false))
					return ItemStack.EMPTY;
			}
			// inventory -> hotbar
			else if (index >= 9 && index <= 35)
			{
				if (!this.moveItemStackTo(slotStack, 0, 9, false))
					return ItemStack.EMPTY;
			}
			// armor/mainhand -> inventory/hotbar
			else if (index == 36 || index == 37)
			{
				if (!this.moveItemStackTo(slotStack, 0, 36, true))
					return ItemStack.EMPTY;
			}

			if (slotStack.isEmpty())
				slot.setByPlayer(ItemStack.EMPTY);
			else
				slot.setChanged();

			if (slotStack.getCount() == clickedStack.getCount())
				return ItemStack.EMPTY;

			slot.onTake(player, slotStack);
		}

		return clickedStack;

	}

	@Override
	public void removed(Player playerIn)
	{
		super.removed(playerIn);

		this.dragonInventory.stopOpen(playerIn);
		this.playerInventory.stopOpen(playerIn);
	}
}