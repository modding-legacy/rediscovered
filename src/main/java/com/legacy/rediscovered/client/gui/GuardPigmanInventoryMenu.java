package com.legacy.rediscovered.client.gui;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;
import com.legacy.rediscovered.registry.RediscoveredMenuType;
import com.mojang.datafixers.util.Pair;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.InventoryMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.EnchantmentHelper;

public class GuardPigmanInventoryMenu extends AbstractContainerMenu
{
	public static final int TOP_MARGIN = 10, BOTTOM_MARGIN = 10, TOTAL_MARGIN = TOP_MARGIN + BOTTOM_MARGIN;
	public static final ResourceLocation BLOCK_ATLAS = new ResourceLocation("textures/atlas/blocks.png");
	public static final ResourceLocation EMPTY_ARMOR_SLOT_HELMET = new ResourceLocation("item/empty_armor_slot_helmet");
	public static final ResourceLocation EMPTY_ARMOR_SLOT_CHESTPLATE = new ResourceLocation("item/empty_armor_slot_chestplate");
	public static final ResourceLocation EMPTY_ARMOR_SLOT_LEGGINGS = new ResourceLocation("item/empty_armor_slot_leggings");
	public static final ResourceLocation EMPTY_ARMOR_SLOT_BOOTS = new ResourceLocation("item/empty_armor_slot_boots");
	public static final ResourceLocation EMPTY_ARMOR_SLOT_SHIELD = new ResourceLocation("item/empty_armor_slot_shield");
	public static final ResourceLocation EMPTY_SLOT_RUBY = RediscoveredMod.locate("item/empty_slot_ruby");
	static final ResourceLocation[] TEXTURE_EMPTY_SLOTS = new ResourceLocation[] { EMPTY_ARMOR_SLOT_BOOTS, EMPTY_ARMOR_SLOT_LEGGINGS, EMPTY_ARMOR_SLOT_CHESTPLATE, EMPTY_ARMOR_SLOT_HELMET };
	private static final EquipmentSlot[] SLOT_IDS = new EquipmentSlot[] { EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.CHEST, EquipmentSlot.HEAD };

	private final Inventory playerInventory;
	private final Container pigmanInventory, rubyInventory;
	public final Slot rubySlot;
	public final GuardPigmanEntity pigman;

	public GuardPigmanInventoryMenu(int id, Inventory playerInventory, FriendlyByteBuf buffer)
	{
		this(id, playerInventory, (GuardPigmanEntity) playerInventory.player.level().getEntity(buffer.readInt()));
	}

	public GuardPigmanInventoryMenu(int id, Inventory playerInventory, final GuardPigmanEntity pigman)
	{
		super(RediscoveredMenuType.GUARD_PIGMAN_INVENTORY.get(), id);
		this.pigman = pigman;
		this.pigmanInventory = pigman.getInventory();
		this.playerInventory = playerInventory;
		this.rubyInventory = new SimpleContainer(1);

		this.playerInventory.startOpen(playerInventory.player);
		this.pigmanInventory.startOpen(playerInventory.player);
		this.rubyInventory.startOpen(playerInventory.player);

		for (int i = 0; i < 9; ++i)
			this.addSlot(new Slot(playerInventory, i, 8 + i * 18, TOTAL_MARGIN + 142));

		for (int y = 0; y < 3; ++y)
			for (int x = 0; x < 9; ++x)
				this.addSlot(new Slot(playerInventory, x + (y + 1) * 9, 8 + x * 18, TOTAL_MARGIN + (84 + y * 18)));

		this.addSlot(new Slot(this.pigmanInventory, 0, 77, TOP_MARGIN + 62 - 18)
		{
			@Override
			public boolean mayPlace(ItemStack stack)
			{
				return pigman.isValidHandEquip(stack);
			}
		});

		this.addSlot(new Slot(this.pigmanInventory, 1, 77, TOP_MARGIN + 62)
		{
			@Override
			public boolean mayPlace(ItemStack stack)
			{
				return pigman.isValidOffHandEquip(stack);
			}
		});

		for (int k = 0; k < 4; ++k)
		{
			final EquipmentSlot equipmentslot = SLOT_IDS[k];
			this.addSlot(new Slot(this.pigmanInventory, 2 + k, 8, TOP_MARGIN + (8 + k * -18) + 18 * 3)
			{
				@Override
				public int getMaxStackSize()
				{
					return 1;
				}

				@Override
				public boolean mayPlace(ItemStack stack)
				{
					return stack.canEquip(equipmentslot, pigman);
				}

				@Override
				public boolean mayPickup(Player player)
				{
					ItemStack itemstack = this.getItem();
					return !itemstack.isEmpty() && !player.isCreative() && EnchantmentHelper.hasBindingCurse(itemstack) ? false : super.mayPickup(player);
				}

				@Override
				public Pair<ResourceLocation, ResourceLocation> getNoItemIcon()
				{
					return Pair.of(InventoryMenu.BLOCK_ATLAS, TEXTURE_EMPTY_SLOTS[equipmentslot.getIndex()]);
				}
			});
		}

		this.rubySlot = this.addSlot(new Slot(this.rubyInventory, 0, 116, TOP_MARGIN + 26)
		{
			@Override
			public boolean mayPlace(ItemStack stack)
			{
				return stack.is(RediscoveredTags.Items.GEMS_RUBY) && !pigman.isBaby();
			}

			@Override
			public int getMaxStackSize()
			{
				return 1;
			}

			@Override
			public Pair<ResourceLocation, ResourceLocation> getNoItemIcon()
			{
				return Pair.of(InventoryMenu.BLOCK_ATLAS, EMPTY_SLOT_RUBY);
			}
		});
	}

	@Override
	public boolean stillValid(Player playerIn)
	{
		return this.pigmanInventory.stillValid(playerIn) && this.pigman.isAlive() && this.pigman.distanceTo(playerIn) < 8.0F && this.pigman.isPlayerCured();
	}

	@Override
	public ItemStack quickMoveStack(Player player, int index)
	{
		ItemStack clickedStack = ItemStack.EMPTY;
		Slot slot = this.slots.get(index);
		if (slot != null && slot.hasItem())
		{
			ItemStack slotStack = slot.getItem();
			clickedStack = slotStack.copy();

			// Slot IDs
			//
			// hotbar : 0 - 8
			// inventory : 9 - 35
			// mainhand : 36
			// offhand : 37
			// feet/legs/chest/head : 38 -> 41
			// ruby : 42

			// inventory/hotbar -> slots
			if (!(index >= 36 && index <= 42) && this.moveItemStackTo(slotStack, 36, 43, false))
			{
				return ItemStack.EMPTY;
			}
			// hotbar -> inventory
			else if (index >= 0 && index <= 8)
			{
				if (!this.moveItemStackTo(slotStack, 9, 36, false))
					return ItemStack.EMPTY;
			}
			// inventory -> hotbar
			else if (index >= 9 && index <= 35)
			{
				if (!this.moveItemStackTo(slotStack, 0, 9, false))
					return ItemStack.EMPTY;
			}
			// armor/hands -> inventory/hotbar
			else if (index >= 36 && index <= 42)
			{
				if (!this.moveItemStackTo(slotStack, 0, 36, true))
					return ItemStack.EMPTY;
			}

			if (slotStack.isEmpty())
				slot.setByPlayer(ItemStack.EMPTY);
			else
				slot.setChanged();

			if (slotStack.getCount() == clickedStack.getCount())
				return ItemStack.EMPTY;

			slot.onTake(player, slotStack);
		}

		return clickedStack;
	}

	@Override
	public boolean clickMenuButton(Player player, int id)
	{
		switch (id)
		{
		// Pigman guarding payment has been recieved
		case 0:
			if (this.rubySlot.hasItem())
			{
				this.rubySlot.remove(1);
				this.pigman.hireAsBodyguard(player.getUUID());
				return true;
			}
			return false;
		// Pigman has been told to stop guarding
		case 1:
			if (this.pigman.getGuardedUUID().isPresent() && this.pigman.getGuardedUUID().get().equals(this.playerInventory.player.getUUID()))
			{
				this.pigman.dismissBodyguard();
				return true;
			}
			return false;
		default:
			return false;
		}
	}

	@Override
	public void removed(Player player)
	{
		super.removed(player);

		this.pigmanInventory.stopOpen(player);
		this.playerInventory.stopOpen(player);
		this.rubyInventory.stopOpen(player);

		if (!player.level().isClientSide())
			this.clearContainer(player, this.rubyInventory);
	}
}