package com.legacy.rediscovered.client;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.joml.Matrix4f;

import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.gui.ClientQuiverTooltip;
import com.legacy.rediscovered.client.gui.DragonInventoryScreen;
import com.legacy.rediscovered.client.gui.GuardPigmanInventoryScreen;
import com.legacy.rediscovered.client.gui.QuiverOverlay;
import com.legacy.rediscovered.client.gui.QuiverScreen;
import com.legacy.rediscovered.client.gui.RedDragonOverlay;
import com.legacy.rediscovered.client.gui.StoreScreen;
import com.legacy.rediscovered.client.item.RubyEyeClient;
import com.legacy.rediscovered.client.item.RubyFluteClient;
import com.legacy.rediscovered.client.render.world.SkylandsCloudRenderer;
import com.legacy.rediscovered.client.render.world.SkylandsSkyRenderer;
import com.legacy.rediscovered.data.RediscoveredLangProv;
import com.legacy.rediscovered.entity.dragon.AbstractRedDragonEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.item.util.QuiverTooltip;
import com.legacy.rediscovered.network.PacketHandler;
import com.legacy.rediscovered.network.c_to_s.CycleQuiverPacket;
import com.legacy.rediscovered.registry.RediscoveredArmorTrims;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredDimensions;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredMenuType;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.legacy.structure_gel.api.events.RegisterArmorTrimTexturesEvent;
import com.mojang.blaze3d.platform.InputConstants;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.datafixers.util.Either;
import com.mojang.datafixers.util.Pair;

import net.minecraft.ChatFormatting;
import net.minecraft.client.Camera;
import net.minecraft.client.CameraType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.inventory.ContainerScreen;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.CubeMap;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.inventory.ChestMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.LogicalSide;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.neoforge.client.event.InputEvent;
import net.neoforged.neoforge.client.event.ModelEvent;
import net.neoforged.neoforge.client.event.RegisterClientTooltipComponentFactoriesEvent;
import net.neoforged.neoforge.client.event.RegisterDimensionSpecialEffectsEvent;
import net.neoforged.neoforge.client.event.RegisterGuiOverlaysEvent;
import net.neoforged.neoforge.client.event.RegisterMenuScreensEvent;
import net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent;
import net.neoforged.neoforge.client.event.RenderGuiOverlayEvent;
import net.neoforged.neoforge.client.event.RenderHighlightEvent;
import net.neoforged.neoforge.client.event.RenderTooltipEvent;
import net.neoforged.neoforge.client.event.ScreenEvent;
import net.neoforged.neoforge.client.event.ViewportEvent;
import net.neoforged.neoforge.client.gui.overlay.VanillaGuiOverlay;
import net.neoforged.neoforge.event.TickEvent.LevelTickEvent;
import net.neoforged.neoforge.event.TickEvent.Phase;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;

public class RediscoveredClientEvents
{
	public static final CubeMap PANORAMA_RESOURCES = new CubeMap(RediscoveredMod.locate("textures/gui/panorama/panorama"));
	public static final ResourceLocation PANORAMA_OVERLAY = RediscoveredMod.locate("textures/gui/panorama_overlay.png");

	public static final ResourceLocation STEVE_MD3 = RediscoveredMod.locate("models/entity/steve.md3");
	public static final ResourceLocation RANA_MD3 = RediscoveredMod.locate("models/entity/mcexport01.md3");

	private static final DimensionSpecialEffects SKY_RENDER_INFO = new SkyRenderInfo();

	public static final Map<ResourceLocation, ResourceLocation> QUIVER_ITEM_MODEL_IDS = new HashMap<>();

	public static final Set<UUID> ACTIVE_DRAGON_FIGHTS = new HashSet<>();
	public static float dragonThunderTime, shakeStrength;

	public static double shakeX, shakeY, shakeZ;
	public static Vec3 shakeVec = Vec3.ZERO, shakeVecOld = Vec3.ZERO;

	public static boolean isAprilFools()
	{
		return isAprilFoolsDay() && RediscoveredConfig.CLIENT.allowAprilFools();
	}
	
	public static boolean isAprilFoolsDay()
	{
		return isDay(Calendar.APRIL, 1);
	}

	static boolean isDay(int month, int day)
	{
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.MONTH) == month && calendar.get(Calendar.DAY_OF_MONTH) == day;
	}

	public static class ModEvents
	{
		@SuppressWarnings("deprecation")
		@SubscribeEvent
		public static void registerModels(ModelEvent.RegisterAdditional event)
		{
			event.register(RediscoveredItems.quiver.builtInRegistryHolder().key().location().withPrefix("item/").withSuffix("_chestplate"));
		}

		@SubscribeEvent
		public static void registerTooltips(RegisterClientTooltipComponentFactoriesEvent event)
		{
			event.register(QuiverTooltip.class, ClientQuiverTooltip::new);
		}

		@SubscribeEvent
		public static void initDimensionRenderInfo(RegisterDimensionSpecialEffectsEvent event)
		{
			event.register(RediscoveredDimensions.SKYLANDS_ID, SKY_RENDER_INFO);
		}

		@SubscribeEvent
		protected static void registerGuiOverlays(final RegisterGuiOverlaysEvent event)
		{
			event.registerAbove(VanillaGuiOverlay.ITEM_NAME.id(), RediscoveredMod.locate("quiver_overlay"), QuiverOverlay::render);
			event.registerAbove(VanillaGuiOverlay.EXPERIENCE_BAR.id(), RediscoveredMod.locate("red_dragon_stamina_display"), RedDragonOverlay::render);

			if (isAprilFoolsDay())
			{
				event.registerBelow(VanillaGuiOverlay.FROSTBITE.id(), RediscoveredMod.locate("firefly_overlay"), StoreScreen.StoreOverlay::renderFirefly);
				event.registerAbove(VanillaGuiOverlay.CROSSHAIR.id(), RediscoveredMod.locate("lock_overlay"), StoreScreen.StoreOverlay::renderLock);
				event.registerAbove(VanillaGuiOverlay.ITEM_NAME.id(), RediscoveredMod.locate("rtx_on"), StoreScreen.StoreOverlay::renderRtx);
			}
		}

		@SubscribeEvent
		public static void clientInit(FMLClientSetupEvent event)
		{
			event.enqueueWork(() ->
			{
				ItemProperties.register(RediscoveredBlocks.ruby_eye.asItem(), RediscoveredMod.locate("eye_glow"), new RubyEyeClient.PropertyFunction());

				ItemProperties.register(RediscoveredItems.ruby_flute.asItem(), RediscoveredMod.locate("playing"), new RubyFluteClient.PropertyFunction());
			});
		}

		@SubscribeEvent
		public static void clientInit(RegisterMenuScreensEvent event)
		{
			event.register(RediscoveredMenuType.QUIVER.get(), QuiverScreen::new);
			event.register(RediscoveredMenuType.DRAGON_INVENTORY.get(), DragonInventoryScreen::new);
			event.register(RediscoveredMenuType.GUARD_PIGMAN_INVENTORY.get(), GuardPigmanInventoryScreen::new);
		}

		@SubscribeEvent
		public static void registerParticleFactories(RegisterParticleProvidersEvent event)
		{
			RediscoveredParticles.Factories.init(event);
		}

		@SubscribeEvent
		public static void registerArmorTrims(RegisterArmorTrimTexturesEvent event)
		{
			RediscoveredArmorTrims.Pattterns.patterns().forEach(pattern -> event.registerPatternSprite(pattern));
			RediscoveredArmorTrims.Materials.materials().forEach(material -> event.registerMaterialSprite(RediscoveredArmorTrims.Materials.assetID(material), material.location(), false));

			event.registerSprite(RediscoveredMod.locate("entity/red_dragon/armor/chain"));
			event.registerSprite(RediscoveredMod.locate("entity/red_dragon/armor/plating"));
			event.registerSprite(RediscoveredMod.locate("entity/red_dragon/armor/inlay"));
		}

		@SubscribeEvent
		public static void onConfigChange(ModConfigEvent.Reloading event)
		{
			StoreScreen.configRefresh(event);
		}
	}

	public static class ForgeEvents
	{
		/*@SubscribeEvent
		public void onOpenGui(GuiOpenEvent event)
		{
			if (event.getGui() instanceof TitleScreen && RediscoveredConfig.skyPanorama)
			{
				TitleScreen.CUBE_MAP = PANORAMA_RESOURCES;
				TitleScreen.PANORAMA_OVERLAY = PANORAMA_OVERLAY;
				event.setGui(new TitleScreen());
			}
		}*/

		@SubscribeEvent
		public static void gatherTooltipComponents(RenderTooltipEvent.GatherComponents event)
		{
			ItemStack stack = event.getItemStack();
			Optional<AttachedItem> attachedItem = AttachedItem.get(stack);
			if (attachedItem.isPresent())
			{
				ItemStack attached = attachedItem.get().getAttached();
				var tooltipImage = attached.getTooltipImage();
				if (tooltipImage.isPresent())
				{
					var elements = event.getTooltipElements();
					int targetIndex = 0;
					for (int i = 0; i < elements.size(); i++)
					{
						var either = elements.get(i);
						if (either.left().isPresent())
						{
							if (either.left().get() instanceof MutableComponent mutableComponent && mutableComponent.getContents() instanceof TranslatableContents translate && translate.getKey().contains("item.modifiers."))
							{
								targetIndex = i;
								break;
							}
						}
					}
					event.getTooltipElements().add(targetIndex, Either.right(tooltipImage.get()));
				}
			}
		}

		@SubscribeEvent
		public static void onTooltip(ItemTooltipEvent event)
		{
			Potion potion = PotionUtils.getPotion(event.getItemStack());
			if (potion != null)
			{
				List<Component> tooltips = event.getToolTip();
				int insertLine = -1;
				for (int i = tooltips.size() - 1; i > -1 && insertLine == -1; i--)
					if (matchesTranslationKey(tooltips.get(i), "potion.whenDrank"))
						insertLine = i + 1;
				if (insertLine != -1)
				{
					for (MobEffectInstance instance : potion.getEffects())
					{
						if (instance.getEffect() == RediscoveredEffects.GOLDEN_AURA.get())
						{
							tooltips.add(insertLine, Component.translatable(RediscoveredLangProv.GOLDEN_AURA_EFFECT).withStyle(ChatFormatting.BLUE));
						}
						if (instance.getEffect() == RediscoveredEffects.CRIMSON_VEIL.get())
						{
							tooltips.add(insertLine, Component.translatable(RediscoveredLangProv.CRIMSON_VEIL_EFFECT).withStyle(ChatFormatting.BLUE));
						}
					}
				}
			}
		}

		private static boolean matchesTranslationKey(Component component, String key)
		{
			return component.getContents() instanceof TranslatableContents trans && trans.getKey().equals(key);
		}

		@SubscribeEvent
		public static void mouseClick(InputEvent.MouseButton.Post event)
		{
			net.minecraft.client.Minecraft mc = mc();
			if (mc.player != null)
			{
				int action = event.getAction();
				if (action == InputConstants.PRESS || action == InputConstants.REPEAT)
				{
					if (mc.screen == null && mc.options.keyAttack.getKey().getValue() == event.getButton())
					{
						Pair<InteractionHand, ItemStack> heldBow = QuiverData.getHeldBow(mc.player);
						if (!heldBow.getSecond().isEmpty() && !QuiverData.isHoldingAmmo(mc.player, heldBow.getSecond()) && QuiverData.getFromChestplate(mc.player.getItemBySlot(EquipmentSlot.CHEST)).isPresent() && (heldBow.getFirst() == InteractionHand.MAIN_HAND || mc.player.isUsingItem()))
						{
							QuiverOverlay.setAccessed();
							PacketHandler.sendToServer(new CycleQuiverPacket());
						}
					}
				}
			}
		}

		@SuppressWarnings("resource")
		@SubscribeEvent
		public static void onDrawHighlight(RenderHighlightEvent.Block event)
		{
			// Handled in the block entity renderer
			BlockState state = mc().level.getBlockState(event.getTarget().getBlockPos());
			if (state.is(RediscoveredBlocks.mini_dragon_pylon) || state.is(RediscoveredBlocks.red_dragon_egg))
			{
				event.setCanceled(true);
			}
		}

		@SuppressWarnings("resource")
		@SubscribeEvent
		public static void onAngleCompute(ViewportEvent.ComputeCameraAngles event)
		{
			CameraType camType = mc().options.getCameraType();
			Camera camera = event.getCamera();
			double pt = event.getPartialTick();
			if (shakeVec != null)
			{
				// float scale = Mth.lerp(pt, shakeStrengthOld, dragonThunderTime)
				camera.move(camera.getMaxZoom(Mth.lerp(pt, shakeVecOld.x(), shakeVec.x())), Mth.lerp(pt, shakeVecOld.y(), shakeVec.y()), Mth.lerp(pt, shakeVecOld.z(), shakeVec.z()));
			}

			if (mc().player.getVehicle() instanceof AbstractRedDragonEntity dragon && camType != CameraType.FIRST_PERSON)
				camera.move(-camera.getMaxZoom(5.0D), 0, 0);
		}

		@SubscribeEvent
		public static void onClientTick(LevelTickEvent event)
		{
			if (event.phase == Phase.END && event.side == LogicalSide.CLIENT)
			{
				ACTIVE_DRAGON_FIGHTS.removeIf(uuid -> !mc().gui.getBossOverlay().events.keySet().contains(uuid));

				if (ACTIVE_DRAGON_FIGHTS.isEmpty())
					dragonThunderTime -= 0.02F;
				else
					dragonThunderTime += 0.02F;

				dragonThunderTime = Mth.clamp(dragonThunderTime, 0, 1);

				shakeStrength = Mth.clamp(shakeStrength, 0, 1);

				RandomSource rand = event.level.getRandom();
				shakeVecOld = shakeVec;
				shakeVec = new Vec3(rand.nextGaussian(), rand.nextGaussian(), rand.nextGaussian()).scale(shakeStrength).scale((float) RediscoveredConfig.CLIENT.screenShakeIntensity() * 0.01F);

				// shakeStrengthOld = shakeStrength;
				shakeStrength *= 0.7F;
			}
		}

		@SuppressWarnings("resource")
		@SubscribeEvent
		public static void joinlevel(EntityJoinLevelEvent event)
		{
			// Fixes seeing rain when you join a world where you shouldn't.
			if (event.getEntity() == mc().player)
			{
				ACTIVE_DRAGON_FIGHTS.clear();
				dragonThunderTime = 0;
				shakeStrength = 0;
			}
		}

		@SubscribeEvent
		public static void renderOverlayPre(RenderGuiOverlayEvent.Pre event)
		{
			ResourceLocation id = event.getOverlay().id();

			if (RediscoveredConfig.WORLD.redDragonStamina() && id.equals(VanillaGuiOverlay.EXPERIENCE_BAR.id()) && mc().player.getVehicle() instanceof RedDragonOffspringEntity dragon)
			{
				if (dragon != null && dragon.getOwner() == mc().player)
					event.setCanceled(true);
			}
		}

		@SubscribeEvent
		public static void openScreen(ScreenEvent.Opening event)
		{
			if (isAprilFools() && event.getNewScreen() instanceof ContainerScreen containerScreen && containerScreen.getMenu().getType().equals(MenuType.GENERIC_9x3) && Minecraft.getInstance().level.random.nextFloat() < StoreScreen.OPEN_CHANCE)
			{
				event.setNewScreen(new StoreScreen());
			}
		}

		private static final Minecraft mc()
		{
			return Minecraft.getInstance();
		}
	}

	public static class SkyRenderInfo extends DimensionSpecialEffects
	{
		public SkyRenderInfo()
		{
			super(0.0F, true, DimensionSpecialEffects.SkyType.NORMAL, false, false);
		}

		@Override
		public Vec3 getBrightnessDependentFogColor(Vec3 fogColor, float fogBrightness)
		{
			return SkylandsSkyRenderer.modifyFogColor(fogColor, fogBrightness);
		}

		@Override
		public boolean isFoggyAt(int posX, int posZ)
		{
			return false;
		}

		@Override
		public boolean renderSky(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, Camera camera, Matrix4f projectionMatrix, boolean isFoggy, Runnable setupFog)
		{
			SkylandsSkyRenderer.INSTANCE.render(ticks, partialTick, poseStack, level, camera, projectionMatrix, setupFog);
			return true;
		}

		@Override
		public boolean renderClouds(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, double camX, double camY, double camZ, Matrix4f projectionMatrix)
		{
			if (RediscoveredConfig.CLIENT.customSkylandsClouds())
			{
				SkylandsCloudRenderer.renderLayers(level, ticks, partialTick, poseStack, camX, camY, camZ, projectionMatrix);
				return true;
			}

			return super.renderClouds(level, ticks, partialTick, poseStack, camX, camY, camZ, projectionMatrix);
		}
	}
}