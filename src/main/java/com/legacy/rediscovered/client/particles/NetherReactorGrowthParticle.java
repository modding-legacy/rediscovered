package com.legacy.rediscovered.client.particles;

import org.joml.Quaternionf;

import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Camera;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class NetherReactorGrowthParticle extends RotatedTextureSheetParticle
{
	private final SpriteSet spriteSet;

	private NetherReactorGrowthParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(level, x, y, z, dx, dy, dz);
		this.spriteSet = spriteSet;
		this.hasPhysics = false;
		this.quadSize *= this.random.nextFloat() * 0.6F + 2.0F;
		float speedMul = 0.02F;
		this.xd *= speedMul;
		this.yd *= speedMul;
		this.zd *= speedMul;
		this.lifetime = (int) (Math.random() * 20 + 5);
		this.setSpriteFromAge(spriteSet);
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}

	@Override
	public void render(VertexConsumer buffer, Camera camera, float partialTicks)
	{
		// Fades out at the end of its life
		float a = 1.0F - Mth.clamp((this.age + partialTicks) / this.lifetime, 0.0F, 1.0F);
		this.setAlpha(1.0F - (float) Math.pow(2.0F, -10.0F * a));

		super.render(buffer, camera, partialTicks);
	}

	@Override
	protected Quaternionf getRotation(Camera camera, float partialTicks)
	{
		if (this.roll == 0.0F)
			return Axis.YN.rotationDegrees(camera.getYRot());
		return Axis.YN.rotationDegrees(camera.getYRot()).rotateZ(Mth.lerp(partialTicks, this.oRoll, this.roll));
	}

	@Override
	public void move(double pX, double pY, double pZ)
	{
		this.setBoundingBox(this.getBoundingBox().move(pX, pY, pZ));
		this.setLocationFromBoundingbox();
	}

	@Override
	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;
		if (this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			this.move(this.xd, this.yd, this.zd);
			double friction = 0.99D;
			this.xd *= friction;
			this.yd *= 1.19;
			this.zd *= friction;
			this.setSpriteFromAge(this.spriteSet);
		}
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		public Particle createParticle(SimpleParticleType type, ClientLevel level, double x, double y, double z, double dx, double dy, double dz)
		{
			return new NetherReactorGrowthParticle(level, x, y, z, dx, dy, dz, this.spriteSet);
		}
	}
}
