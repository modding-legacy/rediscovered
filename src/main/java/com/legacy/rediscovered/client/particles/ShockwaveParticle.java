package com.legacy.rediscovered.client.particles;

import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Camera;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.util.Mth;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.UniformFloat;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class ShockwaveParticle extends Particle
{
	boolean pulsed = false;
	int bolts;

	public ShockwaveParticle(ClientLevel level, double x, double y, double z, int lifetime, int bolts)
	{
		super(level, x, y, z, 0.0D, 0.0D, 0.0D);

		this.xd = 0;
		this.yd = 0;
		this.zd = 0;
		this.lifetime = lifetime;
		this.bolts = bolts;
	}

	public static RediscoveredRendering.Bolt createBolt(int bolts, float time)
	{
		float minLength = 0.25F;

		//@formatter:off
		return RediscoveredRendering.bolt()
		.layers(2)
		.bolts(bolts)
		.startScale(0.04F)
		.endScale(0.001F)
		.centerSpacing(time * 2.5F)
		.length(minLength, Math.max(minLength, (1.0F - time) * 2.0F))
		.turns(2, 4)
		.rotationDegrees(UniformFloat.of(0.0F, 360F), UniformFloat.of(0.0F, 0.1F), UniformFloat.of(85.0F, 95.0F))
		.color(UniformFloat.of(0.2F, 0.6F), UniformFloat.of(0.65F, 0.85F), UniformFloat.of(0.80F, 1.0F), ConstantFloat.of(0.30F));
		//@formatter:on
	}

	@Override
	public void render(VertexConsumer buffer, Camera camera, float partialTicks)
	{
		float t = Mth.clamp(this.age + partialTicks, 0, this.lifetime) / this.lifetime;
		PoseStack poseStack = new PoseStack();
		poseStack.pushPose();

		Vec3 vec3 = camera.getPosition();
		float x = (float) (Mth.lerp(partialTicks, this.xo, this.x) - vec3.x());
		float y = (float) (Mth.lerp(partialTicks, this.yo, this.y) - vec3.y());
		float z = (float) (Mth.lerp(partialTicks, this.zo, this.z) - vec3.z());
		poseStack.translate(x, y, z);
		createBolt(this.bolts, t).render(poseStack, buffer, (int) (this.age + this.x + this.z) / 2 * 10);
		poseStack.popPose();
	}

	public int getLightColor(float pPartialTick)
	{
		return LightTexture.FULL_BRIGHT;
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return RediscoveredRendering.Bolt.PARTICLE_RENDER_TYPE;
	}

	@Override
	public boolean shouldCull()
	{
		return false;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<ShockwaveData>
	{
		@Override
		public ShockwaveParticle createParticle(ShockwaveData type, ClientLevel level, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			return new ShockwaveParticle(level, x, y, z, type.lifetime(), type.bolts());
		}
	}
}
