package com.legacy.rediscovered.client.particles;

import java.util.Locale;

import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.RandomSource;

public record LightningBoltData(float length) implements ParticleOptions
{
	public static final Codec<LightningBoltData> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(RediscoveredUtil.NON_NEGATIVE_FLOAT.fieldOf("length").forGetter(LightningBoltData::length)).apply(instance, LightningBoltData::new);
	});

	@SuppressWarnings("deprecation")
	public static final ParticleOptions.Deserializer<LightningBoltData> DESERIALIZER = new ParticleOptions.Deserializer<LightningBoltData>()
	{

		@Override
		public LightningBoltData fromCommand(ParticleType<LightningBoltData> type, StringReader reader) throws CommandSyntaxException
		{
			reader.expect(' ');
			float length = reader.readFloat();
			return new LightningBoltData(length);
		}

		@Override
		public LightningBoltData fromNetwork(ParticleType<LightningBoltData> type, FriendlyByteBuf buff)
		{
			return new LightningBoltData(buff.readFloat());
		}
	};

	public LightningBoltData(RandomSource rand)
	{
		this(15 + rand.nextInt(40));
	}

	@Override
	public void writeToNetwork(FriendlyByteBuf buff)
	{
		buff.writeFloat(this.length);
	}

	@Override
	public ParticleType<?> getType()
	{
		return RediscoveredParticles.LIGHTNINGBOLT;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String writeToString()
	{
		return String.format(Locale.ROOT, "%s %.2f", BuiltInRegistries.PARTICLE_TYPE.getKey(this.getType()), this.length);
	}
}
