package com.legacy.rediscovered.client.particles;

import java.util.Locale;

import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.ExtraCodecs;

public record PylonShieldBlastData(int lifetime, float startSize, float endSize, float startAlpha, float endAlpha) implements ParticleOptions
{

	//@formatter:off
	public static final Codec<PylonShieldBlastData> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ExtraCodecs.NON_NEGATIVE_INT.fieldOf("lifetime").forGetter(PylonShieldBlastData::lifetime), 
				Codec.FLOAT.fieldOf("start_size").forGetter(PylonShieldBlastData::startSize), 
				Codec.FLOAT.fieldOf("end_size").forGetter(PylonShieldBlastData::endSize), 
				RediscoveredUtil.NON_NEGATIVE_FLOAT.fieldOf("start_alpha").forGetter(PylonShieldBlastData::startAlpha), 
				RediscoveredUtil.NON_NEGATIVE_FLOAT.fieldOf("end_alpha").forGetter(PylonShieldBlastData::endAlpha)).apply(instance, PylonShieldBlastData::new);
	});
	//@formatter:on

	public static final ParticleOptions.Deserializer<PylonShieldBlastData> DESERIALIZER = new ParticleOptions.Deserializer<PylonShieldBlastData>()
	{

		@Override
		public PylonShieldBlastData fromCommand(ParticleType<PylonShieldBlastData> type, StringReader reader) throws CommandSyntaxException
		{
			reader.expect(' ');
			int lifetime = reader.readInt();
			reader.expect(' ');
			float startSize = reader.readFloat();
			reader.expect(' ');
			float endSize = reader.readFloat();
			reader.expect(' ');
			float startAlpha = reader.readFloat();
			reader.expect(' ');
			float endAlpha = reader.readFloat();
			return new PylonShieldBlastData(lifetime, startSize, endSize, startAlpha, endAlpha);
		}

		@Override
		public PylonShieldBlastData fromNetwork(ParticleType<PylonShieldBlastData> type, FriendlyByteBuf buff)
		{
			return new PylonShieldBlastData(buff.readInt(), buff.readFloat(), buff.readFloat(), buff.readFloat(), buff.readFloat());
		}
	};

	public PylonShieldBlastData()
	{
		this(10, 0.0F, 30.0F, 1.0F, 0.0F);
	}

	@Override
	public void writeToNetwork(FriendlyByteBuf buff)
	{
		buff.writeInt(this.lifetime);
		buff.writeFloat(this.startSize);
		buff.writeFloat(this.endSize);
		buff.writeFloat(this.startAlpha);
		buff.writeFloat(this.endAlpha);
	}

	@Override
	public ParticleType<?> getType()
	{
		return RediscoveredParticles.PYLON_SHIELD_BLAST;
	}

	@Override
	public String writeToString()
	{
		return String.format(Locale.ROOT, "%s %d %.2f %.2f %.2f %.2f", BuiltInRegistries.PARTICLE_TYPE.getKey(this.getType()), this.lifetime, this.startSize, this.endSize, this.startAlpha, this.endAlpha);
	}
}
