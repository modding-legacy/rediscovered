package com.legacy.rediscovered.client.particles;

import com.legacy.rediscovered.client.render.RediscoveredRendering;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Camera;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.util.Mth;
import net.minecraft.util.valueproviders.ConstantFloat;
import net.minecraft.util.valueproviders.UniformFloat;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class LightningBoltParticle extends Particle
{
	final RediscoveredRendering.Bolt bolt;

	public LightningBoltParticle(ClientLevel level, double x, double y, double z, float length)
	{
		super(level, x, y, z, 0.0D, 0.0D, 0.0D);
		this.xd = 0;
		this.yd = 0;
		this.zd = 0;
		this.lifetime = 2 + level.random.nextInt(3);
		this.bolt = createBolt(length);
	}

	public static RediscoveredRendering.Bolt createBolt(float length)
	{
		//@formatter:off
		return RediscoveredRendering.bolt()
		.layers(2)
		.bolts(1)
		.startScale(0.7F)
		.endScale(0.1F)
		.length(length)
		.turns(5, 10)
		.rotationDegrees(ConstantFloat.ZERO, ConstantFloat.ZERO, UniformFloat.of(175F, 185F))
		.limitedChaos()
		.color(UniformFloat.of(0.2F, 0.6F), UniformFloat.of(0.65F, 0.85F), UniformFloat.of(0.80F, 1.0F), ConstantFloat.of(0.30F));
		//@formatter:on
	}

	@Override
	public void render(VertexConsumer buffer, Camera camera, float partialTicks)
	{
		PoseStack poseStack = new PoseStack();
		poseStack.pushPose();

		Vec3 vec3 = camera.getPosition();
		float x = (float) (Mth.lerp(partialTicks, this.xo, this.x) - vec3.x());
		float y = (float) (Mth.lerp(partialTicks, this.yo, this.y) - vec3.y());
		float z = (float) (Mth.lerp(partialTicks, this.zo, this.z) - vec3.z());
		poseStack.translate(x, y, z);
		this.bolt.render(poseStack, buffer, (int) (this.age + this.x + this.z) / 2 * 10);
		poseStack.popPose();
	}

	public int getLightColor(float pPartialTick)
	{
		return LightTexture.FULL_BRIGHT;
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return RediscoveredRendering.Bolt.PARTICLE_RENDER_TYPE;
	}

	@Override
	public boolean shouldCull()
	{
		return false;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<LightningBoltData>
	{
		@Override
		public LightningBoltParticle createParticle(LightningBoltData type, ClientLevel level, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			return new LightningBoltParticle(level, x, y, z, type.length());
		}
	}
}
