package com.legacy.rediscovered.client.particles;

import java.util.Locale;

import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.ExtraCodecs;

public record NetherReactorPulseData(int pulses, int lifetime, float maxSize) implements ParticleOptions
{
	public static final Codec<NetherReactorPulseData> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ExtraCodecs.NON_NEGATIVE_INT.fieldOf("pulses").forGetter(NetherReactorPulseData::pulses), ExtraCodecs.NON_NEGATIVE_INT.fieldOf("lifetime").forGetter(NetherReactorPulseData::lifetime), ExtraCodecs.POSITIVE_FLOAT.fieldOf("max_size").forGetter(NetherReactorPulseData::maxSize)).apply(instance, NetherReactorPulseData::new);
	});

	public static final ParticleOptions.Deserializer<NetherReactorPulseData> DESERIALIZER = new ParticleOptions.Deserializer<NetherReactorPulseData>()
	{

		@Override
		public NetherReactorPulseData fromCommand(ParticleType<NetherReactorPulseData> type, StringReader reader) throws CommandSyntaxException
		{
			reader.expect(' ');
			int pulses = reader.readInt();
			reader.expect(' ');
			int lifetime = reader.readInt();
			reader.expect(' ');
			float maxSize = reader.readFloat();
			return new NetherReactorPulseData(pulses, lifetime, maxSize);
		}

		@Override
		public NetherReactorPulseData fromNetwork(ParticleType<NetherReactorPulseData> type, FriendlyByteBuf buff)
		{
			return new NetherReactorPulseData(buff.readInt(), buff.readInt(), buff.readFloat());
		}
	};
	
	public NetherReactorPulseData(int pulses)
	{
		this(pulses, 20, 5.0F);
	}

	@Override
	public void writeToNetwork(FriendlyByteBuf buff)
	{
		buff.writeInt(this.pulses);
		buff.writeInt(this.lifetime);
		buff.writeFloat(this.maxSize);
	}

	@Override
	public ParticleType<?> getType()
	{
		return RediscoveredParticles.NETHER_REACTOR_PULSE;
	}

	@Override
	public String writeToString()
	{
		return String.format(Locale.ROOT, "%s %d %d %.2f", BuiltInRegistries.PARTICLE_TYPE.getKey(this.getType()), this.pulses, this.lifetime, this.maxSize);
	}
}
