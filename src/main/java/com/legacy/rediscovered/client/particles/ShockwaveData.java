package com.legacy.rediscovered.client.particles;

import java.util.Locale;

import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.ExtraCodecs;

public record ShockwaveData(int lifetime, int bolts) implements ParticleOptions
{
	public static final Codec<ShockwaveData> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(
				ExtraCodecs.NON_NEGATIVE_INT.fieldOf("lifetime").forGetter(ShockwaveData::lifetime), 
				ExtraCodecs.NON_NEGATIVE_INT.fieldOf("bolts").forGetter(ShockwaveData::bolts)).apply(instance, ShockwaveData::new);
	});

	public static final ParticleOptions.Deserializer<ShockwaveData> DESERIALIZER = new ParticleOptions.Deserializer<ShockwaveData>()
	{

		@Override
		public ShockwaveData fromCommand(ParticleType<ShockwaveData> type, StringReader reader) throws CommandSyntaxException
		{
			reader.expect(' ');
			int lifetime = reader.readInt();
			reader.expect(' ');
			int bolts = reader.readInt();
			return new ShockwaveData(lifetime, bolts);
		}

		@Override
		public ShockwaveData fromNetwork(ParticleType<ShockwaveData> type, FriendlyByteBuf buff)
		{
			return new ShockwaveData(buff.readInt(), buff.readInt());
		}
	};
	
	public ShockwaveData()
	{
		this(10, 20);
	}

	@Override
	public void writeToNetwork(FriendlyByteBuf buff)
	{
		buff.writeInt(this.lifetime);
		buff.writeInt(this.bolts);
	}

	@Override
	public ParticleType<?> getType()
	{
		return RediscoveredParticles.SHOCKWAVE;
	}

	@Override
	public String writeToString()
	{
		return String.format(Locale.ROOT, "%s %d %d", BuiltInRegistries.PARTICLE_TYPE.getKey(this.getType()), this.lifetime, this.bolts);
	}
}
