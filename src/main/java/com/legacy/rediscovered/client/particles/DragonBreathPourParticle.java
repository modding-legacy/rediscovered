package com.legacy.rediscovered.client.particles;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

// Basically just dragon breath, but it always moves downward
public class DragonBreathPourParticle extends TextureSheetParticle
{
	boolean hasHitGround;
	final SpriteSet spriteSet;

	private DragonBreathPourParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(level, x, y, z);
		this.friction = 0.96F;
		this.xd = dx;
		// Always move downward
		this.yd = -Math.abs(dy) * 6;
		this.zd = dz;
		this.rCol = Mth.nextFloat(this.random, 0.7176471F, 0.8745098F);
		this.gCol = Mth.nextFloat(this.random, 0.0F, 0.0F);
		this.bCol = Mth.nextFloat(this.random, 0.8235294F, 0.9764706F);
		this.quadSize *= 0.75F;
		this.lifetime = (int) (20.0D / ((double) this.random.nextFloat() * 0.8D + 0.2D));
		this.hasHitGround = false;
		this.hasPhysics = false;
		this.spriteSet = spriteSet;
		this.setSpriteFromAge(spriteSet);
	}

	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;
		if (this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			this.setSpriteFromAge(this.spriteSet);
			if (this.onGround)
			{
				this.yd = 0.0D;
				this.hasHitGround = true;
			}

			if (this.hasHitGround)
			{
				this.yd += 0.002D;
			}

			this.move(this.xd, this.yd, this.zd);
			if (this.y == this.yo)
			{
				this.xd *= 1.1D;
				this.zd *= 1.1D;
			}

			this.xd *= (double) this.friction;
			this.zd *= (double) this.friction;
			if (this.hasHitGround)
			{
				this.yd *= (double) this.friction;
			}

		}
	}

	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	public float getQuadSize(float pScaleFactor)
	{
		return this.quadSize * Mth.clamp(((float) this.age + pScaleFactor) / (float) this.lifetime * 32.0F, 0.0F, 1.0F);
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		public Particle createParticle(SimpleParticleType type, ClientLevel level, double x, double y, double z, double dx, double dy, double dz)
		{
			return new DragonBreathPourParticle(level, x, y, z, dx, dy, dz, this.spriteSet);
		}
	}
}
