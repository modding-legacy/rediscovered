package com.legacy.rediscovered.client.particles;

import java.util.Locale;

import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.registry.RediscoveredParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.ExtraCodecs;

public record WindData(int pulses, float scale) implements ParticleOptions
{
	public static final Codec<WindData> CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(ExtraCodecs.NON_NEGATIVE_INT.fieldOf("pulses").forGetter(WindData::pulses), RediscoveredUtil.NON_NEGATIVE_FLOAT.fieldOf("scale").forGetter(WindData::scale)).apply(instance, WindData::new);
	});

	public static final ParticleOptions.Deserializer<WindData> DESERIALIZER = new ParticleOptions.Deserializer<WindData>()
	{

		@Override
		public WindData fromCommand(ParticleType<WindData> type, StringReader reader) throws CommandSyntaxException
		{
			reader.expect(' ');
			int pulses = reader.readInt();
			reader.expect(' ');
			float scale = reader.readFloat();
			return new WindData(pulses, scale);
		}

		@Override
		public WindData fromNetwork(ParticleType<WindData> type, FriendlyByteBuf buff)
		{
			return new WindData(buff.readInt(), buff.readFloat());
		}
	};

	@Override
	public void writeToNetwork(FriendlyByteBuf buff)
	{
		buff.writeInt(this.pulses);
		buff.writeFloat(this.scale);
	}

	@Override
	public ParticleType<?> getType()
	{
		return RediscoveredParticles.WIND;
	}

	@Override
	public String writeToString()
	{
		return String.format(Locale.ROOT, "%s %d %.2f", BuiltInRegistries.PARTICLE_TYPE.getKey(this.getType()), this.pulses, this.scale);
	}
}
