package com.legacy.rediscovered.client.particles;

import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.RediscoveredRenderType;
import com.legacy.rediscovered.client.render.model.DragonPylonRenderer;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class PylonShieldBlastParticle extends Particle
{
	private final ModelPart model;
	private final float initialRotation;
	final float startSize, endSize, startAlpha, endAlpha;

	public PylonShieldBlastParticle(ClientLevel level, double x, double y, double z, int lifetime, float startSize, float endSize, float startAlpha, float endAlpha)
	{
		super(level, x, y, z, 0.0D, 0.0D, 0.0D);

		this.model = Minecraft.getInstance().getEntityModels().bakeLayer(RediscoveredRenderRefs.DRAGON_PYLON).getChild("cube");
		this.xd = 0;
		this.yd = 0;
		this.zd = 0;
		this.initialRotation = level.random.nextFloat() * 360;
		this.lifetime = lifetime;

		this.startSize = startSize;
		this.endSize = endSize;

		this.startAlpha = startAlpha;
		this.endAlpha = endAlpha;
	}

	@Override
	public void render(VertexConsumer buffer, Camera camera, float partialTicks)
	{
		float age = this.age + partialTicks;
		float endPercent = Mth.clamp(age / this.lifetime, 0.0F, 1.0F);
		float startPercent = 1.0F - endPercent;
		
		PoseStack poseStack = new PoseStack();
		poseStack.pushPose();
		Vec3 vec3 = camera.getPosition();
		float x = (float) (Mth.lerp(partialTicks, this.xo, this.x) - vec3.x());
		float y = (float) (Mth.lerp(partialTicks, this.yo, this.y) - vec3.y());
		float z = (float) (Mth.lerp(partialTicks, this.zo, this.z) - vec3.z());
		poseStack.translate(x, y, z);

		float scale = this.startSize * startPercent + this.endSize * endPercent;
		float alpha = this.startAlpha * startPercent + this.endAlpha * endPercent;
		float rotation = this.initialRotation + age * 20;

		MultiBufferSource.BufferSource buffSource = Minecraft.getInstance().renderBuffers().bufferSource();
		VertexConsumer energyBuffer = DragonPylonRenderer.ENERGY_SHIELD_TEXTURE.buffer(buffSource, RediscoveredRenderType::energy);
		DragonPylonRenderer.renderPylonShields(this.model, poseStack, energyBuffer, this.getLightColor(partialTicks), rotation, scale, 1.18F, 1, alpha);
		
		buffSource.endBatch();
		poseStack.popPose();
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	public int getLightColor(float pPartialTick)
	{
		return LightTexture.FULL_BRIGHT;
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.CUSTOM;
	}

	@Override
	public boolean shouldCull()
	{
		return false;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<PylonShieldBlastData>
	{
		@Override
		public PylonShieldBlastParticle createParticle(PylonShieldBlastData type, ClientLevel level, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			return new PylonShieldBlastParticle(level, x, y, z, type.lifetime(), type.startSize(), type.endSize(), type.startAlpha(), type.endAlpha());
		}
	}
}
