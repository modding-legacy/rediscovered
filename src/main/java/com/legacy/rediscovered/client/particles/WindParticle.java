package com.legacy.rediscovered.client.particles;

import java.util.List;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class WindParticle extends RotatedTextureSheetParticle
{
	final SpriteSet spriteSet;
	final float initialRotation, deltaRotation;
	final float baseAlpha;
	final int pulses;
	final float initialScale;

	private WindParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet, int pulses, float initialScale)
	{
		super(level, x, y, z);
		this.spriteSet = spriteSet;
		this.xd = dx;
		this.yd = dy;
		this.zd = dz;
		this.x = x;
		this.y = y;
		this.z = z;
		this.quadSize = 2.0F * (this.random.nextFloat() * 0.2F + 0.5F) * initialScale;
		this.initialScale = initialScale;
		float f = this.random.nextFloat() + 0.8F;
		this.rCol *= Mth.clamp(f, 0.8F, 1.0F);
		this.gCol *= Mth.clamp(f, 0.9F, 1.0F);
		this.bCol *= Mth.clamp(f, 0.9F, 1.0F);
		this.baseAlpha = this.initialScale > 1.0F ? 0.8F : 0.40F + this.random.nextFloat() * 0.3F;
		this.alpha = this.baseAlpha;
		this.lifetime = (int) (this.random.nextFloat() * 5.0F) + 10 + (int) (0.5F * (this.initialScale - 1.0F));
		this.hasPhysics = this.initialScale == 1.0F;
		this.initialRotation = this.random.nextFloat() * 360 * Mth.DEG_TO_RAD;
		this.roll = this.initialRotation;
		this.deltaRotation = (this.random.nextFloat() - 0.5F) * 200 * Mth.DEG_TO_RAD;
		float scale = 0.01F;
		this.setSize(scale, scale);
		this.setSpriteFromAge(spriteSet);
		this.pulses = pulses;
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}

	@Override
	public float getQuadSize(float partialTicks)
	{
		float light = this.level.getBrightness(LightLayer.SKY, BlockPos.containing(this.getBoundingBox().getCenter())) / (float) this.level.getMaxLightLevel();

		float a = (this.age + partialTicks) / (float) this.lifetime;
		return (this.quadSize + (a * 2.0F)) * light;
	}

	@Override
	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;
		this.oRoll = this.roll;
		if (this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			if (this.pulses > 0 && this.age == this.lifetime / 3)
			{
				this.level.addParticle(new WindData(this.pulses - 1, this.initialScale), this.x - xd, this.y - yd, this.z - zd, this.xd, this.yd, this.zd);
			}

			float a = this.age / (float) this.lifetime;
			this.alpha = this.baseAlpha * (1.0f - a);
			this.roll = a * this.deltaRotation + this.initialRotation;
			this.setSpriteFromAge(this.spriteSet);
			this.move(this.xd, this.yd, this.zd);
		}
	}

	private static final double MAXIMUM_COLLISION_VELOCITY_SQUARED = Mth.square(100.0D);
	boolean stoppedByCollision = false;

	public void move(double pX, double pY, double pZ)
	{
		if (!this.stoppedByCollision)
		{
			double d0 = pX;
			double d1 = pY;
			double d2 = pZ;
			if (this.hasPhysics && (pX != 0.0D || pY != 0.0D || pZ != 0.0D) && pX * pX + pY * pY + pZ * pZ < MAXIMUM_COLLISION_VELOCITY_SQUARED)
			{
				Vec3 vec3 = Entity.collideBoundingBox((Entity) null, new Vec3(pX, pY, pZ), this.getBoundingBox(), this.level, List.of());
				pX = vec3.x;
				pY = vec3.y;
				pZ = vec3.z;

				// Collision detection
				if (pX != d0 || pY != d1 || pZ != d2)
				{
					//Vec3 c = this.getBoundingBox().getCenter();
					//level.addParticle(ParticleTypes.HAPPY_VILLAGER, c.x, c.y, c.z, 0, 0, 0);
					this.remove();
				}
			}

			if (pX != 0.0D || pY != 0.0D || pZ != 0.0D)
			{
				this.setBoundingBox(this.getBoundingBox().move(pX, pY, pZ));
				this.setLocationFromBoundingbox();
			}

			if (Math.abs(d1) >= (double) 1.0E-5F && Math.abs(pY) < (double) 1.0E-5F)
			{
				this.stoppedByCollision = true;
			}

			this.onGround = d1 != pY && d1 < 0.0D;
			if (d0 != pX)
			{
				this.xd = 0.0D;
			}

			if (d2 != pZ)
			{
				this.zd = 0.0D;
			}

		}
	}

	@Override
	protected int getLightColor(float partialTick)
	{
		int l = super.getLightColor(partialTick);
		int min = 12;
		return LightTexture.pack(LightTexture.block(l), Math.max(min, LightTexture.sky(l)));
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<WindData>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		public Particle createParticle(WindData data, ClientLevel level, double x, double y, double z, double dx, double dy, double dz)
		{
			return new WindParticle(level, x, y, z, dx, dy, dz, this.spriteSet, data.pulses(), data.scale());
		}
	}
}
