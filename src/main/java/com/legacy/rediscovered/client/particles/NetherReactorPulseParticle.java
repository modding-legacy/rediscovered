package com.legacy.rediscovered.client.particles;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredRenderRefs;
import com.legacy.rediscovered.client.render.model.NetherReactorPulseModel;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.Model;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class NetherReactorPulseParticle extends Particle
{
	private static final RenderType RENDER_TYPE = RenderType.entityTranslucent(RediscoveredMod.locate("textures/particle/nether_reactor_pulse_0.png"));

	private final Model model;
	private float alpha = 1.0F, alphaO = 1.0F;
	private final float initialRotation;
	private int pulses = 0;
	boolean pulsed = false;
	final float maxSize;
	
	public NetherReactorPulseParticle(ClientLevel level, double x, double y, double z, int pulses, int lifetime, float maxSize)
	{
		super(level, x, y, z, 0.0D, 0.0D, 0.0D);

		this.model = new NetherReactorPulseModel<>(Minecraft.getInstance().getEntityModels().bakeLayer(RediscoveredRenderRefs.NETHER_REACTOR_PULSE));
		this.xd = 0;
		this.yd = 0;
		this.zd = 0;
		this.initialRotation = level.random.nextFloat() * 360;
		this.lifetime = lifetime;
		this.pulses = pulses;
		this.maxSize = maxSize;
	}

	@Override
	public void render(VertexConsumer buffer, Camera camera, float partialTicks)
	{
		float age = this.age + partialTicks;

		PoseStack poseStack = new PoseStack();
		poseStack.pushPose();
		Vec3 vec3 = camera.getPosition();
		float f = (float) (Mth.lerp(partialTicks, this.xo, this.x) - vec3.x());
		float f1 = (float) (Mth.lerp(partialTicks, this.yo, this.y) - vec3.y());
		float f2 = (float) (Mth.lerp(partialTicks, this.zo, this.z) - vec3.z());
		poseStack.translate(f, f1, f2);

		poseStack.mulPose(Axis.YP.rotationDegrees(age * 2 + this.initialRotation));
		
		float scale = age / this.lifetime * this.maxSize;
		poseStack.scale(scale, scale, scale);
		
		MultiBufferSource.BufferSource buffSource = Minecraft.getInstance().renderBuffers().bufferSource();
		VertexConsumer particleBuff = buffSource.getBuffer(RENDER_TYPE);
		this.model.renderToBuffer(poseStack, particleBuff, 15728880, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, Mth.lerp(partialTicks, this.alphaO, this.alpha));

		buffSource.endBatch();
		poseStack.popPose();
	}

	public int getLightColor(float pPartialTick)
	{
		return LightTexture.FULL_BRIGHT;
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.CUSTOM;
	}

	@Override
	public void tick()
	{
		super.tick();

		// When at half your lifetime, send another pulse
		if (!this.pulsed && this.pulses > 0 && this.age / (float) this.lifetime > 0.5F)
		{
			
			this.level.addParticle(new NetherReactorPulseData(this.pulses - 1, this.lifetime, this.maxSize), this.x, this.y + 0.001, this.z, 0, 0, 0);
			this.pulsed = true;
		}
		this.alphaO = this.alpha;
		this.alpha = 1.0F - (this.age / (float) this.lifetime);
	}

	@Override
	public boolean shouldCull()
	{
		return false;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<NetherReactorPulseData>
	{
		@Override
		public NetherReactorPulseParticle createParticle(NetherReactorPulseData type, ClientLevel level, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			return new NetherReactorPulseParticle(level, x, y, z, type.pulses(), type.lifetime(), type.maxSize());
		}
	}
}
