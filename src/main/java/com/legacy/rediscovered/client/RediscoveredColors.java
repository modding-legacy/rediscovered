package com.legacy.rediscovered.client;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.item.util.DragonArmorTrim;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.client.Minecraft;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.client.event.RegisterColorHandlersEvent;

@Mod.EventBusSubscriber(modid = RediscoveredMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class RediscoveredColors
{
	@SubscribeEvent
	public static void registerItemColors(final RegisterColorHandlersEvent.Item event)
	{
		event.register((stack, layer) ->
		{
			return layer == 0 ? ((DyeableLeatherItem) stack.getItem()).getColor(stack) : -1;
		}, RediscoveredItems.studded_boots, RediscoveredItems.studded_leggings, RediscoveredItems.studded_chestplate, RediscoveredItems.studded_helmet, RediscoveredItems.quiver);
		event.register((stack, layer) ->
		{
			return event.getItemColors().getColor(Items.GRASS_BLOCK.getDefaultInstance(), layer);
		}, RediscoveredBlocks.grass_slab);
		event.register((stack, layer) ->
		{
			return (Minecraft.getInstance().level != null ? DragonArmorTrim.getTrim(Minecraft.getInstance().level.registryAccess(), stack).orElse(DragonArmorTrim.EMPTY) : DragonArmorTrim.EMPTY).getColor(layer);
		}, RediscoveredItems.dragon_armor);
	}

	@SubscribeEvent
	public static void registerBlockColors(final RegisterColorHandlersEvent.Block event)
	{
		event.register((state, level, pos, index) ->
		{
			return event.getBlockColors().getColor(Blocks.GRASS_BLOCK.defaultBlockState(), level, pos, index);
		}, RediscoveredBlocks.grass_slab);
	}
}
