package com.legacy.rediscovered.client;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.render.block_entity.GearRenderer;
import com.legacy.rediscovered.client.render.block_entity.MiniDragonPylonRenderer;
import com.legacy.rediscovered.client.render.block_entity.RedDragonEggRenderer;
import com.legacy.rediscovered.client.render.block_entity.TableRenderer;
import com.legacy.rediscovered.client.render.entity.BeastBoyRenderer;
import com.legacy.rediscovered.client.render.entity.BlackSteveRenderer;
import com.legacy.rediscovered.client.render.entity.BoltBallRenderer;
import com.legacy.rediscovered.client.render.entity.FishRenderer;
import com.legacy.rediscovered.client.render.entity.MeleePigmanRenderer;
import com.legacy.rediscovered.client.render.entity.PigmanRenderer;
import com.legacy.rediscovered.client.render.entity.PurpleArrowRenderer;
import com.legacy.rediscovered.client.render.entity.PylonBurstRenderer;
import com.legacy.rediscovered.client.render.entity.RanaRenderer;
import com.legacy.rediscovered.client.render.entity.RangedPigmanRenderer;
import com.legacy.rediscovered.client.render.entity.RedDragonBossRenderer;
import com.legacy.rediscovered.client.render.entity.RedDragonOffspringRenderer;
import com.legacy.rediscovered.client.render.entity.ScarecrowRenderer;
import com.legacy.rediscovered.client.render.entity.SteveRenderer;
import com.legacy.rediscovered.client.render.entity.ThunderCloudRenderer;
import com.legacy.rediscovered.client.render.entity.ZombiePigmanRenderer;
import com.legacy.rediscovered.client.render.entity.layer.QuiverArmorLayer;
import com.legacy.rediscovered.client.render.model.DragonPylonRenderer;
import com.legacy.rediscovered.client.render.model.FishModel;
import com.legacy.rediscovered.client.render.model.GearModel;
import com.legacy.rediscovered.client.render.model.NetherReactorPulseModel;
import com.legacy.rediscovered.client.render.model.PigmanModel;
import com.legacy.rediscovered.client.render.model.RanaModel;
import com.legacy.rediscovered.client.render.model.RedDragonModel;
import com.legacy.rediscovered.client.render.model.ScarecrowModel;
import com.legacy.rediscovered.client.render.model.SteveModel;
import com.legacy.rediscovered.client.render.model.ThunderCloudModel;
import com.legacy.rediscovered.registry.RediscoveredBlockEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HumanoidArmorModel;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.client.renderer.entity.NoopRenderer;
import net.minecraft.client.resources.PlayerSkin;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

@Mod.EventBusSubscriber(modid = RediscoveredMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class RediscoveredEntityRendering
{
	@SubscribeEvent
	public static void initLayers(final EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		final CubeDeformation fullDeform = new CubeDeformation(1.0F);
		final CubeDeformation halfDeform = new CubeDeformation(0.5F);

		LayerDefinition pigmanBodyLayer = PigmanModel.createBodyLayer(CubeDeformation.NONE);
		LayerDefinition zombieProfessionLayer = PigmanModel.createBodyLayer(halfDeform);

		LayerDefinition outerArmor = LayerDefinition.create(HumanoidArmorModel.createBodyLayer(fullDeform), 64, 32);
		LayerDefinition innerArmor = LayerDefinition.create(HumanoidArmorModel.createBodyLayer(halfDeform), 64, 32);

		event.registerLayerDefinition(RediscoveredRenderRefs.QUIVER, () -> LayerDefinition.create(HumanoidArmorModel.createBodyLayer(new CubeDeformation(1.1F)), 64, 32));

		event.registerLayerDefinition(RediscoveredRenderRefs.PIGMAN, () -> pigmanBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.PIGMAN_INNER_ARMOR, () -> innerArmor);
		event.registerLayerDefinition(RediscoveredRenderRefs.PIGMAN_OUTER_ARMOR, () -> outerArmor);

		event.registerLayerDefinition(RediscoveredRenderRefs.ZOMBIE_PIGMAN, () -> pigmanBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.ZOMBIE_PIGMAN_PROFESSION, () -> zombieProfessionLayer);

		event.registerLayerDefinition(RediscoveredRenderRefs.FISH, FishModel::createBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.SCARECROW, ScarecrowModel::createBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.GEAR, GearModel::createBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.STEVE, SteveModel::createBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.RANA, RanaModel::createBodyLayer);

		event.registerLayerDefinition(RediscoveredRenderRefs.RED_DRAGON, () -> RedDragonModel.createBodyLayer(CubeDeformation.NONE, CubeDeformation.NONE));
		event.registerLayerDefinition(RediscoveredRenderRefs.RED_DRAGON_ARMOR, () -> RedDragonModel.createBodyLayer(new CubeDeformation(0.3F), new CubeDeformation(0.6F)));
		event.registerLayerDefinition(RediscoveredRenderRefs.RED_DRAGON_SADDLE, () -> RedDragonModel.createBodyLayer(new CubeDeformation(0.75F), new CubeDeformation(0.9F)));
		event.registerLayerDefinition(RediscoveredRenderRefs.DRAGON_PYLON, DragonPylonRenderer::createBodyLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.BOLT_BALL_WIND, BoltBallRenderer::createWindLayer);
		event.registerLayerDefinition(RediscoveredRenderRefs.THUNDER_CLOUD, ThunderCloudModel::createBodyLayer);

		event.registerLayerDefinition(RediscoveredRenderRefs.NETHER_REACTOR_PULSE, NetherReactorPulseModel::createRingLayer);
	}

	@SubscribeEvent
	public static void initRenders(final EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(RediscoveredEntityTypes.FISH, FishRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.PIGMAN, PigmanRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.MELEE_PIGMAN, MeleePigmanRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.RANGED_PIGMAN, RangedPigmanRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.ZOMBIE_PIGMAN, ZombiePigmanRenderer::new);

		event.registerEntityRenderer(RediscoveredEntityTypes.STEVE, SteveRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.BLACK_STEVE, BlackSteveRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.BEAST_BOY, BeastBoyRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.RANA, RanaRenderer::new);

		event.registerEntityRenderer(RediscoveredEntityTypes.RED_DRAGON_BOSS, RedDragonBossRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.RED_DRAGON_OFFSPRING, RedDragonOffspringRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.BOLT_BALL, BoltBallRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.THUNDER_CLOUD, ThunderCloudRenderer::new);

		event.registerEntityRenderer(RediscoveredEntityTypes.DRAGON_PYLON, DragonPylonRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.PYLON_BURST, PylonBurstRenderer::new);

		event.registerEntityRenderer(RediscoveredEntityTypes.SCARECROW, ScarecrowRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.PURPLE_ARROW, PurpleArrowRenderer::new);
		event.registerEntityRenderer(RediscoveredEntityTypes.MOUNTABLE_BLOCK, NoopRenderer::new);

		event.registerBlockEntityRenderer(RediscoveredBlockEntityTypes.GEAR, GearRenderer::new);
		event.registerBlockEntityRenderer(RediscoveredBlockEntityTypes.TABLE, TableRenderer::new);
		event.registerBlockEntityRenderer(RediscoveredBlockEntityTypes.MIN_DRAGON_PYLON, MiniDragonPylonRenderer::new);
		event.registerBlockEntityRenderer(RediscoveredBlockEntityTypes.RED_DRAGON_EGG, RedDragonEggRenderer::new);

	}

	@SuppressWarnings({ "resource", "unchecked" })
	@SubscribeEvent
	public static <T extends LivingEntity, M extends HumanoidModel<T>> void initRenders(final EntityRenderersEvent.AddLayers event)
	{
		var context = event.getContext();
		for (EntityRenderer<?> renderer : Minecraft.getInstance().getEntityRenderDispatcher().renderers.values())
			if (renderer instanceof LivingEntityRenderer living)
				addQuiverLayer(living, context);

		var skinMap = Minecraft.getInstance().getEntityRenderDispatcher().getSkinMap();
		addQuiverLayer(skinMap.get(PlayerSkin.Model.WIDE), context);
		addQuiverLayer(skinMap.get(PlayerSkin.Model.SLIM), context);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static <T extends LivingEntity, M extends HumanoidModel<T>> void addQuiverLayer(EntityRenderer<? extends LivingEntity> entityRenderer, EntityRendererProvider.Context context)
	{
		if (entityRenderer instanceof LivingEntityRenderer living && living.getModel() instanceof HumanoidModel<?>)
			living.addLayer(new QuiverArmorLayer<T, M>(living, context));
	}
}