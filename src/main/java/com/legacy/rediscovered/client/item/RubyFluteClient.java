package com.legacy.rediscovered.client.item;

import org.jetbrains.annotations.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.HumanoidModel.ArmPose;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.item.ClampedItemPropertyFunction;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.client.event.RenderHandEvent;
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class RubyFluteClient implements IClientItemExtensions
{
	public static final ArmPose ARM_POS = ArmPose.create(RediscoveredMod.MODID + "_ruby_flute", true, RubyFluteClient::renderArmPos);

	@Override
	public @Nullable ArmPose getArmPose(LivingEntity entityLiving, InteractionHand hand, ItemStack itemStack)
	{
		return ARM_POS;
	}

	// Positions the arms in third person
	private static void renderArmPos(HumanoidModel<? extends LivingEntity> model, LivingEntity entity, HumanoidArm arm)
	{
		if (entity != null && entity.getUseItem().is(RediscoveredItems.ruby_flute))
		{
			// Wrap the rotation because head rotation gets offset when you ride something.
			float headY = Mth.wrapDegrees(model.head.yRot * Mth.RAD_TO_DEG) * Mth.DEG_TO_RAD;
			if (arm == HumanoidArm.RIGHT)
			{
				float scaledHeadRot = (headY - Mth.TWO_PI) * 0.3F;
				model.rightArm.xRot = -90.0F * Mth.DEG_TO_RAD;
				model.rightArm.yRot = 60.0F * Mth.DEG_TO_RAD + scaledHeadRot;
				model.rightArm.x -= 0.5F;

				model.leftArm.xRot = -85.0F * Mth.DEG_TO_RAD;
				model.leftArm.yRot = 180.0F * Mth.DEG_TO_RAD + scaledHeadRot * 1.8F;
				model.leftArm.z += 1.5F;
			}
			else
			{
				float scaledHeadRot = (headY - Mth.TWO_PI) * 0.3F;
				model.rightArm.xRot = -85.0F * Mth.DEG_TO_RAD;
				model.rightArm.yRot = 210.0F * Mth.DEG_TO_RAD + scaledHeadRot * 1.8F;
				model.rightArm.z += 1.0F;

				model.leftArm.xRot = -90.0F * Mth.DEG_TO_RAD;
				model.leftArm.yRot = (60.0F + 90.0F) * Mth.DEG_TO_RAD + scaledHeadRot;
				model.leftArm.x += 0.5F;
			}

			AnimationUtils.bobModelPart(model.leftArm, entity.tickCount, 1.0F);
			AnimationUtils.bobModelPart(model.rightArm, entity.tickCount, -1.0F);
		}
	}

	// Gives the hand using the ruby flute a slight bob up and down
	@Override
	public boolean applyForgeHandTransform(PoseStack poseStack, LocalPlayer player, HumanoidArm arm, ItemStack itemInHand, float partialTick, float equipProcess, float swingProcess)
	{
		if (player.isUsingItem() && player.getUseItemRemainingTicks() > 0 && itemInHand.is(RediscoveredItems.ruby_flute))
		{
			poseStack.translate(0, Math.sin((player.tickCount + partialTick) * 0.7) * 0.015, 0);
			return true;
		}
		return false;
	}

	@SubscribeEvent
	public static void onHandRender(RenderHandEvent event)
	{
		// When using the ruby flute, don't render the other hand
		Player player = Minecraft.getInstance().player;
		if (player.isUsingItem() && player.getUseItemRemainingTicks() > 0 && player.getUseItem().is(RediscoveredItems.ruby_flute) && player.getUsedItemHand() != event.getHand())
		{
			event.setCanceled(true);
		}
	}

	public static class PropertyFunction implements ClampedItemPropertyFunction
	{
		@Override
		public float unclampedCall(ItemStack stack, ClientLevel level, LivingEntity entity, int pSeed)
		{
			return entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F;
		}
	}
}
