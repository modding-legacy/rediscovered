package com.legacy.rediscovered.client.item;

import com.legacy.rediscovered.item.RubyEyeItem;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.item.ClampedItemPropertyFunction;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.Vec2;
import net.minecraft.world.phys.Vec3;

public class RubyEyeClient
{
	public static class PropertyFunction implements ClampedItemPropertyFunction
	{
		@Override
		public float unclampedCall(ItemStack stack, ClientLevel level, LivingEntity entity, int pSeed)
		{
			int texIndex = RubyEyeItem.getTextureIndex(stack);
			if (texIndex > -1)
				return texIndex / (float) RubyEyeItem.TEXTURE_COUNT;

			BlockPos pos = RubyEyeItem.getStructurePos(stack);
			Entity representation = stack.getEntityRepresentation();
			Entity e = representation == null ? entity : representation;
			if (pos != null && e != null && level.dimension().equals(RubyEyeItem.getStructureDimension(stack)))
			{
				Vec3 sPos = pos.getCenter();
				Vec3 ePos = e.position();

				Vec3 lookAngle = e.getLookAngle();
				Vec2 look = new Vec2((float) lookAngle.x, (float) lookAngle.z).normalized();
				Vec2 nPos = new Vec2((float) (sPos.x - ePos.x), (float) (sPos.z - ePos.z)).normalized();

				float field = Mth.clamp(Math.round(look.distanceToSqr(nPos) / 4.0 * RubyEyeItem.TEXTURE_COUNT), 0, RubyEyeItem.TEXTURE_COUNT) / (float) RubyEyeItem.TEXTURE_COUNT;
				return field;
			}
			return 1.0F;
		}
	}
}
