package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.pigman.data.PigmanData;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.syncher.EntityDataSerializer;
import net.neoforged.neoforge.registries.NeoForgeRegistries;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredDataSerialization
{
	public static final EntityDataSerializer<PigmanData> PIGMAN_DATA = new EntityDataSerializer.ForValueType<PigmanData>()
	{
		@Override
		public void write(FriendlyByteBuf buf, PigmanData data)
		{
			buf.writeUtf(data.getProfession().profName);
			buf.writeVarInt(data.getLevel());
		}

		@Override
		public PigmanData read(FriendlyByteBuf buf)
		{
			return new PigmanData(buf.readUtf(), buf.readVarInt());
		}
	};

	public static void init(RegisterEvent event)
	{
		register(event, "pigman_data", PIGMAN_DATA);
	}

	private static void register(RegisterEvent event, String name, EntityDataSerializer<?> type)
	{
		event.register(NeoForgeRegistries.Keys.ENTITY_DATA_SERIALIZERS, RediscoveredMod.locate(name), () -> type);
	}
}
