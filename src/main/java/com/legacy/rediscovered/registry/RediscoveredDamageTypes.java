package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.level.LevelAccessor;

@RegistrarHolder
public class RediscoveredDamageTypes
{
	public static final RegistrarHandler<DamageType> HANDLER = RegistrarHandler.getOrCreate(Registries.DAMAGE_TYPE, RediscoveredMod.MODID);
	
	public static final Registrar.Pointer<DamageType> SPIKES = HANDLER.createPointer("spikes", () -> new DamageType(RediscoveredMod.MODID + "_spikes", 0.1F));
	
	public static DamageSource spikes(LevelAccessor level)
	{
		return source(level, SPIKES);
	}
	
	private static DamageSource source(LevelAccessor level, Registrar<DamageType> type)
	{
		return new DamageSource(type.getHolder(level).orElseThrow());
	}
}
