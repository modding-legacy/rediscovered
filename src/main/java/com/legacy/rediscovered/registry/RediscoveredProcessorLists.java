package com.legacy.rediscovered.registry;

import java.util.List;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.RediscoveredLootProv;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.templatesystem.AlwaysTrueTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.CappedProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.PosAlwaysTrueTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.ProcessorRule;
import net.minecraft.world.level.levelgen.structure.templatesystem.RandomBlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.rule.blockentity.AppendLoot;

@RegistrarHolder
public class RediscoveredProcessorLists
{
	public static final RegistrarHandler<StructureProcessorList> HANDLER = RegistrarHandler.getOrCreate(Registries.PROCESSOR_LIST, RediscoveredMod.MODID);

	/**
	 * Copy of ProcessorLists.TRAIL_RUINS_HOUSES_ARCHAEOLOGY with a modified loot
	 * table
	 */
	public static final Registrar.Pointer<StructureProcessorList> TRAIL_RUINS_PORTAL_ARCHAEOLOGY = HANDLER.createPointer("trail_ruins_portal_archaeology", bootstrap ->
	{
		//@formatter:off
		return new StructureProcessorList(List.of(
				new RuleProcessor(
						List.of(
								new ProcessorRule(new RandomBlockMatchTest(Blocks.GRAVEL, 0.2F), AlwaysTrueTest.INSTANCE, Blocks.DIRT.defaultBlockState()), 
								new ProcessorRule(new RandomBlockMatchTest(Blocks.GRAVEL, 0.1F), AlwaysTrueTest.INSTANCE, Blocks.COARSE_DIRT.defaultBlockState()), 
								new ProcessorRule(new RandomBlockMatchTest(Blocks.MUD_BRICKS, 0.1F), AlwaysTrueTest.INSTANCE, Blocks.PACKED_MUD.defaultBlockState()))), 
				trailsArchyLootProcessor(RediscoveredLootProv.TRAIL_RUINS_PORTAL_ARCHAEOLOGY_COMMON, 6), 
				trailsArchyLootProcessor(RediscoveredLootProv.TRAIL_RUINS_PORTAL_ARCHAEOLOGY_RARE, 3)));
		//@formatter:on
	});

	private static CappedProcessor trailsArchyLootProcessor(ResourceLocation lootTable, int limit)
	{
		return new CappedProcessor(new RuleProcessor(List.of(new ProcessorRule(new TagMatchTest(BlockTags.TRAIL_RUINS_REPLACEABLE), AlwaysTrueTest.INSTANCE, PosAlwaysTrueTest.INSTANCE, Blocks.SUSPICIOUS_GRAVEL.defaultBlockState(), new AppendLoot(lootTable)))), ConstantInt.of(limit));
	}
}
