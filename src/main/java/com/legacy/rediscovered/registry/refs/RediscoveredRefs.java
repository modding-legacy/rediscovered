package com.legacy.rediscovered.registry.refs;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class RediscoveredRefs
{
	public static interface ConfiguredFeatureRef
	{
	 	ResourceKey<ConfiguredFeature<?, ?>> CHERRY_TREE = key(Registries.CONFIGURED_FEATURE, "cherry_tree");
	}
	
	private static <T> ResourceKey<T> key(ResourceKey<? extends Registry<T>> registry, String id)
	{
		return ResourceKey.create(registry, RediscoveredMod.locate(id));
	}
}
