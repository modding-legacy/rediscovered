package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.util.UUIDHolder;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.mojang.serialization.Codec;

import net.neoforged.neoforge.attachment.AttachmentType;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@RegistrarHolder
public class RediscoveredAttachmentTypes
{
	public static final RegistrarHandler<AttachmentType<?>> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.ATTACHMENT_TYPES, RediscoveredMod.MODID);
	
	public static final Registrar.Static<AttachmentType<Long>> RAIN_TIME = HANDLER.createStatic("rain_time", () -> AttachmentType.builder(() -> 0L).serialize(Codec.LONG).build());
	public static final Registrar.Static<AttachmentType<UUIDHolder>> DRAGON_TELEPORT = HANDLER.createStatic("dragon_teleport", () -> AttachmentType.builder(() -> UUIDHolder.EMPTY).serialize(UUIDHolder.CODEC).build());
}
