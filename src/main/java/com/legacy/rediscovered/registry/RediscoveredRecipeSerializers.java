package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.item.crafting.SmithingDragonArmorRecipe;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Static;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.item.crafting.RecipeSerializer;

@RegistrarHolder
public class RediscoveredRecipeSerializers
{
	public static final RegistrarHandler<RecipeSerializer<?>> HANDLER = RegistrarHandler.getOrCreate(Registries.RECIPE_SERIALIZER, RediscoveredMod.MODID);

	public static final Static<RecipeSerializer<SmithingDragonArmorRecipe>> DRAGON_ARMOR = HANDLER.createStatic("smithing_dragon_armor", () -> new SmithingDragonArmorRecipe.Serializer());
}
