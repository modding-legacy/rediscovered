package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block_entities.GearBlockEntity;
import com.legacy.rediscovered.block_entities.MiniDragonPylonBlockEntity;
import com.legacy.rediscovered.block_entities.NetherReactorBlockEntity;
import com.legacy.rediscovered.block_entities.RedDragonEggBlockEntity;
import com.legacy.rediscovered.block_entities.TableBlockEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredBlockEntityTypes
{
	public static final BlockEntityType<GearBlockEntity> GEAR = BlockEntityType.Builder.of(GearBlockEntity::new, RediscoveredBlocks.gear).build(null);
	public static final BlockEntityType<TableBlockEntity> TABLE = BlockEntityType.Builder.of(TableBlockEntity::new, RediscoveredBlocks.oak_table, RediscoveredBlocks.birch_table, RediscoveredBlocks.spruce_table, RediscoveredBlocks.jungle_table, RediscoveredBlocks.acacia_table, RediscoveredBlocks.dark_oak_table, RediscoveredBlocks.cherry_table, RediscoveredBlocks.mangrove_table, RediscoveredBlocks.bamboo_table, RediscoveredBlocks.warped_table, RediscoveredBlocks.crimson_table).build(null);
	public static final BlockEntityType<NetherReactorBlockEntity> NETHER_REACTOR = BlockEntityType.Builder.of(NetherReactorBlockEntity::new, RediscoveredBlocks.nether_reactor_core).build(null);
	public static final BlockEntityType<MiniDragonPylonBlockEntity> MIN_DRAGON_PYLON = BlockEntityType.Builder.of(MiniDragonPylonBlockEntity::new, RediscoveredBlocks.mini_dragon_pylon).build(null);
	public static final BlockEntityType<RedDragonEggBlockEntity> RED_DRAGON_EGG = BlockEntityType.Builder.of(RedDragonEggBlockEntity::new, RediscoveredBlocks.red_dragon_egg).build(null);

	public static void init(RegisterEvent event)
	{
		register(event, "gear", GEAR);
		register(event, "table", TABLE);
		register(event, "nether_reactor_core", NETHER_REACTOR);
		register(event, "mini_dragon_pylon", MIN_DRAGON_PYLON);
		register(event, "red_dragon_egg", RED_DRAGON_EGG);
	}
	
	private static void register(RegisterEvent event, String name, BlockEntityType<?> type)
	{
		event.register(Registries.BLOCK_ENTITY_TYPE, RediscoveredMod.locate(name), () -> type);
	}
}
