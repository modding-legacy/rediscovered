package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.stats.StatFormatter;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredStats
{
	public static final ResourceLocation INTERACT_WITH_TABLE = RediscoveredMod.locate("interact_with_table");
	
	public static void init(RegisterEvent event)
	{
		register(event, INTERACT_WITH_TABLE, StatFormatter.DEFAULT);
	}
	
	private static void register(RegisterEvent event, ResourceLocation key, StatFormatter formatter)
	{
		event.register(Registries.CUSTOM_STAT, key, () -> key);
	}
}
