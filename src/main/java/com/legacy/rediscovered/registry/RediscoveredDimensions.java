package com.legacy.rediscovered.registry;

import java.util.List;
import java.util.function.Function;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.world.dimension.BetaSkyChunkGenerator;
import com.legacy.structure_gel.api.dimension.DimensionAccessHelper;
import com.legacy.structure_gel.api.dimension.DimensionTypeBuilder;
import com.legacy.structure_gel.api.registry.registrar.DimensionRegistrar;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.SurfaceRuleData;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.MultiNoiseBiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseSettings;

public class RediscoveredDimensions
{
	public static final ResourceLocation SKYLANDS_ID = RediscoveredMod.locate("skylands");
	private static final DimensionRegistrar SKYLANDS = create();

	public static void init()
	{
	}

	private static DimensionRegistrar create()
	{
		Function<BootstapContext<?>, DimensionType> dimType = (bootstrap) -> DimensionTypeBuilder.of().minY(0)/*.fixedTime(OptionalLong.of(6000L))*/.hasRaids(false).respawnAnchorWorks(true).effects(SKYLANDS_ID).build();
		Function<BootstapContext<?>, NoiseGeneratorSettings> noiseSettings = (bootstrap) ->
		{
			var densityFunctions = bootstrap.lookup(Registries.DENSITY_FUNCTION);
			var noise = bootstrap.lookup(Registries.NOISE);

			return DimensionAccessHelper.newDimensionSettings(NoiseSettings.create(0, 384, 1, 2), Blocks.STONE.defaultBlockState(), Blocks.AIR.defaultBlockState(), DimensionAccessHelper.netherNoiseRouter(densityFunctions, noise), SurfaceRuleData.overworldLike(false, false, false), 0, true);
		};
		Function<BootstapContext<?>, ChunkGenerator> chunkGen = (bootstrap) ->
		{
			var noiseGenSettings = bootstrap.lookup(Registries.NOISE_SETTINGS);
			var biomes = bootstrap.lookup(Registries.BIOME);

			return new BetaSkyChunkGenerator(buildBiomeSource(biomes), noiseGenSettings.getOrThrow(SKYLANDS.getNoiseSettings().getKey()));
		};
		return new DimensionRegistrar(SKYLANDS_ID, dimType, noiseSettings, chunkGen);
	}

	public static BiomeSource buildBiomeSource(HolderGetter<Biome> biomes)
	{
		// @formatter:off
        return MultiNoiseBiomeSource.createFromList(new Climate.ParameterList<>(List.of(
        		Pair.of(Climate.parameters(-0.3F, -0.3F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(RediscoveredBiomes.COLD_SKYLANDS.getKey())),
				Pair.of(Climate.parameters(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(RediscoveredBiomes.SKYLANDS.getKey())), 
				Pair.of(Climate.parameters(0.3F, 0.4F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(RediscoveredBiomes.WOODED_SKYLANDS.getKey())), 
				Pair.of(Climate.parameters(0.6F, 0.6F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(RediscoveredBiomes.RAINFOREST.getKey())))));
     // @formatter:on
	}

	public static ResourceKey<Level> skylandsKey()
	{
		return SKYLANDS.getLevelKey();
	}
}
