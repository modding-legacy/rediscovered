package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data_handler.TailorArmorStandDataHandler;
import com.legacy.structure_gel.api.data_handler.DataHandlerType;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

@RegistrarHolder
public class RediscoveredDataHandlers
{
	public static final RegistrarHandler<DataHandlerType<?>> HANDLER = RegistrarHandler.getOrCreate(StructureGelRegistries.Keys.DATA_HANDLER_TYPE, RediscoveredMod.MODID);
	
	public static final Registrar.Static<DataHandlerType<TailorArmorStandDataHandler>> TAILOR_ARMOR_STAND = HANDLER.createStatic("tailor_armor_stand", () -> new DataHandlerType<>(TailorArmorStandDataHandler::new, TailorArmorStandDataHandler::parser));
}
