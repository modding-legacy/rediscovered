package com.legacy.rediscovered.registry;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableList;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.registry.refs.RediscoveredRefs;
import com.legacy.rediscovered.world.feature.AbstractSkyTreeFeature;
import com.legacy.rediscovered.world.feature.AbstractSkyTreeFeature.TreeProperties;
import com.legacy.rediscovered.world.feature.AncientCherryTreeFeature;
import com.legacy.rediscovered.world.feature.IceAndSnowSkyFeature;
import com.legacy.rediscovered.world.feature.SkylandsPoolFeature;
import com.legacy.rediscovered.world.filter.NoVillageFilter;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.features.TreeFeatures;
import net.minecraft.data.worldgen.features.VegetationFeatures;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.WeightedPlacedFeature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.BlockPredicateFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.NoiseThresholdCountPlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraft.world.level.levelgen.placement.SurfaceWaterDepthFilter;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.neoforged.neoforge.registries.RegisterEvent;

@RegistrarHolder
public class RediscoveredFeatures
{
	public static final RegistrarHandler<ConfiguredFeature<?, ?>> CONFIGURED_HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, RediscoveredMod.MODID);
	public static final RegistrarHandler<PlacedFeature> PLACED_HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, RediscoveredMod.MODID);

	// public static final Feature<NoneFeatureConfiguration> SKY_SPAWN = new
	// SkySpawnFeature<>(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> SKY_SNOW = new IceAndSnowSkyFeature<>(NoneFeatureConfiguration.CODEC);

	public static final AncientCherryTreeFeature ANCIENT_CHERRY_TREE = new AncientCherryTreeFeature(TreeProperties.natural(() -> RediscoveredBlocks.ancient_cherry_sapling.defaultBlockState()).height(5, 8));
	public static final Feature<SkylandsPoolFeature.Config> POOL = new SkylandsPoolFeature(SkylandsPoolFeature.Config.CODEC);

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		// register("sky_spawn", SKY_SPAWN);
		register("sky_snow", SKY_SNOW);
		register("ancient_cherry_tree", ANCIENT_CHERRY_TREE);
		register("pool", POOL);
	}

	private static void register(String key, Feature<?> feature)
	{
		registerEvent.register(Registries.FEATURE, RediscoveredMod.locate(key), () -> feature);
	}

	public static class Configured
	{
		public static final Pointer<ConfiguredFeature<?, ?>> CHERRY_TREE = register(RediscoveredRefs.ConfiguredFeatureRef.CHERRY_TREE, RediscoveredFeatures.ANCIENT_CHERRY_TREE, () -> AbstractSkyTreeFeature.DUMMY_CONFIG);
		// @formatter:off
		public static final Pointer<ConfiguredFeature<?, ?>> WOODED_MIX = register("wooded_mix", (c) -> {
			HolderGetter<ConfiguredFeature<?, ?>> featureGetter = c.lookup(Registries.CONFIGURED_FEATURE);

			return new ConfiguredFeature<>(Feature.RANDOM_SELECTOR, new RandomFeatureConfiguration(List.of(
					new WeightedPlacedFeature(PlacementUtils.inlinePlaced(featureGetter.getOrThrow(TreeFeatures.OAK)), 0.1F),
					new WeightedPlacedFeature(PlacementUtils.inlinePlaced(featureGetter.getOrThrow(TreeFeatures.FANCY_OAK)), 0.3F)),
					PlacementUtils.inlinePlaced(featureGetter.getOrThrow(CHERRY_TREE.getKey()))));
		});
		
		public static final Pointer<ConfiguredFeature<?, ?>> RAINFOREST_MIX = register("rainforest_mix", (c) -> {
			HolderGetter<ConfiguredFeature<?, ?>> featureGetter = c.lookup(Registries.CONFIGURED_FEATURE);

			return new ConfiguredFeature<>(Feature.RANDOM_SELECTOR, new RandomFeatureConfiguration(List.of(
					new WeightedPlacedFeature(PlacementUtils.inlinePlaced(featureGetter.getOrThrow(TreeFeatures.OAK)), 0.3F)),
					PlacementUtils.inlinePlaced(featureGetter.getOrThrow(TreeFeatures.FANCY_OAK))));
		});
		// @formatter:on

		public static final Pointer<ConfiguredFeature<?, ?>> ICE_AND_SNOW_SKYLANDS = register("ice_and_snow_skylands", RediscoveredFeatures.SKY_SNOW, () -> FeatureConfiguration.NONE);
		/*public static final Pointer<ConfiguredFeature<?, ?>> SKY_SPAWN = register("sky_spawn", RediscoveredFeatures.SKY_SPAWN, () -> FeatureConfiguration.NONE);*/

		public static final Pointer<ConfiguredFeature<?, ?>> CLASSIC_FLOWERS = register("classic_flowers", Feature.FLOWER, () -> grassPatch(new WeightedStateProvider(SimpleWeightedRandomList.<BlockState>builder().add(RediscoveredBlocks.rose.defaultBlockState(), 4 * 10).add(Blocks.DANDELION.defaultBlockState(), 2 * 10).add(RediscoveredBlocks.paeonia.defaultBlockState(), 1 * 10).add(RediscoveredBlocks.cyan_rose.defaultBlockState(), 1)), 64));
		public static final Pointer<ConfiguredFeature<?, ?>> WOODED_PLANT_MIX = register("wooded_plant_mix", Feature.FLOWER, () -> grassPatch(new WeightedStateProvider(SimpleWeightedRandomList.<BlockState>builder().add(Blocks.TALL_GRASS.defaultBlockState(), 2).add(Blocks.PEONY.defaultBlockState(), 1)), 48));

		public static final Pointer<ConfiguredFeature<?, ?>> POOL = register("pool", RediscoveredFeatures.POOL, () -> new SkylandsPoolFeature.Config(Blocks.WATER.defaultBlockState(), UniformInt.of(4, 8), Optional.of(Blocks.GRASS_BLOCK.defaultBlockState()), 0.2F, Optional.of(Blocks.CLAY.defaultBlockState()), Optional.of(Blocks.CLAY.defaultBlockState()), 0.3F, RediscoveredTags.Blocks.SKYLANDS_POOL_REPLACEABLE));

		public static final Pointer<ConfiguredFeature<?, ?>> COAL_ORE = register("coal_ore", Feature.ORE, () -> new OreConfiguration(ores(Blocks.COAL_ORE.defaultBlockState(), Blocks.DEEPSLATE_COAL_ORE.defaultBlockState()), 20));
		public static final Pointer<ConfiguredFeature<?, ?>> IRON_ORE = register("iron_ore", Feature.ORE, () -> new OreConfiguration(ores(Blocks.IRON_ORE.defaultBlockState(), Blocks.DEEPSLATE_IRON_ORE.defaultBlockState()), 8));
		public static final Pointer<ConfiguredFeature<?, ?>> IRON_ORE_BURRIED = register("iron_ore_burried", Feature.ORE, () -> new OreConfiguration(ores(Blocks.IRON_ORE.defaultBlockState(), Blocks.DEEPSLATE_IRON_ORE.defaultBlockState()), 10, 0.75F));
		public static final Pointer<ConfiguredFeature<?, ?>> GOLD_ORE = register("gold_ore", Feature.ORE, () -> new OreConfiguration(ores(Blocks.GOLD_ORE.defaultBlockState(), Blocks.DEEPSLATE_GOLD_ORE.defaultBlockState()), 5));
		public static final Pointer<ConfiguredFeature<?, ?>> LAPIS_ORE = register("lapis_ore", Feature.ORE, () -> new OreConfiguration(ores(Blocks.LAPIS_ORE.defaultBlockState(), Blocks.DEEPSLATE_LAPIS_ORE.defaultBlockState()), 7));
		public static final Pointer<ConfiguredFeature<?, ?>> DIAMOND_ORE_BURRIED = register("diamond_ore_burried", Feature.ORE, () -> new OreConfiguration(ores(Blocks.DIAMOND_ORE.defaultBlockState(), Blocks.DEEPSLATE_DIAMOND_ORE.defaultBlockState()), 3, 0.75F));
		public static final Pointer<ConfiguredFeature<?, ?>> REDSTONE_ORE = register("redstone_ore", Feature.ORE, () -> new OreConfiguration(ores(Blocks.REDSTONE_ORE.defaultBlockState(), Blocks.DEEPSLATE_REDSTONE_ORE.defaultBlockState()), 6));
		public static final Pointer<ConfiguredFeature<?, ?>> RUBY_ORE = register("ruby_ore", Feature.ORE, () -> new OreConfiguration(ores(RediscoveredBlocks.ruby_ore.defaultBlockState(), RediscoveredBlocks.deepslate_ruby_ore.defaultBlockState()), 4));
		public static final Pointer<ConfiguredFeature<?, ?>> RUBY_ORE_SMALL = register("ruby_ore_small", Feature.ORE, () -> new OreConfiguration(ores(RediscoveredBlocks.ruby_ore.defaultBlockState(), RediscoveredBlocks.deepslate_ruby_ore.defaultBlockState()), 1));
		public static final Pointer<ConfiguredFeature<?, ?>> RUBY_ORE_BURRIED = register("ruby_ore_burried", Feature.ORE, () -> new OreConfiguration(ores(RediscoveredBlocks.ruby_ore.defaultBlockState(), RediscoveredBlocks.deepslate_ruby_ore.defaultBlockState()), 5, 0.7F));
		public static final Pointer<ConfiguredFeature<?, ?>> COPPER_ORE = register("copper_ore", Feature.ORE, () -> new OreConfiguration(ores(Blocks.COPPER_ORE.defaultBlockState(), Blocks.DEEPSLATE_COPPER_ORE.defaultBlockState()), 6));

		public static final Pointer<ConfiguredFeature<?, ?>> GRANITE_ORE = register("granite_ore", Feature.ORE, () -> new OreConfiguration(new TagMatchTest(BlockTags.BASE_STONE_OVERWORLD), Blocks.GRANITE.defaultBlockState(), 48));
		public static final Pointer<ConfiguredFeature<?, ?>> ANDESITE_ORE = register("andesite_ore", Feature.ORE, () -> new OreConfiguration(new TagMatchTest(BlockTags.BASE_STONE_OVERWORLD), Blocks.ANDESITE.defaultBlockState(), 64));
		public static final Pointer<ConfiguredFeature<?, ?>> CALCITE_ORE = register("calcite_ore", Feature.ORE, () -> new OreConfiguration(new TagMatchTest(BlockTags.BASE_STONE_OVERWORLD), Blocks.CALCITE.defaultBlockState(), 64));
		public static final Pointer<ConfiguredFeature<?, ?>> CLAY_ORE = register("clay_ore", Feature.ORE, () -> new OreConfiguration(new TagMatchTest(BlockTags.BASE_STONE_OVERWORLD), Blocks.CLAY.defaultBlockState(), 32));

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(ResourceKey<ConfiguredFeature<?, ?>> key, F feature, Supplier<FC> config)
		{
			return CONFIGURED_HANDLER.createPointer(key.location(), () -> new ConfiguredFeature<>(feature, config.get()));
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Supplier<FC> config)
		{
			return CONFIGURED_HANDLER.createPointer(key, () -> new ConfiguredFeature<>(feature, config.get()));
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, Function<BootstapContext<?>, ConfiguredFeature<?, ?>> fu)
		{
			return CONFIGURED_HANDLER.createPointer(key, fu);
		}

		private static RandomPatchConfiguration grassPatch(BlockStateProvider p_195203_, int pTries)
		{
			return FeatureUtils.simpleRandomPatchConfiguration(pTries, PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new SimpleBlockConfiguration(p_195203_)));
		}

		private static List<OreConfiguration.TargetBlockState> ores(BlockState stoneOre, BlockState deepslateOre)
		{
			return List.of(OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), stoneOre), OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), deepslateOre));
		}
	}

	public static class Placed
	{
		private static final VerticalAnchor BOTTOM = VerticalAnchor.absolute(0);

		// Same as emerald
		public static final Pointer<PlacedFeature> OVERWORLD_RUBY_ORE = register("overworld_ruby_ore", Configured.RUBY_ORE, () -> commonOrePlacement(100, HeightRangePlacement.triangle(VerticalAnchor.absolute(-16), VerticalAnchor.absolute(480))));

		public static final Pointer<PlacedFeature> COAL_ORE = register("coal_ore", Configured.COAL_ORE, () -> commonOrePlacement(22, HeightRangePlacement.triangle(BOTTOM, VerticalAnchor.absolute(186))));
		public static final Pointer<PlacedFeature> IRON_ORE = register("iron_ore", Configured.IRON_ORE, () -> commonOrePlacement(10, HeightRangePlacement.uniform(VerticalAnchor.absolute(11), VerticalAnchor.absolute(186))));
		public static final Pointer<PlacedFeature> IRON_ORE_BURRIED = register("iron_ore_burried", Configured.IRON_ORE_BURRIED, () -> commonOrePlacement(20, HeightRangePlacement.triangle(VerticalAnchor.absolute(11), VerticalAnchor.absolute(88))));
		public static final Pointer<PlacedFeature> GOLD_ORE = register("gold_ore", Configured.GOLD_ORE, () -> commonOrePlacement(2, HeightRangePlacement.triangle(VerticalAnchor.absolute(-57), VerticalAnchor.absolute(57))));
		public static final Pointer<PlacedFeature> LAPIS_ORE = register("lapis_ore", Configured.LAPIS_ORE, () -> commonOrePlacement(6, HeightRangePlacement.triangle(VerticalAnchor.absolute(43), VerticalAnchor.absolute(144))));
		public static final Pointer<PlacedFeature> DIAMOND_ORE_BURRIED = register("diamond_ore_burried", Configured.DIAMOND_ORE_BURRIED, () -> commonOrePlacement(2, HeightRangePlacement.triangle(VerticalAnchor.absolute(6), VerticalAnchor.absolute(50))));
		public static final Pointer<PlacedFeature> REDSTONE_ORE = register("redstone_ore", Configured.REDSTONE_ORE, () -> commonOrePlacement(6, HeightRangePlacement.uniform(VerticalAnchor.absolute(25), VerticalAnchor.absolute(100))));
		public static final Pointer<PlacedFeature> RUBY_ORE_SMALL = register("ruby_ore_small", Configured.RUBY_ORE_SMALL, () -> commonOrePlacement(10, HeightRangePlacement.uniform(BOTTOM, VerticalAnchor.absolute(200))));
		public static final Pointer<PlacedFeature> RUBY_ORE_BURRIED = register("ruby_ore_burried", Configured.RUBY_ORE_BURRIED, () -> commonOrePlacement(15, HeightRangePlacement.triangle(BOTTOM, VerticalAnchor.absolute(400))));
		public static final Pointer<PlacedFeature> COPPER_ORE = register("copper_ore", Configured.COPPER_ORE, () -> commonOrePlacement(12, HeightRangePlacement.uniform(VerticalAnchor.aboveBottom(46), VerticalAnchor.absolute(153))));

		public static final Pointer<PlacedFeature> GRANITE_ORE = register("granite_ore", Configured.GRANITE_ORE, () -> commonOrePlacement(2, HeightRangePlacement.uniform(BOTTOM, VerticalAnchor.absolute(200))));
		public static final Pointer<PlacedFeature> ANDESITE_ORE = register("andesite_ore", Configured.ANDESITE_ORE, () -> commonOrePlacement(4, HeightRangePlacement.uniform(BOTTOM, VerticalAnchor.absolute(200))));
		public static final Pointer<PlacedFeature> CALCITE_ORE = register("calcite_ore", Configured.CALCITE_ORE, () -> rareOrePlacement(3, HeightRangePlacement.uniform(BOTTOM, VerticalAnchor.absolute(40))));
		public static final Pointer<PlacedFeature> CLAY_ORE = register("clay_ore", Configured.CLAY_ORE, () -> commonOrePlacement(2, HeightRangePlacement.uniform(VerticalAnchor.absolute(55), VerticalAnchor.absolute(200))));

		public static final Pointer<PlacedFeature> CHERRY_TREE = register("cherry_tree", RediscoveredFeatures.Configured.CHERRY_TREE, () -> treePlacement(RarityFilter.onAverageOnceEvery(4), RediscoveredBlocks.ancient_cherry_sapling));
		public static final Pointer<PlacedFeature> COMMON_CHERRY_TREE = register("common_cherry_tree", RediscoveredFeatures.Configured.CHERRY_TREE, () -> treePlacement(CountPlacement.of(UniformInt.of(1, 3)), RediscoveredBlocks.ancient_cherry_sapling));
		public static final Pointer<PlacedFeature> WOODED_MIX = register("wooded_mix", RediscoveredFeatures.Configured.WOODED_MIX, () -> treePlacement(CountPlacement.of(3), RediscoveredBlocks.ancient_cherry_sapling));
		public static final Pointer<PlacedFeature> RAINFOREST_MIX = register("rainforest_mix", RediscoveredFeatures.Configured.RAINFOREST_MIX, () -> treePlacement(CountPlacement.of(UniformInt.of(3, 4)), Blocks.OAK_SAPLING));

		public static final Pointer<PlacedFeature> CLASSIC_FLOWERS = register("classic_flowers", Configured.CLASSIC_FLOWERS, () -> List.of(RarityFilter.onAverageOnceEvery(5), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> WOODED_PLANT_MIX = register("wooded_plant_mix", Configured.WOODED_PLANT_MIX, () -> List.of(CountPlacement.of(UniformInt.of(0, 2)), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> FLOWER_CHERRY_SPARSE = register("flower_cherry_sparse", VegetationFeatures.FLOWER_CHERRY, () -> List.of(NoiseThresholdCountPlacement.of(-0.4D, 1, 0), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> FLOWER_CHERRY_COMMON = register("flower_cherry_common", VegetationFeatures.FLOWER_CHERRY, () -> List.of(NoiseThresholdCountPlacement.of(-0.8D, 4, 1), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome()));
		
		public static final Pointer<PlacedFeature> POOL = register("pool", Configured.POOL, () -> List.of(RarityFilter.onAverageOnceEvery(2), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> ICE_AND_SNOW_SKYLANDS = register("ice_and_snow_skylands", Configured.ICE_AND_SNOW_SKYLANDS, () -> List.of(BiomeFilter.biome()));
		// public static final Pointer<PlacedFeature> SKY_SPAWN = register("sky_spawn",
		// Configured.SKY_SPAWN, () -> List.of(BiomeFilter.biome()));

		private static Pointer<PlacedFeature> register(String key, Pointer<ConfiguredFeature<?, ?>> feature, Supplier<List<PlacementModifier>> mods)
		{
			return PLACED_HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods.get())));
		}
		
		private static Pointer<PlacedFeature> register(String key, ResourceKey<ConfiguredFeature<?, ?>> feature, Supplier<List<PlacementModifier>> mods)
		{
			return PLACED_HANDLER.createPointer(key, (b) -> new PlacedFeature(b.lookup(Registries.CONFIGURED_FEATURE).getOrThrow(feature), List.copyOf(mods.get())));
		}

		/**
		 * Copied from VegetationPlacements
		 */
		private static ImmutableList.Builder<PlacementModifier> treePlacementBase(PlacementModifier pPlacement)
		{
			return ImmutableList.<PlacementModifier>builder().add(pPlacement).add(InSquarePlacement.spread()).add(SurfaceWaterDepthFilter.forMaxDepth(0)).add(PlacementUtils.HEIGHTMAP_OCEAN_FLOOR).add(BiomeFilter.biome()).add(NoVillageFilter.instance());
		}

		public static List<PlacementModifier> treePlacement(PlacementModifier pPlacement, Block pSaplingBlock)
		{
			return treePlacementBase(pPlacement).add(BlockPredicateFilter.forPredicate(BlockPredicate.wouldSurvive(pSaplingBlock.defaultBlockState(), BlockPos.ZERO))).build();
		}

		private static List<PlacementModifier> commonOrePlacement(int count, PlacementModifier heightRange)
		{
			return orePlacement(CountPlacement.of(count), heightRange);
		}

		private static List<PlacementModifier> rareOrePlacement(int onceEvery, PlacementModifier heightRange)
		{
			return orePlacement(RarityFilter.onAverageOnceEvery(onceEvery), heightRange);
		}

		private static List<PlacementModifier> orePlacement(PlacementModifier count, PlacementModifier heightRange)
		{
			return List.of(count, InSquarePlacement.spread(), heightRange, BiomeFilter.biome());
		}
	}
}