package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.particles.DragonBreathPourParticle;
import com.legacy.rediscovered.client.particles.LightningBoltData;
import com.legacy.rediscovered.client.particles.LightningBoltParticle;
import com.legacy.rediscovered.client.particles.NetherReactorGrowthParticle;
import com.legacy.rediscovered.client.particles.NetherReactorPulseData;
import com.legacy.rediscovered.client.particles.NetherReactorPulseParticle;
import com.legacy.rediscovered.client.particles.PylonShieldBlastData;
import com.legacy.rediscovered.client.particles.PylonShieldBlastParticle;
import com.legacy.rediscovered.client.particles.ShockwaveData;
import com.legacy.rediscovered.client.particles.ShockwaveParticle;
import com.legacy.rediscovered.client.particles.SkylandsPortalParticle;
import com.legacy.rediscovered.client.particles.WindData;
import com.legacy.rediscovered.client.particles.WindParticle;
import com.mojang.serialization.Codec;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.Registries;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredParticles
{
	public static final ParticleType<NetherReactorPulseData> NETHER_REACTOR_PULSE = customParticleType(true, NetherReactorPulseData.DESERIALIZER, NetherReactorPulseData.CODEC);
	public static final SimpleParticleType NETHER_REACTOR_GROWTH = new SimpleParticleType(false);
	public static final SimpleParticleType SKYLANDS_PORTAL = new SimpleParticleType(false);
	public static final ParticleType<PylonShieldBlastData> PYLON_SHIELD_BLAST = customParticleType(true, PylonShieldBlastData.DESERIALIZER, PylonShieldBlastData.CODEC);
	public static final ParticleType<ShockwaveData> SHOCKWAVE = customParticleType(true, ShockwaveData.DESERIALIZER, ShockwaveData.CODEC);
	public static final ParticleType<LightningBoltData> LIGHTNINGBOLT = customParticleType(false, LightningBoltData.DESERIALIZER, LightningBoltData.CODEC);
	public static final ParticleType<WindData> WIND = customParticleType(false, WindData.DESERIALIZER, WindData.CODEC);
	public static final SimpleParticleType DRAGON_BREATH_POUR = new SimpleParticleType(false);

	public static void init(RegisterEvent event)
	{
		register(event, "nether_reactor_pulse", NETHER_REACTOR_PULSE);
		register(event, "nether_reactor_growth", NETHER_REACTOR_GROWTH);
		register(event, "skylands_portal", SKYLANDS_PORTAL);
		register(event, "pylon_shield_blast", PYLON_SHIELD_BLAST);
		register(event, "shockwave", SHOCKWAVE);
		register(event, "lightningbolt", LIGHTNINGBOLT);
		register(event, "wind", WIND);
		register(event, "dragon_breath_pour", DRAGON_BREATH_POUR);
	}

	private static void register(RegisterEvent event, String key, ParticleType<?> particle)
	{
		event.register(Registries.PARTICLE_TYPE, RediscoveredMod.locate(key), () -> particle);
	}

	@SuppressWarnings("deprecation")
	private static <T extends ParticleOptions> ParticleType<T> customParticleType(boolean overrideLimiter, ParticleOptions.Deserializer<T> deserializer, Codec<T> codec)
	{
		return new ParticleType<T>(overrideLimiter, deserializer)
		{
			@Override
			public Codec<T> codec()
			{
				return codec;
			}
		};
	}
	
	@OnlyIn(Dist.CLIENT)
	public static class Factories
	{
		public static void init(net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.registerSpecial(NETHER_REACTOR_PULSE, new NetherReactorPulseParticle.Factory());
			event.registerSpriteSet(NETHER_REACTOR_GROWTH, NetherReactorGrowthParticle.Factory::new);
			event.registerSpriteSet(SKYLANDS_PORTAL, SkylandsPortalParticle.Factory::new);
			event.registerSpecial(PYLON_SHIELD_BLAST, new PylonShieldBlastParticle.Factory());
			event.registerSpecial(SHOCKWAVE, new ShockwaveParticle.Factory());
			event.registerSpecial(LIGHTNINGBOLT, new LightningBoltParticle.Factory());
			event.registerSpriteSet(WIND, WindParticle.Factory::new);
			event.registerSpriteSet(DRAGON_BREATH_POUR, DragonBreathPourParticle.Factory::new);
		}
	}
}
