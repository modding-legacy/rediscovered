package com.legacy.rediscovered.registry;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.BasicPoiBlock;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.registries.RegisterEvent;

@RegistrarHolder
public class RediscoveredPoiTypes
{
	public static final RegistrarHandler<PoiType> HANDLER = RegistrarHandler.getOrCreate(Registries.POINT_OF_INTEREST_TYPE, RediscoveredMod.MODID);

	public static final Registrar.Static<PoiType> CHAIR = create("chair", () -> builder().states(RediscoveredBlocks.oak_chair, RediscoveredBlocks.oak_chair, RediscoveredBlocks.birch_chair, RediscoveredBlocks.spruce_chair, RediscoveredBlocks.jungle_chair, RediscoveredBlocks.acacia_chair, RediscoveredBlocks.dark_oak_chair, RediscoveredBlocks.mangrove_chair, RediscoveredBlocks.bamboo_chair, RediscoveredBlocks.cherry_chair, RediscoveredBlocks.warped_chair, RediscoveredBlocks.crimson_chair).build());
	public static final Registrar.Static<PoiType> NETHER_REACTOR_CORE = create("nether_reactor_core", () -> builder().states(RediscoveredBlocks.nether_reactor_core).build());
	public static final Registrar.Static<PoiType> SKYLANDS_PORTAL = create("skylands_portal", () -> builder().states(RediscoveredBlocks.skylands_portal).build());
	public static final Registrar.Static<PoiType> GLOWING_OBSIDIAN = create("glowing_obsidian", () -> builder().states(RediscoveredBlocks.glowing_obsidian.defaultBlockState().setValue(BasicPoiBlock.IS_POI, true)).build());
	public static final Registrar.Static<PoiType> MINI_DRAGON_PYLON = create("mini_dragon_pylon", () -> builder().states(RediscoveredBlocks.mini_dragon_pylon).build());

	public static void init(RegisterEvent event)
	{
		HANDLER.registerValues(event);
	}

	private static Builder builder()
	{
		return new Builder();
	}

	@SuppressWarnings("unused")
	private static Builder village()
	{
		return builder().maxTickets(1).validDistance(1);
	}

	private static Registrar.Static<PoiType> create(String name, Supplier<PoiType> builder)
	{
		return HANDLER.createStatic(name, builder);
	}

	private static class Builder
	{
		Set<BlockState> states = new HashSet<>();
		int maxTickets = 0;
		int validDistance = 1;

		/**
		 * Used to determine how many entities are linked to the location. Villagers use
		 * this to ensure only one villager owns a workstation.
		 */
		Builder maxTickets(int maxTickets)
		{
			this.maxTickets = maxTickets;
			return this;
		}

		/**
		 * Used for entities during pathfinding. Villagers use it to get close to their
		 * workstation, or within 6 blocks of the bell. This number is how close "close
		 * enough" is for their pathfinding.
		 */
		Builder validDistance(int validDistance)
		{
			this.validDistance = validDistance;
			return this;
		}

		Builder states(BlockState... states)
		{
			for (BlockState state : states)
				this.states.add(state);
			return this;
		}

		Builder states(Block... blocks)
		{
			for (Block block : blocks)
				for (BlockState state : block.getStateDefinition().getPossibleStates())
					this.states.add(state);
			return this;
		}

		Builder states(Collection<Block> blocks, Predicate<BlockState> condition)
		{
			for (Block block : blocks)
				block.getStateDefinition().getPossibleStates().stream().filter(condition::test).forEach(this.states::add);
			return this;
		}

		@SuppressWarnings("unused")
		Builder states(Block block, Predicate<BlockState> condition)
		{
			return states(List.of(block), condition);
		}

		PoiType build()
		{
			var ret = new PoiType(states, maxTickets, validDistance);
			// System.out.println(ret.matchingStates());
			return ret;
		}
	}
}
