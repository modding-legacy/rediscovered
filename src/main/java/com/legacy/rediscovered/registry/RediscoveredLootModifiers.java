package com.legacy.rediscovered.registry;

import org.jetbrains.annotations.NotNull;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.neoforged.neoforge.common.loot.IGlobalLootModifier;
import net.neoforged.neoforge.common.loot.LootModifier;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@RegistrarHolder
public class RediscoveredLootModifiers
{
	public static final RegistrarHandler<Codec<? extends IGlobalLootModifier>> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, RediscoveredMod.MODID);

	public static final Registrar.Static<Codec<AddItemLootModifier>> ADD_SINGLE_ITEM = HANDLER.createStatic("add_single_item", () -> AddItemLootModifier.CODEC);

	public static class AddItemLootModifier extends LootModifier
	{
		private static final Codec<AddItemLootModifier> CODEC = RecordCodecBuilder.create(instance -> codecStart(instance).and(Codec.FLOAT.fieldOf("chance").forGetter(g -> g.chance)).and(ItemStack.CODEC.fieldOf("item").forGetter(g -> g.item)).apply(instance, AddItemLootModifier::new));

		private final float chance;
		private final ItemStack item;
		
		public AddItemLootModifier(LootItemCondition[] conditions, float chance, ItemStack item)
		{
			super(conditions);
			this.chance = chance;
			this.item = item;
		}

		@Override
		public Codec<? extends IGlobalLootModifier> codec()
		{
			return CODEC;
		}

		@Override
		protected @NotNull ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context)
		{
			if (context.getRandom().nextFloat() < this.chance)
				generatedLoot.add(this.item.copy());
			return generatedLoot;
		}

	}
}
