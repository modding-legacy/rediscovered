package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.triggers.ActivateNetherReactorTrigger;
import com.legacy.rediscovered.data.triggers.CurePigmanTrigger;
import com.legacy.rediscovered.data.triggers.DragonCatchTrigger;
import com.legacy.rediscovered.data.triggers.DragonTeleportTrigger;
import com.legacy.rediscovered.data.triggers.GiveRanaItemTrigger;
import com.legacy.rediscovered.data.triggers.InFarlandsTrigger;
import com.legacy.rediscovered.data.triggers.LocatePortalTrigger;
import com.legacy.rediscovered.data.triggers.RemoveQuiverTrigger;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Static;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.advancements.CriterionTrigger;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.core.registries.Registries;

@RegistrarHolder
public class RediscoveredTriggers
{
	public static final RegistrarHandler<CriterionTrigger<?>> HANDLER = RegistrarHandler.getOrCreate(Registries.TRIGGER_TYPE, RediscoveredMod.MODID);

	public static final Static<LocatePortalTrigger> LOCATE_PORTAL = HANDLER.createStatic("locate_portal", () -> new LocatePortalTrigger());
	public static final Static<RemoveQuiverTrigger> REMOVE_QUIVER = HANDLER.createStatic("remove_quiver", () -> new RemoveQuiverTrigger());
	public static final Static<ActivateNetherReactorTrigger> ACTIVATE_NETHER_REACTOR = HANDLER.createStatic("activate_nether_reactor", () -> new ActivateNetherReactorTrigger());
	public static final Static<InFarlandsTrigger> IN_FARLANDS_TRIGGER = HANDLER.createStatic("in_farlands", () -> new InFarlandsTrigger());
	public static final Static<DragonCatchTrigger> DRAGON_CATCH = HANDLER.createStatic("dragon_catch", () -> new DragonCatchTrigger());
	public static final Static<DragonTeleportTrigger> DRAGON_TELEPORT = HANDLER.createStatic("dragon_teleport", () -> new DragonTeleportTrigger());
	public static final Static<KilledTrigger> KILL_WITH_SPIKES = HANDLER.createStatic("kill_with_spikes", () -> new KilledTrigger());
	public static final Static<CurePigmanTrigger> CURE_PIGMAN = HANDLER.createStatic("cure_pigman", () -> new CurePigmanTrigger());
	public static final Static<GiveRanaItemTrigger> GIVE_RANA_ITEM = HANDLER.createStatic("give_rana_item", () -> new GiveRanaItemTrigger());
}
