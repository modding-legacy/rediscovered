package com.legacy.rediscovered.registry;

import java.util.List;
import java.util.Map;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.Util;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextColor;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.ArmorMaterials;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.armortrim.TrimMaterial;
import net.minecraft.world.item.armortrim.TrimPattern;
import net.minecraft.world.level.ItemLike;

@RegistrarHolder
public class RediscoveredArmorTrims
{
	public static final RegistrarHandler<TrimPattern> PATTERN_HANDLER = RegistrarHandler.getOrCreate(Registries.TRIM_PATTERN, RediscoveredMod.MODID).bootstrap(RediscoveredArmorTrims.Pattterns::trimBootstrap);
	public static final RegistrarHandler<TrimMaterial> MATERIAL_HANDLER = RegistrarHandler.getOrCreate(Registries.TRIM_MATERIAL, RediscoveredMod.MODID).bootstrap(RediscoveredArmorTrims.Materials::materialBootstrap);

	public static interface Pattterns
	{
		ResourceKey<TrimPattern> DRACONIC = PATTERN_HANDLER.key("draconic");

		/**
		 * Add to Item Tag: TRIM_TEMPLATES
		 * 
		 * Add recipes
		 * 
		 * @param context
		 */
		private static void trimBootstrap(BootstapContext<TrimPattern> context)
		{
			register(context, DRACONIC, RediscoveredItems.draconic_trim);
		}

		public static List<ResourceKey<TrimPattern>> patterns()
		{
			return List.of(DRACONIC);
		}

		private static void register(BootstapContext<TrimPattern> context, ResourceKey<TrimPattern> key, Item templateItem)
		{
			TrimPattern trimpattern = new TrimPattern(key.location(), BuiltInRegistries.ITEM.wrapAsHolder(templateItem), Component.translatable(Util.makeDescriptionId("trim_pattern", key.location())), false);
			context.register(key, trimpattern);
		}
	}
	
	public static interface Materials
	{
		ResourceKey<TrimMaterial> RUBY = MATERIAL_HANDLER.key("ruby");

		/**
		 * Also add to Item Tag: TRIM_MATERIALS
		 * 
		 * @param context
		 */
		private static void materialBootstrap(BootstapContext<TrimMaterial> context)
		{
			register(context, RUBY, RediscoveredItems.ruby, "#8A1F2D", 0.400173F);
		}

		public static List<ResourceKey<TrimMaterial>> materials()
		{
			return List.of(RUBY);
		}

		/**
		 * itemModelIndex should be a unique value. In most cases, armors won't work
		 * with the value you use, and instead will use the next lowest value.
		 * 
		 * @formatter:off
		 * 
		 * Vanilla values
		 * _________________
		 * Quartz:		0.1 White
		 * Iron:		0.2 Gray
		 * Netherite: 	0.3 Black
		 * Redstone:	0.4 Red
		 * Copper:	 	0.5 Copper
		 * Gold:		0.6 Yellow
		 * Emerald:		0.7 Green
		 * Diamond:		0.8 Blue
		 * Lapis:		0.9 Indigo ?
		 * Amethyst:	1.0 Violet
		 * 
		 * @formatter:on
		 * 
		 */
		private static void register(BootstapContext<TrimMaterial> context, ResourceKey<TrimMaterial> key, ItemLike ingredient, String hexColor, float itemModelIndex)
		{
			register(context, key, ingredient, hexColor, itemModelIndex, Map.of());
		}

		// overrideArmorMaterials only works with vanilla armors :/ See methods below
		private static void register(BootstapContext<TrimMaterial> context, ResourceKey<TrimMaterial> key, ItemLike ingredient, String hexColor, float itemModelIndex, Map<ArmorMaterials, String> overrideArmorMaterials)
		{
			TrimMaterial trimmaterial = TrimMaterial.create(assetID(key), ingredient.asItem(), itemModelIndex, Component.translatable(Util.makeDescriptionId("trim_material", key.location())).withStyle(Style.EMPTY.withColor(TextColor.parseColor(hexColor).getOrThrow(false, s -> RediscoveredMod.LOGGER.fatal(s + " is not a color for armor trim " + key)))), overrideArmorMaterials);
			context.register(key, trimmaterial);
		}

		/**
		 * @return An asset ID for textures, based on the mod ID:
		 *         {@code mod_id-material_id}
		 */
		public static String assetID(ResourceKey<TrimMaterial> key)
		{
			return key.location().getNamespace() + "-" + key.location().getPath();
		}

		public static String darkerID(ResourceKey<TrimMaterial> key)
		{
			return assetID(key) + "_darker";
		}
	}
}
