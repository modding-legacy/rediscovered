package com.legacy.rediscovered.registry;

import java.util.List;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.data.RediscoveredLangProv;
import com.legacy.rediscovered.event.RediscoveredMappingChanges;
import com.legacy.rediscovered.item.DragonArmorItem;
import com.legacy.rediscovered.item.PurpleArrowItem;
import com.legacy.rediscovered.item.QuiverItem;
import com.legacy.rediscovered.item.RediscoveredArmorItem;
import com.legacy.rediscovered.item.RubyFluteItem;
import com.legacy.rediscovered.item.ScarecrowItem;
import com.legacy.rediscovered.item.util.RediscoveredArmorMaterial;

import net.minecraft.ChatFormatting;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.food.Foods;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.MobBucketItem;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.RecordItem;
import net.minecraft.world.item.SmithingTemplateItem;
import net.minecraft.world.level.material.Fluids;
import net.neoforged.neoforge.common.DeferredSpawnEggItem;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredItems
{
	private static RegisterEvent registry;

	public static Item ruby, purple_arrow;

	public static Item pigman_spawn_egg, melee_pigman_spawn_egg, ranged_pigman_spawn_egg, zombie_pigman_spawn_egg,
			steve_spawn_egg, rana_spawn_egg, black_steve_spawn_egg, beast_boy_spawn_egg, fish_spawn_egg;

	public static Item studded_helmet, studded_chestplate, studded_leggings, studded_boots;

	public static Item plate_helmet, plate_chestplate, plate_leggings, plate_boots;

	public static Item quiver;

	public static Item raw_fish, cooked_fish, fish_bucket, scarecrow;

	public static Item music_disc_calm4;

	public static Item draconic_trim;
	
	public static Item ruby_flute;
	
	public static Item dragon_armor;
	
	public static Item dragon_armor_chain_smithing_template, dragon_armor_plating_smithing_template, dragon_armor_inlay_smithing_template;

	public static void init(RegisterEvent event)
	{
		registry = event;

		registerBlockItems();

		pigman_spawn_egg = register("pigman_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.PIGMAN, 0xf0a5a2, 0xa1a1a1, new Item.Properties()));
		melee_pigman_spawn_egg = register("melee_pigman_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.MELEE_PIGMAN, 0xf0a5a2, 0xa1a1a1, new Item.Properties()));
		ranged_pigman_spawn_egg = register("ranged_pigman_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.RANGED_PIGMAN, 0xf0a5a2, 0xa1a1a1, new Item.Properties()));
		zombie_pigman_spawn_egg = register("zombie_pigman_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.ZOMBIE_PIGMAN, 0xf0a5a2, 0xb63b3b, new Item.Properties()));
		steve_spawn_egg = register("steve_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.STEVE, 44975, 0xf6b201, new Item.Properties()));
		rana_spawn_egg = register("rana_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.RANA, 0x4c7129, 0xf0a5a2, new Item.Properties()));
		black_steve_spawn_egg = register("black_steve_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.BLACK_STEVE, 10489616, 894731, new Item.Properties()));
		beast_boy_spawn_egg = register("beast_boy_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.BEAST_BOY, 0x9932cc, 5349438, new Item.Properties()));
		fish_spawn_egg = register("fish_spawn_egg", new DeferredSpawnEggItem(() -> RediscoveredEntityTypes.FISH, 44975, 2243405, new Item.Properties()));

		ruby = register("ruby", new Item(new Item.Properties()));

		raw_fish = register("raw_fish", new Item(new Item.Properties().food(Foods.COD)));
		cooked_fish = register("cooked_fish", new Item(new Item.Properties().food(Foods.COOKED_COD)));
		fish_bucket = register("fish_bucket", new MobBucketItem(() -> RediscoveredEntityTypes.FISH, () -> Fluids.WATER, () -> SoundEvents.BUCKET_EMPTY_FISH, new Item.Properties().stacksTo(1)));

		studded_helmet = register("studded_helmet", new RediscoveredArmorItem.Dyeable(RediscoveredArmorMaterial.STUDDED, ArmorItem.Type.HELMET, new Item.Properties()));
		studded_chestplate = register("studded_chestplate", new RediscoveredArmorItem.Dyeable(RediscoveredArmorMaterial.STUDDED, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
		studded_leggings = register("studded_leggings", new RediscoveredArmorItem.Dyeable(RediscoveredArmorMaterial.STUDDED, ArmorItem.Type.LEGGINGS, new Item.Properties()));
		studded_boots = register("studded_boots", new RediscoveredArmorItem.Dyeable(RediscoveredArmorMaterial.STUDDED, ArmorItem.Type.BOOTS, new Item.Properties()));

		plate_helmet = register("plate_helmet", new RediscoveredArmorItem(RediscoveredArmorMaterial.PLATE, ArmorItem.Type.HELMET, new Item.Properties()));
		plate_chestplate = register("plate_chestplate", new RediscoveredArmorItem(RediscoveredArmorMaterial.PLATE, ArmorItem.Type.CHESTPLATE, new Item.Properties()));
		plate_leggings = register("plate_leggings", new RediscoveredArmorItem(RediscoveredArmorMaterial.PLATE, ArmorItem.Type.LEGGINGS, new Item.Properties()));
		plate_boots = register("plate_boots", new RediscoveredArmorItem(RediscoveredArmorMaterial.PLATE, ArmorItem.Type.BOOTS, new Item.Properties()));

		quiver = register("quiver", new QuiverItem(RediscoveredArmorMaterial.QUIVER, ArmorItem.Type.CHESTPLATE, new Item.Properties()));

		music_disc_calm4 = register("music_disc_calm4", new RecordItem(2, () -> RediscoveredSounds.RECORDS_MAGNETIC_CIRCUIT, new Item.Properties().stacksTo(1).rarity(Rarity.RARE), 3865));

		scarecrow = register("scarecrow", new ScarecrowItem(new Item.Properties().stacksTo(16)));

		purple_arrow = register("purple_arrow", new PurpleArrowItem(new Item.Properties()));

		draconic_trim = register("draconic_armor_trim_smithing_template", SmithingTemplateItem.createArmorTrimTemplate(RediscoveredArmorTrims.Pattterns.DRACONIC));
		
		ruby_flute = register("ruby_flute", new RubyFluteItem(new Item.Properties().stacksTo(1)));
		dragon_armor = register("dragon_armor", new DragonArmorItem(new Item.Properties().stacksTo(1)));
		dragon_armor_chain_smithing_template = register("dragon_armor_chain_smithing_template", createDragonArmorSmithingTemplate(RediscoveredLangProv.DRAGON_ARMOR_SMITHING_TEMPLATE_CHAIN, dragon_armor));
		dragon_armor_plating_smithing_template = register("dragon_armor_plating_smithing_template", createDragonArmorSmithingTemplate(RediscoveredLangProv.DRAGON_ARMOR_SMITHING_TEMPLATE_PLATING, dragon_armor));
		dragon_armor_inlay_smithing_template = register("dragon_armor_inlay_smithing_template", createDragonArmorSmithingTemplate(RediscoveredLangProv.DRAGON_ARMOR_SMITHING_TEMPLATE_INLAY, dragon_armor));
		
		RediscoveredMappingChanges.addItemAliases(event.getRegistry(Registries.ITEM));
	}

	private static void registerBlockItems()
	{
		RediscoveredBlocks.blockItems.stream().forEach((blockItem) -> register(BuiltInRegistries.BLOCK.getKey(blockItem.getBlock()).getPath(), blockItem));
		RediscoveredBlocks.blockItems = List.of();
	}

	private static <T extends Item> T register(String name, T item)
	{
		registry.register(Registries.ITEM, RediscoveredMod.locate(name), () -> item);
		return item;
	}

	private static Item createDragonArmorSmithingTemplate(String descriptionId, Item dragonArmor)
	{
		Component armorName = dragonArmor.getDescription();
		List<ResourceLocation> materials = SmithingTemplateItem.createTrimmableMaterialIconList();
		return new SmithingTemplateItem(Component.translatable(RediscoveredLangProv.DRAGON_ARMOR_SMITHING_TEMPLATE_APPLIES_TO).withStyle(ChatFormatting.BLUE), SmithingTemplateItem.ARMOR_TRIM_INGREDIENTS, Component.translatable(descriptionId).withStyle(ChatFormatting.GRAY), Component.translatable(RediscoveredLangProv.DRAGON_ARMOR_SMITHING_TEMPLATE_BASE_SLOT_DESCRIPTION, armorName), SmithingTemplateItem.ARMOR_TRIM_ADDITIONS_SLOT_DESCRIPTION, List.of(RediscoveredMod.locate("item/empty_slot_dragon_armor")), materials);
	}
}