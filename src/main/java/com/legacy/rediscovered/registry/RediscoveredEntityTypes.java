package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.entity.AbstractSteveEntity;
import com.legacy.rediscovered.entity.BeastBoyEntity;
import com.legacy.rediscovered.entity.BlackSteveEntity;
import com.legacy.rediscovered.entity.FishEntity;
import com.legacy.rediscovered.entity.PurpleArrowEntity;
import com.legacy.rediscovered.entity.RanaEntity;
import com.legacy.rediscovered.entity.ScarecrowEntity;
import com.legacy.rediscovered.entity.SteveEntity;
import com.legacy.rediscovered.entity.ZombiePigmanEntity;
import com.legacy.rediscovered.entity.dragon.BoltBallEntity;
import com.legacy.rediscovered.entity.dragon.DragonPylonEntity;
import com.legacy.rediscovered.entity.dragon.PylonBurstEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonBossEntity;
import com.legacy.rediscovered.entity.dragon.RedDragonOffspringEntity;
import com.legacy.rediscovered.entity.dragon.ThunderCloudEntity;
import com.legacy.rediscovered.entity.pigman.MeleePigmanEntity;
import com.legacy.rediscovered.entity.pigman.PigmanEntity;
import com.legacy.rediscovered.entity.pigman.RangedPigmanEntity;
import com.legacy.rediscovered.entity.util.MountableBlockEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.animal.AbstractFish;
import net.minecraft.world.entity.animal.WaterAnimal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent.Operation;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredEntityTypes
{
	public static final EntityType<FishEntity> FISH = buildEntity("fish", EntityType.Builder.of(FishEntity::new, MobCategory.WATER_AMBIENT).sized(0.5F, 0.3F));
	public static final EntityType<PigmanEntity> PIGMAN = buildEntity("pigman", EntityType.Builder.of(PigmanEntity::new, MobCategory.CREATURE).sized(0.6F, 1.9F));
	public static final EntityType<MeleePigmanEntity> MELEE_PIGMAN = buildEntity("melee_pigman", EntityType.Builder.of(MeleePigmanEntity::new, MobCategory.CREATURE).sized(0.6F, 1.9F));
	public static final EntityType<RangedPigmanEntity> RANGED_PIGMAN = buildEntity("ranged_pigman", EntityType.Builder.of(RangedPigmanEntity::new, MobCategory.CREATURE).sized(0.6F, 1.9F));
	public static final EntityType<ZombiePigmanEntity> ZOMBIE_PIGMAN = buildEntity("zombie_pigman", EntityType.Builder.of(ZombiePigmanEntity::new, MobCategory.MONSTER).sized(0.6F, 1.9F).fireImmune());

	public static final EntityType<SteveEntity> STEVE = buildEntity("steve", EntityType.Builder.of(SteveEntity::new, MobCategory.CREATURE).sized(0.6F, 1.95F));
	public static final EntityType<RanaEntity> RANA = buildEntity("rana", EntityType.Builder.of(RanaEntity::new, MobCategory.CREATURE).sized(0.6F, 1.95F));
	public static final EntityType<BlackSteveEntity> BLACK_STEVE = buildEntity("black_steve", EntityType.Builder.of(BlackSteveEntity::new, MobCategory.CREATURE).sized(0.6F, 1.95F));
	public static final EntityType<BeastBoyEntity> BEAST_BOY = buildEntity("beast_boy", EntityType.Builder.of(BeastBoyEntity::new, MobCategory.CREATURE).sized(0.6F, 1.95F));

	public static final EntityType<ScarecrowEntity> SCARECROW = buildEntity("scarecrow", EntityType.Builder.<ScarecrowEntity>of(ScarecrowEntity::new, MobCategory.MISC).sized(0.6F, 1.975F));
	public static final EntityType<PurpleArrowEntity> PURPLE_ARROW = buildEntity("purple_arrow", EntityType.Builder.<PurpleArrowEntity>of(PurpleArrowEntity::new, MobCategory.MISC).setShouldReceiveVelocityUpdates(true).sized(0.5F, 0.5F));
	public static final EntityType<MountableBlockEntity> MOUNTABLE_BLOCK = buildEntity("mountable_block", EntityType.Builder.<MountableBlockEntity>of(MountableBlockEntity::new, MobCategory.MISC).sized(1.0F, 1.0F));

	// new: 16.0F, 8.0F | old: 8.0F, 4.5F
	public static final EntityType<RedDragonBossEntity> RED_DRAGON_BOSS = buildEntity("red_dragon", EntityType.Builder.of(RedDragonBossEntity::new, MobCategory.MONSTER).fireImmune().sized(16.0F, 8.0F).clientTrackingRange(16));
	public static final EntityType<RedDragonOffspringEntity> RED_DRAGON_OFFSPRING = buildEntity("red_dragon_offspring", EntityType.Builder.of(RedDragonOffspringEntity::new, MobCategory.CREATURE).fireImmune().sized(4.0F, 3.5F).clientTrackingRange(10));
	public static final EntityType<BoltBallEntity> BOLT_BALL = buildEntity("bolt_ball", EntityType.Builder.<BoltBallEntity>of(BoltBallEntity::new, MobCategory.MISC).updateInterval(1).fireImmune().canSpawnFarFromPlayer().sized(2.0F, 2.0F).clientTrackingRange(10));
	public static final EntityType<ThunderCloudEntity> THUNDER_CLOUD = buildEntity("thunder_cloud", EntityType.Builder.<ThunderCloudEntity>of(ThunderCloudEntity::new, MobCategory.MISC).sized(3.0F, 1.8F).clientTrackingRange(10).fireImmune().updateInterval(Integer.MAX_VALUE));

	public static final EntityType<DragonPylonEntity> DRAGON_PYLON = buildEntity("dragon_pylon", EntityType.Builder.<DragonPylonEntity>of(DragonPylonEntity::new, MobCategory.MISC).clientTrackingRange(16).updateInterval(Integer.MAX_VALUE).sized(2.0F, 2.0F));
	public static final EntityType<PylonBurstEntity> PYLON_BURST = buildEntity("pylon_burst", EntityType.Builder.<PylonBurstEntity>of(PylonBurstEntity::new, MobCategory.MISC).updateInterval(3).fireImmune().canSpawnFarFromPlayer().sized(2.0F, 2.0F).clientTrackingRange(10));

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("fish", FISH);
		register("pigman", PIGMAN);
		register("melee_pigman", MELEE_PIGMAN);
		register("ranged_pigman", RANGED_PIGMAN);
		register("zombie_pigman", ZOMBIE_PIGMAN);

		register("steve", STEVE);
		register("rana", RANA);
		register("black_steve", BLACK_STEVE);
		register("beast_boy", BEAST_BOY);

		register("red_dragon", RED_DRAGON_BOSS);
		register("red_dragon_offspring", RED_DRAGON_OFFSPRING);
		register("bolt_ball", BOLT_BALL);
		register("thunder_cloud", THUNDER_CLOUD);

		register("dragon_pylon", DRAGON_PYLON);
		register("pylon_burst", PYLON_BURST);

		register("scarecrow", SCARECROW);
		register("purple_arrow", PURPLE_ARROW);
		register("mountable_block", MOUNTABLE_BLOCK);
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, RediscoveredMod.locate(name), () -> type);
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(RediscoveredMod.find(key));
	}

	private static void mobPlacement(SpawnPlacementRegisterEvent event, EntityType<? extends Mob> pType)
	{
		event.register(pType, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Mob::checkMobSpawnRules, Operation.REPLACE);
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(FISH, AbstractFish.createAttributes().build());
		event.put(PIGMAN, PigmanEntity.createAttributes().build());
		event.put(MELEE_PIGMAN, MeleePigmanEntity.createAttributes().build());
		event.put(RANGED_PIGMAN, RangedPigmanEntity.createAttributes().build());
		event.put(ZOMBIE_PIGMAN, ZombiePigmanEntity.createAttributes().build());

		event.put(STEVE, AbstractSteveEntity.createBaseSteveAttributes().build());
		event.put(RANA, AbstractSteveEntity.createBaseSteveAttributes().build());
		event.put(BLACK_STEVE, AbstractSteveEntity.createBaseSteveAttributes().build());
		event.put(BEAST_BOY, AbstractSteveEntity.createBaseSteveAttributes().build());

		event.put(RED_DRAGON_BOSS, RedDragonBossEntity.createAttributes().build());
		event.put(RED_DRAGON_OFFSPRING, RedDragonOffspringEntity.createAttributes().build());

		event.put(DRAGON_PYLON, DragonPylonEntity.createAttributes().build());

		event.put(SCARECROW, ScarecrowEntity.createAttributes().build());
	}

	public static void registerPlacements(SpawnPlacementRegisterEvent event)
	{
		mobPlacement(event, PIGMAN);
		mobPlacement(event, STEVE);
		mobPlacement(event, RANA);
		mobPlacement(event, BLACK_STEVE);
		mobPlacement(event, BEAST_BOY);
		mobPlacement(event, MELEE_PIGMAN);
		mobPlacement(event, RANGED_PIGMAN);

		event.register(RED_DRAGON_BOSS, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, (type, level, reason, pos, rand) -> false, Operation.REPLACE);
		event.register(RED_DRAGON_OFFSPRING, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, (type, level, reason, pos, rand) -> false, Operation.REPLACE);

		event.register(RediscoveredEntityTypes.FISH, SpawnPlacements.Type.IN_WATER, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, WaterAnimal::checkSurfaceWaterAnimalSpawnRules, Operation.REPLACE);
	}

	public static void registerPlacementOverrides(SpawnPlacementRegisterEvent event)
	{
		event.register(EntityType.GIANT, null, null, (type, level, reason, pos, rand) ->
		{
			if (reason == MobSpawnType.NATURAL || reason == MobSpawnType.CHUNK_GENERATION)
			{
				if (level.getLevel() != null && level.getLevel().dimension().equals(RediscoveredDimensions.skylandsKey()))
					return Monster.checkAnyLightMonsterSpawnRules(type, level, reason, pos, rand);
			}

			return false;
		}, Operation.OR);
	}
}
