package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.RangedAttribute;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent.RegisterHelper;

@RegistrarHolder
public class RediscoveredAttributes
{
	public static final RegistrarHandler<Attribute> HANDLER = RegistrarHandler.getOrCreate(Registries.ATTRIBUTE, RediscoveredMod.MODID).addListener(RediscoveredAttributes::register);

	// These have to be added to LivingEntityMixin.addAttributes
	// They're being as lazy values instead of using registrar stuff because entities ask for these before attributes get registered in some situations... Yaaaaaay
	public static final Lazy<Attribute> UNDEAD_DAMAGE_SCALING = Lazy.of(() -> new RangedAttribute("attribute.name.generic.rediscovered:undead_damage_resistance", 1.0, -1024, 1024).setSyncable(true));
	public static final Lazy<Attribute> CRIMSON_VEIL_DAMAGE_SCALING = Lazy.of(() -> new RangedAttribute("attribute.name.generic.rediscovered:cimson_veil_damage_resistance", 1.0, -1024, 1024).setSyncable(true));
	public static final Lazy<Attribute> EXPLOSION_RESISTANCE = Lazy.of(() -> new RangedAttribute("attribute.name.generic.rediscovered:explosion_resistance", 1.0, -1024, 1024).setSyncable(true));
	public static final Lazy<Attribute> FIRE_RESISTANCE = Lazy.of(() -> new RangedAttribute("attribute.name.generic.rediscovered:fire_resistance", 1.0, -1024, 1024).setSyncable(true));

	private static void register(RegisterHelper<Attribute> event)
	{
		register(event, "undead_damage_scaling", UNDEAD_DAMAGE_SCALING);
		register(event, "cimson_veil_damage_scaling", CRIMSON_VEIL_DAMAGE_SCALING);
		register(event, "explosion_resistance", EXPLOSION_RESISTANCE);
		register(event, "fire_resistance", FIRE_RESISTANCE);
	}

	private static void register(RegisterHelper<Attribute> event, String name, Lazy<Attribute> attrib)
	{
		event.register(RediscoveredMod.locate(name), attrib.get());
	}

	public static boolean isConversionImmune(LivingEntity entity)
	{
		return entity.getType().is(RediscoveredTags.Entities.CRIMSON_VEIL_AFFECTED) && hasActiveAttribute(entity, RediscoveredAttributes.CRIMSON_VEIL_DAMAGE_SCALING.get()) || entity.getType().is(RediscoveredTags.Entities.GOLDEN_AURA_APPLICABLE) && hasActiveAttribute(entity, RediscoveredAttributes.UNDEAD_DAMAGE_SCALING.get());
	}

	public static boolean hasActiveAttribute(LivingEntity entity, Attribute attribute)
	{
		var attr = entity.getAttribute(attribute);
		return attr != null && attr.getValue() != attr.getBaseValue();
	}
}
