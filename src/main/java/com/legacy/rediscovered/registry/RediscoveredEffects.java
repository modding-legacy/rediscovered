package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionBrewing;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredEffects
{
	//@formatter:off
	public static final Lazy<MobEffect> GOLDEN_AURA = Lazy.of(() -> new RediscoveredEffect(MobEffectCategory.BENEFICIAL, 0xffb24f)
			.addAttributeModifier(RediscoveredAttributes.UNDEAD_DAMAGE_SCALING.get(), "6e9f8db1-37d6-41fe-a478-6e5b1d818e3f", 0.25D, AttributeModifier.Operation.MULTIPLY_BASE));
	
	public static final Lazy<MobEffect> CRIMSON_VEIL = Lazy.of(() -> new RediscoveredEffect(MobEffectCategory.NEUTRAL, 0x990b0b)
			.addAttributeModifier(Attributes.ATTACK_DAMAGE, "6b0cd383-ae9b-48ef-ab43-3f6f272dddfa", 1.0D, AttributeModifier.Operation.ADDITION)
			.addAttributeModifier(RediscoveredAttributes.CRIMSON_VEIL_DAMAGE_SCALING.get(), "8044468e-5e6f-4b68-8a32-b01420caac02", -0.20D, AttributeModifier.Operation.MULTIPLY_BASE));
	//@formatter:on
	
	public static final Lazy<Potion> GOLDEN_AURA_POTION = Lazy.of(() -> new Potion("golden_aura", new MobEffectInstance(GOLDEN_AURA.get(), (5 * 60) * 20)));
	public static final Lazy<Potion> LONG_GOLDEN_AURA_POTION = Lazy.of(() -> new Potion("golden_aura", new MobEffectInstance(GOLDEN_AURA.get(), (10 * 60) * 20)));
	public static final Lazy<Potion> STRONG_GOLDEN_AURA_POTION = Lazy.of(() -> new Potion("golden_aura", new MobEffectInstance(GOLDEN_AURA.get(), (2 * 60) * 20, 1)));

	// Not craftable. Exists to show what the effect does in a tooltip
	public static final Lazy<Potion> CRIMSON_VEIL_POTION = Lazy.of(() -> new Potion("crimson_veil", new MobEffectInstance(CRIMSON_VEIL.get(), (5 * 60) * 20)));

	public static void init(RegisterEvent event)
	{
		event.register(Registries.MOB_EFFECT, RediscoveredMod.locate("golden_aura"), GOLDEN_AURA);
		event.register(Registries.MOB_EFFECT, RediscoveredMod.locate("crimson_veil"), CRIMSON_VEIL);
	}

	public static void initPotions(RegisterEvent event)
	{
		event.register(Registries.POTION, RediscoveredMod.locate("golden_aura"), GOLDEN_AURA_POTION);
		event.register(Registries.POTION, RediscoveredMod.locate("long_golden_aura"), LONG_GOLDEN_AURA_POTION);
		event.register(Registries.POTION, RediscoveredMod.locate("strong_golden_aura"), STRONG_GOLDEN_AURA_POTION);

		event.register(Registries.POTION, RediscoveredMod.locate("crimson_veil"), CRIMSON_VEIL_POTION);
	}

	public static void registerPotions()
	{
		PotionBrewing.addMix(GOLDEN_AURA_POTION.get(), Items.REDSTONE, LONG_GOLDEN_AURA_POTION.get());
		PotionBrewing.addMix(GOLDEN_AURA_POTION.get(), Items.GLOWSTONE_DUST, STRONG_GOLDEN_AURA_POTION.get());
	}

	// AttackDamageMobEffect has a protected constructor :/
	private static class RediscoveredEffect extends MobEffect
	{
		protected RediscoveredEffect(MobEffectCategory pCategory, int pColor)
		{
			super(pCategory, pColor);
		}
	}
}
