package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.item_predicates.AttachedQuiverItemPredicate;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.mojang.serialization.Codec;

import net.neoforged.neoforge.common.advancements.critereon.ICustomItemPredicate;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@RegistrarHolder
public class RediscoveredItemPredicates
{
	public static final RegistrarHandler<Codec<? extends ICustomItemPredicate>> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.ITEM_PREDICATE_SERIALIZERS, RediscoveredMod.MODID);
	
	public static final Registrar.Static<Codec<AttachedQuiverItemPredicate>> ATTACHED_QUIVER = HANDLER.createStatic("chestplate_with_quiver", () -> AttachedQuiverItemPredicate.CODEC);
}
