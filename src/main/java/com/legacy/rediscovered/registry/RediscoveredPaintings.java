package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.decoration.PaintingVariant;

@RegistrarHolder
public class RediscoveredPaintings
{
	public static final RegistrarHandler<PaintingVariant> HANDLER = RegistrarHandler.getOrCreate(Registries.PAINTING_VARIANT, RediscoveredMod.MODID);
	
	public static final Registrar.Static<PaintingVariant> FOG = HANDLER.createStatic("fog", () -> new PaintingVariant(48, 32));
}
