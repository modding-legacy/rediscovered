package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.gui.DragonInventoryMenu;
import com.legacy.rediscovered.client.gui.GuardPigmanInventoryMenu;
import com.legacy.rediscovered.client.gui.QuiverMenu;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.network.IContainerFactory;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredMenuType
{
	public static final Lazy<MenuType<QuiverMenu>> QUIVER = Lazy.of(() -> new MenuType<>(QuiverMenu::new, FeatureFlags.VANILLA_SET));
	public static final Lazy<MenuType<DragonInventoryMenu>> DRAGON_INVENTORY = Lazy.of(() -> makePacket((id, inv, buffer) -> new DragonInventoryMenu(id, inv, buffer)));
	public static final Lazy<MenuType<GuardPigmanInventoryMenu>> GUARD_PIGMAN_INVENTORY = Lazy.of(() -> makePacket((id, inv, buffer) -> new GuardPigmanInventoryMenu(id, inv, buffer)));

	private static <T extends AbstractContainerMenu> MenuType<T> makePacket(IContainerFactory<T> factory)
	{
		return new MenuType<T>(factory, FeatureFlags.VANILLA_SET);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void init(RegisterEvent event)
	{
		event.register(Registries.MENU, RediscoveredMod.locate("quiver"), (Lazy) QUIVER);
		event.register(Registries.MENU, RediscoveredMod.locate("dragon_inventory"), (Lazy) DRAGON_INVENTORY);
		event.register(Registries.MENU, RediscoveredMod.locate("guard_pigman_inventory"), (Lazy) GUARD_PIGMAN_INVENTORY);
	}
}
