package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.BaseFakeFireBlock;
import com.legacy.rediscovered.block.BasicPoiBlock;
import com.legacy.rediscovered.block.BrittleBlock;
import com.legacy.rediscovered.block.ChairBlock;
import com.legacy.rediscovered.block.DirtSlabBlock;
import com.legacy.rediscovered.block.DragonAltarBlock;
import com.legacy.rediscovered.block.GearBlock;
import com.legacy.rediscovered.block.GrassSlabBlock;
import com.legacy.rediscovered.block.MiniDragonPylonBlock;
import com.legacy.rediscovered.block.NetherReactorBlock;
import com.legacy.rediscovered.block.ObsidianBulbBlock;
import com.legacy.rediscovered.block.RedDragonEggBlock;
import com.legacy.rediscovered.block.RotationalConverterBlock;
import com.legacy.rediscovered.block.RubyEyeBlock;
import com.legacy.rediscovered.block.ShallowDirtSlabBlock;
import com.legacy.rediscovered.block.ShearedFlowerBlock;
import com.legacy.rediscovered.block.SkylandsPortalBlock;
import com.legacy.rediscovered.block.SpikeBlock;
import com.legacy.rediscovered.block.TableBlock;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Block;

@RegistrarHolder
public class RediscoveredBlockTypes
{
	public static final RegistrarHandler<MapCodec<? extends Block>> HANDLER = RegistrarHandler.getOrCreate(Registries.BLOCK_TYPE, RediscoveredMod.MODID);

	public static final Registrar.Static<MapCodec<? extends Block>> FAKE_FIRE = HANDLER.createStatic("fake_fire", () -> BaseFakeFireBlock.FakeFireBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> FAKE_SOUL_FIRE = HANDLER.createStatic("fake_soul_fire", () -> BaseFakeFireBlock.FakeSoulFireBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> BASIC_POI = HANDLER.createStatic("basic_poi", () -> BasicPoiBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> BRITTLE = HANDLER.createStatic("brittle", () -> BrittleBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> CHAIR = HANDLER.createStatic("chair", () -> ChairBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> DIRT_SLAB = HANDLER.createStatic("dirt_slab", () -> DirtSlabBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> DRAGON_ALTAR = HANDLER.createStatic("dragon_altar", () -> DragonAltarBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> GEAR = HANDLER.createStatic("gear", () -> GearBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> GRASS_SLAB = HANDLER.createStatic("grass_slab", () -> GrassSlabBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> MINI_DRAGON_PYLON = HANDLER.createStatic("mini_dragon_pylon", () -> MiniDragonPylonBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> NETHER_REACTOR = HANDLER.createStatic("nether_reactor", () -> NetherReactorBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> OBSIDIAN_BULB = HANDLER.createStatic("obsidian_bulb", () -> ObsidianBulbBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> RED_DRAGON_EGG = HANDLER.createStatic("red_dragon_egg", () -> RedDragonEggBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> ROTATIONAL_CONVERTER = HANDLER.createStatic("rotational_converter", () -> RotationalConverterBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> RUBY_EYE = HANDLER.createStatic("ruby_eye", () -> RubyEyeBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> SHALLOW_DIRT_SLAB = HANDLER.createStatic("shallow_dirt_slab", () -> ShallowDirtSlabBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> SHEARED_FLOWER = HANDLER.createStatic("sheared_flower", () -> ShearedFlowerBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> SKYLANDS_PORTAL = HANDLER.createStatic("skylands_portal", () -> SkylandsPortalBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> SPIKE = HANDLER.createStatic("spike", () -> SpikeBlock.CODEC);
	public static final Registrar.Static<MapCodec<? extends Block>> TABLE = HANDLER.createStatic("table", () -> TableBlock.CODEC);	
}
