package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.world.filter.NoVillageFilter;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;
import net.neoforged.neoforge.registries.RegisterEvent;

public interface RediscoveredPlacementModifiers
{
	PlacementModifierType<NoVillageFilter> NO_VILLAGE_FILTER = () -> NoVillageFilter.CODEC;

	public static void init(RegisterEvent event)
	{
		event.register(Registries.PLACEMENT_MODIFIER_TYPE, RediscoveredMod.locate("no_village"), () -> NO_VILLAGE_FILTER);
	}
}
