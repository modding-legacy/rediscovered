package com.legacy.rediscovered.registry;

import java.util.function.Function;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.placement.CavePlacements;
import net.minecraft.data.worldgen.placement.OrePlacements;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;

@RegistrarHolder
public class RediscoveredBiomes
{
	public static final RegistrarHandler<Biome> HANDLER = RegistrarHandler.getOrCreate(Registries.BIOME, RediscoveredMod.MODID);

	public static final Registrar.Pointer<Biome> SKYLANDS = create("skylands", Builders::createSkylandsBiome);
	public static final Registrar.Pointer<Biome> WOODED_SKYLANDS = create("wooded_skylands", Builders::createWoodedSkylandsBiome);
	public static final Registrar.Pointer<Biome> COLD_SKYLANDS = create("cold_skylands", Builders::createColdSkylandsBiome);
	public static final Registrar.Pointer<Biome> RAINFOREST = create("rainforest", Builders::createRainforestBiome);

	private static Registrar.Pointer<Biome> create(String name, Function<BootstapContext<?>, Biome> biome)
	{
		return HANDLER.createPointer(name, biome);
	}

	public static class Builders
	{
		public static Biome createSkylandsBiome(BootstapContext<?> bootstrap)
		{
			BiomeGenerationSettings.Builder builder = new BiomeGenerationSettings.Builder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.CHERRY_TREE.getKey());
			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.FLOWER_CHERRY_SPARSE.getKey());

			return createBaseSkylandsBiome(bootstrap, builder, spawns, 0.5F, false);
		}

		public static Biome createWoodedSkylandsBiome(BootstapContext<?> bootstrap)
		{
			BiomeGenerationSettings.Builder builder = new BiomeGenerationSettings.Builder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.WOODED_MIX.getKey());
			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.WOODED_PLANT_MIX.getKey());
			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.FLOWER_CHERRY_COMMON.getKey());

			return createBaseSkylandsBiome(bootstrap, builder, spawns, 0.5F, false);
		}

		public static Biome createColdSkylandsBiome(BootstapContext<?> bootstrap)
		{
			BiomeGenerationSettings.Builder builder = new BiomeGenerationSettings.Builder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();
			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.SHEEP, 80, 3, 4));

			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.COMMON_CHERRY_TREE.getKey());

			return createBaseSkylandsBiome(bootstrap, builder, spawns, 0.0F, false);
		}

		public static Biome createRainforestBiome(BootstapContext<?> bootstrap)
		{
			BiomeGenerationSettings.Builder builder = new BiomeGenerationSettings.Builder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.RAINFOREST_MIX.getKey());
			builder.addFeature(GenerationStep.Decoration.LAKES, RediscoveredFeatures.Placed.POOL.getKey());

			return createBaseSkylandsBiome(bootstrap, builder, spawns, 0.95F, true);
		}

		public static Biome createBaseSkylandsBiome(BootstapContext<?> bootstrap, BiomeGenerationSettings.Builder builder, MobSpawnSettings.Builder spawns, float temp, boolean humid)
		{

			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.CHICKEN, 150, 2, 4));

			spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.GIANT, 1, 1, 1));
			spawns.addMobCharge(EntityType.GIANT, 6.5F, 1F);

			spawns.addSpawn(MobCategory.WATER_CREATURE, new MobSpawnSettings.SpawnerData(RediscoveredEntityTypes.FISH, 1, 2, 2));

			builder.addFeature(GenerationStep.Decoration.TOP_LAYER_MODIFICATION, RediscoveredFeatures.Placed.ICE_AND_SNOW_SKYLANDS.getKey());
			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, RediscoveredFeatures.Placed.CLASSIC_FLOWERS.getKey());

			if (humid)
				builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, VegetationPlacements.PATCH_GRASS_JUNGLE);
			else
				builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, VegetationPlacements.PATCH_GRASS_NORMAL);

			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_STRUCTURES, CavePlacements.MONSTER_ROOM);
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, OrePlacements.ORE_DIRT);
			addOres(bootstrap, builder);

			// bec4ee previous version's color
			return (new Biome.BiomeBuilder()).hasPrecipitation(true).temperature(temp).downfall(humid ? 0.9F : 0.4F).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(4159204).waterFogColor(329011).fogColor(0xa0a9ef).skyColor(0xb3bbff).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
		}

		private static void addOres(BootstapContext<?> bootstrap, BiomeGenerationSettings.Builder builder)
		{
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.COAL_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.IRON_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.IRON_ORE_BURRIED.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.GOLD_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.LAPIS_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.DIAMOND_ORE_BURRIED.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.REDSTONE_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.RUBY_ORE_SMALL.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.RUBY_ORE_BURRIED.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.COPPER_ORE.getKey());

			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.GRANITE_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.ANDESITE_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.CALCITE_ORE.getKey());
			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, RediscoveredFeatures.Placed.CLAY_ORE.getKey());
		}
	}
}
