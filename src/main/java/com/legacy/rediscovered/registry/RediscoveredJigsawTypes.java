package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.world.structure.BrickPyramidStructure;
import com.legacy.rediscovered.world.structure.IndevHouseStructure;
import com.legacy.rediscovered.world.structure.PigmanVillageStructure;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;

@RegistrarHolder
public class RediscoveredJigsawTypes
{
	public static final RegistrarHandler<JigsawCapabilityType<?>> HANDLER = RegistrarHandler.getOrCreate(StructureGelRegistries.Keys.JIGSAW_TYPE, RediscoveredMod.MODID);
	
	public static final Registrar.Static<JigsawCapabilityType<PigmanVillageStructure.Capability>> PIGMAN_VILLAGE = HANDLER.createStatic("pigman_village", () -> () -> PigmanVillageStructure.Capability.CODEC);
	public static final Registrar.Static<JigsawCapabilityType<BrickPyramidStructure.Capability>> BRICK_PYRAMID = HANDLER.createStatic("brick_pyramid", () -> () -> BrickPyramidStructure.Capability.CODEC);
	public static final Registrar.Static<JigsawCapabilityType<IndevHouseStructure.Capability>> INDEV_HOUSE = HANDLER.createStatic("indev_house", () -> () -> IndevHouseStructure.Capability.CODEC);
}
