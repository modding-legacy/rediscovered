package com.legacy.rediscovered.registry;

import java.util.List;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.world.structure.BrickPyramidPools;
import com.legacy.rediscovered.world.structure.BrickPyramidStructure;
import com.legacy.rediscovered.world.structure.IndevHousePools;
import com.legacy.rediscovered.world.structure.IndevHouseStructure;
import com.legacy.rediscovered.world.structure.ObsidianWallPieces;
import com.legacy.rediscovered.world.structure.ObsidianWallStructure;
import com.legacy.rediscovered.world.structure.PigmanVillagePools;
import com.legacy.rediscovered.world.structure.PigmanVillageStructure;
import com.legacy.rediscovered.world.structure.SkylandsPortalPools;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.ExtendedJigsawStructure;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;
import com.legacy.structure_gel.api.structure.GridStructurePlacement.TaggedExclusionZone;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride.BoundingBoxType;
import net.minecraft.world.level.levelgen.structure.TerrainAdjustment;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class RediscoveredStructures
{
	public static final RegistrarHandler<StructureTemplatePool> HANDLER = RegistrarHandler.getOrCreate(Registries.TEMPLATE_POOL, RediscoveredMod.MODID).bootstrap(RediscoveredStructures::bootstrap);

	// @formatter:off
	public static final StructureRegistrar<ExtendedJigsawStructure> PIGMAN_VILLAGE = StructureRegistrar.jigsawBuilder(RediscoveredMod.locate("pigman_village"))
			.addPiece(() -> PigmanVillageStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(PigmanVillagePools.ROOT)).startHeight(85, 110).maxDepth(12).maxDistanceFromCenter(112).capability(PigmanVillageStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.NONE)
				.spawns(MobCategory.MONSTER, BoundingBoxType.STRUCTURE, List::of)
				.biomes(RediscoveredTags.Biomes.HAS_PIGMAN_VILLAGE)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(45, 31, 0.75F).allowedNearSpawn(true).exclusionZone(new TaggedExclusionZone(c.lookup(Registries.STRUCTURE).getOrThrow(RediscoveredTags.Structures.PIGMAN_VILLAGE_AVOIDS), 10)).build(RediscoveredStructures.PIGMAN_VILLAGE.getRegistryName()))
			.build();
	// @formatter:on

	// @formatter:off
	public static final StructureRegistrar<ExtendedJigsawStructure> BRICK_PYRAMID = StructureRegistrar.jigsawBuilder(RediscoveredMod.locate("brick_pyramid"))
			.addPiece(() -> BrickPyramidStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(BrickPyramidPools.ROOT)).startHeight(90, 115).maxDepth(12).maxDistanceFromCenter(112).capability(BrickPyramidStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.NONE)
				.spawns(MobCategory.MONSTER, BoundingBoxType.STRUCTURE, List::of)
				.biomes(RediscoveredTags.Biomes.HAS_BRICK_PYRAMID)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(45, 31, 1.0F).build(RediscoveredStructures.BRICK_PYRAMID.getRegistryName()))
			.build();
	// @formatter:on

	// @formatter:off
	public static final StructureRegistrar<ObsidianWallStructure> OBSIDIAN_WALL = StructureRegistrar.builder(RediscoveredMod.locate("obsidian_wall"), () -> () -> ObsidianWallStructure.CODEC)
			.addPiece(() -> ObsidianWallPieces.Piece::new)
			.pushStructure(ObsidianWallStructure::new)
				.terrainAdjustment(TerrainAdjustment.NONE)
				.biomes(RediscoveredTags.Biomes.HAS_OBSIDIAN_WALL)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(16, 1.0F).allowedNearSpawn(true).exclusionZone(new TaggedExclusionZone(c.lookup(Registries.STRUCTURE).getOrThrow(RediscoveredTags.Structures.OBSIDIAN_WALL_AVOIDS), 3)).build(RediscoveredStructures.OBSIDIAN_WALL.getRegistryName()))
			.build();
	// @formatter:on

	// @formatter:off
	public static final StructureRegistrar<ExtendedJigsawStructure> INDEV_HOUSE = StructureRegistrar.jigsawBuilder(RediscoveredMod.locate("indev_house"))
			.addPiece(() -> IndevHouseStructure.Piece::new)
			.pushStructure(IndevHouseStructure.Type.DEFAULT.getSerializedName(), (c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(IndevHousePools.DEFAULT)).startHeight(-2).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(new IndevHouseStructure.Capability(IndevHouseStructure.Type.DEFAULT)).build())
				.terrainAdjustment(TerrainAdjustment.NONE)
				.biomes(RediscoveredTags.Biomes.HAS_DEFAULT_INDEV_HOUSE)
			.popStructure()
			.pushStructure(IndevHouseStructure.Type.MOSSY.getSerializedName(), (c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(IndevHousePools.MOSSY)).startHeight(-2).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(new IndevHouseStructure.Capability(IndevHouseStructure.Type.MOSSY)).build())
				.terrainAdjustment(TerrainAdjustment.NONE)
				.biomes(RediscoveredTags.Biomes.HAS_MOSSY_INDEV_HOUSE)
			.popStructure()
			.pushStructure(IndevHouseStructure.Type.CHERRY.getSerializedName(), (c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(IndevHousePools.CHERRY)).startHeight(-2).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(new IndevHouseStructure.Capability(IndevHouseStructure.Type.CHERRY)).build())
				.terrainAdjustment(TerrainAdjustment.NONE)
				.biomes(RediscoveredTags.Biomes.HAS_CHERRY_INDEV_HOUSE)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(12, 1.0F).allowedNearSpawn(true).build(RediscoveredStructures.INDEV_HOUSE.getRegistryName()))
			.build();
	// @formatter:on

	private static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		PigmanVillagePools.bootstrap(bootstrap);
		BrickPyramidPools.bootstrap(bootstrap);
		SkylandsPortalPools.bootstrap(bootstrap);
		IndevHousePools.bootstrap(bootstrap);
	}

	public static void init()
	{
	}
	/*public static final StructureFeature<SmallPigmanVillageConfig> SMALL_PIGMAN_VILLAGE = new SmallPigmanVillageStructure(SmallPigmanVillageConfig.codec);
	
	public static void init(Register<StructureFeature<?>> event)
	{
		register(event.getRegistry(), "small_pigman_village", SMALL_PIGMAN_VILLAGE);
	}
	
	private static void register(IForgeRegistry<StructureFeature<?>> registry, String key, StructureFeature<?> structure)
	{
		RediscoveredRegistry.register(registry, key, structure);
		StructureFeature.STRUCTURES_REGISTRY.put(RediscoveredMod.find(key), structure);
		StructureFeature.STEP.put(SMALL_PIGMAN_VILLAGE, GenerationStep.Decoration.SURFACE_STRUCTURES);
	}*/
}
