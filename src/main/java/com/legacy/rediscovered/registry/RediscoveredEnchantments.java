package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.enchantment.RapidShotEnchantment;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantment.Rarity;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredEnchantments
{
	public static final Enchantment RAPID_SHOT = new RapidShotEnchantment(Rarity.RARE, EquipmentSlot.MAINHAND);

	public static void init(RegisterEvent event)
	{
		register(event, "rapid_shot", RAPID_SHOT);
	}
	
	private static void register(RegisterEvent event, String name, Enchantment ench)
	{
		event.register(Registries.ENCHANTMENT, RediscoveredMod.locate(name), () -> ench);
	}
}