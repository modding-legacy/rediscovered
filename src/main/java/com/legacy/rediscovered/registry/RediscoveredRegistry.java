package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.world.dimension.BetaSkyChunkGenerator;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod.EventBusSubscriber;
import net.neoforged.fml.common.Mod.EventBusSubscriber.Bus;
import net.neoforged.neoforge.registries.NeoForgeRegistries;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = RediscoveredMod.MODID, bus = Bus.MOD)
public class RediscoveredRegistry
{
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		ResourceKey<?> registry = event.getRegistryKey();

		if (registry.equals(Registries.ENTITY_TYPE))
			RediscoveredEntityTypes.init(event);
		else if (registry.equals(Registries.ITEM))
			RediscoveredItems.init(event);
		else if (registry.equals(Registries.BLOCK))
			RediscoveredBlocks.init(event);
		else if (registry.equals(Registries.SOUND_EVENT))
			RediscoveredSounds.init();
		else if (registry.equals(Registries.BLOCK_ENTITY_TYPE))
			RediscoveredBlockEntityTypes.init(event);
		/*else if (registry.equals(Registries.SCHEDULE))
			RediscoveredSchedules.init(event);*/
		else if (event.getRegistryKey().equals(Registries.POINT_OF_INTEREST_TYPE))
			RediscoveredPoiTypes.init(event);
		/*else if (event.getRegistryKey().equals(Registries.BIOME)) //
			RediscoveredBiomes.init();*/
		else if (registry.equals(Registries.FEATURE)) //
		{
			RediscoveredFeatures.init(event);

			// RediscoveredConfiguredCarvers.init();
		}
		/*else if (registry.equals(Registries.CARVER))
			RediscoveredFeatures.Carvers.init(event);
		else if (registry.equals(Registries.PARTICLE_TYPE))
			com.legacy.rediscovered.registries.RediscoveredParticles.init(event);
		else if (registry.equals(Registries.PROCESSOR_LIST))
			RediscoveredStructures.ProcessorLists.init();
		else if (registry.equals(Registries.STRUCTURE_PROCESSOR))
			RediscoveredStructureProcessors.init();*/
		else if (registry.equals(Registries.CHUNK_GENERATOR))
			event.register(Registries.CHUNK_GENERATOR, RediscoveredMod.locate("classic_skylands_chunk_generator"), () -> BetaSkyChunkGenerator.SKYLANDS_CODEC);
		/*else if (registry.equals(Registries.BIOME_SOURCE))
		{
			event.register(Registries.BIOME_SOURCE, Rediscovered.locate("skylands"), () -> RediscoveredBiomeSource.CODEC);
		}*/
		else if (event.getRegistryKey().equals(Registries.CUSTOM_STAT))
			RediscoveredStats.init(event);
		else if (event.getRegistryKey().equals(Registries.ENCHANTMENT))
			RediscoveredEnchantments.init(event);
		else if (registry.equals(Registries.MENU))
			RediscoveredMenuType.init(event);
		else if (registry.equals(NeoForgeRegistries.Keys.ENTITY_DATA_SERIALIZERS))
			RediscoveredDataSerialization.init(event);
		else if (registry.equals(Registries.PARTICLE_TYPE))
			RediscoveredParticles.init(event);
		else if (registry.equals(Registries.PLACEMENT_MODIFIER_TYPE))
			RediscoveredPlacementModifiers.init(event);
		else if (registry.equals(Registries.MOB_EFFECT))
			RediscoveredEffects.init(event);
		else if (registry.equals(Registries.POTION))
			RediscoveredEffects.initPotions(event);
	}
}
