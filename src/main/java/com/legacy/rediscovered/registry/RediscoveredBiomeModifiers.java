package com.legacy.rediscovered.registry;

import java.util.List;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.biome.MobSpawnSettings.SpawnerData;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@SuppressWarnings("unused")
@RegistrarHolder
public class RediscoveredBiomeModifiers
{
	public static final RegistrarHandler<BiomeModifier> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.BIOME_MODIFIERS, RediscoveredMod.MODID);

	public static final Pointer<BiomeModifier> ADD_RUBY_ORE = HANDLER.createPointer("add_ruby_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, RediscoveredTags.Biomes.HAS_RUBY_ORE, RediscoveredFeatures.Placed.OVERWORLD_RUBY_ORE.getKey()));

	public static final Pointer<BiomeModifier> ADD_SPAWNS = HANDLER.createPointer("add_rediscovered_natural_spawns", c ->
	{
		var fish = new MobSpawnSettings.SpawnerData(RediscoveredEntityTypes.FISH, 7, 3, 4);

		return addSpawn(c, RediscoveredTags.Biomes.HAS_FISH_SPAWNS, fish);
	});

	private static BiomeModifier addFeature(BootstapContext<?> bootstrap, GenerationStep.Decoration step, TagKey<Biome> spawnTag, ResourceKey<PlacedFeature> feature)
	{
		return new BiomeModifiers.AddFeaturesBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), HolderSet.direct(bootstrap.lookup(Registries.PLACED_FEATURE).getOrThrow(feature)), step);
	}

	private static BiomeModifier addSpawn(BootstapContext<?> bootstrap, TagKey<Biome> spawnTag, SpawnerData... data)
	{
		return new BiomeModifiers.AddSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), List.of(data));
	}

	private static BiomeModifier removeSpawns(BootstapContext<?> bootstrap, TagKey<Biome> spawnTag, HolderSet<EntityType<?>> entityTypes)
	{
		return new BiomeModifiers.RemoveSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), entityTypes);
	}

	private static List<Holder<EntityType<?>>> getAllEntities()
	{
		return BuiltInRegistries.ENTITY_TYPE.holders().filter(holder -> holder.unwrapKey().isPresent() && holder.unwrapKey().get().location().getNamespace().equals(RediscoveredMod.MODID)).map(holder -> (Holder<EntityType<?>>) holder).toList();		
	}
}
