package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.data.loot_functions.SetAttachedItemFunction;
import com.legacy.rediscovered.data.loot_functions.SetAttachedQuiverFunction;
import com.legacy.rediscovered.data.loot_functions.SetQuiverContentsFunction;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;

@RegistrarHolder
public class RediscoveredLootFunctions
{
	public static final RegistrarHandler<LootItemFunctionType> HANDLER = RegistrarHandler.getOrCreate(Registries.LOOT_FUNCTION_TYPE, RediscoveredMod.MODID);
	
	public static final Registrar.Static<LootItemFunctionType> SET_ATTACHED_ITEM = HANDLER.createStatic("set_attached_item", () -> new LootItemFunctionType(SetAttachedItemFunction.CODEC));
	public static final Registrar.Static<LootItemFunctionType> SET_ATTACHED_QUIVER = HANDLER.createStatic("set_attached_quiver", () -> new LootItemFunctionType(SetAttachedQuiverFunction.CODEC));
	public static final Registrar.Static<LootItemFunctionType> SET_QUIVER_CONTENTS = HANDLER.createStatic("set_quiver_contents", () -> new LootItemFunctionType(SetQuiverContentsFunction.CODEC));
}
