package com.legacy.rediscovered.registry;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.block.BaseFakeFireBlock;
import com.legacy.rediscovered.block.BasicPoiBlock;
import com.legacy.rediscovered.block.BrittleBlock;
import com.legacy.rediscovered.block.ChairBlock;
import com.legacy.rediscovered.block.DirtSlabBlock;
import com.legacy.rediscovered.block.DragonAltarBlock;
import com.legacy.rediscovered.block.GearBlock;
import com.legacy.rediscovered.block.GrassSlabBlock;
import com.legacy.rediscovered.block.MiniDragonPylonBlock;
import com.legacy.rediscovered.block.NetherReactorBlock;
import com.legacy.rediscovered.block.ObsidianBulbBlock;
import com.legacy.rediscovered.block.RedDragonEggBlock;
import com.legacy.rediscovered.block.RotationalConverterBlock;
import com.legacy.rediscovered.block.RubyEyeBlock;
import com.legacy.rediscovered.block.ShallowDirtSlabBlock;
import com.legacy.rediscovered.block.ShearedFlowerBlock;
import com.legacy.rediscovered.block.SkylandsPortalBlock;
import com.legacy.rediscovered.block.SpikeBlock;
import com.legacy.rediscovered.block.TableBlock;
import com.legacy.rediscovered.event.RediscoveredMappingChanges;
import com.legacy.rediscovered.item.MiniDragonPylonItem;
import com.legacy.rediscovered.item.RubyEyeItem;
import com.legacy.rediscovered.registry.refs.RediscoveredRefs;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.WoolCarpetBlock;
import net.minecraft.world.level.block.grower.TreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.neoforged.neoforge.registries.RegisterEvent;

public class RediscoveredBlocks
{
	public static final BlockPathTypes FURNITURE_PATH_TYPE = BlockPathTypes.create(RediscoveredMod.MODID + "_furniture", -1.0F);

	public static Block ruby_ore, deepslate_ruby_ore, ruby_block, ancient_crying_obsidian, glowing_obsidian;

	public static Block ancient_cherry_leaves, ancient_cherry_sapling;

	/*public static Block ancient_cherry_leaves, cherry_log, stripped_cherry_log, cherry_wood, stripped_cherry_wood,
			cherry_planks, cherry_slab, cherry_stairs, cherry_fence, cherry_fence_gate, ancient_cherry_sapling, cherry_button,
			cherry_pressure_plate, cherry_door, cherry_trapdoor;*/

	public static Block rose, paeonia, cyan_rose, empty_rose_bush, empty_peony_bush, potted_rose, potted_paeonia,
			potted_cyan_rose, potted_ancient_cherry_sapling;

	public static Block dirt_slab, coarse_dirt_slab, rooted_dirt_slab, grass_slab, podzol_slab, mycelium_slab,
			dirt_path_slab;

	public static Block spikes, rotational_converter, gear;

	public static Block oak_table, oak_chair, spruce_table, spruce_chair, birch_table, birch_chair, jungle_table,
			jungle_chair, acacia_table, acacia_chair, dark_oak_table, dark_oak_chair, mangrove_table, mangrove_chair,
			cherry_table, cherry_chair, bamboo_table, bamboo_chair, crimson_table, crimson_chair, warped_table,
			warped_chair;

	public static Block bright_green_wool, spring_green_wool, sky_blue_wool, slate_blue_wool, lavender_wool, rose_wool;

	public static Block bright_green_carpet, spring_green_carpet, sky_blue_carpet, slate_blue_carpet, lavender_carpet,
			rose_carpet;

	public static Block red_dragon_egg;

	public static Block nether_reactor_core;

	public static Block fake_fire, fake_soul_fire;

	public static Block obsidian_bulb;

	public static Block large_bricks, large_brick_stairs, large_brick_slab, large_brick_wall;

	public static Block brittle_packed_mud, brittle_mud_bricks;

	public static Block ruby_eye;

	public static Block skylands_portal;

	public static Block mini_dragon_pylon;

	public static Block dragon_altar;

	public static Map<Block, Pair<CreativeModeTab, Item>> blockItemMap = new LinkedHashMap<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	public static List<BlockItem> blockItems = new ArrayList<>();
	private static RegisterEvent registry;

	public static void init(RegisterEvent event)
	{
		RediscoveredBlocks.registry = event;
		Registry<Block> blockRegistry = event.getRegistry(Registries.BLOCK);

		ancient_cherry_leaves = register("ancient_cherry_leaves", new LeavesBlock(copy(Blocks.CHERRY_LEAVES).isValidSpawn(RediscoveredBlocks::ocelotOrParrot).isSuffocating(RediscoveredBlocks::never).isViewBlocking(RediscoveredBlocks::never)));

		dirt_slab = register("dirt_slab", new DirtSlabBlock(copy(Blocks.DIRT).randomTicks()));
		coarse_dirt_slab = register("coarse_dirt_slab", new DirtSlabBlock(copy(Blocks.COARSE_DIRT)));
		rooted_dirt_slab = register("rooted_dirt_slab", new DirtSlabBlock(copy(Blocks.ROOTED_DIRT)));
		grass_slab = register("grass_slab", new GrassSlabBlock(Blocks.GRASS_BLOCK, copy(Blocks.GRASS_BLOCK)));
		podzol_slab = register("podzol_slab", new GrassSlabBlock(Blocks.PODZOL, copy(Blocks.PODZOL)));
		mycelium_slab = register("mycelium_slab", new GrassSlabBlock(Blocks.MYCELIUM, copy(Blocks.MYCELIUM)));
		dirt_path_slab = register("dirt_path_slab", new ShallowDirtSlabBlock(Blocks.DIRT_PATH, copy(Blocks.DIRT_PATH)));

		ancient_cherry_sapling = register("ancient_cherry_sapling", new SaplingBlock(new TreeGrower(RediscoveredMod.find("cherry"), Optional.empty(), Optional.of(RediscoveredRefs.ConfiguredFeatureRef.CHERRY_TREE), Optional.empty()), copy(Blocks.CHERRY_SAPLING)));

		ruby_ore = register("ruby_ore", new Block(copy(Blocks.EMERALD_ORE)));
		deepslate_ruby_ore = register("deepslate_ruby_ore", new Block(copy(Blocks.DEEPSLATE_EMERALD_ORE)));
		ruby_block = register("ruby_block", new Block(copy(Blocks.EMERALD_BLOCK)));

		ancient_crying_obsidian = register("ancient_crying_obsidian", new Block(copy(Blocks.OBSIDIAN).pushReaction(PushReaction.BLOCK)));
		glowing_obsidian = register("glowing_obsidian", new BasicPoiBlock(copy(Blocks.OBSIDIAN).pushReaction(PushReaction.BLOCK).lightLevel(state -> 12)));
		obsidian_bulb = register("obsidian_bulb", new ObsidianBulbBlock(BlockBehaviour.Properties.of().lightLevel(state -> state.getValue(ObsidianBulbBlock.LIGHT_LEVEL)).strength(5.0F, 1200.0F).sound(SoundType.STONE).isValidSpawn((s, l, p, e) -> true)));

		spikes = register("spikes", new SpikeBlock(copy(Blocks.OAK_PLANKS)));
		rotational_converter = register("rotational_converter", new RotationalConverterBlock(copy(Blocks.OBSERVER)));
		gear = register("gear", new GearBlock(copy(Blocks.REDSTONE_WIRE).sound(SoundType.METAL)));

		oak_chair = register("oak_chair", new ChairBlock(copy(Blocks.OAK_PLANKS)));
		oak_table = register("oak_table", new TableBlock(copy(Blocks.OAK_PLANKS)));
		spruce_chair = register("spruce_chair", new ChairBlock(copy(Blocks.SPRUCE_PLANKS)));
		spruce_table = register("spruce_table", new TableBlock(copy(Blocks.SPRUCE_PLANKS)));
		birch_chair = register("birch_chair", new ChairBlock(copy(Blocks.BIRCH_PLANKS)));
		birch_table = register("birch_table", new TableBlock(copy(Blocks.BIRCH_PLANKS)));
		jungle_chair = register("jungle_chair", new ChairBlock(copy(Blocks.JUNGLE_PLANKS)));
		jungle_table = register("jungle_table", new TableBlock(copy(Blocks.JUNGLE_PLANKS)));
		acacia_chair = register("acacia_chair", new ChairBlock(copy(Blocks.ACACIA_PLANKS)));
		acacia_table = register("acacia_table", new TableBlock(copy(Blocks.ACACIA_PLANKS)));
		dark_oak_chair = register("dark_oak_chair", new ChairBlock(copy(Blocks.DARK_OAK_PLANKS)));
		dark_oak_table = register("dark_oak_table", new TableBlock(copy(Blocks.DARK_OAK_PLANKS)));
		mangrove_chair = register("mangrove_chair", new ChairBlock(copy(Blocks.MANGROVE_PLANKS)));
		mangrove_table = register("mangrove_table", new TableBlock(copy(Blocks.MANGROVE_PLANKS)));
		cherry_chair = register("cherry_chair", new ChairBlock(copy(Blocks.CHERRY_PLANKS)));
		cherry_table = register("cherry_table", new TableBlock(copy(Blocks.CHERRY_PLANKS)));
		bamboo_chair = register("bamboo_chair", new ChairBlock(copy(Blocks.BAMBOO_PLANKS)));
		bamboo_table = register("bamboo_table", new TableBlock(copy(Blocks.BAMBOO_PLANKS)));
		crimson_chair = register("crimson_chair", new ChairBlock(copy(Blocks.CRIMSON_PLANKS)));
		crimson_table = register("crimson_table", new TableBlock(copy(Blocks.CRIMSON_PLANKS)));
		warped_chair = register("warped_chair", new ChairBlock(copy(Blocks.WARPED_PLANKS)));
		warped_table = register("warped_table", new TableBlock(copy(Blocks.WARPED_PLANKS)));

		rose = register("rose", new FlowerBlock(() -> MobEffects.NIGHT_VISION, 5, copy(Blocks.POPPY)));
		paeonia = register("paeonia", new FlowerBlock(() -> MobEffects.DOLPHINS_GRACE, 5, copy(Blocks.PEONY)));
		cyan_rose = register("cyan_rose", new FlowerBlock(() -> MobEffects.SATURATION, 5, copy(Blocks.POPPY)));

		empty_rose_bush = register("empty_rose_bush", new ShearedFlowerBlock(copy(Blocks.ROSE_BUSH), () -> Blocks.ROSE_BUSH));
		empty_peony_bush = register("empty_peony_bush", new ShearedFlowerBlock(copy(Blocks.PEONY), () -> Blocks.PEONY));

		bright_green_wool = register("bright_green_wool", new Block(copy(Blocks.WHITE_WOOL)));
		spring_green_wool = register("spring_green_wool", new Block(copy(Blocks.WHITE_WOOL)));
		sky_blue_wool = register("sky_blue_wool", new Block(copy(Blocks.WHITE_WOOL)));
		slate_blue_wool = register("slate_blue_wool", new Block(copy(Blocks.WHITE_WOOL)));
		lavender_wool = register("lavender_wool", new Block(copy(Blocks.WHITE_WOOL)));
		rose_wool = register("rose_wool", new Block(copy(Blocks.WHITE_WOOL)));

		bright_green_carpet = register("bright_green_carpet", new WoolCarpetBlock(DyeColor.LIME, copy(Blocks.WHITE_CARPET)));
		spring_green_carpet = register("spring_green_carpet", new WoolCarpetBlock(DyeColor.LIME, copy(Blocks.WHITE_CARPET)));
		sky_blue_carpet = register("sky_blue_carpet", new WoolCarpetBlock(DyeColor.LIGHT_BLUE, copy(Blocks.WHITE_CARPET)));
		slate_blue_carpet = register("slate_blue_carpet", new WoolCarpetBlock(DyeColor.BLUE, copy(Blocks.WHITE_CARPET)));
		lavender_carpet = register("lavender_carpet", new WoolCarpetBlock(DyeColor.PURPLE, copy(Blocks.WHITE_CARPET)));
		rose_carpet = register("rose_carpet", new WoolCarpetBlock(DyeColor.PINK, copy(Blocks.WHITE_CARPET)));

		red_dragon_egg = register("red_dragon_egg", new RedDragonEggBlock(copy(Blocks.DRAGON_EGG).mapColor(MapColor.TERRACOTTA_RED).strength(9.0F, 1200.0F).pushReaction(PushReaction.BLOCK)), new Item.Properties().rarity(Rarity.EPIC));

		nether_reactor_core = register("nether_reactor_core", new NetherReactorBlock(copy(Blocks.NETHERITE_BLOCK).lightLevel(state -> state.getValue(NetherReactorBlock.ACTIVE) ? 9 : 4)), new Item.Properties().rarity(Rarity.RARE));

		potted_rose = registerBlock("potted_rose", flowerPot(blockRegistry, rose));
		potted_paeonia = registerBlock("potted_paeonia", flowerPot(blockRegistry, paeonia));
		potted_cyan_rose = registerBlock("potted_cyan_rose", flowerPot(blockRegistry, cyan_rose));
		potted_ancient_cherry_sapling = registerBlock("potted_ancient_cherry_sapling", flowerPot(blockRegistry, ancient_cherry_sapling));

		large_bricks = register("large_bricks", new Block(copy(Blocks.BRICKS)));
		large_brick_stairs = register("large_brick_stairs", new StairBlock(large_bricks::defaultBlockState, copy(Blocks.BRICK_STAIRS)));
		large_brick_slab = register("large_brick_slab", new SlabBlock(copy(Blocks.BRICK_SLAB)));
		large_brick_wall = register("large_brick_wall", new WallBlock(copy(Blocks.BRICK_WALL)));

		brittle_packed_mud = register("brittle_packed_mud", new BrittleBlock(copy(Blocks.PACKED_MUD).strength(Blocks.PACKED_MUD.defaultDestroyTime() / 2, Blocks.PACKED_MUD.getExplosionResistance() / 2)));
		brittle_mud_bricks = register("brittle_mud_bricks", new BrittleBlock(copy(Blocks.MUD_BRICKS).strength(Blocks.MUD_BRICKS.defaultDestroyTime() / 2, Blocks.MUD_BRICKS.getExplosionResistance() / 2)));

		fake_fire = registerBlock("fake_fire", new BaseFakeFireBlock.FakeFireBlock(copy(Blocks.FIRE), 1.0F));
		fake_soul_fire = registerBlock("fake_soul_fire", new BaseFakeFireBlock.FakeSoulFireBlock(copy(Blocks.SOUL_FIRE), 2.0F));

		ruby_eye = register("ruby_eye", new RubyEyeBlock(copy(Blocks.AIR)), new Item.Properties().stacksTo(1), RubyEyeItem::new);
		skylands_portal = registerBlock("skylands_portal", new SkylandsPortalBlock(copy(Blocks.NETHER_PORTAL).noLootTable()));

		mini_dragon_pylon = register("mini_dragon_pylon", new MiniDragonPylonBlock(BlockBehaviour.Properties.of().sound(SoundType.GLASS).strength(0.1F, Float.MAX_VALUE).lightLevel(state -> 15).noLootTable()), new Item.Properties().rarity(Rarity.RARE), MiniDragonPylonItem::new);
		dragon_altar = register("dragon_altar", new DragonAltarBlock(BlockBehaviour.Properties.ofFullCopy(RediscoveredBlocks.glowing_obsidian).lightLevel(state -> state.getValue(DragonAltarBlock.SHAPE).getLightLevel()).strength(55.0F, 1200.0F)), new Item.Properties().rarity(Rarity.EPIC));

		RediscoveredMappingChanges.addBlockAliases(event.getRegistry(Registries.BLOCK));
	}

	private static FlowerPotBlock flowerPot(Registry<Block> registry, Block flower)
	{
		FlowerPotBlock block = new FlowerPotBlock(() -> (FlowerPotBlock) Blocks.FLOWER_POT, () -> flower, copy(Blocks.FLOWER_POT));
		((FlowerPotBlock) Blocks.FLOWER_POT).addPlant(registry.getKey(flower), () -> block);
		return block;
	}

	private static Boolean ocelotOrParrot(BlockState p_50822_, BlockGetter p_50823_, BlockPos p_50824_, EntityType<?> p_50825_)
	{
		return (boolean) (p_50825_ == EntityType.OCELOT || p_50825_ == EntityType.PARROT);
	}

	private static boolean always(BlockState p_50775_, BlockGetter p_50776_, BlockPos p_50777_)
	{
		return true;
	}

	private static boolean never(BlockState p_50806_, BlockGetter p_50807_, BlockPos p_50808_)
	{
		return false;
	}

	private static BlockBehaviour.Properties copy(Block from)
	{
		BlockBehaviour.Properties properties = BlockBehaviour.Properties.ofFullCopy(from);
		if (from.getLootTable().equals(BuiltInLootTables.EMPTY))
			properties.noLootTable();
		return properties;
	}

	/**
	 * Registers the passed block and creates a BlockItem to register later
	 */
	private static <B extends Block> B register(String key, B block)
	{
		return register(key, block, new Item.Properties());
	}

	/**
	 * Registers the passed block and creates a BlockItem to register later
	 */
	private static <B extends Block> B register(String key, B block, Item.Properties itemProperties)
	{
		return register(key, block, itemProperties, BlockItem::new);
	}

	@SuppressWarnings("unused")
	private static <B extends Block> B register(String key, B block, BiFunction<Block, Item.Properties, BlockItem> blockItemFactory)
	{
		return register(key, block, new Item.Properties(), blockItemFactory);
	}

	/**
	 * Registers the passed block and creates a BlockItem to register later
	 */
	private static <B extends Block> B register(String key, B block, Item.Properties itemProperties, BiFunction<Block, Item.Properties, BlockItem> blockItemFactory)
	{
		registerBlock(key, block);
		BlockItem blockItem = blockItemFactory.apply(block, itemProperties);
		/*blockItem.setRegistryName(block.getRegistryName());*/
		blockItems.add(blockItem);
		return block;
	}

	/**
	 * Registers the passed block with no item
	 */
	private static <B extends Block> B registerBlock(String key, B block)
	{
		registry.register(Registries.BLOCK, RediscoveredMod.locate(key), () -> block);
		return block;
	}
}