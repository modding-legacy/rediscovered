package com.legacy.rediscovered.registry;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.world.structure.pool_elements.BrickPyramidIslandPoolElement;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.levelgen.structure.pools.StructurePoolElementType;

@RegistrarHolder
public class RediscoveredPoolElementTypes
{
	public static final RegistrarHandler<StructurePoolElementType<?>> HANDLER = RegistrarHandler.getOrCreate(Registries.STRUCTURE_POOL_ELEMENT, RediscoveredMod.MODID);

	public static final Registrar.Static<StructurePoolElementType<BrickPyramidIslandPoolElement>> BRICK_PYRAMID_ISLAND = HANDLER.createStatic("brick_pyramid_island", () -> () -> BrickPyramidIslandPoolElement.CODEC);
}
