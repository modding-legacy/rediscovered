package com.legacy.rediscovered.entity.dragon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.Nullable;

import org.joml.Vector3f;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.particles.LightningBoltData;
import com.legacy.rediscovered.client.particles.WindData;
import com.legacy.rediscovered.entity.util.animation.AnimData;
import com.legacy.rediscovered.entity.util.animation.IAnimated;
import com.legacy.rediscovered.network.PacketHandler;
import com.legacy.rediscovered.network.s_to_c.AddClientMotionPacket;
import com.legacy.rediscovered.network.s_to_c.SendBossIdPacket;
import com.legacy.rediscovered.network.s_to_c.SetLocalRainPacket;
import com.legacy.rediscovered.registry.RediscoveredAttachmentTypes;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.util.LogicUtil;
import com.legacy.structure_gel.api.util.Positions;
import com.mojang.math.Axis;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.chat.Component;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerBossEvent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.TicketType;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.BossEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LevelEvent;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class RedDragonBossEntity extends AbstractRedDragonEntity implements Enemy, IAnimated
{
	private static final BiPredicate<RedDragonBossEntity, Entity> ATTACKABLE_TARGET = (d, p) -> EntitySelector.NO_CREATIVE_OR_SPECTATOR.and(EntitySelector.LIVING_ENTITY_STILL_ALIVE).test(p) && d.getSensing().hasLineOfSight(p) && p instanceof Player;
	public final List<AnimData> anims = new ArrayList<>();
	public final AnimData boltChargeAnim = new AnimData(anims, 0.15F, 0.4F, true);
	public final AnimData boltShootAnim = new AnimData(anims, 0.7F, 0.35F, true);
	public final AnimData windBlowAnim = new AnimData(anims, 0.3F, 0.4F, true);
	public final AnimData cloudPrepAnim = new AnimData(anims, 0.3F, 0.4F, true);

	private final ServerBossEvent bossEvent = new ServerBossEvent(this.getDisplayName(), BossEvent.BossBarColor.RED, BossEvent.BossBarOverlay.PROGRESS);

	private static final int DUNGEON_CHUNK_RADIUS = 9;
	public static final TicketType<ChunkPos> RED_DRAGON_TICKET = TicketType.create(RediscoveredMod.find("red_dragon"), Comparator.comparingLong(ChunkPos::toLong), 60);
	private static final EntityDataAccessor<Integer> PYLON_TIME = SynchedEntityData.defineId(RedDragonBossEntity.class, EntityDataSerializers.INT);
	private static final EntityDataAccessor<Boolean> IN_DEATH = SynchedEntityData.defineId(RedDragonBossEntity.class, EntityDataSerializers.BOOLEAN);

	private static final EntityDataAccessor<Float> X_ROT_MOD_OLD = SynchedEntityData.defineId(RedDragonBossEntity.class, EntityDataSerializers.FLOAT);
	private static final EntityDataAccessor<Float> X_ROT_MOD = SynchedEntityData.defineId(RedDragonBossEntity.class, EntityDataSerializers.FLOAT);

	public int dragonDeathTicks, burstSpawnDelay = 60, extraLightningTime;

	private boolean spawnsRain = true;

	int boltBallCooldown = 80, thunderCloudCooldown = 100, windCooldown = 120;

	int attackCooldown = 60;
	AttackType attackType = AttackType.BALL;

	public long passiveBoltSeed, chargeBoltSeed;

	public RedDragonBossEntity(EntityType<? extends RedDragonBossEntity> type, Level level)
	{
		super(type, level);

		this.noPhysics = true;
		this.xpReward = 0;

		// this.spawnsRain = false;
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return AbstractRedDragonEntity.createDragonAttributes().add(Attributes.MAX_HEALTH, 300.0D);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.getEntityData().define(PYLON_TIME, 0);
		this.getEntityData().define(IN_DEATH, false);
		this.getEntityData().define(X_ROT_MOD, 0.0F);
		this.getEntityData().define(X_ROT_MOD_OLD, 0.0F);
	}

	@Override
	public void handleEntityEvent(byte pId)
	{
		super.handleEntityEvent(pId);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(1, new ShootBoltBallGoal());
		this.goalSelector.addGoal(2, new CloudLineGoal());
		this.goalSelector.addGoal(3, new FlapWindAttackGoal());
	}

	static final String HOME_POS_KEY = "DragonHomePos", PYLON_TIME_KEY = "PylonChargeTime",
			DEATH_TICKS_KEY = "DragonDeathTicks", BURST_SPAWN_DELAY = "BurstSpawnDelay", ATTACK_TYPE_KEY = "AttackType",
			ATTACK_COOLDOWN_KEY = "AttackCooldown";

	@Override
	public void addAdditionalSaveData(CompoundTag pCompound)
	{
		super.addAdditionalSaveData(pCompound);

		if (this.homePos != null)
			pCompound.put(HOME_POS_KEY, NbtUtils.writeBlockPos(this.homePos));

		pCompound.putInt(PYLON_TIME_KEY, this.getPylonTime());
		pCompound.putInt(DEATH_TICKS_KEY, this.dragonDeathTicks);
		pCompound.putInt(BURST_SPAWN_DELAY, this.burstSpawnDelay);
		pCompound.putInt(ATTACK_COOLDOWN_KEY, this.attackCooldown);
		pCompound.putInt(ATTACK_TYPE_KEY, this.attackType.getId());
	}

	@Override
	public void readAdditionalSaveData(CompoundTag pCompound)
	{
		super.readAdditionalSaveData(pCompound);

		if (pCompound.contains(HOME_POS_KEY, 10))
			this.homePos = NbtUtils.readBlockPos(pCompound.getCompound(HOME_POS_KEY));

		this.setPylonTime(pCompound.getInt(PYLON_TIME_KEY));

		this.dragonDeathTicks = pCompound.getInt(DEATH_TICKS_KEY);
		this.burstSpawnDelay = pCompound.getInt(BURST_SPAWN_DELAY);

		this.attackCooldown = pCompound.getInt(ATTACK_COOLDOWN_KEY);
		this.attackType = AttackType.getFromId(pCompound.getInt(ATTACK_TYPE_KEY));

		if (this.hasCustomName())
			this.bossEvent.setName(this.getDisplayName());
	}

	@Override
	public void tick()
	{
		super.tick();
		this.tickAnims(this);

		if (this.level().isClientSide())
		{
			this.chargeBoltSeed = this.random.nextLong();

			if (this.tickCount % 2 == 0)
				this.passiveBoltSeed = this.random.nextLong();

			if (this.windBlowAnim.atApex())
			{
				float boxDistance = 20, boxSize = 20;
				Vector3f boxCenter = new Vector3f(0, 0, boxDistance).rotate(Axis.YN.rotationDegrees(this.getYRot()));
				Vec3 moveVec = this.position().add(boxCenter.x(), boxCenter.y(), boxCenter.z());
				Vec3 baseMotion = moveVec.subtract(this.position()).normalize();
				Vec3 motion = baseMotion.scale(2 + LogicUtil.withinRange(1, random));

				for (int i = 0; i < 12; ++i)
					this.level().addParticle(new WindData(4, 1.0F), moveVec.x() + LogicUtil.withinRange(boxSize, random), moveVec.y() + LogicUtil.withinRange(boxSize, random), moveVec.z() + LogicUtil.withinRange(boxSize, random), motion.x(), motion.y(), motion.z());

				// Sync large particle with wing flap
				float wingSin = Mth.cos(this.animTime * Mth.PI * 2.0F);
				float wingSinOld = Mth.cos(this.prevAnimTime * Mth.PI * 2.0F);
				if (wingSinOld <= -0.3F && wingSin >= -0.3F)
				{
					float speed = 2.5F;
					Vec3 m = baseMotion.multiply(speed, speed, speed);
					this.level().addParticle(new WindData(0, 10.0F), true, this.getX(), this.getY() - 1.5, this.getZ(), m.x(), m.y(), m.z());
				}
			}
		}
		else
		{
			this.spawnsRain = !this.isNoAi();

			if (!this.isNoAi() && (this.targetVec == null || this.targetVec == Vec3.ZERO || this.targetVec != null && Math.sqrt(this.distanceToSqr(this.targetVec)) > 300) && (this.tickCount <= 40 || this.getPylonTime() > 0))
			{
				this.setNewTarget();
			}
		}
	}

	@Override
	public float getFlapSpeed(float motion)
	{
		float boltPose = this.boltChargeAnim.getValue(1.0F);
		float windPose = this.windBlowAnim.getValue(1.0F);
		float cloudPose = this.cloudPrepAnim.getValue(1.0F);
		float mixed = Mth.clamp(boltPose + windPose + cloudPose, 0, 1);
		return (super.getFlapSpeed(motion) * (1.0F - mixed)) + (0.12F * boltPose) + (0.08F * windPose) + (0.15F * cloudPose);
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.level() instanceof ServerLevel sl)
		{
			// DEBUG CODE
			/*if (this.targetVec == null)
			{
				// if (this.tickCount % 200 == 0)
				if (!this.boltChargeAnim.isActive())
				{
					this.boltChargeAnim.play();
				}
			
				if (boltChargeAnim.atApex())
					boltShootAnim.play();
			
				if (boltShootAnim.getApexTicks() >= 30)
				{
					boltShootAnim.stop();
					boltChargeAnim.stop();
				}
			}*/

			/*if (this.targetVec == null)
			{
				if (!this.cloudPrepAnim.isActive())
				{
					this.cloudPrepAnim.play();
				}
			
				if (cloudPrepAnim.getApexTicks() >= 80)
				{
					cloudPrepAnim.stop();
				}
			}*/

			if (this.attackCooldown > 0)
				--attackCooldown;

			if (this.boltBallCooldown > 0)
				--boltBallCooldown;

			if (this.thunderCloudCooldown > 0)
				--thunderCloudCooldown;

			if (this.windCooldown > 0)
				--windCooldown;

			// this.pylonTimeO = this.getPylonTime();
			if (this.homePos != null)
			{
				// add the ticket if a player is close to the dragon or the pyramid
				if (this.getPlayersInArea(true).findAny().isPresent())
				{
					ChunkPos homeChunk = new ChunkPos(this.homePos);
					sl.getChunkSource().addRegionTicket(RED_DRAGON_TICKET, homeChunk, DUNGEON_CHUNK_RADIUS, homeChunk);
				}
			}

			if (this.getPylonTime() > 0)
			{
				this.setPylonTime(this.getPylonTime() - 1);

				if (this.getPylonTime() == 1)
				{
					this.extraLightningTime = 20 * 3;
					this.ambientSoundTime = -this.getAmbientSoundInterval();
					this.level().playSound(null, this, RediscoveredSounds.ENTITY_RED_DRAGON_SHIELD_DOWN, this.getSoundSource(), 20F, 1F);
				}
				Player player = null;

				if (--this.burstSpawnDelay <= 0 && (player = this.findTarget()) != null)
				{
					float dist = 3.5F;

					for (int w = 0; w < 3 + this.getRandom().nextInt(2); ++w)
					{
						float offset = this.getRandom().nextFloat() * 360.0F;
						double x = dist * Math.cos(offset);
						double z = dist * Math.sin(offset);

						Vec3 offsetVec = this.position().add(x, 0, z);

						PylonBurstEntity projectile = new PylonBurstEntity(this.level(), this, player);
						projectile.setPos(offsetVec.x, offsetVec.y, offsetVec.z);

						double xc = this.position().x - offsetVec.x;
						double zc = this.position().z - offsetVec.z;

						float vertMul = 0.15F;

						projectile.setDeltaMovement(xc * -vertMul, 0.28F + (this.getRandom().nextFloat() * 0.05F), zc * -vertMul);
						this.level().addFreshEntity(projectile);

						projectile.timeUntilBlast = (12 + this.getRandom().nextInt(5)) * 20;
						projectile.speedAddition = this.random.nextFloat() * 0.05F;
					}

					this.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_SHED_BURSTS, 5, 0.5F + this.getRandom().nextFloat() * 0.1F);

					// 25
					this.burstSpawnDelay = (10 + this.getRandom().nextInt(5)) * 20;
				}
			}
			else if (this.spawnsRain && this.getPlayersInArea().findAny().isPresent())
			{
				if (this.extraLightningTime > 0 && this.tickCount % 10 == 9 || this.isDeadOrDying() && this.tickCount % 15 == 0 || this.getRandom().nextInt(100) == 0 && this.homePos != null)
				{
					int length = 80;
					BlockPos offset = this.homePos.offset(this.getRandom().nextInt(length) - this.getRandom().nextInt(length), 0, this.getRandom().nextInt(length) - this.getRandom().nextInt(length));
					int height = this.level().getHeight(Types.WORLD_SURFACE, offset.getX(), offset.getZ());

					if (height > 10)
					{
						LightningBolt bolt = EntityType.LIGHTNING_BOLT.create(this.level());
						bolt.setVisualOnly(true);

						bolt.setPos(offset.getX(), height, offset.getZ());

						this.level().addFreshEntity(bolt);
					}
				}

				if (this.extraLightningTime > 0)
					--this.extraLightningTime;

				BlockPos particleCenter = this.homePos != null ? this.homePos : this.blockPosition();
				for (ServerPlayer player : this.getPlayersInArea().toList())
				{
					sl.sendParticles(player, new LightningBoltData(this.getRandom()), true, particleCenter.getX(), 250, particleCenter.getZ(), this.random.nextInt(2) + 1, 200, 10, 200, 0);
				}
			}
			if (!this.isNoAi() && this.spawnsRain)
			{
				if (this.tickCount % 10 == 0)
				{
					this.applyLocalEffects(sl);
				}
			}

			this.bossEvent.setProgress(this.getHealth() / this.getMaxHealth());
		}
	}

	public void applyLocalEffects(ServerLevel level)
	{
		Set<ServerPlayer> playersOutside = this.bossEvent.getPlayers().stream().filter(player -> !this.isInBossArea(player, this.getLocalEffectsDist(), true)).collect(Collectors.toSet());
		for (ServerPlayer player : playersOutside)
		{
			this.bossEvent.removePlayer(player);
		}

		long gameTime = level.getGameTime();
		this.getEntitiesInArea(true).forEach(entity ->
		{
			if (entity instanceof ServerPlayer p)
			{
				if (!playersOutside.contains(p))
				{
					if (this.getPylonTime() <= 0)
					{
						if (!this.bossEvent.getPlayers().contains(p))
							PacketHandler.sendToClient(new SendBossIdPacket(this.bossEvent.getId()), p);
						this.bossEvent.addPlayer(p);

						this.setRainTime(level, entity, gameTime);
					}
					else
					{
						this.bossEvent.removePlayer(p);
					}
				}
			}
			else
			{
				if (this.getPylonTime() <= 0)
				{
					this.setRainTime(level, entity, gameTime);
				}
			}
		});
	}

	private void setRainTime(ServerLevel level, Entity entity, long rainTime)
	{
		BlockPos pos = entity.blockPosition();
		if (level.canSeeSky(pos))
		{
			entity.setData(RediscoveredAttachmentTypes.RAIN_TIME, rainTime);
			PacketHandler.sendToClients(new SetLocalRainPacket(entity.getId()), level, pos, 500);
		}
	}

	@Override
	public void customServerAiStep()
	{
		super.customServerAiStep();
	}

	@Override
	protected void tickDeath()
	{
		this.hurtTime = 5;
		if (this.level().isClientSide() && !this.getEntityData().get(IN_DEATH))
			return;

		BlockPos home = this.homePos;
		if (home != null && Mth.sqrt((float) this.distanceToSqr(new Vec3(home.getX(), home.getY() + 20, home.getZ()))) > 10 && this.dragonDeathTicks <= 0)
		{
			this.targetVec = Vec3.atCenterOf(home.above(20));
			return;
		}

		if (!this.level().isClientSide())
			this.getEntityData().set(IN_DEATH, true);

		this.prevAnimTime = this.animTime;
		++this.dragonDeathTicks;

		if (this.dragonDeathTicks >= 180 && this.dragonDeathTicks <= 200)
		{
			double xOffs = (this.random.nextDouble() - 0.5D) * 8.0D;
			double yOffs = (this.random.nextDouble() - 0.5D) * 4.0D;
			double zOffs = (this.random.nextDouble() - 0.5D) * 8.0D;
			this.level().addParticle(ParticleTypes.EXPLOSION, this.getX() + xOffs, this.getY() + 2.0D + yOffs, this.getZ() + zOffs, 0.0D, 0.0D, 0.0D);
		}

		int var4;
		int var5;

		if (!this.level().isClientSide())
		{
			if (this.dragonDeathTicks > 150 && this.dragonDeathTicks % 5 == 0)
			{
				var4 = !this.isTame() ? 100 : 0;

				while (var4 > 0)
				{
					var5 = ExperienceOrb.getExperienceValue(var4);
					var4 -= var5;
					this.level().addFreshEntity(new ExperienceOrb(this.level(), this.getX(), this.getY(), this.getZ(), var5));
				}
			}

			if (this.dragonDeathTicks == 1)
				this.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_DEATH, this.getSoundVolume(), 1.0F);

		}
		this.move(MoverType.SELF, new Vec3(0.0D, 0.1F, 0.0D));

		this.setYRot(this.getYRot() + 20.0F);

		if (this.dragonDeathTicks == 200 && !this.level().isClientSide)
		{
			var4 = !this.isTame() ? 100 : 0;

			while (var4 > 0)
			{
				var5 = ExperienceOrb.getExperienceValue(var4);
				var4 -= var5;
				this.level().addFreshEntity(new ExperienceOrb(this.level(), this.getX(), this.getY(), this.getZ(), var5));
			}

			if (this.homePos != null)
			{
				// Try to position the egg on top of blocks if something occupies that space
				BlockPos eggPos = this.homePos;
				int maxEggOffset = 32;
				for (int i = 0; i <= maxEggOffset && !this.level().getBlockState(eggPos).canBeReplaced(); i++)
				{
					// Give up and just use the homePos regardless of what's there
					if (i == maxEggOffset)
					{
						eggPos = this.homePos;
						break;
					}
					eggPos = eggPos.above();
				}
				BlockState egg = RediscoveredBlocks.red_dragon_egg.defaultBlockState();
				this.level().destroyBlock(eggPos, true);
				this.level().setBlockAndUpdate(eggPos, egg);
				this.level().levelEvent(LevelEvent.PARTICLES_DESTROY_BLOCK, eggPos, Block.getId(egg));

				LightningBolt lightning = EntityType.LIGHTNING_BOLT.create(this.level());
				lightning.setVisualOnly(true);
				lightning.setPos(Vec3.atBottomCenterOf(eggPos));
				this.level().addFreshEntity(lightning);
			}

			this.remove(RemovalReason.KILLED);
		}

		if (!this.getPassengers().isEmpty())
			this.getPassengers().get(0).stopRiding();
	}

	public void setPylonTime(int time)
	{
		this.getEntityData().set(PYLON_TIME, time);
	}

	public int getPylonTime()
	{
		return this.getEntityData().get(PYLON_TIME);
	}

	@Override
	public boolean isInvulnerableTo(DamageSource pSource)
	{
		return pSource != this.damageSources().lightningBolt() && super.isInvulnerableTo(pSource);
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (source.isCreativePlayer() && source.getEntity() instanceof Player p && p.getMainHandItem().getItem() == Items.DEBUG_STICK && !this.level().isClientSide)
		{
			this.targetVec = null;
			return false;
		}

		if (this.getPylonTime() > 0)
			return false;

		return super.hurt(source, amount);
	}

	@Override
	public void die(DamageSource pCause)
	{
		if (this.level() instanceof ServerLevel sl)
		{
			if (this.homePos != null)
			{
				ChunkPos homeChunk = new ChunkPos(this.homePos);
				sl.getChunkSource().removeRegionTicket(RED_DRAGON_TICKET, homeChunk, DUNGEON_CHUNK_RADIUS, homeChunk);
			}
		}
		super.die(pCause);
	}

	@Override
	protected void setNewTarget()
	{
		this.setXRot(0);

		/*List<ServerPlayer> players = null;
		if (this.getPylonTime() <= 0 && this.random.nextInt(3) == 0 && !(players = this.getPlayersInArea().filter(p -> this.getSensing().hasLineOfSight(p) && !p.isCreative() && !p.isSpectator()).toList()).isEmpty())
		{
			this.setDragonTarget(players.get(this.random.nextInt(players.size())));
			return;
		}*/

		super.setNewTarget();
	}

	@Override
	public float maxHorizontalWanderDistance()
	{
		return this.getPylonTime() <= 0 ? 80 : super.maxHorizontalWanderDistance();
	}

	@Override
	public float verticalWanderDistance()
	{
		return this.getPylonTime() <= 0 ? -25 + (this.random.nextFloat() * 20) : super.verticalWanderDistance();
	}

	@Override
	public Vec3 wanderCenterVector()
	{
		return super.wanderCenterVector();
	}

	@Override
	public void startSeenByPlayer(ServerPlayer pPlayer)
	{
		super.startSeenByPlayer(pPlayer);

		if (!this.isNoAi() && this.spawnsRain && (this.getPylonTime() <= 0 || this.homePos == null))
		{
			/*if (!this.bossEvent.getPlayers().contains(pPlayer))
				PacketHandler.sendToClient(new SendBossIdPacket(this.bossEvent.getId()), pPlayer);
			
			this.bossEvent.addPlayer(pPlayer);*/
		}
	}

	@Override
	public void stopSeenByPlayer(ServerPlayer pPlayer)
	{
		super.stopSeenByPlayer(pPlayer);
		this.bossEvent.removePlayer(pPlayer);
	}

	@Override
	public boolean isTame()
	{
		return false;
	}

	@Override
	public void setTame(boolean pTamed)
	{
	}

	@Override
	@Nullable
	public LivingEntity getOwner()
	{
		return null;
	}

	@Override
	@Nullable
	public UUID getOwnerUUID()
	{
		return null;
	}

	@Override
	public void setOwnerUUID(UUID pUuid)
	{
	}

	@Override
	public boolean isInSittingPose()
	{
		return false;
	}

	@Override
	public int getAge()
	{
		return 0;
	}

	@Override
	public void knockback(double strength, double x, double z)
	{
	}

	@Override
	public void push(double x, double y, double z)
	{
	}

	@Override
	public void setCustomName(@Nullable Component pName)
	{
		super.setCustomName(pName);
		this.bossEvent.setName(this.getDisplayName());
	}

	@Override
	protected boolean canPathfind()
	{
		return !this.getEntityData().get(IN_DEATH);
	}

	public Stream<ServerPlayer> getPlayersInArea()
	{
		return this.getPlayersInArea(false);
	}

	public Stream<ServerPlayer> getPlayersInArea(boolean includeDragon)
	{
		if (this.level() instanceof ServerLevel sl)
		{
			int maxDist = getLocalEffectsDist();
			return sl.players().stream().filter(p -> this.isInBossArea(p, maxDist, includeDragon)).sorted((e1, e2) -> Double.compare(e1.distanceToSqr(this), e2.distanceToSqr(this)));
		}
		return Stream.empty();
	}

	public Stream<Entity> getEntitiesInArea(boolean includeDragon)
	{
		if (this.level() instanceof ServerLevel sl)
		{
			int maxDist = getLocalEffectsDist();
			return StreamSupport.stream(sl.getAllEntities().spliterator(), false).filter(entity -> this.isInBossArea(entity, maxDist, includeDragon));
		}
		return Stream.empty();
	}

	public boolean isInBossArea(Entity entity, int maxDist, boolean includeDragon)
	{
		int dist = maxDist * maxDist;
		return (this.homePos == null || includeDragon) && (float) entity.distanceToSqr(this.getX(), entity.getY(), this.getZ()) < dist || this.homePos != null && (float) entity.distanceToSqr(this.homePos.getX(), entity.getY(), this.homePos.getZ()) < dist;
	}

	public int getLocalEffectsDist()
	{
		return (DUNGEON_CHUNK_RADIUS + 1) * 16;
	}

	@Override
	public List<AnimData> getAnimations()
	{
		return this.anims;
	}

	public ServerPlayer findTarget()
	{
		return this.getPlayersInArea().filter(e -> ATTACKABLE_TARGET.test(this, e)).findFirst().orElse(null);
	}

	public AttackType randomizeAttackType(AttackType type)
	{
		AttackType newType = Util.getRandom(Arrays.asList(AttackType.values()).stream().filter(t -> type == null || t != type).toList(), this.getRandom());
		this.attackType = newType;
		this.attackCooldown = newType.getRandomCooldown(this.getRandom());
		return newType;
	}

	public float getXRotModifier()
	{
		return this.getEntityData().get(X_ROT_MOD);
	}

	public float getOldXRotModifier()
	{
		return this.getEntityData().get(X_ROT_MOD_OLD);
	}

	@Override
	public SoundSource getSoundSource()
	{
		return SoundSource.HOSTILE;
	}

	class ShootBoltBallGoal extends Goal
	{
		private final RedDragonBossEntity boss = RedDragonBossEntity.this;
		private ServerPlayer target;

		private int baseTime = 200;
		private int chargeTime;

		public ShootBoltBallGoal()
		{
			super();

			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
		}

		@Override
		public boolean canUse()
		{
			boolean base = this.boss.getPylonTime() <= 0 && this.boss.attackCooldown <= 0 && this.boss.attackType == AttackType.BALL && !this.boss.inWall && (this.target = this.boss.findTarget()) != null;

			if (base)
			{
				if (this.boss.distanceTo(this.target) <= 20)
					this.boss.setDragonTarget(null);
				else if (this.boss.distanceTo(this.target) <= 40)
					return true;
				else
					this.boss.setDragonTarget(this.target);
			}

			return false;
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.chargeTime > 0 && this.target != null && !this.target.isRemoved() && this.target.isAlive();
		}

		@Override
		public void start()
		{
			this.boss.boltChargeAnim.play();
			this.boss.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_BOLT_BALL_CHARGE, this.boss.getSoundVolume(), 1.0F);
			this.chargeTime = this.baseTime;

			this.boss.targetVec = null;

			this.boss.setDragonTarget(null);
		}

		@Override
		public void tick()
		{
			if (this.target == null || this.target.isRemoved() || !this.target.isAlive())
				return;

			if (this.chargeTime > 20)
			{
				this.boss.setDeltaMovement(this.boss.getDeltaMovement().scale(0.9F));

				Vec3 pv = target.position();
				float rotDest = ((float) -Mth.atan2(pv.x() - this.boss.getX(), pv.z() - this.boss.getZ())) * Mth.RAD_TO_DEG;
				this.boss.setYRot(Mth.rotLerp(0.2F, this.boss.getYRot(), rotDest));

				double d0 = pv.x() - neck.getX();
				double d1 = pv.y() - (neck.getY() + 0.5F);
				double d2 = pv.z() - neck.getZ();

				float xr = (float) ((-Mth.atan2(d1, Math.sqrt(d0 * d0 + d2 * d2)) * Mth.RAD_TO_DEG));

				this.boss.getEntityData().set(X_ROT_MOD_OLD, this.boss.getXRotModifier());
				this.boss.getEntityData().set(X_ROT_MOD, Mth.wrapDegrees(xr) * Mth.DEG_TO_RAD);

				if (boltChargeAnim.atApex())
					boltShootAnim.play();

				if (this.boss.boltShootAnim.getApexTicks() == 1)
				{
					this.boss.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_BOLT_BALL_SHOOT, this.boss.getSoundVolume(), 1.0F);

					BoltBallEntity ball = new BoltBallEntity(this.boss.level(), this.boss, null);

					ball.timeUntilBlast = 200;
					ball.setPos(this.boss.head.position());

					ball.setDeltaMovement(this.target.position().subtract(this.boss.head.position()).normalize().scale(1F));
					this.boss.level().addFreshEntity(ball);
				}
				else if (boltShootAnim.getApexTicks() >= 20)
				{
					boltShootAnim.stop();
					boltChargeAnim.stop();

					this.chargeTime = 0;
				}
			}
		}

		@Override
		public void stop()
		{
			this.boss.randomizeAttackType(this.boss.attackType);

			this.target = null;
			this.boss.boltChargeAnim.stop();
			this.boss.boltShootAnim.stop();

			this.boss.setNewTarget();

			this.boss.thunderCloudCooldown = Math.max(60, this.boss.thunderCloudCooldown);
		}

		@Override
		public boolean isInterruptable()
		{
			return false;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}
	}

	class CloudLineGoal extends Goal
	{
		final RedDragonBossEntity boss = RedDragonBossEntity.this;
		private ServerPlayer target;

		boolean spawnedClouds;
		BlockPos targetPos;

		public CloudLineGoal()
		{
			super();

			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
		}

		@Override
		public boolean canUse()
		{
			boolean base = this.boss.getPylonTime() <= 0 && this.boss.attackCooldown <= 0 && this.boss.attackType == AttackType.THUNDER && !this.boss.inWall && (this.target = this.boss.findTarget()) != null;

			if (base)
			{
				if (this.boss.distanceTo(this.target) < 50)
					this.boss.setDragonTarget(null);
				else if (this.boss.distanceTo(this.target) <= 60)
					return true;
				else
					this.boss.setDragonTarget(this.target);
			}

			return false;
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.target != null && !this.target.isRemoved() && this.target.isAlive() && cloudPrepAnim.getApexTicks() < 40;
		}

		@Override
		public void start()
		{
			this.boss.cloudPrepAnim.play();
			
			// TODO Bailey Post-Beta: Custom sound
			/*this.boss.playSound(SoundEvents.BEACON_ACTIVATE, this.boss.getSoundVolume(), 1.0F);*/
			this.spawnedClouds = false;

			this.boss.targetVec = null;

			this.boss.setDragonTarget(null);
		}

		@Override
		public void tick()
		{
			if (this.target == null || this.target.isRemoved() || !this.target.isAlive())
				return;

			Vec3 pv = target.position();
			float rotDest = -((float) Mth.atan2(pv.x() - this.boss.getX(), pv.z() - this.boss.getZ())) * Mth.RAD_TO_DEG;
			this.boss.setYRot(Mth.rotLerp(0.2F, this.boss.getYRot(), rotDest));

			if (!spawnedClouds && this.boss.cloudPrepAnim.atApex())
			{
				// TODO Bailey Post-Beta: Custom sound
				/*this.boss.playSound(SoundEvents.BEACON_POWER_SELECT, 2.0F, 0.8F);*/

				float f = (float) Mth.atan2(target.getZ() - boss.getZ(), target.getX() - boss.getX());

				for (int l = 0; l < 16; ++l)
				{
					double d2 = 10 + (5.25D * (double) (l + 1.5D));
					int j = 30 + (3 * l);
					this.spawnClouds(boss.getX() + (double) Mth.cos(f) * d2, boss.getZ() + (double) Mth.sin(f) * d2, this.boss.homePos != null ? this.boss.homePos.getY() + 15 : this.boss.getY() + 10, j);
				}

				this.spawnedClouds = true;
			}
		}

		@Override
		public void stop()
		{
			this.boss.randomizeAttackType(this.boss.attackType);

			this.target = null;
			this.boss.cloudPrepAnim.stop();

			this.boss.setNewTarget();
		}

		@Override
		public boolean isInterruptable()
		{
			return false;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}

		public void spawnClouds(double xPos, double zPos, double yPos, int warmupDelay)
		{
			Level level = this.boss.level();
			BlockPos blockpos = Positions.blockPos(xPos, yPos, zPos);
			boolean flag = true;
			double d0 = 0.0D;

			if (flag && level.noCollision(new AABB(blockpos)))
			{
				ThunderCloudEntity cloud = new ThunderCloudEntity(level, xPos, (double) blockpos.getY() + d0, zPos);

				cloud.setDelay(warmupDelay);
				cloud.setDespawnDelay(30);
				cloud.setOwner(this.boss);

				level.addFreshEntity(cloud);
			}

		}
	}

	class FlapWindAttackGoal extends Goal
	{
		private final RedDragonBossEntity boss = RedDragonBossEntity.this;
		private Direction offsetDir = Direction.NORTH;
		private int ticks;

		public FlapWindAttackGoal()
		{
			super();

			this.offsetDir = getRandomDirection();
			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
		}

		@Override
		public boolean canUse()
		{
			boolean base = this.boss.homePos != null && this.boss.getPylonTime() <= 0 && this.boss.attackCooldown <= 0 && this.boss.attackType == AttackType.WIND && !this.boss.inWall && !this.boss.level().getEntities(this.boss, new AABB(this.boss.homePos).inflate(10), e -> ATTACKABLE_TARGET.test(this.boss, e)).isEmpty();

			if (base)
			{
				BlockPos homeOffset = this.boss.homePos.relative(this.offsetDir, 30);
				float dist = Mth.sqrt((float) this.boss.distanceToSqr(homeOffset.getX(), this.boss.getY(), homeOffset.getZ()));

				if (dist <= 5)
					return true;
				else
				{
					if (this.boss.getY() - 8 > homeOffset.getY())
						this.boss.targetVec = new Vec3(this.boss.getX(), homeOffset.getY() + 5, this.boss.getZ());
					else
						this.boss.targetVec = Vec3.atCenterOf(homeOffset.above(5));
				}
			}

			return false;
		}

		@Override
		public boolean canContinueToUse()
		{
			// change to true to make it happen forever
			return this.boss.windBlowAnim.getApexTicks() <= 160;
		}

		@Override
		public void start()
		{
			this.ticks = 0;
			this.boss.windBlowAnim.play();
			this.boss.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_PREPARE_WIND_BLOW, this.boss.getSoundVolume(), 1.0F);

			this.boss.targetVec = null;

			this.boss.setDragonTarget(null);
		}

		@Override
		public void tick()
		{
			if (this.boss.homePos == null)
				return;

			this.boss.targetVec = null;

			Vec3 pv = Vec3.atCenterOf(this.boss.homePos);
			float rotDest = -((float) Mth.atan2(pv.x() - this.boss.getX(), pv.z() - this.boss.getZ())) * Mth.RAD_TO_DEG;
			this.boss.setYRot(Mth.rotLerp(0.2F, this.boss.getYRot(), rotDest));

			if (this.boss.windBlowAnim.getApexTicks() > 0)
			{
				this.boss.windBlowAnim.play();

				float boxDistance = 35, boxSize = 35;
				Vector3f boxCenter = new Vector3f(0, 0, boxDistance).rotate(Axis.YN.rotationDegrees(rotDest));
				Vec3 moveVec = this.boss.position().add(boxCenter.x(), boxCenter.y(), boxCenter.z());
				AABB windBox = AABB.unitCubeFromLowerCorner(moveVec).move(-0.5F, -0.5F, -0.5F).inflate(boxSize);

				for (var e : this.boss.level().getEntities(this.boss, windBox))
				{
					boolean sight = this.boss.getSensing().hasLineOfSight(e);

					double kb = e instanceof LivingEntity living ? (1.0F - Mth.clamp(living.getAttributeValue(Attributes.KNOCKBACK_RESISTANCE), 0, 1)) : 1;
					Vec3 motion = moveVec.subtract(this.boss.position()).normalize().scale((sight ? 0.2F : 0.04F) * kb);

					if (e.level().getBrightness(LightLayer.SKY, e.blockPosition()) >= 10 && !e.isSpectator() && !(e instanceof Player p && p.isCreative()))
					{
						e.setShiftKeyDown(false);

						if (e instanceof ServerPlayer player)
							PacketHandler.sendToClient(new AddClientMotionPacket(motion.x(), motion.y(), motion.z()), player);
						else
							e.setDeltaMovement(e.getDeltaMovement().add(motion));
					}
				}

				if (this.ticks % 40 == 0)
					this.boss.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_WIND_BLOW, 8, 1.0F);

				++this.ticks;
			}
		}

		@Override
		public void stop()
		{
			this.boss.randomizeAttackType(this.boss.attackType);
			this.boss.windCooldown = 140;
			this.boss.windBlowAnim.stop();

			this.boss.setNewTarget();

			this.boss.thunderCloudCooldown = Math.max(60, this.boss.thunderCloudCooldown);

			this.offsetDir = getRandomDirection();
		}

		@Override
		public boolean isInterruptable()
		{
			return false;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return true;
		}

		private Direction getRandomDirection()
		{
			return Util.getRandomSafe(List.of(Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST), this.boss.getRandom()).orElse(Direction.NORTH);
		}
	}

	private enum AttackType
	{
		BALL(0, 80, 20),
		THUNDER(1, 100, 20),
		WIND(2, 60, 10);

		final int id, cooldown, cooldownAddition;

		private AttackType(int id, int cooldown, int cooldownAddition)
		{
			this.id = id;
			this.cooldown = cooldown;
			this.cooldownAddition = cooldownAddition;
		}

		public int getId()
		{
			return this.id;
		}

		public int getRandomCooldown(RandomSource rand)
		{
			return this.cooldown + rand.nextInt(this.cooldownAddition);
		}

		public static AttackType getFromId(int id)
		{
			for (AttackType type : values())
				if (id == type.id)
					return type;
			return AttackType.BALL;
		}
	}
}
