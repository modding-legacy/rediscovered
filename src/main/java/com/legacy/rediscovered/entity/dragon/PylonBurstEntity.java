package com.legacy.rediscovered.entity.dragon;

import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.particles.PylonShieldBlastData;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.projectile.ProjectileUtil;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;

public class PylonBurstEntity extends Projectile
{
	@Nullable
	private Entity finalTarget;
	public int timeUntilBlast = 60;
	@Nullable
	private UUID targetId;

	public float speedAddition = 0.0F;

	public PylonBurstEntity(EntityType<? extends PylonBurstEntity> pEntityType, Level pLevel)
	{
		super(pEntityType, pLevel);
		this.noPhysics = true;
	}

	public PylonBurstEntity(Level pLevel, LivingEntity pShooter, Entity pFinalTarget)
	{
		this(RediscoveredEntityTypes.PYLON_BURST, pLevel);
		this.setOwner(pShooter);
		BlockPos blockpos = pShooter.blockPosition();
		double d0 = (double) blockpos.getX() + 0.5D;
		double d1 = (double) blockpos.getY() + 0.5D;
		double d2 = (double) blockpos.getZ() + 0.5D;
		this.moveTo(d0, d1, d2, this.getYRot(), this.getXRot());
		this.finalTarget = pFinalTarget;

		this.setNoGravity(true);
	}

	@Override
	public SoundSource getSoundSource()
	{
		return SoundSource.HOSTILE;
	}

	@Override
	protected void addAdditionalSaveData(CompoundTag pCompound)
	{
		super.addAdditionalSaveData(pCompound);

		if (this.finalTarget != null)
			pCompound.putUUID("Target", this.finalTarget.getUUID());

		pCompound.putInt("TimeUntilBlast", this.timeUntilBlast);
	}

	@Override
	protected void readAdditionalSaveData(CompoundTag pCompound)
	{
		super.readAdditionalSaveData(pCompound);

		this.timeUntilBlast = pCompound.getInt("TimeUntilBlast");

		if (pCompound.hasUUID("Target"))
			this.targetId = pCompound.getUUID("Target");
	}

	@Override
	protected void defineSynchedData()
	{
	}

	@Override
	public void checkDespawn()
	{
		if (this.level().getDifficulty() == Difficulty.PEACEFUL)
			this.discard();
	}

	@Override
	public void tick()
	{
		super.tick();

		if (!this.level().isClientSide())
		{
			ServerLevel serverLevel = (ServerLevel) this.level();

			if (this.finalTarget == null && this.targetId != null)
			{
				this.finalTarget = serverLevel.getEntity(this.targetId);

				if (this.finalTarget == null)
					this.targetId = null;
			}

			Player possibleNewTarget;
			if ((possibleNewTarget = this.level().getNearestPlayer(this, 30)) != null && possibleNewTarget != this.finalTarget && EntitySelector.NO_CREATIVE_OR_SPECTATOR.and(EntitySelector.LIVING_ENTITY_STILL_ALIVE).test(possibleNewTarget))
				this.finalTarget = possibleNewTarget;

			if (this.finalTarget == null || !this.finalTarget.isAlive() || this.finalTarget instanceof Player && this.finalTarget.isSpectator())
			{
				// this.finalTarget = this.level().getNearestPlayer(this, 30);
			}
			else
			{
				if (this.finalTarget != null)
				{
					boolean fresh = this.tickCount < (60 + (this.getUUID().hashCode() % 5 * 5));
					if (!fresh)
						this.setDeltaMovement(this.getDeltaMovement().scale(0.9F).add(this.finalTarget.position().subtract(this.position()).normalize().scale(0.04F + this.speedAddition)));

					if (this.distanceTo(this.finalTarget) <= 6)
					{
						this.playExplosionEffect(serverLevel);
						for (Entity entity : this.level().getEntities(this, this.getBoundingBox().inflate(5), EntitySelector.LIVING_ENTITY_STILL_ALIVE.and(EntitySelector.NO_CREATIVE_OR_SPECTATOR)))
						{
							if (entity instanceof LivingEntity living)
								this.onHit(new EntityHitResult(living));
						}
						this.discard();
					}
				}
			}

			HitResult hitresult = ProjectileUtil.getHitResultOnMoveVector(this, this::canHitEntity);
			if (hitresult.getType() != HitResult.Type.MISS && !net.neoforged.neoforge.event.EventHooks.onProjectileImpact(this, hitresult))
			{
				this.onHit(hitresult);
			}

			if (this.timeUntilBlast > 0)
			{
				--this.timeUntilBlast;
			}
			else
			{
				this.playExplosionEffect(serverLevel);
				this.discard();
			}
		}

		this.checkInsideBlocks();
		Vec3 vec31 = this.getDeltaMovement();
		this.setPos(this.getX() + vec31.x, this.getY() + vec31.y, this.getZ() + vec31.z);
		ProjectileUtil.rotateTowardsMovement(this, 0.5F);
		if (this.level().isClientSide())
		{
			if (this.random.nextFloat() < 0.5F)
			{
				float offset = 1.2F;
				this.level().addAlwaysVisibleParticle(ParticleTypes.END_ROD, true, this.getRandomX(offset), this.getRandomY(), this.getRandomZ(offset), 0.0D, 0.0D, 0.0D);
			}
		}

	}

	public void playExplosionEffect(ServerLevel serverLevel)
	{
		serverLevel.sendParticles(ParticleTypes.EXPLOSION, this.getX(), this.getY(), this.getZ(), 2, 0.2D, 0.2D, 0.2D, 0.0D);
		serverLevel.sendParticles(ParticleTypes.POOF, this.getX(), this.getY(), this.getZ(), 20, 0.2D, 0.2D, 0.2D, 0.4D);
		serverLevel.sendParticles(new PylonShieldBlastData(), this.getX(), this.getY(), this.getZ(), 1, 0.0D, 0.0D, 0.0D, 0.0D);

		this.playSound(RediscoveredSounds.ENTITY_PYLON_BURST_EXPLODE);
		this.playSound(SoundEvents.GENERIC_EXPLODE, 0.2F, 1.5F);
	}

	@Override
	protected boolean canHitEntity(Entity hit)
	{
		return super.canHitEntity(hit) && !hit.noPhysics && !hit.is(this.getOwner());
	}

	@Override
	public boolean isOnFire()
	{
		return false;
	}

	@Override
	public boolean shouldRenderAtSqrDistance(double pDistance)
	{
		return true;
	}

	@Override
	public float getLightLevelDependentMagicValue()
	{
		return 1.0F;
	}

	@Override
	protected void onHitEntity(EntityHitResult pResult)
	{
		super.onHitEntity(pResult);
		Entity entity = pResult.getEntity();
		Entity entity1 = this.getOwner();
		LivingEntity livingentity = entity1 instanceof LivingEntity ? (LivingEntity) entity1 : null;
		boolean flag = entity == entity1 ? false : entity.hurt(this.damageSources().explosion(this, livingentity), 8.0F);

		if (flag)
			this.doEnchantDamageEffects(livingentity, entity);
	}

	@Override
	protected void onHitBlock(BlockHitResult pResult)
	{
		super.onHitBlock(pResult);
		((ServerLevel) this.level()).sendParticles(ParticleTypes.EXPLOSION, this.getX(), this.getY(), this.getZ(), 2, 0.2D, 0.2D, 0.2D, 0.0D);
		this.playSound(RediscoveredSounds.ENTITY_PYLON_BURST_EXPLODE, 1.0F, 1.0F);
	}

	private void destroy()
	{
		this.discard();
		this.level().gameEvent(GameEvent.ENTITY_DAMAGE, this.position(), GameEvent.Context.of(this));
	}

	@Override
	protected void onHit(HitResult pResult)
	{
		super.onHit(pResult);
		this.destroy();
	}

	@Override
	public boolean isPickable()
	{
		return true;
	}

	@Override
	public boolean hurt(DamageSource pSource, float pAmount)
	{
		if (!this.level().isClientSide())
		{
			this.playSound(RediscoveredSounds.ENTITY_PYLON_BURST_DESTROYED, 1.0F, 1.0F);
			((ServerLevel) this.level()).sendParticles(ParticleTypes.CRIT, this.getX(), this.getY(), this.getZ(), 15, 0.2D, 0.2D, 0.2D, 0.0D);
			this.destroy();
		}

		return true;
	}
}
