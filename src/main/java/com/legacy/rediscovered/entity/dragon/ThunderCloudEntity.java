package com.legacy.rediscovered.entity.dragon;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.particles.ShockwaveData;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.util.LogicUtil;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.TraceableEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class ThunderCloudEntity extends Entity implements TraceableEntity
{
	protected static final EntityDataAccessor<Integer> DELAY = SynchedEntityData.<Integer>defineId(ThunderCloudEntity.class, EntityDataSerializers.INT);
	protected static final EntityDataAccessor<Integer> DESPAWN_DELAY = SynchedEntityData.<Integer>defineId(ThunderCloudEntity.class, EntityDataSerializers.INT);
	protected static final EntityDataAccessor<Float> STRIKE_DISTANCE = SynchedEntityData.<Float>defineId(ThunderCloudEntity.class, EntityDataSerializers.FLOAT);
	protected static final EntityDataAccessor<Integer> STRIKE_TIME = SynchedEntityData.<Integer>defineId(ThunderCloudEntity.class, EntityDataSerializers.INT);

	@Nullable
	private UUID ownerUUID;
	@Nullable
	private Entity cachedOwner;

	public final List<Vec3> renderOffsets;

	public ThunderCloudEntity(EntityType<? extends ThunderCloudEntity> pEntityType, Level pLevel)
	{
		super(pEntityType, pLevel);
		this.noPhysics = true;

		this.renderOffsets = new ArrayList<>();

		double distance = 1.0F;
		for (int a = 0; a < 8; ++a)
			this.renderOffsets.add(Vec3.ZERO.add(this.random.nextGaussian() * distance, this.random.nextGaussian() * distance, this.random.nextGaussian() * distance));

		this.noCulling = true; // To make the lightning still be visible
	}

	public ThunderCloudEntity(Level level, double pX, double pY, double pZ)
	{
		this(RediscoveredEntityTypes.THUNDER_CLOUD, level);
		this.setPos(pX, pY, pZ);
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(DELAY, 40);
		this.entityData.define(DESPAWN_DELAY, 20);

		this.entityData.define(STRIKE_DISTANCE, 0.0F);
		this.entityData.define(STRIKE_TIME, 0);
	}

	static final String DELAY_KEY = "Delay", DESPAWN_DELAY_KEY = "DespawnDelay", OWNER_KEY = "Owner",
			STRIKE_DISTANCE_KEY = "StrikeDistance", STRIKE_TIME_KEY = "StrikeTime";

	@Override
	protected void readAdditionalSaveData(CompoundTag tag)
	{
		this.setDelay(tag.getInt(DELAY_KEY));
		this.setDespawnDelay(tag.getInt(DESPAWN_DELAY_KEY));

		if (tag.hasUUID(OWNER_KEY))
		{
			this.ownerUUID = tag.getUUID(OWNER_KEY);
			this.cachedOwner = null;
		}

		this.setStrikeDistance(tag.getFloat(STRIKE_DISTANCE_KEY));
		this.setStrikeTime(tag.getInt(STRIKE_TIME_KEY));
	}

	@Override
	protected void addAdditionalSaveData(CompoundTag tag)
	{
		tag.putInt(DELAY_KEY, this.getDelay());
		tag.putInt(DESPAWN_DELAY_KEY, this.getDespawnDelay());

		if (this.ownerUUID != null)
			tag.putUUID(OWNER_KEY, this.ownerUUID);

		tag.putFloat(STRIKE_DISTANCE_KEY, this.getStrikeDistance());
		tag.putInt(STRIKE_TIME_KEY, this.getStrikeTime());
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.level().isClientSide())
		{
			float hRange = 2F;
			if (this.random.nextFloat() < 0.6F)
				this.level().addParticle(ParticleTypes.CLOUD, this.getX() + LogicUtil.withinRange(hRange, this.random), this.getY() + LogicUtil.withinRange(1.3F, this.random) + (this.getBbHeight() / 2), this.getZ() + LogicUtil.withinRange(hRange, this.random), LogicUtil.withinRange(0.1F, this.random), LogicUtil.withinRange(0.1F, this.random), LogicUtil.withinRange(0.1F, this.random));

			if (this.getDelay() > 0)
			{

				if (this.tickCount % 8 == 0)
				{
					float sRange = 0.5F;

					// TODO Post-Beta: Spark particle
					for (int i = 0; i < 20; ++i)
						this.level().addParticle(ParticleTypes.SMOKE, this.getX() + LogicUtil.withinRange(sRange, this.random), this.getY() - (this.random.nextFloat() * this.getStrikeDistance()), this.getZ() + LogicUtil.withinRange(sRange, this.random), LogicUtil.withinRange(0.1F, this.random), LogicUtil.withinRange(0.1F, this.random), LogicUtil.withinRange(0.1F, this.random));
				}

				if (this.getDelay() == 1)
				{
					// right when it is ready to strike
				}
			}
			else
			{
				// after it has struck
			}

			if (this.getDespawnDelay() == 1)
			{
				for (int i = 0; i < 25; ++i)
					this.level().addParticle(ParticleTypes.CLOUD, true, this.getX() + LogicUtil.withinRange(hRange, this.random), this.getY() + LogicUtil.withinRange(1.3F, this.random) + (this.getBbHeight() / 2), this.getZ() + LogicUtil.withinRange(hRange, this.random), LogicUtil.withinRange(0.5F, this.random), LogicUtil.withinRange(0.5F, this.random), LogicUtil.withinRange(0.5F, this.random));
			}
		}
		else
		{
			BlockPos offset = this.blockPosition();
			int height = this.level().getHeight(Types.WORLD_SURFACE, offset.getX(), offset.getZ());

			this.setStrikeDistance((float) (this.getY() - height));

			if (this.getDelay() > 0)
			{
				int oldDelay = this.getDelay();
				this.setDelay(this.getDelay() - 1);

				if (oldDelay > 0 && this.getDelay() == 0)
				{
					this.setStrikeTime(10);

					if (!this.isSilent())
					{
						this.level().playSound((Player) null, this.getX(), this.getY(), this.getZ(), SoundEvents.LIGHTNING_BOLT_THUNDER, this.getSoundSource(), 5.0F, 1.0F);
						this.level().playSound((Player) null, this.getX(), this.getY() - this.getStrikeDistance(), this.getZ(), SoundEvents.LIGHTNING_BOLT_IMPACT, this.getSoundSource(), 1.0F, 1.0F);
					}

					if (this.level()instanceof ServerLevel sl)
						sl.sendParticles(new ShockwaveData(), this.getX(), this.getY() - this.getStrikeDistance(), this.getZ(), 1, 0, 0, 0, 0);

					if (!this.level().isClientSide() && this.level().getGameRules().getBoolean(GameRules.RULE_DOFIRETICK))
					{
						BlockPos blockpos = this.blockPosition().atY(height);
						BlockState blockstate = BaseFireBlock.getState(this.level(), blockpos);

						if (this.level().getBlockState(blockpos).isAir() && blockstate.canSurvive(this.level(), blockpos))
							this.level().setBlockAndUpdate(blockpos, blockstate);

						for (int i = 0; i < 4; ++i)
						{
							BlockPos blockpos1 = blockpos.offset(this.random.nextInt(3) - 1, this.random.nextInt(3) - 1, this.random.nextInt(3) - 1);
							blockstate = BaseFireBlock.getState(this.level(), blockpos1);

							if (this.level().getBlockState(blockpos1).isAir() && blockstate.canSurvive(this.level(), blockpos1))
								this.level().setBlockAndUpdate(blockpos1, blockstate);
						}

					}
				}
			}
			else
			{
				if (this.getStrikeTime() > 0)
				{
					List<Entity> list1 = this.level().getEntities(this, AABB.unitCubeFromLowerCorner(this.position()).inflate(2F, this.getStrikeDistance(), 2F).move(-0.5F, -this.getStrikeDistance(), -0.5F), Entity::isAlive);

					for (Entity entity : list1)
					{
						if (entity != this.getOwner())
						{
							entity.setRemainingFireTicks(entity.getRemainingFireTicks() + 1);

							if (entity.getRemainingFireTicks() == 0)
								entity.setSecondsOnFire(8);

							entity.hurt(entity.damageSources().lightningBolt(), 10);
						}
					}

					this.setStrikeTime(this.getStrikeTime() - 1);
				}

				if (this.getDespawnDelay() == 0)
				{
					this.playSound(SoundEvents.WOOL_BREAK);
					this.discard();
				}
				else if (this.getDespawnDelay() > 0)
					this.setDespawnDelay(this.getDespawnDelay() - 1);
			}
		}
	}

	public int getDelay()
	{
		return this.entityData.get(DELAY);
	}

	public void setDelay(int delay)
	{
		this.entityData.set(DELAY, delay);
	}

	public int getDespawnDelay()
	{
		return this.entityData.get(DESPAWN_DELAY);
	}

	public void setDespawnDelay(int time)
	{
		this.entityData.set(DESPAWN_DELAY, time);
	}

	public float getStrikeDistance()
	{
		return this.entityData.get(STRIKE_DISTANCE);
	}

	public void setStrikeDistance(float dist)
	{
		this.entityData.set(STRIKE_DISTANCE, dist);
	}

	public int getStrikeTime()
	{
		return this.entityData.get(STRIKE_TIME);
	}

	public void setStrikeTime(int time)
	{
		this.entityData.set(STRIKE_TIME, time);
	}

	public void setOwner(@Nullable Entity pOwner)
	{
		if (pOwner != null)
		{
			this.ownerUUID = pOwner.getUUID();
			this.cachedOwner = pOwner;
		}
	}

	@Nullable
	public Entity getOwner()
	{
		if (this.cachedOwner != null && !this.cachedOwner.isRemoved())
			return this.cachedOwner;
		else if (this.ownerUUID != null && this.level()instanceof ServerLevel sl)
			return this.cachedOwner = sl.getEntity(this.ownerUUID);

		return null;
	}
}
