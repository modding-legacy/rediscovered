package com.legacy.rediscovered.entity.dragon;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.annotation.Nullable;

import org.joml.Vector3f;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.legacy.rediscovered.RediscoveredConfig;
import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.gui.DragonInventoryMenu;
import com.legacy.rediscovered.entity.util.animation.AnimData;
import com.legacy.rediscovered.entity.util.animation.IAnimated;
import com.legacy.rediscovered.item.DragonArmorItem;
import com.legacy.rediscovered.item.RubyFluteItem;
import com.legacy.rediscovered.network.PacketHandler;
import com.legacy.rediscovered.network.c_to_s.DragonFlightStatusPacket;
import com.legacy.rediscovered.network.c_to_s.DragonStaminaDecreasePacket;
import com.legacy.rediscovered.network.s_to_c.SendScreenShakePacket;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.legacy.rediscovered.registry.RediscoveredTriggers;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerListener;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.HasCustomInventoryScreen;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.Saddleable;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.LookControl;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.vehicle.DismountHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.NeoForgeMod;

public class RedDragonOffspringEntity extends AbstractRedDragonEntity implements IAnimated, HasCustomInventoryScreen, Saddleable, ContainerListener
{
	public static final String MOUNT_DRAGON_KEY = "gui.rediscovered.tooltip.mount_red_dragon";
	private static final AttributeModifier DRAGON_ARMOR_MODIFIER = new AttributeModifier(UUID.fromString("b8cfda89-c9a8-4eb9-9226-d5a47e770a41"), "Dragon Armor bonus", 20.0D, AttributeModifier.Operation.ADDITION);

	private static final EntityDataAccessor<Boolean> FLYING = SynchedEntityData.defineId(RedDragonOffspringEntity.class, EntityDataSerializers.BOOLEAN);
	private static final EntityDataAccessor<String> MOVEMENT_TYPE = SynchedEntityData.defineId(RedDragonOffspringEntity.class, EntityDataSerializers.STRING);
	private static final EntityDataAccessor<Boolean> SADDLED = SynchedEntityData.defineId(RedDragonOffspringEntity.class, EntityDataSerializers.BOOLEAN);
	private static final EntityDataAccessor<Float> STAMINA = SynchedEntityData.defineId(RedDragonOffspringEntity.class, EntityDataSerializers.FLOAT);

	private static final EntityDataAccessor<Boolean> TIRED = SynchedEntityData.defineId(RedDragonOffspringEntity.class, EntityDataSerializers.BOOLEAN);

	public final List<AnimData> anims = new ArrayList<>();
	public final AnimData flyingAnim = new AnimData(anims, 1F, true);
	public final AnimData hoverAnim = new AnimData(anims, 0.5F, true);
	public final AnimData fallingAnim = new AnimData(anims, 1F, true).serverControlledStops();
	public final AnimData reverseAnim = new AnimData(anims, 2F, true);

	public final AnimData sufferingAnim = new AnimData(anims, 0.2F, 0.5F, true);
	public final AnimData tiredAnim = new AnimData(anims, 0.5F, true);

	public int minimumLandTime, timeOffGround, dismountDelay = 30;

	public Vec3 fluteCallVec;
	protected SimpleContainer inventory;

	public int jumpTimer;
	private boolean wasRiderJumping;
	public float riderZza = 0;

	public float oStamina = 100;

	public RedDragonOffspringEntity(EntityType<? extends RedDragonOffspringEntity> type, Level level)
	{
		super(type, level);

		this.createInventory();
		this.lookControl = new LookControl(this)
		{
			@Override
			public void tick()
			{
				if (!hasControllingPassenger())
					super.tick();
			}
		};
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return AbstractRedDragonEntity.createDragonAttributes().add(NeoForgeMod.STEP_HEIGHT.value(), 1.0D).add(Attributes.MOVEMENT_SPEED, 0.7D);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new TravelToFluteCallGoal(this));
		this.goalSelector.addGoal(6, new DragonStrollGoal(this));
		this.goalSelector.addGoal(7, new DragonLookAtEntityGoal(this, Player.class, 10.0F));
		this.goalSelector.addGoal(8, new DragonLookAroundGoal(this));
	}

	@Override
	protected void reassessTameGoals()
	{
		super.reassessTameGoals();
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.getEntityData().define(FLYING, false);
		this.getEntityData().define(SADDLED, false);
		this.getEntityData().define(MOVEMENT_TYPE, MovementType.WANDER.getName());
		this.getEntityData().define(STAMINA, 100.0F);
		this.getEntityData().define(TIRED, false);
	}

	@Override
	public void onSyncedDataUpdated(EntityDataAccessor<?> pKey)
	{
		super.onSyncedDataUpdated(pKey);

		// reset the wing flap animation to a nice spot
		if (FLYING.equals(pKey))
		{
			if (this.isFlying())
			{
				this.animTime = this.prevAnimTime = 0F;
				this.playFlapSound();
			}
		}
	}

	static final String FLYING_KEY = "IsFlying", MOVEMENT_TYPE_KEY = "MovementType", SADDLE_KEY = "SaddleItem",
			ARMOR_KEY = "ArmorItem", STAMINA_KEY = "Stamina", TIRED_KEY = "Tired";

	@Override
	public void addAdditionalSaveData(CompoundTag tag)
	{
		super.addAdditionalSaveData(tag);

		tag.putBoolean(FLYING_KEY, this.isFlying());
		tag.putString(MOVEMENT_TYPE_KEY, this.getMovementType().getName());

		tag.putFloat(STAMINA_KEY, this.getStamina());
		tag.putBoolean(TIRED_KEY, this.isTired());

		ItemStack saddle = this.inventory.getItem(0), armor = this.inventory.getItem(1);

		// if (!saddle.isEmpty())
		tag.put(SADDLE_KEY, saddle.save(new CompoundTag()));

		// if (!armor.isEmpty())
		tag.put(ARMOR_KEY, armor.save(new CompoundTag()));
	}

	@Override
	public void readAdditionalSaveData(CompoundTag tag)
	{
		super.readAdditionalSaveData(tag);

		this.setFlying(tag.getBoolean(FLYING_KEY));
		this.setMovementType(MovementType.getFromName(tag.getString(MOVEMENT_TYPE_KEY)));

		this.setStamina(tag.getFloat(STAMINA_KEY));
		this.oStamina = this.getStamina();
		this.setTired(tag.getBoolean(TIRED_KEY));

		ItemStack saddle = ItemStack.of(tag.getCompound(SADDLE_KEY)), armor = ItemStack.of(tag.getCompound(ARMOR_KEY));
		// if (!saddle.isEmpty())
		this.inventory.setItem(0, saddle);

		// if (!armor.isEmpty())
		this.inventory.setItem(1, armor);

		this.updateContainerEquipment();
	}

	@Override
	public void tick()
	{
		super.tick();
		this.tickAnims(this);

		this.flyingAnim.play(this.isFlying());
		this.tiredAnim.play(this.isTired());
		this.sufferingAnim.play(this.isFlying() && this.getStamina() <= 20.0F);

		if (!this.onGround())
			this.dismountDelay = 15;
		else if (this.dismountDelay > 0)
			--this.dismountDelay;

		if (!this.level().isClientSide())
		{
			if (this.getRandom().nextInt(700) == 0 && this.deathTime == 0)
				this.heal(1.0F);

			if (this.getStamina() <= 0)
				this.setTired(true);
			else if (this.getStamina() > 50F)
				this.setTired(false);

			if (!RediscoveredConfig.WORLD.redDragonStamina())
				this.setStamina(100F);
			else if (this.onGround() && RediscoveredConfig.WORLD.redDragonStamina())
				this.setStamina(this.getStamina() + (this.isTired() ? 0.03F : 0.06F));

			if (this.getControllingPassenger() instanceof Player player)
			{
				this.riderZza = player.zza;
				this.setYHeadRot(player.getYHeadRot());
			}

			this.reverseAnim.play(this.zza < 0 || this.riderZza < 0);

			if (!this.hasControllingPassenger() && this.fluteCallVec == null)
				this.setFlying(this.getDragonTarget() != null || this.targetVec != null);
			if (this.getDragonTarget() != null && this.distanceTo(this.getDragonTarget()) <= 20 && this.minimumLandTime <= 0 && this.onGround())
			{
				this.setDragonTarget(null);
				this.setNewTarget();
			}
			this.setNoGravity(this.isFlying());

			this.hoverAnim.play(this.isFlying() && (this.hasControllingPassenger() && this.riderZza <= 0 || !this.hasControllingPassenger() && (this.targetVec == null && this.getDragonTarget() == null)));
		}
		else
		{
			this.fallingAnim.play(!this.isFlying() && !this.onGround() && timeOffGround >= 5);

			if (this.onGround() || this.isInWater())
				this.timeOffGround = 0;
			else if (this.tickCount > 80)
				++this.timeOffGround;

			if (this.jumpTimer > 0)
				--jumpTimer;
		}

		if (this.isFlying())
		{
			this.navigation.stop();
			this.setXRot(this.getXRot() * 0.8F);
		}
	}

	@Override
	protected void updateWalkAnimation(float pPartialTick)
	{
		float f = Math.min(pPartialTick * 4.0F, 1.0F) * (this.isTired() && this.isVehicle() ? 0.8F : 1);
		this.walkAnimation.update(this.reverseAnim.isPlaying() ? -f : f, 0.4F);
	}

	@Override
	protected boolean useNormalAI()
	{
		return !this.isFlying() || this.hasControllingPassenger();
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.minimumLandTime > 0)
			--this.minimumLandTime;
	}

	@Override
	public void customServerAiStep()
	{
		super.customServerAiStep();

		if (this.hasControllingPassenger() && this.isTame() && this.getFirstPassenger() instanceof Player)
		{
			this.setDragonTarget(null);
			this.targetVec = null;
		}
	}

	@Override
	protected void tickDeath()
	{
		super.tickDeath();
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		return super.hurt(source, amount);
	}

	@Override
	public void die(DamageSource pCause)
	{
		super.die(pCause);
	}

	@Override
	protected boolean shouldResetYawVelocity()
	{
		return this.isTame() && this.hasControllingPassenger();
	}

	@Override
	public boolean canRiderInteract()
	{
		return true;
	}

	@Override
	public boolean canBeLeashed(Player player)
	{
		return player == this.getOwner();
	}

	@Override
	protected double followLeashSpeed()
	{
		return 0.7D;
	}

	@Override
	protected boolean shouldStayCloseToLeashHolder()
	{
		return false;
	}

	public void summonFromFlute(Player player)
	{
		if (this.getOwner() == player)
			this.fluteCallVec = player.position();
	}

	@Override
	public boolean isFood(ItemStack pStack)
	{
		return false;
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		InteractionResult base = super.mobInteract(player, hand);
		ItemStack itemstack = player.getItemInHand(hand);

		if (!base.consumesAction())
		{
			if (player.getVehicle() != this && this.getOwner() == player)
			{
				if (player.isShiftKeyDown())
				{
					this.openCustomInventoryScreen(player);
					return InteractionResult.SUCCESS;
				}

				var food = itemstack.getFoodProperties(this);
				if (food != null && food.isMeat() && this.getHealth() < this.getMaxHealth())
				{
					this.heal(food.getNutrition());

					if (!player.getAbilities().instabuild)
						itemstack.shrink(1);

					this.gameEvent(GameEvent.EAT);
					return InteractionResult.SUCCESS;
				}

				if (itemstack.is(RediscoveredItems.ruby_flute) || itemstack.is(Items.LEAD))
					return InteractionResult.PASS;

				if (itemstack.getItem() instanceof DragonArmorItem)
				{
					if (!this.level().isClientSide())
					{
						if (!this.getArmor().isEmpty())
							player.setItemInHand(hand, this.getArmor());

						this.equipArmor(player, itemstack);
					}

					return InteractionResult.SUCCESS;
				}

				if (itemstack.is(Items.SADDLE) && this.getSaddle().isEmpty())
				{
					itemstack.interactLivingEntity(player, this, hand);
					return InteractionResult.SUCCESS;
				}
			}

			if (player.getVehicle() == this && itemstack.isEmpty())
			{
				player.stopRiding();

				if (!this.onGround() && player == this.getOwner())
				{
					this.minimumLandTime = 60;
					this.setDragonTarget(player);
				}

				return InteractionResult.SUCCESS;
			}
			else if (this.getOwner() != null && this.isTame() && (player == this.getOwner() || this.getPassengers().contains(this.getOwner())) && this.mount(player))
			{
				return InteractionResult.SUCCESS;
			}

			return InteractionResult.CONSUME;
		}

		return base;
	}

	public boolean mount(Player player)
	{
		if (!this.level().isClientSide() && !this.isBaby())
		{
			boolean mounted = !this.isBaby() && player.startRiding(this);

			if (mounted)
			{
				player.setShiftKeyDown(false);
				player.setYRot(this.getYRot());
				player.setYBodyRot(player.getYRot());
				this.setYHeadRot(this.getYRot());
			}

			return mounted;
		}

		return false;
	}

	/**
	 * Nearly a direct copy of the original, but with the added condition to
	 * put/keep the owner at the front
	 */
	@Override
	protected void addPassenger(Entity pPassenger)
	{
		if (pPassenger.getVehicle() != this)
		{
			throw new IllegalStateException("Use x.startRiding(y), not y.addPassenger(x)");
		}
		else
		{
			if (this.getPassengers().isEmpty())
			{
				this.passengers = ImmutableList.of(pPassenger);
			}
			else
			{
				List<Entity> list = Lists.newArrayList(this.getPassengers());
				if (!this.level().isClientSide() && pPassenger instanceof Player && (!(this.getFirstPassenger() instanceof Player) || this.getFirstPassenger() != this.getOwner()))
				{
					list.add(0, pPassenger);
				}
				else
				{
					list.add(pPassenger);
				}

				this.passengers = ImmutableList.copyOf(list);
			}

			this.gameEvent(GameEvent.ENTITY_MOUNT, pPassenger);
		}
	}

	@Override
	protected boolean canAddPassenger(Entity pPassenger)
	{
		return this.getPassengers().size() < 2;
	}

	@Override
	protected boolean canPathfind()
	{
		return super.canPathfind();
	}

	@Override
	protected void forceDragonHeadRot()
	{
		if (!this.isFlying())
		{

		}
		else if (!this.hasControllingPassenger())
			super.forceDragonHeadRot();
	}

	// getMaxHeadYRot

	float turnAnim;

	@Override
	public void travel(Vec3 pTravelVector)
	{
		// System.out.println(this.level());

		if (this.getControllingPassenger() instanceof Player player)
		{
			// Prevent from being in the overworld void by teleportation
			if (this.position().y < this.level().getMinBuildHeight() && this.level().dimension().equals(Level.OVERWORLD) && this.tickCount < 20)
			{
				RediscoveredMod.LOGGER.debug(player + " likely teleported into the overworld void while on the Red Dragon. Attempting to move to world height.");
				this.moveTo(this.position().x, this.level().getMaxBuildHeight(), this.position().z);
			}

			player.getAbilities().flying = false;

			if (this.isControlledByLocalInstance())
			{
				boolean jumping = player.jumping;

				float forward = player.zza;
				float strafe = player.xxa;

				this.riderZza = forward;

				if (forward > 0)
					forward = 1;
				else if (forward < 0)
					forward = -1;

				if (strafe > 0)
					strafe = 1;
				else if (strafe < 0)
					strafe = -1;
				strafe = strafe * forward;
				// strafe = 1.0F;
				// System.out.println(forward);
				if (forward <= 0.0F)
				{
					forward *= this.onGround() ? 0.5F : 0.25F;
				}

				if (!this.onGround() && !this.isFlying())
					forward *= 0.2F;

				if (this.isTired())
					forward *= 0.8F;

				if (RediscoveredConfig.WORLD.redDragonStamina() && this.getStamina() > 0 && this.isFlying())
				{
					float decrease = 0.02F;
					// decrease = 1.02F;
					decrease += 0.06F * (forward * (player.isShiftKeyDown() && !jumping ? 0.8F : 1));

					if (jumping)
						decrease += 0.04F;

					PacketHandler.sendToServer(new DragonStaminaDecreasePacket(this.getId(), (decrease * 0.8F)));
				}

				this.setStamina(Mth.clamp(this.getStamina(), 0, 100));

				this.turnAnim = Mth.rotLerp(0.15F, this.turnAnim, strafe);

				float rotDest = player.getYHeadRot() - ((this.turnAnim) * Mth.RAD_TO_DEG);
				this.setYRot((!this.isFlying() ? Mth.rotLerp(strafe != 0 ? 0.8F : 0.15F, this.getYRot(), rotDest) : rotDest));
				// this.yHeadRotO = this.getYHeadRot();
				this.setYHeadRot(player.getYHeadRot());
				this.yBodyRot = Mth.wrapDegrees((this.onGround() ? Mth.rotLerp(0.1F, this.yBodyRot, rotDest) : rotDest));
				this.yRotO = this.getYHeadRot();

				float leftRight = (float) -this.getLatencyPos(7, 1)[0] * Mth.DEG_TO_RAD;

				player.setYBodyRot(rotDest - leftRight);

				player.yBodyRot = rotDest - leftRight;

				float xRotDest = this.isFlying() && this.hoverAnim.isPlaying() ? player.getXRot() * 1.2F : 0;

				this.setXRot(Mth.rotLerp(0.15F, this.getXRot(), xRotDest));

				if (jumping && !this.isFlying() && this.onGround())
				{
					this.setOnGround(false);
					this.jumpFromGround();
					this.timeOffGround += 5;
				}

				boolean noStamina = RediscoveredConfig.WORLD.redDragonStamina() && (this.getStamina() <= 0 || this.isTired());
				boolean breakNextJump = false;

				if (!this.wasRiderJumping && jumping && !noStamina)
				{
					if (this.jumpTimer == 0)
						this.jumpTimer = 7;
					else
					{
						PacketHandler.sendToServer(new DragonFlightStatusPacket(this.getId(), !this.isFlying()));

						if (!this.isFlying())
							this.setDeltaMovement(this.getDeltaMovement().add(0, 1F, 0));

						this.jumpTimer = 0;
						this.wasRiderJumping = false;

						breakNextJump = true;
					}
				}
				else if (this.isFlying() && this.onGround() || noStamina)
					PacketHandler.sendToServer(new DragonFlightStatusPacket(this.getId(), false));

				float verticalSpeed = 0.03F;
				var movement = player.getViewVector(1.0F).with(Axis.Y, 0).normalize().scale(Mth.clamp(forward /*+ Mth.abs(strafe)*/, -1, 1)).yRot(strafe).scale(0.15F);

				if (this.isFlying())
				{
					this.setDeltaMovement(this.getDeltaMovement().add(movement).add(Vec3.ZERO.with(Axis.Y, jumping ? verticalSpeed : player.isShiftKeyDown() ? -verticalSpeed : 0)));

				}
				else
				{
					this.setDeltaMovement(this.getDeltaMovement().add(movement));
				}

				if (!breakNextJump)
					this.wasRiderJumping = jumping;

				super.travel(Vec3.ZERO);
			}
			else
			{
				this.setDeltaMovement(Vec3.ZERO);
			}

		}
		else
			super.travel(pTravelVector);

	}

	@Override
	public double getPassengersRidingOffset()
	{
		return super.getPassengersRidingOffset();
	}

	@Override
	protected Vector3f getPassengerAttachmentPoint(Entity passenger, EntityDimensions size, float scale)
	{
		return super.getPassengerAttachmentPoint(passenger, size, scale);
	}

	@Override
	public void positionRider(Entity passenger, MoveFunction pCallback)
	{
		if (this.hasPassenger(passenger))
		{
			int i = this.getPassengers().indexOf(passenger);

			boolean notDriver = i != 0;

			float fly = this.flyingAnim.getValue(1);
			float hover = this.hoverAnim.getValue(1);
			float hoverInv = 1.0F - hover;

			// in degrees, may need *10
			float upDown = (float) (this.getLatencyPos(5, 0)[1] - this.getLatencyPos(10, 1)[1]) * fly * hoverInv;

			// may need to be negative
			float leftRight = (((float) -this.getLatencyPos(7, 1)[0] * Mth.DEG_TO_RAD) * fly) - ((Mth.wrapDegrees(this.yBodyRot) * Mth.DEG_TO_RAD) * (1.0F - fly));

			float tilt = Mth.wrapDegrees((float) (this.getLatencyPos(5, 1)[0] - this.getLatencyPos(10, 1)[0])) * Mth.DEG_TO_RAD * fly;

			float mul = 1F + (2.0F * ((upDown) * -0.35F)) - (i * 1.2F);

			float x = (-Mth.sin(leftRight) * mul);
			float z = (Mth.cos(-leftRight) * mul);

			float y = (upDown * (upDown < 0 ? (notDriver ? -0.15F : 0.32F) : (notDriver ? 0.2F : 0.10F))) - (Mth.abs(tilt) * 0.1F);

			if (notDriver)
				y = -y;

			y += 3F - (i * 1.2F * hover);

			y -= (((Math.sin((double) (this.animTime * (Mth.PI * 2F) - 1.0F))) * 0.2F * fly) + (0.2F * (1.0F - fly)));
			if (!this.getSaddle().isEmpty())
				y += 0.1;
			pCallback.accept(passenger, this.getX() - x, this.getY() + y + passenger.getMyRidingOffset(this), this.getZ() + z);
		}
	}

	@Override
	public float getFlapSpeed(float motion)
	{
		float startPercent = 30, maxPercent = 5;
		float lowStaminaMul = Mth.clamp((-this.getStamina() + startPercent) * (1 / (startPercent - maxPercent)), 0.0F, 1.0F);

		return !this.isFlying() ? 0.0F : super.getFlapSpeed((!this.isControlledByLocalInstance() && this.hasControllingPassenger() ? (this.hoverAnim.isPlaying() ? 0.1F : 0.02F) : motion)) + ((this.hoverAnim.isPlaying() ? 0.02F : 0.1F) * lowStaminaMul);
	}

	@Override
	public LivingEntity getControllingPassenger()
	{
		return this.getFirstPassenger() instanceof LivingEntity living && this.isTame() && this.isSaddled() && this.getOwner() == living ? living : null;
	}

	public void setFlying(boolean flag)
	{
		this.getEntityData().set(FLYING, flag);
	}

	@Override
	public boolean isFlying()
	{
		return this.getEntityData().get(FLYING);
	}

	public void setStamina(float stamina)
	{
		this.oStamina = this.getStamina();
		this.getEntityData().set(STAMINA, stamina);
	}

	public float getStamina()
	{
		return this.getEntityData().get(STAMINA);
	}

	public void setTired(boolean flag)
	{
		this.getEntityData().set(TIRED, flag);
	}

	public boolean isTired()
	{
		return this.getEntityData().get(TIRED);
	}

	public void setMovementType(MovementType type)
	{
		this.getEntityData().set(MOVEMENT_TYPE, type.getName());
	}

	public MovementType getMovementType()
	{
		return MovementType.getFromName(this.getEntityData().get(MOVEMENT_TYPE));
	}

	public float lerpedStamina(float partialTick)
	{
		return Mth.clamp(Mth.lerp(partialTick, this.oStamina, this.getStamina()), 0.0F, 100.0F) / 100.0F;
	}

	@Override
	protected float getStandingEyeHeight(Pose pPose, EntityDimensions pDimensions)
	{
		return this.isBaby() ? 1.0F : 2F;
	}

	@Override
	protected void setNewTarget()
	{
		this.forceNewTarget = false;
		this.targetVec = null;
	}

	@Override
	public boolean isMultipartEntity()
	{
		return false;
	}

	@Override
	public List<AnimData> getAnimations()
	{
		return this.anims;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return !this.isFlying() && this.getRandom().nextFloat() < 0.95F || this.isTired() ? RediscoveredSounds.ENTITY_RED_DRAGON_IDLE_CALM : super.getAmbientSound();
	}

	@Override
	protected float getSoundVolume()
	{
		return this.isBaby() ? 2.0F : 4.0F;
	}

	@Override
	public int getAmbientSoundInterval()
	{
		return super.getAmbientSoundInterval() * (!this.isFlying() ? 3 : 5);
	}

	@Override
	protected void playStepSound(BlockPos pPos, BlockState pState)
	{
		if (!this.isFlying())
		{
			if (this.level() instanceof ServerLevel sl && !this.isBaby())
			{
				Vec3 centerPos = this.position();
				sl.players().forEach(player ->
				{
					if (player.getVehicle() == this)
						PacketHandler.sendToClient(new SendScreenShakePacket(0.03F), player);
					else if (player.onGround())
					{
						float dist = (float) centerPos.distanceTo(player.position());

						if (dist <= 30)
							PacketHandler.sendToClient(new SendScreenShakePacket(Math.min(0.8F, 1F / dist)), player);
					}
				});
			}

			this.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_STEP, this.isBaby() ? 0.6F : 2F, (this.isBaby() ? 1.2F : 0.8F) + this.random.nextFloat() * 0.2F);
		}
	}

	@Override
	protected float nextStep()
	{
		return (this.moveDist) + (this.isBaby() ? 0.5F : !this.hasControllingPassenger() ? 1.7F : 2F);
	}

	@Override
	public boolean causeFallDamage(float pFallDistance, float pMultiplier, DamageSource pSource)
	{
		if (this.level() instanceof ServerLevel sl && !this.isBaby())
		{
			Vec3 centerPos = this.position();
			sl.players().forEach(player ->
			{
				float d = Math.min(10, pFallDistance / 2);
				if (player.getVehicle() == this)
					PacketHandler.sendToClient(new SendScreenShakePacket(0.03F * d), player);
				else if (player.onGround())
				{
					float dist = (float) centerPos.distanceTo(player.position());

					if (dist <= 30)
						PacketHandler.sendToClient(new SendScreenShakePacket(Math.min(0.8F, 1F / dist) * d), player);
				}
			});
		}

		return false;
	}

	@Override
	protected void dropEquipment()
	{
		super.dropEquipment();
		if (this.inventory != null)
		{
			for (int i = 0; i < this.inventory.getContainerSize(); ++i)
			{
				ItemStack itemstack = this.inventory.getItem(i);
				if (!itemstack.isEmpty() && !EnchantmentHelper.hasVanishingCurse(itemstack))
					this.spawnAtLocation(itemstack);
			}
		}
	}

	/**
	 * Copied from Horse
	 */
	@Override
	public Vec3 getDismountLocationForPassenger(LivingEntity pLivingEntity)
	{
		Vec3 vec3 = getCollisionHorizontalEscapeVector((double) this.getBbWidth(), (double) pLivingEntity.getBbWidth(), this.getYRot() + (pLivingEntity.getMainArm() == HumanoidArm.RIGHT ? 90.0F : -90.0F));
		Vec3 vec31 = this.getDismountLocationInDirection(vec3, pLivingEntity);
		if (vec31 != null)
		{
			return vec31;
		}
		else
		{
			Vec3 vec32 = getCollisionHorizontalEscapeVector((double) this.getBbWidth(), (double) pLivingEntity.getBbWidth(), this.getYRot() + (pLivingEntity.getMainArm() == HumanoidArm.LEFT ? 90.0F : -90.0F));
			Vec3 vec33 = this.getDismountLocationInDirection(vec32, pLivingEntity);
			return vec33 != null ? vec33 : this.position();
		}
	}

	/**
	 * Copied from Horse
	 */
	@Nullable
	private Vec3 getDismountLocationInDirection(Vec3 pDirection, LivingEntity pPassenger)
	{
		double d0 = this.getX() + pDirection.x;
		double d1 = this.getBoundingBox().minY;
		double d2 = this.getZ() + pDirection.z;
		BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();

		for (Pose pose : pPassenger.getDismountPoses())
		{
			blockpos$mutableblockpos.set(d0, d1, d2);
			double d3 = this.getBoundingBox().maxY + 0.75D;

			while (true)
			{
				double d4 = this.level().getBlockFloorHeight(blockpos$mutableblockpos);
				if ((double) blockpos$mutableblockpos.getY() + d4 > d3)
				{
					break;
				}

				if (DismountHelper.isBlockFloorValid(d4))
				{
					AABB aabb = pPassenger.getLocalBoundsForPose(pose);
					Vec3 vec3 = new Vec3(d0, (double) blockpos$mutableblockpos.getY() + d4, d2);
					if (DismountHelper.canDismountTo(this.level(), pPassenger, aabb.move(vec3)))
					{
						pPassenger.setPose(pose);
						return vec3;
					}
				}

				blockpos$mutableblockpos.move(Direction.UP);
				if (!((double) blockpos$mutableblockpos.getY() < d3))
				{
					break;
				}
			}
		}

		return null;
	}

	@Override
	public void openCustomInventoryScreen(Player player)
	{
		if (player instanceof ServerPlayer sp)
		{
			sp.openMenu(new SimpleMenuProvider((id, inventory, playerIn) ->
			{
				return new DragonInventoryMenu(id, inventory, this);
			}, this.getName()), (buffer) -> buffer.writeInt(this.getId()));
		}
	}

	@Override
	public boolean isSaddleable()
	{
		return this.isAlive() && !this.isBaby() && this.isTame();
	}

	@Override
	public void equipSaddle(@Nullable SoundSource pSource)
	{
		if (this.isBaby())
			return;

		this.inventory.setItem(0, Items.SADDLE.getDefaultInstance());
	}

	public void equipArmor(@Nullable Player player, ItemStack armor)
	{
		if (this.isBaby())
			return;

		if (armor.getItem() instanceof DragonArmorItem)
		{
			this.inventory.setItem(1, armor.copyWithCount(1));
			if (player != null && !player.getAbilities().instabuild)
				armor.shrink(1);
		}
	}

	@Override
	public boolean isSaddled()
	{
		return this.entityData.get(SADDLED);
	}

	protected void createInventory()
	{
		SimpleContainer simplecontainer = this.inventory;
		this.inventory = new SimpleContainer(2);
		if (simplecontainer != null)
		{
			simplecontainer.removeListener(this);
			int i = Math.min(simplecontainer.getContainerSize(), this.inventory.getContainerSize());

			for (int j = 0; j < i; ++j)
			{
				ItemStack itemstack = simplecontainer.getItem(j);
				if (!itemstack.isEmpty())
				{
					this.inventory.setItem(j, itemstack.copy());
				}
			}
		}

		this.inventory.addListener(this);
		this.updateContainerEquipment();
	}

	protected void updateContainerEquipment()
	{
		if (!this.level().isClientSide())
		{
			this.entityData.set(SADDLED, !this.inventory.getItem(0).isEmpty());

			this.setItemSlot(EquipmentSlot.CHEST, this.inventory.getItem(1));
			this.setDropChance(EquipmentSlot.CHEST, 0.0F);
		}
	}

	public ItemStack getSaddle()
	{
		return this.inventory.getItem(0);
	}

	public ItemStack getArmor()
	{
		return this.getItemBySlot(EquipmentSlot.CHEST);
	}

	@Override
	public void containerChanged(Container pContainer)
	{
		boolean flag = this.isSaddled();
		ItemStack oldArmor = this.getArmor();

		this.getAttribute(Attributes.ARMOR).removeModifier(DRAGON_ARMOR_MODIFIER.getId());

		this.updateContainerEquipment();

		if (this.tickCount > 20 && !flag && this.isSaddled())
			this.playSound(this.getSaddleSoundEvent(), 1F, 1.0F);

		ItemStack newArmor = this.getArmor();

		if (newArmor.getItem() instanceof DragonArmorItem)
		{
			if (!this.getAttribute(Attributes.ARMOR).hasModifier(DRAGON_ARMOR_MODIFIER))
				this.getAttribute(Attributes.ARMOR).addTransientModifier(DRAGON_ARMOR_MODIFIER);

			if (this.tickCount > 20 && !oldArmor.equals(newArmor))
				this.playSound(RediscoveredSounds.ENTITY_RED_DRAGON_EQUIP_ARMOR, 1.0F, 1.0F);
		}
	}

	@Override
	public SoundEvent getSaddleSoundEvent()
	{
		return RediscoveredSounds.ENTITY_RED_DRAGON_EQUIP_SADDLE;
	}

	public Container getDragonInventory()
	{
		return this.inventory;
	}

	public static enum ControlType
	{
		MOUSE,
		KEYS;
	}

	private enum MovementType
	{
		WANDER(0),
		SIT(1),
		FOLLOW(2);

		// final int id;

		private MovementType(int id)
		{
			// this.id = id;
		}

		/*public int getId()
		{
			return this.id;
		}*/

		public String getName()
		{
			return this.name().toLowerCase(Locale.US);
		}

		public static MovementType getFromName(String name)
		{
			for (MovementType type : values())
				if (name == type.getName())
					return type;
			return MovementType.WANDER;
		}

		/*public static MovementType getFromId(int id)
		{
			for (MovementType type : values())
				if (id == type.id)
					return type;
			return MovementType.WANDER;
		}*/
	}

	protected boolean canUseGoals()
	{
		return !this.isFlying() && !this.hasControllingPassenger() && this.fluteCallVec == null;
	}

	@Override
	protected boolean shouldEnforcedFlyingDistanceLimit()
	{
		return this.fluteCallVec == null;
	}

	@Override
	public int getMaxFallDistance()
	{
		return this.fluteCallVec != null ? 30 : 10;
	}

	private class DragonStrollGoal extends WaterAvoidingRandomStrollGoal
	{
		public DragonStrollGoal(PathfinderMob pMob)
		{
			super(pMob, 0.4F, 1F);
		}

		@Override
		public boolean canUse()
		{
			return canUseGoals() && super.canUse();
		}

		@Override
		public boolean canContinueToUse()
		{
			return canUseGoals() && super.canContinueToUse();
		}
	}

	private class DragonLookAroundGoal extends RandomLookAroundGoal
	{
		public DragonLookAroundGoal(Mob pMob)
		{
			super(pMob);
		}

		@Override
		public boolean canUse()
		{
			return canUseGoals() && super.canUse();
		}

		@Override
		public boolean canContinueToUse()
		{
			return canUseGoals() && super.canContinueToUse();
		}
	}

	private class DragonLookAtEntityGoal extends LookAtPlayerGoal
	{
		public DragonLookAtEntityGoal(Mob pMob, Class<? extends LivingEntity> pLookAtType, float pLookDistance)
		{
			super(pMob, pLookAtType, pLookDistance, 0.1F);
		}

		@Override
		public boolean canUse()
		{
			return canUseGoals() && super.canUse();
		}

		@Override
		public boolean canContinueToUse()
		{
			return canUseGoals() && super.canContinueToUse();
		}
	}

	/**
	 * @formatter:off
	 * 
	 * TODO Red Dragon Flute Call extras. Post-Beta:
	 * - Excessive distance check (maybe)
	 * 
	 * @formatter:on
	 */
	private class TravelToFluteCallGoal extends Goal
	{
		private final RedDragonOffspringEntity mob;
		boolean reachedUpVec;
		int flightCooldown = 0, tickCount;

		Vec3 lastVec;

		int stuckChecks = 0;

		public TravelToFluteCallGoal(RedDragonOffspringEntity mob)
		{
			super();

			this.mob = mob;
		}

		@Override
		public boolean canUse()
		{
			return !this.mob.hasControllingPassenger() && this.mob.fluteCallVec != null && this.mob.getOwner() != null;
		}

		@Override
		public boolean canContinueToUse()
		{
			return !this.mob.hasControllingPassenger() && this.mob.fluteCallVec != null;
		}

		@Override
		public void start()
		{
			this.mob.navigation.stop();
			this.mob.targetVec = null;

			this.flightCooldown = 0;

			reachedUpVec = false;
			lastVec = null;
			this.stuckChecks = 0;
		}

		@Override
		public void tick()
		{
			Vec3 vec = this.mob.fluteCallVec;

			if (vec == null)
				return;

			boolean ownerNear = false;
			if (this.mob.getOwner() != null && this.mob.distanceTo(this.mob.getOwner()) <= 20)
			{
				this.mob.fluteCallVec = this.mob.getOwner().position();
				this.mob.getLookControl().setLookAt(this.mob.getOwner());
				ownerNear = true;

				if (this.mob.getOwner() instanceof ServerPlayer sp && this.mob.distanceTo(sp) <= 8 && this.mob.isSaddled() && !sp.onGround())
				{
					this.mob.mount(sp);
					RediscoveredTriggers.DRAGON_CATCH.get().trigger(sp);
				}
			}

			int groundHeight = this.mob.level().getHeight(Types.WORLD_SURFACE_WG, (int) this.mob.getX(), (int) this.mob.getZ());
			int groundHeightFromCall = this.mob.level().getHeight(Types.WORLD_SURFACE_WG, (int) this.mob.fluteCallVec.x(), (int) this.mob.fluteCallVec.z());

			if (this.distanceTo(vec) <= 30 && Mth.abs((float) (this.mob.getY() - groundHeight)) <= 15 && Mth.abs((float) (this.mob.fluteCallVec.y() - groundHeightFromCall)) <= 5)
			{
				if (this.mob.isFlying())
					this.mob.setDeltaMovement(Vec3.ZERO);
				flightCooldown = 20;
				this.mob.setFlying(false);

				if (this.mob.getDragonTarget() != null)
					this.mob.setDragonTarget(null);

				if (this.mob.targetVec != null)
					this.mob.targetVec = null;

				if (!ownerNear)
					this.mob.getLookControl().setLookAt(vec);

				if (this.distanceTo(vec) >= (ownerNear ? 12 : 8))
				{
					this.mob.getNavigation().moveTo(vec.x, vec.y, vec.z, 0.5F);
				}
				else
				{
					this.stop();
					return;
				}
			}
			else if (flightCooldown <= 0)
			{
				if (this.mob.isTired())
				{
					if (this.mob.getOwner() instanceof Player player)
						player.displayClientMessage(Component.translatable(RubyFluteItem.DRAGON_TIRED_KEY, this.mob.getDisplayName()), true);
					this.stop();
					return;
				}

				if (!this.mob.isFlying() && this.mob.onGround())
					this.mob.setDeltaMovement(this.mob.getDeltaMovement().add(0, 2, 0));

				this.mob.setFlying(true);

				Vec3 upVec = vec.add(0, 20, 0);

				double dist = this.distanceTo(new Vec3(upVec.x(), this.mob.getY(), upVec.z()));

				if (this.mob.getY() < vec.y())
				{
					this.mob.targetVec = new Vec3(this.mob.getX(), upVec.y(), this.mob.getZ());
				}
				else if (this.mob.horizontalCollision && this.mob.level().canSeeSkyFromBelowWater(this.mob.blockPosition()))
				{
					this.mob.targetVec = new Vec3(this.mob.getX(), this.mob.getY() + 10, this.mob.getZ());
				}
				else if (!this.reachedUpVec && dist > 20)
				{
					this.mob.targetVec = upVec;
				}
				else
				{
					this.mob.targetVec = vec;
					this.reachedUpVec = true;
				}

				if (this.stuckChecks >= 10)
				{
					if (this.mob.getOwner() instanceof Player player)
						player.displayClientMessage(Component.translatable(RubyFluteItem.DRAGON_STUCK_KEY, this.mob.getDisplayName()), true);
					this.stop();
					return;
				}

				/*if (this.lastVec != null)
					System.out.println(this.distanceTo(this.lastVec) + " " + this.stuckChecks);*/
				if (this.lastVec != null)
				{
					if (this.distanceTo(this.lastVec) <= 5)
						this.stuckChecks++;
					else if (this.stuckChecks > 0)
						this.stuckChecks--;
				}
			}

			if (flightCooldown > 0)
				--flightCooldown;

			if (this.tickCount % 10 == 0)
				this.lastVec = this.mob.position();

			++this.tickCount;
		}

		@Override
		public void stop()
		{
			this.reachedUpVec = false;
			this.mob.fluteCallVec = null;
			this.mob.targetVec = null;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return super.requiresUpdateEveryTick();
		}

		private double distanceTo(Vec3 vec)
		{
			return Math.sqrt(this.mob.distanceToSqr(vec));
		}
	}
}
