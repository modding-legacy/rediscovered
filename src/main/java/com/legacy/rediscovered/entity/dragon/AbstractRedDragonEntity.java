package com.legacy.rediscovered.entity.dragon;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.entity.util.RedDragonEntityPart;
import com.legacy.rediscovered.registry.RediscoveredBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.OwnableEntity;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.entity.PartEntity;

public abstract class AbstractRedDragonEntity extends TamableAnimal
{
	public Vec3 targetVec;

	public double[][] positions = new double[64][3];
	public int posPointer = -1;

	protected final @Nullable PartEntity<AbstractRedDragonEntity>[] subEntities;
	protected RedDragonEntityPart head, neck, body, tail1, tail2, tail3, rightWing, leftWing;

	public float prevAnimTime = 0.0F;
	public float animTime = 0.0F;
	public boolean forceNewTarget = false;
	public boolean angry = false;
	public boolean renderTailSpike = false;
	public boolean inWall = false;
	private Entity target;

	public float yRotA = 0;
	public BlockPos homePos;

	public AbstractRedDragonEntity(EntityType<? extends AbstractRedDragonEntity> type, Level level)
	{
		super(type, level);

		this.setHealth(this.getMaxHealth());

		if (this.isMultipartEntity())
		{
			this.head = new RedDragonEntityPart(this, "head", 1.0F, 1.0F);
			this.neck = new RedDragonEntityPart(this, "neck", 3.0F, 3.0F);
			this.body = new RedDragonEntityPart(this, "body", 5.0F, 3.0F);
			this.tail1 = new RedDragonEntityPart(this, "tail1", 2.0F, 2.0F);
			this.tail2 = new RedDragonEntityPart(this, "tail2", 2.0F, 2.0F);
			this.tail3 = new RedDragonEntityPart(this, "tail3", 2.0F, 2.0F);
			this.rightWing = new RedDragonEntityPart(this, "right_wing", 4.0F, 2.0F);
			this.leftWing = new RedDragonEntityPart(this, "left_wing", 4.0F, 2.0F);
			this.subEntities = new RedDragonEntityPart[] { this.head, this.neck, this.body, this.tail1, this.tail2, this.tail3, this.rightWing, this.leftWing };

			this.setId(ENTITY_COUNTER.getAndAdd(this.subEntities.length + 1) + 1);
		}
		else
			this.subEntities = new RedDragonEntityPart[] {};

		this.noCulling = true;

		// 16.0F, 8.0F
	}

	@Override
	public void setId(int pId)
	{
		super.setId(pId);

		if (this.isMultipartEntity())
		{
			for (int i = 0; i < this.subEntities.length; i++)
				this.subEntities[i].setId(pId + i + 1);
		}
	}

	public static AttributeSupplier.Builder createDragonAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 100.0D).add(Attributes.KNOCKBACK_RESISTANCE, 0.5D);
	}

	@Override
	protected void registerGoals()
	{
	}

	@Override
	public boolean causeFallDamage(float pFallDistance, float pMultiplier, DamageSource pSource)
	{
		return false;
	}

	public double[] getLatencyPos(int bufferIndexOffset, float partialTicks)
	{
		// fixes the jittering during death animation if it has one
		if (this.isDeadOrDying() && !this.canPathfind())
			partialTicks = 0.0F;

		partialTicks = 1.0F - partialTicks;
		int i = this.posPointer - bufferIndexOffset & 63;
		int j = this.posPointer - bufferIndexOffset - 1 & 63;
		double[] adouble = new double[3];
		double d0 = this.positions[i][0];
		double d1 = Mth.wrapDegrees(this.positions[j][0] - d0);
		adouble[0] = d0 + d1 * (double) partialTicks;
		d0 = this.positions[i][1];
		d1 = this.positions[j][1] - d0;
		adouble[1] = d0 + d1 * (double) partialTicks;
		adouble[2] = Mth.lerp((double) partialTicks, this.positions[i][2], this.positions[j][2]);
		return adouble;
	}

	protected boolean useNormalAI()
	{
		return false;
	}

	protected void playFlapSound()
	{
		if (!this.isSilent())
			this.level().playLocalSound(this.getX(), this.getY(), this.getZ(), RediscoveredSounds.ENTITY_RED_DRAGON_FLAP, this.getSoundSource(), this.getSoundVolume(), 0.8F + this.random.nextFloat() * 0.3F, false);
	}

	public boolean isFlying()
	{
		return true;
	}

	@SuppressWarnings("unused")
	@Override
	public void aiStep()
	{
		if (this.useNormalAI())
		{
			// this.lerpSteps = 4;
			super.aiStep();
		}
		else
		{
			if (this.isEffectiveAi() && this.isAlive())
			{
				this.level().getProfiler().push("sensing");
				this.getSensing().tick();
				this.level().getProfiler().pop();
				int i = this.level().getServer().getTickCount() + this.getId();
				if (i % 2 != 0 && this.tickCount > 1)
				{
					this.level().getProfiler().push("targetSelector");
					this.targetSelector.tickRunningGoals(false);
					this.level().getProfiler().pop();
					this.level().getProfiler().push("goalSelector");
					this.goalSelector.tickRunningGoals(false);
					this.level().getProfiler().pop();
				}
				else
				{
					this.level().getProfiler().push("targetSelector");
					this.targetSelector.tick();
					this.level().getProfiler().pop();
					this.level().getProfiler().push("goalSelector");
					this.goalSelector.tick();
					this.level().getProfiler().pop();
				}
			}
		}

		float wingSin;
		float wingSinOld;

		if (this.level().isClientSide())
		{
			wingSin = Mth.cos(this.animTime * Mth.PI * 2.0F);
			wingSinOld = Mth.cos(this.prevAnimTime * Mth.PI * 2.0F);

			if (wingSinOld <= -0.3F && wingSin >= -0.3F)
				this.playFlapSound();
		}

		this.prevAnimTime = this.animTime;

		if (this.canPathfind())
		{
			Vec3 vec34 = this.getDeltaMovement();
			float f9 = 0.2F / ((float) vec34.horizontalDistance() * 10.0F + 1.0F);
			f9 *= (float) Math.pow(2.0D, vec34.y);

			// boolean sitting = f9 > 0.1F;

			this.animTime += this.getFlapSpeed(f9);

			this.setYRot(Mth.wrapDegrees(this.getYRot()));

			if (this.isNoAi())
			{
				this.animTime = 0.5F;
			}
			else
			{
				if (this.posPointer < 0)
				{
					for (int i = 0; i < this.positions.length; ++i)
					{
						this.positions[i][0] = (double) this.getYRot();
						this.positions[i][1] = this.getY();
					}
				}

				if (++this.posPointer == this.positions.length)
				{
					this.posPointer = 0;
				}

				this.positions[this.posPointer][0] = (double) this.getYRot();
				this.positions[this.posPointer][1] = this.getY();

				if (this.level().isClientSide())
				{
					if (this.lerpSteps > 0 && !this.useNormalAI())
					{
						double d7 = this.getX() + (this.lerpX - this.getX()) / (double) this.lerpSteps;
						double d0 = this.getY() + (this.lerpY - this.getY()) / (double) this.lerpSteps;
						double d1 = this.getZ() + (this.lerpZ - this.getZ()) / (double) this.lerpSteps;
						double d2 = Mth.wrapDegrees(this.lerpYRot - (double) this.getYRot());
						this.setYRot((float) ((double) this.getYRot() + d2 / (double) this.lerpSteps));
						this.setXRot((float) ((double) this.getXRot() + (this.lerpXRot - (double) this.getXRot()) / (double) this.lerpSteps));
						--this.lerpSteps;
						this.setPos(d7, d0, d1);
						this.setRot(this.getYRot(), this.getXRot());
					}
				}
				else
				{
					Vec3 vec3 = this.targetVec;

					if (vec3 != null)
					{
						double d7 = vec3.x - this.getX();
						double d8 = vec3.y - this.getY();
						double d9 = vec3.z - this.getZ();
						double d3 = d7 * d7 + d8 * d8 + d9 * d9;
						float flySpeed = this instanceof OwnableEntity ? 25 : 10;
						double d4 = Math.sqrt(d7 * d7 + d9 * d9);

						if (d4 > 0.0D)
							d8 = Mth.clamp(d8 / d4, (double) (-flySpeed), (double) flySpeed);

						this.setDeltaMovement(this.getDeltaMovement().add(0.0D, d8 * 0.01D, 0.0D));
						this.setYRot(Mth.wrapDegrees(this.getYRot()));
						Vec3 vec31 = vec3.subtract(this.getX(), this.getY(), this.getZ()).normalize();
						Vec3 vec32 = (new Vec3((double) Mth.sin(this.getYRot() * Mth.DEG_TO_RAD), this.getDeltaMovement().y, (double) (-Mth.cos(this.getYRot() * Mth.DEG_TO_RAD)))).normalize();
						float f5 = Math.max(((float) vec32.dot(vec31) + 0.5F) / 1.5F, 0.0F);

						if (Math.abs(d7) > (double) 1.0E-5F || Math.abs(d9) > (double) 1.0E-5F)
						{
							float rotDest = -((float) Mth.atan2(d7, d9)) * Mth.RAD_TO_DEG;
							float maxDelta = 50.0F;
							float diff = Mth.degreesDifference(this.getYRot(), rotDest);
							float addition = Mth.clamp(diff, -maxDelta, maxDelta);

							float newDegrees = Mth.rotLerp(this.getTurnSpeed() * 0.5F, this.getYRot(), Mth.wrapDegrees(this.getYRot() + addition));

							// this.getYRot() + this.yRotA * 0.1F
							this.setYRot(Mth.wrapDegrees(newDegrees));
						}

						float f19 = (float) (2.0D / (d3 + 1.0D));
						this.moveRelative(0.06F * (f5 * f19 + (1.0F - f19)), new Vec3(0.0D, 0.0D, -1.0D));

						this.move(MoverType.SELF, this.getDeltaMovement().scale(this.inWall ? 0.8F : 1).multiply(-1, 1, -1));

						Vec3 vec33 = this.getDeltaMovement().normalize();
						double d5 = 0.8D + 0.15D * (vec33.dot(vec32) + 1.0D) / 2.0D;
						this.setDeltaMovement(this.getDeltaMovement().multiply(d5, (double) 0.91F, d5));

						double d101 = this.targetVec.x() - this.getX();
						double d0 = this.targetVec.y() - this.getY();
						double d1 = this.targetVec.z() - this.getZ();
						double d2 = d101 * d101 + d0 * d0 + d1 * d1;

						// System.out.println("3");

						// Won't fly a distance larger than 150 blocks at most
						if (this.forceNewTarget || (this.shouldEnforcedFlyingDistanceLimit() && (d2 < 100.0D || d2 > 22500.0D))/* || this.horizontalCollision || this.verticalCollision*/)
						{
							// System.out.println("4");

							this.targetVec = null;
							this.forceNewTarget = false;
							this.setNewTarget();
						}
					}

					if (this.getPassengers().isEmpty())
					{
						if (this.getDragonTarget() != null)
						{
							this.targetVec = this.getDragonTarget().position();
							double d31 = this.targetVec.x() - this.getX();
							double d51 = this.targetVec.z() - this.getZ();
							double d7 = Math.sqrt(d31 * d31 + d51 * d51);
							double d81 = 0.4000000059604645D + d7 / 80.0D - 1.0D;

							if (d81 > 10.0D)
							{
								d81 = 10.0D;
							}

							// System.out.println("1");
							this.targetVec = this.targetVec.with(Axis.Y, this.getDragonTarget().getY() - 2);
							// this.targetY = this.getDragonTarget().getBoundingBox().minY + d81;
						}
						else if (this.targetVec != null)
						{
							// System.out.println("2");

							this.targetVec = this.targetVec.add(this.random.nextGaussian() * 2.0D, 0, this.random.nextGaussian() * 2.0D);
						}
					}
				}
				// this.yBodyRotO = this.yBodyRot;

				if (!this.isVehicle() && this.isFlying())
					this.yBodyRot = this.getYRot();

				Vec3[] avec3 = this.subEntities != null && this.subEntities.length > 0 ? new Vec3[this.subEntities.length] : new Vec3[0];

				for (int j = 0; j < this.subEntities.length; ++j)
				{
					avec3[j] = new Vec3(this.subEntities[j].getX(), this.subEntities[j].getY(), this.subEntities[j].getZ());
				}

				float f12 = (float) (this.getLatencyPos(5, 1.0F)[1] - this.getLatencyPos(10, 1.0F)[1]) * 10.0F * Mth.DEG_TO_RAD;
				float f13 = Mth.cos(f12);
				float f = Mth.sin(f12);
				float f14 = this.getYRot() * Mth.DEG_TO_RAD;
				float f1 = Mth.sin(f14);
				float f15 = Mth.cos(f14);
				this.tickPart(this.body, (double) (f1 * 0.5F), 0.0D, (double) (-f15 * 0.5F));
				this.tickPart(this.rightWing, (double) (f15 * 4.5F), 2.0D, (double) (f1 * 4.5F));
				this.tickPart(this.leftWing, (double) (f15 * -4.5F), 2.0D, (double) (f1 * -4.5F));
				if (!this.level().isClientSide() && this.hurtTime == 0 && this.isMultipartEntity())
				{
					this.knockBack(this.level().getEntities(this, this.rightWing.getBoundingBox().inflate(4.0D, 2.0D, 4.0D).move(0.0D, -2.0D, 0.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
					this.knockBack(this.level().getEntities(this, this.leftWing.getBoundingBox().inflate(4.0D, 2.0D, 4.0D).move(0.0D, -2.0D, 0.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
					this.hurt(this.level().getEntities(this, this.head.getBoundingBox().inflate(1.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
					this.hurt(this.level().getEntities(this, this.neck.getBoundingBox().inflate(1.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
				}

				float f2 = Mth.sin(this.getYRot() * Mth.DEG_TO_RAD - this.yRotA * 0.01F);
				float f16 = Mth.cos(this.getYRot() * Mth.DEG_TO_RAD - this.yRotA * 0.01F);
				float f3 = this.getHeadYOffset();
				this.tickPart(this.head, (double) (f2 * 6.5F * f13), (double) (f3 + f * 6.5F), (double) (-f16 * 6.5F * f13));
				this.tickPart(this.neck, (double) (f2 * 5.5F * f13), (double) (f3 + f * 5.5F), (double) (-f16 * 5.5F * f13));
				double[] adouble = this.getLatencyPos(5, 1.0F);

				for (int k = 0; k < 3; ++k)
				{
					RedDragonEntityPart tailPiece = null;
					if (k == 0)
						tailPiece = this.tail1;

					if (k == 1)
						tailPiece = this.tail2;

					if (k == 2)
						tailPiece = this.tail3;

					double[] adouble1 = this.getLatencyPos(12 + k * 2, 1.0F);
					float f17 = this.getYRot() * Mth.DEG_TO_RAD + (float) Mth.wrapDegrees(adouble1[0] - adouble[0]) * Mth.DEG_TO_RAD;
					float f18 = Mth.sin(f17);
					float f20 = Mth.cos(f17);
					float f21 = 1.5F;
					float f22 = (float) (k + 1) * 2.0F;
					this.tickPart(tailPiece, (double) (-(f1 * f21 + f18 * f22) * f13), adouble1[1] - adouble[1] - (double) ((f22 + f21) * f) + f21, (double) ((f15 * f21 + f20 * f22) * f13));
				}

				if (!this.level().isClientSide())
				{
					this.inWall = this.head != null && this.destroyBlocksInAABB(this.head.getBoundingBox()) || this.body != null && this.destroyBlocksInAABB(this.body.getBoundingBox());
				}

				for (int l = 0; l < this.subEntities.length; ++l)
				{
					this.subEntities[l].xo = avec3[l].x;
					this.subEntities[l].yo = avec3[l].y;
					this.subEntities[l].zo = avec3[l].z;
					this.subEntities[l].xOld = avec3[l].x;
					this.subEntities[l].yOld = avec3[l].y;
					this.subEntities[l].zOld = avec3[l].z;
				}

				// System.out.println(this.getLatencyPos(5, 1.0F)[1]);
				/*for (int l = 0; l < this.dragonParts.length; ++l)
				{
					this.dragonParts[l].xo = avec3d[l].x;
					this.dragonParts[l].yo = avec3d[l].y;
					this.dragonParts[l].zo = avec3d[l].z;
				}*/
			}

			float amount = (float) Math.sin((float) tickCount / 200) * 0.1F;
			/*this.positions[10][1] = -amount;
			this.positions[5][1] = amount;
			
			System.out.println(amount);*/

			if (this.isFlying())
				this.setYBodyRot(this.getYRot());

			this.forceDragonHeadRot();
		}

	}

	protected boolean shouldEnforcedFlyingDistanceLimit()
	{
		return true;
	}

	protected void forceDragonHeadRot()
	{
		this.setYHeadRot(this.getYRot());
	}

	public float getFlapSpeed(float motion)
	{
		if (this.isDeadOrDying())
			return 0.13F;
		else if (motion > 0.1F)
			return 0.1F;
		else if (this.inWall)
			return motion * 0.5F;
		else
			return motion;
	}

	private void tickPart(RedDragonEntityPart pPart, double pOffsetX, double pOffsetY, double pOffsetZ)
	{
		if (this.isMultipartEntity() && pPart != null)
			pPart.setPos(this.getX() + -pOffsetX, this.getY() + pOffsetY, this.getZ() + -pOffsetZ);
	}

	private float getHeadYOffset()
	{
		/*if (this.phaseManager.getCurrentPhase().isSitting())
		{
			return -1.0F;
		}
		else*/
		{
			double[] adouble = this.getLatencyPos(5, 1.0F);
			double[] adouble1 = this.getLatencyPos(0, 1.0F);
			return (float) (adouble[1] - adouble1[1]);
		}
	}

	protected boolean canPathfind()
	{
		return !this.isDeadOrDying();
	}

	public float getTurnSpeed()
	{
		float f = (float) this.getDeltaMovement().horizontalDistance() + 1.0F;
		float f1 = Math.min(f, 40.0F);
		return 0.7F / f1 / f;
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	public double getPassengersRidingOffset()
	{
		double offset = this.getDeltaMovement().y() < 0 ? this.getDeltaMovement().y() - this.getDeltaMovement().y() * 2 : this.getDeltaMovement().y();
		return Math.min(3.3D, 3 + offset);
	}

	private void knockBack(List<Entity> pEntities)
	{
		double d0 = (this.body.getBoundingBox().minX + this.body.getBoundingBox().maxX) / 2.0D;
		double d1 = (this.body.getBoundingBox().minZ + this.body.getBoundingBox().maxZ) / 2.0D;

		for (Entity entity : pEntities)
		{
			if (entity instanceof LivingEntity living)
			{
				double d2 = entity.getX() - d0;
				double d3 = entity.getZ() - d1;
				double d4 = Math.max(d2 * d2 + d3 * d3, 0.1D);
				entity.push(d2 / d4 * 4.0D, (double) 0.2F, d3 / d4 * 4.0D);
				if (/*!this.phaseManager.getCurrentPhase().isSitting() && */living.getLastHurtByMobTimestamp() < entity.tickCount - 2)
				{
					entity.hurt(this.damageSources().mobAttack(this), 5.0F);
					this.doEnchantDamageEffects(this, entity);
				}
			}
		}

	}

	private void hurt(List<Entity> pEntities)
	{
		for (Entity entity : pEntities)
		{
			if (entity instanceof LivingEntity)
			{
				entity.hurt(this.damageSources().mobAttack(this), 10.0F);
				this.doEnchantDamageEffects(this, entity);
			}
		}

	}

	protected void setNewTarget()
	{
		boolean flag = false;

		do
		{
			Vec3 startVec = this.wanderCenterVector();
			this.targetVec = new Vec3(startVec.x() + (double) ((this.random.nextFloat() * this.maxHorizontalWanderDistance()) - (this.random.nextFloat() * this.maxHorizontalWanderDistance())), startVec.y() + this.verticalWanderDistance(), startVec.z() + (double) ((this.random.nextFloat() * this.maxHorizontalWanderDistance()) - (this.random.nextFloat() * this.maxHorizontalWanderDistance())));

			Vec3 vec = this.position();

			double d0 = vec.x() - this.targetVec.x();
			double d1 = vec.y() - this.targetVec.y();
			double d2 = vec.z() - this.targetVec.z();
			flag = d0 * d0 + d1 * d1 + d2 * d2 > 100.0D;
		}
		while (!flag);

		this.setDragonTarget(null);
	}

	public float maxHorizontalWanderDistance()
	{
		return 120.0F;
	}

	public float verticalWanderDistance()
	{
		return this.random.nextFloat() * 50.0F;
	}

	public Vec3 wanderCenterVector()
	{
		return this.homePos != null ? Vec3.atCenterOf(this.homePos).add(0, 30, 0) : new Vec3(this.getX(), 70, this.getZ());
	}

	@Override
	public void positionRider(Entity passenger, Entity.MoveFunction pCallback)
	{
		super.positionRider(passenger, pCallback);
		/*if (this.hasPassenger(passenger))
		{
			pCallback.accept(passenger, this.getX(), this.getY() + this.getPassengersRidingOffset() + passenger.getMyRidingOffset(), this.getZ());
		}*/
	}

	private boolean destroyBlocksInAABB(AABB bounds)
	{
		boolean flag = false;
		boolean flag1 = false;

		if (this.level().collidesWithSuffocatingBlock(this, bounds))
			flag = true;

		if (flag1)
		{
			double d1 = bounds.minX + (bounds.maxX - bounds.minX) * (double) this.random.nextFloat();
			double d2 = bounds.minY + (bounds.maxY - bounds.minY) * (double) this.random.nextFloat();
			double d0 = bounds.minZ + (bounds.maxZ - bounds.minZ) * (double) this.random.nextFloat();
			this.level().addParticle(ParticleTypes.EXPLOSION, d1, d2, d0, 0.0D, 0.0D, 0.0D);
		}

		return flag;
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (!this.getPassengers().isEmpty() && this.getPassengers().contains(source.getEntity()))
		{
			return false;
		}
		else
		{
			return super.hurt(source, amount);
		}
	}

	@Override
	public void die(DamageSource pCause)
	{
		super.die(pCause);
	}

	@Override
	protected void tickDeath()
	{
		super.tickDeath();
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return false;
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		return true;
	}

	@Override
	public @Nullable PartEntity<AbstractRedDragonEntity>[] getParts()
	{
		return this.subEntities;
	}

	@Override
	public boolean isMultipartEntity()
	{
		return true;
	}

	@Override
	public boolean isPickable()
	{
		return !this.isMultipartEntity();
	}

	@Override
	public float getWalkTargetValue(BlockPos pos)
	{
		return this.level().getMaxLocalRawBrightness(pos) - 0.5F;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return RediscoveredSounds.ENTITY_RED_DRAGON_IDLE;
	}

	@Override
	public void playAmbientSound()
	{
		SoundEvent soundevent = this.getAmbientSound();
		if (soundevent != null)
			this.level().playSound(null, this, soundevent, this.getSoundSource(), this.getSoundVolume(), this.getVoicePitch());
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return RediscoveredSounds.ENTITY_RED_DRAGON_HURT;
	}

	@Override
	protected float getSoundVolume()
	{
		return 6.0F;
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel level, AgeableMob ageable)
	{
		return null;
	}

	public Entity getDragonTarget()
	{
		return target;
	}

	public void setDragonTarget(Entity target)
	{
		if (this.level().isClientSide())
			return;

		this.target = target;
	}

	@Override
	public void kill()
	{
		this.discard();
	}

	protected boolean shouldResetYawVelocity()
	{
		return false;
	}

	@Override
	public boolean canBeLeashed(Player pPlayer)
	{
		return false;
	}

	@Override
	public ItemStack getPickedResult(HitResult target)
	{
		return RediscoveredBlocks.red_dragon_egg.asItem().getDefaultInstance();
	}
}
