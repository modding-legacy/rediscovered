package com.legacy.rediscovered.entity.dragon;

import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.particles.ShockwaveData;
import com.legacy.rediscovered.entity.util.RedDragonEntityPart;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.projectile.ProjectileUtil;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;

public class BoltBallEntity extends Projectile
{
	@Nullable
	private Entity finalTarget;
	public int timeUntilBlast = 60;
	@Nullable
	private UUID targetId;

	/**
	 * Used client side only, not saved
	 */
	private long seed = 0L;

	public BoltBallEntity(EntityType<? extends BoltBallEntity> type, Level level)
	{
		super(type, level);
		this.noPhysics = true;
		if (level.isClientSide())
			this.seed = this.random.nextLong();
	}

	public BoltBallEntity(Level level, LivingEntity shooter, Entity finalTarget)
	{
		this(RediscoveredEntityTypes.BOLT_BALL, level);
		this.setOwner(shooter);
		BlockPos blockpos = shooter.blockPosition();
		double d0 = (double) blockpos.getX() + 0.5D;
		double d1 = (double) blockpos.getY() + 0.5D;
		double d2 = (double) blockpos.getZ() + 0.5D;
		this.moveTo(d0, d1, d2, this.getYRot(), this.getXRot());
		this.finalTarget = finalTarget;

		this.setNoGravity(true);
	}

	@Override
	public SoundSource getSoundSource()
	{
		return SoundSource.HOSTILE;
	}

	@Override
	protected void addAdditionalSaveData(CompoundTag pCompound)
	{
		super.addAdditionalSaveData(pCompound);

		if (this.finalTarget != null)
			pCompound.putUUID("Target", this.finalTarget.getUUID());

		pCompound.putInt("TimeUntilBlast", this.timeUntilBlast);
	}

	@Override
	protected void readAdditionalSaveData(CompoundTag pCompound)
	{
		super.readAdditionalSaveData(pCompound);

		this.timeUntilBlast = pCompound.getInt("TimeUntilBlast");

		if (pCompound.hasUUID("Target"))
			this.targetId = pCompound.getUUID("Target");
	}

	@Override
	protected void defineSynchedData()
	{
	}

	@Override
	public void checkDespawn()
	{
		if (this.level().getDifficulty() == Difficulty.PEACEFUL)
			this.discard();
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.level()instanceof ServerLevel level)
		{
			if (this.finalTarget == null && this.targetId != null)
			{
				this.finalTarget = level.getEntity(this.targetId);

				if (this.finalTarget == null)
					this.targetId = null;
			}

			if (this.finalTarget == null || !this.finalTarget.isAlive() || this.finalTarget instanceof Player && this.finalTarget.isSpectator())
			{
				// this.finalTarget = this.level().getNearestPlayer(this, 30);
			}
			else
			{
				if (this.finalTarget != null)
				{
					this.setDeltaMovement(this.getDeltaMovement().scale(0.9).add(this.finalTarget.position().subtract(this.position()).normalize().scale(0.3F)));

					/*if (this.distanceTo(this.finalTarget) <= 6)
					{
						((ServerLevel) this.level()).sendParticles(ParticleTypes.EXPLOSION, this.getX(), this.getY(), this.getZ(), 2, 0.2D, 0.2D, 0.2D, 0.0D);
						((ServerLevel) this.level()).sendParticles(ParticleTypes.POOF, this.getX(), this.getY(), this.getZ(), 20, 0.2D, 0.2D, 0.2D, 0.4D);
					
						this.playSound(SoundEvents.SHULKER_BULLET_HIT);
						this.playSound(SoundEvents.GENERIC_EXPLODE, 0.2F, 1.5F);
					
						for (Entity entity : this.level().getEntities(this, this.getBoundingBox().inflate(5), EntitySelector.LIVING_ENTITY_STILL_ALIVE.and(EntitySelector.NO_CREATIVE_OR_SPECTATOR)))
						{
							if (entity instanceof LivingEntity living)
								this.onHit(new EntityHitResult(living));
						}
					}*/
				}
			}

			HitResult hitresult = ProjectileUtil.getHitResultOnMoveVector(this, this::canHitEntity);
			if (hitresult.getType() != HitResult.Type.MISS && !net.neoforged.neoforge.event.EventHooks.onProjectileImpact(this, hitresult))
			{
				this.onHit(hitresult);
			}

			if (this.timeUntilBlast > 0)
			{
				--this.timeUntilBlast;
			}
			else
			{
				((ServerLevel) this.level()).sendParticles(ParticleTypes.POOF, this.getX(), this.getY(), this.getZ(), 20, 0.2D, 0.2D, 0.2D, 0.4D);
				((ServerLevel) this.level()).sendParticles(new ShockwaveData(), this.getX(), this.getY(), this.getZ(), 1, 0, 0, 0, 0);

				this.playSound(SoundEvents.SHULKER_BULLET_HIT);
				this.playSound(SoundEvents.GENERIC_EXPLODE, 0.2F, 1.5F);
				this.discard();
			}
		}

		this.checkInsideBlocks();
		Vec3 vec31 = this.getDeltaMovement();
		this.setPos(this.getX() + vec31.x, this.getY() + vec31.y, this.getZ() + vec31.z);
		ProjectileUtil.rotateTowardsMovement(this, 0.5F);
		if (this.level().isClientSide())
		{
			if (random.nextFloat() < 0.85F)
				this.setSeed(this.random.nextLong());

			// for (int i = 0; i < 2; ++i)
			{
				// this.level().addParticle(ParticleTypes.POOF, this.getRandomX(0.5D),
				// this.getRandomY(), this.getRandomZ(0.5D), 0.0D, 0.0D, 0.0D);
			}
		}

	}

	@Override
	protected boolean canHitEntity(Entity hit)
	{
		return super.canHitEntity(hit) && !hit.noPhysics && !hit.is(this.getOwner());
	}

	@Override
	public boolean isOnFire()
	{
		return false;
	}

	@Override
	public boolean shouldRenderAtSqrDistance(double pDistance)
	{
		return true;
	}

	@Override
	public float getLightLevelDependentMagicValue()
	{
		return 1.0F;
	}

	@Override
	protected void onHitEntity(EntityHitResult pResult)
	{
		super.onHitEntity(pResult);
		Entity entity = pResult.getEntity();
		Entity entity1 = this.getOwner();
		LivingEntity livingentity = entity1 instanceof LivingEntity ? (LivingEntity) entity1 : null;

		boolean isDragon = entity instanceof RedDragonBossEntity || entity instanceof RedDragonEntityPart part && part.dragon instanceof RedDragonBossEntity;
		boolean flag = entity == entity1 ? false : entity.hurt(this.damageSources().explosion(this, livingentity), isDragon ? 40 : 8.0F);
		if (flag)
		{
			this.doEnchantDamageEffects(livingentity, entity);
			if (isDragon)
			{
				LightningBolt bolt = EntityType.LIGHTNING_BOLT.create(this.level());
				bolt.setVisualOnly(true);
				bolt.setPos(this.position());
				this.level().addFreshEntity(bolt);

				// LivingEntity livingentity1 = (LivingEntity) entity;
				/*livingentity1.addEffect(new MobEffectInstance(MobEffects.LEVITATION, 200), MoreObjects.firstNonNull(entity1, this));*/
			}
		}

	}

	@Override
	protected void onHitBlock(BlockHitResult pResult)
	{
		super.onHitBlock(pResult);
		this.playSound(SoundEvents.SHULKER_BULLET_HIT, 1.0F, 1.0F);
	}

	private void destroy()
	{
		((ServerLevel) this.level()).sendParticles(ParticleTypes.POOF, this.getX(), this.getY(), this.getZ(), 20, 0.2D, 0.2D, 0.2D, 0.4D);
		((ServerLevel) this.level()).sendParticles(new ShockwaveData(), this.getX(), this.getY(), this.getZ(), 1, 0, 0, 0, 0);

		this.discard();
		this.level().gameEvent(GameEvent.ENTITY_DAMAGE, this.position(), GameEvent.Context.of(this));
	}

	@Override
	protected void onHit(HitResult pResult)
	{
		super.onHit(pResult);
		this.destroy();
	}

	@Override
	public boolean isPickable()
	{
		return true;
	}

	@Override
	public boolean hurt(DamageSource pSource, float pAmount)
	{
		if (this.isInvulnerableTo(pSource))
		{
			return false;
		}
		else
		{
			this.markHurt();
			Entity entity = pSource.getEntity();
			if (entity != null && entity != this.getOwner())
			{
				if (!this.level().isClientSide())
				{
					// this.playSound(SoundEvents., 1.0F, 1.0F);

					Vec3 vec3 = entity.getLookAngle();
					this.setDeltaMovement(vec3);

					this.finalTarget = this.getOwner();
					this.setOwner(entity);
				}

				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public long getSeed()
	{
		return seed;
	}

	public void setSeed(long newSeed)
	{
		this.seed = newSeed;
	}
}
