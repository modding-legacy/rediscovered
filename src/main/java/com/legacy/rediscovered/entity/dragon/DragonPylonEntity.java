package com.legacy.rediscovered.entity.dragon;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.particles.PylonShieldBlastData;
import com.legacy.rediscovered.mixin.ClientLevelAccessor;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredPoiTypes;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.Vec3;

public class DragonPylonEntity extends LivingEntity
{
	private static final EntityDataAccessor<Integer> LAYERS = SynchedEntityData.defineId(DragonPylonEntity.class, EntityDataSerializers.INT);
	private static final EntityDataAccessor<Optional<UUID>> ATTACHED_DRAGON_UUID = SynchedEntityData.<Optional<UUID>>defineId(DragonPylonEntity.class, EntityDataSerializers.OPTIONAL_UUID);

	public List<BlockPos> miniPylons = new ArrayList<>();
	public int time;
	public boolean forceLayerUpdate;

	public DragonPylonEntity(EntityType<? extends DragonPylonEntity> pEntityType, Level pLevel)
	{
		super(pEntityType, pLevel);
		this.blocksBuilding = true;
		this.time = this.random.nextInt(100000);
	}

	public DragonPylonEntity(Level pLevel, double pX, double pY, double pZ)
	{
		this(RediscoveredEntityTypes.DRAGON_PYLON, pLevel);
		this.setPos(pX, pY, pZ);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return LivingEntity.createLivingAttributes().add(Attributes.MAX_HEALTH, 30.0D).add(Attributes.KNOCKBACK_RESISTANCE, 100.0D);
	}

	@Override
	protected Entity.MovementEmission getMovementEmission()
	{
		return Entity.MovementEmission.NONE;
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.getEntityData().define(LAYERS, 0);
		this.getEntityData().define(ATTACHED_DRAGON_UUID, Optional.empty());
	}

	@Override
	public void tick()
	{
		super.tick();
		++this.time;
		if (this.level()instanceof ServerLevel sl)
		{
			if (this.getAttachedDragonUUID().isEmpty() && this.tickCount % 40 == 0)
			{
				Optional<UUID> uuid = sl.<RedDragonBossEntity>getEntitiesOfClass(RedDragonBossEntity.class, this.getBoundingBox().inflate(70.0D, 80.0D, 70.0D)).stream().filter(e -> !e.isRemoved()).findFirst().map(e -> e.getUUID());

				if (uuid.isPresent())
					this.setAttachedDragonUUID(uuid.get());
			}

			// if (this.getLayerCount() > 0)
			{
				RedDragonBossEntity dragon = this.getAttachedDragon();
				if (dragon != null)
				{
					dragon.setPylonTime(20);
				}
			}

			if (this.tickCount < 10 || this.tickCount % (20 * 5) == 0 || this.forceLayerUpdate)
			{
				int radius = 45;
				BlockPos startPos = this.blockPosition().below(15);
				PoiManager pointofinterestmanager = sl.getPoiManager();
				pointofinterestmanager.ensureLoadedAndValid(sl, startPos, radius);

				this.miniPylons = sl.getPoiManager().getInSquare((poiType) -> poiType.is(RediscoveredPoiTypes.MINI_DRAGON_PYLON.getKey()), startPos, radius, PoiManager.Occupancy.ANY).map(record -> record.getPos()).toList();

				int currentCount = this.getLayerCount();

				this.setLayerCount(Math.min(this.miniPylons.size(), 4));

				if (this.getLayerCount() < currentCount)
				{
					this.playSound(RediscoveredSounds.ENTITY_DRAGON_PYLON_SHIELD_LOST, 2, 1);
					for (ServerPlayer player : sl.players())
						sl.sendParticles(player, new PylonShieldBlastData(30, 5, 50, 1.0F, 0.0F), true, this.getX(), this.getY() + (this.getBbHeight() / 2), this.getZ(), 1, 0.0, 0.0, 0.0, 0.0);
				}

				this.forceLayerUpdate = false;
			}
		}
	}

	static final String DRAGON_KEY = "AttachedDragonUUID", LAYERS_KEY = "Layers";

	@Override
	public void addAdditionalSaveData(CompoundTag pCompound)
	{
		if (this.getAttachedDragonUUID().isPresent())
			pCompound.putUUID(DRAGON_KEY, this.getAttachedDragonUUID().get());

		pCompound.putInt(LAYERS_KEY, this.getLayerCount());
	}

	@Override
	public void readAdditionalSaveData(CompoundTag pCompound)
	{
		if (pCompound.hasUUID(DRAGON_KEY))
			this.setAttachedDragonUUID(pCompound.getUUID(DRAGON_KEY));

		this.setLayerCount(pCompound.getInt(LAYERS_KEY));
	}

	@Override
	public boolean isPickable()
	{
		return true;
	}

	@Override
	public PushReaction getPistonPushReaction()
	{
		return PushReaction.IGNORE;
	}

	@Override
	public boolean hurt(DamageSource pSource, float pAmount)
	{
		if (this.isInvulnerableTo(pSource))
			return false;
		else if (pSource.getEntity() instanceof RedDragonBossEntity)
			return false;
		else if (this.getLayerCount() <= 0 && (pSource.getEntity() instanceof Player || pSource.is(DamageTypeTags.BYPASSES_INVULNERABILITY)))
			return super.hurt(pSource, pAmount);

		return false;
	}

	@Override
	public void kill()
	{
		this.setLayerCount(0);
		super.kill();
	}

	public void setLayerCount(int layers)
	{
		this.getEntityData().set(LAYERS, layers);
	}

	public int getLayerCount()
	{
		return this.getEntityData().get(LAYERS);
	}

	public Optional<UUID> getAttachedDragonUUID()
	{
		return this.entityData.get(ATTACHED_DRAGON_UUID);
	}

	public void setAttachedDragonUUID(UUID id)
	{
		this.entityData.set(ATTACHED_DRAGON_UUID, Optional.of(id));
	}

	@Nullable
	private RedDragonBossEntity clientDragonCache;

	@Nullable
	public RedDragonBossEntity getAttachedDragon()
	{
		if (this.getAttachedDragonUUID().isPresent())
		{
			if (this.level()instanceof ServerLevel level)
				return (RedDragonBossEntity) level.getEntity(this.getAttachedDragonUUID().get());
			else if (this.level()instanceof ClientLevel level)
			{
				if (this.clientDragonCache != null && this.clientDragonCache.isRemoved())
					this.clientDragonCache = null;

				if (this.clientDragonCache == null)
					this.clientDragonCache = (RedDragonBossEntity) ((ClientLevelAccessor) level).invokeGetEntities().get(this.getAttachedDragonUUID().get());

				return this.clientDragonCache;
			}
		}

		return null;
	}

	@Override
	public boolean shouldRenderAtSqrDistance(double pDistance)
	{
		return super.shouldRenderAtSqrDistance(pDistance) || this.getAttachedDragonUUID().isPresent();
	}

	@Override
	public Iterable<ItemStack> getHandSlots()
	{
		return List.of();
	}

	@Override
	public Iterable<ItemStack> getArmorSlots()
	{
		return List.of();
	}

	@Override
	public ItemStack getItemBySlot(EquipmentSlot pSlot)
	{
		return ItemStack.EMPTY;
	}

	@Override
	public void setItemSlot(EquipmentSlot pSlot, ItemStack pStack)
	{
	}

	@Override
	public HumanoidArm getMainArm()
	{
		return HumanoidArm.RIGHT;
	}

	@Override
	public void knockback(double strength, double x, double z)
	{
	}

	@Override
	public void push(double x, double y, double z)
	{
	}

	@Override
	public void move(MoverType pType, Vec3 pPos)
	{
	}

	@Override
	public boolean canBeAffected(MobEffectInstance effect)
	{
		return effect.getEffect() == MobEffects.GLOWING;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource pDamageSource)
	{
		return RediscoveredSounds.ENTITY_DRAGON_PYLON_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_DRAGON_PYLON_DEATH;
	}
}