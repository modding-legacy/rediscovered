package com.legacy.rediscovered.entity;

import com.legacy.rediscovered.client.RediscoveredSounds;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;

public class BeastBoyEntity extends AbstractSteveEntity
{
	public BeastBoyEntity(EntityType<? extends BeastBoyEntity> type, Level world)
	{
		super(type, world);
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return RediscoveredSounds.ENTITY_BEAST_BOY_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_BEAST_BOY_DEATH;
	}

	@Override
	public float getVoicePitch()
	{
		return super.getVoicePitch() + 0.3F;
	}
}