package com.legacy.rediscovered.entity;

import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;
import com.legacy.rediscovered.entity.pigman.PigmanEntity;
import com.legacy.rediscovered.entity.pigman.data.PigmanData;
import com.legacy.rediscovered.entity.util.IPigman;
import com.legacy.rediscovered.entity.util.IPigmanDataHolder;
import com.legacy.rediscovered.registry.RediscoveredDataSerialization;
import com.legacy.rediscovered.registry.RediscoveredTriggers;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.Dynamic;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.ZombifiedPiglin;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.trading.MerchantOffers;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.neoforge.event.EventHooks;

public class ZombiePigmanEntity extends ZombifiedPiglin implements IPigmanDataHolder
{
	protected static final EntityDataAccessor<PigmanData> PIGMAN_DATA = SynchedEntityData.defineId(ZombiePigmanEntity.class, RediscoveredDataSerialization.PIGMAN_DATA);
	private static final EntityDataAccessor<Boolean> CONVERTING = SynchedEntityData.defineId(ZombiePigmanEntity.class, EntityDataSerializers.BOOLEAN);
	private static final EntityDataAccessor<Byte> CONVERSION_TYPE = SynchedEntityData.defineId(ZombiePigmanEntity.class, EntityDataSerializers.BYTE);

	private int conversionTime;
	private UUID converstionStarter;

	@Nullable
	public CompoundTag tradeOffers;
	public int traderXp;

	public ZombiePigmanEntity(EntityType<? extends ZombiePigmanEntity> type, Level level)
	{
		super(type, level);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(CONVERTING, false);
		this.entityData.define(CONVERSION_TYPE, (byte) 0);
		this.entityData.define(PIGMAN_DATA, new PigmanData(PigmanData.Profession.NONE, 1));
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource random, DifficultyInstance difficulty)
	{
		if (this.getConversionType() > 0)
			return;

		this.setItemSlot(EquipmentSlot.MAINHAND, Items.IRON_SWORD.getDefaultInstance());
	}

	@Override
	protected void populateDefaultEquipmentEnchantments(RandomSource random, DifficultyInstance difficulty)
	{
		if (this.getConversionType() > 0)
			return;

		super.populateDefaultEquipmentEnchantments(random, difficulty);
	}

	@Override
	protected void customServerAiStep()
	{
		super.customServerAiStep();

		if (!this.level().isClientSide() && this.isAlive() && this.isConverting() && this.level()instanceof ServerLevel serverLevel)
		{
			int i = this.getConversionProgress();
			this.conversionTime -= i;

			if (this.conversionTime <= 0)
				this.finishCure(serverLevel);
		}
	}

	private static final String CONVERSION_PLAYER_KEY = "ConversionPlayer", CONVERSION_TIME_KEY = "ConversionTime",
			CONVERSION_TYPE_KEY = "ConversionType", TRADER_XP_KEY = "Xp", TRADER_OFFERS_KEY = "Offers";

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		PigmanData.CODEC.encodeStart(NbtOps.INSTANCE, this.getPigmanData()).resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent((data) -> compound.put(PIGMAN_DATA_KEY, data));

		if (this.tradeOffers != null)
			compound.put(TRADER_OFFERS_KEY, this.tradeOffers);

		compound.putInt(TRADER_XP_KEY, this.traderXp);

		compound.putByte(CONVERSION_TYPE_KEY, this.getConversionType());
		compound.putInt(CONVERSION_TIME_KEY, this.isConverting() ? this.conversionTime : -1);

		if (this.converstionStarter != null)
			compound.putUUID(CONVERSION_PLAYER_KEY, this.converstionStarter);

	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);

		if (compound.contains(PIGMAN_DATA_KEY, 10))
		{
			DataResult<PigmanData> dataresult = PigmanData.CODEC.parse(new Dynamic<>(NbtOps.INSTANCE, compound.get(PIGMAN_DATA_KEY)));
			dataresult.resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent(this::setPigmanData);
		}

		if (compound.contains(TRADER_OFFERS_KEY, 10))
			this.tradeOffers = compound.getCompound(TRADER_OFFERS_KEY);

		if (compound.contains(TRADER_XP_KEY, 3))
			this.traderXp = compound.getInt(TRADER_XP_KEY);

		this.setConversionType(compound.getByte(CONVERSION_TYPE_KEY));

		if (compound.contains(CONVERSION_TIME_KEY, 99) && compound.getInt(CONVERSION_TIME_KEY) > -1)
			this.startConverting(compound.hasUUID(CONVERSION_PLAYER_KEY) ? compound.getUUID(CONVERSION_PLAYER_KEY) : null, compound.getInt(CONVERSION_TIME_KEY));
	}

	@Override
	protected InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack playerItem = player.getItemInHand(hand);

		if (this.hasEffect(MobEffects.WEAKNESS) && playerItem.is(Items.GOLDEN_CARROT))
		{
			if (this.level().isClientSide())
				return InteractionResult.SUCCESS;

			this.startConverting(player.getUUID(), this.level().getRandom().nextInt(2400) + 3600);

			if (!this.isSilent())
				this.playSound(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_CURE, 1.0F + this.level().getRandom().nextFloat(), this.level().getRandom().nextFloat() * 0.7F + 0.3F);

			if (!player.isCreative())
				playerItem.shrink(1);

			// This is so later when the player decides its specialty, it doesn't drop the
			// sword it spawns with
			if (this.getEquipmentDropChance(EquipmentSlot.MAINHAND) < 1.0F)
				this.setDropChance(EquipmentSlot.MAINHAND, 0.0F);

			return InteractionResult.SUCCESS;
		}
		else if (this.isConverting() && this.isApplicableConversionItem(playerItem))
		{
			if (this.level().isClientSide())
				return InteractionResult.SUCCESS;

			if (this.trySetConversionType(playerItem))
			{
				this.setItemSlotAndDropWhenKilled(EquipmentSlot.MAINHAND, playerItem.copyWithCount(1));

				if (!player.isCreative())
					playerItem.shrink(1);
			}

			return InteractionResult.SUCCESS;
		}

		return super.mobInteract(player, hand);
	}

	public boolean isConverting()
	{
		return this.getEntityData().get(CONVERTING);
	}

	public int getConversionTime()
	{
		return conversionTime;
	}

	public int getConversionProgress()
	{
		int i = 1;

		if (this.level().getRandom().nextFloat() < 0.01F)
		{
			int j = 0;
			BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();

			for (int k = (int) this.getX() - 4; k < (int) this.getX() + 4 && j < 14; ++k)
			{
				for (int l = (int) this.getY() - 4; l < (int) this.getY() + 4 && j < 14; ++l)
				{
					for (int i1 = (int) this.getZ() - 4; i1 < (int) this.getZ() + 4 && j < 14; ++i1)
					{
						Block block = this.level().getBlockState(blockpos$mutableblockpos.set(k, l, i1)).getBlock();

						if (block == Blocks.IRON_BARS || block instanceof BedBlock)
						{
							if (this.level().getRandom().nextFloat() < 0.3F)
								++i;

							++j;
						}
					}
				}
			}
		}

		return i;
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return IPigman.Type.get(this.getConversionType()) == IPigman.Type.UNASSIGNED;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return this.isAngry() ? RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_ANGRY : RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource pDamageSource)
	{
		return RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_DEATH;
	}

	@Override
	public void playAngerSound()
	{
		this.playSound(RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_ANGRY, this.getSoundVolume() * 2.0F, this.getVoicePitch() * 1.8F);
	}

	public void setPigmanData(PigmanData data)
	{
		PigmanData currentData = this.getPigmanData();

		if (currentData.getProfession() != data.getProfession())
			this.tradeOffers = null;

		this.entityData.set(PIGMAN_DATA, data);
	}

	public PigmanData getPigmanData()
	{
		return this.entityData.get(PIGMAN_DATA);
	}

	public void setConversionType(byte type)
	{
		this.entityData.set(CONVERSION_TYPE, type);
	}

	public byte getConversionType()
	{
		return this.entityData.get(CONVERSION_TYPE);
	}

	public void startConverting(@Nullable UUID conversionStarter, int conversionTime)
	{
		this.setCanPickUpLoot(true);
		this.converstionStarter = conversionStarter;
		this.conversionTime = conversionTime;
		this.getEntityData().set(CONVERTING, true);
		this.removeEffect(MobEffects.WEAKNESS);
		this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_BOOST, conversionTime, Math.min(this.level().getDifficulty().getId() - 1, 0)));
	}

	@SuppressWarnings("deprecation")
	private <P extends Mob & IPigman> void finishCure(ServerLevel level)
	{
		byte typeId = this.getConversionType();
		var wow = IPigman.Type.get(typeId);
		RediscoveredMod.LOGGER.debug("Zombie Pigman cured into type {} ({}) at {} in {}", typeId, wow, this.blockPosition(), this.level().dimension());
		P pigman = this.convertTo(wow.getTypeResult(), typeId > 0);

		pigman.finalizeSpawn(level, level.getCurrentDifficultyAt(pigman.blockPosition()), MobSpawnType.CONVERSION, (SpawnGroupData) null, (CompoundTag) null);

		pigman.setLeftHanded(this.isLeftHanded());
		pigman.setBaby(this.isBaby());
		pigman.addEffect(new MobEffectInstance(MobEffects.CONFUSION, 200, 0));

		if (!pigman.isSilent())
			pigman.level().playSound(null, pigman.blockPosition(), RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_FINISH_CURE, pigman.getSoundSource(), 1.5F, 1.0F);

		for (EquipmentSlot equipmentslot : EquipmentSlot.values())
		{
			ItemStack itemstack = pigman.getItemBySlot(equipmentslot);
			if (!itemstack.isEmpty() && !EnchantmentHelper.hasVanishingCurse(itemstack) && itemstack.isDamageableItem() && this.getEquipmentDropChance(equipmentslot) < 2)
			{
				itemstack.setDamageValue(itemstack.getMaxDamage() - this.random.nextInt(1 + this.random.nextInt(Math.max(itemstack.getMaxDamage() - 3, 1))));
				pigman.setItemSlot(equipmentslot, itemstack.copy());
				pigman.setGuaranteedDrop(equipmentslot);
			}
		}

		if (pigman instanceof GuardPigmanEntity guard)
		{
			guard.setPlayerCured(true);
		}

		if (pigman instanceof PigmanEntity trader)
		{
			for (EquipmentSlot equipmentslot : EquipmentSlot.values())
			{
				ItemStack itemstack = this.getItemBySlot(equipmentslot);
				if (!itemstack.isEmpty())
				{
					if (EnchantmentHelper.hasBindingCurse(itemstack))
					{
						trader.getSlot(equipmentslot.getIndex() + 300).set(itemstack);
					}
					else
					{
						double d0 = (double) this.getEquipmentDropChance(equipmentslot);
						if (d0 > 1.0D)
						{
							this.spawnAtLocation(itemstack);
						}
					}
				}
			}

			trader.setCanPickUpLoot(false);
			if (this.converstionStarter != null)
				trader.curedUuids.add(this.converstionStarter);

			trader.setXp(this.traderXp);
			trader.setPigmanData(this.getPigmanData());

			if (this.tradeOffers != null)
				trader.setOffers(new MerchantOffers(this.tradeOffers));
		}

		if (this.converstionStarter != null)
		{
			Player player = level.getPlayerByUUID(this.converstionStarter);
			if (player instanceof ServerPlayer)
			{
				RediscoveredTriggers.CURE_PIGMAN.get().trigger((ServerPlayer) player, pigman);
			}
		}

		/*level.levelEvent((Player) null, 1027, pigman.blockPosition(), 0);*/
		EventHooks.onLivingConvert(this, pigman);

		return;
	}

	@Override
	protected void pickUpItem(ItemEntity item)
	{
		ItemStack stack = item.getItem().copy();

		super.pickUpItem(item);

		if (this.conversionTime > 0)
			this.trySetConversionType(stack);
	}

	@Override
	public boolean wantsToPickUp(ItemStack stack)
	{
		return super.wantsToPickUp(stack);
	}

	/**
	 * We want to make sure the player can swap the item it's holding. Since it
	 * usually holds an iron sword, you wouldn't be able to give it anything "worse"
	 * otherwise.
	 */
	@Override
	protected boolean canReplaceCurrentItem(ItemStack pCandidate, ItemStack pExisting)
	{
		if (this.conversionTime > 0)
			return this.isApplicableConversionItem(pCandidate);

		return super.canReplaceCurrentItem(pCandidate, pExisting);
	}

	private boolean isApplicableConversionItem(ItemStack candidate)
	{
		return this.getConversionType() <= 0 && IPigman.Type.isValidHandEquipForAny(candidate);
	}

	private boolean trySetConversionType(ItemStack stack)
	{
		if (IPigman.Type.get(getConversionType()) == IPigman.Type.UNASSIGNED && !this.isBaby())
		{
			for (IPigman.Type type : IPigman.Type.values())
			{
				if (type.isValidHandEquip(stack))
				{
					this.playSound(type.getZombieTypeSelectSound(), 2, 1);
					this.setConversionType((byte) type.ordinal());
					return true;
				}
			}
		}

		return false;
	}

	// TODO Post-Beta: Give Pigmen head items
	/*@Override
	protected ItemStack getSkull()
	{
		return ItemStack.EMPTY;
	}*/
}
