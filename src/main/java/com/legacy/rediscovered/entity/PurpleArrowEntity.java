package com.legacy.rediscovered.entity;

import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.common.util.Lazy;

public class PurpleArrowEntity extends AbstractArrow
{
	private static final Lazy<ItemStack> DEFAULT_STACK = Lazy.of(() -> RediscoveredItems.purple_arrow.getDefaultInstance());

	public PurpleArrowEntity(EntityType<? extends PurpleArrowEntity> entity, Level level)
	{
		super(entity, level, DEFAULT_STACK.get());
	}

	public PurpleArrowEntity(Level worldIn, LivingEntity shooter, ItemStack pickupStack)
	{
		super(RediscoveredEntityTypes.PURPLE_ARROW, shooter, worldIn, pickupStack);
	}

	public PurpleArrowEntity(Level worldIn, double x, double y, double z, ItemStack pickupStack)
	{
		super(RediscoveredEntityTypes.PURPLE_ARROW, x, y, z, worldIn, pickupStack);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (!this.level().isClientSide() && !this.inGround)
			this.setDeltaMovement(this.getDeltaMovement().add(0, 0.025F, 0));

		if (this.level().isClientSide() && !this.inGround)
		{
			float animRange = 0.2F, vertRange = 0.1F;
			this.level().addParticle(ParticleTypes.REVERSE_PORTAL, this.getX(), this.getY(), this.getZ(), (animRange * 2 * this.random.nextDouble()) - animRange, (vertRange * 2 * this.random.nextDouble()) - vertRange + 0.1F, (animRange * 2 * this.random.nextDouble()) - animRange);
		}
	}

	@Override
	protected ItemStack getPickupItem()
	{
		return RediscoveredItems.purple_arrow.getDefaultInstance();
	}

	@Override
	protected void doPostHurtEffects(LivingEntity living)
	{
		super.doPostHurtEffects(living);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag tag)
	{
		super.readAdditionalSaveData(tag);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag tag)
	{
		super.addAdditionalSaveData(tag);
	}
}