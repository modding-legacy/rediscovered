package com.legacy.rediscovered.entity;

import com.legacy.rediscovered.client.RediscoveredSounds;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;

public class SteveEntity extends AbstractSteveEntity
{
	public SteveEntity(EntityType<? extends SteveEntity> type, Level world)
	{
		super(type, world);
	}
	
	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return RediscoveredSounds.ENTITY_STEVE_HURT;
	}
	
	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_STEVE_DEATH;
	}
}