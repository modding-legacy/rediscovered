package com.legacy.rediscovered.entity.pigman.ai;

import java.util.EnumSet;

import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.target.TargetGoal;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;

/**
 * Modified vanilla copy of OwnerHurtByTargetGoal
 */
public class GuardedHurtByTargetGoal extends TargetGoal
{
	private final GuardPigmanEntity pigman;
	private LivingEntity ownerLastHurtBy;
	private int timestamp;

	public GuardedHurtByTargetGoal(GuardPigmanEntity pTameAnimal)
	{
		super(pTameAnimal, false);
		this.pigman = pTameAnimal;
		this.setFlags(EnumSet.of(Goal.Flag.TARGET));
	}

	@Override
	public boolean canUse()
	{
		if (this.pigman.isPlayerCured())
		{
			LivingEntity livingentity = this.pigman.getGuardedEntity();
			if (livingentity == null)
			{
				return false;
			}
			else
			{
				this.ownerLastHurtBy = livingentity.getLastHurtByMob();
				int i = livingentity.getLastHurtByMobTimestamp();
				return i != this.timestamp && this.canAttack(this.ownerLastHurtBy, TargetingConditions.DEFAULT)/* && this.pigman.wantsToAttack(this.ownerLastHurtBy, livingentity)*/;
			}
		}
		else
		{
			return false;
		}
	}

	@Override
	public void start()
	{
		this.mob.setTarget(this.ownerLastHurtBy);
		LivingEntity livingentity = this.pigman.getGuardedEntity();

		if (livingentity != null)
			this.timestamp = livingentity.getLastHurtByMobTimestamp();

		super.start();
	}
}