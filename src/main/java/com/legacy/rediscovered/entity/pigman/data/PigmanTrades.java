package com.legacy.rediscovered.entity.pigman.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.legacy.rediscovered.RediscoveredUtil;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.item.util.QuiverData;
import com.legacy.rediscovered.registry.RediscoveredBlocks;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredEnchantments;
import com.legacy.rediscovered.registry.RediscoveredItems;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.core.Holder;
import net.minecraft.core.NonNullList;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.npc.VillagerTrades.ItemListing;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.DyeItem;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.EnchantedBookItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionBrewing;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.item.alchemy.Potions;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.EnchantmentInstance;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;

public class PigmanTrades
{
	// @formatter:off
	public static final Int2ObjectMap<VillagerTrades.ItemListing[]> METALWORKER_OFFERS = tradeMap(ImmutableMap.of(
			1, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Items.CHARCOAL, 16).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 3).result(Items.IRON_HOE.getDefaultInstance()).givenXP(6).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).result(Items.IRON_SHOVEL.getDefaultInstance()).givenXP(4).build(),
					new Trade.Builder().firstItem(Items.IRON_INGOT, 4).result(RediscoveredItems.ruby, 1).givenXP(5).build()},
			2, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(Items.GOLD_INGOT, 5).result(RediscoveredItems.ruby, 1).givenXP(13).priceMultiplier(0.05F * 2).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 4).result(Items.IRON_AXE.getDefaultInstance()).givenXP(12).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 5).result(Items.IRON_SWORD.getDefaultInstance()).givenXP(15).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).result(Items.IRON_PICKAXE.getDefaultInstance()).givenXP(11). build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 5).secondItem(Items.BUCKET.getDefaultInstance()).result(Items.LAVA_BUCKET.getDefaultInstance()).givenXP(13).priceMultiplier(0.05F * 2).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Items.CHAINMAIL_LEGGINGS.getDefaultInstance()).givenXP(10).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 3).result(Items.CHAINMAIL_BOOTS.getDefaultInstance()).givenXP(10).build()},
			3,new VillagerTrades.ItemListing[] {
					new EnchantedItemAtLevelTrade(13, 6, Items.IRON_INGOT, Items.IRON_SWORD, 3, 15, 8, 10),
					new EnchantedItemAtLevelTrade(15, 6, Items.IRON_INGOT, Items.IRON_PICKAXE, 3, 18, 8, 10),
					new EnchantedItemAtLevelTrade(13, 6, Items.IRON_INGOT, Items.IRON_AXE, 3, 15, 8, 10),
					new EnchantedItemAtLevelTrade(9, 6, Items.IRON_INGOT, Items.IRON_SHOVEL, 3, 16, 8, 10),
					new Trade.Builder().firstItem(Items.DAMAGED_ANVIL, 1).secondItem(RediscoveredItems.ruby, 8).result(Items.ANVIL.getDefaultInstance()).givenXP(11).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Items.CHAINMAIL_HELMET.getDefaultInstance()).givenXP(11).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 4).result(Items.CHAINMAIL_CHESTPLATE.getDefaultInstance()).givenXP(11).build()},
			4, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 3).result(RediscoveredItems.plate_helmet.getDefaultInstance()).givenXP(13).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 5).result(RediscoveredItems.plate_boots.getDefaultInstance()).givenXP(15).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 20).result(EnchantedBookItem.createForEnchantment(new EnchantmentInstance(Enchantments.SHARPNESS, 1))).givenXP(23).priceMultiplier(0.05F * 5).build()},
			5,new VillagerTrades.ItemListing[] {
					new EnchantedItemAtLevelTrade(15, 10, Items.LAPIS_LAZULI,RediscoveredItems.plate_leggings,  3, 17, 15, 8),
					new EnchantedItemAtLevelTrade(17, 10, Items.LAPIS_LAZULI, RediscoveredItems.plate_chestplate, 3, 18, 15, 8),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 25).result(EnchantedBookItem.createForEnchantment(new EnchantmentInstance(Enchantments.UNBREAKING, 1))).givenXP(27).priceMultiplier(0.05F * 5).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 18).result(RediscoveredItems.dragon_armor.getDefaultInstance()).givenXP(10).maxUses(3).build()}));

	public static final Int2ObjectMap<VillagerTrades.ItemListing[]> BOWYER_OFFERS = tradeMap(ImmutableMap.of(
			1, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Items.ARROW, 18).givenXP(2).build(),
					new Trade.Builder().firstItem(Items.FLINT, 20).result(RediscoveredItems.ruby, 1).givenXP(5).priceMultiplier(0.05F * 4).build()},
			2, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).result(Items.BOW.getDefaultInstance()).givenXP(12).maxUses(12).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 3).result(Items.CROSSBOW.getDefaultInstance()).givenXP(13).maxUses(12).build(),
					new Trade.Builder().firstItem(Items.LEATHER, 5).result(RediscoveredItems.ruby, 1).givenXP(8).build(),
					new QuiverWithRandomArrowsTrade(6, 2, 64, 16, 52, 32, 3, 5, 15, false)},
			3,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(RediscoveredItems.purple_arrow, 12).givenXP(6).build(),
					new Trade.Builder().firstItem(Items.PHANTOM_MEMBRANE, 16).result(RediscoveredItems.ruby, 1).givenXP(10).priceMultiplier(0.05F * 6).build(),
					new Trade.Builder().firstItem(Items.STRING, 13).result(RediscoveredItems.ruby, 1).givenXP(10).priceMultiplier(0.05F * 4).build()},
			4, new VillagerTrades.ItemListing[] {
					new EnchantedItemAtLevelTrade(12, 14, Items.LAPIS_LAZULI, Items.BOW, 3, 23, 15, 10),
					new EnchantedItemAtLevelTrade(14, 10, Items.LAPIS_LAZULI, Items.CROSSBOW, 3, 24, 15, 10),
					new QuiverWithRandomArrowsTrade(12, 4, 128, 16, 52, 64, 5, 5, 17, true)},
			5,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(Items.DRAGON_BREATH, 1).result(RediscoveredItems.ruby, 16).givenXP(20).maxUses(1).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 16).result(EnchantedBookItem.createForEnchantment(new EnchantmentInstance(RediscoveredEnchantments.RAPID_SHOT, 1))).givenXP(23).priceMultiplier(0.05F * 4).build()}));
	
	public static final Int2ObjectMap<VillagerTrades.ItemListing[]> TECHNICIAN_OFFERS = tradeMap(ImmutableMap.of(
			1, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(RediscoveredBlocks.gear, 8).givenXP(5).build(),
					new Trade.Builder().firstItem(Items.REDSTONE, 32).result(RediscoveredItems.ruby, 1).givenXP(3).priceMultiplier(0.05F * 8).build()},
			2, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Items.IRON_INGOT, 4).result(RediscoveredBlocks.spikes, 8).givenXP(12).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Blocks.PISTON, 3).givenXP(12).build(),
					new Trade.Builder().firstItem(Items.IRON_INGOT, 4).result(RediscoveredItems.ruby, 1).givenXP(10).build()},
			3,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Items.REDSTONE_TORCH, 4).result(Items.REDSTONE, 4).givenXP(12).maxUses(24).build(),
					new Trade.Builder().firstItem(Items.QUARTZ, 12).result(RediscoveredItems.ruby, 1).givenXP(10).priceMultiplier(0.05F * 2).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).secondItem(Items.REDSTONE, 12).result(Blocks.REPEATER, 6).givenXP(12).build()},
			4, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Items.REDSTONE, 2).result(Blocks.DISPENSER, 2).givenXP(18).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Items.QUARTZ, 2).result(RediscoveredBlocks.rotational_converter, 2).givenXP(18).build(),
					new Trade.Builder().firstItem(Items.GLOWSTONE_DUST, 24).result(RediscoveredItems.ruby, 1).givenXP(15).priceMultiplier(0.05F * 6).build()},
			5,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Items.SLIME_BALL, 2).givenXP(30).maxUses(10).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 30).result(EnchantedBookItem.createForEnchantment(new EnchantmentInstance(Enchantments.SILK_TOUCH, 1))).givenXP(23).priceMultiplier(0.05F * 8).build()}));
	
	public static final Int2ObjectMap<VillagerTrades.ItemListing[]> TAILOR_OFFERS = tradeMap(ImmutableMap.of(
			1, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Blocks.WHITE_WOOL, 8).givenXP(5).build(),
					new Trade.Builder().firstItem(Items.STRING, 18).result(RediscoveredItems.ruby, 1).givenXP(3).build()},
			2, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(Items.STRING, 8 * 4).givenXP(12).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).result(Items.SHEARS.getDefaultInstance()).givenXP(12).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(RediscoveredBlocks.spring_green_wool, 8).givenXP(14).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(RediscoveredBlocks.bright_green_wool, 8).givenXP(14).build()},
			3,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(RediscoveredBlocks.lavender_wool, 8).givenXP(14).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(RediscoveredBlocks.rose_wool, 8).givenXP(14).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 7).result(RediscoveredItems.studded_boots, 1).givenXP(22).priceMultiplier(0.05F * 3).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 10).result(RediscoveredItems.studded_leggings, 1).givenXP(22).priceMultiplier(0.05F * 3).build()},
			4, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(RediscoveredBlocks.sky_blue_wool, 8).givenXP(14).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).secondItem(Blocks.WHITE_WOOL, 8).result(RediscoveredBlocks.slate_blue_wool, 8).givenXP(14).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 12).result(RediscoveredItems.studded_chestplate, 1).givenXP(22).priceMultiplier(0.05F * 3).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 8).result(RediscoveredItems.studded_helmet, 1).givenXP(22).priceMultiplier(0.05F * 3).build()},
			5,new VillagerTrades.ItemListing[] {
					new EnchantedItemAtLevelTrade(10, 9, Items.LAPIS_LAZULI, RediscoveredItems.studded_boots, 3, 23, 16, 10),
					new EnchantedItemAtLevelTrade(13, 9, Items.LAPIS_LAZULI, RediscoveredItems.studded_leggings, 3, 23, 16, 10),
					new EnchantedItemAtLevelTrade(15, 9, Items.LAPIS_LAZULI, RediscoveredItems.studded_chestplate, 3, 23, 16, 10),
					new EnchantedItemAtLevelTrade(11, 9, Items.LAPIS_LAZULI, RediscoveredItems.studded_helmet, 3, 23, 16, 10),
					new RandomTaggedItemTrade(2, 0, new ItemStack(Items.BONE_MEAL, 16), ItemTags.SMALL_FLOWERS, RediscoveredTags.Items.TAILOR_UNSELLABLE_FLOWERS, 6, 16, 10)}));
	
	public static final Int2ObjectMap<VillagerTrades.ItemListing[]> DOCTOR_OFFERS = tradeMap(ImmutableMap.of(
			1, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Items.CARROT, 12).givenXP(5).build(),
					new Trade.Builder().firstItem(Items.NETHER_WART, 20).result(RediscoveredItems.ruby, 1).givenXP(3).priceMultiplier(0.05F * 5).build()},
			2, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 1).result(Blocks.BROWN_MUSHROOM, 4).givenXP(12).build(),
					new Trade.Builder().firstItem(Items.SPIDER_EYE, 26).result(RediscoveredItems.ruby, 1).givenXP(13).priceMultiplier(0.05F * 6).build(),
					new Trade.Builder().firstItem(Items.GOLD_INGOT, 3).result(RediscoveredItems.ruby, 1).givenXP(15).build()},
			3,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(Blocks.SAND, 32).result(RediscoveredItems.ruby, 1).givenXP(10).priceMultiplier(0.05F * 8).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).result(Items.SUGAR, 18).givenXP(12).build()},
			4, new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 2).secondItem(Items.GUNPOWDER, 8).result(PotionUtils.setPotion(Items.SPLASH_POTION.getDefaultInstance(), Potions.WEAKNESS)).givenXP(12).maxUses(24).build(),
					new Trade.Builder().firstItem(Items.GHAST_TEAR, 3).result(RediscoveredItems.ruby, 1).givenXP(26).build(),
					new Trade.Builder().firstItem(Items.BLAZE_ROD, 16).result(RediscoveredItems.ruby, 1).givenXP(22).priceMultiplier(0.05F * 2).build()},
			5,new VillagerTrades.ItemListing[] {
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 3).result(Items.GOLDEN_CARROT, 3).givenXP(30).build(),
					new Trade.Builder().firstItem(RediscoveredItems.ruby, 8).result(PotionUtils.setPotion(Items.POTION.getDefaultInstance(), RediscoveredEffects.GOLDEN_AURA_POTION.get())).maxUses(6).givenXP(30).build()}));
	// @formatter:on

	private static Int2ObjectMap<VillagerTrades.ItemListing[]> tradeMap(ImmutableMap<Integer, VillagerTrades.ItemListing[]> tradeListIn)
	{
		return new Int2ObjectOpenHashMap<>(tradeListIn);
	}

	static record EnchantedItemAtLevelTrade(int rubyCount, int rubyVariance, @Nullable Item toolMaterial, Item toolItem, int maxUses, int xpValue, int enchantLevel, int enchantVariance) implements VillagerTrades.ItemListing
	{
		@Nullable
		@Override
		public MerchantOffer getOffer(Entity trader, RandomSource rand)
		{
			ItemStack itemstack = new ItemStack(this.toolItem);

			/*Dye items if they can be. This is only used for the Tailor's final studded armor trade*/
			if (itemstack.getItem() instanceof DyeableLeatherItem)
			{
				List<DyeItem> list = Lists.newArrayList();
				list.add(getRandomDye(rand));

				if (rand.nextFloat() > 0.7F)
					list.add(getRandomDye(rand));

				if (rand.nextFloat() > 0.8F)
					list.add(getRandomDye(rand));

				itemstack = DyeableLeatherItem.dyeArmor(itemstack, list);
			}

			if (this.enchantLevel > 0)
				EnchantmentHelper.enchantItem(rand, itemstack, this.enchantLevel + rand.nextInt(this.enchantVariance), false);

			return this.toolMaterial != null ? new MerchantOffer(new ItemStack(RediscoveredItems.ruby, this.rubyCount + rand.nextInt(this.rubyVariance)), new ItemStack(this.toolMaterial), itemstack, this.maxUses, this.xpValue, 0.2F) : new MerchantOffer(new ItemStack(RediscoveredItems.ruby, this.rubyCount + rand.nextInt(this.rubyVariance)), itemstack, this.maxUses, this.xpValue, 0.05F * 6);
		}
	}

	static record QuiverWithRandomArrowsTrade(int rubyCost, int rubyVariance, int arrowBudget, int arrowMinumum, int arrowMaximum, int maxStickCost, int minumumArrowStacks, int maxUses, int xpValue, boolean dye) implements VillagerTrades.ItemListing
	{
		@SuppressWarnings("deprecation")
		@Nullable
		@Override
		public MerchantOffer getOffer(Entity trader, RandomSource rand)
		{
			ItemStack stack = RediscoveredItems.quiver.getDefaultInstance();

			if (this.dye && stack.getItem() instanceof DyeableLeatherItem)
			{
				List<DyeItem> list = Lists.newArrayList();
				list.add(getRandomDye(rand));

				if (rand.nextFloat() > 0.7F)
					list.add(getRandomDye(rand));

				if (rand.nextFloat() > 0.8F)
					list.add(getRandomDye(rand));

				stack = DyeableLeatherItem.dyeArmor(stack, list);
			}

			QuiverData quiver = QuiverData.getOrCreate(stack);

			int stickCount = 64 * 2;
			int arrowTypeCount = this.minumumArrowStacks;

			if (arrowTypeCount < 5)
				arrowTypeCount += rand.nextInt(QuiverData.QUIVER_SIZE - this.minumumArrowStacks + 1);

			NonNullList<ItemStack> contents = NonNullList.withSize(QuiverData.QUIVER_SIZE, ItemStack.EMPTY);
			Set<Item> immuneArrows = Set.of(Items.ARROW, Items.TIPPED_ARROW, RediscoveredItems.purple_arrow),
					usedArrows = new HashSet<>();
			List<Item> arrowTypes = BuiltInRegistries.ITEM.stream().filter(i -> i != Items.ARROW && i != RediscoveredItems.purple_arrow && QuiverData.isAmmo(i.getDefaultInstance(), Items.BOW.getDefaultInstance())).toList();
			List<Potion> usedPotions = new ArrayList<>();

			for (int i = 0; i < arrowTypeCount; i++)
			{
				Item arrow = i == 0 ? Items.ARROW : i == 1 ? RediscoveredItems.purple_arrow : arrowTypes.get(rand.nextInt(arrowTypes.size()));

				int attempts = 0;
				// Catch duplicate arrows. Tipped are always available.
				while (usedArrows.contains(arrow) || arrow.builtInRegistryHolder().is(RediscoveredTags.Items.BOWYER_UNSELLABLE_ARROWS))
				{
					// break out if we really can't find any available arrows
					if (attempts++ == 20)
					{
						arrow = Items.ARROW;
						break;
					}

					arrow = arrowTypes.get(rand.nextInt(arrowTypes.size()));
				}

				/*Prevent the arrow from being used next time unless it's immune from being used again. This allows tipped arrows to be seen multiple times.*/
				if (!immuneArrows.contains(arrow))
					usedArrows.add(arrow);

				// have blurred minimums and maximums in case some of the arrows get greedy
				int split = (int) Math.min(this.arrowMaximum + rand.nextInt(5), Math.max(this.arrowMinumum + rand.nextInt(5), rand.nextDouble() * stickCount));
				if (split > 0)
				{
					stickCount -= split;

					ItemStack arrowStack = new ItemStack(arrow, split);

					if (arrow == Items.TIPPED_ARROW)
					{
						List<Potion> list = BuiltInRegistries.POTION.stream().filter((potion) -> !potion.getEffects().isEmpty() && PotionBrewing.isBrewablePotion(potion) && !usedPotions.contains(potion)).collect(Collectors.toList());
						Potion potion = list.get(rand.nextInt(list.size()));
						arrowStack = PotionUtils.setPotion(new ItemStack(arrow, split), potion);
						usedPotions.add(potion);
					}

					contents.set(i, arrowStack);
				}
			}

			quiver.injectData(contents);

			return new MerchantOffer(new ItemStack(RediscoveredItems.ruby, rubyCost + rand.nextInt(rubyVariance)), new ItemStack(Items.STICK, (this.maxStickCost / 2) + rand.nextInt(this.maxStickCost / 2)), stack, this.maxUses, this.xpValue, 0.2F);
		}
	}

	static record RandomTaggedItemTrade(int rubyCount, int rubyVariance, @Nullable ItemStack secondItem, TagKey<Item> tag, @Nullable TagKey<Item> exclusions, int itemCount, int maxUses, int xpValue) implements VillagerTrades.ItemListing
	{
		@Nullable
		@Override
		public MerchantOffer getOffer(Entity trader, RandomSource rand)
		{
			if (trader == null)
				return null;

			ItemStack result = null;

			Optional<Holder<Item>> item = RediscoveredUtil.getRandom(Registries.ITEM, this.tag, trader.level().registryAccess(), rand);

			if (this.exclusions != null)
				item = item.filter(i -> !i.is(this.exclusions));

			if (item.isPresent())
				result = new ItemStack(item.get().value(), this.itemCount);

			if (result == null || result.isEmpty())
				return null;

			Trade.Builder trade = new Trade.Builder().maxUses(this.maxUses).givenXP(this.xpValue).firstItem(new ItemStack(RediscoveredItems.ruby, this.rubyCount + (this.rubyVariance > 1 ? rand.nextInt(this.rubyVariance) : 0))).result(result);

			if (this.secondItem != null && !this.secondItem.isEmpty())
				trade.secondItem(this.secondItem);

			return trade.build().getOffer(trader, rand);
		}
	}

	static record DyedItemTrade(Item item, int rubyCost, int maxUses, int givenXp) implements VillagerTrades.ItemListing
	{
		@Nullable
		@Override
		public MerchantOffer getOffer(Entity trader, RandomSource rand)
		{
			ItemStack stack = this.item.getDefaultInstance();

			if (stack.getItem() instanceof DyeableLeatherItem)
			{
				List<DyeItem> list = Lists.newArrayList();
				list.add(getRandomDye(rand));

				if (rand.nextFloat() > 0.7F)
					list.add(getRandomDye(rand));

				if (rand.nextFloat() > 0.8F)
					list.add(getRandomDye(rand));

				stack = DyeableLeatherItem.dyeArmor(stack, list);
			}

			return new Trade.Builder().maxUses(this.maxUses).givenXP(this.givenXp).firstItem(new ItemStack(RediscoveredItems.ruby, this.rubyCost)).result(stack).build().getOffer(trader, rand);
		}
	}

	public static record Trade(ItemStack itemGiven1, ItemStack itemGiven2, ItemStack itemSold, int maxUses, int givenXP, float priceMultiplier) implements ItemListing
	{

		@Override
		public MerchantOffer getOffer(@Nullable Entity trader, @Nullable RandomSource rand)
		{
			MerchantOffer offer;

			//System.out.println("Creating " + this.itemGiven1 + " | " + this.itemGiven2 + " | " + this.itemSold  + " | " + this.priceMultiplier());
			if (this.itemGiven2.isEmpty())
				offer = new MerchantOffer(this.itemGiven1, this.itemSold, this.maxUses, this.givenXP, this.priceMultiplier);
			else
				offer = new MerchantOffer(this.itemGiven1, this.itemGiven2, this.itemSold, this.maxUses, this.givenXP, this.priceMultiplier);

			return offer;
		}

		public static class Builder
		{
			private ItemStack itemGiven1 = Items.WHEAT.getDefaultInstance();
			private ItemStack itemGiven2 = ItemStack.EMPTY;
			private ItemStack itemSold = Items.BREAD.getDefaultInstance();

			private int maxUses = 16;
			private int givenXP = 3;
			private float priceMultiplier = 0.05F;

			public Builder()
			{
			}

			public Builder firstItem(ItemLike item, int count)
			{
				return this.firstItem(new ItemStack(item, count));
			}

			public Builder secondItem(ItemLike item, int count)
			{
				return this.secondItem(new ItemStack(item, count));
			}

			public Builder result(ItemLike item, int count)
			{
				return this.result(new ItemStack(item, count));
			}

			public Builder firstItem(ItemStack stack)
			{
				this.itemGiven1 = stack;
				return this;
			}

			public Builder secondItem(ItemStack stack)
			{
				this.itemGiven2 = stack;
				return this;
			}

			public Builder result(ItemStack stack)
			{
				this.itemSold = stack;
				return this;
			}

			public Builder maxUses(int maxUses)
			{
				this.maxUses = maxUses;
				return this;
			}

			public Builder givenXP(int givenXp)
			{
				this.givenXP = givenXp;
				return this;
			}

			public Builder priceMultiplier(float priceMultiplier)
			{
				this.priceMultiplier = priceMultiplier;
				return this;
			}

			public Trade build()
			{
				return new Trade(this.itemGiven1, this.itemGiven2, this.itemSold, this.maxUses, this.givenXP, this.priceMultiplier);
			}
		}
	}

	private static DyeItem getRandomDye(RandomSource pRandom)
	{
		return DyeItem.byColor(DyeColor.byId(pRandom.nextInt(16)));
	}
}
