package com.legacy.rediscovered.entity.pigman.ai;

import java.util.EnumSet;

import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.navigation.FlyingPathNavigation;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.level.pathfinder.WalkNodeEvaluator;

public class PigmanFollowGuardedGoal extends Goal
{
	public static final int TELEPORT_WHEN_DISTANCE_IS = 12;
	private static final int MIN_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING = 2;
	private static final int MAX_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING = 3;
	private static final int MAX_VERTICAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING = 1;
	private final GuardPigmanEntity pigman;
	private LivingEntity owner;
	private final LevelReader level;
	private final double speedModifier;
	private final PathNavigation navigation;
	private int timeToRecalcPath;
	private final float stopDistance;
	private final float startDistance;
	private float oldWaterCost;
	private final boolean canFly;

	public PigmanFollowGuardedGoal(GuardPigmanEntity pTamable, double pSpeedModifier, float pStartDistance, float pStopDistance, boolean pCanFly)
	{
		this.pigman = pTamable;
		this.level = pTamable.level();
		this.speedModifier = pSpeedModifier;
		this.navigation = pTamable.getNavigation();
		this.startDistance = pStartDistance;
		this.stopDistance = pStopDistance;
		this.canFly = pCanFly;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
	}

	@Override
	public boolean canUse()
	{
		LivingEntity livingentity = this.pigman.getGuardedEntity();
		if (livingentity == null)
		{
			return false;
		}
		else if (livingentity.isSpectator())
		{
			return false;
		}
		else if (this.unableToMove())
		{
			return false;
		}
		else if (this.pigman.distanceToSqr(livingentity) < (double) (this.startDistance * this.startDistance))
		{
			return false;
		}
		else
		{
			this.owner = livingentity;
			return true;
		}
	}

	@Override
	public boolean canContinueToUse()
	{
		if (this.navigation.isDone())
		{
			return false;
		}
		else if (this.unableToMove())
		{
			return false;
		}
		else
		{
			return !(this.pigman.distanceToSqr(this.owner) <= (double) (this.stopDistance * this.stopDistance));
		}
	}

	private boolean unableToMove()
	{
		return this.pigman.isPassenger() || this.pigman.isLeashed();
	}

	@Override
	public void start()
	{
		this.timeToRecalcPath = 0;
		this.oldWaterCost = this.pigman.getPathfindingMalus(BlockPathTypes.WATER);
		this.pigman.setPathfindingMalus(BlockPathTypes.WATER, 0.0F);
	}

	@Override
	public void stop()
	{
		this.owner = null;
		this.navigation.stop();
		this.pigman.setPathfindingMalus(BlockPathTypes.WATER, this.oldWaterCost);
	}

	@Override
	public void tick()
	{
		this.pigman.getLookControl().setLookAt(this.owner, 10.0F, (float) this.pigman.getMaxHeadXRot());
		if (--this.timeToRecalcPath <= 0)
		{
			this.timeToRecalcPath = this.adjustedTickDelay(10);
			if (this.pigman.distanceToSqr(this.owner) >= 144.0D)
			{
				this.teleportToOwner();
			}
			else
			{
				this.navigation.moveTo(this.owner, this.speedModifier);
			}

		}
	}

	private void teleportToOwner()
	{
		BlockPos blockpos = this.owner.blockPosition();

		for (int i = 0; i < 10; ++i)
		{
			int j = this.randomIntInclusive(-MAX_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING, MAX_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING);
			int k = this.randomIntInclusive(-MAX_VERTICAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING, MAX_VERTICAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING);
			int l = this.randomIntInclusive(-MAX_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING, MAX_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING);
			boolean flag = this.maybeTeleportTo(blockpos.getX() + j, blockpos.getY() + k, blockpos.getZ() + l);
			if (flag)
			{
				return;
			}
		}

	}

	private boolean maybeTeleportTo(int pX, int pY, int pZ)
	{
		if (Math.abs((double) pX - this.owner.getX()) < MIN_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING && Math.abs((double) pZ - this.owner.getZ()) < MIN_HORIZONTAL_DISTANCE_FROM_PLAYER_WHEN_TELEPORTING)
		{
			return false;
		}
		else if (!this.canTeleportTo(new BlockPos(pX, pY, pZ)))
		{
			return false;
		}
		else
		{
			this.pigman.moveTo((double) pX + 0.5D, (double) pY, (double) pZ + 0.5D, this.pigman.getYRot(), this.pigman.getXRot());
			this.navigation.stop();
			return true;
		}
	}

	private boolean canTeleportTo(BlockPos pPos)
	{
		BlockPathTypes blockpathtypes = WalkNodeEvaluator.getBlockPathTypeStatic(this.level, pPos.mutable());
		if (blockpathtypes != BlockPathTypes.WALKABLE)
		{
			return false;
		}
		else
		{
			BlockState blockstate = this.level.getBlockState(pPos.below());
			if (!this.canFly && blockstate.getBlock() instanceof LeavesBlock)
			{
				return false;
			}
			else
			{
				BlockPos blockpos = pPos.subtract(this.pigman.blockPosition());
				return this.level.noCollision(this.pigman, this.pigman.getBoundingBox().move(blockpos));
			}
		}
	}

	private int randomIntInclusive(int pMin, int pMax)
	{
		return this.pigman.getRandom().nextInt(pMax - pMin + 1) + pMin;
	}
}