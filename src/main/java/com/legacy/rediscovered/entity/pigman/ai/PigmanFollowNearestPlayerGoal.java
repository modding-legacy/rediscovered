package com.legacy.rediscovered.entity.pigman.ai;

import java.util.EnumSet;

import javax.annotation.Nullable;

import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;

import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.level.pathfinder.BlockPathTypes;

public class PigmanFollowNearestPlayerGoal extends Goal
{
	private final GuardPigmanEntity pigman;

	@Nullable
	private LivingEntity nearestPlayer;
	private final double speedModifier;
	private final PathNavigation navigation;
	private int timeToRecalcPath;
	private final float stopDistance;
	private final float startDistance;
	private float oldWaterCost;

	public PigmanFollowNearestPlayerGoal(GuardPigmanEntity pigman, double pSpeedModifier, float pStartDistance, float stopDistance)
	{
		this.pigman = pigman;
		this.speedModifier = pSpeedModifier;
		this.navigation = pigman.getNavigation();
		this.startDistance = pStartDistance;
		this.stopDistance = stopDistance;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
	}

	@Override
	public boolean canUse()
	{
		if (!this.pigman.isDismissedUnsafely() || this.pigman.safetySeenTimer > 0)
		{
			this.nearestPlayer = null;
			return false;
		}

		LivingEntity livingentity = this.pigman.level().getNearestPlayer(this.pigman.getX(), this.pigman.getY(), this.pigman.getZ(), 20, EntitySelector.NO_CREATIVE_OR_SPECTATOR.and(EntitySelector.LIVING_ENTITY_STILL_ALIVE));
		if (livingentity == null || livingentity.isSpectator() || this.unableToMove() || this.pigman.distanceToSqr(livingentity) < (double) (this.startDistance * this.startDistance))
			return false;

		this.nearestPlayer = livingentity;

		return true;
	}

	@Override
	public boolean canContinueToUse()
	{
		if (this.navigation.isDone() || this.unableToMove() || this.pigman.safetySeenTimer > 0)
			return false;

		return !(this.pigman.distanceToSqr(this.nearestPlayer) <= (double) (this.stopDistance * this.stopDistance));
	}

	private boolean unableToMove()
	{
		return this.pigman.isPassenger() || this.pigman.isLeashed();
	}

	@Override
	public void start()
	{
		this.timeToRecalcPath = 0;
		this.oldWaterCost = this.pigman.getPathfindingMalus(BlockPathTypes.WATER);
		this.pigman.setPathfindingMalus(BlockPathTypes.WATER, 0.0F);
	}

	@Override
	public void stop()
	{
		this.nearestPlayer = null;
		this.navigation.stop();
		this.pigman.setPathfindingMalus(BlockPathTypes.WATER, this.oldWaterCost);
	}

	@Override
	public void tick()
	{
		this.pigman.getLookControl().setLookAt(this.nearestPlayer, 10.0F, (float) this.pigman.getMaxHeadXRot());
		if (--this.timeToRecalcPath <= 0)
		{
			this.timeToRecalcPath = this.adjustedTickDelay(10);

			this.navigation.moveTo(this.nearestPlayer, this.speedModifier);
		}
	}
}