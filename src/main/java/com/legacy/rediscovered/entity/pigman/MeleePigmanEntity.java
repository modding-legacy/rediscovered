package com.legacy.rediscovered.entity.pigman;

import javax.annotation.Nullable;

import com.legacy.rediscovered.entity.util.IPigman;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class MeleePigmanEntity extends GuardPigmanEntity
{
	public MeleePigmanEntity(EntityType<? extends MeleePigmanEntity> type, Level level)
	{
		super(type, level);
		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);
	}

	@Override
	public void registerAdditionalGoals()
	{
		this.goalSelector.addGoal(1, new MeleeAttackGoal(this, 0.65D, true));
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, MobSpawnType reason, @Nullable SpawnGroupData spawnData, @Nullable CompoundTag dataTag)
	{
		return super.finalizeSpawn(level, difficulty, reason, spawnData, dataTag);
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource random, DifficultyInstance difficulty)
	{
		if (random.nextBoolean())
		{
			this.setItemSlot(EquipmentSlot.HEAD, RediscoveredItems.plate_helmet.getDefaultInstance());
			this.setItemSlot(EquipmentSlot.CHEST, RediscoveredItems.plate_chestplate.getDefaultInstance());
		}
		else
		{
			this.setItemSlot(EquipmentSlot.HEAD, RediscoveredItems.studded_helmet.getDefaultInstance());
			this.setItemSlot(EquipmentSlot.CHEST, RediscoveredItems.studded_chestplate.getDefaultInstance());
			this.setItemSlot(EquipmentSlot.LEGS, RediscoveredItems.studded_leggings.getDefaultInstance());
			this.setItemSlot(EquipmentSlot.FEET, RediscoveredItems.studded_boots.getDefaultInstance());
		}

		this.setItemSlot(EquipmentSlot.MAINHAND, Items.IRON_SWORD.getDefaultInstance());
	}

	@Override
	protected float getStandingEyeHeight(Pose pose, EntityDimensions size)
	{
		return this.isBaby() ? 0.85F : 1.80F;
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return GuardPigmanEntity.createBasePigmanAttributes().add(Attributes.MAX_HEALTH, 40.0D);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public boolean doHurtTarget(Entity entity)
	{
		return super.doHurtTarget(entity);
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		return true;
	}

	@Override
	public IPigman.Type getPigmanType()
	{
		return IPigman.Type.MELEE;
	}
}