package com.legacy.rediscovered.entity.pigman.ai;

import java.util.EnumSet;
import java.util.Optional;
import java.util.function.Predicate;

import com.legacy.rediscovered.block.ChairBlock;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.ai.SearchForPoiGoal;
import com.legacy.rediscovered.entity.pigman.PigmanEntity;
import com.legacy.rediscovered.entity.pigman.data.PigmanData;
import com.legacy.rediscovered.entity.pigman.data.PigmanData.Profession;
import com.legacy.rediscovered.entity.util.MountableBlockEntity;
import com.legacy.rediscovered.registry.RediscoveredAttributes;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredPoiTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.core.GlobalPos;
import net.minecraft.core.Holder;
import net.minecraft.core.SectionPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.behavior.BehaviorUtils;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.TradeWithPlayerGoal;
import net.minecraft.world.entity.ai.goal.UseItemGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.util.DefaultRandomPos;
import net.minecraft.world.entity.ai.util.LandRandomPos;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiRecord;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.ai.village.poi.PoiTypes;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.schedule.Activity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

/**
 * Separates the massive amount of Pigman AI code into its own place
 */
public class PigmanGoals
{
	public static class TradeWithPlayerOffGroundGoal extends TradeWithPlayerGoal
	{
		private final AbstractVillager mob;

		public TradeWithPlayerOffGroundGoal(AbstractVillager villager)
		{
			super(villager);

			this.mob = villager;
		}

		@Override
		public boolean canUse()
		{
			// modified to allow it to not be on the ground
			if (!this.mob.isAlive() || this.mob.isInWater() || this.mob.hurtMarked)
			{
				return false;
			}
			else
			{
				Player player = this.mob.getTradingPlayer();

				if (player == null)
					return false;
				else if (this.mob.distanceToSqr(player) > 16.0D)
					return false;

				return player.containerMenu != null;
			}
		}
	}

	public static class StrollWithVillageBiasGoal extends WaterAvoidingRandomStrollGoal
	{
		private final PigmanEntity pigman;

		public StrollWithVillageBiasGoal(PigmanEntity mob)
		{
			super(mob, 0.5D, 0.01F);

			this.pigman = mob;
		}

		@Override
		public boolean canUse()
		{
			if (this.pigman.getVehicle() instanceof MountableBlockEntity)
				return false;

			return super.canUse();
		}

		@Override
		public boolean canContinueToUse()
		{
			if (this.pigman.getVehicle() instanceof MountableBlockEntity)
				return false;

			return super.canContinueToUse();
		}

		@Override
		protected Vec3 getPosition()
		{
			int horizontalDist = 10, verticalDist = 7;
			var mob = this.mob;

			if (!(mob.level()instanceof ServerLevel sl) || this.pigman.homePos == null || this.mob.getRandom().nextFloat() < 0.3F)
				return super.getPosition();

			BlockPos blockpos = mob.blockPosition();
			Vec3 vec3;

			if (sl.isVillage(blockpos))
			{
				vec3 = LandRandomPos.getPos(mob, horizontalDist, verticalDist);
			}
			else
			{
				SectionPos sectionpos = SectionPos.of(blockpos);
				SectionPos sectionpos1 = BehaviorUtils.findSectionClosestToVillage(sl, sectionpos, 2);

				if (sectionpos1 != sectionpos)
				{
					vec3 = DefaultRandomPos.getPosTowards(mob, horizontalDist, verticalDist, Vec3.atBottomCenterOf(sectionpos1.center()), (double) ((float) Math.PI / 2F));
				}
				else
				{
					vec3 = LandRandomPos.getPos(mob, horizontalDist, verticalDist);
				}
			}

			// System.out.println("village: " + vec3);

			return vec3;

		}
	}

	public static class AvoidNetherReactorGoal extends AvoidPoiGoal
	{
		public AvoidNetherReactorGoal(PigmanEntity mob)
		{
			super(mob, RediscoveredPoiTypes.NETHER_REACTOR_CORE.getKey(), 50, 10 * 20, 50);
			this.setMovementFlags();
		}

		@Override
		public boolean canUse()
		{
			return !this.mob.isImmuneToZombification() && (super.canUse() || this.mob.hasEffect(RediscoveredEffects.CRIMSON_VEIL.get())) && !RediscoveredAttributes.isConversionImmune(this.mob);
		}

		@Override
		public boolean canContinueToUse()
		{
			return !this.mob.isImmuneToZombification() && !RediscoveredAttributes.isConversionImmune(this.mob) && super.canContinueToUse();
		}
	}

	public static class AvoidNetherPortalGoal extends AvoidPoiGoal
	{
		public AvoidNetherPortalGoal(PigmanEntity mob)
		{
			super(mob, PoiTypes.NETHER_PORTAL, 10, 5 * 20, 10);
			this.setMovementFlags();
		}
	}

	public static class FindAndSitInChairGoal<T extends PigmanEntity> extends SearchForPoiGoal<T>
	{
		public FindAndSitInChairGoal(T mob)
		{
			super(mob, PigmanEntity::blockPosition, RediscoveredPoiTypes.CHAIR.getKey(), 15, 30 * 20, 60 * 20);
			this.setFlags(EnumSet.of(Flag.MOVE));
		}

		@Override
		public boolean canUse()
		{
			if (this.mob.getVehicle() != null)
				return false;

			return super.canUse();
		}

		@Override
		public boolean canContinueToUse()
		{
			return super.canContinueToUse();
		}

		@Override
		public void onPoiFound(BlockPos pos)
		{
			this.mob.getNavigation().stop();
		}

		@Override
		public void tickFound(BlockPos pos)
		{
			this.mob.getLookControl().setLookAt(Vec3.atCenterOf(pos));

			if (this.mob.getNavigation().isDone())
				this.mob.getNavigation().moveTo(pos.getX(), pos.getY(), pos.getZ(), 0.5F);

			if (this.distanceTo() <= 2)
			{
				this.mob.getNavigation().stop();

				if (ChairBlock.tryInteract(this.mob, this.foundPos, null))
					this.mob.sitTimer = (30 * 20) + this.mob.getRandom().nextInt(60 * 20);

				this.finish();
			}
		}

		@Override
		public void onPoiNotFound()
		{
			this.resetDelay();
			this.currentDelay = this.currentDelay / 2;
		}

		@Override
		public Predicate<PoiRecord> poiFilter()
		{
			return record -> ChairBlock.canMount(this.mob.level(), record.getPos(), false);
		}
	}

	public static class SearchForMeetingPointGoal<T extends PigmanEntity> extends SearchForPoiGoal<T>
	{
		public SearchForMeetingPointGoal(T mob)
		{
			super(mob, (e) -> !e.originFoundNaturally && e.villageOrigin != null ? e.villageOrigin.pos() : e.blockPosition(), PoiTypes.MEETING, 60, 10 * 20);
		}

		@Override
		public boolean canUse()
		{
			return !this.mob.originFoundNaturally && super.canUse();
		};

		@Override
		public void onPoiFound(BlockPos pos)
		{
			if (this.mob.level()instanceof ServerLevel sl)
			{
				sl.getPoiManager().take(this.poi, (poi, blockpos) -> blockpos.equals(pos), pos, 1);

				this.mob.originFoundNaturally = true;
				this.mob.villageOrigin = GlobalPos.of(sl.dimension(), pos);
			}

			this.finish();
		}
	}

	public static class SearchForHomeGoal<T extends PigmanEntity> extends SearchForPoiGoal<T>
	{
		public SearchForHomeGoal(T mob)
		{
			super(mob, PigmanEntity::blockPosition, PoiTypes.HOME, 50, 5 * 20);
		}

		@Override
		public boolean canUse()
		{
			return this.mob.homePos == null && super.canUse();
		};

		@Override
		public void onPoiFound(BlockPos pos)
		{
			if (this.mob.level()instanceof ServerLevel sl)
			{
				sl.getPoiManager().take(this.poi, (poi, blockpos) -> blockpos.equals(pos), pos, 1);

				this.mob.homePos = GlobalPos.of(sl.dimension(), pos);
				sl.broadcastEntityEvent(this.mob, (byte) 14);
			}
			this.finish();
		}

		@Override
		protected PoiManager.Occupancy getOccupancyType()
		{
			return PoiManager.Occupancy.HAS_SPACE;
		}
	}

	public static class SearchForJobGoal<T extends PigmanEntity> extends SearchForPoiGoal<T>
	{
		PigmanData.Profession profession;

		public SearchForJobGoal(T mob)
		{
			super(mob, PigmanEntity::blockPosition, poi -> mob.getPigmanData().getProfession().usableJobSites.test(poi), 50, 5 * 20, 0);
		}

		@Override
		public boolean canUse()
		{
			return !this.mob.isBaby() && this.mob.jobSite == null && this.mob.possibleJobSite == null && (this.mob.tickCount <= 40 || this.mob.getCurrentActivity() == Activity.WORK) && super.canUse();
		};

		@Override
		public void onPoiFound(BlockPos pos)
		{
			this.mob.possibleJobSite = GlobalPos.of(this.mob.level().dimension(), pos);

			this.finish();
		}

		@Override
		public void tickFound(BlockPos pos)
		{
		}

		@Override
		protected PoiManager.Occupancy getOccupancyType()
		{
			return PoiManager.Occupancy.HAS_SPACE;
		}
	}

	/*public static class YieldJobGoal<T extends PigmanEntity> extends Goal
	{
		PigmanData.Profession profession;
	
		public YieldJobGoal(T mob)
		{
			super(mob, PigmanEntity::blockPosition, poi -> poi.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION), 50, 5 * 20, 0);
		}
	
		@Override
		public boolean canUse()
		{
			return this.mob.jobSite == null && this.mob.possibleJobSite == null && (this.mob.tickCount > 40 || this.mob.getCurrentActivity() == Activity.WORK) && super.canUse();
		};
	
		@Override
		public void onPoiFound(BlockPos pos)
		{
			if (this.mob.level() instanceof ServerLevel sl)
			{
				sl.getPoiManager().getType(pos)
			}
			this.mob.possibleJobSite = GlobalPos.of(this.mob.level().dimension(), pos);
		}
	
		@Override
		public void tickFound(BlockPos pos)
		{
		}
	
		@Override
		protected PoiManager.Occupancy getOccupancyType()
		{
			return PoiManager.Occupancy.IS_OCCUPIED;
		}
	}*/

	public static class AcquireJobSiteGoal<T extends PigmanEntity> extends Goal
	{
		private static final int TIMEOUT = 60;
		T mob;
		int noPathTimeout;
		PigmanData.Profession profession;

		public AcquireJobSiteGoal(T mob)
		{
			this.mob = mob;
			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
		}

		@Override
		public boolean canUse()
		{
			return !this.mob.isBaby() && this.mob.possibleJobSite != null;
		}

		@Override
		public boolean canContinueToUse()
		{
			var site = this.mob.possibleJobSite;
			return site != null && this.profession != null && this.profession != Profession.NONE && this.mob.level()instanceof ServerLevel sl && !(!sl.dimension().equals(site.dimension()) || !sl.getPoiManager().exists(site.pos(), p -> p.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION))) && this.noPathTimeout > 0;
		}

		@Override
		public void start()
		{
			this.noPathTimeout = TIMEOUT;
			BlockPos pos = this.mob.possibleJobSite.pos();
			if (this.mob.level()instanceof ServerLevel sl)
			{
				var type = sl.getPoiManager().getType(pos);

				if (type.isPresent())
				{
					for (var prof : PigmanData.Profession.values())
					{
						if (prof.jobSite.test(type.get()))
						{
							sl.getPoiManager().take(prof.usableJobSites, (poi, blockpos) -> blockpos.equals(pos), pos, 1);

							this.mob.possibleJobSite = GlobalPos.of(sl.dimension(), pos);
							this.profession = prof;

							this.yieldJob(50);

							break;
						}
					}
				}
			}
		}

		@Override
		public void tick()
		{
			if (this.mob.possibleJobSite == null || this.profession == null || this.profession == Profession.NONE)
				return;

			BlockPos pos = this.mob.possibleJobSite.pos();

			Vec3 vec = Vec3.atCenterOf(pos);
			this.mob.getLookControl().setLookAt(vec);

			if (this.mob.getNavigation().isDone())
			{
				this.mob.setMovingToRemembered(true);

				Path path = this.mob.getNavigation().createPath(vec.x(), vec.y(), vec.z(), 1);

				if (path != null)
				{
					// if (path.canReach())
					{
						this.mob.getNavigation().moveTo(path, 0.5F);
					}
					/*else
					{
						this.mob.playSound(SoundEvents.ANVIL_LAND);
						this.finish();
					}*/
				}
			}

			if (distanceTo(this.mob, pos) <= 2F && this.profession != null)
			{
				this.mob.setPigmanData(this.mob.getPigmanData().setProfession(this.profession));

				this.mob.jobSite = this.mob.possibleJobSite;
				this.mob.possibleJobSite = null;

				this.mob.level().broadcastEntityEvent(this.mob, (byte) 14);
				this.mob.getNavigation().stop();
			}

			if (!this.mob.isPathFinding())
				--this.noPathTimeout;
			else if (!this.mob.getNavigation().isStuck())
				this.noPathTimeout = TIMEOUT;
		}

		@Override
		public void stop()
		{
			this.mob.releasePossibleJobSite();
		}

		private void yieldJob(double range)
		{
			if (this.mob.level()instanceof ServerLevel sl)
			{
				Optional<Holder<PoiType>> poi = sl.getPoiManager().getType(this.mob.possibleJobSite.pos());

				if (poi.isPresent())
				{
					sl.getEntities(this.mob, new AABB(this.mob.blockPosition()).inflate(range)).stream().filter((p_258898_) ->
					{
						return p_258898_ instanceof PigmanEntity && p_258898_ != this.mob;
					}).map((p_258896_) ->
					{
						return (PigmanEntity) p_258896_;
					}).filter(LivingEntity::isAlive).filter((newPigman) ->
					{
						var p = this.mob.possibleJobSite.pos();
						return nearbyWantsJobsite(poi.get(), newPigman, p) && distanceTo(newPigman, p) < distanceTo(this.mob, p);
					}).findFirst().ifPresent((newPigman) ->
					{
						this.mob.getNavigation().stop();

						/*newPigman.addEffect(new MobEffectInstance(MobEffects.GLOWING, 5));*/
						newPigman.possibleJobSite = this.mob.possibleJobSite;

						this.mob.possibleJobSite = null;
					});
				}
			}
		}

		private static boolean nearbyWantsJobsite(Holder<PoiType> pPoi, PigmanEntity pigman, BlockPos pPos)
		{
			boolean hasJob = pigman.possibleJobSite != null || pigman.jobSite != null;

			if (hasJob)
				return false;

			// TODO Bailey Post-Beta: Make sure the Pigman can actually reach their locations
			/*Optional<GlobalPos> optional = Optional.ofNullable(pigman.possibleJobSite);*/
			Profession villagerprofession = pigman.getPigmanData().getProfession();
			if (villagerprofession.usableJobSites.test(pPoi))
			{
				return true;// optional.isEmpty() ? canReachPos(pigman, pPos, pPoi.value()) :
							// optional.get().pos().equals(pPos);
			}
			else
			{
				return false;
			}
		}

		/*private static boolean canReachPos(PathfinderMob pMob, BlockPos pPos, PoiType pPoi)
		{
			Path path = pMob.getNavigation().createPath(pPos, 99);
			return path != null && path.canReach();
		}*/
	}

	private static abstract class AvoidPoiGoal extends SearchForPoiGoal<PigmanEntity>
	{
		private final int escapeDistance;

		public AvoidPoiGoal(PigmanEntity mob, ResourceKey<PoiType> poi, int radius, int checkDelay, int escapeDistance)
		{
			super(mob, e -> mob.blockPosition(), poi, radius, checkDelay);

			this.escapeDistance = escapeDistance;
		}

		@Override
		public void onPoiFound(BlockPos pos)
		{
			this.mob.sitTimer = 0;
			this.mob.playSound(RediscoveredSounds.ENTITY_PIGMAN_SCARED, 1, 0.7F);
		}

		@Override
		public void tickFound(BlockPos pos)
		{
			Vec3 vec3 = DefaultRandomPos.getPosAway(this.mob, 16, 7, Vec3.atCenterOf(pos));
			if (vec3 == null)
				return;

			if (mob.getNavigation().isDone())
				this.mob.getNavigation().moveTo(vec3.x(), vec3.y(), vec3.z(), 0.7F);

			if (this.distanceTo() > this.escapeDistance || this.mob.level()instanceof ServerLevel sl && !sl.getPoiManager().exists(pos, this.poi))
				this.finish();
		}
	}

	public static class DoctorDrinkAuraPotionGoal<T extends PigmanEntity> extends UseItemGoal<T>
	{
		public DoctorDrinkAuraPotionGoal(T mob)
		{
			super(mob, PotionUtils.setPotion(new ItemStack(Items.POTION), RediscoveredEffects.GOLDEN_AURA_POTION.get()), RediscoveredSounds.ENTITY_PIGMAN_IDLE, DoctorDrinkAuraPotionGoal::getPredicate);
		}

		private static final <P extends PigmanEntity> boolean getPredicate(P pigman)
		{
			return pigman.getPigmanData().getProfession() == PigmanData.Profession.DOCTOR && pigman.getConvertTime() > 0 && !pigman.hasEffect(RediscoveredEffects.GOLDEN_AURA.get());
		}
	}

	protected static final float distanceTo(Entity entity, BlockPos pos)
	{
		return Mth.sqrt((float) entity.distanceToSqr(Vec3.atCenterOf(pos)));
	}
}
