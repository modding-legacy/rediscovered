package com.legacy.rediscovered.entity.pigman;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.client.gui.GuardPigmanInventoryMenu;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.ai.SearchForPoiGoal;
import com.legacy.rediscovered.entity.pigman.ai.GuardedHurtByTargetGoal;
import com.legacy.rediscovered.entity.pigman.ai.GuardedHurtTargetGoal;
import com.legacy.rediscovered.entity.pigman.ai.PigmanFollowGuardedGoal;
import com.legacy.rediscovered.entity.pigman.ai.PigmanFollowNearestPlayerGoal;
import com.legacy.rediscovered.entity.util.IPigman;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerListener;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveThroughVillageGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.ai.village.poi.PoiTypes;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.ZombifiedPiglin;
import net.minecraft.world.entity.npc.InventoryCarrier;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.event.EventHooks;

public abstract class GuardPigmanEntity extends PathfinderMob implements IPigman, InventoryCarrier, ContainerListener
{
	public static final Map<EquipmentSlot, Integer> SLOT_MAP = Map.of(EquipmentSlot.MAINHAND, 0, EquipmentSlot.OFFHAND, 1, EquipmentSlot.FEET, 2, EquipmentSlot.LEGS, 3, EquipmentSlot.CHEST, 4, EquipmentSlot.HEAD, 5);

	protected static final EntityDataAccessor<Boolean> PLAYER_CURED = SynchedEntityData.defineId(GuardPigmanEntity.class, EntityDataSerializers.BOOLEAN);
	protected static final EntityDataAccessor<Boolean> IMMUNE_TO_ZOMBIFICATION = SynchedEntityData.defineId(GuardPigmanEntity.class, EntityDataSerializers.BOOLEAN);
	protected static final EntityDataAccessor<Integer> ZOMBIFICATION_TIME = SynchedEntityData.defineId(GuardPigmanEntity.class, EntityDataSerializers.INT);
	protected static final EntityDataAccessor<Optional<UUID>> GUARDED_UUID = SynchedEntityData.defineId(GuardPigmanEntity.class, EntityDataSerializers.OPTIONAL_UUID);
	protected static final EntityDataAccessor<Long> TIME_PAID = SynchedEntityData.defineId(GuardPigmanEntity.class, EntityDataSerializers.LONG);
	protected static final EntityDataAccessor<Boolean> DISMISSED = SynchedEntityData.defineId(GuardPigmanEntity.class, EntityDataSerializers.BOOLEAN);

	private final PigmanInventory inventory;

	public int safetySeenTimer;

	public GuardPigmanEntity(EntityType<? extends GuardPigmanEntity> type, Level world)
	{
		super(type, world);

		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);

		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
		this.setPathfindingMalus(BlockPathTypes.TRAPDOOR, -1.0F);

		this.inventory = new PigmanInventory(this, 6);
		this.inventory.addListener(this);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(PLAYER_CURED, false);
		this.entityData.define(IMMUNE_TO_ZOMBIFICATION, false);
		this.entityData.define(ZOMBIFICATION_TIME, 0);
		this.entityData.define(GUARDED_UUID, Optional.empty());
		this.entityData.define(TIME_PAID, 0L);
		this.entityData.define(DISMISSED, false);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new SearchForSafetyGoal(this, PoiTypes.HOME, 30, 60));
		this.goalSelector.addGoal(2, new PigmanFollowNearestPlayerGoal(this, 0.7D, 8.0F, 4.0F));

		// this.goalSelector.addGoal(2, new MoveTowardsVillageGoal(this, 0.6D));
		this.goalSelector.addGoal(3, new MoveThroughVillageGoal(this, 0.6D, false, 4, () -> true)
		{
			@Override
			public boolean canUse()
			{
				return getGuardedUUID().isEmpty() && !isDismissedUnsafely() && super.canUse();
			}
		});
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));

		this.targetSelector.addGoal(1, new HurtByTargetGoal(this).setAlertOthers(GuardPigmanEntity.class));

		/*this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Player.class, false));*/
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Mob.class, 5, false, false, (mob) -> mob instanceof Enemy && !(mob instanceof Creeper) && !(mob instanceof ZombifiedPiglin))
		{
			@Override
			public boolean canUse()
			{
				return getGuardedUUID().isEmpty() && super.canUse();
			}
		});

		this.goalSelector.addGoal(1, new AvoidEntityGoal<ZombifiedPiglin>(this, ZombifiedPiglin.class, 8.0F, 0.6D, 0.8D));

		this.goalSelector.addGoal(6, new PigmanFollowGuardedGoal(this, 0.7D, 10.0F, 2.0F, false));
		this.targetSelector.addGoal(1, new GuardedHurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new GuardedHurtTargetGoal(this));

		this.registerAdditionalGoals();
	}

	public void registerAdditionalGoals()
	{
	}

	public static AttributeSupplier.Builder createBasePigmanAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D);
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, MobSpawnType reason, @Nullable SpawnGroupData spawnData, @Nullable CompoundTag dataTag)
	{
		if (reason != MobSpawnType.CONVERSION)
		{
			this.populateDefaultEquipmentSlots(level.getRandom(), difficulty);
			this.populateDefaultEquipmentEnchantments(level.getRandom(), difficulty);
		}

		return super.finalizeSpawn(level, difficulty, reason, spawnData, dataTag);
	}

	protected static final String CURED_KEY = "PlayerCured", GUARDED_UUID_KEY = "GuardedUUID",
			TIME_PAID_KEY = "TimePaid", DISMISSED_KEY = "DismissedUnsafely";

	@Override
	public void addAdditionalSaveData(CompoundTag tag)
	{
		super.addAdditionalSaveData(tag);

		tag.putBoolean(CURED_KEY, this.isPlayerCured());

		if (this.getGuardedUUID().isPresent())
		{
			tag.putUUID(GUARDED_UUID_KEY, this.getGuardedUUID().get());
			tag.putLong(TIME_PAID_KEY, this.getTimePaid());
		}

		tag.putInt(ZOMBIFICATION_TIME_KEY, this.getConvertTime());
		tag.putBoolean(ZOMBIFICATION_IMMUNE_KEY, this.isImmuneToZombification());
		tag.putBoolean(DISMISSED_KEY, this.isDismissedUnsafely());

		this.writeInventoryToTag(tag);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag tag)
	{
		super.readAdditionalSaveData(tag);

		this.setPlayerCured(tag.getBoolean(CURED_KEY));

		if (tag.contains(GUARDED_UUID_KEY, Tag.TAG_INT_ARRAY))
		{
			this.setGuardedUUID(tag.getUUID(GUARDED_UUID_KEY));
			this.setTimePaid(tag.getLong(TIME_PAID_KEY));
		}

		this.setConvertTime(tag.getInt(ZOMBIFICATION_TIME_KEY));
		this.setImmuneToZombification(tag.getBoolean(ZOMBIFICATION_IMMUNE_KEY));
		this.setDismissedUnsafely(tag.getBoolean(DISMISSED_KEY));

		this.readInventoryFromTag(tag);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return this.isBaby() ? 0.85F : 1.80F;
	}

	@Override
	public boolean doHurtTarget(Entity entity)
	{
		if (this.isPlayerCured())
		{
			ItemStack stack = this.getMainHandItem();
			if (entity instanceof LivingEntity living)
				stack.getItem().hurtEnemy(this.getMainHandItem(), living, this);
			else if (stack.isDamageableItem())
				stack.hurtAndBreak(2, this, (e) -> e.broadcastBreakEvent(EquipmentSlot.MAINHAND));
		}

		return super.doHurtTarget(entity);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public void aiStep()
	{
		super.aiStep();
		this.updateSwingTime();
	}

	@Override
	protected void customServerAiStep()
	{
		super.customServerAiStep();

		if (this.safetySeenTimer > 0)
			--this.safetySeenTimer;

		if (this.getRandom().nextInt(900) == 0 && this.deathTime == 0)
			this.heal(1.0F);

		if (this.isPlayerCured() && this.getGuardedUUID().isPresent())
		{
			var guarded = this.getGuardedEntity();

			if (guarded != null)
				this.restrictTo(guarded.blockPosition(), 10);

			/*System.out.println((this.level().getGameTime() - this.timePaid) / 20);*/

			if ((this.level().getGameTime() - this.getTimePaid()) >= this.timeToBodyguard() * 20L)
			{
				this.dismissBodyguard();
			}
		}

		if (this.shouldConvert(this))
			this.setConvertTime(this.getConvertTime() + 1);
		else
			this.setConvertTime(0);

		if (this.getConvertTime() > NATURAL_CONVERSION_TIME && EventHooks.canLivingConvert(this, RediscoveredEntityTypes.ZOMBIE_PIGMAN, (timer) -> this.setConvertTime(timer)))
			IPigman.zombify(this, null);
	}

	public long timeToBodyguard()
	{
		return 60 * 15;
	}

	@Override
	public boolean isAlliedTo(Entity entity)
	{
		if (entity instanceof Player && this.isPlayerCured() || entity.getType().is(RediscoveredTags.Entities.PIGMAN_ALLIES))
			return this.getTeam() == null && entity.getTeam() == null;

		return this.isAlliedTo(entity.getTeam());
	}

	public LivingEntity getGuardedEntity()
	{
		Optional<UUID> uuid = this.getGuardedUUID();

		if (uuid.isEmpty())
			return null;

		return this.level().getPlayerByUUID(uuid.get());
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return RediscoveredSounds.ENTITY_PIGMAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return RediscoveredSounds.ENTITY_PIGMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_PIGMAN_DEATH;
	}

	protected void playStepSound(BlockPos pos, BlockState block)
	{
		this.playSound(SoundEvents.PIG_STEP, 0.15F, 1.0F);
	}

	public boolean isPlayerCured()
	{
		return this.entityData.get(PLAYER_CURED);
	}

	public void setPlayerCured(boolean flag)
	{
		this.entityData.set(PLAYER_CURED, flag);
	}

	public void setImmuneToZombification(boolean flag)
	{
		this.getEntityData().set(IMMUNE_TO_ZOMBIFICATION, flag);
	}

	public boolean isImmuneToZombification()
	{
		return this.getEntityData().get(IMMUNE_TO_ZOMBIFICATION);
	}

	public void setConvertTime(int time)
	{
		this.getEntityData().set(ZOMBIFICATION_TIME, time);
	}

	public int getConvertTime()
	{
		return this.getEntityData().get(ZOMBIFICATION_TIME);
	}

	public Optional<UUID> getGuardedUUID()
	{
		return this.entityData.get(GUARDED_UUID);
	}

	public void setGuardedUUID(UUID id)
	{
		this.entityData.set(GUARDED_UUID, Optional.ofNullable(id));
	}

	public void setTimePaid(long time)
	{
		this.getEntityData().set(TIME_PAID, time);
	}

	public long getTimePaid()
	{
		return this.getEntityData().get(TIME_PAID);
	}

	public boolean isDismissedUnsafely()
	{
		return this.entityData.get(DISMISSED);
	}

	public void setDismissedUnsafely(boolean flag)
	{
		this.entityData.set(DISMISSED, flag);
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		return true;
	}

	@Override
	public boolean canPickUpLoot()
	{
		return super.canPickUpLoot() || this.isPlayerCured();
	}

	public boolean isValidHandEquip(ItemStack stack)
	{
		return this.getPigmanType().isValidHandEquip(stack);
	}

	public boolean isValidOffHandEquip(ItemStack stack)
	{
		return this.getPigmanType().isValidOffHandEquip(stack);
	}

	public void hireAsBodyguard(UUID uuid)
	{
		this.setDismissedUnsafely(false);
		this.setTimePaid(this.level().getGameTime());
		this.setGuardedUUID(uuid);

		this.playSound(RediscoveredSounds.ENTITY_PIGMAN_AGREE, this.getSoundVolume(), 1F);
	}

	public void dismissBodyguard()
	{
		this.clearRestriction();
		this.setGuardedUUID(null);
		this.setTimePaid(0);
		this.setDismissedUnsafely(true);
		/*this.inventory.clearContent();*/

		this.playSound(RediscoveredSounds.ENTITY_PIGMAN_DISAGREE, this.getSoundVolume(), 1F);
	}

	@Override
	public InteractionResult interactAt(Player player, Vec3 vec, InteractionHand hand)
	{
		if (this.isPlayerCured())
		{
			if (player instanceof ServerPlayer sp)
			{
				sp.openMenu(new SimpleMenuProvider((id, inventory, playerIn) ->
				{
					return new GuardPigmanInventoryMenu(id, inventory, this);
				}, this.getName()), (buffer) -> buffer.writeInt(this.getId()));
			}

			return InteractionResult.SUCCESS;

		}

		return super.interactAt(player, vec, hand);
	}

	@Override
	public SimpleContainer getInventory()
	{
		return this.inventory;
	}

	@Override
	public void containerChanged(Container pContainer)
	{
		SLOT_MAP.forEach((slot, id) ->
		{
			if (this.isPlayerCured())
				this.setItemSlotAndDropWhenKilled(slot, this.inventory.getItem(id));
			else
				this.setItemSlot(slot, this.inventory.getItem(id));
		});
	}

	@Override
	public void setItemSlot(EquipmentSlot pSlot, ItemStack pStack)
	{
		super.setItemSlot(pSlot, pStack);
		this.inventory.setItemNoUpdate(SLOT_MAP.get(pSlot), pStack);
	}

	@Override
	protected void hurtArmor(DamageSource source, float damage)
	{
		if (this.isPlayerCured())
			this.inventory.damageArmor(source, damage);
	}

	@Override
	public boolean wantsToPickUp(ItemStack stack)
	{
		if (!this.isPlayerCured())
			return super.wantsToPickUp(stack);

		return false;
	}

	public GuardPigmanEntity.PigmanArmPose getArmPose()
	{
		return GuardPigmanEntity.PigmanArmPose.NEUTRAL;
	}

	public static enum PigmanArmPose
	{
		NEUTRAL, BOW_AND_ARROW, CROSSBOW_CHARGE, CROSSBOW_HOLD;
	}

	private class SearchForSafetyGoal extends SearchForPoiGoal<GuardPigmanEntity>
	{
		public SearchForSafetyGoal(GuardPigmanEntity mob, ResourceKey<PoiType> poi, int radius, int checkDelay)
		{
			super(mob, e -> mob.blockPosition(), poi, radius, checkDelay);
		}

		@Override
		public boolean canUse()
		{
			return this.mob.isDismissedUnsafely() && super.canUse();
		}

		@Override
		public boolean canContinueToUse()
		{
			return super.canContinueToUse() && this.mob.isDismissedUnsafely();
		}

		@Override
		public void onPoiFound(BlockPos pos)
		{
			this.mob.playSound(RediscoveredSounds.ENTITY_PIGMAN_AGREE, 1, 1.5F);
		}

		@Override
		public void tickFound(BlockPos pos)
		{
			this.mob.safetySeenTimer = 60;

			this.mob.getLookControl().setLookAt(Vec3.atCenterOf(pos));

			if (this.mob.getNavigation().isDone())
				this.mob.getNavigation().moveTo(pos.getX(), pos.getY(), pos.getZ(), 0.8F);

			if (this.distanceTo() <= 10 || this.mob.level()instanceof ServerLevel sl && !sl.getPoiManager().exists(pos, this.poi))
			{
				this.mob.setDismissedUnsafely(false);
				this.finish();
			}
		}
	}
}
