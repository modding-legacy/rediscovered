package com.legacy.rediscovered.entity.pigman.ai;

import java.util.EnumSet;

import com.legacy.rediscovered.entity.pigman.GuardPigmanEntity;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.target.TargetGoal;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;

/**
 * Modified vanila copy of OwnerHurtTargetGoal
 */
public class GuardedHurtTargetGoal extends TargetGoal
{
	private final GuardPigmanEntity tameAnimal;
	private LivingEntity ownerLastHurt;
	private int timestamp;

	public GuardedHurtTargetGoal(GuardPigmanEntity pTameAnimal)
	{
		super(pTameAnimal, false);
		this.tameAnimal = pTameAnimal;
		this.setFlags(EnumSet.of(Goal.Flag.TARGET));
	}

	@Override
	public boolean canUse()
	{
		if (this.tameAnimal.isPlayerCured())
		{
			LivingEntity livingentity = this.tameAnimal.getGuardedEntity();
			if (livingentity == null)
			{
				return false;
			}
			else
			{
				this.ownerLastHurt = livingentity.getLastHurtMob();
				int i = livingentity.getLastHurtMobTimestamp();
				return i != this.timestamp && this.canAttack(this.ownerLastHurt, TargetingConditions.DEFAULT)/* && this.tameAnimal.wantsToAttack(this.ownerLastHurt, livingentity)*/;
			}
		}
		else
		{
			return false;
		}
	}

	@Override
	public void start()
	{
		this.mob.setTarget(this.ownerLastHurt);
		LivingEntity livingentity = this.tameAnimal.getGuardedEntity();
		if (livingentity != null)
		{
			this.timestamp = livingentity.getLastHurtMobTimestamp();
		}

		super.start();
	}
}