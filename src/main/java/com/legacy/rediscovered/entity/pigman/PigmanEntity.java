package com.legacy.rediscovered.entity.pigman;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.pigman.ai.PigmanGoals;
import com.legacy.rediscovered.entity.pigman.data.PigmanData;
import com.legacy.rediscovered.entity.pigman.data.PigmanData.Profession;
import com.legacy.rediscovered.entity.util.IPigman;
import com.legacy.rediscovered.entity.util.IPigmanDataHolder;
import com.legacy.rediscovered.entity.util.MountableBlockEntity;
import com.legacy.rediscovered.registry.RediscoveredDataSerialization;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.Dynamic;

import net.minecraft.core.BlockPos;
import net.minecraft.core.GlobalPos;
import net.minecraft.core.Holder;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.game.DebugPackets;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.stats.Stats;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.InteractGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.LookAtTradingPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveBackToVillageGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.ai.village.poi.PoiTypes;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.monster.ZombifiedPiglin;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.npc.VillagerData;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.schedule.Activity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.item.trading.MerchantOffers;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.level.pathfinder.Node;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.level.pathfinder.PathFinder;
import net.minecraft.world.level.pathfinder.WalkNodeEvaluator;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.event.EventHooks;

public class PigmanEntity extends AbstractVillager implements IPigman, IPigmanDataHolder
{
	protected static final EntityDataAccessor<PigmanData> PIGMAN_DATA = SynchedEntityData.defineId(PigmanEntity.class, RediscoveredDataSerialization.PIGMAN_DATA);
	protected static final EntityDataAccessor<Boolean> IMMUNE_TO_ZOMBIFICATION = SynchedEntityData.defineId(PigmanEntity.class, EntityDataSerializers.BOOLEAN);
	protected static final EntityDataAccessor<Integer> ZOMBIFICATION_TIME = SynchedEntityData.defineId(PigmanEntity.class, EntityDataSerializers.INT);

	public final Set<UUID> curedUuids = new HashSet<>();
	private int xp, timeUntilReset, numberOfRestocksToday;
	private long lastRestockGameTime, lastRestockCheckDayTime;
	private boolean customer, movingToRemembered;

	@Nullable
	public GlobalPos homePos, villageOrigin, jobSite, possibleJobSite;
	public boolean originFoundNaturally, jobSiteLocated;

	public int sitTimer;

	public PigmanEntity(EntityType<? extends PigmanEntity> type, Level world)
	{
		super(type, world);

		if (this.getNavigation()instanceof GroundPathNavigation p)
		{
			p.setCanOpenDoors(true);
			p.setCanFloat(true);
			p.setCanWalkOverFences(false);
		}

		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
		this.setPathfindingMalus(BlockPathTypes.FENCE, -5.0F);
		this.setPathfindingMalus(BlockPathTypes.TRAPDOOR, -1.0F);
	}

	@Override
	protected PathNavigation createNavigation(Level level)
	{
		return new GroundPathNavigation(this, level)
		{
			@Override
			protected PathFinder createPathFinder(int maxNodes)
			{
				this.nodeEvaluator = new PigmanWalkNodeEvaluator();
				this.nodeEvaluator.setCanPassDoors(true);
				this.nodeEvaluator.setCanWalkOverFences(false);
				return new PathFinder(this.nodeEvaluator, maxNodes)
				{
					@Override
					protected float distance(Node start, Node end)
					{
						return movingToRemembered ? 0 : start.distanceToXZ(end);
					}
				};
			}
		};
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		/*this.goalSelector.addGoal(0, new LocateHomeGoal(this));*/
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));

		this.goalSelector.addGoal(1, new PigmanGoals.TradeWithPlayerOffGroundGoal(this));
		this.goalSelector.addGoal(1, new LookAtTradingPlayerGoal(this));

		this.goalSelector.addGoal(3, new PanicGoal(this, 0.5D));

		this.goalSelector.addGoal(1, new GoToBedGoal(this));
		this.goalSelector.addGoal(1, new GoToMeetingGoal(this));
		this.goalSelector.addGoal(1, new GoToWorkGoal(this));

		this.goalSelector.addGoal(2, new MoveBackToVillageGoal(this, 0.6D, false));
		this.goalSelector.addGoal(8, new PigmanGoals.StrollWithVillageBiasGoal(this));
		this.goalSelector.addGoal(9, new InteractGoal(this, Player.class, 3.0F, 1.0F));
		this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Mob.class, 8.0F));

		this.goalSelector.addGoal(7, new PigmanGoals.FindAndSitInChairGoal<>(this));

		this.goalSelector.addGoal(3, new PigmanGoals.AvoidNetherReactorGoal(this));
		this.goalSelector.addGoal(3, new PigmanGoals.AvoidNetherPortalGoal(this));

		this.goalSelector.addGoal(0, new PigmanGoals.SearchForHomeGoal<>(this));
		this.goalSelector.addGoal(0, new PigmanGoals.SearchForMeetingPointGoal<>(this));
		this.goalSelector.addGoal(1, new PigmanGoals.SearchForJobGoal<>(this));
		this.goalSelector.addGoal(1, new PigmanGoals.AcquireJobSiteGoal<>(this));

		// Doctor can save itself
		this.goalSelector.addGoal(0, new PigmanGoals.DoctorDrinkAuraPotionGoal<>(this));

		this.addAvoidGoals(0.6F, 0.8F);
	}

	public void addAvoidGoals(float farSpeed, float closeSpeed)
	{
		this.targetSelector.addGoal(1, new AvoidEntityGoal<ZombifiedPiglin>(this, ZombifiedPiglin.class, 8.0F, farSpeed, closeSpeed));
		this.targetSelector.addGoal(1, new AvoidAndAlertGuardsGoal<>(this, Zombie.class, (e) -> !(e instanceof ZombifiedPiglin), 8.0F, farSpeed, closeSpeed));
	}

	@Override
	public boolean isWithinRestriction(BlockPos pos)
	{
		return super.isWithinRestriction(pos);
	}

	@Override
	protected void ageBoundaryReached()
	{
		super.ageBoundaryReached();
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return this.isBaby() ? 0.85F : 1.80F;
	}

	@Override
	public boolean removeWhenFarAway(double distanceToClosestPlayer)
	{
		return false;
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.FOLLOW_RANGE, 48.0D).add(Attributes.ATTACK_DAMAGE, 3.0D);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(PIGMAN_DATA, new PigmanData(PigmanData.Profession.NONE, 1));
		this.entityData.define(IMMUNE_TO_ZOMBIFICATION, false);
		this.entityData.define(ZOMBIFICATION_TIME, 0);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor pLevel, DifficultyInstance pDifficulty, MobSpawnType pReason, SpawnGroupData pSpawnData, CompoundTag pDataTag)
	{
		return super.finalizeSpawn(pLevel, pDifficulty, pReason, pSpawnData, pDataTag);
	}

	private static final String HOME_POS_KEY = "HomePosition", VILLAGE_ORIGIN_KEY = "VillageOrigin",
			JOB_SITE_KEY = "JobSite", POSSIBLE_JOB_SITE_KEY = "PossibleJobSite", CURED_UUIDS = "CuredUUIDs";

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		PigmanData.CODEC.encodeStart(NbtOps.INSTANCE, this.getPigmanData()).resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent((data) -> compound.put(PIGMAN_DATA_KEY, data));

		compound.putInt("Xp", this.xp);
		compound.putLong("LastRestock", this.lastRestockGameTime);
		compound.putInt("RestocksToday", this.numberOfRestocksToday);

		if (this.homePos != null)
			GlobalPos.CODEC.encodeStart(NbtOps.INSTANCE, this.homePos).resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent((data) -> compound.put(HOME_POS_KEY, data));

		if (this.villageOrigin != null)
			GlobalPos.CODEC.encodeStart(NbtOps.INSTANCE, this.villageOrigin).resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent((data) -> compound.put(VILLAGE_ORIGIN_KEY, data));

		if (this.jobSite != null)
			GlobalPos.CODEC.encodeStart(NbtOps.INSTANCE, this.jobSite).resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent((data) -> compound.put(JOB_SITE_KEY, data));

		if (this.possibleJobSite != null)
			GlobalPos.CODEC.encodeStart(NbtOps.INSTANCE, this.possibleJobSite).resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent((data) -> compound.put(POSSIBLE_JOB_SITE_KEY, data));

		compound.putBoolean("OriginFoundNaturally", this.originFoundNaturally);

		compound.putInt(ZOMBIFICATION_TIME_KEY, this.getConvertTime());
		compound.putBoolean(ZOMBIFICATION_IMMUNE_KEY, this.isImmuneToZombification());

		compound.putInt("SittingTime", this.sitTimer);

		if (!this.curedUuids.isEmpty())
		{
			ListTag victorsTag = new ListTag();
			this.curedUuids.forEach(victor -> victorsTag.add(NbtUtils.createUUID(victor)));
			compound.put(CURED_UUIDS, victorsTag);
		}
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);

		if (compound.contains(PIGMAN_DATA_KEY, Tag.TAG_COMPOUND))
		{
			DataResult<PigmanData> dataresult = PigmanData.CODEC.parse(new Dynamic<>(NbtOps.INSTANCE, compound.get(PIGMAN_DATA_KEY)));
			dataresult.resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent(this::setPigmanData);
		}

		if (compound.contains("Offers", Tag.TAG_COMPOUND))
			this.offers = new MerchantOffers(compound.getCompound("Offers"));

		if (compound.contains("Xp", Tag.TAG_INT))
			this.xp = compound.getInt("Xp");

		this.lastRestockGameTime = compound.getLong("LastRestock");
		this.numberOfRestocksToday = compound.getInt("RestocksToday");

		if (compound.contains(HOME_POS_KEY, Tag.TAG_COMPOUND))
		{
			DataResult<GlobalPos> dataresult = GlobalPos.CODEC.parse(new Dynamic<>(NbtOps.INSTANCE, compound.get(HOME_POS_KEY)));
			dataresult.resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent(r -> this.homePos = r);
		}

		if (compound.contains(VILLAGE_ORIGIN_KEY, Tag.TAG_COMPOUND))
		{
			DataResult<GlobalPos> dataresult = GlobalPos.CODEC.parse(new Dynamic<>(NbtOps.INSTANCE, compound.get(VILLAGE_ORIGIN_KEY)));
			dataresult.resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent(r -> this.villageOrigin = r);
		}

		if (compound.contains(JOB_SITE_KEY, Tag.TAG_COMPOUND))
		{
			DataResult<GlobalPos> dataresult = GlobalPos.CODEC.parse(new Dynamic<>(NbtOps.INSTANCE, compound.get(JOB_SITE_KEY)));
			dataresult.resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent(r -> this.jobSite = r);
		}

		if (compound.contains(POSSIBLE_JOB_SITE_KEY, Tag.TAG_COMPOUND))
		{
			DataResult<GlobalPos> dataresult = GlobalPos.CODEC.parse(new Dynamic<>(NbtOps.INSTANCE, compound.get(POSSIBLE_JOB_SITE_KEY)));
			dataresult.resultOrPartial(RediscoveredMod.LOGGER::error).ifPresent(r -> this.possibleJobSite = r);
		}

		this.originFoundNaturally = compound.getBoolean("OriginFoundNaturally");

		this.setConvertTime(compound.getInt(ZOMBIFICATION_TIME_KEY));
		this.setImmuneToZombification(compound.getBoolean(ZOMBIFICATION_IMMUNE_KEY));

		this.sitTimer = compound.getInt("SittingTime");

		if (compound.contains(CURED_UUIDS, Tag.TAG_LIST))
			compound.getList(CURED_UUIDS, Tag.TAG_INT_ARRAY).forEach(t -> this.curedUuids.add(NbtUtils.loadUUID(t)));
	}

	@Override
	public float getWalkTargetValue(BlockPos pos, LevelReader reader)
	{
		/*boolean isDay = this.level().isDay() || this.level().dimensionType().hasFixedTime();
		
		if (this.homePos != null && !isDay && this.level().getBrightness(LightLayer.SKY, pos) <= 2)
			return -3.0F;*/

		return super.getWalkTargetValue(pos, reader);
	}

	@Override
	public void tick()
	{
		super.tick();

		/*if (this.villageCenter != null && this.getPosDistance(this.villageCenter) > 130)
			this.villageCenter = null;
		else if (this.homePos != null && this.getPosDistance(this.homePos) > 200 && !this.isWithinRestriction())
			this.homePos = null;
		
		int restrictDist = !this.homeFoundNaturally && (!isDay || this.level().isThundering()) ? (this.level().canSeeSky(this.blockPosition()) ? 1 : 5) : this.restrictDistance;
		if (this.villageCenter != null && isDay && !this.level().isThundering())
			this.restrictTo(this.villageCenter, restrictDist);
		else if (this.homePos != null)
			this.restrictTo(this.homePos, restrictDist);
		else
			this.clearRestriction();*/

		/*if (this.homePos != null)
		{
			if (this.getPosDistance(this.homePos) < 2)
			{
				this.startSleeping(this.homePos);
			}
			else
			this.navigation.moveTo(this.homePos.getX(), this.homePos.getY(), this.homePos.getZ(), 0.6);
		}*/

		if (this.level().getDayTime() % 6000 == 0 && this.level().isDay() && this.shouldRestock())
		{
			SoundEvent workSound = this.getPigmanData().getProfession().workSound;
			if (workSound != null)
				this.playSound(workSound);

			this.restock();
		}

		/*if (this.tickCount % 20 == 0)
			this.swing(InteractionHand.MAIN_HAND);*/
	}

	@Override
	protected void customServerAiStep()
	{
		if (this.navigation.isDone())
			this.setMovingToRemembered(false);

		if (!this.isTrading() && this.timeUntilReset > 0)
		{
			--this.timeUntilReset;

			if (this.timeUntilReset <= 0)
			{
				if (this.customer)
				{
					this.levelUp();
					this.customer = false;
				}

				this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 200, 0));
			}
		}

		if (this.level()instanceof ServerLevel sl)
		{
			if (this.homePos != null && (!this.level().dimension().equals(this.homePos.dimension()) || !sl.getPoiManager().existsAtPosition(PoiTypes.HOME, this.homePos.pos())))
			{
				this.releaseHome();
				this.level().broadcastEntityEvent(this, (byte) 13);
			}

			if (this.villageOrigin != null && (!sl.dimension().equals(this.villageOrigin.dimension()) || !sl.getPoiManager().existsAtPosition(PoiTypes.MEETING, this.villageOrigin.pos())))
			{
				this.releaseOrigin();

				this.villageOrigin = null;
				this.originFoundNaturally = false;
			}

			// release the potential job if aquiring it becomes impossible
			/*if (this.possibleJobSite != null && (!this.isPathFinding() || !sl.dimension().equals(this.possibleJobSite.dimension()) || !sl.getPoiManager().exists(this.possibleJobSite.pos(), p -> p.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION))))
			{
				// this.releasePossibleJobSite();
			}*/

			var data = this.getPigmanData();

			if (this.jobSite != null && (this.isBaby() || !sl.dimension().equals(this.jobSite.dimension()) || !sl.getPoiManager().exists(this.jobSite.pos(), p -> p.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION))/* || data != null && (!this.jobSiteLocated && !sl.getPoiManager().exists(this.jobSite.pos(), data.getProfession().jobSite))*/))
			{
				this.releaseJobSite();

				this.jobSite = null;
			}

			if (data != null && this.getVillagerXp() <= 0 && data.getLevel() <= 1 && data.getProfession() != PigmanData.Profession.NONE && this.jobSite == null)
				this.setPigmanData(data.setProfession(PigmanData.Profession.NONE));

			if (data != null && this.jobSite != null && this.getCurrentActivity() == Activity.WORK)
			{
				this.restrictTo(this.jobSite.pos(), 10);
			}
			else
				this.clearRestriction();
		}

		super.customServerAiStep();

		if (this.shouldConvert(this))
			this.setConvertTime(this.getConvertTime() + 1);
		else
			this.setConvertTime(0);

		if (this.getConvertTime() > NATURAL_CONVERSION_TIME && EventHooks.canLivingConvert(this, RediscoveredEntityTypes.ZOMBIE_PIGMAN, (timer) -> this.setConvertTime(timer)))
			IPigman.zombify(this, null);

		boolean inChair = this.getVehicle() instanceof MountableBlockEntity;

		if (this.isPathFinding())
			this.sitTimer = 0;

		if (inChair && !this.isTrading() && --this.sitTimer <= 0)
			this.stopRiding();

		if (!inChair && this.isTrading() && this.distanceTo(this.getTradingPlayer()) > 1.5F)
			this.navigation.moveTo(this.getTradingPlayer(), 0.5F);

		if (this.isSleeping() && (this.getCurrentActivity() != Activity.REST))
			this.stopSleeping();
	}

	@Override
	public void aiStep()
	{
		this.updateSwingTime();
		super.aiStep();
	}

	@OnlyIn(Dist.CLIENT)
	public void handleEntityEvent(byte id)
	{
		switch (id)
		{
		case 12:
			this.addParticlesAroundSelf(ParticleTypes.HEART);
			break;
		case 13:
			this.addParticlesAroundSelf(ParticleTypes.ANGRY_VILLAGER);
			break;
		case 14:
			this.addParticlesAroundSelf(ParticleTypes.HAPPY_VILLAGER);
			break;
		case 42:
			this.addParticlesAroundSelf(ParticleTypes.SPLASH);
			break;
		default:
			super.handleEntityEvent(id);
			break;
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return this.isSleeping() ? null : RediscoveredSounds.ENTITY_PIGMAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return RediscoveredSounds.ENTITY_PIGMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_PIGMAN_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.PIG_STEP, 0.15F, 1.0F);
	}

	@Override
	public SoundEvent getNotifyTradeSound()
	{
		return RediscoveredSounds.ENTITY_PIGMAN_AGREE;
	}

	@Override
	protected SoundEvent getTradeUpdatedSound(boolean positive)
	{
		return positive ? RediscoveredSounds.ENTITY_PIGMAN_AGREE : RediscoveredSounds.ENTITY_PIGMAN_DISAGREE;
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel level, AgeableMob otherParent)
	{
		PigmanEntity baby = RediscoveredEntityTypes.PIGMAN.create(level);
		baby.setAge(Integer.MAX_VALUE);

		baby.villageOrigin = this.villageOrigin;
		baby.originFoundNaturally = true;

		return baby;
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		/*try
		{
			// XXX Pigman Profession Debug
			if (this.getOffers().isEmpty() && player.isCreative() && player.isSecondaryUseActive())
			{
				if (this.level().isClientSide())
					return InteractionResult.SUCCESS;
		
				for (var prof : PigmanData.Profession.values())
				{
					if (prof != Profession.NONE)
					{
						if (itemstack.getItem()instanceof BlockItem block && PoiTypes.hasPoi(block.getBlock().defaultBlockState()) && prof.usableJobSites.test(PoiTypes.forState(block.getBlock().defaultBlockState()).get()))
						{
							this.setPigmanData(new PigmanData(prof, 1));
							return InteractionResult.SUCCESS;
						}
		
					}
				}
				this.setPigmanData(new PigmanData(Profession.BOWYER, 1));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}*/

		if (itemstack.getItem() != RediscoveredItems.pigman_spawn_egg && this.isAlive() && !this.isSleeping() && !this.isBaby() && !player.isSecondaryUseActive())
		{
			if (hand == InteractionHand.MAIN_HAND)
				player.awardStat(Stats.TALKED_TO_VILLAGER);

			if (this.getOffers().isEmpty())
			{
				this.playSound(this.getTradeUpdatedSound(false), this.getSoundVolume(), this.getVoicePitch());
				return InteractionResult.sidedSuccess(this.level().isClientSide());
			}
			else
			{
				if (!this.level().isClientSide() && !this.offers.isEmpty())
				{
					this.updateSpecialPrices(player);
					this.setTradingPlayer(player);
					this.openTradingScreen(player, this.getDisplayName(), this.getPigmanData().getLevel());
				}

				return InteractionResult.sidedSuccess(this.level().isClientSide());
			}
		}
		else
		{
			return super.mobInteract(player, hand);
		}
	}

	@Override
	protected void stopTrading()
	{
		super.stopTrading();
		this.resetSpecialPrices();
	}

	private void levelUp()
	{
		this.setPigmanData(this.getPigmanData().setLevel(this.getPigmanData().getLevel() + 1));
		this.updateTrades();
	}

	// VILLAGER START
	@Override
	public void setTradingPlayer(@Nullable Player pPlayer)
	{
		boolean flag = this.getTradingPlayer() != null && pPlayer == null;
		super.setTradingPlayer(pPlayer);

		if (flag)
			this.stopTrading();
	}

	private void resetSpecialPrices()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
			merchantoffer.resetSpecialPriceDiff();
	}

	public void restock()
	{
		this.updateDemand();

		for (MerchantOffer merchantoffer : this.getOffers())
		{
			merchantoffer.resetUses();
		}

		this.lastRestockGameTime = this.level().getGameTime();
		++this.numberOfRestocksToday;
	}

	private boolean needsToRestock()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
		{
			if (merchantoffer.needsRestock())
				return true;
		}

		return false;
	}

	private boolean allowedToRestock()
	{
		return this.numberOfRestocksToday == 0 || this.numberOfRestocksToday < 2 && this.level().getGameTime() > this.lastRestockGameTime + 2400L;
	}

	public boolean shouldRestock()
	{
		if (this.getOffers().isEmpty())
			return false;

		long i = this.lastRestockGameTime + 12000L;
		long j = this.level().getGameTime();
		boolean flag = j > i;
		long k = this.level().getDayTime();
		if (this.lastRestockCheckDayTime > 0L)
		{
			long l = this.lastRestockCheckDayTime / 24000L;
			long i1 = k / 24000L;
			flag |= i1 > l;
		}

		this.lastRestockCheckDayTime = k;
		if (flag)
		{
			this.lastRestockGameTime = j;
			this.resetNumberOfRestocks();
		}

		return this.allowedToRestock() && this.needsToRestock();
	}

	private void catchUpDemand()
	{
		int i = 2 - this.numberOfRestocksToday;

		if (i > 0)
		{
			for (MerchantOffer merchantoffer : this.getOffers())
				merchantoffer.resetUses();
		}

		for (int j = 0; j < i; ++j)
			this.updateDemand();

	}

	private void updateDemand()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
			merchantoffer.updateDemand();
	}

	private void resetNumberOfRestocks()
	{
		this.catchUpDemand();
		this.numberOfRestocksToday = 0;
	}

	@Override
	public boolean canRestock()
	{
		return true;
	}

	public void setOffers(MerchantOffers pOffers)
	{
		this.offers = pOffers;
	}
	// VILLAGER END

	@Override
	protected void rewardTradeXp(MerchantOffer offer)
	{
		int i = 3 + this.random.nextInt(4);
		this.xp += offer.getXp();

		if (this.canLevelUp())
		{
			this.timeUntilReset = 40;
			this.customer = true;
			i += 5;
		}

		if (offer.shouldRewardExp())
			this.level().addFreshEntity(new ExperienceOrb(this.level(), this.getX(), this.getY() + 0.5D, this.getZ(), i));
	}

	@Override
	protected void updateTrades()
	{
		PigmanData data = this.getPigmanData();
		var trades = data.getProfession().trades;

		if (trades != null && !trades.isEmpty())
		{
			VillagerTrades.ItemListing[] tradeListing = trades.get(data.getLevel());

			if (tradeListing != null)
				this.addOffersFromItemListings(this.getOffers(), tradeListing, 2);
		}
	}

	private boolean canLevelUp()
	{
		int i = this.getPigmanData().getLevel();
		return VillagerData.canLevelUp(i) && this.xp >= VillagerData.getMaxXpPerLevel(i);
	}

	public void setPigmanData(PigmanData data)
	{
		PigmanData currentData = this.getPigmanData();

		if (currentData.getProfession() != data.getProfession())
			this.offers = null;

		this.entityData.set(PIGMAN_DATA, data);
	}

	@Override
	public PigmanData getPigmanData()
	{
		return this.entityData.get(PIGMAN_DATA);
	}

	@Override
	public int getVillagerXp()
	{
		return this.xp;
	}

	public void setXp(int amount)
	{
		this.xp = amount;
	}

	public void setImmuneToZombification(boolean flag)
	{
		this.getEntityData().set(IMMUNE_TO_ZOMBIFICATION, flag);
	}

	public boolean isImmuneToZombification()
	{
		return this.getEntityData().get(IMMUNE_TO_ZOMBIFICATION);
	}

	public void setConvertTime(int flag)
	{
		this.getEntityData().set(ZOMBIFICATION_TIME, flag);
	}

	public int getConvertTime()
	{
		return this.getEntityData().get(ZOMBIFICATION_TIME);
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		this.sitTimer = 0;
		return super.hurt(source, amount);
	}

	@Override
	public void die(DamageSource cause)
	{
		this.releaseAllPois();

		super.die(cause);
	}

	public double getPosDistance(BlockPos pos)
	{
		return getPosDistance(Vec3.atBottomCenterOf(pos));
	}

	public double getPosDistance(Vec3 vec)
	{
		return getPosDistance(vec.x(), vec.y(), vec.z());
	}

	public double getPosDistance(double x, double y, double z)
	{
		double f = this.getX() - x;
		double f1 = this.getY() - y;
		double f2 = this.getZ() - z;
		return Math.sqrt(f * f + f1 * f1 + f2 * f2);
	}

	@Override
	public boolean isAlliedTo(Entity entityIn)
	{
		return entityIn.getType().is(RediscoveredTags.Entities.PIGMAN_ALLIES) || super.isAlliedTo(entityIn.getTeam());
	}

	@Override
	public boolean canBreed()
	{
		return this.getAge() == 0;
	}

	@Override
	public void startSleeping(BlockPos pos)
	{
		super.startSleeping(pos);
	}

	@Override
	public IPigman.Type getPigmanType()
	{
		return IPigman.Type.TRADER;
	}

	@Override
	protected Component getTypeName()
	{
		Profession prof = this.getPigmanData().getProfession();

		if (prof != Profession.NONE)
			return Component.translatable(this.getType().getDescriptionId() + "." + prof.profName);

		return super.getTypeName();
	}

	@Override
	protected float ridingOffset(Entity pEntity)
	{
		return -0.7F;
	}

	protected void releaseHome()
	{
		if (this.homePos != null)
		{
			this.releasePoi(p -> p.is(PoiTypes.HOME), this.homePos);

			this.homePos = null;
		}
	}

	protected void releaseOrigin()
	{
		if (this.villageOrigin != null && this.originFoundNaturally)
		{
			this.releasePoi(p -> p.is(PoiTypes.MEETING), this.villageOrigin);

			this.originFoundNaturally = false;
			this.villageOrigin = null;
		}
	}

	protected void releaseJobSite()
	{
		var data = this.getPigmanData();
		if (this.jobSite != null)
		{
			if (data != null)
				this.releasePoi(p -> p.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION), this.jobSite);

			this.jobSite = null;
		}
	}

	public void releasePossibleJobSite()
	{
		if (this.possibleJobSite != null)
		{
			this.releasePoi(p -> p.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION), this.possibleJobSite);
			this.possibleJobSite = null;
		}
	}

	public void releaseAllPois()
	{
		this.releaseHome();
		this.releaseOrigin();
		this.releaseJobSite();
		this.releasePossibleJobSite();
	}

	public void releasePoi(Predicate<Holder<PoiType>> poi, @Nullable GlobalPos pos)
	{
		if (pos == null)
			return;

		if (this.level()instanceof ServerLevel sl)
		{
			MinecraftServer minecraftserver = sl.getServer();
			ServerLevel serverlevel = minecraftserver.getLevel(pos.dimension());
			if (serverlevel != null)
			{
				PoiManager poimanager = serverlevel.getPoiManager();
				Optional<Holder<PoiType>> optional = poimanager.getType(pos.pos());

				if (optional.isPresent() && poi.test(optional.get()))
				{
					poimanager.release(pos.pos());
					DebugPackets.sendPoiTicketCountPacket(serverlevel, pos.pos());
				}

			}
		}
	}

	public Activity getCurrentActivity()
	{
		return this.getPigmanData().getProfession().getSchedule().getActivityAt((int) (this.level().dayTime() % 24000L));
	}

	public void setMovingToRemembered(boolean movingToRemembered)
	{
		this.movingToRemembered = movingToRemembered;
	}

	private void updateSpecialPrices(Player player)
	{
		int i = this.curedUuids.contains(player.getUUID()) ? 20 : 0;
		if (i != 0)
		{
			for (MerchantOffer merchantoffer : this.getOffers())
			{
				System.out.println(merchantoffer.getResult() + " " + merchantoffer.getPriceMultiplier());
				merchantoffer.addToSpecialPriceDiff(-Mth.floor((float) i * merchantoffer.getPriceMultiplier()));
			}
		}

		/*if (pPlayer.hasEffect(MobEffects.HERO_OF_THE_VILLAGE))
		{
			MobEffectInstance mobeffectinstance = pPlayer.getEffect(MobEffects.HERO_OF_THE_VILLAGE);
			int k = mobeffectinstance.getAmplifier();
		
			for (MerchantOffer merchantoffer1 : this.getOffers())
			{
				double d0 = 0.3D + 0.0625D * (double) k;
				int j = (int) Math.floor(d0 * (double) merchantoffer1.getBaseCostA().getCount());
				merchantoffer1.addToSpecialPriceDiff(-Math.max(j, 1));
			}
		}*/
	}

	class GoToBedGoal extends Goal
	{
		final PigmanEntity mob;

		public GoToBedGoal(PigmanEntity mob)
		{
			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
			this.mob = mob;
		}

		@Override
		public boolean canUse()
		{
			return this.mob.homePos != null && this.mob.getCurrentActivity() == Activity.REST;
		}

		@Override
		public void tick()
		{
			if (this.mob.homePos == null || this.mob.isSleeping())
				return;

			Vec3 vec = Vec3.atCenterOf(this.mob.homePos.pos());

			if (this.mob.navigation.isDone())
			{
				this.mob.setMovingToRemembered(true);

				Path path = this.mob.getNavigation().createPath(vec.x(), vec.y(), vec.z(), 1);

				if (path != null)
					this.mob.getNavigation().moveTo(path, 0.5F);
			}

			if (Mth.sqrt((float) this.mob.distanceToSqr(vec)) <= 2.0F)
			{
				this.mob.startSleeping(this.mob.homePos.pos());
				this.mob.navigation.stop();
			}
		}

		@Override
		public void stop()
		{
			this.mob.getNavigation().stop();
		}
	}

	class GoToMeetingGoal extends Goal
	{
		final PigmanEntity mob;

		public GoToMeetingGoal(PigmanEntity mob)
		{
			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
			this.mob = mob;
		}

		@Override
		public boolean canUse()
		{
			return this.mob.villageOrigin != null && this.mob.getPosDistance(this.mob.villageOrigin.pos()) > 20 && this.mob.getCurrentActivity() == Activity.MEET;
		}

		@Override
		public void tick()
		{
			if (this.mob.villageOrigin == null)
				return;

			Vec3 vec = Vec3.atCenterOf(this.mob.villageOrigin.pos());

			if (this.mob.navigation.isDone())
			{
				this.mob.setMovingToRemembered(true);

				Path path = this.mob.getNavigation().createPath(vec.x(), vec.y(), vec.z(), 1);

				if (path != null)
					this.mob.getNavigation().moveTo(path, 0.5F);
			}

		}

		@Override
		public void stop()
		{
			this.mob.getNavigation().stop();
		}
	}

	class GoToWorkGoal extends Goal
	{
		final PigmanEntity mob;

		public GoToWorkGoal(PigmanEntity mob)
		{
			this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
			this.mob = mob;
		}

		@Override
		public boolean canUse()
		{
			return this.mob.jobSite != null && this.mob.getPosDistance(this.mob.jobSite.pos()) > 10 && this.mob.getCurrentActivity() == Activity.WORK;
		}

		@Override
		public void tick()
		{
			if (this.mob.jobSite == null)
				return;

			Vec3 vec = Vec3.atCenterOf(this.mob.jobSite.pos());

			if (this.mob.navigation.isDone())
			{
				this.mob.setMovingToRemembered(true);

				Path path = this.mob.getNavigation().createPath(vec.x(), vec.y(), vec.z(), 1);

				if (path != null)
					this.mob.getNavigation().moveTo(path, 0.5F);
			}
		}

		@Override
		public void stop()
		{
			this.mob.getNavigation().stop();
		}
	}

	class AvoidAndAlertGuardsGoal<T extends LivingEntity> extends AvoidEntityGoal<T>
	{
		public AvoidAndAlertGuardsGoal(PathfinderMob pMob, Class<T> pEntityClassToAvoid, Predicate<LivingEntity> pAvoidPredicate, float pMaxDistance, double pWalkSpeedModifier, double pSprintSpeedModifier)
		{
			super(pMob, pEntityClassToAvoid, pAvoidPredicate, pMaxDistance, pWalkSpeedModifier, pSprintSpeedModifier, EntitySelector.NO_CREATIVE_OR_SPECTATOR::test);
		}

		@Override
		public void tick()
		{
			super.tick();

			if (this.toAvoid != null && this.mob.tickCount % 20 <= 2)
			{
				double range = 20;
				Mob nearestGuard = this.mob.level().getNearestEntity(Mob.class, TargetingConditions.forNonCombat().ignoreLineOfSight().range(range).ignoreInvisibilityTesting().selector(e -> e.getType().is(RediscoveredTags.Entities.PIGMAN_VILLAGE_GUARDS)), this.mob, this.mob.getX(), this.mob.getY(), this.mob.getZ(), this.mob.getBoundingBox().inflate(range));

				if (nearestGuard != null)
					nearestGuard.setTarget(this.toAvoid);
			}
		}
	}

	private class PigmanWalkNodeEvaluator extends WalkNodeEvaluator
	{
		@Override
		public BlockPathTypes getBlockPathType(BlockGetter pLevel, int pX, int pY, int pZ)
		{
			BlockPathTypes belowType = getBlockPathTypeStatic(pLevel, new BlockPos.MutableBlockPos(pX, pY - 1, pZ)),
					type = getBlockPathTypeStatic(pLevel, new BlockPos.MutableBlockPos(pX, pY, pZ));

			if (belowType == BlockPathTypes.FENCE && type == BlockPathTypes.WALKABLE)
				return BlockPathTypes.FENCE;

			return type;
		}
	}

}