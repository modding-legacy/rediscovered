package com.legacy.rediscovered.entity.pigman.data;

import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.ai.village.poi.PoiTypes;
import net.minecraft.world.entity.npc.VillagerData;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.schedule.Schedule;
import net.neoforged.neoforge.common.IExtensibleEnum;

public class PigmanData
{
	public static final Predicate<Holder<PoiType>> ALL_ACQUIRABLE_JOBS = (holder) -> holder.is(RediscoveredTags.Poi.APPLICABLE_PIGMAN_WORKSTATION);

	private static final int[] NEXT_LEVEL_XP_THRESHOLDS = new int[] { 0, 10, 70, 150, 250 };
	public static final Codec<PigmanData> CODEC = RecordCodecBuilder.create((codec) ->
	{
		return codec.group(Codec.STRING.fieldOf("profession").orElseGet(() -> Profession.NONE.profName).forGetter((data) -> data.profession.profName), Codec.INT.fieldOf("level").orElse(1).forGetter((data) -> data.level)).apply(codec, PigmanData::new);
	});
	private final Profession profession;
	private final int level;

	public PigmanData(Profession profession, int level)
	{
		this.profession = profession;
		this.level = Math.max(1, level);
	}

	public PigmanData(String professionName, int level)
	{
		this(Profession.fromName(professionName), level);
	}

	public Profession getProfession()
	{
		return this.profession;
	}

	public int getLevel()
	{
		return this.level;
	}

	public PigmanData setProfession(Profession profession)
	{
		return new PigmanData(profession, this.level);
	}

	public PigmanData setLevel(int level)
	{
		return new PigmanData(this.profession, level);
	}

	public static int getMinXpPerLevel(int level)
	{
		return canLevelUp(level) ? NEXT_LEVEL_XP_THRESHOLDS[level - 1] : 0;
	}

	public static int getMaxXpPerLevel(int level)
	{
		return canLevelUp(level) ? NEXT_LEVEL_XP_THRESHOLDS[level] : 0;
	}

	public static boolean canLevelUp(int level)
	{
		return level >= VillagerData.MIN_VILLAGER_LEVEL && level < VillagerData.MAX_VILLAGER_LEVEL;
	}

	public static enum Profession implements IExtensibleEnum
	{
		// @formatter:off
		NONE("none", PoiType.NONE, ALL_ACQUIRABLE_JOBS, null, null, true, false),
		METALWORKER("metalworker", PoiTypes.TOOLSMITH, PigmanTrades.METALWORKER_OFFERS, RediscoveredSounds.ENTITY_PIGMAN_WORK_METALWORKER, true, true),
		BOWYER("bowyer", PoiTypes.FLETCHER, PigmanTrades.BOWYER_OFFERS, RediscoveredSounds.ENTITY_PIGMAN_WORK_BOWYER, false, false),
		TECHNICIAN("technician", PoiTypes.LIBRARIAN, PigmanTrades.TECHNICIAN_OFFERS, RediscoveredSounds.ENTITY_PIGMAN_WORK_TECHNICIAN, false, false),
		TAILOR("tailor", PoiTypes.SHEPHERD, PigmanTrades.TAILOR_OFFERS, RediscoveredSounds.ENTITY_PIGMAN_WORK_TAILOR, false, false),
		DOCTOR("doctor", PoiTypes.CLERIC, PigmanTrades.DOCTOR_OFFERS, RediscoveredSounds.ENTITY_PIGMAN_WORK_DOCTOR, false, false);
		// @formatter:on

		public final String profName;
		public final Predicate<Holder<PoiType>> jobSite, usableJobSites;
		public final Int2ObjectMap<VillagerTrades.ItemListing[]> trades;

		@Nullable
		public final SoundEvent workSound;
		public final boolean renderZombiePigmanLoincloth, renderLevelOnJacket;

		private Profession(String name, Predicate<Holder<PoiType>> jobSite, Predicate<Holder<PoiType>> usableJobSites, Int2ObjectMap<VillagerTrades.ItemListing[]> trades, @Nullable SoundEvent workSound, boolean renderZombiePigmanLoincloth, boolean renderLevelOnJacket)
		{
			this.profName = name;
			this.jobSite = jobSite;
			this.usableJobSites = usableJobSites;
			this.trades = trades;
			this.workSound = workSound;
			this.renderZombiePigmanLoincloth = renderZombiePigmanLoincloth;
			this.renderLevelOnJacket = renderLevelOnJacket;
		}

		private Profession(String name, ResourceKey<PoiType> jobSite, Int2ObjectMap<VillagerTrades.ItemListing[]> trades, @Nullable SoundEvent workSound, boolean renderZombiePigmanLoincloth, boolean renderLevelOnJacket)
		{
			this(name, h -> h.is(jobSite), h -> h.is(jobSite), trades, workSound, renderZombiePigmanLoincloth, renderLevelOnJacket);
		}

		/**
		 * Other mods could technically make their own professions with this
		 * 
		 * @see IExtensibleEnum
		 */
		public static Profession create(String name, String profName, Predicate<Holder<PoiType>> jobSite, Predicate<Holder<PoiType>> usableJobSites, Int2ObjectMap<VillagerTrades.ItemListing[]> trades, @Nullable SoundEvent workSound, boolean renderZombiePigmanLoincloth, boolean renderLevelOnJacket)
		{
			throw new IllegalStateException("Enum not extended");
		}

		public static Profession fromName(String name)
		{
			for (Profession type : values())
				if (name.equals(type.profName))
					return type;

			return NONE;
		}

		public Schedule getSchedule()
		{
			return Schedule.VILLAGER_DEFAULT;
		}
	}
}