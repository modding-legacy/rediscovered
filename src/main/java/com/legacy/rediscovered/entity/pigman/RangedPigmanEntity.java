package com.legacy.rediscovered.entity.pigman;

import javax.annotation.Nullable;

import com.legacy.rediscovered.entity.pigman.ai.PigmanCrossbowAttackGoal;
import com.legacy.rediscovered.entity.util.IPigman;
import com.legacy.rediscovered.item.util.AttachedItem;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.RangedBowAttackGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.monster.CrossbowAttackMob;
import net.minecraft.world.entity.monster.RangedAttackMob;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.projectile.ProjectileUtil;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.CrossbowItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.neoforged.neoforge.common.CommonHooks;

public class RangedPigmanEntity extends GuardPigmanEntity implements RangedAttackMob, CrossbowAttackMob
{
	private static final EntityDataAccessor<Boolean> CHARGING_CROSSBOW = SynchedEntityData.defineId(RangedPigmanEntity.class, EntityDataSerializers.BOOLEAN);

	public RangedPigmanEntity(EntityType<? extends RangedPigmanEntity> type, Level world)
	{
		super(type, world);
		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(CHARGING_CROSSBOW, false);
	}

	@Override
	public void registerAdditionalGoals()
	{
		this.goalSelector.addGoal(2, new RangedBowAttackGoal<>(this, 0.65D, 20, 15.0F));
		this.goalSelector.addGoal(2, new PigmanCrossbowAttackGoal<>(this, 1.0D, 8.0F));
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected void populateDefaultEquipmentSlots(RandomSource random, DifficultyInstance difficulty)
	{
		if (random.nextBoolean())
		{
			this.setItemSlot(EquipmentSlot.HEAD, RediscoveredItems.plate_helmet.getDefaultInstance());
			this.setItemSlot(EquipmentSlot.CHEST, AttachedItem.attachItem(RediscoveredItems.plate_chestplate.getDefaultInstance(), RediscoveredItems.quiver.getDefaultInstance()));
		}
		else
		{
			this.setItemSlot(EquipmentSlot.HEAD, RediscoveredItems.studded_helmet.getDefaultInstance());
			this.setItemSlot(EquipmentSlot.CHEST, RediscoveredItems.quiver.getDefaultInstance());
		}

		this.setItemSlot(EquipmentSlot.MAINHAND, random.nextFloat() < 0.40F ? Items.CROSSBOW.getDefaultInstance() : Items.BOW.getDefaultInstance());
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return GuardPigmanEntity.createBasePigmanAttributes().add(Attributes.MAX_HEALTH, 20.0D);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public boolean doHurtTarget(Entity entity)
	{
		return super.doHurtTarget(entity);
	}

	public boolean isChargingCrossbow()
	{
		return this.entityData.get(CHARGING_CROSSBOW);
	}

	public void setChargingCrossbow(boolean charging)
	{
		this.entityData.set(CHARGING_CROSSBOW, charging);
	}

	public void onCrossbowAttackPerformed()
	{
		this.noActionTime = 0;
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		return true;
	}

	@Override
	public GuardPigmanEntity.PigmanArmPose getArmPose()
	{
		if (this.isHolding(is -> is.getItem() instanceof BowItem) && this.isAggressive())
			return GuardPigmanEntity.PigmanArmPose.BOW_AND_ARROW;
		else if (this.isHolding(is -> is.getItem() instanceof CrossbowItem))
			return this.isChargingCrossbow() ? GuardPigmanEntity.PigmanArmPose.CROSSBOW_CHARGE : GuardPigmanEntity.PigmanArmPose.CROSSBOW_HOLD;

		return GuardPigmanEntity.PigmanArmPose.NEUTRAL;
	}

	@Override
	public IPigman.Type getPigmanType()
	{
		return IPigman.Type.RANGED;
	}

	/*private static final Lazy<ItemStack> DEFAULT_ARROW = Lazy.of(() ->
	{
		var stack = RediscoveredItems.purple_arrow.getDefaultInstance();
		stack.addTagElement("pigman_marker", new CompoundTag());
		return stack;
	});*/

	@Override
	public void performRangedAttack(LivingEntity target, float distanceFactor)
	{
		if (this.isHolding(is -> is.getItem() instanceof CrossbowItem))
		{
			this.performCrossbowAttack(this, 1.6F);
			return;
		}

		ItemStack shotStack = this.getProjectile(this.getMainHandItem());
		AbstractArrow arrow = ProjectileUtil.getMobArrow(this, shotStack, distanceFactor);

		double d0 = target.getX() - this.getX();
		double d1 = target.getBoundingBox().minY + (double) (target.getBbHeight() / 3.0F) - arrow.getY();
		double d2 = target.getZ() - this.getZ();
		double d3 = (double) Mth.sqrt((float) (d0 * d0 + d2 * d2));
		arrow.shoot(d0, d1 + d3 * (double) 0.2F, d2, 1.6F, (float) (14 - this.level().getDifficulty().getId() * 4));
		this.playSound(SoundEvents.ARROW_SHOOT, 1.0F, 1.0F / (this.getRandom().nextFloat() * 0.4F + 0.8F));
		this.level().addFreshEntity(arrow);

		if (this.isPlayerCured())
		{
			this.getMainHandItem().hurtAndBreak(1, this, e ->
			{
			});

			shotStack.shrink(1);
		}
	}

	@Override
	public void shootCrossbowProjectile(LivingEntity pTarget, ItemStack pCrossbowStack, Projectile pProjectile, float pProjectileAngle)
	{
		this.shootCrossbowProjectile(this, pTarget, pProjectile, pProjectileAngle, 1.6F);
	}

	@Override
	public ItemStack getProjectile(ItemStack weapon)
	{
		ItemStack offhand = this.getItemBySlot(EquipmentSlot.OFFHAND);
		return CommonHooks.getProjectile(this, weapon, offhand.is(ItemTags.ARROWS) || weapon.getItem() instanceof CrossbowItem && offhand.is(Items.FIREWORK_ROCKET) ? offhand : RediscoveredItems.purple_arrow.getDefaultInstance());
	}
}