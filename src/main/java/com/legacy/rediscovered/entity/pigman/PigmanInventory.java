package com.legacy.rediscovered.entity.pigman;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ItemStack;

public class PigmanInventory extends SimpleContainer
{
	private final GuardPigmanEntity pigman;

	public PigmanInventory(GuardPigmanEntity pigman, int size)
	{
		super(size);
		this.pigman = pigman;
	}

	public void setItemNoUpdate(int pIndex, ItemStack pStack)
	{
		this.items.set(pIndex, pStack);
		if (!pStack.isEmpty() && pStack.getCount() > this.getMaxStackSize())
		{
			pStack.setCount(this.getMaxStackSize());
		}
	}

	/**
	 * Based on the player's Inventory
	 */
	public void damageArmor(DamageSource source, float damage)
	{
		if (this.pigman == null)
			return;

		if (!(damage <= 0.0F))
		{
			damage /= 4.0F;

			if (damage < 1.0F)
				damage = 1.0F;

			for (int i = 0; i < 4; ++i)
			{
				ItemStack itemstack = this.items.get(2 + i);
				if ((!source.is(DamageTypeTags.IS_FIRE) || !itemstack.getItem().isFireResistant()) && itemstack.getItem() instanceof ArmorItem)
				{
					final int slot = i;
					itemstack.hurtAndBreak((int) damage, this.pigman, (e) -> e.broadcastBreakEvent(EquipmentSlot.byTypeAndIndex(EquipmentSlot.Type.ARMOR, slot)));
				}
			}

		}
	}

	// allow loading to empty slots
	@Override
	public void fromTag(ListTag pContainerNbt)
	{
		this.clearContent();

		for (int i = 0; i < pContainerNbt.size(); ++i)
		{
			ItemStack itemstack = ItemStack.of(pContainerNbt.getCompound(i));
			if (!itemstack.isEmpty())
			{
				this.setItem(i, itemstack);
			}
		}

	}

	// allow saving empty slots
	@Override
	public ListTag createTag()
	{
		ListTag listtag = new ListTag();

		for (int i = 0; i < this.getContainerSize(); ++i)
		{
			ItemStack itemstack = this.getItem(i);
			listtag.add(itemstack.save(new CompoundTag()));
		}

		return listtag;
	}

}
