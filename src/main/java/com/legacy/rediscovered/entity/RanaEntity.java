package com.legacy.rediscovered.entity;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.data.RediscoveredLootProv;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.ai.SearchForItemGoal;
import com.legacy.rediscovered.registry.RediscoveredTriggers;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundTakeItemEntityPacket;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.behavior.BehaviorUtils;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.event.EventHooks;

public class RanaEntity extends PathfinderMob
{
	protected static final EntityDataAccessor<Integer> TRADE_COOLDOWN = SynchedEntityData.defineId(RanaEntity.class, EntityDataSerializers.INT);

	private int throwDelay;

	public RanaEntity(EntityType<? extends RanaEntity> type, Level world)
	{
		super(type, world);

		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);

		this.setCanPickUpLoot(true);
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
		this.setPathfindingMalus(BlockPathTypes.TRAPDOOR, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));

		this.goalSelector.addGoal(1, new PanicGoal(this, 0.7F));

		this.goalSelector.addGoal(2, new SearchForItemGoal<>(this, 0.65F, RediscoveredTags.Items.RANA_CURRENCY)
		{
			@Override
			public boolean canUse()
			{
				return super.canUse() && getOffhandItem().isEmpty() && getTradeCooldown() <= 0;
			}
		});

		this.goalSelector.addGoal(3, new TemptGoal(this, 0.6F, Ingredient.of(RediscoveredTags.Items.RANA_CURRENCY), false)
		{
			@Override
			public boolean canUse()
			{
				return super.canUse() && getOffhandItem().isEmpty() && getTradeCooldown() <= 0;
			}

			@Override
			public void tick()
			{
				super.tick();

				if (this.isRunning() && !this.mob.isPathFinding())
				{
					if (this.mob.onGround())
						this.mob.getJumpControl().jump();

					if (!this.mob.swinging && this.mob.tickCount % 5 == (this.mob.getRandom().nextInt(2)))
						this.mob.swing(InteractionHand.MAIN_HAND);
				}
			}
		});

		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(TRADE_COOLDOWN, 0);
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	protected static final String TRADE_COOLDOWN_KEY = "TradeCooldown", THROW_DELAY_KEY = "ThrowDelay";

	@Override
	public void addAdditionalSaveData(CompoundTag tag)
	{
		super.addAdditionalSaveData(tag);

		tag.putInt(TRADE_COOLDOWN_KEY, this.getTradeCooldown());
		tag.putInt(THROW_DELAY_KEY, this.throwDelay);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag tag)
	{
		super.readAdditionalSaveData(tag);

		this.setTradeCooldown(tag.getInt(TRADE_COOLDOWN_KEY));
		this.throwDelay = tag.getInt(THROW_DELAY_KEY);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.9F;
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 30.0D);
	}

	@Override
	public boolean doHurtTarget(Entity entityIn)
	{
		return super.doHurtTarget(entityIn);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public void aiStep()
	{
		super.aiStep();
		this.updateSwingTime();
	}

	@Override
	protected void customServerAiStep()
	{
		super.customServerAiStep();

		if (this.getTradeCooldown() > 0)
			this.setTradeCooldown(this.getTradeCooldown() - 1);
		else if (this.isCurrency(this.getOffhandItem()))
		{
			this.setItemSlot(EquipmentSlot.OFFHAND, ItemStack.EMPTY);
			this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC);
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return RediscoveredSounds.ENTITY_RANA_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_RANA_DEATH;
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		return true;
	}

	public void setTradeCooldown(int flag)
	{
		this.getEntityData().set(TRADE_COOLDOWN, flag);
	}

	public int getTradeCooldown()
	{
		return this.getEntityData().get(TRADE_COOLDOWN);
	}

	@Override
	public boolean isAlliedTo(Entity entity)
	{
		if (entity.getType().is(RediscoveredTags.Entities.PIGMAN_ALLIES))
			return this.getTeam() == null && entity.getTeam() == null;

		return this.isAlliedTo(entity.getTeam());
	}

	private void tradeItem(Vec3 pos, @Nullable Entity gifter)
	{
		this.setTradeCooldown((60 + this.random.nextInt(30)) * 20);

		if (this.level()instanceof ServerLevel sl)
		{
			LootTable loottable = sl.getServer().getLootData().getLootTable(RediscoveredLootProv.RANA_TRADING);
			List<ItemStack> list = loottable.getRandomItems((new LootParams.Builder(sl)).withParameter(LootContextParams.ORIGIN, this.getEyePosition()).withParameter(LootContextParams.THIS_ENTITY, this).create(LootContextParamSets.GIFT));

			if (!list.isEmpty())
			{
				this.swing(InteractionHand.OFF_HAND);

				for (ItemStack itemstack : list)
					BehaviorUtils.throwItem(this, itemstack, pos.add(0.0D, 1.0D, 0.0D));
			}

			if (gifter instanceof ServerPlayer sp)
				RediscoveredTriggers.GIVE_RANA_ITEM.get().trigger(sp, this);
		}
	}

	@Override
	public InteractionResult interactAt(Player player, Vec3 vec, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		if (this.getTradeCooldown() <= 0 && this.isCurrency(itemstack))
		{
			if (player.level().isClientSide())
				return InteractionResult.CONSUME;

			this.tradeItem(player.position(), player);

			ItemStack held = itemstack.copy();
			held.setCount(1);
			itemstack.shrink(1);
			this.holdInOffHand(held);

			return InteractionResult.SUCCESS;
		}

		return super.interactAt(player, vec, hand);
	}

	@Override
	protected void pickUpItem(ItemEntity entity)
	{
		if (this.isCurrency(entity.getItem()))
		{
			this.tradeItem(entity.getOwner() != null ? entity.getOwner().position() : this.position(), entity.getOwner());

			this.onItemPickup(entity);
			this.take(entity, 1);
			this.holdInOffHand(removeOneItemFromItemEntity(entity));
		}
		else
			super.pickUpItem(entity);
	}

	protected void holdInOffHand(ItemStack stack)
	{
		if (this.isCurrency(stack))
		{
			this.setItemSlot(EquipmentSlot.OFFHAND, stack);
			this.setGuaranteedDrop(EquipmentSlot.OFFHAND);
		}
		else
			this.setItemSlotAndDropWhenKilled(EquipmentSlot.OFFHAND, stack);
	}

	public boolean wantsToPickUp(ItemStack stack)
	{
		return EventHooks.getMobGriefingEvent(this.level(), this) && this.canPickUpLoot() && stack.is(RediscoveredTags.Items.RANA_CURRENCY) && this.getOffhandItem().isEmpty();
	}

	private static ItemStack removeOneItemFromItemEntity(ItemEntity pItemEntity)
	{
		ItemStack itemstack = pItemEntity.getItem();
		ItemStack itemstack1 = itemstack.split(1);

		if (itemstack.isEmpty())
			pItemEntity.discard();
		else
			pItemEntity.setItem(itemstack);

		return itemstack1;
	}

	public void take(Entity pEntity, int pAmount)
	{
		if (!pEntity.isRemoved() && !this.level().isClientSide && (pEntity instanceof ItemEntity || pEntity instanceof AbstractArrow || pEntity instanceof ExperienceOrb))
			((ServerLevel) this.level()).getChunkSource().broadcast(pEntity, new ClientboundTakeItemEntityPacket(pEntity.getId(), this.getId(), pAmount));
	}

	private final boolean isCurrency(ItemStack stack)
	{
		return stack.is(RediscoveredTags.Items.RANA_CURRENCY);
	}
}