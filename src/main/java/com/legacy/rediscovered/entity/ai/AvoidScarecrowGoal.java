package com.legacy.rediscovered.entity.ai;

import java.util.EnumSet;

import javax.annotation.Nullable;

import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.entity.ai.util.DefaultRandomPos;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.phys.Vec3;

// Basically the avoid goal but with handling to cover mobs that change their path navigator
// The mob's panic goal is also passed in to determine if they should be capable of being afraid
public class AvoidScarecrowGoal<T extends LivingEntity> extends Goal
{
	protected final PathfinderMob mob;
	private final double walkSpeedModifier;
	private final double sprintSpeedModifier;
	@Nullable
	protected T toAvoid;
	protected final float maxDist;
	@Nullable
	protected Path path;
	/** Class of entity this behavior seeks to avoid */
	protected final Class<T> avoidClass;
	private final TargetingConditions avoidEntityTargeting;

	public AvoidScarecrowGoal(PathfinderMob mob, Class<T> entityClassToAvoid, float maxDistance, double walkSpeedModifier, double sprintSpeedModifier)
	{
		this.mob = mob;
		this.avoidClass = entityClassToAvoid;
		this.maxDist = maxDistance;
		this.walkSpeedModifier = walkSpeedModifier;
		this.sprintSpeedModifier = sprintSpeedModifier;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE));
		this.avoidEntityTargeting = TargetingConditions.forCombat().range(maxDistance).selector(EntitySelector.NO_CREATIVE_OR_SPECTATOR::test);
	}

	private PathNavigation getNavigation()
	{
		return this.mob.getNavigation();
	}

	public boolean canUse()
	{
		this.toAvoid = this.mob.level().getNearestEntity(this.mob.level().getEntitiesOfClass(this.avoidClass, this.mob.getBoundingBox().inflate(this.maxDist, 3.0D, this.maxDist), avoid -> true), this.avoidEntityTargeting, this.mob, this.mob.getX(), this.mob.getY(), this.mob.getZ());
		if (this.toAvoid == null)
		{
			return false;
		}
		else
		{
			Vec3 vec3 = DefaultRandomPos.getPosAway(this.mob, 16, 7, this.toAvoid.position());
			if (vec3 == null)
			{
				return false;
			}
			else if (this.toAvoid.distanceToSqr(vec3.x, vec3.y, vec3.z) < this.toAvoid.distanceToSqr(this.mob))
			{
				return false;
			}
			else
			{
				this.path = this.getNavigation().createPath(vec3.x, vec3.y, vec3.z, 0);
				return this.path != null;
			}
		}
	}

	public boolean canContinueToUse()
	{
		// Displays when the mob is running away
		/*var ret = !this.getNavigation().isDone();
		((ServerLevel) this.mob.level()).sendParticles(ParticleTypes.ANGRY_VILLAGER, this.mob.getX(), this.mob.getEyeY(), this.mob.getZ(), 1, 0, 0, 0, 0);
		return ret;*/
		return !this.getNavigation().isDone();
	}

	public void start()
	{
		this.getNavigation().moveTo(this.path, this.walkSpeedModifier);
	}

	public void stop()
	{
		this.toAvoid = null;
	}

	public void tick()
	{
		if (this.mob.distanceToSqr(this.toAvoid) < 49.0D)
		{
			this.mob.getNavigation().setSpeedModifier(this.sprintSpeedModifier);
		}
		else
		{
			this.mob.getNavigation().setSpeedModifier(this.walkSpeedModifier);
		}
	}
}
