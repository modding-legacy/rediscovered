package com.legacy.rediscovered.entity.ai;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiRecord;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public abstract class SearchForPoiGoal<T extends Mob> extends Goal
{
	protected final T mob;
	protected final Function<T, BlockPos> start;
	protected final Predicate<Holder<PoiType>> poi;
	protected final int radius, checkDelay, delayOffset;

	protected int currentDelay;

	@Nullable
	protected BlockPos foundPos;

	public boolean actionComplete;

	public SearchForPoiGoal(T mob, Function<T, BlockPos> start, Predicate<Holder<PoiType>> poi, int radius, int checkDelay, int delayOffset)
	{
		this.mob = mob;
		this.start = start;
		this.poi = poi;
		this.radius = radius;
		this.checkDelay = checkDelay / 2;
		this.delayOffset = delayOffset;
	}

	public SearchForPoiGoal(T mob, Function<T, BlockPos> start, ResourceKey<PoiType> poi, int radius, int checkDelay, int delayOffset)
	{
		this(mob, start, p -> p.is(poi), radius, checkDelay, delayOffset);
	}

	public SearchForPoiGoal(T mob, Function<T, BlockPos> start,  ResourceKey<PoiType> poi, int radius, int checkDelay)
	{
		this(mob, start, p -> p.is(poi), radius, checkDelay, 0);
	}

	public SearchForPoiGoal<T> setMovementFlags()
	{
		this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
		return this;
	}

	@Override
	public boolean canUse()
	{
		return --this.currentDelay <= 0;
	}

	@Override
	public boolean canContinueToUse()
	{
		return this.foundPos != null && !this.actionComplete;
	}

	@Override
	public void start()
	{
		final BlockPos start = this.start.apply(this.mob);
		Stream<PoiRecord> poi = getPoisInCircle(this.mob.level(), start, this.poi, this.radius).sorted(Comparator.comparingDouble((record) -> record.getPos().distSqr(start))).filter(this.poiFilter());
		Optional<PoiRecord> optional = poi.findFirst();

		if (optional.isPresent())
		{
			this.foundPos = optional.get().getPos();
			this.onPoiFound(this.foundPos);
		}
	}

	@Override
	public void tick()
	{
		if (this.foundPos != null)
			this.tickFound(this.foundPos);
	}

	@Override
	public void stop()
	{
		this.resetDelay();

		if (this.foundPos == null)
			this.onPoiNotFound();

		this.foundPos = null;
		this.actionComplete = false;

	}

	@Override
	public boolean requiresUpdateEveryTick()
	{
		return false;
	}

	public abstract void onPoiFound(BlockPos pos);

	public void tickFound(BlockPos pos)
	{
	}

	public void onPoiNotFound()
	{
	}

	protected final void finish()
	{
		this.actionComplete = true;
	}

	protected final void resetDelay()
	{
		this.currentDelay = this.delayOffset <= 0 ? this.checkDelay : this.checkDelay + this.mob.getRandom().nextInt(this.delayOffset);
	}

	public Predicate<PoiRecord> poiFilter()
	{
		return record -> true;
	}

	protected final float distanceTo()
	{
		return Mth.sqrt((float) this.mob.distanceToSqr(Vec3.atCenterOf(this.foundPos)));
	}

	protected final float distanceToBottom()
	{
		return Mth.sqrt((float) this.mob.distanceToSqr(Vec3.atBottomCenterOf(this.foundPos)));
	}

	protected PoiManager.Occupancy getOccupancyType()
	{
		return PoiManager.Occupancy.ANY;
	}

	public Stream<PoiRecord> getPoisInCircle(Entity entity, Predicate<Holder<PoiType>> type, int radius)
	{
		return getPoisInCircle(entity.level(), entity.blockPosition(), type, radius);
	}

	public Stream<PoiRecord> getPoisInCircle(Level level, BlockPos startPos, Predicate<Holder<PoiType>> type, int radius)
	{
		if (!(level instanceof ServerLevel serverLevel))
			return Stream.empty();

		PoiManager poiManager = serverLevel.getPoiManager();
		poiManager.ensureLoadedAndValid(level, startPos, radius);

		Stream<PoiRecord> stream = serverLevel.getPoiManager().getInRange(type, startPos, radius, this.getOccupancyType());

		return stream;
	}
}
