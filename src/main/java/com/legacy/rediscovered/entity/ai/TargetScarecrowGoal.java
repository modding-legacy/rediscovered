package com.legacy.rediscovered.entity.ai;

import java.util.function.Predicate;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;

public class TargetScarecrowGoal<T extends LivingEntity> extends NearestAttackableTargetGoal<T>
{

	public TargetScarecrowGoal(Mob pMob, Class<T> pTargetType, boolean pMustSee)
	{
		this(pMob, pTargetType, 10, pMustSee, false, (Predicate<LivingEntity>) null);
	}

	public TargetScarecrowGoal(Mob pMob, Class<T> pTargetType, int pRandomInterval, boolean pMustSee, boolean pMustReach, Predicate<LivingEntity> pTargetPredicate)
	{
		super(pMob, pTargetType, pRandomInterval, pMustSee, pMustReach, pTargetPredicate);
	}
	
	
	@Override
	public boolean canContinueToUse()
	{
		return this.target != null && this.mob.level().getEntity(this.target.getId()) != null;
	}
}
