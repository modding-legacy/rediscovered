package com.legacy.rediscovered.entity.ai;

import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;

import com.legacy.rediscovered.mixin.MobAccessor;

import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;

public class SearchForItemGoal<T extends PathfinderMob> extends Goal
{
	private static final Predicate<ItemEntity> CAN_PICKUP_ITEM = (itemEntity) -> !itemEntity.hasPickUpDelay() && itemEntity.isAlive();

	final T mob;
	final Predicate<ItemEntity> allowedItems;
	final double speedModifier;

	public SearchForItemGoal(T mob, double speedModifier, Predicate<ItemEntity> entityPredicate)
	{
		this.mob = mob;
		this.speedModifier = speedModifier;
		this.allowedItems = CAN_PICKUP_ITEM.and(entityPredicate);

		this.setFlags(EnumSet.of(Goal.Flag.MOVE));
	}

	public SearchForItemGoal(T mob, double speedModifier, ItemLike item)
	{
		this(mob, speedModifier, (itemEntity) -> itemEntity.getItem().is(item.asItem()));
	}

	public SearchForItemGoal(T mob, double speedModifier, TagKey<Item> tag)
	{
		this(mob, speedModifier, (itemEntity) -> itemEntity.getItem().is(tag));
	}

	@Override
	public boolean canUse()
	{
		List<ItemEntity> list = this.mob.level().getEntitiesOfClass(ItemEntity.class, this.mob.getBoundingBox().inflate(16.0D, 8.0D, 16.0D), this.allowedItems);

		if (!list.isEmpty())
			return this.mob.getNavigation().moveTo(list.get(0), this.speedModifier);

		return false;
	}

	@Override
	public void tick()
	{
		if (this.mob.getNavigation().getTargetPos().closerToCenterThan(this.mob.position(), 1.414D))
		{
			List<ItemEntity> list = this.mob.level().getEntitiesOfClass(ItemEntity.class, this.mob.getBoundingBox().inflate(4.0D, 4.0D, 4.0D), this.allowedItems);

			if (!list.isEmpty())
				((MobAccessor) this.mob).rediscovered$pickUpItem(list.get(0));
		}

	}
}