package com.legacy.rediscovered.entity;

import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.AbstractSchoolingFish;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class FishEntity extends AbstractSchoolingFish
{
	public FishEntity(EntityType<? extends FishEntity> type, Level world)
	{
		super(type, world);
	}

	@Override
	public ItemStack getBucketItemStack()
	{
		return new ItemStack(RediscoveredItems.fish_bucket);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return RediscoveredSounds.ENTITY_FISH_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return RediscoveredSounds.ENTITY_FISH_DEATH;
	}

	@Override
	protected SoundEvent getFlopSound()
	{
		return RediscoveredSounds.ENTITY_FISH_FLOP;
	}
}