package com.legacy.rediscovered.entity;

import java.util.EnumSet;

import javax.annotation.Nullable;

import com.legacy.rediscovered.block.BaseFakeFireBlock;
import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.entity.util.IMD3Entity;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;

public abstract class AbstractSteveEntity extends Monster implements IMD3Entity
{
	protected int fireCooldown = 0;

	public AbstractSteveEntity(EntityType<? extends AbstractSteveEntity> type, Level world)
	{
		super(type, world);

		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);

		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
		this.setPathfindingMalus(BlockPathTypes.TRAPDOOR, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(1, new BurnGroundGoal<>(this));
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));

		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Mob.class, 5, false, false, (mob) -> mob instanceof Enemy && !(mob instanceof Creeper)));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	protected static final String FIRE_COOLDOWN_KEY = "FireCooldown";

	@Override
	public void addAdditionalSaveData(CompoundTag tag)
	{
		super.addAdditionalSaveData(tag);

		tag.putInt(FIRE_COOLDOWN_KEY, this.fireCooldown);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag tag)
	{
		super.readAdditionalSaveData(tag);

		this.fireCooldown = tag.getInt(FIRE_COOLDOWN_KEY);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.9F;
	}

	public static AttributeSupplier.Builder createBaseSteveAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 50.0D);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	protected void customServerAiStep()
	{
		super.customServerAiStep();

		if (this.fireCooldown > 0)
			--this.fireCooldown;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return super.getHurtSound(damageSourceIn);
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return super.getDeathSound();
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		return true;
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return false;
	}

	@Override
	public boolean isAlliedTo(Entity entity)
	{
		if (entity.getType().is(RediscoveredTags.Entities.PIGMAN_ALLIES))
			return this.getTeam() == null && entity.getTeam() == null;

		return this.isAlliedTo(entity.getTeam());
	}

	public class BurnGroundGoal<T extends AbstractSteveEntity> extends Goal
	{
		private final T mob;
		private final ItemStack item;
		private BlockPos burnPos;
		private int ticksSinceFire;

		public BurnGroundGoal(T pMob)
		{
			this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK, Goal.Flag.JUMP));
			this.mob = pMob;
			this.item = Items.FLINT_AND_STEEL.getDefaultInstance();
		}

		@Override
		public boolean canUse()
		{
			if (this.mob.fireCooldown > 0)
				return false;

			var target = this.mob.getTarget();

			if (target != null)
			{
				return !target.isOnFire() && this.mob.distanceTo(target) < 5 && this.mob.distanceTo(target) > 2 && !this.mob.hasItemInSlot(EquipmentSlot.OFFHAND) && BaseFireBlock.canBePlacedAt(this.mob.level(), this.burnPos = target.blockPosition(), Direction.DOWN) && this.mob.level().getEntities(target, target.getBoundingBox().inflate(1.5F)).isEmpty();
			}

			return false;
		}

		@Override
		public boolean canContinueToUse()
		{
			return this.mob.getTarget() != null && this.burnPos != null && this.mob.level().getBlockState(this.burnPos).getBlock() instanceof BaseFireBlock;
		}

		@Override
		public void start()
		{
			this.mob.setItemSlot(EquipmentSlot.OFFHAND, this.item.copy());
			this.mob.swing(InteractionHand.OFF_HAND, true);

			Level level = this.mob.level();
			BlockPos pos = this.burnPos;

			level.playSound(null, pos, SoundEvents.FLINTANDSTEEL_USE, SoundSource.BLOCKS, 1.0F, level.getRandom().nextFloat() * 0.4F + 0.8F);
			BlockState blockstate1 = BaseFakeFireBlock.getState(level, pos);
			level.setBlock(pos, blockstate1, 11);
			level.gameEvent(this.mob, GameEvent.BLOCK_PLACE, pos);
		}

		@Override
		public void tick()
		{
			++this.ticksSinceFire;

			if (this.burnPos != null)
				this.mob.lookControl.setLookAt(Vec3.atCenterOf(this.burnPos));

			var target = this.mob.getTarget();

			if (target != null)
			{
				if (this.burnPos != null && (target.isOnFire() || this.ticksSinceFire >= 10))
				{
					this.mob.swing(InteractionHand.OFF_HAND, true);
					this.mob.level().destroyBlock(this.burnPos, false);
					this.mob.level().playSound(null, this.burnPos, SoundEvents.FIRE_EXTINGUISH, SoundSource.BLOCKS, 0.5F, 2.6F + (this.mob.random.nextFloat() - this.mob.random.nextFloat()) * 0.8F);
				}
			}
		}

		@Override
		public void stop()
		{
			this.mob.setItemSlot(EquipmentSlot.OFFHAND, ItemStack.EMPTY);

			this.burnPos = null;
			this.ticksSinceFire = 0;
			this.mob.fireCooldown = 5 * 20;
		}
	}

}
