package com.legacy.rediscovered.entity.util;

import javax.annotation.Nullable;

import com.legacy.rediscovered.entity.dragon.AbstractRedDragonEntity;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.entity.PartEntity;

public class RedDragonEntityPart extends PartEntity<AbstractRedDragonEntity>
{
	public final AbstractRedDragonEntity dragon;
	public final String name;
	private final EntityDimensions size;

	public RedDragonEntityPart(AbstractRedDragonEntity dragon, String partName, float width, float height)
	{
		super(dragon);
		this.size = EntityDimensions.scalable(width, height);
		this.refreshDimensions();
		this.dragon = dragon;
		this.name = partName;
	}

	@Override
	protected void defineSynchedData()
	{
	}

	@Override
	protected void readAdditionalSaveData(CompoundTag compound)
	{
	}

	@Override
	protected void addAdditionalSaveData(CompoundTag compound)
	{
	}

	@Override
	public boolean isPickable()
	{
		return true;
	}

	@Override
	public InteractionResult interact(Player player, InteractionHand hand)
	{
		return super.interact(player, hand);
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		return this.isInvulnerableTo(source) ? false : this.dragon.hurt(source, amount);
	}

	@Override
	public boolean is(Entity entityIn)
	{
		return this == entityIn || this.dragon == entityIn;
	}

	@Nullable
	public ItemStack getPickResult()
	{
		return this.dragon.getPickResult();
	}

	@Override
	public Packet<ClientGamePacketListener> getAddEntityPacket()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public EntityDimensions getDimensions(Pose pPose)
	{
		return this.size;
	}

	@Override
	public boolean shouldBeSaved()
	{
		return false;
	}
}