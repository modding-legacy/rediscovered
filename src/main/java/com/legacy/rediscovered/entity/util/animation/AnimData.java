package com.legacy.rediscovered.entity.util.animation;

import java.util.List;

import com.legacy.rediscovered.network.PacketHandler;
import com.legacy.rediscovered.network.s_to_c.UpdateAnimationPacket;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;

/**
 * Modding Legacy animation system (All Rights Reserved)
 */
public class AnimData
{
	private static final float MAX = 6.0F;

	private final float increasePerTick, decreasePerTick;
	private final boolean hasApex;
	private final int ticksBeforeReset;
	public final int id;

	private float animTicks, animTicksOld;
	private boolean shouldPlay, wasPlaying;
	private int apexTicks;
	private boolean serverControlledStops, resetWhenComplete, atApex;

	public AnimData(List<AnimData> anims, float increasePerTick, float decreasePerTick, boolean hasApex, int ticksBeforeReset)
	{
		this.increasePerTick = increasePerTick;
		this.decreasePerTick = decreasePerTick;
		this.hasApex = hasApex;
		this.ticksBeforeReset = ticksBeforeReset;

		this.id = anims.size();
		anims.add(this);
	}

	public AnimData(List<AnimData> anims, float increasePerTick, float decreasePerTick, boolean hasApex)
	{
		this(anims, increasePerTick, decreasePerTick, hasApex, 0);
	}

	public AnimData(List<AnimData> anims, float increasePerTick, boolean hasApex)
	{
		this(anims, increasePerTick, increasePerTick, hasApex);
	}

	/**
	 * Determines weather the server should have rough control over when the
	 * animation stops. This only matters on animations with an apex.
	 */
	public AnimData serverControlledStops()
	{
		this.serverControlledStops = true;
		return this;
	}

	/**
	 * Skips the transition back and instead completely cancels the animation.
	 * Useful if whatever is being done returns to the original position anyway.
	 */
	public AnimData resetWhenComplete()
	{
		this.resetWhenComplete = true;
		return this;
	}

	public <T extends Entity> void tick(T entity)
	{
		this.animTicksOld = this.animTicks;
		this.animTicks = tickValue(this.animTicks);

		if (this.serverControlledStops && !entity.level().isClientSide || !this.serverControlledStops)
		{
			this.atApex = false;

			// if it doesn't have an apex, it will continue to play until it ends.
			// This means transitioning back to where it started, at the decrease speed.
			if (this.atApex() && (!this.hasApex || this.ticksBeforeReset > 0 && this.apexTicks >= this.ticksBeforeReset))
			{
				if (!this.resetWhenComplete)
				{
					// decrease it by 1, this prevents 2 frames of the same animation, resulting in
					// it being smooth, however prevents detecting the apex
					this.animTicks = this.animTicks - this.decreasePerTick;
					this.atApex = true;
					this.stop();
				}
				else
					this.cancel();
			}

			if (this.atApex() && (this.ticksBeforeReset > 0 && this.apexTicks < this.ticksBeforeReset || this.ticksBeforeReset == 0))
				++this.apexTicks;
			else
				this.apexTicks = 0;
		}

		if (this.wasPlaying != this.shouldPlay && entity.level()instanceof ServerLevel sl)
		{
			if (!this.hasApex && this.shouldPlay || this.serverControlledStops || this.hasApex)
				PacketHandler.sendToClients(new UpdateAnimationPacket(entity.getId(), this.id, this.shouldPlay), sl);

			this.wasPlaying = this.shouldPlay;
		}

		/*if (this.shouldPlay)
			BlueSkies.LOGGER.info("playing " + entity.level.isClientSide);*/
	}

	private final float tickValue(float value)
	{
		return value = Mth.clamp(this.shouldPlay ? (value + this.increasePerTick) : value - this.decreasePerTick, 0.0F, MAX);
	}

	public float getTicks()
	{
		return animTicks;
	}

	public float getLastTick()
	{
		return animTicksOld;
	}

	private float getAnimScale(float partialTicks)
	{
		float trans = /*this.noLerp ? Mth.approach(this.animTicksOld, this.animTicks, partialTicks) : */Mth.lerp(partialTicks, this.animTicksOld, this.animTicks);
		return trans / MAX;
	}

	public float getValue(float partialTicks)
	{
		float f1 = this.getAnimScale(partialTicks);
		return f1 = f1 * f1;
	}

	/**
	 * Only used by some older animations. Based on some vanilla anims.
	 */
	public float getSpecialValue(float partialTicks, float range)
	{
		float f1 = this.getValue(partialTicks);
		float f2 = -1.5707964F + f1 * range;
		return Mth.HALF_PI + f2;
	}

	/**
	 * Results may vary
	 */
	public boolean atApex()
	{
		return this.animTicks > 0 && this.animTicks == this.animTicksOld || this.atApex;
	}

	public int getApexTicks()
	{
		return this.apexTicks;
	}

	public boolean isPlaying()
	{
		return this.shouldPlay;
	}

	public boolean wasPlaying()
	{
		return this.wasPlaying;
	}

	/**
	 * Different from isPlaying, this is true even when decreasing
	 */
	public boolean isActive()
	{
		return this.getTicks() > 0 || this.getLastTick() > 0;
	}

	/**
	 * Only true when the animation is on its way back to 0
	 */
	public boolean isStopping()
	{
		return this.isActive() && !this.isPlaying();
	}

	public boolean play()
	{
		return this.play(true);
	}

	public boolean play(boolean should)
	{
		boolean playing = this.isPlaying();
		this.shouldPlay = should;

		/*if (should)
			this.apexTicks = 0;*/

		return playing != this.isPlaying();
	}

	public final void forcePlay(boolean play)
	{
		this.shouldPlay = play;
	}

	/**
	 * Stops the animation, it will transition to its default position
	 */
	public void stop()
	{
		this.shouldPlay = false;
		this.apexTicks = 0;
	}

	/**
	 * Completely cancels the animation, resetting it without a transition
	 */
	public void cancel()
	{
		this.shouldPlay = false;
		this.animTicks = this.animTicksOld = 0;
		this.apexTicks = 0;
	}
}