package com.legacy.rediscovered.entity.util;

import javax.annotation.Nullable;

import com.legacy.rediscovered.RediscoveredMod;
import com.legacy.rediscovered.client.RediscoveredSounds;
import com.legacy.rediscovered.entity.ZombiePigmanEntity;
import com.legacy.rediscovered.entity.pigman.PigmanEntity;
import com.legacy.rediscovered.registry.RediscoveredAttributes;
import com.legacy.rediscovered.registry.RediscoveredEffects;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.CrossbowItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.neoforged.neoforge.common.ToolActions;
import net.neoforged.neoforge.event.EventHooks;

public interface IPigman
{
	public static final String ZOMBIFICATION_TIME_KEY = "ZombificationTime",
			ZOMBIFICATION_IMMUNE_KEY = "IsImmuneToZombification";
	public static final int NATURAL_CONVERSION_TIME = 300;

	IPigman.Type getPigmanType();

	boolean isImmuneToZombification();

	default boolean shouldConvert(Mob entity)
	{
		return !this.isImmuneToZombification() && !entity.isNoAi() && !RediscoveredAttributes.isConversionImmune(entity) && (entity.level().dimensionType().ultraWarm() || entity.hasEffect(RediscoveredEffects.CRIMSON_VEIL.get()));
	}

	public static boolean zombify(Mob killed, @Nullable DamageSource source)
	{
		if (!(killed.level()instanceof ServerLevel level))
			return false;

		if (source != null && !(source.getEntity() instanceof Zombie))
			return false;

		if ((source == null || level.getDifficulty() == Difficulty.NORMAL || level.getDifficulty() == Difficulty.HARD) && killed instanceof IPigman pigman && (source == null || source != null && EventHooks.canLivingConvert(killed, RediscoveredEntityTypes.ZOMBIE_PIGMAN, (timer) ->
		{
		})))
		{
			if (source != null && level.getDifficulty() != Difficulty.HARD && level.getRandom().nextBoolean())
				return false;

			ZombiePigmanEntity zombie = killed.convertTo(RediscoveredEntityTypes.ZOMBIE_PIGMAN, true);

			if (zombie != null)
			{
				RediscoveredMod.LOGGER.debug("Pigman zombified with type {} ({}) at {} in {}", pigman.getPigmanType().ordinal(), Type.get((byte) pigman.getPigmanType().ordinal()), killed.blockPosition(), killed.level().dimension());

				zombie.setConversionType((byte) pigman.getPigmanType().ordinal());
				zombie.finalizeSpawn(level, level.getCurrentDifficultyAt(zombie.blockPosition()), MobSpawnType.CONVERSION, null, (CompoundTag) null);
				zombie.setLeftHanded(killed.isLeftHanded());
				zombie.setBaby(killed.isBaby());

				if (killed instanceof PigmanEntity trader)
				{
					zombie.setPigmanData(trader.getPigmanData());
					zombie.tradeOffers = trader.getOffers().createTag();
					zombie.traderXp = trader.getVillagerXp();

					trader.releaseAllPois();
				}

				EventHooks.onLivingConvert(killed, zombie);

				if (!killed.isSilent())
					level.playSound(null, killed.blockPosition(), RediscoveredSounds.ENTITY_PIGMAN_ZOMBIFY, SoundSource.HOSTILE, 2, 1);
				/*level.levelEvent((Player) null, 1026, killed.blockPosition(), 0);*/

				zombie.setPersistenceRequired();

				return true;
			}
		}

		return false;
	}

	public static enum Type
	{
		UNASSIGNED, TRADER, MELEE, RANGED;

		@SuppressWarnings("unchecked")
		public <P extends Mob & IPigman> EntityType<P> getTypeResult()
		{
			return (EntityType<P>) (this == MELEE ? RediscoveredEntityTypes.MELEE_PIGMAN : this == RANGED ? RediscoveredEntityTypes.RANGED_PIGMAN : RediscoveredEntityTypes.PIGMAN);
		}

		public SoundEvent getZombieTypeSelectSound()
		{
			return this == MELEE ? RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_SELECT_MELEE : this == RANGED ? RediscoveredSounds.ENTITY_ZOMBIE_PIGMAN_SELECT_RANGED : SoundEvents.ITEM_PICKUP;
		}

		public boolean isValidHandEquip(ItemStack stack)
		{
			if (this == MELEE)
				return stack.is(ItemTags.SWORDS) || stack.is(ItemTags.AXES);

			if (this == RANGED)
				return stack.getItem() instanceof BowItem || stack.getItem() instanceof CrossbowItem;

			return false;
		}

		public boolean isValidOffHandEquip(ItemStack stack)
		{
			if (this == MELEE)
				return stack.canPerformAction(ToolActions.SHIELD_BLOCK);

			if (this == RANGED)
				return stack.is(ItemTags.ARROWS) || stack.is(Items.FIREWORK_ROCKET);

			return false;
		}

		public static boolean isValidHandEquipForAny(ItemStack stack)
		{
			for (Type type : values())
			{
				if (type.isValidHandEquip(stack))
					return true;
			}

			return false;
		}

		public static Type get(byte id)
		{
			for (Type type : values())
				if (id == type.ordinal())
					return type;

			return UNASSIGNED;
		}
	}
}
