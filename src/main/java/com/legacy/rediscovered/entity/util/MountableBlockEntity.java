package com.legacy.rediscovered.entity.util;

import java.util.List;

import org.joml.Vector3f;

import com.legacy.rediscovered.data.RediscoveredTags;
import com.legacy.rediscovered.registry.RediscoveredEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.vehicle.DismountHelper;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

public class MountableBlockEntity extends Entity
{
	public MountableBlockEntity(EntityType<? extends MountableBlockEntity> type, Level world)
	{
		super(type, world);

		this.noPhysics = this.blocksBuilding = true;
	}

	public MountableBlockEntity(Level world, double x, double y, double z)
	{
		this(RediscoveredEntityTypes.MOUNTABLE_BLOCK, world);

		setPos(x, y, z);
	}

	public MountableBlockEntity(Level world, int i, int j, int k, float mountingX, float mountingY, float mountingZ)
	{
		this(RediscoveredEntityTypes.MOUNTABLE_BLOCK, world);

		setPos(mountingX, mountingY, mountingZ);
	}

	@Override
	public InteractionResult interact(Player player, InteractionHand hand)
	{
		return interactBasic(player);
	}

	/**
	 * Exists so the Pigman/other entities can interact with this
	 */
	public InteractionResult interactBasic(LivingEntity entity)
	{
		if (this.getVehicle() != null && this.getVehicle() instanceof Player && this.getVehicle() != entity)
			return InteractionResult.CONSUME;

		if (!this.level().isClientSide())
		{
			entity.startRiding(this);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.level().isClientSide())
			return;

		if (this.tickCount > 10 && (this.getPassengers().isEmpty() || !this.getPassengers().get(0).isAlive() || !this.level().getBlockState(this.blockPosition()).is(RediscoveredTags.Blocks.CHAIRS)))
		{
			this.discard();
		}
		double floorY = Math.ceil(this.getY());
		if (this.getY() != floorY)
			this.setPos(this.getX(), floorY, this.getZ());
	}

	@Override
	protected void addPassenger(Entity passenger)
	{
		super.addPassenger(passenger);
		this.updateComparators(this.level(), this.blockPosition());
	}

	@Override
	protected void removePassenger(Entity passenger)
	{
		super.removePassenger(passenger);
		this.updateComparators(this.level(), this.blockPosition());
	}

	@Override
	public void ejectPassengers()
	{
		super.ejectPassengers();
		this.updateComparators(this.level(), this.blockPosition());
	}

	private void updateComparators(Level level, BlockPos pos)
	{
		if (!level.isClientSide)
		{
			// Prevent world lockups with an area loaded check
			if (level.isAreaLoaded(pos, 16))
				level.updateNeighbourForOutputSignal(pos, level.getBlockState(pos).getBlock());
		}
	}

	@Override
	protected Vector3f getPassengerAttachmentPoint(Entity passenger, EntityDimensions size, float scale)
	{
		return new Vector3f(0.0F, size.height - 0.5F * scale, 0.0F);
	}

	@Override
	public boolean isPushable()
	{
		return false;
	}

	@Override
	protected void positionRider(Entity rider, MoveFunction pCallback)
	{
		super.positionRider(rider, pCallback);
		this.clampRotation(rider);
	}

	@Override
	public Vec3 getDismountLocationForPassenger(LivingEntity pLivingEntity)
	{
		// dismount prioritizing directions in this order
		for (var dir : List.of(pLivingEntity.getDirection(), this.getDirection(), this.getDirection().getClockWise(), this.getDirection().getCounterClockWise(), this.getDirection().getOpposite()))
		{
			BlockPos pos = this.blockPosition().relative(dir);
			double d2 = this.level().getBlockFloorHeight(pos);
			if (DismountHelper.isBlockFloorValid(d2) && this.level().noCollision(new AABB(pos).deflate(0.1F)))
				return Vec3.atBottomCenterOf(pos);
		}

		return super.getDismountLocationForPassenger(pLivingEntity);
	}

	@Override
	public void onPassengerTurned(Entity pEntityToUpdate)
	{
		this.clampRotation(pEntityToUpdate);
	}

	protected void clampRotation(Entity pEntityToUpdate)
	{
		pEntityToUpdate.setYBodyRot(this.getYRot());
		float f = Mth.wrapDegrees(pEntityToUpdate.getYRot() - this.getYRot());
		float f1 = Mth.clamp(f, -105.0F, 105.0F);
		pEntityToUpdate.yRotO += f1 - f;

		if (pEntityToUpdate instanceof Player)
		{
			pEntityToUpdate.setYRot(pEntityToUpdate.getYRot() + f1 - f);
			pEntityToUpdate.setYHeadRot(pEntityToUpdate.getYRot());
		}
		else
		{
			int maxHeadRot = 70;
			pEntityToUpdate.setYHeadRot(Mth.clamp(pEntityToUpdate.getYHeadRot(), this.getYRot() - maxHeadRot, this.getYRot() + maxHeadRot));
			pEntityToUpdate.setYRot(this.getYRot());
		}
	}

	@Override
	public void defineSynchedData()
	{
	}

	@Override
	public void readAdditionalSaveData(CompoundTag nbttagcompound)
	{
	}

	@Override
	public void addAdditionalSaveData(CompoundTag nbttagcompound)
	{
	}
}