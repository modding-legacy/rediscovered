package com.legacy.rediscovered.entity.util.animation;

import java.util.List;

import net.minecraft.world.entity.Entity;

public interface IAnimated
{
	List<AnimData> getAnimations();

	default <T extends Entity & IAnimated> void tickAnims(T entity)
	{
		this.getAnimations().forEach(anim -> anim.tick(entity));
	}
}
