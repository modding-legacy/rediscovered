package com.legacy.rediscovered.entity.util;

import com.legacy.rediscovered.entity.pigman.data.PigmanData;

public interface IPigmanDataHolder
{
	static final String PIGMAN_DATA_KEY = "PigmanData";

	PigmanData getPigmanData();
}
