package com.legacy.rediscovered.entity;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.rediscovered.registry.RediscoveredEntityTypes;
import com.legacy.rediscovered.registry.RediscoveredItems;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.gameevent.GameEvent;
import net.neoforged.neoforge.common.NeoForgeMod;
import net.neoforged.neoforge.fluids.FluidType;

public class ScarecrowEntity extends LivingEntity
{
	private static final byte HURT_EVENT = 32, HURT_BY_ENTITY_EVENT = 33;
	public long lastHit;
	public boolean shouldDance = false;
	@Nullable
	private BlockPos jukebox;

	public ScarecrowEntity(EntityType<? extends ScarecrowEntity> type, Level world)
	{
		super(type, world);
	}

	public ScarecrowEntity(Level worldIn, double posX, double posY, double posZ)
	{
		this(RediscoveredEntityTypes.SCARECROW, worldIn);
		this.setPos(posX, posY, posZ);
	}

	public static AttributeSupplier.Builder createAttributes()
	{
		return LivingEntity.createLivingAttributes().add(Attributes.MAX_HEALTH, 10.0F).add(NeoForgeMod.STEP_HEIGHT.value(), 0.0F);
	}

	@Override
	public void tick()
	{
		super.tick();

	}

	@Override
	public void aiStep()
	{
		if (this.jukebox == null || !this.jukebox.closerToCenterThan(this.position(), 4D) || !this.level().getBlockState(this.jukebox).is(Blocks.JUKEBOX))
		{
			this.shouldDance = false;
			this.jukebox = null;
		}

		// Heal every tick to prevent breaking
		this.setHealth(this.getHealth() + 0.5F);

		super.aiStep();
	}

	@Override
	public void refreshDimensions()
	{
		double d0 = this.getX();
		double d1 = this.getY();
		double d2 = this.getZ();
		super.refreshDimensions();
		this.setPos(d0, d1, d2);
	}

	@Override
	public Iterable<ItemStack> getHandSlots()
	{
		return List.of();
	}

	@Override
	public Iterable<ItemStack> getArmorSlots()
	{
		return List.of();
	}

	@Override
	public ItemStack getItemBySlot(EquipmentSlot pSlot)
	{
		return ItemStack.EMPTY;
	}

	@Override
	public void setItemSlot(EquipmentSlot pSlot, ItemStack pStack)
	{
	}

	@Override
	public boolean canTakeItem(ItemStack pItemstack)
	{
		return false;
	}

	// ---- Cannot be pushed, but pushes other entities away
	@Override
	public boolean isPushable()
	{
		return false;
	}

	@Override
	public void knockback(double strength, double x, double z)
	{
	}

	@Override
	public void push(double x, double y, double z)
	{
	}
	// ---- Pushing logic end

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (!this.isRemoved() && !this.level().isClientSide())
		{
			if (source.is(DamageTypeTags.BYPASSES_INVULNERABILITY))
			{
				this.kill();
				return false;
			}
			else if (!this.isInvulnerableTo(source))
			{
				if (source.is(DamageTypeTags.IS_EXPLOSION))
				{
					this.brokenByAnything(source);
					this.kill();
					return false;
				}
				else if (source.is(DamageTypeTags.IS_FIRE))
				{
					if (this.isOnFire())
					{
						this.causeDamage(source, amount * 1.0F);
					}
					else
					{
						this.setSecondsOnFire(5);
					}

					return false;
				}
				else if (source.is(DamageTypes.IN_WALL))
				{
					return false;
				}
				else
				{
					// If hurt by player, deal increased damage. Creative players instakill
					if (source.getDirectEntity() instanceof Player player)
					{
						if (player.isCreative())
							amount = this.getMaxHealth();
						else
							amount = Math.max(amount * 3.0F, 5.0F);
					}
					this.level().broadcastEntityEvent(this, source.getDirectEntity() != null ? HURT_BY_ENTITY_EVENT : HURT_EVENT);
					this.gameEvent(GameEvent.ENTITY_DAMAGE, source.getEntity());
					this.showBreakingParticles(Mth.clamp((int) amount, 1, 20));
					this.causeDamage(source, amount);
					return true;
				}
			}
		}
		return false;
	}

	private void showBreakingParticles(int amount)
	{
		if (this.level() instanceof ServerLevel)
		{
			((ServerLevel) this.level()).sendParticles(new BlockParticleOption(ParticleTypes.BLOCK, Blocks.HAY_BLOCK.defaultBlockState()), this.getX(), this.getY(0.6666666666666666D), this.getZ(), amount, (double) (this.getBbWidth() / 4.0F), (double) (this.getBbHeight() / 4.0F), (double) (this.getBbWidth() / 4.0F), 0.05D);
		}

	}

	private void causeDamage(DamageSource damage, float amount)
	{
		float health = this.getHealth();
		health -= amount;
		if (health <= 0.5F)
		{
			this.brokenByAnything(damage);
			this.kill();
		}
		else
		{
			this.setHealth(health);
			this.gameEvent(GameEvent.ENTITY_DAMAGE, damage.getEntity());
		}

	}

	private void brokenByAnything(DamageSource damage)
	{
		this.playBrokenSound();
		// Don't drop from creative mode players
		if (!(damage.getDirectEntity() instanceof Player player) || !player.isCreative())
			this.dropAllDeathLoot(damage);
	}

	private void playBrokenSound()
	{
		this.level().playSound((Player) null, this.getX(), this.getY(), this.getZ(), SoundEvents.ARMOR_STAND_BREAK, this.getSoundSource(), 1.0F, 1.0F);
	}

	@Override
	public void handleEntityEvent(byte event)
	{
		if (event == HURT_EVENT || event == HURT_BY_ENTITY_EVENT)
		{
			if (this.level().isClientSide())
			{
				if (event == HURT_BY_ENTITY_EVENT || this.level().getGameTime() - this.lastHit > 7)
				{
					this.level().playLocalSound(this.getX(), this.getY(), this.getZ(), SoundEvents.ARMOR_STAND_HIT, this.getSoundSource(), 0.3F, 1.0F, false);
					this.lastHit = this.level().getGameTime();
				}
			}
		}
		else
		{
			super.handleEntityEvent(event);
		}

	}

	@Override
	public boolean canBeAffected(MobEffectInstance effectInstance)
	{
		return effectInstance.getEffect() == MobEffects.GLOWING;
	}

	@Override
	public boolean shouldRenderAtSqrDistance(double pDistance)
	{
		double d0 = this.getBoundingBox().getSize() * 4.0D;
		if (Double.isNaN(d0) || d0 == 0.0D)
		{
			d0 = 4.0D;
		}

		d0 *= 64.0D;
		return pDistance < d0 * d0;
	}

	@Override
	protected float tickHeadTurn(float pYRot, float pAnimStep)
	{
		this.yBodyRotO = this.yRotO;
		this.yBodyRot = this.getYRot();
		return 0.0F;
	}

	@Override
	protected float getStandingEyeHeight(Pose pPose, EntityDimensions pSize)
	{
		return pSize.height * (this.isBaby() ? 0.5F : 0.9F);
	}

	@Override
	public void setYBodyRot(float pOffset)
	{
		this.yBodyRotO = this.yRotO = pOffset;
		this.yHeadRotO = this.yHeadRot = pOffset;
	}

	@Override
	public void setYHeadRot(float pRotation)
	{
		this.yBodyRotO = this.yRotO = pRotation;
		this.yHeadRotO = this.yHeadRot = pRotation;
	}

	@Override
	public void kill()
	{
		this.remove(Entity.RemovalReason.KILLED);
		this.gameEvent(GameEvent.ENTITY_DIE);
	}

	@Override
	public boolean ignoreExplosion(Explosion pExplosion)
	{
		return this.isInvisible();
	}

	@Override
	public boolean skipAttackInteraction(Entity pEntity)
	{
		return pEntity instanceof Player && !this.level().mayInteract((Player) pEntity, this.blockPosition());
	}

	@Override
	public HumanoidArm getMainArm()
	{
		return HumanoidArm.RIGHT;
	}

	@Override
	public LivingEntity.Fallsounds getFallSounds()
	{
		return new LivingEntity.Fallsounds(SoundEvents.ARMOR_STAND_FALL, SoundEvents.ARMOR_STAND_FALL);
	}

	@Override
	@Nullable
	protected SoundEvent getHurtSound(DamageSource pDamageSource)
	{
		return SoundEvents.ARMOR_STAND_HIT;
	}

	@Override
	@Nullable
	protected SoundEvent getDeathSound()
	{
		return SoundEvents.ARMOR_STAND_BREAK;
	}

	@Override
	public void thunderHit(ServerLevel pLevel, LightningBolt pLightning)
	{
	}

	@Override
	public boolean isAffectedByPotions()
	{
		return false;
	}

	@Override
	public boolean canDrownInFluidType(FluidType type)
	{
		return false;
	}

	@Override
	public boolean attackable()
	{
		return false;
	}

	@Override
	public ItemStack getPickResult()
	{
		ItemStack stack = RediscoveredItems.scarecrow.getDefaultInstance();
		if (this.hasCustomName())
			stack.setHoverName(this.getDisplayName());
		return stack;
	}

	@Override
	public boolean canBeSeenByAnyone()
	{
		return !this.isInvisible();
	}

	@Override
	public void setRecordPlayingNearby(BlockPos jukeboxPos, boolean isPlaying)
	{
		this.jukebox = jukeboxPos;
		this.shouldDance = isPlaying;
	}
}