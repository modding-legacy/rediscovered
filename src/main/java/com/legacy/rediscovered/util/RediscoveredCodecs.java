package com.legacy.rediscovered.util;

import java.util.UUID;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

public class RediscoveredCodecs
{
	public static final Codec<UUID> UUID_CODEC = Codec.STRING.flatXmap(s ->
	{
		try
		{
			UUID uuid = UUID.fromString(s);
			return DataResult.success(uuid);
		}
		catch (IllegalArgumentException e)
		{
			return DataResult.error(e::getMessage);
		}
	}, uuid ->
	{
		return DataResult.success(uuid.toString());
	});
}
