package com.legacy.rediscovered.util;

import net.minecraft.util.RandomSource;

/**
 * General math utilities
 */
public class LogicUtil
{
	/**
	 * Returns original plus or minus a random value within range
	 */
	public static double withinRange(double original, double range, RandomSource rand)
	{
		return (range * 2 * rand.nextDouble()) - range + original;
	}

	/**
	 * Returns a 0 plus or minus a random value within degree
	 */
	public static double withinRange(double range, RandomSource rand)
	{
		return withinRange(0.0D, range, rand);
	}

	/**
	 * Returns original plus or minus a random value within range
	 */
	public static float withinRange(float original, float range, RandomSource rand)
	{
		return (range * 2 * rand.nextFloat()) - range + original;
	}

	/**
	 * Returns a 0 plus or minus a random value within degree
	 */
	public static float withinRange(float range, RandomSource rand)
	{
		return withinRange(0.0F, range, rand);
	}

	/**
	 * Returns original plus or minus a random value within range (inclusive)
	 */
	public static int withinRange(int original, int range, RandomSource rand)
	{
		return rand.nextInt(range * 2 + 1) - range + original;
	}

	/**
	 * Returns a 0 plus or minus a random value within degree (inclusive)
	 */
	public static int withinRange(int range, RandomSource rand)
	{
		return withinRange(0, range, rand);
	}

	/**
	 * Converts oldValue from the range of oldMin to oldMax to a number within the
	 * range of newMin and newMax
	 */
	public static double convertToNewRange(double oldValue, double oldMin, double oldMax, double newMin, double newMax)
	{
		return ((oldValue - oldMin) / (oldMax - oldMin)) * (newMax - newMin) + newMin;
	}

	/**
	 * For lerping with 360 rotations
	 */
	public static float rotLerp(float start, float end, float amount)
	{
		float difference = Math.abs(end - start);

		if (difference > 180)
		{
			if (end > start)
				start += 360;
			else
				end += 360;
		}

		float value = (start + ((end - start) * amount));

		float rangeZero = 360;

		if (value >= 0 && value <= 360)
			return value;

		return (value % rangeZero);
	}
}
