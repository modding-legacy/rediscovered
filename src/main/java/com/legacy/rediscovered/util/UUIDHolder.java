package com.legacy.rediscovered.util;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

public record UUIDHolder(Optional<UUID> uuid)
{
	public static final UUIDHolder EMPTY = new UUIDHolder(Optional.empty());
	public static final Codec<UUIDHolder> CODEC = RecordCodecBuilder.create(instance -> instance.group(RediscoveredCodecs.UUID_CODEC.optionalFieldOf("uuid").forGetter(UUIDHolder::uuid)).apply(instance, UUIDHolder::new));

	public static UUIDHolder of(@Nullable UUID uuid)
	{
		return uuid == null ? EMPTY : new UUIDHolder(Optional.of(uuid));
	}
}
