package com.legacy.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.javafmlmod.FMLJavaModLoadingContext;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.RegisterCommandsEvent;

@Mod(TestMod.MODID)
public class TestMod
{
	public static final String MODID = "test";
	public static final Logger LOGGER = LogManager.getLogger("ModdingLegacy/" + TestMod.MODID);

	public static boolean pathfinding, goals;

	public TestMod()
	{
		IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();
		IEventBus forgeBus = NeoForge.EVENT_BUS;
		
		modBus.addListener(TestMod::commonInit);
		
		forgeBus.addListener(TestMod::onCommandsRegistered);
		forgeBus.register(TestEvents.class);
	}

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static String find(String key)
	{
		return MODID + ":" + key;
	}

	public static void commonInit(final FMLCommonSetupEvent event)
	{
		TestPacketHandler.register();
	}

	public static void clientInit(final FMLClientSetupEvent event)
	{
	}

	public static void onCommandsRegistered(final RegisterCommandsEvent event)
	{
		TestModCommand.register(event.getDispatcher());
	}
}
