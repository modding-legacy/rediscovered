package com.legacy.test.mixin;

import java.util.Set;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.world.level.pathfinder.Node;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.level.pathfinder.Target;

@Mixin(Path.class)
public interface PathInvoker
{
	@Invoker("setDebug")
	abstract void setDebug(Node[] p_164710_, Node[] p_164711_, Set<Target> p_164712_);
}
