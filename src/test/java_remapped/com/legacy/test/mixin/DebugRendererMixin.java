package com.legacy.test.mixin;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.debug.DebugRenderer;
import net.minecraft.client.renderer.debug.GoalSelectorDebugRenderer;
import net.minecraft.client.renderer.debug.PathfindingRenderer;

@Mixin(DebugRenderer.class)
public class DebugRendererMixin
{
	@Shadow
	@Final
	public PathfindingRenderer pathfindingRenderer;

	@Shadow
	@Final
	public GoalSelectorDebugRenderer goalSelectorRenderer;

	@Inject(at = @At("HEAD"), method = "render(Lcom/mojang/blaze3d/vertex/PoseStack;Lnet/minecraft/client/renderer/MultiBufferSource$BufferSource;DDD)V", cancellable = true)
	private void render(PoseStack pPoseStack, MultiBufferSource.BufferSource pBufferSource, double pCamX, double pCamY, double pCamZ, CallbackInfo callback)
	{
		this.goalSelectorRenderer.render(pPoseStack, pBufferSource, pCamX, pCamY, pCamZ);
		this.pathfindingRenderer.render(pPoseStack, pBufferSource, pCamX, pCamY, pCamZ);
	}
}
