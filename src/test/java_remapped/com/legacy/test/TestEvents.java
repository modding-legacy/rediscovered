package com.legacy.test;

import com.legacy.test.network.ClearGoalsPacket;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.EntityLeaveLevelEvent;

public class TestEvents
{
	@SubscribeEvent
	public static void onEntityExit(EntityLeaveLevelEvent event)
	{
		if (TestMod.goals)
			TestPacketHandler.sendToAllWorlds(new ClearGoalsPacket(event.getEntity().getId()));
	}

}
