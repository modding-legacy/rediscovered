package com.legacy.test.hooks;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nullable;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.blaze3d.vertex.VertexFormat;

import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.level.pathfinder.Node;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.level.pathfinder.Target;
import net.neoforged.fml.util.ObfuscationReflectionHelper;

public class DebugHooks
{
	public static void sendPathFindingPacket(ServerLevel pLevel, Mob pMob, @Nullable Path path, float pMaxDistanceToWaypoint, FriendlyByteBuf buf)
	{
		try
		{
			buf.writeInt(pMob.getId());
			buf.writeFloat(pMaxDistanceToWaypoint);

			Set<Target> targets = new HashSet<>();

			for (int i = 0; i < path.getNodeCount(); i++)
				targets.add(new Target(path.getNode(i)));

			ObfuscationReflectionHelper.setPrivateValue(Path.class, path, targets, "targetNodes");

			path.writeToStream(buf);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void renderPathLine(PoseStack pose, VertexConsumer vertex, Path pPath, double pX, double pY, double pZ)
	{
		try
		{
			/*RenderSystem.setShaderColor(1F, 1F, 1F, 1F);
			
			Tesselator tesselator = Tesselator.getInstance();
			BufferBuilder bufferbuilder = tesselator.getBuilder();
			RenderSystem.setShader(GameRenderer::getRendertypeLinesShader);
			RenderSystem.disableCull();
			
			RenderSystem.lineWidth(7F);
			bufferbuilder.begin(VertexFormat.Mode.LINE_STRIP, DefaultVertexFormat.POSITION_COLOR_NORMAL);
			
			for (int i = 0; i < pPath.getNodeCount(); ++i)
			{
				Node node = pPath.getNode(i);
				if (!(distanceToCamera(node.asBlockPos(), pX, pY, pZ) > 80.0F))
				{
					float f = (float) i / (float) pPath.getNodeCount() * 0.33F;
					int j = i == 0 ? 0 : Mth.hsvToRgb(f, 0.9F, 0.9F);
					int k = j >> 16 & 255;
					int l = j >> 8 & 255;
					int i1 = j & 255;
			
					bufferbuilder.vertex((double) node.x - pX + 0.5D, (double) node.y - pY + 0.5D, (double) node.z - pZ + 0.5D).color(k, l, i1, 255).normal(1F, 1F, 1F).endVertex();
				}
			}
			
			tesselator.end();
			
			RenderSystem.enableCull();
			RenderSystem.lineWidth(1);
			//GlStateManager._depthMask(false);
			RenderSystem.setShaderColor(1F, 1F, 1F, 0.75F);*/

			for (int i = 0; i < pPath.getNodeCount(); ++i)
			{
				Node node = pPath.getNode(i);
				if (!(distanceToCamera(node.asBlockPos(), pX, pY, pZ) > 80.0F))
				{
					float f = (float) i / (float) pPath.getNodeCount() * 0.33F;
					int j = i == 0 ? 0 : Mth.hsvToRgb(f, 0.9F, 0.9F);
					int k = j >> 16 & 255;
					int l = j >> 8 & 255;
					int i1 = j & 255;
					vertex.vertex(pose.last().pose(), (float) ((double) node.x - pX + 0.5D), (float) ((double) node.y - pY + 0.5D), (float) ((double) node.z - pZ + 0.5D)).color(k, l, i1, 255).endVertex();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static float distanceToCamera(BlockPos pPos, double pX, double pY, double pZ)
	{
		return (float) (Math.abs((double) pPos.getX() - pX) + Math.abs((double) pPos.getY() - pY) + Math.abs((double) pPos.getZ() - pZ));
	}
}
