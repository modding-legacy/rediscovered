package com.legacy.test.network;

import java.util.function.Supplier;

import net.minecraft.network.FriendlyByteBuf;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.DistExecutor;
import net.neoforged.neoforge.network.NetworkEvent;
import var;

public class ClearGoalsPacket
{
	private final int id;

	public ClearGoalsPacket(int id)
	{
		this.id = id;
	}

	// write
	public static void encoder(ClearGoalsPacket packet, FriendlyByteBuf buff)
	{
		buff.writeInt(packet.id);
	}

	// read
	public static ClearGoalsPacket decoder(FriendlyByteBuf buff)
	{
		return new ClearGoalsPacket(buff.readInt());
	}

	// handler
	public static void handler(ClearGoalsPacket packet, Supplier<NetworkEvent.Context> context)
	{
		context.get().enqueueWork(() -> DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> handlePacket(packet)));
		context.get().setPacketHandled(true);
	}

	// What to do. Could be put in handler but this is cleaner.
	@OnlyIn(Dist.CLIENT)
	private static void handlePacket(ClearGoalsPacket packet)
	{
		var mc = net.minecraft.client.Minecraft.getInstance();

		if (packet.id < 0)
			mc.debugRenderer.goalSelectorRenderer.clear();
		else
			mc.debugRenderer.goalSelectorRenderer.removeGoalSelector(packet.id);
	}
}
