package com.legacy.test;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.test.network.ClearGoalsPacket;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.common.util.FakePlayer;
import net.neoforged.neoforge.network.NetworkDirection;
import net.neoforged.neoforge.network.NetworkEvent;
import net.neoforged.neoforge.network.NetworkRegistry;
import net.neoforged.neoforge.network.simple.SimpleChannel;
import net.neoforged.neoforge.server.ServerLifecycleHooks;

public class TestPacketHandler
{
	private static final String PROTOCOL_VERSION = "1";
	private static int index = 0;
	public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(TestMod.locate("main"), () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals);

	public static void register()
	{
		// Server -> Client
		register(ClearGoalsPacket.class, ClearGoalsPacket::encoder, ClearGoalsPacket::decoder, ClearGoalsPacket::handler);
	}

	private static <MSG> void register(Class<MSG> packet, BiConsumer<MSG, FriendlyByteBuf> encoder, Function<FriendlyByteBuf, MSG> decoder, BiConsumer<MSG, Supplier<NetworkEvent.Context>> messageConsumer)
	{
		INSTANCE.registerMessage(index, packet, encoder, decoder, messageConsumer);
		index++;
	}

	/**
	 * Server -> Client
	 * 
	 * @param packet
	 * @param serverPlayer
	 */
	public static void sendToClient(Object packet, ServerPlayer serverPlayer)
	{
		if (!(serverPlayer instanceof FakePlayer) && serverPlayer.connection != null)
			INSTANCE.sendTo(packet, serverPlayer.connection.connection, NetworkDirection.PLAY_TO_CLIENT);
	}

	/**
	 * Server -> Clients in same world
	 * 
	 * @param packet
	 * @param world
	 */
	public static void sendToAllClients(Object packet, Level world)
	{
		world.players().forEach(player -> sendToClient(packet, (ServerPlayer) player));
	}
	
	/**
	 * Server -> Clients every world
	 * 
	 * @param packet
	 * @param world
	 */
	public static void sendToAllWorlds(Object packet)
	{
		MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
		
		if (server != null)
			server.getPlayerList().getPlayers().forEach(player -> sendToClient(packet, player));
	}

	/**
	 * Client -> Server
	 * 
	 * @param packet
	 */
	public static void sendToServer(Object packet)
	{
		INSTANCE.sendToServer(packet);
	}
}
