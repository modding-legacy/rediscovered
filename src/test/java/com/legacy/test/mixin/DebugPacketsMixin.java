/*package com.legacy.test.mixin;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.test.TestMod;
import com.legacy.test.hooks.DebugHooks;

import io.netty.buffer.Unpooled;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.game.ClientboundCustomPayloadPacket;
import net.minecraft.network.protocol.game.DebugPackets;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.GoalSelector;
import net.minecraft.world.entity.ai.goal.WrappedGoal;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.pathfinder.Path;

@Mixin(DebugPackets.class)
public class DebugPacketsMixin
{

	@Shadow
	private static void sendPacketToAllPlayers(ServerLevel p_133692_, FriendlyByteBuf p_133693_, ResourceLocation p_133694_)
	{
	}

	@Inject(at = @At("HEAD"), method = "sendGoalSelector(Lnet/minecraft/world/level/Level;Lnet/minecraft/world/entity/Mob;Lnet/minecraft/world/entity/ai/goal/GoalSelector;)V", cancellable = true)
	private static void sendGoalSelector(Level level, Mob entity, GoalSelector selector, CallbackInfo callback)
	{
		if (TestMod.goals && level instanceof ServerLevel sl)
		{
			FriendlyByteBuf buf = new FriendlyByteBuf(Unpooled.buffer());
			buf.writeBlockPos(entity.blockPosition().above((int) entity.getEyeHeight()));
			List<WrappedGoal> goals = selector.getAvailableGoals().stream().collect(Collectors.toList());
			buf.writeInt(entity.getId());
			buf.writeInt(goals.size());

			for (int i = 0; i < goals.size(); i++)
			{
				var goal = goals.get(i);
				buf.writeInt(i);
				buf.writeBoolean(goal.isRunning());
				buf.writeUtf(goal.getGoal().toString(), 255);
			}

			sendPacketToAllPlayers(sl, buf, ClientboundCustomPayloadPacket.DEBUG_GOAL_SELECTOR);
		}
	}

	@Inject(at = @At("HEAD"), method = "sendPathFindingPacket(Lnet/minecraft/world/level/Level;Lnet/minecraft/world/entity/Mob;Lnet/minecraft/world/level/pathfinder/Path;F)V", cancellable = true)
	private static void sendPathFindingPacket(Level pLevel, Mob pMob, @Nullable Path path, float pMaxDistanceToWaypoint, CallbackInfo callback)
	{
		if (TestMod.pathfinding && pLevel instanceof ServerLevel sl && path != null)
		{
			FriendlyByteBuf buf = new FriendlyByteBuf(Unpooled.buffer());
			DebugHooks.sendPathFindingPacket(sl, pMob, path, pMaxDistanceToWaypoint, buf);
			sendPacketToAllPlayers(sl, buf, ClientboundCustomPayloadPacket.DEBUG_PATHFINDING_PACKET);
		}
	}
}
*/