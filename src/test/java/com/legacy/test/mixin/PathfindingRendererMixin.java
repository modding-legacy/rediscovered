package com.legacy.test.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.debug.PathfindingRenderer;
import net.minecraft.world.level.pathfinder.Path;

@Mixin(PathfindingRenderer.class)
public class PathfindingRendererMixin
{
	@Inject(at = @At("HEAD"), method = "renderPathLine(Lcom/mojang/blaze3d/vertex/PoseStack;Lcom/mojang/blaze3d/vertex/VertexConsumer;Lnet/minecraft/world/level/pathfinder/Path;DDD)V", cancellable = true)
	private static void renderPathLine(PoseStack pose, VertexConsumer vertex, Path path, double pX, double pY, double pZ, CallbackInfo callback)
	{
		//DebugHooks.renderPathLine(pose, vertex, path, pX, pY, pZ);
		//callback.cancel();
		// System.out.println("BURGER " + path.toString());
	}
}
