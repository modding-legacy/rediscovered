/*package com.legacy.test;

import com.legacy.test.network.ClearGoalsPacket;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;

public class TestModCommand
{
	public static void register(CommandDispatcher<CommandSourceStack> dispatcher)
	{
		LiteralArgumentBuilder<CommandSourceStack> command = Commands.literal("mldebug");

		LiteralArgumentBuilder<CommandSourceStack> pathingShown = Commands.literal("pathfinding").requires(source -> source.hasPermission(2));
		pathingShown.then(Commands.argument("active", BoolArgumentType.bool()).executes(context -> setPathfindingShown(context, BoolArgumentType.getBool(context, "active"))));
		command.then(pathingShown);

		LiteralArgumentBuilder<CommandSourceStack> goalsShown = Commands.literal("goals").requires(source -> source.hasPermission(2));
		goalsShown.then(Commands.argument("active", BoolArgumentType.bool()).executes(context -> setGoalsShown(context, BoolArgumentType.getBool(context, "active"))));
		command.then(goalsShown);

		dispatcher.register(command);
	}

	private static int setPathfindingShown(CommandContext<CommandSourceStack> context, boolean flag)
	{
		TestMod.pathfinding = flag;
		context.getSource().sendSuccess(() -> Component.literal(!flag ? "Pathfinding display has been disabled." : "Pathfinding now shown."), true);

		return 1;
	}

	private static int setGoalsShown(CommandContext<CommandSourceStack> context, boolean flag)
	{
		TestMod.goals = flag;
		context.getSource().sendSuccess(() -> Component.literal(!flag ? "AI Goal display has been disabled." : "AI Goals now shown."), true);

		if (!flag)
			TestPacketHandler.sendToAllWorlds(new ClearGoalsPacket(-1));

		return 1;
	}
}
*/